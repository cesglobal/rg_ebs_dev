#!/bin/bash
. $RGAP_TOP/bin/RGAP_TEMPLATE.sh 3286
# +============================================================================+
# | CREATE LOGFILE HEADER
# +============================================================================+
WRAPLOG

# +============================================================================+
# | INSTALL PROCESS
# +============================================================================+
echo " "                                                               |${TEE}
echo "Begin Of Installation Process..."                                |${TEE}

# +============================================================================+
# | Reference Information for LDTs
# +============================================================================+
# afcprset.lct --> Request Set                                $FND_TOP
# afcpprog.lct --> Concurrent Program with option executable  $FND_TOP
# affrmcus.lct --> Forms Personalization                      $FND_TOP
# afscprof.lct --> Profile Option                             $FND_TOP
# afffload.lct --> Value Set with optional Values             $FND_TOP
# afscursp.lct --> Responsibility                             $FND_TOP
# afsload.lct  --> Menu                                       $FND_TOP
# afcpreqg.lct --> Request Group                              $FND_TOP
# aflvmlu.lct  --> FND Lookups                                $FND_TOP
# afffload.lct --> Descriptive Flexfields                     $FND_TOP
# afmdmsg.lct  --> Message                                    $FND_TOP
# afscursp.lct --> User                                       $FND_TOP
# alr.lct      --> Alert                                      $ALR_TOP  
# xdotmpl.lct  --> XDO Loader                                 $XDO_TOP

# +============================================================================+
# | CUSTOM CALLS
# |
# +============================================================================+
echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Types"                              |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENTYP MY_TYPE_SCRIPT.typ
 
echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Tables"                             |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENTBL MY_TABLE_SCRIPT.tbl

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Copy Control Files"                          |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# CPCTRL MY_CONTROL_FILE.ctl

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Copy Graphic Files"                          |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# CPGIFF comp_logo.gif

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Copy SQL Plus Files"                         |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# CPSQL MY_SQL_PGM.sql


echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Copy Report Files"                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# CPRPT MY_AUDIT_REPORT.rdf

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Sequences"                          |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENSEQ MY_TABLE_SEQUENCE.seq

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Synonyms"                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENSYN MY_SYN_CREATION.syn

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Functions"                          |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Functions
# GENPKG XX_FUNC.sql

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating View"                               |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENVIEW MY_DB_VIEW.vw

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Index"                              |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENIDX MY_TABLE_INDEX.idx

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Libraries"                          |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: <Library Name without Extension>
# GENLIB CUSTOM 
 
echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Running Script"                              |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# RUNSRPT UPDATE_CUSTOM_TABLE.SQL
RUNSRPT RG_MONITOR_UPDATES.sql

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Installing Workflows"                        |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENWF MY_NEW_CUSTOM_WORKFLOW.wft
 
echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Installing Packages"                         |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENPKG MY_PACKAGE_SPEC.pks
# GENPKG MY_PACKAGE_BODY.pkb
GENPKG rgap_interface_sync_pkg.pks
GENPKG rgap_interface_sync_pkg.pkb

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Grants"                             |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GTSRPT  MY_GRANT_CREATION.gt


echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Trigger"                            |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENTRG MY_DB_TRIGGER.trg

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating Forms"                              |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Example: 
# GENFORM XXCUST_PO_FORM <FORM Name without Extension>

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Creating LDTs"                               |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Examples: (See LDT Reference section)
# GENLDT $XDO_TOP xdotmpl.lct MY_XDO_LDT.ldt
# GENLDT $FND_TOP afcpprog.lct my_conc_pgm.ldt
# GENLDT $ALR_TOP alr.lct XX_ALERT_NAME.ldt
GENLDT $FND_TOP afcpprog.lct RGAP_INTERFACE_CONTROL.ldt
GENLDT $FND_TOP afcpreqg.lct RGAP_INTERFACE_CONTROL_REQU.ldt

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Uploading BI Publisher Templates"            |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Examples:
# GENXDO IBYPOS_PAY_en.rtf RTF-ETEXT "<Positive pay lob code with spaces>" IBY TEMPLATE
# GENXDO XXWSHPACKRPT_XML.rtf RTF XXCUSTWSHPACKRP OTPCSTM TEMPLATE

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Uploading OAF related zip(class) files"      |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Examples:
# CPOAFZIP aso.zip 
# Note: File will be copied and the directory structure will be created under $JAVA_TOP/oracle/apps
# CPOAFZIP aso.zip

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Uploading OAF XML files"                         |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Examples:
# GENOAFXML QuoteSummaryPG.xml "/xxtams/oracle/apps/cz/quotesummary/webui" 

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Uploading OAF JPX files"                     |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}

#GENOAFJPX Tams_EXT_080_Prj.jpx

echo " "                                           |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
echo "Uploading OAF Personalizations"             |${TEE}
echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
# Examples:
# GENOAFPER QotAddProductPG.xml "/oracle/apps/qot/quote/inventory/webui/customizations/site/0"


# ---Start Soft Link
# Below example is used only for creating the soft link for host program
# Uncomment and use it if required and delete the comments
#

#echo " "                                           |${TEE}
#echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
#echo "Copying Files"                               |${TEE}
#echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
  
#  CPFILE xxokscvrevrecon.ctl $BINDIR
#  CPFILE xxokscvrevrecon.prog $BINDIR
#  chmod 755 ${BINDIR}/xxokscvrevrecon.prog
  
#echo " "                                           |${TEE}
#echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
#echo "Creating a link for xxokscvrevrecon.prog "   |${TEE}
#echo "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~"      |${TEE}
 
#  cd $BINDIR
#  if [ ! -f xxokscvrevrecon ] ; then
#    echo "xxokscvrevrecon Link File is not present. Creating link"    
#   ln -s $FND_TOP/bin/fndcpesr xxokscvrevrecon
#  fi

# --End Soft Link

echo " "                                                                |${TEE}
echo "End of Installation Process."                                     |${TEE}


# +============================================================================+
# | CREATE LOGFILE FOOTER
# +============================================================================+
WRAPLOG


# +===========================================================================+
# |                           END OF SCRIPT                                   |
# +===========================================================================+


REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: rgap_vendorintf_nested_tabletype.sql
REM PURPOSE : To create the custom rgap_vendorintf_nested_tabletype table type
REM PARAMETERS : 
REM CALLED :
REM CALLED BY :rgap_coupa_vendorintf_pkg
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 19-AUG-15 1.0 INITIAL RELEASE
REM
REM ======================================================================

REM DROP TYPE RGAP_VENDOR_NESTED_TABLE;

REM DROP TYPE RGAP_VENDOR_COL_TYPE;

CREATE OR REPLACE TYPE rgap_vendor_col_type IS OBJECT
                  (Supplier_Name VARCHAR2 (240 BYTE),
                   Display_Name VARCHAR2 (320 BYTE),
                   Inactive_Flag VARCHAR2 (20 BYTE),
                   Supplier_Number VARCHAR2 (30 BYTE),
                   Account_Number VARCHAR2 (15 BYTE),
                   Tax_ID VARCHAR2 (20 BYTE),
                   Tax_Code VARCHAR2 (50 BYTE),
                   DUNS VARCHAR2 (30 BYTE),
                   EMAIL_ADDRESS VARCHAR2 (2000 BYTE),
                   Mobile_Number VARCHAR2 (60 BYTE),
                   Contact_Number VARCHAR2 (60 BYTE),
                   Fax_Number VARCHAR2 (60 BYTE),
                   Contact_Name_Given VARCHAR2 (150 BYTE),
                   Contact_Name_Family VARCHAR2 (150 BYTE),
                   VAT_REGISTRATION_NUM VARCHAR2 (20 BYTE),
                   ADDRESS1 VARCHAR2 (240 BYTE),
                   ADDRESS2 VARCHAR2 (240 BYTE),
                   CITY VARCHAR2 (240 BYTE),
                   STATE VARCHAR2 (60 BYTE),
                   POSTAL_CODE VARCHAR2 (60 BYTE),
                   COUNTRY VARCHAR2 (60 BYTE),
                   match_option VARCHAR2 (25 BYTE),
                   payment_method_lookup_code VARCHAR2 (25 BYTE),
                   Primary_Address_VAT VARCHAR2 (240 BYTE),
                   Terms VARCHAR2 (50 BYTE),
                   Invoice_Email VARCHAR2 (2000 BYTE));

/
CREATE OR REPLACE TYPE rgap_vendor_nested_table is table of rgap_vendor_col_type;
/



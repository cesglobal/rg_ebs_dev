REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: rgap_payment_nested_tabletype.sql
REM PURPOSE : To create the custom rgap_payment_nested_tabletype table type
REM PARAMETERS : 
REM CALLED :
REM CALLED BY :rgap_coupa_payments_pkg
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 19-AUG-15 1.0 INITIAL RELEASE
REM
REM ======================================================================

REM DROP Type rgap_payment_nested_table;

CREATE OR REPLACE TYPE rgap_payment_col_type is OBJECT (
       Supplier_Name  VARCHAR2(240),
       Supplier_Number VARCHAR2(30),
       PO_Number VARCHAR2(20),
       Invoice_Number VARCHAR2(50),
	   Legacy_Inv_Num VARCHAR2(50),
       Paid_Infull_Date DATE,
       Amount_Paid Number,
       Check_Number Number(15),
       Payment_Date DATE,
       Payment_Method_Lookup_Code VARCHAR2(25),
       Note VARCHAR2(240)
 );
/

CREATE OR REPLACE TYPE rgap_payment_nested_table is table of rgap_payment_col_type;
/

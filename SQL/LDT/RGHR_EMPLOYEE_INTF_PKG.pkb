CREATE OR REPLACE PACKAGE BODY APPS.rghr_employee_intf_pkg
IS
   ------------------------------------------------------------------------
   -- Name...: APPS.rghr_employee_cnv_pkg.pkb
   -- Desc...: This package is used to Migrate Employees data to EBS
   --
   --
   -- History:
   --
   -- Date Name Version Number Revision Summary
   -- ------------ -------------- --------------- ------------------
   -- 13-Aug-2015 Srinivas Anne 1.0
   -- 26-Nov-2015 Srinivas Anne 1.1     Employee Gender validation changed
   --
   -- 30-Aug-2016 - Enhancement#125-- Prabhat
   -- New procedure diable_user is added to the pacakge
   -- this procedure is called within rghr_emp_term+proc per#125 requirement
   -- to check if there is a EBS account for the employee that is being termianted
   -- and EBS account is end dated with sysdate if the employee record is terminated 
   -- successfully.
   --
   -- ADDITIONAL NOTES
   -- ================
   --
   ------------------------------------------------------------------------
   PROCEDURE rg_log (p_msgtxt_in IN VARCHAR2) IS
   BEGIN
      IF fnd_global.conc_login_id = -1
      THEN
         DBMS_OUTPUT.put_line (p_msgtxt_in);
      ELSE
         fnd_file.put_line (fnd_file.LOG, p_msgtxt_in);
      END IF;

      fnd_file.put_line (fnd_file.LOG, p_msgtxt_in);
   END rg_log;

   -- *************************************************
   -- Procedure to print messages or notes in the
   -- OUTPUT file of the concurrent program
   -- ************************************************
   PROCEDURE rg_output (p_msgtxt_in IN VARCHAR2) IS
   BEGIN
      IF fnd_global.conc_login_id = -1
      THEN
         DBMS_OUTPUT.put_line (p_msgtxt_in);
      ELSE
         fnd_file.put_line (fnd_file.output, p_msgtxt_in);
      END IF;
   END rg_output;
   
   -- **************************************************
   -- New Procedure added as part of enhancement#125
   -- to disable a EBS account if the associated employee is 
   -- terminated in HR
   -- **************************************************
    PROCEDURE disable_user(p_user_name IN fnd_user.user_name%type) IS
   
     begin
     
        fnd_user_pkg.disableuser(p_user_name); 
        
     end;
     
     

   PROCEDURE rghr_emp_ass_dup_proc (p_errcode   OUT VARCHAR2,
                                                                         p_errmess   OUT VARCHAR2) AS
                                                                         
   BEGIN
      UPDATE   rgper_emp_details_stg
         SET   rec_status = 'D'
       WHERE   ROWID NOT IN (  SELECT   MAX (ROWID)
                                 FROM   rgper_emp_details_stg
                             GROUP BY   effective_start_date,
                                        effective_end_date,
                                        person_type,
                                        last_name,
                                        email_address,
                                        employee_number,
                                        first_name,
                                        known_as,
                                        middle_names,
                                        internal_location,
                                        business_group_name,
                                        attribute1,
                                        attribute2,
                                        attribute3,
                                        attribute4,
                                        attribute5,
                                        attribute6,
                                        attribute7,
                                        attribute8,
                                        attribute9,
                                        attribute10)
               AND (rec_status IS NULL OR rec_status = 'N');

      COMMIT;

      UPDATE   rgper_emp_assignment_stg
         SET   rec_status = 'D'
       WHERE   ROWID NOT IN (  SELECT   MAX (ROWID)
                                 FROM   rgper_emp_assignment_stg
                             GROUP BY   employee_number,
                                        effective_start_date,
                                        supervisor_number,
                                        organization_id,
                                        job,
                                        people_group,
                                        POSITION,
                                        grade,
                                        payroll,
                                        LOCATION,
                                        ledger_name,
                                        default_code_comb_id,
                                        business_group_name,
                                        attribute1,
                                        attribute2,
                                        attribute3,
                                        attribute4,
                                        attribute5,
                                        attribute6,
                                        attribute7,
                                        attribute8,
                                        attribute9,
                                        attribute10)
               AND (rec_status IS NULL OR rec_status = 'N');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errcode := '1';
         p_errmess := SUBSTR (SQLERRM, 1, 299);
   END;

   PROCEDURE rghr_employee_val_proc (p_errcode   OUT VARCHAR2,
                                                                         p_errmess   OUT VARCHAR2) AS
      CURSOR c_emp_rec IS
         SELECT   ed.ROWID,
                  (SELECT   assignment_id
                     FROM   per_all_assignments_f
                    WHERE   person_id = ed.person_id
                            AND ed.effective_start_date BETWEEN effective_start_date
                                                            AND  effective_end_date)
                     d_assignment_id,
                  (SELECT   business_group_id
                     FROM   per_business_groups
                    WHERE   NAME = ed.business_group_name)
                     buss_group_id,
                  ed.*
           FROM   rgper_emp_details_stg ed
          WHERE   1 = 1 AND rec_status = 'N'--AND business_group_name = 'Riot Games Business Group'
                                            --AND employee_number = '0001485'
                                            --'0000868'
                                            --AND effective_end_date IS NULL
   ;

      l_errm                           VARCHAR2 (3000) := NULL;
      l_cnt_empno                      NUMBER := 0;
      l_empno                          VARCHAR2 (300) := NULL;
      --l_sex CHAR (1) := NULL;
      l_national_code                  VARCHAR2 (300) := NULL;
      l_mar_status_code                VARCHAR2 (300) := NULL;
      l_reg_code                       VARCHAR2 (300) := NULL;
      l_emp_arb_name                   VARCHAR2 (3000) := NULL;
      l_title_code                     VARCHAR2 (3000) := NULL;
      l_last_name                      VARCHAR2 (3000) := NULL;
      l_sex                            VARCHAR2 (3000) := NULL;
      l_err_message                    VARCHAR2 (3000) := NULL;
      lc_employee_number               per_all_people_f.employee_number%TYPE;
      ln_person_id                     per_all_people_f.person_id%TYPE;
      ln_assignment_id                 per_all_assignments_f.assignment_id%TYPE;
      ln_object_ver_number             per_all_assignments_f.object_version_number%TYPE;
      ln_asg_ovn                       NUMBER;
      ld_per_effective_start_date      per_all_people_f.effective_start_date%TYPE;
      ld_per_effective_end_date        per_all_people_f.effective_end_date%TYPE;
      lc_full_name                     per_all_people_f.full_name%TYPE;
      ln_per_comment_id                per_all_people_f.comment_id%TYPE;
      ln_assignment_sequence           per_all_assignments_f.assignment_sequence%TYPE;
      lc_assignment_number             per_all_assignments_f.assignment_number%TYPE;
      lb_name_combination_warning      BOOLEAN;
      lb_assign_payroll_warning        BOOLEAN;
      lb_orig_hire_warning             BOOLEAN;
      -- Local Variables
      -- -----------------------
      ln_object_version_number per_all_people_f.object_version_number%TYPE
            := 1 ;
      lc_dt_ud_mode                    VARCHAR2 (100) := NULL;
      --ln_assignment_id PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_ID%TYPE := 33564;
      --lc_employee_number PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE := 'PRAJ_01';

      -- Out Variables for Find Date Track Mode API
      -- ----------------------------------------------------------------
      lb_correction                    BOOLEAN;
      lb_update                        BOOLEAN;
      lb_update_override               BOOLEAN;
      lb_update_change_insert          BOOLEAN;
      -- Out Variables for Update Employee API
      -- -----------------------------------------------------------
      ld_effective_start_date          DATE;
      ld_effective_end_date            DATE;
      --lc_full_name PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
      ln_comment_id                    per_all_people_f.comment_id%TYPE;
      --lb_name_combination_warning BOOLEAN;
      --lb_assign_payroll_warning BOOLEAN;
      l_validate                       BOOLEAN := FALSE;
      l_period_of_service_id           NUMBER;
      l_object_version_number          NUMBER;
      l_actual_notice_period_date      DATE;
      l_effective_date                 DATE;
      l_supervisor_warning             BOOLEAN;
      l_event_warning                  BOOLEAN;
      l_interview_warning              BOOLEAN;
      l_review_warning                 BOOLEAN;
      l_recruiter_warning              BOOLEAN;
      l_asg_future_changes_warning     BOOLEAN;
      l_f_asg_future_changes_warning   BOOLEAN;
      l_pay_proposal_warning           BOOLEAN;
      l_dod_warning                    BOOLEAN;
      l_final_process_date             DATE;
      l_org_now_no_manager_warning     BOOLEAN;
      l_entries_changed_warning        VARCHAR2 (255);
      l_f_entries_changed_warning      VARCHAR2 (255);
      l_alu_change_warning             VARCHAR2 (255);
      --l_person_type_id NUMBER;
      l_last_std_process_date_out      DATE;
      l_person_type_id                 NUMBER := 0;
      l_val_errm                       VARCHAR2 (300) := NULL;
      l_val_procc_flag                 VARCHAR2 (300) := NULL;
      v_person_id                      NUMBER;
      v_per_object_version_number      NUMBER;
      v_per_effective_start_date       DATE;
      v_per_effective_end_date         DATE;
      v_pdp_object_version_number      NUMBER;
      v_full_name                      VARCHAR2 (240);
      v_comment_id                     NUMBER;
      v_assignment_id                  NUMBER;
      v_asg_object_version_number      NUMBER;
      v_assignment_sequence            NUMBER;
      v_assignment_number              VARCHAR2 (240);
      v_name_combination_warning       BOOLEAN;
      v_npw_number                     VARCHAR2 (240);
      lext_person_id                   NUMBER := 0;
      l_cnt_cw_empno                   NUMBER := 0;
      l_cnt_cw_pid                     NUMBER := 0;
      
   BEGIN
   
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      FOR i_emp_rec IN c_emp_rec
      LOOP
         l_cnt_empno := NULL;
         lext_person_id := NULL;
         l_cnt_cw_empno := NULL;
         l_cnt_cw_pid := NULL;
         l_sex := NULL;
         l_person_type_id := NULL;
         l_val_errm := NULL;
         l_val_procc_flag := NULL;

         /*Check Employees count*/
         IF i_emp_rec.person_type != 'Contingent Worker'
         THEN
            BEGIN
                 SELECT   COUNT (0), person_id
                   INTO   l_cnt_empno, lext_person_id
                   FROM   per_all_people_f
                  WHERE   employee_number = TRIM (i_emp_rec.employee_number)
                          AND business_group_id = 0
               GROUP BY   person_id;

               UPDATE   rgper_emp_details_stg
                  SET   person_id = lext_person_id
                WHERE   ROWID = i_emp_rec.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cnt_empno := NULL;
                  lext_person_id := NULL;
            END;
         ELSE
            BEGIN
                 SELECT   COUNT (0), ppf.person_id
                   INTO   l_cnt_cw_empno, l_cnt_cw_pid
                   FROM   per_periods_of_placement ppp, per_all_people_f ppf
                  WHERE       ppp.person_id = ppf.person_id
                          AND ppf.npw_number = TRIM (i_emp_rec.employee_number)
                          AND ppf.business_group_id = i_emp_rec.buss_group_id
               GROUP BY   ppf.person_id;

               UPDATE   rgper_emp_details_stg
                  SET   person_id = l_cnt_cw_pid
                WHERE   ROWID = i_emp_rec.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_cnt_cw_empno := NULL;
                  l_cnt_cw_pid := NULL;
            END;
         END IF;

         /*Employee gender validation */
         IF i_emp_rec.sex IS NOT NULL
         THEN
            BEGIN
               SELECT   DECODE (UPPER (TRIM (i_emp_rec.sex)),
                                'MALE', 'M',
                                'FEMALE', 'F',
                                'M', 'M',
                                'F', 'F',
                                'F')
                 INTO   l_sex
                 FROM   DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_sex := NULL;
            END;
         ELSE
            l_sex := 'F';
         END IF;

         /*Employee Person Type Validation*/
         IF i_emp_rec.person_type IS NOT NULL
         THEN
            BEGIN
               SELECT   person_type_id
                 INTO   l_person_type_id
                 FROM   per_person_types
                WHERE   user_person_type = i_emp_rec.person_type
                        AND business_group_id = i_emp_rec.buss_group_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_person_type_id := NULL;
            END;

            IF l_person_type_id IS NULL
            THEN
               l_val_errm :=
                  l_val_errm || ' - Employee Person Type is not valid';
               l_val_procc_flag := 'E';
            END IF;
         ELSE
            l_val_errm := 'Employee Person Type is Null';
            l_val_procc_flag := 'E';
         END IF;

         /*Employee Business Group Validation*/
         IF i_emp_rec.buss_group_id IS NULL
         THEN
            l_val_errm :=
               l_val_errm || ' - Employee Business Group is not valid ';
            l_val_procc_flag := 'E';
         END IF;

         /*Checking employee Process flag E or not*/
         BEGIN
            IF l_val_procc_flag != 'E'
            THEN
               UPDATE   rgper_emp_details_stg
                  SET   rec_status = 'E',
                        rec_message =
                           SUBSTR (l_val_errm || ' Val proc els', 1, 299)
                WHERE   ROWID = i_emp_rec.ROWID;
            ELSE
               UPDATE   rgper_emp_details_stg
                  SET   rec_status = 'V',
                        rec_message = 'Employee Record Validated',
                        person_type_id = l_person_type_id,
                        business_group_id = i_emp_rec.buss_group_id
                WHERE   ROWID = i_emp_rec.ROWID;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               ROLLBACK;
               l_errm := SUBSTR (SQLERRM, 1, 299);

               UPDATE   rgper_emp_details_stg
                  SET   rec_status = 'E',
                        rec_message =
                           SUBSTR (l_errm || ' Val Package ', 1, 290)
                WHERE   ROWID = i_emp_rec.ROWID;

               rg_log (SUBSTR (SQLERRM, 1, 299));
               COMMIT;
         END;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errmess := 'error';
         p_errcode := '1';
         l_errm := SUBSTR (SQLERRM, 1, 299);
         rg_log (SUBSTR (SQLERRM, 1, 299));
   END rghr_employee_val_proc;
   

   PROCEDURE rghr_emp_term_proc (p_errcode   OUT VARCHAR2,
                                 p_errmess   OUT VARCHAR2)
   AS
      CURSOR emp_rec_cur
      IS
         SELECT   ed.ROWID,
                  (SELECT   assignment_id
                     FROM   per_all_assignments_f
                    WHERE   person_id = ed.person_id
                            AND ed.effective_start_date BETWEEN effective_start_date
                                                            AND  effective_end_date)
                     d_assignment_id,
                  (SELECT   business_group_id
                     FROM   per_business_groups
                    WHERE   NAME = ed.business_group_name)
                     buss_group_id,
                  ed.*
           FROM   rgper_emp_details_stg ed
          WHERE       1 = 1
                  AND rec_status = 'V'
                  AND effective_end_date IS NOT NULL--and employee_number = '0001485'
   ;

      l_dat_effective_date             DATE;
      l_num_person_id                  NUMBER;
      l_dat_date_start                 DATE;
      l_num_ovn                        NUMBER;
      l_dat_final_process_date         DATE;
      l_dat_last_std_process_date      DATE;
      l_bool_supervisor_warning        BOOLEAN;
      l_bool_event_warning             BOOLEAN;
      l_bool_interview_warning         BOOLEAN;
      l_bool_review_warning            BOOLEAN;
      l_bool_recruiter_warning         BOOLEAN;
      l_bool_asg_future_warning        BOOLEAN;
      l_var_entries_changed_warning    VARCHAR2 (1000);
      l_bool_pay_proposal_warning      BOOLEAN;
      l_bool_dod_warning               BOOLEAN;
      l_bool_no_manager_warning        BOOLEAN;
      l_bool_addl_rights_warning       BOOLEAN;
      l_num_record_no                  NUMBER;
      l_var_employee_number            VARCHAR2 (100);
      l_var_process                    VARCHAR2 (100);
      l_num_successful_records         NUMBER := 0;
      l_num_failed_records             NUMBER := 0;
      l_var_error                      VARCHAR2 (4000);
      l_errm                           VARCHAR2 (3000) := NULL;
      l_cnt_empno                      NUMBER := 0;
      l_empno                          VARCHAR2 (300) := NULL;
      l_national_code                  VARCHAR2 (300) := NULL;
      l_mar_status_code                VARCHAR2 (300) := NULL;
      l_reg_code                       VARCHAR2 (300) := NULL;
      l_emp_arb_name                   VARCHAR2 (3000) := NULL;
      l_title_code                     VARCHAR2 (3000) := NULL;
      l_last_name                      VARCHAR2 (3000) := NULL;
      l_sex                            VARCHAR2 (3000) := NULL;
      l_err_message                    VARCHAR2 (3000) := NULL;
      lc_employee_number               per_all_people_f.employee_number%TYPE;
      ln_person_id                     per_all_people_f.person_id%TYPE;
      ln_assignment_id                 per_all_assignments_f.assignment_id%TYPE;
      ln_object_ver_number             per_all_assignments_f.object_version_number%TYPE;
      ln_asg_ovn                       NUMBER;
      ld_per_effective_start_date      per_all_people_f.effective_start_date%TYPE;
      ld_per_effective_end_date        per_all_people_f.effective_end_date%TYPE;
      lc_full_name                     per_all_people_f.full_name%TYPE;
      ln_per_comment_id                per_all_people_f.comment_id%TYPE;
      ln_assignment_sequence           per_all_assignments_f.assignment_sequence%TYPE;
      lc_assignment_number             per_all_assignments_f.assignment_number%TYPE;
      lb_name_combination_warning      BOOLEAN;
      lb_assign_payroll_warning        BOOLEAN;
      lb_orig_hire_warning             BOOLEAN;
      -- Local Variables
      -- -----------------------
      ln_object_version_number per_all_people_f.object_version_number%TYPE
            := 1 ;
      lc_dt_ud_mode                    VARCHAR2 (100) := NULL;
      --ln_assignment_id PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_ID%TYPE := 33564;
      --lc_employee_number PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE := 'PRAJ_01';

      -- Out Variables for Find Date Track Mode API
      -- ----------------------------------------------------------------
      lb_correction                    BOOLEAN;
      lb_update                        BOOLEAN;
      lb_update_override               BOOLEAN;
      lb_update_change_insert          BOOLEAN;
      -- Out Variables for Update Employee API
      -- -----------------------------------------------------------
      ld_effective_start_date          DATE;
      ld_effective_end_date            DATE;
      --lc_full_name PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
      ln_comment_id                    per_all_people_f.comment_id%TYPE;
      --lb_name_combination_warning BOOLEAN;
      --lb_assign_payroll_warning BOOLEAN;
      l_validate                       BOOLEAN := FALSE;
      l_period_of_service_id           NUMBER;
      l_object_version_number          NUMBER;
      l_actual_notice_period_date      DATE;
      l_effective_date                 DATE;
      l_supervisor_warning             BOOLEAN;
      l_event_warning                  BOOLEAN;
      l_interview_warning              BOOLEAN;
      l_review_warning                 BOOLEAN;
      l_recruiter_warning              BOOLEAN;
      l_asg_future_changes_warning     BOOLEAN;
      l_f_asg_future_changes_warning   BOOLEAN;
      l_pay_proposal_warning           BOOLEAN;
      l_dod_warning                    BOOLEAN;
      l_final_process_date             DATE;
      l_org_now_no_manager_warning     BOOLEAN;
      l_entries_changed_warning        VARCHAR2 (255);
      l_f_entries_changed_warning      VARCHAR2 (255);
      l_alu_change_warning             VARCHAR2 (255);
      --l_person_type_id NUMBER;
      l_last_std_process_date_out      DATE;
      l_person_type_id                 NUMBER := 0;
      l_val_errm                       VARCHAR2 (300) := NULL;
      l_val_procc_flag                 VARCHAR2 (300) := NULL;
      v_person_id                      NUMBER;
      v_per_object_version_number      NUMBER;
      v_per_effective_start_date       DATE;
      v_per_effective_end_date         DATE;
      v_pdp_object_version_number      NUMBER;
      v_full_name                      VARCHAR2 (240);
      v_comment_id                     NUMBER;
      v_assignment_id                  NUMBER;
      v_asg_object_version_number      NUMBER;
      v_assignment_sequence            NUMBER;
      v_assignment_number              VARCHAR2 (240);
      v_name_combination_warning       BOOLEAN;
      v_npw_number                     VARCHAR2 (240);
      lv_errcode                       VARCHAR2 (300) := NULL;
      lv_errmess                       VARCHAR2 (300) := NULL;
      l_per_id                         NUMBER := 0;
      v_emp_number                     VARCHAR2 (300) := NULL;
      
       l_user_name fnd_user.user_name%type;
      
   BEGIN
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      BEGIN
         rghr_employee_val_proc (lv_errcode, lv_errmess);
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_errcode := '1';
      END;

      FOR i_emp_rec IN emp_rec_cur
      LOOP
         l_per_id := NULL;
         rg_log ('Term loop');

         /*Check Employees count*/
         IF i_emp_rec.person_type IN ('Employee', 'Contractor') THEN
            BEGIN
               SELECT   person_id
                 INTO   l_per_id
                 FROM   per_all_people_f
                WHERE   employee_number = TRIM (i_emp_rec.employee_number)
                        AND i_emp_rec.effective_start_date BETWEEN effective_start_date AND  effective_end_date
                        AND business_group_id = i_emp_rec.business_group_id;

               UPDATE   rgper_emp_details_stg
                  SET   person_id = l_per_id
                WHERE   employee_number = i_emp_rec.employee_number;

               COMMIT;
               rg_log(   'l_cnt_empno '|| l_cnt_empno|| ' '|| i_emp_rec.employee_number);
               
            EXCEPTION
               WHEN OTHERS THEN
                  l_cnt_empno := 0;

                  UPDATE   rgper_emp_details_stg
                     SET   rec_message = i_emp_rec.person_type || ' record not found',
                              rec_status = 'E'
                   WHERE   employee_number = i_emp_rec.employee_number;
            END;
            
         ELSE
         
            BEGIN
               SELECT   papf.person_id
                 INTO   l_per_id
                 FROM per_all_people_f papf, per_periods_of_placement_v pps
                WHERE papf.person_id = pps.person_id
                        AND papf.business_group_id =
                                 i_emp_rec.business_group_id
                        AND papf.employee_number = i_emp_rec.employee_number
                        AND papf.current_npw_flag = 'Y'
                        AND i_emp_rec.effective_start_date BETWEEN papf.effective_start_date AND  papf.effective_end_date
                        AND i_emp_rec.effective_end_date BETWEEN pps.date_start
                                                             AND  COALESCE (
                                                                     pps.projected_termination_date,
                                                                     i_emp_rec.effective_end_date,
                                                                     i_emp_rec.effective_end_date
                                                                  );
            EXCEPTION
               WHEN OTHERS THEN
                  l_cnt_empno := 0;

                  UPDATE rgper_emp_details_stg
                     SET   rec_message = i_emp_rec.person_type || ' record not found',
                           rec_status = 'E'
                   WHERE employee_number = i_emp_rec.employee_number;
            END;
            
         END IF;

         COMMIT;

         /*Checking employee Process flag E or not*/
         IF l_per_id IS NOT NULL AND i_emp_rec.person_type IN ('Employee', 'Contractor') THEN
         
            BEGIN
               rg_log ('API ' || i_emp_rec.employee_number);

               BEGIN
                  SELECT   ppos.period_of_service_id,
                                 ppos.object_version_number
                    INTO   l_period_of_service_id, 
                                l_object_version_number
                    FROM   per_periods_of_service ppos
                   WHERE   1 = 1 AND ppos.person_id = l_per_id
                           AND i_emp_rec.effective_end_date BETWEEN ppos.date_start
                                                                AND  COALESCE (
                                                                        ppos.projected_termination_date,
                                                                        i_emp_rec.effective_end_date,
                                                                        i_emp_rec.effective_end_date
                                                                     );

                  rg_log(   l_period_of_service_id|| ' -------- '|| l_object_version_number);
                  
               EXCEPTION
               
                  WHEN OTHERS THEN
                     l_period_of_service_id := 0;
                     l_object_version_number := 0;

                     UPDATE   rgper_emp_details_stg
                        SET   rec_message = 'Employee record not found',
                                 rec_status = 'E'
                      WHERE   employee_number = i_emp_rec.employee_number;
               END;

               rg_log ('actual_termination_emp');

               BEGIN
                  hr_ex_employee_api.actual_termination_emp (
                     --p_validate => l_validate,
                     p_effective_date               => i_emp_rec.effective_end_date,
                     p_period_of_service_id         => l_period_of_service_id,
                     p_object_version_number        => l_object_version_number,
                     p_actual_termination_date      => i_emp_rec.effective_end_date,
                     p_last_standard_process_date   => i_emp_rec.effective_end_date,
                     --p_person_type_id => l_person_type_id,
                     --,p_leaving_reason => 'RESS'
                     p_last_std_process_date_out    => l_last_std_process_date_out,
                     p_supervisor_warning           => l_supervisor_warning,
                     p_event_warning                => l_event_warning,
                     p_interview_warning            => l_interview_warning,
                     p_review_warning               => l_review_warning,
                     p_recruiter_warning            => l_recruiter_warning,
                     p_asg_future_changes_warning   => l_asg_future_changes_warning,
                     p_entries_changed_warning      => l_entries_changed_warning,
                     p_pay_proposal_warning         => l_pay_proposal_warning,
                     p_dod_warning                  => l_dod_warning,
                     p_alu_change_warning           => l_alu_change_warning
                  );
                   
                    -- Enhancement#125
                    -- -------------------------
                    -- start
                    begin

                        select user_name
                          into l_user_name
                        from fnd_user
                             where employee_id = l_per_id;
                             
                     exception
                     
                       when others then
                        l_user_name := 'X';
                        
                     end; 
                     
                        if nvl(l_user_name, 'X') != 'X' then
                       
                          disable_user(l_user_name);
                          
                        end if;
                        
                      -- end


                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'P',
                           rec_message = 'Employee Record Terminated'
                   WHERE   ROWID = i_emp_rec.ROWID;
                   
                   
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_errm := SUBSTR (SQLERRM, 1, 299);

                     UPDATE   rgper_emp_details_stg
                        SET   rec_status = 'E', rec_message = l_errm
                      WHERE   ROWID = i_emp_rec.ROWID;
               END;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ROLLBACK;
                  l_errm := SUBSTR (SQLERRM, 1, 299);

                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'E',
                           rec_message =
                              SUBSTR (l_errm || 'Emp Term Proc', 1, 299),
                           person_id = ln_person_id,
                           assignment_id = ln_assignment_id
                   WHERE   ROWID = i_emp_rec.ROWID;

                  rg_log (SUBSTR (SQLERRM, 1, 299));
                  COMMIT;
            END;
         END IF;

         IF l_per_id IS NOT NULL
            AND i_emp_rec.person_type = 'Contingent Worker'
         THEN
            BEGIN
               SELECT   pps.object_version_number,
                        papf.person_id,
                        pps.date_start
                 INTO   l_num_ovn, l_num_person_id, l_dat_date_start
                 FROM   per_all_people_f papf, per_periods_of_placement_v pps
                WHERE   papf.person_id = pps.person_id
                        AND papf.business_group_id =
                              i_emp_rec.business_group_id
                        AND papf.employee_number = i_emp_rec.employee_number
                        AND papf.current_npw_flag = 'Y'
                        AND i_emp_rec.effective_start_date BETWEEN papf.effective_start_date
                                                               AND  papf.effective_end_date
                        AND i_emp_rec.effective_end_date BETWEEN pps.date_start
                                                             AND  COALESCE (
                                                                     pps.projected_termination_date,
                                                                     i_emp_rec.effective_end_date,
                                                                     i_emp_rec.effective_end_date
                                                                  );

               l_var_process := 'Y';
            EXCEPTION
               WHEN OTHERS
               THEN
                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'E',
                           rec_message = ' '|| 'unable to fetch service period id from ex CWK'
                   WHERE   ROWID = i_emp_rec.ROWID;

                  COMMIT;
                  l_var_process := 'N';
            END;

            IF NVL (l_var_process, 'Y') != 'N'
            THEN
               BEGIN
                  hr_contingent_worker_api.terminate_placement (
                     p_effective_date               => i_emp_rec.effective_end_date,
                     p_person_id                    => l_num_person_id,
                     p_date_start                   => l_dat_date_start,
                     p_object_version_number        => l_num_ovn,
                     p_actual_termination_date      => i_emp_rec.effective_end_date,
                     p_final_process_date           => i_emp_rec.effective_end_date-- final process date is 3 months after the actual termination date
                     ,
                     p_last_standard_process_date   => i_emp_rec.effective_end_date,
                     --p_termination_reason => 'Deceased',
                     p_supervisor_warning           => l_bool_supervisor_warning,
                     p_event_warning                => l_bool_event_warning,
                     p_interview_warning            => l_bool_interview_warning,
                     p_review_warning               => l_bool_review_warning,
                     p_recruiter_warning            => l_bool_recruiter_warning,
                     p_asg_future_changes_warning   => l_bool_asg_future_warning,
                     p_entries_changed_warning      => l_var_entries_changed_warning,
                     p_pay_proposal_warning         => l_bool_pay_proposal_warning,
                     p_dod_warning                  => l_bool_dod_warning,
                     p_org_now_no_manager_warning   => l_bool_no_manager_warning,
                     p_addl_rights_warning          => l_bool_addl_rights_warning
                  );
                  l_num_successful_records := l_num_successful_records + 1;
                  
                    -- Enhancement#125
                    -- -------------------------
                    -- Start
                    begin

                        select user_name
                          into l_user_name
                        from fnd_user
                             where employee_id = l_per_id;
                             
                     exception
                     
                       when others then
                        l_user_name := 'X';
                        
                     end; 
                     
                        if nvl(l_user_name, 'X') != 'X' then
                       
                          disable_user(l_user_name);
                          
                        end if;
                        -- End

                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'P',
                           rec_message =
                              i_emp_rec.employee_number
                              || ' Contingent Worker Record terminated'
                   WHERE   ROWID = i_emp_rec.ROWID;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_var_error := SUBSTR (SQLERRM, 1, 299);

                     UPDATE   rgper_emp_details_stg
                        SET   rec_status = 'E',
                              rec_message =
                                 'Unable to terminate Employee Record'
                                 || SUBSTR (l_var_error, 1, 100)
                      WHERE   ROWID = i_emp_rec.ROWID;

                     COMMIT;
                     l_var_process := 'N';
               END;
            END IF;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errmess := 'error';
         p_errcode := '1';
         l_errm := SUBSTR (SQLERRM, 1, 299);
         rg_log (SUBSTR (SQLERRM, 1, 299));
   END rghr_emp_term_proc;

   PROCEDURE rghr_employee_creation_proc (p_errcode   OUT VARCHAR2,
                                          p_errmess   OUT VARCHAR2)
   AS
      CURSOR c1
      IS
         SELECT   ed.ROWID,
                  (SELECT   assignment_id
                     FROM   per_all_assignments_f
                    WHERE   person_id = ed.person_id
                            AND ed.effective_start_date BETWEEN effective_start_date
                                                            AND  effective_end_date)
                     d_assignment_id,
                  (SELECT   business_group_id
                     FROM   per_business_groups
                    WHERE   NAME = ed.business_group_name)
                     buss_group_id,
                  ed.*
           FROM   rgper_emp_details_stg ed
          WHERE   1 = 1 AND rec_status = 'V' AND effective_end_date IS NULL--and employee_number = '0001485'
   ;

      l_errm                           VARCHAR2 (3000) := NULL;
      l_cnt_empno                      NUMBER := 0;
      l_empno                          VARCHAR2 (300) := NULL;
      l_national_code                  VARCHAR2 (300) := NULL;
      l_mar_status_code                VARCHAR2 (300) := NULL;
      l_reg_code                       VARCHAR2 (300) := NULL;
      l_emp_arb_name                   VARCHAR2 (3000) := NULL;
      l_title_code                     VARCHAR2 (3000) := NULL;
      l_last_name                      VARCHAR2 (3000) := NULL;
      l_sex                            VARCHAR2 (3000) := NULL;
      l_err_message                    VARCHAR2 (3000) := NULL;
      lc_employee_number               per_all_people_f.employee_number%TYPE;
      ln_person_id                     per_all_people_f.person_id%TYPE;
      ln_assignment_id                 per_all_assignments_f.assignment_id%TYPE;
      ln_object_ver_number             per_all_assignments_f.object_version_number%TYPE;
      ln_asg_ovn                       NUMBER;
      ld_per_effective_start_date      per_all_people_f.effective_start_date%TYPE;
      ld_per_effective_end_date        per_all_people_f.effective_end_date%TYPE;
      lc_full_name                     per_all_people_f.full_name%TYPE;
      ln_per_comment_id                per_all_people_f.comment_id%TYPE;
      ln_assignment_sequence           per_all_assignments_f.assignment_sequence%TYPE;
      lc_assignment_number             per_all_assignments_f.assignment_number%TYPE;
      lb_name_combination_warning      BOOLEAN;
      lb_assign_payroll_warning        BOOLEAN;
      lb_orig_hire_warning             BOOLEAN;
      -- Local Variables
      -- -----------------------
      ln_object_version_number per_all_people_f.object_version_number%TYPE
            := 1 ;
      lc_dt_ud_mode                    VARCHAR2 (100) := NULL;
      --ln_assignment_id PER_ALL_ASSIGNMENTS_F.ASSIGNMENT_ID%TYPE := 33564;
      --lc_employee_number PER_ALL_PEOPLE_F.EMPLOYEE_NUMBER%TYPE := 'PRAJ_01';

      -- Out Variables for Find Date Track Mode API
      -- ----------------------------------------------------------------
      lb_correction                    BOOLEAN;
      lb_update                        BOOLEAN;
      lb_update_override               BOOLEAN;
      lb_update_change_insert          BOOLEAN;
      -- Out Variables for Update Employee API
      -- -----------------------------------------------------------
      ld_effective_start_date          DATE;
      ld_effective_end_date            DATE;
      --lc_full_name PER_ALL_PEOPLE_F.FULL_NAME%TYPE;
      ln_comment_id                    per_all_people_f.comment_id%TYPE;
      --lb_name_combination_warning BOOLEAN;
      --lb_assign_payroll_warning BOOLEAN;
      l_validate                       BOOLEAN := FALSE;
      l_period_of_service_id           NUMBER;
      l_object_version_number          NUMBER;
      l_actual_notice_period_date      DATE;
      l_effective_date                 DATE;
      l_supervisor_warning             BOOLEAN;
      l_event_warning                  BOOLEAN;
      l_interview_warning              BOOLEAN;
      l_review_warning                 BOOLEAN;
      l_recruiter_warning              BOOLEAN;
      l_asg_future_changes_warning     BOOLEAN;
      l_f_asg_future_changes_warning   BOOLEAN;
      l_pay_proposal_warning           BOOLEAN;
      l_dod_warning                    BOOLEAN;
      l_final_process_date             DATE;
      l_org_now_no_manager_warning     BOOLEAN;
      l_entries_changed_warning        VARCHAR2 (255);
      l_f_entries_changed_warning      VARCHAR2 (255);
      l_alu_change_warning             VARCHAR2 (255);
      --l_person_type_id NUMBER;
      l_last_std_process_date_out      DATE;
      l_person_type_id                 NUMBER := 0;
      l_val_errm                       VARCHAR2 (300) := NULL;
      l_val_procc_flag                 VARCHAR2 (300) := NULL;
      v_person_id                      NUMBER;
      v_per_object_version_number      NUMBER;
      v_per_effective_start_date       DATE;
      v_per_effective_end_date         DATE;
      v_pdp_object_version_number      NUMBER;
      v_full_name                      VARCHAR2 (240);
      v_comment_id                     NUMBER;
      v_assignment_id                  NUMBER;
      v_asg_object_version_number      NUMBER;
      v_assignment_sequence            NUMBER;
      v_assignment_number              VARCHAR2 (240);
      v_name_combination_warning       BOOLEAN;
      v_npw_number                     VARCHAR2 (240);
      lv_errcode                       VARCHAR2 (300) := NULL;
      lv_errmess                       VARCHAR2 (300) := NULL;
      l_term_errcode                   VARCHAR2 (300) := NULL;
      l_term_errmess                   VARCHAR2 (300) := NULL;
      l_cnt_cw_empno                   NUMBER := 0;
      l_up_assignment_id               NUMBER := NULL;
      v_emp_number                     VARCHAR2 (300) := NULL;
   BEGIN
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);

      BEGIN
         rghr_employee_val_proc (lv_errcode, lv_errmess);
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_errcode := '1';
      END;

      FOR i_emp_rec IN c1
      LOOP
         ln_person_id := NULL;
         l_cnt_empno := NULL;
         l_cnt_cw_empno := NULL;
         l_sex := NULL;
         l_val_errm := NULL;
         l_up_assignment_id := NULL;

         /*Check Employees count*/
         BEGIN
            SELECT   COUNT (0)
              INTO   l_cnt_empno
              FROM   per_all_people_f
             WHERE   employee_number = TRIM (i_emp_rec.employee_number)
                     AND business_group_id = i_emp_rec.buss_group_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cnt_empno := 0;
         END;

         BEGIN
            SELECT   COUNT (0)
              INTO   l_cnt_cw_empno
              FROM   per_periods_of_placement ppp, per_all_people_f ppf
             WHERE       ppp.person_id = ppf.person_id
                     AND ppf.npw_number = TRIM (i_emp_rec.employee_number)
                     AND ppf.business_group_id = i_emp_rec.buss_group_id;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cnt_cw_empno := 0;
         END;

         /*Employee gender validation */
         IF i_emp_rec.sex IS NOT NULL
         THEN
            BEGIN
               SELECT   DECODE (UPPER (TRIM (i_emp_rec.sex)),
                                'MALE', 'M',
                                'FEMALE', 'F',
                                'M', 'M',
                                'F', 'F',
                                'F')
                 INTO   l_sex
                 FROM   DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_sex := NULL;
            END;
         ELSE
            l_sex := 'F';
         END IF;

         /*Checking employee Process flag E or not*/
         IF     l_cnt_empno = 0
            AND i_emp_rec.person_type IN ('Employee', 'Contractor')
            AND i_emp_rec.person_type_id IS NOT NULL
         THEN
            BEGIN
               rg_log ('API ' || i_emp_rec.employee_number);
               hr_employee_api.create_employee (
                  -- Input data elements --
                  p_hire_date                   => TRUNC (i_emp_rec.effective_start_date),
                  p_business_group_id           => i_emp_rec.buss_group_id,
                  p_last_name                   => i_emp_rec.last_name,
                  p_first_name                  => TRIM (i_emp_rec.first_name),
                  p_middle_names                => TRIM (i_emp_rec.middle_names),
                  --p_known_as => TRIM (i_emp_rec.known_as),
                  p_sex                         => TRIM (l_sex),
                  p_person_type_id              => i_emp_rec.person_type_id,
                  p_email_address               => TRIM (i_emp_rec.email_address),
                  p_original_date_of_hire       => TRUNC(i_emp_rec.effective_start_date),
                  p_internal_location           => i_emp_rec.internal_location,
                  -- Output data elements
                  -- --------------------------------
                  p_employee_number             => i_emp_rec.employee_number,
                  p_person_id                   => ln_person_id,
                  p_assignment_id               => ln_assignment_id,
                  p_per_object_version_number   => ln_object_ver_number,
                  p_asg_object_version_number   => ln_asg_ovn,
                  p_per_effective_start_date    => ld_per_effective_start_date,
                  p_per_effective_end_date      => ld_per_effective_end_date,
                  p_full_name                   => lc_full_name,
                  p_per_comment_id              => ln_per_comment_id,
                  p_assignment_sequence         => ln_assignment_sequence,
                  p_assignment_number           => lc_assignment_number,
                  p_name_combination_warning    => lb_name_combination_warning,
                  p_assign_payroll_warning      => lb_assign_payroll_warning,
                  p_orig_hire_warning           => lb_orig_hire_warning
               );
               rg_log ('Employee Created ');
               COMMIT;

               UPDATE   rgper_emp_details_stg
                  SET   rec_status = 'P',
                        rec_message = 'Created Employee Record',
                        --employee_number = l_empno,
                        person_id = ln_person_id,
                        assignment_id = ln_assignment_id
                WHERE   ROWID = i_emp_rec.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ROLLBACK;
                  l_errm := SUBSTR (SQLERRM, 1, 299);

                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'E',
                           rec_message = l_errm,
                           person_id = ln_person_id,
                           assignment_id = ln_assignment_id
                   WHERE   ROWID = i_emp_rec.ROWID;

                  rg_log (SUBSTR (SQLERRM, 1, 299));
                  COMMIT;
            END;
         END IF;

         IF l_cnt_cw_empno = 0
            AND i_emp_rec.person_type = 'Contingent Worker'
         THEN
            BEGIN
               rg_log(   ' HR_CONTINGENT_WORKER_API API'
                      || i_emp_rec.employee_number
                      || ' '
                      || i_emp_rec.person_type_id);
               hr_contingent_worker_api.create_cwk (
                  --p_validate => FALSE
                  p_start_date                  => TRUNC (i_emp_rec.effective_start_date),
                  p_business_group_id           => i_emp_rec.buss_group_id,
                  p_last_name                   => i_emp_rec.last_name,
                  p_first_name                  => TRIM (i_emp_rec.first_name),
                  p_middle_names                => TRIM (i_emp_rec.middle_names),
                  p_sex                         => TRIM (l_sex),
                  p_email_address               => TRIM (i_emp_rec.email_address),
                  p_internal_location           => i_emp_rec.internal_location,
                  --p_known_as                    => TRIM (i_emp_rec.known_as),
                  p_person_type_id              => i_emp_rec.person_type_id,
                  p_npw_number                  => i_emp_rec.employee_number,
                  p_person_id                   => v_person_id,
                  p_per_object_version_number   => v_per_object_version_number,
                  p_per_effective_start_date    => v_per_effective_start_date,
                  p_per_effective_end_date      => v_per_effective_end_date,
                  p_pdp_object_version_number   => v_pdp_object_version_number,
                  p_full_name                   => v_full_name,
                  p_comment_id                  => v_comment_id,
                  p_assignment_id               => v_assignment_id,
                  p_asg_object_version_number   => v_asg_object_version_number,
                  p_assignment_sequence         => v_assignment_sequence,
                  p_assignment_number           => v_assignment_number,
                  p_name_combination_warning    => v_name_combination_warning
               );
               DBMS_OUTPUT.put_line (i_emp_rec.person_type || ' Created ');
               COMMIT;

               UPDATE   rgper_emp_details_stg
                  SET   rec_status = 'P',
                        rec_message =
                           i_emp_rec.person_type || ' Record Created ',
                        person_id = v_person_id,
                        assignment_id = v_assignment_id,
                        person_type_id = l_person_type_id,
                        business_group_id = i_emp_rec.buss_group_id
                WHERE   ROWID = i_emp_rec.ROWID;

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  ROLLBACK;
                  l_errm := SUBSTR (SQLERRM, 1, 299);

                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'E',
                           rec_message = l_errm,
                           person_id = v_person_id,
                           assignment_id = v_assignment_id
                   WHERE   ROWID = i_emp_rec.ROWID;

                  rg_log (SUBSTR (SQLERRM, 1, 299));
                  COMMIT;
            END;
         END IF;

         IF (l_cnt_empno != 0 OR l_cnt_cw_empno != 0)
            AND i_emp_rec.effective_end_date IS NULL
         THEN
            --l_up_assignment_id := NULL;
            rg_log ('Contingent Worker');

            IF i_emp_rec.person_type = 'Contingent Worker'
            THEN
               rg_log ('Contingent Worker ssss');

               BEGIN
                  SELECT   ppf.object_version_number,
                           ppf.effective_start_date,
                           ppf.effective_end_date,
                           paf.assignment_id
                    INTO   ln_object_version_number,
                           ld_effective_start_date,
                           ld_effective_end_date,
                           l_up_assignment_id
                    FROM   per_periods_of_placement ppp,
                           per_all_people_f ppf,
                           per_all_assignments_f paf
                   WHERE       ppp.person_id = ppf.person_id
                           AND ppf.person_id = i_emp_rec.person_id
                           AND paf.person_id = ppf.person_id
                           AND i_emp_rec.effective_start_date BETWEEN paf.effective_start_date
                                                                  AND  paf.effective_end_date
                           AND i_emp_rec.effective_start_date BETWEEN ppf.effective_start_date
                                                                  AND  ppf.effective_end_date;

                  rg_log('Contingent Worker --> '
                         || TO_CHAR (ld_effective_start_date, 'DD-MON-YYYY'));
                  rg_log('Contingent Worker --> ln_object_version_number '
                         || ln_object_version_number);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_object_version_number := NULL;
                     ld_effective_start_date := NULL;
                     ld_effective_end_date := NULL;
                     l_up_assignment_id := NULL;
                     l_val_errm := SUBSTR (SQLERRM, 1, 100);
               END;
            ELSE
               BEGIN
                  SELECT   ppf.object_version_number,
                           ppf.effective_start_date,
                           ppf.effective_end_date,
                           paf.assignment_id
                    INTO   ln_object_version_number,
                           ld_effective_start_date,
                           ld_effective_end_date,
                           l_up_assignment_id
                    FROM   per_all_people_f ppf, per_all_assignments_f paf
                   WHERE   ppf.person_id = i_emp_rec.person_id
                           AND ppf.person_id = paf.person_id
                           AND i_emp_rec.effective_start_date BETWEEN paf.effective_start_date
                                                                  AND  paf.effective_end_date
                           AND i_emp_rec.effective_start_date BETWEEN ppf.effective_start_date
                                                                  AND  ppf.effective_end_date;

                  --ln_object_version_number := 4;
                  rg_log (TO_CHAR (ld_effective_start_date, 'DD-MON-YYYY'));
                  rg_log (
                     'ln_object_version_number ' || ln_object_version_number
                  );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ln_object_version_number := NULL;
                     ld_effective_start_date := NULL;
                     ld_effective_end_date := NULL;
                     l_up_assignment_id := NULL;
                     rg_log (' error ');
               END;
            END IF;

            IF l_up_assignment_id != NULL
            THEN
               BEGIN
                  dt_api.find_dt_upd_modes (            -- Input Data Elements
                     -- ------------------------------
                     p_effective_date         => TRUNC(NVL (
                                                          i_emp_rec.effective_start_date,
                                                          SYSDATE
                                                       )),
                     p_base_table_name        => 'PER_ALL_ASSIGNMENTS_F',
                     p_base_key_column        => 'ASSIGNMENT_ID',
                     p_base_key_value         => l_up_assignment_id,
                     -- Output data elements
                     -- -------------------------------
                     p_correction             => lb_correction,
                     p_update                 => lb_update,
                     p_update_override        => lb_update_override,
                     p_update_change_insert   => lb_update_change_insert
                  );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_val_errm := SUBSTR (SQLERRM, 1, 100);

                     UPDATE   rgper_emp_details_stg
                        SET   rec_status = 'E',
                              rec_message =
                                 l_up_assignment_id || ' ' || l_val_errm
                      WHERE   ROWID = i_emp_rec.ROWID;
               END;
            END IF;

            IF (lb_update_override = TRUE OR lb_update_change_insert = TRUE)
            THEN
               -- UPDATE_OVERRIDE
               -- ---------------------------------
               lc_dt_ud_mode := 'UPDATE_OVERRIDE';
            END IF;

            IF (lb_correction = TRUE)
            THEN
               -- CORRECTION
               -- ----------------------
               lc_dt_ud_mode := 'CORRECTION';
            END IF;

            IF (lb_update = TRUE)
            THEN
               -- UPDATE
               -- --------------
               lc_dt_ud_mode := 'UPDATE';
            END IF;

            BEGIN
               IF i_emp_rec.person_type != 'Contingent Worker'
               THEN
                  BEGIN
                     hr_person_api.update_person (      -- Input Data Elements
                        -- ------------------------------
                        p_effective_date             => TRUNC(i_emp_rec.effective_start_date),
                        p_datetrack_update_mode      => NVL (lc_dt_ud_mode,
                                                             'CORRECTION'),
                        --p_business_group_id => i_emp_rec.business_group_id,
                        p_last_name                  => i_emp_rec.last_name,
                        p_first_name                 => TRIM (
                                                          i_emp_rec.first_name
                                                       ),
                        p_middle_names               => TRIM (
                                                          i_emp_rec.middle_names
                                                       ),
                        --p_known_as                   => TRIM (i_emp_rec.known_as),
                        p_sex                        => TRIM (l_sex),
                        p_person_type_id             => TRIM(i_emp_rec.person_type_id),
                        p_email_address              => TRIM(i_emp_rec.email_address),
                        --p_original_date_of_hire => i_emp_rec.effective_start_date,
                        p_person_id                  => i_emp_rec.person_id,
                        p_internal_location          => i_emp_rec.internal_location,
                        -- Output Data Elements
                        -- ----------------------------------
                        p_employee_number            => i_emp_rec.employee_number,
                        p_object_version_number      => ln_object_version_number,
                        p_effective_start_date       => ld_effective_start_date,
                        p_effective_end_date         => ld_effective_end_date,
                        p_full_name                  => lc_full_name,
                        p_comment_id                 => ln_comment_id,
                        p_name_combination_warning   => lb_name_combination_warning,
                        p_assign_payroll_warning     => lb_assign_payroll_warning,
                        p_orig_hire_warning          => lb_orig_hire_warning
                     );
                     rg_log ('Employee record updated');

                     UPDATE   rgper_emp_details_stg
                        SET   rec_status = 'P',
                              rec_message = 'Employee record updated'
                      WHERE   ROWID = i_emp_rec.ROWID;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_errm := SUBSTR (SQLERRM, 1, 299);

                        UPDATE   rgper_emp_details_stg
                           SET   rec_status = 'E',
                                 rec_message =
                                       'hr_person_api.update_person -- '
                                    || lc_dt_ud_mode
                                    || '--'
                                    || l_errm
                         WHERE   ROWID = i_emp_rec.ROWID;
                  END;
               ELSE
                  BEGIN
                     hr_person_api.update_person (
                        p_effective_date             => TRUNC(i_emp_rec.effective_start_date),
                        p_datetrack_update_mode      => lc_dt_ud_mode,
                        --p_business_group_id => i_emp_rec.business_group_id,
                        p_last_name                  => i_emp_rec.last_name,
                        p_first_name                 => TRIM (
                                                          i_emp_rec.first_name
                                                       ),
                        p_middle_names               => TRIM (
                                                          i_emp_rec.middle_names
                                                       ),
                        --p_known_as                   => TRIM (i_emp_rec.known_as),
                        p_sex                        => TRIM (l_sex),
                        p_person_type_id             => TRIM(i_emp_rec.person_type_id),
                        p_email_address              => TRIM(i_emp_rec.email_address),
                        --p_original_date_of_hire => i_emp_rec.effective_start_date,
                        p_person_id                  => i_emp_rec.person_id,
                        p_internal_location          => i_emp_rec.internal_location,
                        p_employee_number            => v_emp_number,
                        p_npw_number                 => i_emp_rec.employee_number,
                        p_object_version_number      => ln_object_version_number,
                        p_effective_start_date       => ld_effective_start_date,
                        p_effective_end_date         => ld_effective_end_date,
                        p_full_name                  => lc_full_name,
                        p_comment_id                 => ln_comment_id,
                        p_name_combination_warning   => lb_name_combination_warning,
                        p_assign_payroll_warning     => lb_assign_payroll_warning,
                        p_orig_hire_warning          => lb_orig_hire_warning
                     );

                     BEGIN
                        UPDATE   rgper_emp_details_stg
                           SET   rec_status = 'P',
                                 rec_message = 'Employee Record Updated',
                                 employee_number = i_emp_rec.employee_number,
                                 person_id = ln_person_id,
                                 assignment_id = ln_assignment_id,
                                 person_type_id = i_emp_rec.person_type_id,
                                 business_group_id =
                                    i_emp_rec.business_group_id
                         WHERE   ROWID = i_emp_rec.ROWID;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           -- rec_status:= 'E';
                           l_errm := SUBSTR (SQLERRM, 1, 299);
                     END;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_errm := SUBSTR (SQLERRM, 1, 299);

                        UPDATE   rgper_emp_details_stg
                           SET   rec_status = 'E',
                                 rec_message =
                                       'hr_person_api.update_person'
                                    || ' '
                                    || l_errm
                         WHERE   ROWID = i_emp_rec.ROWID;
                  END;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_errm := SUBSTR (SQLERRM, 1, 100);

                  UPDATE   rgper_emp_details_stg
                     SET   rec_status = 'E', rec_message = l_errm
                   WHERE   ROWID = i_emp_rec.ROWID;

                  COMMIT;
            END;

            COMMIT;
         END IF;
      END LOOP;

      BEGIN
         rghr_emp_term_proc (l_term_errcode, l_term_errmess);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_term_errcode := '1';
            l_term_errmess := SUBSTR (SQLERRM, 1, 299);
      END;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errmess := 'error';
         p_errcode := '1';
         l_errm := SUBSTR (SQLERRM, 1, 299);
         rg_log (SUBSTR (SQLERRM, 1, 299));
   END rghr_employee_creation_proc;
   
   

   PROCEDURE rghr_assig_val_proc (p_errcode   OUT VARCHAR2,
                                  p_errmess   OUT VARCHAR2)
   AS
      CURSOR c_ass
      IS
           SELECT   xsid.ROWID, xsid.*
             FROM   rgper_emp_assignment_stg xsid
            WHERE   rec_status = 'N'
         --AND EMPLOYEE_NUMBER = '0002883'
         ORDER BY   xsid.effective_start_date;

      v_enabled_flag            VARCHAR2 (2) := 'Y';
      v_summary_flag            VARCHAR2 (2) := 'Y';
      v_start_date_active       DATE := TO_DATE ('01-Jan-1951', 'DD-Mon-YYYY');
      v_error_msg               VARCHAR2 (1000) := NULL;
      v_who_type                fnd_flex_loader_apis.who_type;
      v_request_id              NUMBER;
      v_rec_success             NUMBER;
      v_rec_error               NUMBER;
      v_rec_cnt                 NUMBER := 0;
      v_user_id                 NUMBER := fnd_global.user_id;
      v_login_id                NUMBER := fnd_global.login_id;
      v_req_id                  NUMBER := fnd_global.conc_request_id;
      l_person_id               NUMBER := 0;
      l_ass_id                  NUMBER := 0;
      l_bg_id                   NUMBER := 0;
      l_job_id                  NUMBER := 0;
      l_job_group_id            NUMBER := 0;
      l_object_version_number   NUMBER := NULL;
      l_err_msg                 VARCHAR2 (300) := NULL;
      l_err_flag                VARCHAR2 (300) := NULL;
      l_name                    VARCHAR2 (500) := NULL;
      l_job_definition_id       NUMBER := NULL;
      l_sup_id                  NUMBER := NULL;
      l_person_name             VARCHAR2 (300) := NULL;
      l_def_cc_id               NUMBER := NULL;
   BEGIN
      /*Employee Assignment staging table loop*/
      FOR i_ass_rec IN c_ass
      LOOP
         l_err_msg := NULL;
         l_err_flag := NULL;
         l_bg_id := NULL;
         l_person_id := NULL;
         l_ass_id := NULL;
         l_sup_id := NULL;
         l_job_id := NULL;
         l_job_group_id := NULL;
         l_def_cc_id := NULL;

         /*Employee Assignment Business group Validation*/
         BEGIN
            SELECT   business_group_id
              INTO   l_bg_id
              FROM   per_business_groups
             WHERE   NAME = i_ass_rec.business_group_name;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_msg := l_err_msg || ' - Business Group Name is Invalid';
               l_err_flag := 'E';
               l_bg_id := -999999;
         END;

         /*Check the employee business group is valid or not */
         IF l_bg_id != -999999
         THEN
            /* Getting Employee Person id and assignment id */
            BEGIN
               /*
               SELECT ppf.person_id, paf.assignment_id
               INTO l_person_id, l_ass_id
               FROM per_all_people_f ppf, per_all_assignments_f paf
               WHERE ppf.person_id = paf.person_id
               AND i_ass_rec.effective_start_date
               BETWEEN ppf.effective_start_date
               AND ppf.effective_end_date
               AND i_ass_rec.effective_start_date
               BETWEEN paf.effective_start_date
               AND paf.effective_end_date
               AND ppf.employee_number = i_ass_rec.employee_number
               AND ppf.business_group_id = l_bg_id;


               l_person_id := NULL;
               l_ass_id := NULL;

               */
               SELECT                  --count(0),ppf.person_id,ppf.npw_number
                     ppf.person_id,
                        paf.assignment_id,
                        paf.default_code_comb_id
                 INTO   l_person_id, l_ass_id, l_def_cc_id
                 FROM   per_periods_of_placement ppp,
                        per_all_people_f ppf,
                        per_all_assignments_f paf
                WHERE   ppp.person_id(+) = ppf.person_id
                        AND NVL (ppf.employee_number, ppf.npw_number) =
                              i_ass_rec.employee_number
                        --AND PPF.business_group_id = i_emp_rec.buss_group_id
                        AND i_ass_rec.effective_start_date BETWEEN ppf.effective_start_date
                                                               AND  ppf.effective_end_date
                        AND i_ass_rec.effective_start_date BETWEEN paf.effective_start_date
                                                               AND  paf.effective_end_date
                        AND ppf.person_id = paf.person_id
                        /*AND PPF.PERSON_ID IN (SELECT PPTU.PERSON_ID
                        FROM PER_PERSON_TYPE_USAGES_F PPTU, PER_PERSON_TYPES PPT
                        WHERE PPTU.PERSON_TYPE_ID = PPT.PERSON_TYPE_ID
                        AND USER_PERSON_TYPE = 'Contingent Worker'
                        AND PPT.BUSINESS_GROUP_ID = PPF.BUSINESS_GROUP_ID )*/
                        AND ppf.business_group_id = l_bg_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_person_id := 0;
                  l_ass_id := 0;
                  l_err_msg := l_err_msg || ' - Employee Details not found';
                  l_err_flag := 'E';
            END;

            /* Getting Employee Supervisor Person id */
            IF i_ass_rec.supervisor_number IS NOT NULL
            THEN
               BEGIN
                  SELECT   ppf.person_id
                    INTO   l_sup_id
                    FROM   per_all_people_f ppf
                   WHERE   1 = 1
                           AND i_ass_rec.effective_start_date BETWEEN ppf.effective_start_date
                                                                  AND  ppf.effective_end_date
                           AND ppf.employee_number =
                                 i_ass_rec.supervisor_number
                           AND ppf.business_group_id = l_bg_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_sup_id := 0;
                     l_err_msg :=
                        l_err_msg
                        || ' - Supervisor start date is after employee start date';
                     l_err_flag := 'E';
               END;
            ELSE
               l_sup_id := NULL;
            END IF;


            /*Default expense account*/





            /* Getting Job details */
            IF i_ass_rec.job IS NOT NULL
            THEN
               BEGIN
                  SELECT   job_id
                    INTO   l_job_id
                    FROM   per_jobs
                   WHERE   UPPER (NAME) = UPPER (i_ass_rec.job);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_job_id := -1;
               --l_err_msg := 'JOB ERROR ';
               END;

               /* Getting Job Group Id for the employee business group*/
               IF l_job_id = -1
               THEN
                  l_job_group_id := NULL;

                  BEGIN
                     SELECT   job_group_id
                       INTO   l_job_group_id
                       FROM   per_job_groups_v
                      WHERE   business_group_id = l_bg_id;

                     rg_log ('l_job_group_id --> ' || l_job_group_id);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_job_group_id := 0;
                        l_err_msg :=
                           l_err_msg
                           || ' - Valid Job Group Name is not found';
                        l_err_flag := 'E';
                        rg_log ('l_err_msg --> ' || l_err_msg);
                  END;

                  IF l_job_group_id != 0                  --AND l_job_id != -1
                  THEN
                     l_job_id := NULL;
                     l_object_version_number := NULL;
                     l_job_definition_id := NULL;
                     l_name := NULL;

                     /* Creating Jobs process*/
                     BEGIN
                        rg_log ('i_ass_rec.job --> ' || i_ass_rec.job);
                        hr_job_api.create_job (
                           --p_validate => FALSE,
                           p_business_group_id          => l_bg_id,
                           p_date_from                  => TO_DATE ('01-JAN-1951',
                                                                    'DD-MON-YYYY'),
                           p_job_group_id               => l_job_group_id,
                           p_segment1                   => i_ass_rec.job,
                           p_job_information_category   => 'US',
                           p_job_id                     => l_job_id,
                           p_object_version_number      => l_object_version_number,
                           p_job_definition_id          => l_job_definition_id,
                           p_name                       => l_name
                        );
                        COMMIT;
                        rg_log ('l_job_id --> ' || l_job_id);

                        UPDATE   rgper_emp_assignment_stg
                           SET   business_group_id = l_bg_id,
                                 attribute1 = TO_CHAR (l_job_id),
                                 person_id = l_person_id,
                                 assignment_id = l_ass_id,
                                 DEFAULT_CODE_COMB_ID = l_def_cc_id,
                                 supervisor_id = l_sup_id,
                                 rec_status = 'V'
                         WHERE   ROWID = i_ass_rec.ROWID;

                        COMMIT;
                     EXCEPTION
                        WHEN OTHERS
                        THEN
                           l_err_msg :=
                                 l_err_msg
                              || ' '
                              || i_ass_rec.job
                              || ' - Job Is not created --'
                              || SQLERRM;
                           l_err_flag := 'E';

                           UPDATE   rgper_emp_assignment_stg
                              SET   attribute1 = '- Job Is not created --',
                                    rec_status = 'E'
                            WHERE   ROWID = i_ass_rec.ROWID;

                           rg_log ('l_err_msg --> ' || l_err_msg);
                     END;
                  END IF;
               END IF;
            END IF;

            /* Updating the staging table based on the Process */
            IF l_err_flag = 'E'
            THEN
               UPDATE   rgper_emp_assignment_stg
                  SET   business_group_id = l_bg_id,
                        --attribute1 = TO_CHAR (l_job_id),
                        person_id = l_person_id,
                        assignment_id = l_ass_id,
                        DEFAULT_CODE_COMB_ID = l_def_cc_id,
                        supervisor_id = l_sup_id,
                        rec_status = l_err_flag,
                        rec_message = l_err_msg
                WHERE   ROWID = i_ass_rec.ROWID AND rec_status = 'N';

               COMMIT;
            ELSE
               UPDATE   rgper_emp_assignment_stg
                  SET   business_group_id = l_bg_id,
                        --attribute1 = TO_CHAR (l_job_id),
                        person_id = l_person_id,
                        assignment_id = l_ass_id,
                        supervisor_id = l_sup_id,
                        DEFAULT_CODE_COMB_ID = l_def_cc_id,
                        rec_status = 'V'
                WHERE   ROWID = i_ass_rec.ROWID AND rec_status = 'N';

               COMMIT;
            END IF;
         END IF;
      END LOOP;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errcode := '1';
         p_errmess := SUBSTR (SQLERRM, 1, 299);
   END rghr_assig_val_proc;

   PROCEDURE rghr_assignment_creation_proc (p_errcode   OUT VARCHAR2,
                                            p_errmess   OUT VARCHAR2)
   AS
      CURSOR c_ass_cur
      IS
         SELECT   xsid.ROWID,
                  xsid.*,
                  (SELECT   organization_id
                     FROM   hr_organization_units
                    WHERE   organization_id =
                               NVL (xsid.organization_id,
                                    xsid.business_group_id)
                            AND business_group_id = xsid.business_group_id)
                     org_id,
                  (SELECT   job_id
                     FROM   per_jobs_vl
                    WHERE   UPPER (NAME) = UPPER (xsid.job)
                            AND business_group_id = xsid.business_group_id
                            AND xsid.effective_start_date BETWEEN date_from
                                                              AND  NVL (
                                                                      date_to,
                                                                      xsid.effective_start_date
                                                                   ))
                     job_id,
                  nvl((SELECT   set_of_books_id
                     FROM   gl_sets_of_books
                    WHERE   NAME = xsid.ledger_name),(select max(set_of_books_id) from
		                      per_all_people_f ppf,per_all_assignments_f paa
		                      where 1=1 and ppf.person_id = xsid.person_id
		                      and ppf.person_id = paa.person_id AND set_of_books_id IS NOT NULL
		                      and xsid.effective_start_date between ppf.effective_start_date and ppf.effective_end_date
                    		      and xsid.effective_start_date between paa.effective_start_date and paa.effective_end_date
                    and ppf.employee_number = xsid.employee_number))
                     set_of_books_id,
		                    nvl((SELECT   code_combination_id
		                       FROM   gl_code_combinations_kfv
		                      WHERE   code_combination_id = xsid.default_code_comb_id),(select max(default_code_comb_id) from
		                      per_all_people_f ppf,per_all_assignments_f paa
		                      where 1=1 and ppf.person_id = xsid.person_id
		                      and ppf.person_id = paa.person_id AND default_code_comb_id IS NOT NULL
		                      and xsid.effective_start_date between ppf.effective_start_date and ppf.effective_end_date
                    		      and xsid.effective_start_date between paa.effective_start_date and paa.effective_end_date
                    and ppf.employee_number = xsid.employee_number))
                     code_combination_id,
                  NULL location_id,
                  NULL payroll_id,
                  hr_api.g_number people_group_id,
                  NULL grade_id,
                  (SELECT   MAX (ppt.user_person_type)
                     FROM   per_person_type_usages_f pptu,
                            per_person_types ppt
                    WHERE   pptu.person_type_id = ppt.person_type_id
                            AND pptu.person_id = xsid.person_id
                            AND ppt.business_group_id =
                                  (SELECT   business_group_id
                                     FROM   per_business_groups
                                    WHERE   name = xsid.BUSINESS_GROUP_NAME)--xsid.business_group_id
                                                                            --and PPT.USER_PERSON_TYPE = 'Contingent Worker'
                  )
                     p_person_type
           FROM   rgper_emp_assignment_stg xsid
          WHERE   rec_status = 'V'    --WHERE xsid.employee_number = '0000235'
                                  ;

      -- Local Variables
      -- -----------------------
      lc_dt_ud_mode                   VARCHAR2 (100) := NULL;
      ln_assignment_id                NUMBER := 0;
      ln_supervisor_id                NUMBER := 2;
      ln_object_number                NUMBER := 0;
      ln_people_group_id              NUMBER := 0;
      -- Out Variables for Find Date Track Mode API
      -- -----------------------------------------------------------------
      lb_correction                   BOOLEAN;
      lb_update                       BOOLEAN;
      lb_update_override              BOOLEAN;
      lb_update_change_insert         BOOLEAN;
      -- Out Variables for Update Employee Assignment API
      -- ----------------------------------------------------------------------------
      ln_soft_coding_keyflex_id       hr_soft_coding_keyflex.soft_coding_keyflex_id%TYPE;
      lc_concatenated_segments        VARCHAR2 (2000);
      ln_comment_id                   per_all_assignments_f.comment_id%TYPE;
      lb_no_managers_warning          BOOLEAN;
      l_err_message                   VARCHAR2 (3000) := NULL;
      -- Out Variables for Update Employee Assgment Criteria
      -- -------------------------------------------------------------------------------
      ln_special_ceiling_step_id      per_all_assignments_f.special_ceiling_step_id%TYPE;
      lc_group_name                   VARCHAR2 (30);
      ld_effective_start_date         per_all_assignments_f.effective_start_date%TYPE;
      ld_effective_end_date           per_all_assignments_f.effective_end_date%TYPE;
      lb_org_now_no_manager_warning   BOOLEAN;
      lb_other_manager_warning        BOOLEAN;
      lb_spp_delete_warning           BOOLEAN;
      lc_entries_changed_warning      VARCHAR2 (30);
      lb_tax_district_changed_warn    BOOLEAN;
      p_effective_date                DATE;
      l_v_errcode                     VARCHAR2 (300);
      l_v_errbuff                     VARCHAR2 (300);
      l_people_group_name             VARCHAR2 (100);
      lc_hourly_salaried_warning      BOOLEAN;
      lc_org_now_no_manager_warning   BOOLEAN;
      l_ass_err_mess                  VARCHAR2 (3000) := NULL;
   BEGIN
      fnd_global.apps_initialize (fnd_global.user_id,
                                  fnd_global.resp_id,
                                  fnd_global.resp_appl_id);
      rghr_assig_val_proc (l_v_errcode, l_v_errbuff);
      COMMIT;

      FOR i_ass IN c_ass_cur
      LOOP
         l_err_message := NULL;

         BEGIN
            BEGIN
               dt_api.find_dt_upd_modes (
                  p_effective_date         => TRUNC (i_ass.effective_start_date),
                  p_base_table_name        => 'PER_ALL_ASSIGNMENTS_F',
                  p_base_key_column        => 'ASSIGNMENT_ID',
                  p_base_key_value         => i_ass.assignment_id,
                  -- Output data elements
                  -- --------------------------------
                  p_correction             => lb_correction,
                  p_update                 => lb_update,
                  p_update_override        => lb_update_override,
                  p_update_change_insert   => lb_update_change_insert
               );
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_err_message := 'error';
                  rg_log ('EE' || '-->' || l_err_message);
            END;

            IF (lb_update_override = TRUE OR lb_update_change_insert = TRUE)
            THEN
               -- UPDATE_OVERRIDE
               -- ---------------------------------
               lc_dt_ud_mode := 'UPDATE_OVERRIDE';
            END IF;

            IF (lb_correction = TRUE)
            THEN
               -- CORRECTION
               -- ----------------------
               lc_dt_ud_mode := 'CORRECTION';
            END IF;

            IF (lb_update = TRUE)
            THEN
               -- UPDATE
               -- --------------
               lc_dt_ud_mode := 'UPDATE';
            END IF;

            rg_log (lc_dt_ud_mode);
            rg_log ('2');

            BEGIN
               rg_log ('i_ass.assignment_id ' || i_ass.assignment_id);

               SELECT   pas.effective_start_date,
                        pas.object_version_number,
                        pas.soft_coding_keyflex_id,
                        pas.comment_id
                 INTO   p_effective_date,
                        ln_object_number,
                        lc_concatenated_segments,
                        ln_comment_id
                 FROM   per_all_assignments_f pas
                WHERE   pas.effective_end_date =
                           TO_DATE ('12/31/4712', 'MM/DD/RRRR')
                        AND pas.assignment_id = i_ass.assignment_id;

               rg_log(   ' TEST '
                      || '-'
                      || ln_object_number
                      || '-'
                      || lc_concatenated_segments
                      || '-'
                      || ln_comment_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  rg_log ('error ' || SUBSTR (SQLERRM, 1, 299));

                  UPDATE   rgper_emp_assignment_stg
                     SET   rec_status = 'E',
                           rec_message =
                              'Employees Assignment record not found'
                   --,assignment_id = i_ass.assignment_id
                   WHERE   ROWID = i_ass.ROWID AND rec_status = 'V';
            END;

            rg_log ('3');
            lc_concatenated_segments := NULL;
            ld_effective_start_date := NULL;
            ld_effective_end_date := NULL;
            lb_no_managers_warning := NULL;
            lb_other_manager_warning := NULL;

            -- Update Employee Assignment
            -- ---------------------------------------------
            IF i_ass.p_person_type != 'Contingent Worker'
            THEN
               BEGIN
                  hr_assignment_api.update_emp_asg (    -- Input data elements
                     -- ------------------------------
                     p_effective_date             => TRUNC (i_ass.effective_start_date),
                     p_datetrack_update_mode      => lc_dt_ud_mode,
                     p_assignment_id              => i_ass.assignment_id,
                     p_supervisor_id              => i_ass.supervisor_id,
                     p_default_code_comb_id       => i_ass.code_combination_id,
                     p_set_of_books_id            => i_ass.set_of_books_id,
                     p_change_reason              => NULL,
                     p_manager_flag               => 'N',
                     p_bargaining_unit_code       => NULL,
                     p_labour_union_member_flag   => NULL,
                     -- Output data elements
                     -- -------------------------------
                     p_object_version_number      => ln_object_number,
                     p_soft_coding_keyflex_id     => ln_soft_coding_keyflex_id,
                     p_concatenated_segments      => lc_concatenated_segments,
                     p_comment_id                 => ln_comment_id,
                     p_effective_start_date       => ld_effective_start_date,
                     p_effective_end_date         => ld_effective_end_date,
                     p_no_managers_warning        => lb_no_managers_warning,
                     p_other_manager_warning      => lb_other_manager_warning
                  );
                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_message :=
                        'update_emp_asg' || SUBSTR (SQLERRM, 1, 299);
               END;
            ELSE
               BEGIN
                  hr_assignment_api.update_cwk_asg (
                     p_effective_date               => TRUNC (i_ass.effective_start_date),
                     p_datetrack_update_mode        => lc_dt_ud_mode,
                     p_assignment_id                => i_ass.assignment_id,
                     p_supervisor_id                => i_ass.supervisor_id,
                     --p_assignment_status_type_id => l_assignment_status_type_id,
                     p_default_code_comb_id         => i_ass.code_combination_id,
                     p_set_of_books_id              => i_ass.set_of_books_id,
                     --p_normal_hours => l_normal_hours ,
                     --In/Out,
                     p_object_version_number        => ln_object_number,
                     --Out
                     p_org_now_no_manager_warning   => lc_org_now_no_manager_warning,
                     p_effective_start_date         => ld_effective_start_date,
                     --1
                     p_effective_end_date           => ld_effective_end_date,
                     --1
                     p_comment_id                   => ln_comment_id,      --1
                     p_no_managers_warning          => lb_no_managers_warning,
                     --1
                     p_other_manager_warning        => lb_other_manager_warning,
                     --1
                     p_soft_coding_keyflex_id       => ln_soft_coding_keyflex_id,
                     --1
                     p_concatenated_segments        => lc_concatenated_segments,
                     --1
                     p_hourly_salaried_warning      => lc_hourly_salaried_warning
                  );
                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_message :=
                        'update_cwk_asg' || SUBSTR (SQLERRM, 1, 299);
               END;
            END IF;

            dt_api.find_dt_upd_modes (
               p_effective_date         => TRUNC (i_ass.effective_start_date),
               p_base_table_name        => 'PER_ALL_ASSIGNMENTS_F',
               p_base_key_column        => 'ASSIGNMENT_ID',
               p_base_key_value         => i_ass.assignment_id,
               -- Output data elements
               -- --------------------------------
               p_correction             => lb_correction,
               p_update                 => lb_update,
               p_update_override        => lb_update_override,
               p_update_change_insert   => lb_update_change_insert
            );

            IF (lb_update_override = TRUE OR lb_update_change_insert = TRUE)
            THEN
               -- UPDATE_OVERRIDE
               -- ---------------------------------
               lc_dt_ud_mode := 'UPDATE_OVERRIDE';
            END IF;

            IF (lb_correction = TRUE)
            THEN
               -- CORRECTION
               -- ----------------------
               lc_dt_ud_mode := 'CORRECTION';
            END IF;

            IF (lb_update = TRUE)
            THEN
               -- UPDATE
               -- --------------
               lc_dt_ud_mode := 'UPDATE';
            END IF;

            IF i_ass.p_person_type != 'Contingent Worker'
            THEN
               BEGIN
                  rg_log ('i_ass.job_id -> ' || i_ass.job_id);
                  hr_assignment_api.update_emp_asg_criteria ( -- Input data elements
                     -- ------------------------------
                     p_effective_date                 => TRUNC (i_ass.effective_start_date),
                     p_datetrack_update_mode          => lc_dt_ud_mode,
                     --'CORRECTION',
                     p_assignment_id                  => i_ass.assignment_id,
                     p_location_id                    => i_ass.location_id,
                     p_grade_id                       => i_ass.grade_id,
                     p_job_id                         => i_ass.job_id,
                     p_payroll_id                     => i_ass.payroll_id,
                     p_organization_id                => i_ass.org_id,
                     p_employment_category            => 'FT',
                     -- Output data elements
                     -- -------------------------------
                     p_people_group_id                => i_ass.people_group_id,
                     p_object_version_number          => ln_object_number,
                     p_special_ceiling_step_id        => ln_special_ceiling_step_id,
                     p_group_name                     => lc_group_name,
                     p_effective_start_date           => ld_effective_start_date,
                     p_effective_end_date             => ld_effective_end_date,
                     p_org_now_no_manager_warning     => lb_org_now_no_manager_warning,
                     p_other_manager_warning          => lb_other_manager_warning,
                     p_spp_delete_warning             => lb_spp_delete_warning,
                     p_entries_changed_warning        => lc_entries_changed_warning,
                     p_tax_district_changed_warning   => lb_tax_district_changed_warn
                  );
                  COMMIT;
                  rg_log(   'i_ass.job_id COMPLETED -> '
                         || i_ass.job_id
                         || ' - '
                         || lc_dt_ud_mode);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_message :=
                           l_err_message
                        || ' update_emp_asg_criteria '
                        || SUBSTR (SQLERRM, 1, 299);
                     rg_log (SQLERRM);

                     UPDATE   rgper_emp_assignment_stg
                        SET   rec_status = 'E',
                              rec_message = 'Job Not assigned to the employee'
                      --,assignment_id = i_ass.assignment_id
                      WHERE   ROWID = i_ass.ROWID AND rec_status = 'V';
               END;
            ELSE
               BEGIN
                  rg_log ('4 -- ' || i_ass.job_id || ' -- ' || lc_dt_ud_mode);
                  hr_assignment_api.update_cwk_asg_criteria (
                     p_effective_date                 => TRUNC (i_ass.effective_start_date),
                     p_datetrack_update_mode          => lc_dt_ud_mode,
                     p_assignment_id                  => i_ass.assignment_id,
                     --, p_validate => false --lb_validate_mode
                     p_called_from_mass_update        => TRUE,
                     --, p_position_id => l_position_id
                     p_job_id                         => i_ass.job_id,
                     p_location_id                    => i_ass.location_id,
                     p_organization_id                => i_ass.org_id,
                     -- In/Out

                     -- Out
                     p_people_group_name              => l_people_group_name,
                     p_people_group_id                => i_ass.people_group_id,
                     p_object_version_number          => ln_object_number,
                     p_effective_start_date           => ld_effective_start_date,
                     p_effective_end_date             => ld_effective_end_date,
                     p_org_now_no_manager_warning     => lb_org_now_no_manager_warning,
                     p_other_manager_warning          => lb_other_manager_warning,
                     p_spp_delete_warning             => lb_spp_delete_warning,
                     p_entries_changed_warning        => lc_entries_changed_warning,
                     p_tax_district_changed_warning   => lb_tax_district_changed_warn
                  );
                  COMMIT;
                  rg_log (
                     '4 -- ' || i_ass.job_id || ' -- ' || ln_object_number
                  );
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_err_message :=
                        'update_cwk_asg_criteria '
                        || SUBSTR (SQLERRM, 1, 299);
                     rg_log ('l_err_message ' || l_err_message);
               /*UPDATE RGPER_EMP_ASSIGNMENT_STG
               SET rec_status = 'E', rec_message = 'JOB '||l_err_message
               --,assignment_id = i_ass.assignment_id
               WHERE ROWID = i_ass.ROWID
               AND rec_status = 'V';*/
               END;
            END IF;

            COMMIT;

            IF TRIM (l_err_message) IS NULL
            THEN
               UPDATE   rgper_emp_assignment_stg
                  SET   rec_status = 'P',
                        rec_message = 'Employee Assignment Updated'
                --,assignment_id = i_ass.assignment_id
                WHERE   ROWID = i_ass.ROWID AND rec_status = 'V';

               l_err_message := NULL;
            ELSE
               UPDATE   rgper_emp_assignment_stg
                  SET   rec_status = 'E',
                        rec_message = 'EXE ' || l_err_message
                --,assignment_id = i_ass.assignment_id
                WHERE   ROWID = i_ass.ROWID AND rec_status = 'V';

               l_err_message := NULL;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_err_message := SUBSTR (SQLERRM, 1, 299);

               UPDATE   rgper_emp_assignment_stg
                  SET   rec_status = 'E', rec_message = l_err_message
                --,assignment_id = i_ass.assignment_id
                WHERE   ROWID = i_ass.ROWID;

               l_err_message := NULL;
               COMMIT;
               rg_log ('pkg --> ' || SUBSTR (SQLERRM, 1, 299));
         END;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         p_errcode := '1';
         rg_log (SUBSTR (SQLERRM, 1, 299));
   END;

   PROCEDURE rghr_emp_det_dmp (p_errcode   OUT VARCHAR2,
                               p_errmess   OUT VARCHAR2)
   AS
   BEGIN
      INSERT INTO rgper_emp_details_stg
           SELECT   * FROM rgper_emp_details_stg;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errmess := SUBSTR (SQLERRM, 1, 299);
   END;

   PROCEDURE rghr_emp_ass_det_dmp (p_errcode   OUT VARCHAR2,
                                   p_errmess   OUT VARCHAR2)
   AS
   BEGIN
      INSERT INTO rgper_emp_assignment_stg
           SELECT   * FROM rgper_emp_assignment_stg;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errmess := SUBSTR (SQLERRM, 1, 299);
   END;


   PROCEDURE rghr_emp_email
   AS
      p_dest              VARCHAR2 (300) := 'ext.sanne@riotgames.com';
      g_user_id           fnd_user.user_id%TYPE;
      g_resp_id           NUMBER;
      g_resp_appl_id      NUMBER;
      g_log_level         NUMBER := 0;
      g_user_name         VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_US_USER');
      g_interface_email   VARCHAR2 (40) := 'ext.sanne@riotgames.com';
      g_mailhost VARCHAR2 (30)
            := fnd_profile.VALUE ('RGAP_SMTP_SERVER') ;
      g_port NUMBER
            := fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT') ;
      g_sender            VARCHAR2 (100) := 'donotreply@riotgames.com';
      g_resp_key VARCHAR2 (40)
            := fnd_profile.VALUE ('RGAP_US_RESP_KEY') ;
      g_subject VARCHAR2 (150)
            := 'RIS to EBS Employee Interface Report' ;

      mail_conn           UTL_SMTP.connection;
      r                   UTL_SMTP.replies;

      v_instance_name     VARCHAR2 (20);
      L_EMP_ASS_REC_NP    NUMBER := 0;
      L_EMP_ASS_REC_P     NUMBER := 0;

      L_EMP_REC_NP        NUMBER := 0;
      L_EMP_REC_P         NUMBER := 0;
   BEGIN
      SELECT   name INTO v_instance_name FROM v$database;


      BEGIN
         SELECT   DESCRIPTION
           INTO   p_dest
           FROM   FND_LOOKUP_VALUES
          WHERE   LOOKUP_TYPE = 'RGPER_EMPLOYEE_INTER_EMAIL'
          AND MEANING = 'EMP';
      EXCEPTION
         WHEN OTHERS
         THEN
            p_dest := 'noreply@riotgames.com';
      END;

      mail_conn := UTL_SMTP.open_connection (G_mailhost, G_port);
      r := UTL_SMTP.ehlo (mail_conn, G_mailhost);
      UTL_SMTP.mail (mail_conn, G_sender);
      UTL_SMTP.rcpt (mail_conn, p_dest);
      UTL_SMTP.open_data (mail_conn);
      UTL_SMTP.write_data (mail_conn,
                           'MIME-version: 1.0' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
            'Content-Type: text/html; charset=ISO-8859-15'
         || CHR (13)
         || CHR (10)
      );
      UTL_SMTP.write_data (
         mail_conn,
         'Content-Transfer-Encoding: 8bit' || CHR (13) || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn,
                           'From: ' || g_sender || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
            'Subject: '
         || g_subject
         || '-['
         || v_instance_name
         || ']'
         || CHR (13)
         || CHR (10)
      );
      --'Concurr Expenses Raised error during Oracle Import'
      UTL_SMTP.write_data (mail_conn,
                           'To: ' || p_dest || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
         'Employee interface from RIS to Oracle EBS completed successfully. Below are details of success/exception/error records for employee and assignment records.'
         || CHR (13)
         || CHR (10)
      );

      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));

      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
         '<b>Employee Records - Success</b>' || CHR (13) || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));


      BEGIN
         SELECT   COUNT (0)
           INTO   L_EMP_REC_P
           FROM   rgper_emp_details_stg
          WHERE   REC_STATUS = 'P'
                  AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            L_EMP_REC_P := 0;
      END;

      IF L_EMP_REC_P != 0
      THEN
         UTL_SMTP.write_data (
            mail_conn,
            rgap_common_utility_pkg.rgap_get_html_report('select Employee_Number,first_name,last_name
                     ,person_type,effective_start_date,EFFECTIVE_END_DATE,decode(REC_STATUS,''P'',''Processed'',''D'',''Duplicate Record'',''E'',''Error'') REC_STATUS,REC_MESSAGE
                              from rgper_emp_details_stg
                           WHERE REC_STATUS = ''P'' AND TRUNC(SYSDATE) = TRUNC (CREATION_DATE)')
            || CHR (13)
            || CHR (10)
         );
      ELSE
         UTL_SMTP.write_data (
            mail_conn,
            'Employee Success Records not found' || CHR (13) || CHR (10)
         );
      END IF;

      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));


      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
         '<b>Employee Records - Error/Exception</b>' || CHR (13) || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));


      BEGIN
         SELECT   COUNT (0)
           INTO   L_EMP_REC_NP
           FROM   rgper_emp_details_stg
          WHERE   REC_STATUS != 'P'
                  AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            L_EMP_REC_NP := 0;
      END;

      IF L_EMP_REC_NP != 0
      THEN
         UTL_SMTP.write_data (
            mail_conn,
            rgap_common_utility_pkg.rgap_get_html_report('select Employee_Number,first_name,last_name
                        ,person_type,effective_start_date,EFFECTIVE_END_DATE,decode(REC_STATUS,''P'',''Processed'',''D'',''Duplicate Record'',''E'',''Error'') REC_STATUS,REC_MESSAGE
                                 from rgper_emp_details_stg
                              WHERE REC_STATUS != ''P'' AND TRUNC(SYSDATE) = TRUNC (CREATION_DATE)')
            || CHR (13)
            || CHR (10)
         );
      ELSE
         UTL_SMTP.write_data (
            mail_conn,
               'Employee Error/Exception Records not found'
            || CHR (13)
            || CHR (10)
         );
      END IF;

      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));


      UTL_SMTP.write_data (
         mail_conn,
            '<b>Employee Assignment Records - Success</b>'
         || CHR (13)
         || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));

      BEGIN
         SELECT   COUNT (0)
           INTO   L_EMP_ASS_REC_P
           FROM   RGPER_EMP_ASSIGNMENT_STG
          WHERE   REC_STATUS = 'P'
                  AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            L_EMP_ASS_REC_P := 0;
      END;


      IF L_EMP_ASS_REC_P != 0
      THEN
         UTL_SMTP.write_data (
            mail_conn,
            rgap_common_utility_pkg.rgap_get_html_report('SELECT EMPLOYEE_NUMBER,
                                                  SUPERVISOR_NUMBER,
                                                  JOB,
                                                  decode(REC_STATUS,''P'',''Processed'',''D'',''Duplicate Record'',''E'',''Error'') REC_STATUS,
                                                  REC_MESSAGE
                                                     FROM RGPER_EMP_ASSIGNMENT_STG
                                                     WHERE REC_STATUS = ''P''  AND TRUNC(SYSDATE) = TRUNC (CREATION_DATE)')
            || CHR (13)
            || CHR (10)
         );
      ELSE
         UTL_SMTP.write_data (
            mail_conn,
               'Employee Assignment Success records not found'
            || CHR (13)
            || CHR (10)
         );
      END IF;


      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));



      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));


      UTL_SMTP.write_data (
         mail_conn,
            '<b>Employee Assignment Records - Error/Exception</b>'
         || CHR (13)
         || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));


      BEGIN
         SELECT   COUNT (0)
           INTO   L_EMP_ASS_REC_NP
           FROM   RGPER_EMP_ASSIGNMENT_STG
          WHERE   REC_STATUS != 'P'
                  AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            L_EMP_ASS_REC_NP := 0;
      END;

      IF L_EMP_ASS_REC_NP != 0
      THEN
         UTL_SMTP.write_data (
            mail_conn,
            rgap_common_utility_pkg.rgap_get_html_report('SELECT EMPLOYEE_NUMBER,
                                                  SUPERVISOR_NUMBER,
                                                  JOB,
                                                  decode(REC_STATUS,''P'',''Processed'',''D'',''Duplicate Record'',''E'',''Error'') REC_STATUS,
                                                  REC_MESSAGE
                                                     FROM RGPER_EMP_ASSIGNMENT_STG
                                                     WHERE REC_STATUS != ''P'' AND TRUNC(SYSDATE) = TRUNC (CREATION_DATE)')
            || CHR (13)
            || CHR (10)
         );
      ELSE
         UTL_SMTP.write_data (
            mail_conn,
               'Employee Assignment Error/Exception Records not found'
            || CHR (13)
            || CHR (10)
         );
      END IF;

      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));

      UTL_SMTP.write_data (
         mail_conn,
         'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to '
         || '<b>'
         || 'Oracle IT Support Center'
         || '</b>'
         || ' and it will be investigated.'
         || CHR (13)
         || CHR (10)
      );
      --UTL_SMTP.write_data (mail_conn, 'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oralcle IT Support Center</strong> and it will be investigated.' || CHR (13) || CHR (10));
      UTL_SMTP.close_data (mail_conn);
      UTL_SMTP.quit (mail_conn);
   EXCEPTION
      WHEN OTHERS
      THEN
         rgcomn_debug_pkg.log_message (
            P_LEVEL          => g_log_level,
            P_PROGRAM_NAME   => 'rgap_common_utility_pkg.rgap_sendmail',
            P_MESSAGE        =>   'Error Occured while sending email'
                               || '-'
                               || SQLCODE
                               || '  '
                               || SQLERRM
         );
   END;


   PROCEDURE rghr_emp_email_norec
   AS
      p_dest              VARCHAR2 (300) := 'ext.sanne@riotgames.com';
      g_user_id           fnd_user.user_id%TYPE;
      g_resp_id           NUMBER;
      g_resp_appl_id      NUMBER;
      g_log_level         NUMBER := 0;
      g_user_name         VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_US_USER');
      g_interface_email   VARCHAR2 (40) := 'ext.sanne@riotgames.com';
      g_mailhost VARCHAR2 (30)
            := fnd_profile.VALUE ('RGAP_SMTP_SERVER') ;
      g_port NUMBER
            := fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT') ;
      g_sender            VARCHAR2 (100) := 'donotreply@riotgames.com';
      g_resp_key VARCHAR2 (40)
            := fnd_profile.VALUE ('RGAP_US_RESP_KEY') ;
      g_subject VARCHAR2 (150)
            := 'RIS to EBS Employee Interface Report' ;

      mail_conn           UTL_SMTP.connection;
      r                   UTL_SMTP.replies;

      v_instance_name     VARCHAR2 (20);
   BEGIN
      SELECT   name INTO v_instance_name FROM v$database;


      BEGIN
         SELECT   DESCRIPTION
           INTO   p_dest
           FROM   FND_LOOKUP_VALUES
          WHERE   LOOKUP_TYPE = 'RGPER_EMPLOYEE_INTER_EMAIL'
          AND MEANING = 'EMP';
      EXCEPTION
         WHEN OTHERS
         THEN
            p_dest := 'noreply@riotgames.com';
      END;

      mail_conn := UTL_SMTP.open_connection (G_mailhost, G_port);
      r := UTL_SMTP.ehlo (mail_conn, G_mailhost);
      UTL_SMTP.mail (mail_conn, G_sender);
      UTL_SMTP.rcpt (mail_conn, p_dest);
      UTL_SMTP.open_data (mail_conn);
      UTL_SMTP.write_data (mail_conn,
                           'MIME-version: 1.0' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
            'Content-Type: text/html; charset=ISO-8859-15'
         || CHR (13)
         || CHR (10)
      );
      UTL_SMTP.write_data (
         mail_conn,
         'Content-Transfer-Encoding: 8bit' || CHR (13) || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn,
                           'From: ' || g_sender || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
            'Subject: '
         || g_subject
         || '-['
         || v_instance_name
         || ']'
         || CHR (13)
         || CHR (10)
      );
      --'Concurr Expenses Raised error during Oracle Import'
      UTL_SMTP.write_data (mail_conn,
                           'To: ' || p_dest || CHR (13) || CHR (10));
      UTL_SMTP.write_data (
         mail_conn,
         'Employee interface from RIS to Oracle EBS completed successfully. There was no new record in stage table to process.'
         || CHR (13)
         || CHR (10)
      );
      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));


      UTL_SMTP.write_data (mail_conn, ' ' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));



      UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));

      UTL_SMTP.write_data (
         mail_conn,
         'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to '
         || '<b>'
         || 'Oracle IT Support Center'
         || '</b>'
         || ' and it will be investigated.'
         || CHR (13)
         || CHR (10)
      );
      --UTL_SMTP.write_data (mail_conn, 'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oralcle IT Support Center</strong> and it will be investigated.' || CHR (13) || CHR (10));
      UTL_SMTP.close_data (mail_conn);
      UTL_SMTP.quit (mail_conn);
   EXCEPTION
      WHEN OTHERS
      THEN
         rgcomn_debug_pkg.log_message (
            P_LEVEL          => g_log_level,
            P_PROGRAM_NAME   => 'rgap_common_utility_pkg.rgap_sendmail',
            P_MESSAGE        =>   'Error Occured while sending email'
                               || '-'
                               || SQLCODE
                               || '  '
                               || SQLERRM
         );
   END;



   PROCEDURE rghr_main (p_errcode OUT VARCHAR2, p_errmess OUT VARCHAR2)
   AS
      ld_errcode          VARCHAR2 (300) := NULL;
      ld_errmess          VARCHAR2 (300) := NULL;
      le_errcode          VARCHAR2 (300) := NULL;
      le_errmess          VARCHAR2 (300) := NULL;
      lea_errcode         VARCHAR2 (300) := NULL;
      lea_errmess         VARCHAR2 (300) := NULL;
      lv_emp_layout       BOOLEAN;
      lv_emp_req_id       NUMBER := NULL;
      lv_emp_ass_layout   BOOLEAN;
      lv_emp_ass_req_id   NUMBER := NULL;
      l_cnt_emp_ass       NUMBER := 0;
      l_cnt_emp           NUMBER := 0;
   BEGIN
      BEGIN
         rg_output ('Identifing duplicate data process going to start');
         rghr_emp_ass_dup_proc (ld_errcode, ld_errmess);
         rg_output ('Identifing duplicate data process exicuted');
         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_errcode := ld_errcode;
            p_errmess := ld_errmess;
            rg_log (ld_errmess);
      END;

      BEGIN
         rg_output ('Create Employee data process going to start');
         rghr_employee_creation_proc (lea_errcode, lea_errmess);
         rg_output ('Create Employee data process completed');
         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            p_errcode := lea_errcode;
            p_errmess := lea_errmess;
            rg_log (lea_errmess);
      END;

      BEGIN
         rg_output ('Create Employee Assignment data process going to start');

         BEGIN
            rghr_assig_val_proc (le_errcode, le_errmess);

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               p_errcode := le_errcode;
               p_errmess := le_errmess;
               rg_log (lea_errmess);
         END;

         rg_output ('Create Employee Assignment VAL COMP');

         BEGIN
            rghr_assignment_creation_proc (le_errcode, le_errmess);
            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               p_errcode := le_errcode;
               p_errmess := le_errmess;
               rg_log (le_errmess);
         END;

         rg_output ('Create Employee Assignment data process completed');
      EXCEPTION
         WHEN OTHERS
         THEN
            p_errcode := le_errcode;
            p_errmess := le_errmess;
            rg_log (le_errmess);
      END;

      BEGIN
         BEGIN
            SELECT   COUNT (0)
              INTO   l_cnt_emp
              FROM   rgper_emp_details_stg
             WHERE   1 = 1                      --rec_status NOT IN ('P', 'N')
                          AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cnt_emp := 0;
         END;

         BEGIN
            SELECT   COUNT (0)
              INTO   l_cnt_emp_ass
              FROM   rgper_emp_assignment_stg
             WHERE   1 = 1                      --rec_status NOT IN ('P', 'N')
                          AND TRUNC (SYSDATE) = TRUNC (CREATION_DATE);
         EXCEPTION
            WHEN OTHERS
            THEN
               l_cnt_emp_ass := 0;
         END;

         IF l_cnt_emp != 0 AND l_cnt_emp_ass != 0
         THEN
            rghr_emp_email;
         ELSE
            rghr_emp_email_norec;
         END IF;


         COMMIT;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_emp_req_id := 0;
            lv_emp_ass_req_id := 0;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errcode := '1';
         p_errmess := SUBSTR (SQLERRM, 1, 299);
   END;
END rghr_employee_intf_pkg;
/
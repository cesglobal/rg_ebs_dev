CREATE OR REPLACE PACKAGE APPS.rghr_employee_intf_pkg
AS
------------------------------------------------------------------------
-- Name...: RGHR_EMPLOYEE_CNV_PKG.pks
-- Desc...: This package is used to Migrate Employees data to EBS
--
--
-- History:
--
-- Date Name Version Number Revision Summary
-- ------------ -------------- --------------- ------------------
-- 13-Aug-2015 Srinivas Anne 1.0
-- 30-Aug-2016 - Enhancement#125-- Prabhat
-- changes added in package body to faclitate #125 business
-- requirement
--
-- ADDITIONAL NOTES
-- ================
--
------------------------------------------------------------------------
   PROCEDURE rghr_emp_ass_dup_proc (
      p_errcode   OUT   VARCHAR2,
      p_errmess   OUT   VARCHAR2
   );

   PROCEDURE rghr_employee_creation_proc (
      p_errcode   OUT   VARCHAR2,
      p_errmess   OUT   VARCHAR2
   );

   PROCEDURE rghr_assig_val_proc (
      p_errcode   OUT   VARCHAR2,
      p_errmess   OUT   VARCHAR2
   );

   PROCEDURE rghr_assignment_creation_proc (
      p_errcode   OUT   VARCHAR2,
      p_errmess   OUT   VARCHAR2
   );

   PROCEDURE rghr_emp_det_dmp (p_errcode OUT VARCHAR2, p_errmess OUT VARCHAR2);

   PROCEDURE rghr_emp_ass_det_dmp (
      p_errcode   OUT   VARCHAR2,
      p_errmess   OUT   VARCHAR2
   );
   PROCEDURE rghr_emp_email ;

   PROCEDURE rghr_main (p_errcode OUT VARCHAR2, p_errmess OUT VARCHAR2);
END rghr_employee_intf_pkg;
/
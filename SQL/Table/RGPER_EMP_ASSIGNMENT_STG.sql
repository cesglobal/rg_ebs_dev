/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Anne
REM    Purpose              : custom Staging table creation script.
REM
REM    Module               : RG HRMS
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Anne          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/

DROP TABLE RGPER.RGPER_EMP_ASSIGNMENT_STG CASCADE CONSTRAINTS;

CREATE TABLE RGPER.RGPER_EMP_ASSIGNMENT_STG
(
  ASSIGNMENT_ID         NUMBER,
  PERSON_ID             NUMBER,
  EMPLOYEE_NUMBER       VARCHAR2(30 BYTE),
  EFFECTIVE_START_DATE  DATE,
  SUPERVISOR_ID         NUMBER,
  SUPERVISOR_NUMBER     VARCHAR2(250 BYTE),
  ORGANIZATION_ID       NUMBER,
  JOB                   VARCHAR2(150 BYTE),
  PEOPLE_GROUP          VARCHAR2(250 BYTE),
  POSITION              VARCHAR2(250 BYTE),
  GRADE                 VARCHAR2(250 BYTE),
  PAYROLL               VARCHAR2(250 BYTE),
  LOCATION              VARCHAR2(250 BYTE),
  LEDGER_NAME           VARCHAR2(250 BYTE),
  DEFAULT_CODE_COMB_ID  VARCHAR2(250 BYTE),
  REC_STATUS            VARCHAR2(1 BYTE),
  REC_MESSAGE           VARCHAR2(3000 BYTE),
  BUSINESS_GROUP_NAME   VARCHAR2(250 BYTE),
  BUSINESS_GROUP_ID     NUMBER,
  CREATION_DATE         DATE,
  CREATED_BY            NUMBER,
  LAST_UPDATE_DATE      DATE,
  LAST_UPDATED_BY       NUMBER,
  ATTRIBUTE1            VARCHAR2(250 BYTE),
  ATTRIBUTE2            VARCHAR2(250 BYTE),
  ATTRIBUTE3            VARCHAR2(250 BYTE),
  ATTRIBUTE4            VARCHAR2(250 BYTE),
  ATTRIBUTE5            VARCHAR2(250 BYTE),
  ATTRIBUTE6            VARCHAR2(250 BYTE),
  ATTRIBUTE7            VARCHAR2(250 BYTE),
  ATTRIBUTE8            VARCHAR2(250 BYTE),
  ATTRIBUTE9            VARCHAR2(250 BYTE),
  ATTRIBUTE10           VARCHAR2(250 BYTE),
  ATTRIBUTE11           VARCHAR2(250 BYTE),
  ATTRIBUTE12           VARCHAR2(250 BYTE),
  ATTRIBUTE13           VARCHAR2(250 BYTE),
  ATTRIBUTE14           VARCHAR2(250 BYTE),
  ATTRIBUTE15           VARCHAR2(250 BYTE)
)
/

EXIT;
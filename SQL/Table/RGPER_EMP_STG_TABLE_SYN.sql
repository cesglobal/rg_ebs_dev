REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: rgap_coupa_ebs_inv_stg_syn.syn
REM PURPOSE : Customer Data Staging table for Employee Interface
REM PARAMETERS : 
REM CALLED :
REM CALLED BY :
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 14-OCT-15 1.0 INITIAL RELEASE
REM
REM ======================================================================

DROP SYNONYM APPS.RGPER_EMP_DETAILS_STG;

CREATE OR REPLACE SYNONYM APPS.RGPER_EMP_DETAILS_STG FOR RGPER.RGPER_EMP_DETAILS_STG;

DROP SYNONYM APPS.RGPER_EMP_ASSIGNMENT_STG;

CREATE OR REPLACE SYNONYM APPS.RGPER_EMP_ASSIGNMENT_STG FOR RGPER.RGPER_EMP_ASSIGNMENT_STG;

/

EXIT;



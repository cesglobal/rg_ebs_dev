create table RGCE.RGCE_INT_DAY_SQLLDR_ERRORS_STG
(STATEMENT_NUMBER                 VARCHAR2(50)
,BANK_ACCOUNT_NUM                 VARCHAR2(30)
,REC_NO                           NUMBER
,MESSAGE_TEXT                     VARCHAR2(2000)
,STATUS                           VARCHAR2(30)
,CREATED_BY                       NUMBER(15)
,CREATION_DATE                    DATE
,LAST_UPDATED_BY                  NUMBER(15)
,LAST_UPDATE_DATE                 DATE
,LAST_UPDATE_LOGIN                NUMBER(15)
,BANK_ACCT_CURRENCY_CODE          VARCHAR2(15)
)
/
EXIT;
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Anne
REM    Purpose              : custom Staging table creation script.
REM
REM    Module               : RG HRMS
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Anne          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/

DROP TABLE RGPER.RGPER_EMP_DETAILS_STG;

CREATE TABLE RGPER.RGPER_EMP_DETAILS_STG
(
  PERSON_ID             NUMBER,
  EFFECTIVE_START_DATE  DATE,
  EFFECTIVE_END_DATE    DATE,
  PERSON_TYPE_ID        NUMBER,
  PERSON_TYPE           VARCHAR2(50 BYTE),
  LAST_NAME             VARCHAR2(150 BYTE),
  EMAIL_ADDRESS         VARCHAR2(240 BYTE),
  EMPLOYEE_NUMBER       VARCHAR2(30 BYTE),
  FIRST_NAME            VARCHAR2(150 BYTE),
  FULL_NAME             VARCHAR2(240 BYTE),
  SEX                   VARCHAR2(30 BYTE),
  DATE_OF_BIRTH         DATE,
  KNOWN_AS              VARCHAR2(250 BYTE),
  MIDDLE_NAMES          VARCHAR2(60 BYTE),
  MARITAL_STATUS        VARCHAR2(30 BYTE),
  INTERNAL_LOCATION     VARCHAR2(250 BYTE),
  BUSINESS_GROUP_NAME   VARCHAR2(150 BYTE),
  BUSINESS_GROUP_ID     NUMBER,
  ASSIGNMENT_ID         NUMBER,
  REC_STATUS            VARCHAR2(1 BYTE),
  REC_MESSAGE           VARCHAR2(3000 BYTE),
  CREATION_DATE         DATE,
  CREATED_BY            NUMBER,
  LAST_UPDATE_DATE      DATE,
  LAST_UPDATED_BY       NUMBER,
  ATTRIBUTE1            VARCHAR2(250 BYTE),
  ATTRIBUTE2            VARCHAR2(250 BYTE),
  ATTRIBUTE3            VARCHAR2(250 BYTE),
  ATTRIBUTE4            VARCHAR2(250 BYTE),
  ATTRIBUTE5            VARCHAR2(250 BYTE),
  ATTRIBUTE6            VARCHAR2(250 BYTE),
  ATTRIBUTE7            VARCHAR2(250 BYTE),
  ATTRIBUTE8            VARCHAR2(250 BYTE),
  ATTRIBUTE9            VARCHAR2(250 BYTE),
  ATTRIBUTE10           VARCHAR2(250 BYTE),
  ATTRIBUTE11           VARCHAR2(250 BYTE),
  ATTRIBUTE12           VARCHAR2(250 BYTE),
  ATTRIBUTE13           VARCHAR2(250 BYTE),
  ATTRIBUTE14           VARCHAR2(250 BYTE),
  ATTRIBUTE15           VARCHAR2(250 BYTE)
)
/

EXIT;
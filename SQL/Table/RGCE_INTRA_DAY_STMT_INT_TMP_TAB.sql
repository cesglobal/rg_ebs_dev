/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : custom Staging table creation script.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
drop table RGCE.RGCE_INTRA_DAY_STMT_INT_TMP;

CREATE TABLE RGCE.RGCE_INTRA_DAY_STMT_INT_TMP
(--REC_NO     	    NUMBER
 REC_ID_NO          VARCHAR2(30)
,COLUMN1            VARCHAR2(2000)
,COLUMN2            VARCHAR2(255)
,COLUMN3            VARCHAR2(255)
,COLUMN4            VARCHAR2(255)
,COLUMN5            VARCHAR2(255)
,COLUMN6            VARCHAR2(255)
,COLUMN7            VARCHAR2(255)
,COLUMN8            VARCHAR2(255)
,COLUMN9            VARCHAR2(255)
,COLUMN10           VARCHAR2(255)
,COLUMN11           VARCHAR2(255)
,COLUMN12           VARCHAR2(255)
,COLUMN13           VARCHAR2(255)
,COLUMN14           VARCHAR2(255)
,COLUMN15           VARCHAR2(255)
,COLUMN16           VARCHAR2(255)
,COLUMN17           VARCHAR2(255)
,COLUMN18           VARCHAR2(255)
,COLUMN19           VARCHAR2(255)
,COLUMN20           VARCHAR2(255)
,COLUMN21           VARCHAR2(255)
,COLUMN22           VARCHAR2(255)
,COLUMN23           VARCHAR2(255)
,COLUMN24           VARCHAR2(255)
,COLUMN25           VARCHAR2(255)
,COLUMN26           VARCHAR2(255)
,COLUMN27           VARCHAR2(255)
,COLUMN28           VARCHAR2(255)
,COLUMN29           VARCHAR2(255)
,COLUMN30           VARCHAR2(255)
,COLUMN31           VARCHAR2(255)
,COLUMN32           VARCHAR2(255)
,COLUMN33           VARCHAR2(255)
,COLUMN34           VARCHAR2(255)
,COLUMN35           VARCHAR2(255)
,CREATED_BY         NUMBER(15)
,CREATION_DATE      DATE
,LAST_UPDATED_BY    NUMBER(15)
,LAST_UPDATE_DATE   DATE
,LAST_UPDATE_LOGIN  NUMBER(15)
,COLUMN36           VARCHAR2(2000)
,FILE_NAME          VARCHAR2(255)
,FILE_path          VARCHAR2(240)
,REQUEST_ID         NUMBER
)
/
SHOW ERRORS;
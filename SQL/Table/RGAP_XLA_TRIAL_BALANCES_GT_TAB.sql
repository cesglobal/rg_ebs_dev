DROP TABLE RGAP.RGAP_XLA_TRIAL_BALANCES_GT CASCADE CONSTRAINTS;

CREATE GLOBAL TEMPORARY TABLE RGAP.RGAP_XLA_TRIAL_BALANCES_GT
(
  DEFINITION_CODE                VARCHAR2(30 BYTE) NOT NULL,
  LEDGER_ID                      NUMBER(15)     NOT NULL,
  LEDGER_NAME                    VARCHAR2(30 BYTE) NOT NULL,
  LEDGER_SHORT_NAME              VARCHAR2(20 BYTE) NOT NULL,
  LEDGER_CURRENCY_CODE           VARCHAR2(15 BYTE) NOT NULL,
  RECORD_TYPE_CODE               VARCHAR2(30 BYTE) NOT NULL,
  SOURCE_ENTITY_ID               NUMBER(15),
  SOURCE_APPLICATION_ID          NUMBER(15),
  SOURCE_ENTITY_CODE             VARCHAR2(30 BYTE),
  APPLIED_TO_ENTITY_ID           NUMBER(15),
  APPLIED_TO_APPLICATION_ID      NUMBER(15),
  APPLIED_TO_ENTITY_CODE         VARCHAR2(30 BYTE),
  EVENT_CLASS_CODE               VARCHAR2(30 BYTE),
  TRANSACTION_NUMBER             VARCHAR2(80 BYTE),
  CODE_COMBINATION_ID            NUMBER(15)     NOT NULL,
  GL_DATE                        DATE,
  TRX_CURRENCY_CODE              VARCHAR2(15 BYTE) NOT NULL,
  ENTERED_UNROUNDED_ORIG_AMOUNT  NUMBER,
  ENTERED_UNROUNDED_REM_AMOUNT   NUMBER,
  ENTERED_ROUNDED_ORIG_AMOUNT    NUMBER,
  ENTERED_ROUNDED_REM_AMOUNT     NUMBER,
  ACCTD_UNROUNDED_ORIG_AMOUNT    NUMBER,
  ACCTD_UNROUNDED_REM_AMOUNT     NUMBER,
  ACCTD_ROUNDED_ORIG_AMOUNT      NUMBER,
  ACCTD_ROUNDED_REM_AMOUNT       NUMBER,
  THIRD_PARTY_NAME               VARCHAR2(360 BYTE),
  THIRD_PARTY_NUMBER             VARCHAR2(30 BYTE),
  THIRD_PARTY_TYPE_CODE          VARCHAR2(30 BYTE),
  THIRD_PARTY_TYPE               VARCHAR2(80 BYTE),
  THIRD_PARTY_SITE_NAME          VARCHAR2(240 BYTE),
  THIRD_PARTY_ACCOUNT_NUMBER     VARCHAR2(30 BYTE),
  BALANCING_SEGMENT_VALUE        VARCHAR2(25 BYTE),
  NATURAL_ACCOUNT_SEGMENT_VALUE  VARCHAR2(25 BYTE),
  COST_CENTER_SEGMENT_VALUE      VARCHAR2(25 BYTE),
  INTERCOMPANY_SEGMENT_VALUE     VARCHAR2(25 BYTE),
  MANAGEMENT_SEGMENT_VALUE       VARCHAR2(25 BYTE),
  USER_TRX_IDENTIFIER_NAME_1     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_1    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_2     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_2    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_3     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_3    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_4     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_4    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_5     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_5    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_6     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_6    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_7     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_7    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_8     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_8    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_9     VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_9    VARCHAR2(240 BYTE),
  USER_TRX_IDENTIFIER_NAME_10    VARCHAR2(80 BYTE),
  USER_TRX_IDENTIFIER_VALUE_10   VARCHAR2(240 BYTE),
  NON_AP_AMOUNT                  NUMBER,
  MANUAL_SLA_AMOUNT              NUMBER,
  INVOICE_ID                     NUMBER
)
ON COMMIT PRESERVE ROWS
NOCACHE;

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, DEBUG, FLASHBACK ON RGAP.RGAP_XLA_TRIAL_BALANCES_GT TO APPS;

GRANT ALTER, DELETE, INDEX, INSERT, REFERENCES, SELECT, UPDATE, ON COMMIT REFRESH, QUERY REWRITE, READ, DEBUG, FLASHBACK ON RGAP.RGAP_XLA_TRIAL_BALANCES_GT TO RIOTSRV;


/
BEGIN
UPDATE BNE_INTERFACE_COLS_B 
   SET data_type = 3 
 WHERE interface_code LIKE 'RGAR_INV_UPD_XINTG_INTF1'
   AND sequence_num = 25 
   AND interface_col_name = 'P_TEXT6';
COMMIT; 
EXCEPTION
	WHEN OTHERS THEN
	dbms_output.put_line('Error while updating BNE_INTERFACE_COLS_B Table:'||SQLERRM);
END;
/
EXIT;  
/


begin
declare
cursor c1 is
select a.invoice_id, a.invoice_num, a.vendor_id, c.segment1, c.vendor_name, b.attribute1
from ap_invoices_all a,
		  ap_invoice_lines_all b,
		  ap_suppliers c
where a.invoice_id = b.invoice_id
and a.vendor_id = c.vendor_id
and b.line_type_lookup_code = 'ITEM'
and a.invoice_num in (select invoice_number
											from  rgap_invoice_po_map_temp);
cursor c3 is
select a.invoice_id, a.invoice_num, a.vendor_id, c.segment1, c.vendor_name, b.attribute1, b.line_number, d.po_number
from ap_invoices_all a,
		  ap_invoice_lines_all b,
		  ap_suppliers c,
		  rgap_invoice_po_map_temp d
where a.invoice_id = b.invoice_id
and a.vendor_id = c.vendor_id
and b.line_type_lookup_code = 'ITEM'
and a.invoice_num = d.invoice_number
and c.segment1 = d.supplier_number
and d.status = 'N'
and b.attribute1 is null;
cursor c5 is
select a.invoice_id, a.invoice_num, a.vendor_id, c.segment1, c.vendor_name, b.attribute1, b.line_number, b.attribute_category, b.line_source
from ap_invoices_all a,
		  ap_invoice_lines_all b,
		  ap_suppliers c
where a.invoice_id = b.invoice_id
and a.vendor_id = c.vendor_id
and b.line_type_lookup_code = 'ITEM'
and (b.attribute_category != 'US Context' or b.attribute_category is null)
and a.source = 'COUPA';
begin
for c2 in c1
loop
update rgap_invoice_po_map_temp
set invoice_id = c2.invoice_id,
       vendor_id = c2.vendor_id,
       status = 'N'
 where invoice_number = c2.invoice_num
 and supplier_number = c2.segment1;
 end loop;
 commit;
update rgap_invoice_po_map_temp
set status = 'E',
      message = 'Invoice/Vendor not found in EBS'
 where status is null;
 commit;
 for c4 in c3
 loop
 update ap_invoice_lines_all
 set attribute1 = c4.po_number,
       attribute10 = 'PO Update'
 where invoice_id = c4.invoice_id
 and line_number = c4.line_number;
 update rgap_invoice_po_map_temp
 set status = 'S'
 where invoice_number = c4.invoice_num
 and supplier_number = c4.segment1;
 commit;
 end loop;
for c6 in c5
loop
update  ap_invoice_lines_all
 set attribute_category = 'US Context'
 where invoice_id = c6.invoice_id
 and line_number = c6.line_number;
 commit;
 end loop;
 end;
 end;
/

BEGIN
UPDATE bne_attributes 
   SET attribute2 = 'DATE'
 WHERE attribute1 = 'P_TEXT6'
   AND attribute_code IN (SELECT attribute_code 
                           FROM bne_param_list_items
                          WHERE param_list_code = (SELECT upload_param_list_code 
						                             FROM bne_interfaces_vl 
													WHERE integrator_code = 'RGAR_INV_UPD_XINTG'));
COMMIT;
EXCEPTION
	WHEN OTHERS THEN
	dbms_output.put_line('Error while updating BNE_ATTRIBUTES Table:'||SQLERRM);
END;
/
EXIT;
/
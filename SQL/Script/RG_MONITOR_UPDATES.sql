 /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose              : This Script will enable all Inbound interfaces to inlude in Riot Interface scheduler.
   --    Module               : RGAP
   --    Modification History :
    --       Date             Name             Version Number    Issue No   Revision Summary
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    10-May-2016    Srinivasa              1.0  Initial version 
   --   
   --
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
 
update RGINTF_MONITOR_TAB set attribute2='INTF_USER' ,attribute3='Payables Manager',Attribute4='RGAP_CREBS_INV_LOAD' where interface_name='INV_INTF_CR_EBS';
update RGINTF_MONITOR_TAB set attribute2='INTF_USER' ,attribute3='Payables Manager',Attribute4='RGAP_COUPA_EBS_INV_INT_PKG' where interface_name='INV_INTF_CP_EBS';
update RGINTF_MONITOR_TAB set attribute2='INTF_USER' ,attribute3='Cash Management Superuser',Attribute4='RGCE_BANK_STATEMENT_LOAD',Attribute5='LOAD',Attribute6='1020'   where interface_name='STM_INTF_BANK_EBS';
update RGINTF_MONITOR_TAB set attribute2='INTF_USER' ,attribute3='HR Foundation',Attribute4='RGPER_EMP_INTERFACE' where interface_name='HR_INTF_RIS_EBS';
update RGINTF_MONITOR_TAB set attribute2='INTF_USER' ,attribute3='US RGI GL Manager',Attribute4='RGGL_DAILY_RATES_INT',Attribute10='REQUEST_SET' where interface_name='RATE_INTF_TR_EBS';
/
Commit;
/

-- Script Name : RGFAASSETKR_FNC.fnd
-- Purpose     : Following tasks are performed for report:
--               1. Creates function to load stage data for report.
-- Version     : 1.0

-- Copyright Riot Games  All Rights Reserved

-- MODIFICATION HISTORY
-- Person       Date       Comments
-- ---------    ------     ------------------------------------------
-- Srinivasa    12/29/2015 Created

CREATE OR REPLACE function RGFAASSETKR_FNC(P_Asset_Book in varchar2 ,P_Start_Period in varchar2,P_End_Period in varchar2,P_Rep_Type in varchar2,P_Rep_Type2 in varchar2) return varchar2 is 
PRAGMA AUTONOMOUS_TRANSACTION;
Begin
Delete from FA_BALANCES_REPORT_GT;
Delete from RGFA_BALANCES_REPORT_GT;
commit;
FA_FASCOSTD_XMLP_PKG.Insert_Info(P_Asset_Book,P_Start_Period,P_End_Period,P_Rep_Type,NULL);
Insert into RGFA_BALANCES_REPORT_GT select * from FA_BALANCES_REPORT_GT;
commit;
Delete from FA_BALANCES_REPORT_GT;
commit;
FA_FASCOSTD_XMLP_PKG.Insert_Info(P_Asset_Book,P_Start_Period,P_End_Period,P_Rep_Type2,NULL);
Return('Done');
End RGFAASSETKR_FNC;
/


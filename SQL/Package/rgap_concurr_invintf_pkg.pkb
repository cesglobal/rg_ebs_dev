CREATE OR REPLACE PACKAGE BODY rgap_concurr_invintf_pkg AS
 /* ------------------------------------------------------------------------------------------------------------------------------------------------------
 -- Author : Srinivasa
 -- Purpose : This Package is having Main Procedure for Validating the Data from Staging Table
 --
 -- Module : RGAP
 -- Interface Tables :
 -- Modification History :
 -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- Date Name Version Number Issue No 		 Revision 			Summary
 -- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 14-Aug-2015 Srinivasa 1.0
 -- 16-Feb-2016 Srinivasa 1.1 91 and 92 							Changed Procedure call for sendemail.
 -- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 12-May-2016 Prabhat				 1.2																				Re-arrange validation logic to display error message
 --																																				in proper order
 -- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 12-May-2016 Prabhat				 1.2																				1. Introduce oracle std sequence use to avoid sequence number for
 --																																			 invoice_id and invoice_line_id coming from ODS. This should also
 --																																				 be in line with coupa code
 -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 16-May-2016 Prabhat 1.3 1. Add logic to submit payable open interface by batch
 -- 2. Change site pick logic to exclude deactivated sites
 -- 3. introduce rollbak in interface table insert procedure to preven header insert
 -- without line or partial line insert.
 -- 4. put logic to delete orphan stage line records.
 -- 5. send completion email when there are no errors to notify
 --
 -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 29-Jun-2016 Srinivasa 1.4 1 50 Added logic to update interface stats into stage table i.e.RGINTF_RUN_REC_COUNTS

 -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
 -- Global Variables--
 g_program_name VARCHAR2 (30) := 'rgap_concurr_invintf_pkg';
 g_source VARCHAR2 (30) := 'CONCUR';
 g_vendor_site VARCHAR2 (30) := 'OFFICE';


 FUNCTION Chk_Inv_exists (p_invoice_num IN ap_invoices_interface.invoice_num%TYPE,
												 p_vend_num ap_invoices_interface.vendor_num%TYPE) RETURN NUMBER IS

 -- Variable
 -- -------------
 l_inv_count NUMBER := 0;
 l_module_name VARCHAR2 (40) := 'Chk_Inv_exists';

 BEGIN
 Fnd_File.put_line (Fnd_File.LOG, 'Inside chk_inv_exists');
 Fnd_File.put_line (Fnd_File.LOG, 'Invoice Number: ' || p_invoice_num);

 BEGIN
 SELECT COUNT (*)
 INTO l_inv_count
 FROM ap_invoices_interface
 WHERE invoice_num = p_invoice_num
 AND vendor_num = p_vend_num
 AND ROWNUM = 1;

 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 l_inv_count := -1;
 END;

 RETURN (l_inv_count);

 EXCEPTION
 WHEN OTHERS THEN
 l_inv_count := -1;
 RETURN (l_inv_count);

 END Chk_Inv_exists;

 --Waiting for a concurrent request
 -- ------------------------------------------
 FUNCTION wait_for_request (request_id IN NUMBER DEFAULT NULL,
 phase OUT NOCOPY VARCHAR2,
 status OUT NOCOPY VARCHAR2,
 dev_phase OUT NOCOPY VARCHAR2,
 dev_status OUT NOCOPY VARCHAR2,
 MESSAGE OUT NOCOPY VARCHAR2) RETURN BOOLEAN IS

 call_status BOOLEAN;
 req_phase VARCHAR2 (30);
 rid NUMBER := request_id;
 i NUMBER;
 l_module_name VARCHAR2 (50) := 'wait_for_request';

 BEGIN
 LOOP
 call_status := fnd_concurrent.get_request_status (rid,
 NULL,
 NULL,
 phase,
 status,
 req_phase,
 dev_status,
 MESSAGE);

 IF (req_phase = 'COMPLETE') THEN
 dev_phase := req_phase;
 RETURN (call_status);
 END IF;

 -- Version 1.1 Too many log messages during wait period
 /* rgcomn_debug_pkg.log_message (g_log_level,
 l_module_name,
 'WAIT_FOR_REQUEST'
 || 'WAITING FOR 10 more SECONDS'
 ); */
 END LOOP;

 EXCEPTION

 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'FND_CONCURRENT.WAIT_FOR_REQUEST'|| 'FUNCTION ERRORED AS '|| SQLCODE);
 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'FND_CONCURRENT.WAIT_FOR_REQUEST'|| 'FUNCTION ERRORED AS '|| SQLERRM);
 RETURN FALSE;

 END wait_for_request;


 PROCEDURE rgap_archive_records (p_request_id IN NUMBER) IS

 l_module_name VARCHAR2 (50) := 'rgap_archive_records';

 BEGIN
 
 Fnd_File.put_line (Fnd_File.LOG, '---------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting moving previousley processed records to archive table process');
 Fnd_File.put_line (Fnd_File.LOG, 'Inside rgap_archive_records');

 INSERT INTO rgap_crebs_inv_hdr_hist
 SELECT *
 FROM rgap_crebs_inv_hdr_stg
 WHERE processed_flag = 'P' 
 AND request_id != p_request_id;

 DELETE rgap_crebs_inv_hdr_stg
 WHERE processed_flag = 'P' 
 AND request_id != p_request_id;

 INSERT INTO rgap_crebs_inv_lines_hist
 SELECT *
 FROM rgap_crebs_inv_lines_stg
 WHERE processed_flag = 'P' 
 AND request_id != p_request_id;

 DELETE rgap_crebs_inv_lines_stg
 WHERE processed_flag = 'P' 
 AND request_id != p_request_id;


 COMMIT;

 EXCEPTION
 
 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Error executing archive code-' || SQLCODE);
 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Error executing archive code-' || SQLERRM);

 END rgap_archive_records;
 

-- ---------------------------------------------------------------------------------------------------------------------------------
-- Purpose: The Procedure rgap_invoice_val_prc will be used to validate and Load concurr data .
-- ---------------------------------------------------------------------------------------------------------------------------------
 PROCEDURE rgap_invoice_val_prc (p_request_id IN NUMBER) IS

 CURSOR c_invoice_headers_c1 IS
 SELECT xii.ROWID, xii.*
 FROM RGAP_CREBS_INV_HDR_STG xii
 WHERE PROCESSED_FLAG = 'N' 
 AND request_id = p_request_id;

 CURSOR c_invoice_lines_c1 (p_invoice_id NUMBER) IS
 SELECT xls.ROWID, xls.*
 FROM RGAP_CREBS_INV_LINES_STG xls
 WHERE invoice_id = p_invoice_id 
 AND PROCESSED_FLAG = 'N';

 -- Local Variables
 l_org_id hr_operating_units.organization_id%TYPE;
 v_status VARCHAR2 (20);
 v_status_line VARCHAR2 (20);
 l_err_msg VARCHAR2 (4000);
 l_err_msg_line VARCHAR2 (4000);
 l_err_mhdr VARCHAR2 (2000);
 l_err_mlin VARCHAR2 (2000);
 l_error_flag VARCHAR2 (1);
 l_error_flag_line VARCHAR2 (1);
 l_module_name VARCHAR2 (50) := 'rgap_invoice_val_prc';
 l_vendor_id NUMBER;
 --l_terms_id NUMBER;
 --l_invoice_cur_code VARCHAR2(5);
 l_vendor_site_code VARCHAR2 (50);
 --l_ship_to_location_id number;
 --l_vendorsite_id NUMBER;
 --l_term_id NUMBER;
 --l_vendor_name VARCHAR2 (100);
 --l_invoice_exist VARCHAR2 (25);
 --l_segment VARCHAR2 (100);
 --l_vendor_site_id NUMBER;
 l_ccid NUMBER;
 l_errors NUMBER;
 l_inv_curr_code ap_invoices_interface.invoice_currency_code%TYPE;
 l_pres NUMBER;

 BEGIN
 
 Fnd_File.put_line (Fnd_File.LOG, '---------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Invoice Validation process');
 Fnd_File.put_line (Fnd_File.LOG, 'Inside rgap_invoice_val_prc');
 Fnd_File.put_line (Fnd_File.LOG, 'Validation for Request Id: ' || p_request_id);
 Fnd_File.put_line (Fnd_File.LOG, 'pre validation Header validation status: ' || v_status);
 Fnd_File.put_line (Fnd_File.LOG, 'pre validation Line validation status: ' || v_status_line);

 -- Count Total No.of Records in the staging table --
 SELECT COUNT (1)
 INTO g_invrecords_processed
 FROM RGAP_CREBS_INV_HDR_STG
 WHERE status IS NULL 
 AND request_id = p_request_id;

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Number of invoices found for Import :'|| g_invrecords_processed);

 SELECT COUNT (1)
 INTO g_invlinerecords_processed
 FROM RGAP_CREBS_INV_LINES_STG rli
 WHERE EXISTS
 (SELECT 1
 FROM RGAP_CREBS_INV_HDR_STG RII
 WHERE rii.invoice_id = rli.invoice_id
 AND rii.status IS NULL
 AND rii.request_id = p_request_id);

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Number of invoice lines found for Import :'|| g_invlinerecords_processed);

 Fnd_File.put_line (Fnd_File.LOG, 'Total # of header records in stage table: '|| g_invrecords_processed);
 Fnd_File.put_line (Fnd_File.LOG, 'Total # of line records in stage table: '|| g_invlinerecords_processed);

 g_invrecords_processed := 0;
 g_invrecords_rejected := 0;
 g_invlinerecords_processed := 0;
 g_invlinerecords_rejected := 0;

 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Invoice header validation');

 FOR cur_master IN c_invoice_headers_c1
 LOOP

				 DELETE FROM RGAP_CREBS_INV_ERROR_STG
					 WHERE INVOICE_ID = cur_master.invoice_id
							 AND INVOICE_NUMBER = cur_master.invoice_num;

				 --Start of version 1.1
			 DELETE FROM rgap_crebs_inv_lines_stg rhs
					WHERE NVL (rhs.processed_flag, 'N') <> 'N'
						 AND EXISTS (SELECT 1
 FROM rgap_crebs_inv_lines_stg rci
								 WHERE rci.processed_flag = 'N'
										 AND rci.request_id IS NULL
										 AND rci.invoice_id = rhs.invoice_id
										 AND rci.invoice_line_id = rhs.invoice_line_id);

			 DELETE FROM rgap_crebs_inv_hdr_stg rhs
					WHERE NVL (rhs.processed_flag, 'N') <> 'N'
						 AND EXISTS (SELECT 1
 FROM rgap_crebs_inv_hdr_stg rci
								 WHERE rci.processed_flag = 'N'
										 AND rci.request_id IS NULL
										 AND rci.invoice_num = rhs.invoice_num);


			 DELETE FROM rgap_crebs_inv_lines_stg rls
					WHERE NVL (rls.processed_flag, 'N') NOT IN ('N', 'P')
						 AND INVOICE_ID IN (SELECT INVOICE_ID
 FROM rgap_crebs_inv_hdr_stg rhs
								 WHERE NVL (rhs.processed_flag, 'N') NOT IN ('N', 'P')
								         AND NOT EXISTS (SELECT 1 FROM RGINTF_RUN_REC_COUNTS WHERE  NVL(PROCESSED_FLAG,'N')='N' and QUEUE_ID=rhs.QUEUE_ID) --  Version 1.4 Changes
										 AND EXISTS (SELECT 1
 FROM ap_invoices_all a,
														 ap_invoice_lines_all b,
														 hr_operating_units hou
												 WHERE a.invoice_id = b.invoice_id
														 AND a.invoice_num = rhs.invoice_num
											 AND a.vendor_id = (SELECT vendor_id
 FROM po_vendors
												 WHERE segment1 = rhs.vendor_num)
														 AND a.org_id = hou.organization_id
														 AND hou.organization_id IN (SELECT lookup_code
 FROM fnd_lookup_values
																 WHERE lookup_type = 'RGAP_CONCR_IRELAND_HUB_OU')));


			 DELETE FROM rgap_crebs_inv_hdr_stg rhs
					WHERE NVL (rhs.processed_flag, 'N') NOT IN ('N', 'P')
               			AND NOT EXISTS (SELECT 1 FROM RGINTF_RUN_REC_COUNTS WHERE NVL(PROCESSED_FLAG,'N')='N' and QUEUE_ID=rhs.QUEUE_ID) --  Version 1.4 Changes
						 AND EXISTS (SELECT 1
 FROM ap_invoices_all a,
										 ap_invoice_lines_all b,
										 hr_operating_units hou
								 WHERE a.invoice_id = b.invoice_id
										 AND a.invoice_num = rhs.invoice_num
										 AND a.vendor_id = (SELECT vendor_id
 FROM po_vendors
												 WHERE segment1 = rhs.vendor_num)
										 AND a.org_id = hou.organization_id
										 AND hou.organization_id IN (SELECT lookup_code
 FROM fnd_lookup_values
												 WHERE lookup_type = 'RGAP_CONCR_IRELAND_HUB_OU'));

				 DELETE FROM RGAP_CREBS_INV_ERROR_STG RES
					 WHERE EXISTS (SELECT 1
 FROM ap_invoices_all aia
								 WHERE aia.source LIKE 'CONCUR'
									 AND aia.invoice_num = RES.invoice_number) OR
							 NOT EXISTS (SELECT 1
								 FROM rgap_crebs_inv_hdr_stg aia
								 WHERE aia.invoice_id = RES.invoice_id);

				 --End of version 1.1

		 -- new delete to remove orphan line records from stage line table
 -- ----------------------------------------------------------------------------------
 delete from rgap_crebs_inv_lines_stg rls
 where nvl (rls.processed_flag, 'N') not in ('N', 'P')
 and rls.invoice_id not in (select invoice_id
 from rgap_crebs_inv_hdr_stg rhs
 where nvl (rhs.processed_flag, 'N') not in ('N', 'P'));


 COMMIT;

 l_org_id := NULL;

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Invoices ****** :' || cur_master.invoice_num);

 l_err_msg := '';
 l_error_flag := 'N';
 v_status := ' ';

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' ------ INVOICE VALIDATION STARTS ------');
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Before Invoice Number Validation');

 -- invoice number Validation
 IF cur_master.invoice_num IS NULL THEN

 l_err_msg := l_err_msg|| ' - '|| 'invoice_number cannot be NULL'|| ' invoice_number - '|| cur_master.invoice_num;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := 'Invoice number can not be null';
 Fnd_File.put_line (Fnd_File.LOG, 'Invoice number can not be null');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => l_err_msg);

 END IF;

 -- Supplier Number Validation
 IF cur_master.vendor_num IS NULL THEN

 l_err_msg := l_err_msg|| ' - '|| 'SUPPLIER Number cannot be NULL '|| ' Supplier Number - '|| cur_master.vendor_num;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr || '-' || 'Vendor number can not be null';
 Fnd_File.put_line (Fnd_File.LOG, 'Vendor number can not be null');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => l_err_msg);

 END IF;

 -- Supplier Site Code Validation
 IF cur_master.vendor_site_code IS NULL THEN

 l_err_msg := l_err_msg|| ' - '|| 'SUPPLIER Site Code cannot be NULL '|| ' Supplier Site Code - '|| cur_master.vendor_site_code;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr || '-' || 'Vendor site code can not be null';
 Fnd_File.put_line (Fnd_File.LOG, 'Vendor site code can not be null');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => l_err_msg);

 END IF;

 -- Invoice Type Validation
 IF cur_master.invoice_type_lookup_code IS NULL THEN

 l_err_msg := l_err_msg|| ' - '|| 'Invoice Type Code cannot be NULL '|| ' Invoice Type Code - '|| cur_master.invoice_type_lookup_code;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr|| '-'|| 'Invoice type lookup code can not be null';
 Fnd_File.put_line (Fnd_File.LOG, 'Invoice type lookup code can not be null');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => l_err_msg);

 END IF;

 -- Vendor Number Validation
 IF cur_master.vendor_num IS NOT NULL THEN

 BEGIN
 SELECT vendor_id
 INTO l_vendor_id
 FROM ap_suppliers
 WHERE SYSDATE BETWEEN START_DATE_ACTIVE
 AND NVL (END_DATE_ACTIVE, SYSDATE)
 AND LTRIM (RTRIM (UPPER (segment1))) = LTRIM (RTRIM (UPPER (cur_master.VENDOR_NUM)));

 Fnd_File.put_line (Fnd_File.LOG, '--------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Vendor id: ' || l_vendor_id);

 EXCEPTION

 WHEN NO_DATA_FOUND THEN
 l_err_msg := l_err_msg|| ' - '|| 'No Data Found for vendor number '|| cur_master.VENDOR_NUM;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr || '-' || 'Could not derive Vendor id';
 Fnd_File.put_line (Fnd_File.LOG, 'Vendor id cannot be derived.');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => l_err_msg);

 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, 
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => ' VENDOR_NUM does not exists '|| ' VENDOR Number Segment1 column - '|| cur_master.VENDOR_NUM|| '-'|| SQLCODE|| ' '|| SQLERRM);

 l_err_mhdr := l_err_mhdr || '-' || 'Error while deriving vendor Id';
 Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving vendor Id');

 END;
 END IF;

 -- Vendor Site Validation
 IF cur_master.vendor_site_code IS NOT NULL THEN

 BEGIN
 SELECT vendor_site_code,
 org_id,
 invoice_currency_code
 INTO l_vendor_site_code,
 l_org_id,
 l_inv_curr_code
 FROM ap_supplier_sites_all
 WHERE vendor_id = l_vendor_id
 AND EXISTS (SELECT 1
												FROM fnd_lookup_values
												WHERE lookup_type = 'RGAP_CONCR_IRELAND_HUB_OU'
												AND lookup_code = org_id)
					 and inactive_date is null
 AND vendor_site_code = g_vendor_site
 AND ROWNUM = 1;

 Fnd_File.put_line (Fnd_File.LOG, 'Vendor site code :' || l_vendor_site_code);

 EXCEPTION

 WHEN NO_DATA_FOUND THEN

 l_err_msg :=l_err_msg|| ' - '|| 'No Data Found for vendor Site '|| cur_master.vendor_site_code;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr|| '-'|| 'Could not derive Vendor site code and OU id';

 Fnd_File.put_line (Fnd_File.LOG, 'Could not derive Vendor site code and OU id');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => l_err_msg);

 WHEN OTHERS THEN

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => ' Vendor Site does not exists '|| ' VENDOR Site validation column - '|| cur_master.vendor_site_code|| '-'|| SQLCODE|| ' '|| SQLERRM);

 l_err_mhdr := l_err_mhdr|| '-'|| 'Error while deriving Vendor site code and OU id.';
 Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving Vendor site code and OU id.');

 END;

 END IF;


 -- Currency precision
 BEGIN
 SELECT NVL (precision, 2)
 INTO l_pres
 FROM fnd_currencies
 WHERE currency_code = l_inv_curr_code AND ROWNUM = 1;

 EXCEPTION

 WHEN NO_DATA_FOUND THEN

 l_err_msg := l_err_msg|| ' - '|| 'No precision value found for currency code '|| l_inv_curr_code;
 l_pres := 2;

 WHEN OTHERS THEN
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => ' Error while deriving currency precision for currency code: '|| l_inv_curr_code|| '-'|| SQLCODE|| ' '|| SQLERRM);

 l_err_mhdr := l_err_mhdr || 'Error while deriving currency precision.';
 Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving currency precision.');

 END;


 /*------------------- Org_Id derivation------------
 BEGIN
 SELECT org_id
 into v_org_id
 FROM ap.ap_suppliers asa,
 ap.ap_supplier_sites_all ass
 WHERE asa.vendor_id = ass.vendor_id
 AND asa.segment1 = cur_master.VENDOR_NUM
 AND NVL(cur_master.vendor_site_code,vendor_site_code)=vendor_site_code
 AND NVL(ASS.INACTIVE_DATE,sysdate+1) > sysdate
 AND ROWNUM=1;

 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 l_err_msg :=
 l_err_msg
 || ' - '
 || 'No Data Found for Org_id '
 || cur_master.vendor_site_code;
 l_error_flag := 'Y';
 v_status := 'E';

 l_err_mhdr := l_err_mhdr||'Could not derive org id.';
 Fnd_File.put_line(Fnd_File.log,'Could not derive org id.');

 rgcomn_debug_pkg.
 log_message (
 P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => l_err_msg);

 WHEN OTHERS THEN
 rgcomn_debug_pkg.
 log_message (
 P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => ' Org_id does not exists '
 || ' Org_id validation column - '
 || cur_master.vendor_site_code
 || '-'
 || SQLCODE
 || ' '
 || SQLERRM);

 l_err_mhdr := l_err_mhdr||'Error while deriving Org Id.';
 Fnd_File.put_line(Fnd_File.log,'Error while deriving Org Id.');

 END;
 */

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => 'Before Invoice Line Validation');

 Fnd_File.put_line (Fnd_File.LOG, '----------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Invoice Line Validation');

 FOR inv_line IN c_invoice_lines_c1 (cur_master.INVOICE_ID)
 LOOP

 l_err_msg_line := '';
 l_err_mlin := NULL;
 l_error_flag_line := 'N';
 v_status_line := ' ';
 l_ccid := '';

 /*------------------- Invoice Line Number Validation ------------*/
 IF inv_line.Line_Number IS NULL THEN
 
 l_err_msg_line := l_err_msg_line|| ' - '|| 'Invoice Line Number cannot be NULL '|| ' Invoice Line Number - '|| inv_line.Line_Number;
 l_error_flag_line := 'Y';
 v_status := 'E';

 l_err_mlin := l_err_mlin || '-' || 'Line number can not be null.';

 Fnd_File.put_line (Fnd_File.LOG, 'Line number can not be null.');
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => l_err_msg_line);

 END IF;

 -- Invoice Line Type Validation
 IF inv_line.Line_Type_lookup_code IS NULL THEN

 l_err_msg_line := l_err_msg_line || ' - '|| 'Invoice Line Number cannot be NULL '|| ' Invoice Line Number - '|| inv_line.Line_Type_lookup_code;
 l_error_flag_line := 'Y';
 v_status := 'E';

 l_err_mlin := l_err_mlin|| '-'|| 'Line type lookup code can not be null';

 Fnd_File.put_line (Fnd_File.LOG, 'Line type lookup code can not be null');

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
 P_PROGRAM_NAME => l_module_name,
 P_MESSAGE => l_err_msg_line);
 END IF;

 /*------------------- Invoice Line Type Validation ------------
 IF inv_line.DIST_CODE_CONCATENATED IS NULL AND inv_line.DIST_CODE_COMBINATION_ID IS NULL
 THEN
 l_err_msg_line :=
 l_err_msg_line
 || ' - '
 || 'Invoice Line DIST_CODE_CONCATENATED cannot be NULL '
 || ' Invoice Line DIST_CODE_CONCATENATED - '
 || inv_line.DIST_CODE_CONCATENATED;
 l_error_flag_line := 'Y';
 v_status := 'E';
 rgcomn_debug_pkg.
 log_message (
 P_LEVEL => g_log_level,
 P_PROGRAM_NAME => 'rgap_concurr_invintf_pkg.rgap_invoice_val_prc',
 P_MESSAGE => l_err_msg_line);
 END IF; */

 Fnd_File.put_line (Fnd_File.LOG, 'post validation Header validation status: ' || v_status);
 Fnd_File.put_line (Fnd_File.LOG, 'post validation Line validation status: ' || v_status_line);

 IF v_status_line = 'E' OR v_status = 'E' THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Header or Line validation status was in error, updating stage table status as E');

 UPDATE RGAP_CREBS_INV_LINES_STG
 SET PROCESSED_FLAG = 'E',
 err_msg = l_err_mlin
 WHERE INVOICE_ID = cur_master.invoice_id
 AND INVOICE_LINE_ID = inv_line.INVOICE_LINE_ID
 AND LINE_NUMBER = inv_line.LINE_Number
 AND request_id = p_request_id;

 INSERT INTO RGAP_CREBS_INV_ERROR_STG (INVOICE_ID,
 invoice_number,
 INVOICE_LINE_NUM,
 ERROR_DESCRIPTION,
 CREATION_DATE)
 VALUES (cur_master.invoice_id,
 cur_master.invoice_num,
 inv_line.LINE_NUMber,
 NVL (l_err_msg_line, 'Header Record has errors'),
 SYSDATE);

 COMMIT;

 ELSE

 Fnd_File.put_line (Fnd_File.LOG, 'No Header or Line validation error, Updating inv line id '||inv_line.INVOICE_LINE_ID|| ' as P');

 UPDATE RGAP_CREBS_INV_LINES_STG
 SET PROCESSED_FLAG = 'P', org_id = l_org_id
 WHERE INVOICE_ID = cur_master.invoice_id
 AND INVOICE_LINE_ID = inv_line.INVOICE_LINE_ID
 AND LINE_NUMBER = inv_line.LINE_Number
 AND request_id = p_request_id;

 COMMIT;
 END IF;
 END LOOP;



 IF v_status = 'E' OR v_status_line = 'E' THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Header validation status: ' || v_status);
 Fnd_File. put_line (Fnd_File.LOG, 'Line validation status: ' || v_status_line);
 Fnd_File.put_line (Fnd_File.LOG, 'Starting exception/error record insertion into error table');

 UPDATE RGAP_CREBS_INV_HDR_STG
 SET PROCESSED_FLAG = 'E',
 err_msg = l_err_mhdr
 WHERE INVOICE_ID = cur_master.invoice_id
 AND invoice_num = cur_master.invoice_num
 AND request_id = p_request_id;

 INSERT INTO RGAP_CREBS_INV_ERROR_STG (INVOICE_ID,
 invoice_number,
 ERROR_DESCRIPTION,
 CREATION_DATE)
 VALUES (cur_master.invoice_id,
								cur_master.invoice_num,
								NVL (l_err_msg,
 'Lines Has Errors'),
								SYSDATE);

 COMMIT;

 ELSE

 Fnd_File.put_line (Fnd_File.LOG, 'No errors updating stage header table.');

 UPDATE RGAP_CREBS_INV_HDR_STG
 SET PROCESSED_FLAG = 'P',
					 vendor_id = l_vendor_id,
					 vendor_site_code = l_vendor_site_code,
 invoice_currency_code = l_inv_curr_code,
 org_id = l_org_id
 WHERE INVOICE_ID = cur_master.invoice_id
 AND invoice_num = cur_master.invoice_num
 AND request_id = p_request_id;

 COMMIT;

 END IF;
 END LOOP;

 Fnd_File.put_line (Fnd_File.LOG, 'Checking previous error count in error table.');

 SELECT COUNT (1)
 INTO l_errors
 FROM RGAP_CREBS_INV_ERROR_STG;

 Fnd_File.put_line (Fnd_File.LOG, 'Total error count in error table: ' || l_errors);

 IF l_errors > 0 THEN

 Fnd_File.put_line (Fnd_File.output, 'Error found sending email.');
 Fnd_File.put_line (Fnd_File.LOG, 'Error found sending email.');

 rgap_common_utility_pkg.rgap_send_email (p_dest => g_Interface_Email,
 p_subject => g_stg_subject,
 p_msg => rgap_common_utility_pkg.rgap_get_html_report ('select distinct VENDOR_NUM,INVOICE_NUMBER,INVOICE_AMOUNT,ERROR_DESCRIPTION, RE.CREATION_DATE
 from RGAP_CREBS_INV_ERROR_STG RE,
 RGAP_CREBS_INV_HDR_STG RH
 where RH.invoice_id = RE.Invoice_id
 AND ERROR_DESCRIPTION not like ''Header Record has errors'' order by 1 asc'),
 p_msg2 => g_concur_msg2,
 p_msg3 => g_concur_msg3,
 p_mailhost => g_mailhost,
 p_port => g_port,
 p_sender => g_sender);

 END IF;

 Fnd_File.put_line (Fnd_File.LOG, 'Validation process Complete. Returning to main process.');
 Fnd_File.put_line (Fnd_File.LOG, '--------------------------------------------------');

 END rgap_invoice_val_prc;


 --Load data form staging tables tom Interaface tables
 -- ---------------------------------------------------------------------
 -- procedure updated with a rollback to not insert header if
 -- there is error while inserting the lines. and also not insert partial
 -- lines if there is error in one of the lines.

 PROCEDURE load_intf_data (p_request_id NUMBER) IS

 --v_invoice_exists NUMBER;
 l_module_name VARCHAR2 (100) := 'load_intf_data';
 l_err_flag VARCHAR2 (10);
 l_pres NUMBER;

 linv_line_insert_err exception;

 CURSOR cur_load_hdr IS
 SELECT h.ROWID hrow_id,
 h.*
 FROM rgap_crebs_inv_hdr_stg h
			WHERE h.processed_flag = 'P'
			AND h.request_id = p_request_id
 AND NOT EXISTS
 (SELECT 1
 FROM rgap_crebs_inv_lines_stg l
								WHERE h.invoice_id = l.invoice_id
								AND l.processed_flag = 'E'
								AND l.request_id = p_request_id);

 CURSOR cur_load_line (p_invoice_id number) IS
 SELECT l.ROWID lrow_id, l.*
 FROM rgap_crebs_inv_lines_stg l
			WHERE l.invoice_id = p_invoice_id
 AND l.processed_flag = 'P'
 AND l.request_id = p_request_id;

 BEGIN

 Fnd_File.put_line (Fnd_File.output, 'Starting Procedure to insert data into open interface table from stage table');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Procedure (load_intf_data) to insert data into open interface table from stage table');

 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Load Data Start of Stage table data into Oracle Interface Tables');

 FOR hdr_rec IN cur_load_hdr
 LOOP
 begin
 
 /*
 Fnd_File.put_line (Fnd_File.LOG, 'Checking if invoice#: '||hdr_rec.invoice_num||' already in open interface table. Calling chk_inv_exists Function');

 v_invoice_exists := chk_inv_exists (p_invoice_num => hdr_rec.invoice_num,
 p_vend_num => hdr_rec.vendor_num);

 Fnd_File.put_line (Fnd_File.LOG, 'Invoice count in open Interface table: ' || v_invoice_exists);
 */

 l_err_flag := 'N';
 
 /*
 IF v_invoice_exists > 0 THEN

 rgcomn_debug_pkg.log_message (g_log_level,g_program_name || '.' || l_module_name, hdr_rec.invoice_num|| '-'|| hdr_rec.vendor_num|| 'Invoice Already Exists in Invoice Interface Table');

 Fnd_File.put_line (Fnd_File.output, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| hdr_rec.invoice_num);
 Fnd_File.put_line (Fnd_File.LOG, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| hdr_rec.invoice_num);

 -- write stage table update here
 UPDATE RGAP_CREBS_INV_HDR_STG
 SET PROCESSED_FLAG = 'E',
 err_msg = 'Invoice with same number for this supplier already exists in Invoice Interface Table'
 WHERE INVOICE_ID = hdr_rec.invoice_id
 AND invoice_num = hdr_rec.invoice_num
 AND request_id = p_request_id;
 */

 --ELSE

 -- Currency precision
 BEGIN
 SELECT NVL (precision, 2)
 INTO l_pres
 FROM fnd_currencies
 WHERE currency_code = hdr_rec.invoice_currency_code
 AND ROWNUM = 1;

 EXCEPTION

 WHEN NO_DATA_FOUND THEN
 
 l_pres := 2;
 
 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Error while deriving currency precision for currency code: '|| hdr_rec.invoice_currency_code|| '-'|| SQLCODE|| ' '|| SQLERRM); 
 Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving currency precision.');

 END;


 Fnd_File.put_line (Fnd_File.LOG, 'Inserting Invoice#: '||hdr_rec.invoice_num||' in header Interface table.');
 savepoint inv_insert;

 BEGIN
 INSERT INTO ap_invoices_interface (org_id,
 invoice_id,
 invoice_num,
 invoice_type_lookup_code,
 invoice_currency_code,
 invoice_date,
 vendor_num,
 vendor_site_code,
 invoice_amount,
 description,
 source,
 GROUP_ID,
 creation_date,
 created_by,
 last_update_date,
 last_updated_by)
 VALUES (hdr_rec.org_id,
 ap_invoices_interface_s.NEXTVAL, --hdr_rec.invoice_id,
 hdr_rec.invoice_num,
 hdr_rec.invoice_type_lookup_code,
 hdr_rec.invoice_currency_code,
 hdr_rec.invoice_date,
 hdr_rec.vendor_num,
 hdr_rec.vendor_site_code,
 ROUND (hdr_rec.invoice_amount, l_pres),
 hdr_rec.description,
 hdr_rec.source,
 hdr_rec.GROUP_ID,
 SYSDATE,
 DECODE (fnd_global.user_id, -1, g_user_id, fnd_global.user_id),
 SYSDATE,
 DECODE (fnd_global.user_id, -1, g_user_id, fnd_global.user_id));

 EXCEPTION

 WHEN OTHERS THEN

 l_err_flag := 'Y';

 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, hdr_rec.invoice_num|| '-'|| hdr_rec.vendor_num|| 'Exception'|| SUBSTR (SQLERRM, 1, 40)|| 'Interface Header records not inserted');
 Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface header. Invoice num: '|| hdr_rec.invoice_num);
 Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface header.: '|| SQLERRM);

 UPDATE rgap_crebs_inv_hdr_stg
 SET processed_flag = 'E',
 err_msg = 'Error while Inserting Invoice in open Interface header.'
 WHERE processed_flag = 'P'
 AND invoice_id = hdr_rec.invoice_id
 AND request_id = p_request_id;

 END;


 IF NVL (l_err_flag, 'N') <> 'Y' THEN

 Fnd_File.put_line(Fnd_File.LOG, 'Starting record insertion into Open Interface Line table');
 Fnd_File.put_line(Fnd_File.LOG, 'Inserting Invoice#: '||hdr_rec.invoice_num||' lines in open Interface table.');

 FOR line_rec IN cur_load_line (hdr_rec.invoice_id)
 LOOP
 BEGIN

					 Fnd_File.put_line(Fnd_File.LOG, 'Invocie_line_id and num: '||line_rec.invoice_line_id||' and '||line_rec.line_number);

 INSERT INTO ap_invoice_lines_interface (invoice_id,
 invoice_line_id,
 line_number,
 line_type_lookup_code,
 amount,
 description,
 dist_code_concatenated,
 creation_date,
 created_by,
 last_update_date,
 last_updated_by)
 VALUES (ap_invoices_interface_s.CURRVAL, --hdr_rec.invoice_id,
 ap_invoice_lines_interface_s.NEXTVAL, --line_rec.invoice_line_id,
 line_rec.line_number,
 line_rec.line_type_lookup_code,
 ROUND (line_rec.amount, l_pres),
 line_rec.description,
 line_rec.dist_code_concatenated,
 SYSDATE,
 DECODE (fnd_global.user_id, -1, g_user_id, fnd_global.user_id),
 SYSDATE,
 DECODE (fnd_global.user_id, -1, g_user_id, fnd_global.user_id));

 --COMMIT;

 EXCEPTION
 WHEN OTHERS THEN

 rgcomn_debug_pkg.log_message(g_log_level, l_module_name, hdr_rec.invoice_num|| '-'|| line_rec.line_number|| 'Exception'|| SUBSTR (SQLERRM, 1, 40)|| 'Interface line records not inserted');

 Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice num: '|| hdr_rec.invoice_num||' and line num: '||line_rec.line_number||' in open Interface line table.');
 Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface line table.: '|| SQLCODE);
 Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface line table.: '|| SQLERRM);

 raise linv_line_insert_err;

 /*
 UPDATE rgap_crebs_inv_lines_stg
 SET processed_flag = 'E',
 err_msg = 'Error while Inserting Invoice in open Interface line.'
 WHERE processed_flag = 'P'
 AND invoice_id = hdr_rec.invoice_id
 AND request_id = p_request_id;

 UPDATE rgap_crebs_inv_hdr_stg
 SET processed_flag = 'E',
 err_msg = 'Error while Inserting Invoice in open Interface line.'
 WHERE processed_flag = 'P'
 AND invoice_id = hdr_rec.invoice_id
 AND request_id = p_request_id;

 rollback to inv_insert;
 */

 END;
 END LOOP;

 else

 rollback to inv_insert;

 END IF;

 --END IF;

 exception
 when linv_line_insert_err then

 rollback to inv_insert;

 UPDATE rgap_crebs_inv_lines_stg
 SET processed_flag = 'E',
 err_msg = 'Error while Inserting Invoice in open Interface line.'
 WHERE processed_flag = 'P'
 AND invoice_id = hdr_rec.invoice_id
 AND request_id = p_request_id;

 UPDATE rgap_crebs_inv_hdr_stg
 SET processed_flag = 'E',
 err_msg = 'Error while Inserting Invoice in open Interface line.'
 WHERE processed_flag = 'P'
 AND invoice_id = hdr_rec.invoice_id
 AND request_id = p_request_id;

 end;

 END LOOP;
 COMMIT;

 -- Calling Standard Payables interface program

 --rgap_load_ivoices(p_request_id);

 EXCEPTION

 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || 'load_intf_data', 'Load stage to interface table program errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);
 Fnd_File.put_line (Fnd_File.LOG, 'Error in load_intf_data procedure : ' || SQLCODE);
 Fnd_File.put_line (Fnd_File.LOG, 'Error in load_intf_data procedure: ' || SQLERRM);

 END load_intf_data;



 PROCEDURE rgap_concinv_main_prc (errbuf OUT VARCHAR2,
																		retcode OUT VARCHAR2) IS


 l_module_name VARCHAR2 (50) := 'rgap_concinv_main_prc';
 l_request_id NUMBER := fnd_global.conc_request_id;
 l_errors NUMBER;
 l_phase VARCHAR2 (120);
 l_status VARCHAR2 (120);
 l_dev_phase VARCHAR2 (120);
 l_dev_status VARCHAR2 (120);
 l_message VARCHAR2 (200);
 l_hdr_inv_recs NUMBER := 0;
 l_line_count NUMBER := 0;
 l_validated_count NUMBER := 0;
 l_intf_errs NUMBER := 0;
 l_req_status BOOLEAN;
 l_phase_code VARCHAR2 (50);
 l_status_code VARCHAR2 (50);
 l_prev_rec_cnt NUMBER := 0;
 l_batch_id VARCHAR2 (80);
 l_stg_error NUMBER;
 l_orcl_error NUMBER;
 l_inv_exists NUMBER;
 l_boolean    BOOLEAN;


 CURSOR line_chk_cur (p_request_id NUMBER) IS
 SELECT COUNT (*),
 invoice_id,
 invoice_num
					FROM rgap_crebs_inv_hdr_stg
					WHERE processed_flag = 'N'
					AND request_id = p_request_id
					GROUP BY invoice_id, invoice_num;
					
	 CURSOR hdr_cur (p_request_id NUMBER) IS
 SELECT h.ROWID hrow_id,
 h.*
 FROM rgap_crebs_inv_hdr_stg h
			WHERE h.processed_flag = 'N'
			AND h.request_id = p_request_id
 AND NOT EXISTS
 (SELECT 1
 FROM rgap_crebs_inv_lines_stg l
								WHERE h.invoice_id = l.invoice_id
								AND l.processed_flag = 'E'
								AND l.request_id = p_request_id);


 cursor grp_id_cur (p_request_id number) is
 select group_id
 from rgap_crebs_inv_hdr_stg h
 where h.processed_flag = 'P'
 and h.request_id = p_request_id
 group by group_id;

 BEGIN
 Fnd_File.put_line (Fnd_File.output, '***********************************************************');
 Fnd_File.put_line (Fnd_File.output, 'Starting Concur Invoice Import and Auto Invoice Creation Process');
 Fnd_File.put_line (Fnd_File.LOG, '**************************************************************');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Concur Invoice Import and Auto Invoice Creation Process');

 Fnd_File.put_line (Fnd_File.output, 'Process Reqest Id: ' || l_request_id);
 Fnd_File.put_line (Fnd_File.LOG, 'Process Reqest Id:' || l_request_id);

 rgcomn_debug_pkg.log_cp_message (P_LEVEL => g_log_level, P_MESSAGE => '******************** Process Starts :'|| TO_CHAR (SYSDATE, 'DD:MON:YYYY HH:MI:SS')|| '******************** ');

 -- move previous processed record to archive after each run
 SELECT COUNT (*)
 INTO l_prev_rec_cnt
 FROM rgap_crebs_inv_hdr_stg
 WHERE processed_flag = 'P' 
 AND request_id != l_request_id;

 IF NVL (l_prev_rec_cnt, 0) > 0 THEN

 Fnd_File.put_line (Fnd_File.output, 'Calling process to archive previous processed stage records.');
 Fnd_File.put_line (Fnd_File.LOG, 'Calling process to archive previous processed stage records.');
 Fnd_File.put_line (Fnd_File.LOG, 'Prev record to be archived: '|| l_prev_rec_cnt);

 rgap_archive_records (l_request_id);

 ELSE

 Fnd_File.put_line (Fnd_File.output, 'No previous processed record to archive.');
 Fnd_File.put_line (Fnd_File.LOG, 'No previous processed record to archive.');

 END IF;


 --Count Total No.of Records in the staging table
 -- -----------------------------------------------------------
 SELECT COUNT (*)
 INTO l_hdr_inv_recs
 FROM rgap_crebs_inv_hdr_stg
 WHERE processed_flag = 'N' 
 AND request_id IS NULL;

 Fnd_File.put_line (Fnd_File.output, '---------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.output, 'Total Invoice header records in stage table: ' || l_hdr_inv_recs);
 Fnd_File.put_line (Fnd_File.LOG, '---------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Total Invoice header records in stage table: ' || l_hdr_inv_recs);

 -- new records in stage table
 IF NVL (l_hdr_inv_recs, 0) > 0 THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Updating header stage records with request id');

 UPDATE rgap_crebs_inv_hdr_stg
 SET request_id = l_request_id
 WHERE processed_flag = 'N' AND request_id IS NULL;

 Fnd_File.put_line (Fnd_File.LOG, 'Updating line stage records with request id');

 UPDATE rgap_crebs_inv_lines_stg
 SET request_id = l_request_id
 WHERE processed_flag = 'N' AND request_id IS NULL;
 
 -- Version 1.4 changes Start
 Fnd_File.put_line (Fnd_File.LOG, 'Updating rgintf_run_rec_counts table with request id');

 update rgintf_run_rec_counts rrc set PROCESSED_FLAG='P' where request_id is not null;
 
 update rgintf_run_rec_counts rrc
 set request_id = l_request_id, 
 rrc.ebs_stg_cnt = (select count (distinct invoice_id)
 from rgap_crebs_inv_hdr_stg rcih
 where processed_flag = 'N'
 and request_id = l_request_id
 and rrc.interface_id = rcih.interface_id
 and rrc.queue_id = rcih.queue_id)
 Where request_id is null ;
 -- Version 1.4 changes End


 /*
 Fnd_File.put_line (Fnd_File.LOG, 'Deriving Batch Id to be passed to Payabale Open Interface');

 BEGIN
 SELECT DISTINCT GROUP_ID
 INTO l_batch_id
 FROM rgap_crebs_inv_hdr_stg h
 WHERE h.processed_flag = 'N'
 AND h.request_id = l_request_id
 AND ROWNUM = 1;

 Fnd_File.put_line (Fnd_File.LOG, 'Derived Batch Id is: ' || l_batch_id);

 EXCEPTION
 WHEN NO_DATA_FOUND
 THEN
 l_batch_id :=
 g_source
 || '-'
 || TO_CHAR (SYSDATE, 'DDMONYY')
 || '-'
 || l_request_id;
 Fnd_File.
 put_line (
 Fnd_File.LOG,
 'Could not derived Batch Id from stage table setting to batch id: '
 || l_batch_id);
 END;
 */

 Fnd_File.put_line(Fnd_File.LOG, 'Checking if any of the header record is without lines in stage table');

 -- Verify if there is a header record in stage table without line
 FOR line_chk_rec IN line_chk_cur (l_request_id)
 LOOP
 
 BEGIN
 SELECT COUNT (*)
 INTO l_line_count
 FROM rgap_crebs_inv_lines_stg ls
 WHERE ls.invoice_id = line_chk_rec.invoice_id
 AND processed_flag = 'N'
 AND request_id = l_request_id;

 Fnd_File.put_line(Fnd_File.LOG, 'Line count is: '||l_line_count||' for invoice_id: '||line_chk_rec.invoice_id||' '||chr(38)||' invoice num: '||line_chk_rec.invoice_num);

 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 l_line_count := 0;
 END;

 IF l_line_count = 0 THEN

 Fnd_File.put_line(Fnd_File.LOG, 'No lines in stage table for invoice num: '||line_chk_rec.invoice_num||' Updating stage record as error');

 UPDATE rgap_crebs_inv_hdr_stg
 SET processed_flag = 'E',
 err_msg = 'This Invoice has no line in lines table, can not be processed'
						WHERE invoice_num = line_chk_rec.invoice_num
						AND processed_flag = 'N'
						AND request_id IS NULL;
 END IF;

 END LOOP;
 
 Fnd_File.put_line(Fnd_File.LOG, 'Checking if invoice already in interface table.');
 
 for shdr_rec IN hdr_cur(l_request_id)
 loop
 begin

 Fnd_File.put_line (Fnd_File.LOG, 'Checking if invoice#: '||shdr_rec.invoice_num||' already in open interface table. Calling chk_inv_exists Function');

 l_inv_exists := chk_inv_exists (p_invoice_num => shdr_rec.invoice_num,
 p_vend_num => shdr_rec.vendor_num);

 Fnd_File.put_line (Fnd_File.LOG, 'Invoice count in open Interface table: ' || l_inv_exists);


 IF l_inv_exists > 0 THEN

 rgcomn_debug_pkg.log_message (g_log_level,g_program_name || '.' || l_module_name, shdr_rec.invoice_num|| '-'|| shdr_rec.vendor_num|| 'Invoice Already Exists in Invoice Interface Table');

 Fnd_File.put_line (Fnd_File.output, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| shdr_rec.invoice_num);
 Fnd_File.put_line (Fnd_File.LOG, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| shdr_rec.invoice_num);

 -- write stage table update here
 update rgap_crebs_inv_hdr_stg
 set processed_flag = 'E',
 err_msg = 'Invoice with same number for this supplier already exists in Invoice Interface Table'
 where invoice_id = shdr_rec.invoice_id
 and invoice_num = shdr_rec.invoice_num
 and request_id = l_request_id;
 
 end if;
 end;
 
 end loop; 

 Fnd_File.put_line (Fnd_File.LOG, 'End of pre-validation process of stage header and line records');


 --- Calling validation Procedure
 -- --------------------------------------
 BEGIN

 Fnd_File.put_line (Fnd_File.output, '----------------------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.log, '----------------------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.output, 'Calling process to validate data in stage table before insert to open interface table');
 Fnd_File.put_line (Fnd_File.LOG, 'Calling process to validate data in stage table before insert to open interface table');
 Fnd_File.put_line (Fnd_File.LOG, 'Calling validate_stage_recs Procedure...');

 rgap_invoice_val_prc (l_request_id);

 Fnd_File.put_line (Fnd_File.LOG, 'Back from rgap_invoice_val_prc. Procedure completed.');

 EXCEPTION
 
 WHEN OTHERS THEN
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => 'APPS.rgap_concurr_invintf_pkg.rgap_concinv_main_prc', P_MESSAGE => ' Exception at Calling Validation Procedure '|| '-'|| SQLCODE|| ' '|| SQLERRM);
 rgcomn_debug_pkg.log_cp_message (P_LEVEL => g_log_level, P_MESSAGE => ' Exception at Calling Validation Procedure '|| '-'|| SQLCODE|| ' '|| SQLERRM);
 
 END;

 -- new record not in stage table
 ELSE

 rgcomn_debug_pkg.log_message (p_level => g_log_level, p_program_name => l_module_name, p_message => 'There is no new record in Header and line stage tables to process.');

 Fnd_File.put_line (Fnd_File.Output, 'There is no new record in Header and line stage tables to process.');
 Fnd_File.put_line (Fnd_File.LOG, 'There is no new record in Header and line stage tables to process. sending completion email.');

 rgap_common_utility_pkg.rgap_send_email (p_dest => g_interface_email,
 p_subject => g_stg_nd_subject,
 p_msg => g_nd_msg2,
 p_msg2 => g_nd_msg1,
 p_msg3 => null,
 p_mailhost => g_mailhost,
 p_port => g_port,
 p_sender => g_sender);

 END IF;
 
 -- Version 1.4 changes Start
 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.output, 'Updating RGINTF_RUN_REC_COUNTS table with number of errored invoices in Stage table.');
 Fnd_File.put_line (Fnd_File.LOG, 'Updating RGINTF_RUN_REC_COUNTS table with number of errored invoices in Stage table.');

 update rgintf_run_rec_counts rrc
 set rrc.ebs_stg_err_cnt = (select count ( distinct invoice_id)
 from rgap_crebs_inv_hdr_stg rcih
 where processed_flag = 'E'
 and request_id = l_request_id
 and rrc.interface_id = rcih.interface_id
 and rrc.queue_id = rcih.queue_id)
 Where request_id = l_request_id;
 
 -- Version 1.4 changes End


 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.output, 'Checking if validated record in stage table to insert in interface table.');
 Fnd_File.put_line (Fnd_File.LOG, 'Checking if validated record in stage table to insert in interface table.');

 BEGIN
 SELECT COUNT (*)
 INTO l_validated_count
 FROM rgap_crebs_inv_hdr_stg
 WHERE processed_flag = 'P'
 AND request_id = l_request_id;

 Fnd_File.put_line (Fnd_File.LOG, 'Total validated header record found: ' || l_validated_count);
 rgcomn_debug_pkg.log_message (p_level => g_log_level, p_program_name => l_module_name, p_message => 'Total validated record found: '|| l_validated_count);
 
 EXCEPTION
 WHEN NO_DATA_FOUND THEN
 l_validated_count := 0;
 
 END;

 -- 1
 -- validated records are in stage table
 IF NVL (l_validated_count, 0) > 0 THEN

 BEGIN

 Fnd_File.put_line (Fnd_File.output, 'Call process to load invoice data in open interface table');

 rgcomn_debug_pkg.log_message (p_level => g_log_level, p_program_name => l_module_name, p_message => 'Calling Procedure load_intf_data');
 Fnd_File.put_line (Fnd_File.log, 'Calling process load_intf_data to transfer data from stage to open interface table');

 load_intf_data (l_request_id);

 Fnd_File.put_line (Fnd_File.LOG, 'Back from load_inf_data. Procedure.');

 EXCEPTION

 WHEN OTHERS THEN
 rgcomn_debug_pkg.log_message (p_level => g_log_level, p_program_name => l_module_name, p_message => 'Stage to interface validation load_intf_data procedure errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

 Fnd_File.put_line (Fnd_File.LOG, 'Error calling load_intf_data procedure.' || SQLCODE);
 Fnd_File.put_line (Fnd_File.LOG, 'Error calling load_intf_data procedure.' || SQLERRM);

 END;


 begin
 for grp_id_rec in grp_id_cur(l_request_id)
 loop

 Fnd_File.put_line (Fnd_File.LOG, '--------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Calling process to run Payable Open Interface.');
 Fnd_File.put_line (Fnd_File.LOG, 'submitting Payable Open Interface process for batch: '||grp_id_rec.group_id);

 SELECT responsibility_id
 INTO g_resp_id
 FROM fnd_responsibility
 WHERE UPPER (responsibility_key) LIKE g_resp_key
 AND ROWNUM = 1;

 SELECT user_id
 INTO g_user_id
 FROM fnd_user
 WHERE user_name = g_user_name;

 SELECT application_id
 INTO g_resp_appl_id
 FROM fnd_responsibility
 WHERE responsibility_id = g_resp_id;

 IF fnd_profile.VALUE ('USERNAME') IS NULL THEN
 Fnd_File.put_line (Fnd_File.LOG, 'User name is null, will derive user, resp and resp id from profile');

 fnd_global.apps_initialize (g_user_id, g_resp_id, g_resp_appl_id);
 mo_global.set_policy_context ('S', fnd_profile.VALUE ('ORG_ID'));

 ELSE

 Fnd_File.put_line (Fnd_File.LOG, 'User name is not null, will derive user, resp and resp id from FND_GLOBAL');
 FND_GLOBAL.Apps_Initialize (FND_GLOBAL.USER_ID, FND_GLOBAL.RESP_ID, FND_GLOBAL.RESP_APPL_ID);
 
 END IF;


 Fnd_File.put_line (Fnd_File.output, 'Starting Process to call Payable Open Invoice Import Program');
 Fnd_File.put_line (Fnd_File.LOG, 'Starting Process to call Payable Open Invoice Import Program');

 l_request_id := FND_REQUEST.SUBMIT_REQUEST (
 application => 'SQLAP',
 program => 'APXIIMPT',
 description => 'Payables Open Interface Import',
 start_time => NULL,
 sub_request => FALSE,
 argument1 => NULL, -- fnd_profile.VALUE ('ORG_ID')org
 argument2 => g_source, -- source
 argument3 => grp_id_rec.group_id, --NULL, -- group
 argument4 => grp_id_rec.group_id, --l_batch_id, --g_source||'-'||TO_CHAR (SYSDATE,'DDMONYY')||'-'||l_request_id,
 argument5 => NULL, -- hold_name
 argument6 => NULL, -- hold_reason
 argument7 => NULL, -- gl_date
 argument8 => 'N', -- purge
 argument9 => 'N', -- trace_switch
 argument10 => 'N', -- debug_switch
 argument11 => 'N', -- summarize report
 argument12 => 1000, -- commit_batch_size
 argument13 => fnd_global.user_id,
 argument14 => fnd_global.user_id);
 COMMIT;

 -- 2
 IF l_request_id <> 0 THEN

 Fnd_File.put_line (Fnd_File.output,'Payable Open Invoice import request id: '|| l_request_id);
 Fnd_File.put_line (Fnd_File.LOG, 'Open Invoice import request id: '|| l_request_id);

 -- DELETE FROM RGAP_CREBS_INV_HDR_STG
 --WHERE PROCESSED_FLAG = 'P';

 --DELETE FROM RGAP_CREBS_INV_LINES_STG
 --WHERE PROCESSED_FLAG = 'P';

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Payables open interface concurrent program submitted with Request Request_Id:'|| '-'|| l_request_id);


 l_req_status := wait_for_request (request_id => l_request_id,
 phase => l_phase,
 status => l_status,
 dev_phase => l_phase_code,
 dev_status => l_status_code,
 MESSAGE => l_message);

 Fnd_File.put_line (Fnd_File.LOG, 'Waiting for Open Inovoice Reqest to complete');


 IF l_req_status = TRUE THEN

 IF l_status IN ('Cancelled', 'Error', 'Terminated') THEN
 
 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, g_source|| ' payable open interface import errored for request id '|| l_request_id|| ' ended with status'|| l_status);
 Fnd_File.put_line (Fnd_File.LOG, 'Open Invoice Import program did not complete successfully');
 
 END IF;

 ELSE

 rgcomn_debug_pkg.log_message (g_log_level, l_module_name, g_source || 'WAITING WENT TO FALSE');
 Fnd_File.put_line (Fnd_File.LOG, 'Waiting returned FALSE status.');

 END IF;

 --2
 ELSE

 Fnd_File.put_line (Fnd_File.output, 'Unable to submit Payable Open Invoice import request');
 Fnd_File.put_line (Fnd_File.LOG, 'Unable to submit Payable Open Invoice import request');

 END IF;

 end loop;
 commit;

 EXCEPTION

 WHEN OTHERS THEN
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error occured while submitting Payable open interface');
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error code: ' || SQLCODE);
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error message: ' || SQLERRM);
 
 END;



 BEGIN

 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Check if there are interface erros in table.');

 SELECT COUNT (*)
 INTO l_intf_errs
 FROM (SELECT air.*
 FROM ap_interface_rejections air,
 ap_invoices_interface aii
 WHERE aii.invoice_id = air.parent_id
 AND air.parent_table = 'AP_INVOICES_INTERFACE'
 AND aii.source = g_source
 UNION
 SELECT air.*
 FROM ap_interface_rejections air,
 ap_invoice_lines_interface aii,
 ap_invoices_interface ai
 WHERE aii.invoice_line_id = air.parent_id
 AND aii.invoice_id = ai.invoice_id
 AND air.parent_table = 'AP_INVOICE_LINES_INTERFACE'
 AND ai.source = g_source);
 
 
 -- Version 1.4 changes Start 
 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Updating Interface error count into table RGINTF_RUN_REC_COUNTS.');
 
 UPDATE RGINTF_RUN_REC_COUNTS RRC
 SET RRC.EBS_INTF_ERR_CNT =
 (WITH CONCUR_INTF_ERROR
 AS (SELECT DISTINCT aii.invoice_id, 
 rci.interface_id, 
 rci.queue_id,rci.request_id
 FROM ap_interface_rejections air,
 ap_invoices_interface aii,
 rgap_crebs_inv_hdr_stg rci
 WHERE aii.invoice_id = air.parent_id
 AND air.parent_table = 'AP_INVOICES_INTERFACE'
 AND aii.source = g_source
 AND RCI.processed_flag = 'P'
 AND RCI.INVOICE_Num = aii.INVOICE_Num
 AND RCI.Vendor_Num = aii.Vendor_num
 AND RCI.GROUP_ID = aii.GROUP_ID
 UNION
 SELECT DISTINCT aii.invoice_id, rci.interface_id, rci.queue_id,rci.request_id
 FROM ap_interface_rejections air,
 ap_invoice_lines_interface aii,
 ap_invoices_interface ai,
 rgap_crebs_inv_hdr_stg rci
 WHERE aii.invoice_line_id = air.parent_id
 AND aii.invoice_id = ai.invoice_id
 AND RCI.INVOICE_Num = ai.INVOICE_Num
 AND RCI.Vendor_Num = ai.Vendor_num
 AND RCI.GROUP_ID = ai.GROUP_ID
 AND air.parent_table = 'AP_INVOICE_LINES_INTERFACE'
 AND RCI.processed_flag = 'P'
 AND ai.source = g_source)
 SELECT COUNT (DISTINCT concur_intf_error.invoice_id)
 FROM concur_intf_error
 where concur_intf_error.interface_id = rrc.interface_id
 and rrc.queue_id = concur_intf_error.queue_id
 and rrc.request_id = concur_intf_error.request_id
 ) WHERE PROCESSED_FLAG='N';

 Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'Updating Interface soucess records count into table RGINTF_RUN_REC_COUNTS.');
 
 UPDATE RGAP.RGINTF_RUN_REC_COUNTS RRC
 SET RRC.EBS_SUCCESS_CNT =
 (SELECT COUNT (DISTINCT aii.invoice_Id)
 FROM ap_invoices_interface aii, 
 rgap_crebs_inv_hdr_stg rci
 WHERE aii.source = g_source
 AND aii.status = 'PROCESSED'
 AND RCI.INVOICE_Num = aii.INVOICE_Num
 AND RCI.GROUP_ID = aii.GROUP_ID
 AND RCI.Vendor_Num = aii.Vendor_num
 AND RCI.interface_id = RRC.INTERFACE_ID
 AND RRC.QUEUE_ID = RCI.QUEUE_ID)
 WHERE PROCESSED_FLAG='N'; 
 Commit;

 Fnd_File.put_line (Fnd_File.LOG, 'Submitting Riot Concur File Import Reconciliation .');
-- Commented using XML bursting rather than Concurrent delivery option
 /* Fnd_File.put_line (Fnd_File.LOG, 'Setting up delivery option as EMAIL for Riot Concur File Import Reconciliation.');

 l_boolean :=
  fnd_request.add_delivery_option ( TYPE => 'E', -- this one to speciy the delivery option as Email
								    p_argument1 => 'Riot Concur Expense Reconcile Report', -- subject for the mail
									p_argument2 => g_sender, -- from address
									p_argument3 => fnd_profile.value('RGAP_CONCUR_RPT_EMAIL'), -- to address
									p_argument4 => '',   -- cc address to be specified here.
									nls_language => ''); -- language option);

  IF NOT l_boolean  THEN
         Fnd_File.put_line (Fnd_File.LOG, 'Adding Delivery Option for Riot Concur File Import Reconciliation program failed: ' || SQLERRM);
         rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Adding Delivery Option for Riot Concur File Import Reconciliation program failed: ' || SQLERRM);
  END IF;		 
*/
Fnd_File.put_line (Fnd_File.LOG, 'Setting up rtf template for Riot Concur File Import Reconciliation.');
  l_boolean :=
            fnd_request.add_layout
           (template_appl_name      => 'RGAP',
            template_code           => 'RGAP_CONC_RECONCILE_REPORT',
            template_language       => 'EN',
            template_territory      => '',
            output_format           => 'PDF'
           );
  IF NOT l_boolean  THEN
         Fnd_File.put_line (Fnd_File.LOG, 'Adding Teamplate for Riot Concur File Import Reconciliation program failed: ' || SQLERRM);
         rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Adding Teamplate for Riot Concur File Import Reconciliation program failed: ' || SQLERRM);
  END IF;		 
		   
 l_request_id := FND_REQUEST.SUBMIT_REQUEST (application => 'RGAP',
											 program => 'RGAP_CONC_RECONCILE_REPORT',
											 description => 'Riot Concur File Import Reconciliation',
											 start_time => NULL,
											 sub_request => FALSE);
 COMMIT;

 Fnd_File.put_line (Fnd_File.LOG, 'Riot Concur File Import Reconciliation submitted id is: ' || l_request_id);
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Riot Concur File Import Reconciliation submitted with Request Request_Id:'|| '-'|| l_request_id);

 -- Version 1.4 changes End 
 
 
 
 IF NVL (l_intf_errs, 0) > 0 THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Interface error record count: .' || l_intf_errs);
 Fnd_File.put_line (Fnd_File.LOG, 'Interface error records found submitting error report');
 Fnd_File.put_line (Fnd_File.LOG, 'Calling rgap_concinv_error_prc.');

 l_request_id := FND_REQUEST.SUBMIT_REQUEST (application => 'RGAP',
 program => 'RGAP_CREBS_LOAD_ERRORS',
 description => 'RG Payables Concurr Expense Invoice Errors',
 start_time => NULL,
 sub_request => FALSE);
 COMMIT;

 Fnd_File.put_line (Fnd_File.LOG, 'Error request submitted id is: ' || l_request_id);
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' RG Payables Concurr Expense Invoice Errors concurrent program submitted with Request Request_Id:'|| '-'|| l_request_id);

 ELSE

 Fnd_File.put_line (Fnd_File.LOG, 'No Interface error records found. No error reports needed');
 Fnd_File.put_line (Fnd_File.output, 'No Interface error records found. No error reports needed');

 END IF;

 EXCEPTION
 
 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error while submitting error report');
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error code: ' || SQLCODE);
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => 'Error message: ' || SQLERRM);
 
 END;


 -- 1
 ELSE

 SELECT COUNT (1)
 INTO l_stg_error
 FROM rgap_crebs_inv_hdr_stg rh,
 rgap_crebs_inv_lines_stg rl
 WHERE rh.invoice_id = rl.invoice_id
 AND (rh.processed_flag = 'E' OR rl.processed_flag = 'E');

 SELECT COUNT (1)
 INTO l_orcl_error
 FROM ap_invoices_interface
 WHERE source = 'CONCUR' AND STATUS = 'REJECTED';

 Fnd_File.put_line (Fnd_File.LOG, 'No new record in stage table to process.');
 Fnd_File.put_line (Fnd_File.LOG, 'Stage error record : '||l_stg_error);
 Fnd_File.put_line (Fnd_File.LOG, 'Interface error record : '||l_orcl_error);



 IF l_stg_error > 0 THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Stage error record exists calling send_stg_err_report.');

 rgap_common_utility_pkg.rgap_send_email (p_dest => g_Interface_Email,
 p_subject => g_stg_subject,
 p_msg => rgap_common_utility_pkg.rgap_get_html_report ('select distinct VENDOR_NUM,INVOICE_NUMBER,INVOICE_AMOUNT,ERROR_DESCRIPTION, RE.CREATION_DATE
 from RGAP_CREBS_INV_ERROR_STG RE,
 RGAP_CREBS_INV_HDR_STG RH
 where RH.invoice_id=RE.Invoice_id AND ERROR_DESCRIPTION not like ''Header Record has errors'' order by 1 asc'),
 p_msg2 => g_concur_msg2,
 p_msg3 => g_concur_msg3,
 p_mailhost => g_mailhost,
 p_port => g_port,
 p_sender => g_sender);

 END IF;

 IF l_orcl_error > 0 THEN

 Fnd_File.put_line (Fnd_File.LOG, 'Interface error record exists sending interface error report.');

 rgap_common_utility_pkg.rgap_send_email (p_dest => FND_PROFILE.VALUE ('RGAP_ORACLE_EMAIL'),
 p_subject => g_intf_subject,
 p_msg => rgap_common_utility_pkg.rgap_get_html_report ('SELECT aii.Vendor_Num,aii.INVOICE_NUM,aii.INVOICE_AMOUNT,air.reject_lookup_code,aii.creation_date
 FROM ap_interface_rejections air, ap_invoices_interface aii
 WHERE aii.invoice_id = air.parent_id AND
 air.parent_table = ''AP_INVOICES_INTERFACE'' AND
 aii.source = ''CONCUR''
 UNION
 SELECT ai.Vendor_Num,ai.INVOICE_NUM,aii.AMOUNT,air.reject_lookup_code,aii.creation_date
 FROM ap_interface_rejections air,
 ap_invoice_lines_interface aii,
 ap_invoices_interface ai
 WHERE aii.invoice_line_id = air.parent_id AND
 aii.invoice_id=ai.invoice_id AND
 air.parent_table = ''AP_INVOICE_LINES_INTERFACE'' AND
 ai.source = ''CONCUR'''),
 p_msg2 => g_orcl_msg2,
 p_msg3 => g_orcl_msg3,
 p_mailhost => g_mailhost,
 p_port => g_port,
 p_sender => g_sender);

 END IF;

 Fnd_File.put_line (Fnd_File.output, 'No validated Invoice in stage table for insert into open interface table.');
 Fnd_File.put_line (Fnd_File.output, 'No validated Invoice in stage table, would not submit Payable open interface.');
 Fnd_File.put_line (Fnd_File.output, 'Concur Invoice import process complete.');
 Fnd_File.put_line (Fnd_File.output, '------------------------------------------------------');
 Fnd_File.put_line (Fnd_File.LOG, 'No validated Invoice in stage table for insert into open interface table.');
 Fnd_File.put_line (Fnd_File.LOG, 'Concur Invoice import process complete.');
 Fnd_File.put_line (Fnd_File.output, '------------------------------------------------------');

 rgcomn_debug_pkg.
 log_message (
 p_level => g_log_level,
 p_program_name => l_module_name,
 p_message => 'No validated Invoice in stage table for insert into open interface table');

 END IF;

 -- main exception
 EXCEPTION

 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Exception at launching Payables open interface concurrent program '|| '-'|| SQLCODE|| ' '|| SQLERRM);
 Fnd_File.put_line (Fnd_File.LOG, 'Exception at launching Payables open interface concurrent program.');
 Fnd_File.put_line (Fnd_File.LOG, 'Error code: ' || SQLCODE);
 Fnd_File.put_line (Fnd_File.LOG, 'Error message: ' || SQLERRM);

 rgcomn_debug_pkg.log_cp_message (P_LEVEL => g_log_level, P_MESSAGE => ' Exception at launching Payables open interface concurrent program '|| '-'|| SQLCODE|| ' '|| SQLERRM);
 
 rgap_common_utility_pkg.rgap_send_email (p_dest => FND_PROFILE.VALUE ('RGAP_ORACLE_EMAIL'),
 p_subject => g_intf_subject,
 p_msg => ' Exception at launching Payables open interface concurrent program '|| SQLCODE|| '-'|| SQLERRM,
 p_msg2 => g_orcl_msg2,
 p_mailhost => fnd_profile.VALUE ('RGAP_SMTP_SERVER'),
 p_port => fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT'),
 p_sender => 'noreply@riotgames.com');

 END rgap_concinv_main_prc;



 PROCEDURE rgap_concinv_error_prc (errbuf OUT VARCHAR2,
 retcode OUT VARCHAR2) IS

 l_module_name VARCHAR2 (50) := 'rgap_concinv_main_prc';
 l_request_id NUMBER;
 l_errors NUMBER;
 l_phase VARCHAR2 (120);
 l_status VARCHAR2 (120);
 l_dev_phase VARCHAR2 (120);
 l_dev_status VARCHAR2 (120);
 l_message VARCHAR2 (200);
 l_req_status BOOLEAN;
 --
 BEGIN
 
 Fnd_File.put_line (Fnd_File.output, 'Starting Error report submission process');
 rgcomn_debug_pkg.log_cp_message (P_LEVEL => g_log_level, P_MESSAGE => '******************** Process Starts :'|| TO_CHAR (SYSDATE, 'DD:MON:YYYY HH:MI:SS')|| ' ******************** ');

 SELECT responsibility_id
 INTO g_resp_id
 FROM fnd_responsibility
 WHERE UPPER (responsibility_key) LIKE g_resp_key AND ROWNUM = 1;

 SELECT user_id
 INTO g_user_id
 FROM fnd_user
 WHERE user_name = g_user_name;

 SELECT application_id
 INTO g_resp_appl_id
 FROM fnd_responsibility
 WHERE responsibility_id = g_resp_id;

 IF fnd_profile.VALUE ('USERNAME') IS NULL THEN
 
 fnd_global.apps_initialize (g_user_id, g_resp_id, g_resp_appl_id);
 mo_global.set_policy_context ('S', fnd_profile.VALUE ('ORG_ID'));
 
 ELSE

 FND_GLOBAL.Apps_Initialize (FND_GLOBAL.USER_ID, FND_GLOBAL.RESP_ID, FND_GLOBAL.RESP_APPL_ID);
 
 END IF;

 SELECT COUNT (*)
 INTO l_errors
 FROM (SELECT air.*
 FROM ap_interface_rejections air, ap_invoices_interface aii
 WHERE aii.invoice_id = air.parent_id
 AND air.parent_table = 'AP_INVOICES_INTERFACE'
 AND aii.source = g_source
 UNION
 SELECT air.*
 FROM ap_interface_rejections air,
 ap_invoice_lines_interface aii,
 ap_invoices_interface ai
 WHERE aii.invoice_line_id = air.parent_id
 AND aii.invoice_id = ai.invoice_id
 AND air.parent_table = 'AP_INVOICE_LINES_INTERFACE'
 AND ai.source = g_source);

 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Payables interface Error concurrent program submitted');

 IF l_errors > 0 THEN
 
 rgap_common_utility_pkg.rgap_send_email (p_dest => FND_PROFILE.VALUE ('RGAP_ORACLE_EMAIL'),
 p_subject => g_intf_subject,
 p_msg => rgap_common_utility_pkg.rgap_get_html_report ('SELECT aii.Vendor_Num,aii.INVOICE_NUM,aii.INVOICE_AMOUNT,air.reject_lookup_code,aii.creation_date
 FROM ap_interface_rejections air, ap_invoices_interface aii
 WHERE aii.invoice_id = air.parent_id AND
 air.parent_table = ''AP_INVOICES_INTERFACE'' AND
 aii.source = ''CONCUR''
 UNION
 SELECT ai.Vendor_Num,ai.INVOICE_NUM,aii.AMOUNT,air.reject_lookup_code,aii.creation_date
 FROM ap_interface_rejections air,
 ap_invoice_lines_interface aii,
 ap_invoices_interface ai
 WHERE aii.invoice_line_id = air.parent_id AND
 aii.invoice_id=ai.invoice_id AND
 air.parent_table = ''AP_INVOICE_LINES_INTERFACE'' AND
 ai.source = ''CONCUR'''),
 p_msg2 => g_orcl_msg2,
 p_msg3 => g_orcl_msg3,
 p_mailhost => g_mailhost,
 p_port => g_port,
 p_sender => g_sender);
 END IF;

 EXCEPTION
 
 WHEN OTHERS THEN
 
 rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level, P_PROGRAM_NAME => l_module_name, P_MESSAGE => ' Exception at launching Payables open interface concurrent program '|| '-'|| SQLCODE|| ' '|| SQLERRM);
 rgcomn_debug_pkg.log_cp_message (P_LEVEL => g_log_level, P_MESSAGE => ' Exception at launching Payables open interface concurrent program '|| '-'|| SQLCODE|| ' '|| SQLERRM);

 rgap_common_utility_pkg.rgap_send_email (p_dest => FND_PROFILE.VALUE ('RGAP_ORACLE_EMAIL'),
 p_subject => g_intf_subject,
 p_msg => ' Exception at launching Payables open interface concurrent program '|| SQLCODE|| '-'|| SQLERRM,
 p_msg2 => g_orcl_msg2,
 p_mailhost => fnd_profile.VALUE ('RGAP_SMTP_SERVER'),
 p_port => fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT'),
                                                                                            p_sender => 'noreply@riotgames.com');
                                                                                            
   END rgap_concinv_error_prc;

END rgap_concurr_invintf_pkg;
/


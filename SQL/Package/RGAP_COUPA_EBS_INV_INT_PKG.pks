CREATE OR REPLACE PACKAGE APPS.rgap_coupa_ebs_inv_int_pkg AS
------------------------------------------------------------------------
   -- Name...: RGAP_COUPA_EBS_INV_INT_PKG.pks
   -- Desc...: This package is used to process data from Coupa to EBS
   --
   --
   -- History:
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Date                       Name                        Version Number      Issue No       Revision                                     Summary
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   -- 13-Aug-2015           Praveen Yadiki                  1.0                                                                                Created initial version
   -- 16-Feb-2016           Srinivasa                            1.1                91 and 92                                              Check for Invoice coming for reprocess.Modified email procedure call
   --
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    16-May-2016    Prabhat                             1.3                                                                     1. Add logic to submit payable open interface by batch
   --                                                                                                                                                      2. Change site pick logic to exclude deactivated sites
   --                                                                                                                                                      3. introduce rollbak in interface table insert procedure to preven header insert
   --                                                                                                                                                         without line or partial line insert.
   --                                                                                                                                                     4. put logic to delete orphan stage line records
   --                                                                                                                                                     5. send completion email when there are no errors to notify
   --                                                                                                                                                     6. populate asset book type code in interface line table.
   --
   -- 22-Jun-2016   Prabhat                                 1.4                                                                     1. Fix interface table rejected records not geeting picked in payable open interface run
   --                                                                                                                                                         after grouping by batch id was introduced in May bucket..
   --                                                                                                                                                    2. When a invoice is already in EBS interface table stage table records are not getting
   --                                                                                                                                                        marked as 'E'. The record is not inserted into interface table but gets marked as 'P'.
   -- ADDITIONAL NOTES
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   g_user_id           fnd_user.user_id%TYPE;
   g_resp_id           NUMBER;
   g_resp_appl_id      NUMBER;
   g_log_level         NUMBER                  := 0;
   g_user_name         VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_US_USER');
   g_interface_email   VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_COUPA_EMAIL');
   g_mailhost          VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   g_port              NUMBER  := fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT');
   g_sender            VARCHAR2 (100) := 'donotreply@riotgames.com';
   g_resp_key         VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_US_RESP_KEY');
   g_stg_subject varchar2(150) := 'Coupa to Oracle EBS Invoice Interface (Stage) Error/Exception Report';
   g_intf_subject varchar2(150) := 'Coupa to Oracle EBS Invoice Interface (Intf) Error/Exception Report';
   g_orcl_msg2  varchar2(150) := 'Following table lists error/exception records in interface table(s).';
   g_orcl_msg3  varchar2(350) := 'These records were rejected during open interface run. Please correct them in interface table within Oracle EBS and re-run Payable Open Interface or delete exception/error record(s) from interface table and retransmit from Coupa.';
   g_coupa_msg2 varchar2(150) := 'Following table lists error/exception records in interface stage table(s).';
   g_coupa_msg3  varchar2(250) := 'These records were rejected during pre-validation process. Please correct them in Coupa and re-transmit.';
   g_stg_nd_subject varchar2(150) := 'Coupa to Oracle EBS Invoice Interface completion Report';
   g_nd_msg1 varchar2(150) := 'Coupa to Oracle EBS Invoice Interface Completed Successfully';
   g_nd_msg2 varchar2(150) := 'No new records were found in stage table for processing. Next run of this interface will be in an hour.';
   g_nd_msg3 varchar2(150) := 'No error/exception were generated in this run of interface.';

  PROCEDURE rgap_inv_main (errbuf OUT VARCHAR2, retcode OUT NUMBER);

END RGAP_COUPA_EBS_INV_INT_PKG;
/


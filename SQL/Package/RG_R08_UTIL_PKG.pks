CREATE OR REPLACE PACKAGE APPS.RG_R08_UTIL_PKG
AS
  FUNCTION GET_TRANS_NUM (p_header in number, p_line in varchar2)
    RETURN VARCHAR2;

  FUNCTION GET_DESC (p_header in number, p_line in varchar2, p_source in varchar2, p_cat in varchar2)
    RETURN VARCHAR2;

END RG_R08_UTIL_PKG;
/


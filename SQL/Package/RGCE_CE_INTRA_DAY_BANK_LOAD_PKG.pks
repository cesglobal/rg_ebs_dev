CREATE OR REPLACE PACKAGE RGCE_INTRA_DAY_BANK_LOAD_PKG
AS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Package is Used to load data Cash Management Interface tables from files.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    24-Feb-2016     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
PROCEDURE SUBMIT_SHELL(p_request_id NUMBER);
PROCEDURE REUSB_BANK_SUB_CAP (
   piv_file_name     IN   VARCHAR2,
   pin_request_id    IN   NUMBER,
   piv_file_path     IN   VARCHAR2,
   piv_load_option   IN   VARCHAR2,
   piv_map_temp      IN   VARCHAR2,
   piv_bank_br_num   IN   VARCHAR2,
   piv_accnt_num     IN   VARCHAR2,
   piv_gl_date       IN   VARCHAR2);
PROCEDURE RGCE_ERRORS_STG;
PROCEDURE MAIN ( ERRBUF  OUT VARCHAR2,
				 RETCODE OUT NUMBER,
				 p_process_id IN VARCHAR2,
				 p_Mapping_id IN VARCHAR2);
PROCEDURE RGCE_SEND_ERROR_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_SEND_SUCCESS_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_PROG_ERROR_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_SEND_NOFILE_MAIL;
END RGCE_INTRA_DAY_BANK_LOAD_PKG;				 
/   
SHOW ERRORS;  
CREATE OR REPLACE PACKAGE APPS.rgap_coupa_payments_pkg
   AUTHID DEFINER
AS
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose              : This Package is having Main Function to retuns payment information into the  Table of required columns.
   --
   --    Module               : RGAP
   --    Modification History :
   --       Date             Name             Version Number      Revision Summary
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    19-AUG-2015       Srinivasa          1.0
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables
   G_Interface_name   VARCHAR2 (20) := 'PAY_INTF_EBS_CP';
   G_Org_Name         VARCHAR2 (240);
   G_Org_Id           NUMBER;

   FUNCTION rgap_coupa_payments_fnc (p_asof_date DATE DEFAULT NULL)
      RETURN rgap_payment_nested_table;

   PROCEDURE rgap_extract_result (p_date DATE, p_result VARCHAR2);
END rgap_coupa_payments_pkg;
/


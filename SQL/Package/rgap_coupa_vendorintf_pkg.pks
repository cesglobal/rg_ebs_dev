CREATE OR REPLACE PACKAGE APPS.rgap_coupa_vendorintf_pkg AUTHID DEFINER AS
/* ------------------------------------------------------------------------------------------------------------------------------------------------------
--    Author               : Ram
--    Purpose              : This Package is having Main Function to retuns Vendor information into the  Table of required columns.
--
--    Module               : RGAP
--    Modification History :
--       Date             Name             Version Number      Revision Summary
-- ------------------------------------------------------------------------------------------------------------------------------------------------------
--    24-AUG-2015       Ram          1.0
-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
-- Global Variables
G_Interface_name varchar2(20):='VEND_INTF_EBS_CP';
G_Org_Id           NUMBER;

Function rgap_coupa_vendors_fnc(p_asof_date date DEFAULT NULL) RETURN rgap_vendor_nested_table;
Procedure rgap_extract_result(p_date Date,p_result varchar2);

END rgap_coupa_vendorintf_pkg;
/


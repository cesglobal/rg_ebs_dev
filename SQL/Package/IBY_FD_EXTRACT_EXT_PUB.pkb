CREATE OR REPLACE PACKAGE BODY APPS.iby_fd_extract_ext_pub  AS
/* $Header: ibyfdxeb.pls 120.2 2006/09/20 18:52:12 frzhang noship $ */


  --
  -- This API is called once only for the payment instruction.
  -- Implementor should construct the extract extension elements
  -- at the payment instruction level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Below is an example implementation:
/*
  FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
  RETURN XMLTYPE
  IS
    l_ins_ext_agg XMLTYPE;

    CURSOR l_ins_ext_csr (p_payment_instruction_id IN NUMBER) IS
    SELECT XMLConcat(
             XMLElement("Extend",
               XMLElement("Name", ext_table.attr_name1),
               XMLElement("Value", ext_table.attr_value1)),
             XMLElement("Extend",
               XMLElement("Name", ext_table.attr_name2),
               XMLElement("Value", ext_table.attr_value2))
           )
      FROM your_pay_instruction_lvl_table ext_table
     WHERE ext_table.payment_instruction_id = p_payment_instruction_id;

  BEGIN

    OPEN l_ins_ext_csr (p_payment_instruction_id);
    FETCH l_ins_ext_csr INTO l_ins_ext_agg;
    CLOSE l_ins_ext_csr;

    RETURN l_ins_ext_agg;

  END Get_Ins_Ext_Agg;
*/
  FUNCTION Get_Ins_Ext_Agg(p_payment_instruction_id IN NUMBER)
  RETURN XMLTYPE
  IS
  l_ins_ext_agg   XMLTYPE;
  Vl_Message      LONG;
  vl_sqlerrm      VARCHAR2(4000);
  v_count         NUMBER;
  v_PAYMENT_PROCESS_REQUEST_NAME VARCHAR2(60);
  i                    NUMBER;
  vl_bank_account_num  VARCHAR2(60);
  vl_COUNTRY_CODE      VARCHAR2(60);
  vl_BANK_ACCOUNT_NAME VARCHAR2(60);
  vl_CURRENCY_CODE     VARCHAR2(60);
  vl_BANK_NUMBER       VARCHAR2(60);
  vl_request_id        NUMBER;
  vl_vendor_name       ap_suppliers.vendor_name%type;
  vl_msg               NUMBER;

CURSOR l_ins_ext_csr (p_payment_instruction_id IN NUMBER)
IS
    SELECT XMLConcat(
             XMLElement("Extend",
               XMLELEMENT("RECTYPE_0",'0'),
              XMLELEMENT("FILEDATE",TO_CHAR(SYSDATE, 'YYMMDD')),
              XMLELEMENT("CitiBranch", ext_table.INT_BANK_BRANCH_ALT_NAME),
              XMLELEMENT("CITIBASE", ''),
              XMLELEMENT("FILLER", ''),
              XMLELEMENT("MACRECTYP", ''),
              XMLELEMENT("MACCODE", ''),
              XMLELEMENT("MACDATE", ''),
              XMLELEMENT("MACTIME", ''),
              XMLELEMENT("RSDID", ''),
              XMLELEMENT("KEYINDEX", ''),
              XMLELEMENT("FILESEQNO", ''),
              XMLELEMENT("FILLER", ''),
              XMLELEMENT("RECTYPE_1",'1'),
              XMLELEMENT("TransSeq_1",'0001'),
              XMLELEMENT("RecID_1",'01'),
              XMLELEMENT("TransDate",(SELECT TO_CHAR(CHECK_DATE, 'YYMMDD' ) from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("TransRef", (SELECT CHECK_ID from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("InstCCY", (SELECT CURRENCY_CODE from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("Filler", ''),
              XMLELEMENT("TransType", 'C'),
              XMLELEMENT("InstAmt", (SELECT AMOUNT from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("Filler", ''),
              XMLELEMENT("CustBulk1", ''),
              XMLELEMENT("CustBulk", ext_table.INT_BANK_ACCOUNT_NUMBER),
              XMLELEMENT("TransCCY", (SELECT CURRENCY_CODE from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("CitiBranch", ext_table.INT_BANK_BRANCH_ALT_NAME),
              XMLELEMENT("CustName", ext_table.PAYER_legal_entity_name),
              XMLELEMENT("FXFlag", ''),
              XMLELEMENT("CustCountry", ''),
              XMLELEMENT("TransHandlCde", ''),
              XMLELEMENT("CustAcctTyp", ''),
              XMLELEMENT("IntraCo", ''),
              XMLELEMENT("Confid", ''),
              XMLELEMENT("PDCDisc", ''),
              XMLELEMENT("TransPostDate", ''),
              XMLELEMENT("DecInd", '')  ,
              XMLELEMENT("RECTYPE_2",'1'),
              XMLELEMENT("TransSeq_2",'0002'),
              XMLELEMENT("RecID_2",'02'),
              XMLELEMENT("CtryCode",'' ),
              XMLELEMENT("InterSWIFT", ''),
              XMLELEMENT("TargetSWIFT", ext_table.ext_bank_number),
              XMLELEMENT("TgtBnkCtryCde", '' ),
              XMLELEMENT("TgtAcct", ext_table.INT_bank_account_number),
              XMLELEMENT("TgtName", ext_table.payee_name),
              XMLELEMENT("TgtCtryCde", ''),
              XMLELEMENT("TgtBnkAcctTyp", ''),
              XMLELEMENT("TgtBnkCdeTyp", ''),
              XMLELEMENT("TgtBnkInd", ''),
              XMLELEMENT("PrtyCode", ''),
              XMLELEMENT("Filler", ''),
              XMLELEMENT("RECTYPE_3",'1'),
              XMLELEMENT("TransSeq_3",'0003'),
              XMLELEMENT("RecID_3",'03'),
              XMLELEMENT("TransDet1",'' ),
              XMLELEMENT("TranMethod", ''),
              XMLELEMENT("ChargeCde", ''),
              XMLELEMENT("ChargeAcct", '' ),
              XMLELEMENT("TransCode", ''),
              XMLELEMENT("CustAcctSub", ''),
              XMLELEMENT("TgtBankCde", ext_table.INT_BANK_branch_NUMBER),
              XMLELEMENT("InetrBankCde", ''),
              XMLELEMENT("BatchNum", (SELECT CHECKRUN_NAME from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("PIUID", ''),
             XMLELEMENT("country", (SELECT country from AP_CHECKS_ALL where PAYMENT_ID = ext_table.payment_id and rownum =1)),
              XMLELEMENT("Type",'9'),
              XMLELEMENT("RecCount",'6'),--
              XMLELEMENT("TransCount",''),
            --  XMLELEMENT("TransTotal", sum(ACA.AMOUNT) ),
              XMLELEMENT("Filler", ''),
               XMLElement("Product_Code",'DFT'),
               XMLElement("Debit_Account_Country_Code", 'KR'),
               XMLElement("Debit_account_number", ext_table.INT_bank_account_number),
               XMLElement("Payment_currency", 'KRW'),
               XMLElement("Payment_amount", REPLACE(ext_table.payment_amount,'.','')||'.'),
               XMLElement("Value_date", to_char(to_date(ext_table.payment_date,'DD-MON-YY'),'YYYYMMDD')),
               XMLElement("Payment_document_number", ext_table.paper_document_number),
               XMLElement("Ordering_party_name", 'RIOT GAMES KOREA' ),
               XMLElement("Beneficary_Name", ext_table.EXT_BANK_ACCOUNT_NAME ),
               XMLElement("Beneficary_Account_number", ext_table.ext_bank_account_number ),
               XMLElement("Beneficary_bank_name", ext_table.ext_bank_name ),
               XMLElement("Beneficary_bank_routing_code", ext_table.ext_bank_number )))
      FROM IBY_PAYMENTS_ALL ext_table
     WHERE ext_table.payment_instruction_id = p_payment_instruction_id;

  CURSOR l_positive_pay_csr (p_payment_instruction_id IN NUMBER)
      IS
   SELECT XMLElement("BankNumEle", INT_BANK_ACCOUNT_NUM_ELEC)
     FROM IBY_PAYMENTS_ALL ipmtins
WHERE ipmtins.payment_instruction_id =p_payment_instruction_id;

    CURSOR C_CUR(p_payment_instruction_id IN NUMBER)
    IS
    SELECT ext_table.INT_bank_account_number,
           ext_table.payment_currency_code,
           ext_table.INT_BANK_branch_NUMBER,
           ext_table.INT_BANK_BRANCH_ALT_NAME,
           ext_table.payment_amount,
           ext_table.payee_name,
           ext_table.PAYER_legal_entity_name,
           ext_table.payment_date,
          /* (SELECT check_number
              from AP_CHECKS_ALL
             where PAYMENT_ID = ext_table.payment_id
               and rownum = 1)*/ ext_table.PAPER_DOCUMENT_NUMBER check_number,
           ext_table.EXT_BANK_ACCOUNT_NAME ,
           ext_table.ext_bank_account_number ,
           ext_table.ext_bank_name ,
           ext_table.ext_bank_number,
           ext_table.payment_id,
           ext_table.PAYEE_PARTY_ID
      FROM IBY_PAYMENTS_ALL ext_table
     WHERE ext_table.payment_instruction_id = p_payment_instruction_id;

    CURSOR v_CUR(p_payment_instruction_id IN NUMBER)
    IS
    SELECT ext_table.INT_bank_account_number,
           ext_table.payment_currency_code,
           ext_table.INT_BANK_branch_NUMBER,
           ext_table.INT_BANK_BRANCH_ALT_NAME,
           ext_table.payment_amount,
           ext_table.payee_name,
           ext_table.PAYER_legal_entity_name,
           ext_table.payment_date,
           /*(SELECT check_number
              from AP_CHECKS_ALL
             where PAYMENT_ID = ext_table.payment_id
               and rownum = 1)*/ ext_table.PAPER_DOCUMENT_NUMBER check_number,
           ext_table.EXT_BANK_ACCOUNT_NAME ,
           ext_table.ext_bank_account_number ,
           ext_table.ext_bank_name ,
           ext_table.ext_bank_number,
           ext_table.payment_id,
           ext_table.PAYEE_PARTY_ID
      FROM IBY_PAYMENTS_ALL ext_table
     WHERE ext_table.payment_instruction_id = p_payment_instruction_id;

    l_request_id NUMBER;
    l_result     boolean;

  BEGIN
    FND_FILE.PUT_LINE (FND_FILE.LOG,'Entered Into Main Begin');
    l_request_id := fnd_global.conc_request_id;
    BEGIN
        SELECT PAYMENT_PROFILE_SYS_NAME
          INTO v_PAYMENT_PROCESS_REQUEST_NAME
          FROM IBY_PAYMENTS_ALL
         WHERE payment_instruction_id = p_payment_instruction_id
         GROUP BY PAYMENT_PROFILE_SYS_NAME;
    EXCEPTION
        WHEN OTHERS THEN
        v_PAYMENT_PROCESS_REQUEST_NAME := NULL;
    FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while Extracting PAYMENT_PROCESS_REQUEST_NAME:'||SQLERRM);
    END;

         BEGIN
            OPEN l_positive_pay_csr (p_payment_instruction_id);
            FETCH l_positive_pay_csr INTO l_ins_ext_agg;
            CLOSE l_positive_pay_csr;
         EXCEPTION
            WHEN OTHERS
            THEN
               FND_FILE.
                PUT_LINE (
                  FND_FILE.LOG,
                  'Error while Extracting Payment Instructions:' || SQLERRM);
         END;

    FND_FILE.PUT_LINE (FND_FILE.LOG,'Before Into IF condition');

    IF (upper(v_PAYMENT_PROCESS_REQUEST_NAME) like UPPER('RGKR%EFT%etext%') OR
       upper(v_PAYMENT_PROCESS_REQUEST_NAME) = UPPER('RG_KOR_EFT_PPP'))
    THEN

    /*FND_FILE.PUT_LINE (FND_FILE.LOG,'Entered Into IF condition');
    FND_FILE.PUT_LINE (FND_FILE.LOG,'Payment Process Request Name :'||v_PAYMENT_PROCESS_REQUEST_NAME);

        SELECT count(1)
          INTO v_count
          FROM IBY_PAYMENTS_ALL
         WHERE payment_instruction_id = p_payment_instruction_id;

        IF v_count > 0
        THEN

        FND_FILE.PUT_LINE (FND_FILE.LOG,'Count Of lines in Payment Format:'||v_count);

        BEGIN
         i:=0;
         vl_msg := 0;

            FOR R_CUR IN C_CUR(p_payment_instruction_id)
            LOOP
            FND_FILE.PUT_LINE (FND_FILE.LOG,'Validation Cursor Start :'||R_CUR.payment_id);
            BEGIN
                SELECT BANK_ACCOUNT_NUM,COUNTRY_CODE,BANK_ACCOUNT_NAME,CURRENCY_CODE,BANK_NUMBER,
                  (SELECT vendor_name from ap_suppliers where party_id = R_CUR.PAYEE_PARTY_ID and rownum =1)
                  INTO vl_bank_account_num,vl_COUNTRY_CODE,vl_BANK_ACCOUNT_NAME,vl_CURRENCY_CODE,vl_BANK_NUMBER,vl_vendor_name
                  FROM iby_payee_all_bankacct_v
                 WHERE party_id =  R_CUR.PAYEE_PARTY_ID
                   AND bank_account_num = R_CUR.ext_bank_account_number;
            EXCEPTION
                WHEN OTHERS THEN
                vl_bank_account_num  := NULL;
                vl_COUNTRY_CODE      := NULL;
                vl_BANK_ACCOUNT_NAME := NULL;
                vl_CURRENCY_CODE     := NULL;
                vl_BANK_NUMBER       := NULL;
            END;

            IF vl_bank_account_num IS NULL
            THEN
            vl_msg := vl_msg + 1;

                FND_FILE.PUT_LINE (FND_FILE.LOG,'Bank Account Number is Null for Supplier :'||vl_vendor_name);
                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Bank Account Number is Null for Supplier :'||vl_vendor_name);

            END IF;

            IF  vl_COUNTRY_CODE <> 'KR'
            THEN
             vl_msg := vl_msg + 1;
                FND_FILE.PUT_LINE (FND_FILE.LOG,'Country code is not Korea Republic of for Supplier :'||vl_vendor_name);
                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Country code is not Korea Republic of for Supplier :'||vl_vendor_name);

            END IF;

            IF vl_BANK_ACCOUNT_NAME IS NULL
            THEN
             vl_msg := vl_msg + 1;
                 FND_FILE.PUT_LINE (FND_FILE.LOG,'Bank Account Name is not defined for Supplier :'||vl_vendor_name);
                 FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Bank Account Name is not defined for Supplier :'||vl_vendor_name);

            END IF;

            IF vl_CURRENCY_CODE <> 'KRW'
            THEN
             vl_msg := vl_msg + 1;
                FND_FILE.PUT_LINE (FND_FILE.LOG,'Currency Code KRW is not Defined for Supplier :'||vl_vendor_name);
                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Currency Code KRW is not Defined for Supplier :'||vl_vendor_name);

            END IF;

            IF vl_BANK_NUMBER IS NULL
            THEN
             vl_msg := vl_msg + 1;
                FND_FILE.PUT_LINE (FND_FILE.LOG,'Bank Number is not Defined for Supplier :'||vl_vendor_name);
                FND_FILE.PUT_LINE (FND_FILE.OUTPUT,'Bank Number is not Defined for Supplier :'||vl_vendor_name);

            END IF;

            END LOOP;
        --l_ins_ext_agg := NULL;
        EXCEPTION
            WHEN OTHERS THEN
        FND_FILE.PUT_LINE (FND_FILE.LOG,'Validation Cursor :'||SQLERRM);
        END;

            IF  vl_msg = 0
            THEN*/

            BEGIN

                FOR R_CUR IN v_CUR(p_payment_instruction_id)
                LOOP
                    i:= i + 1;

                    FND_FILE.PUT_LINE (FND_FILE.LOG,'Payment Id :'||R_CUR.payment_id);
                    --IF i > 1
            --THEN

                    FND_FILE.PUT_LINE (FND_FILE.OUTPUT,
                    RPAD('DFT@KR@'||R_CUR.INT_bank_account_number||'@KRW@'||LPAD(REPLACE(R_CUR.payment_amount,'.','')||'.',13,' ')||'@@'||to_char(to_date(R_CUR.payment_date,'DD-MON-YY'),'YYYYMMDD')||'@'||RPAD(NVL(to_char(R_CUR.check_number),' '),15,' ')||'@@@@@@'||RPAD('RIOT GAMES KOREA',35,' ')||'@@@@@@'||RPAD(NVL(R_CUR.EXT_BANK_ACCOUNT_NAME,' '),35,' ')||'@@@@@'||RPAD(NVL(R_CUR.ext_bank_account_number,' '),16,' ')||'@@'||RPAD(NVL(R_CUR.ext_bank_name,' '),35,' ')||'@@@@@'||RPAD(NVL(R_CUR.ext_bank_number,' '),3,' ')||'@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@',290));
                    --END IF;

                END LOOP;
      EXCEPTION
        WHEN OTHERS THEN
        FND_FILE.PUT_LINE (FND_FILE.LOG,'Payment Cursor :'||SQLERRM);
          END;

    /*        ELSIF  vl_msg > 0
      THEN
        BEGIN
        EXECUTE IMMEDIATE 'update IBY_PAYMENTS_ALL set payment_status = ''INSTRUCTION_FAILED_VALIDATION'' where payment_instruction_id = '||p_payment_instruction_id||'';
        COMMIT;
        EXCEPTION
            WHEN OTHERS THEN
        FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while updating status :'||SQLERRM);
        END;

            BEGIN
                SELECT request_id
                  INTO vl_request_id
                  FROM fnd_concurrent_requests
                 WHERE argument1 = p_payment_instruction_id
                   AND concurrent_program_id IN (SELECT concurrent_program_id
                                                   FROM fnd_concurrent_programs_vl
                                                  WHERE concurrent_program_name = 'IBY_FD_PAYMENT_FORMAT_TEXT');

            FND_FILE.PUT_LINE (FND_FILE.LOG,'vl_request_id:'||vl_request_id);
            EXCEPTION
                WHEN OTHERS THEN
                vl_request_id := 0;
                FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while fetching Request Id:'||vl_request_id);
            END;
            FND_FILE.PUT_LINE(FND_FILE.LOG,'Conc Request Id :'||l_request_id);

            begin
            l_result := fnd_concurrent.set_completion_status('ERROR',NULL);
            commit;
            exception
            when others then
            fnd_file.put_line(fnd_file.log,'Error while making conc error:'||SQLERRM);
            end;

            --FND_FILE.PUT_LINE(FND_FILE.LOG,'l_result:'||l_result);

            END IF;

        END IF;*/

        BEGIN
            OPEN l_ins_ext_csr (p_payment_instruction_id);
           FETCH l_ins_ext_csr INTO l_ins_ext_agg;
           CLOSE l_ins_ext_csr;
        EXCEPTION
          WHEN OTHERS THEN
          FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while Extracting Payment Instructions:'||SQLERRM);
        END;
        ELSIF  UPPER (v_PAYMENT_PROCESS_REQUEST_NAME) LIKE   UPPER ('RG US CHECK PAYMENT PROCESS PROFILE') THEN
         BEGIN
            OPEN l_positive_pay_csr (p_payment_instruction_id);
            FETCH l_positive_pay_csr INTO l_ins_ext_agg;
            CLOSE l_positive_pay_csr;
         EXCEPTION
            WHEN OTHERS
            THEN
               FND_FILE.
                PUT_LINE (
                  FND_FILE.LOG,
                  'Error while Extracting Payment Instructions:' || SQLERRM);
         END;
    ELSE
         BEGIN
            OPEN l_positive_pay_csr (p_payment_instruction_id);
            FETCH l_positive_pay_csr INTO l_ins_ext_agg;
            CLOSE l_positive_pay_csr;
         EXCEPTION
            WHEN OTHERS
            THEN
               FND_FILE.
                PUT_LINE (
                  FND_FILE.LOG,
                  'Error while Extracting Payment Instructions:' || SQLERRM);
         END;
/*        BEGIN
            OPEN l_ins_ext_csr (p_payment_instruction_id);
           FETCH l_ins_ext_csr INTO l_ins_ext_agg;
           CLOSE l_ins_ext_csr;
        EXCEPTION
          WHEN OTHERS THEN
          FND_FILE.PUT_LINE (FND_FILE.LOG,'Error while Extracting Payment Instructions:'||SQLERRM);
        END;
*/
    END IF;


   RETURN l_ins_ext_agg;

  END Get_Ins_Ext_Agg;


  --
  -- This API is called once per payment.
  -- Implementor should construct the extract extension elements
  -- at the payment level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  FUNCTION Get_Pmt_Ext_Agg(p_payment_id IN NUMBER)
  RETURN XMLTYPE
  IS
l_pmt_ext_agg XMLTYPE;

CURSOR l_pmt_ext_csr (p_payment_id IN NUMBER) IS
 SELECT XMLConcat(
             XMLElement("Extend",
                    XMLELEMENT("FILEDATE",TO_CHAR(SYSDATE, 'YYMMDD')),
              XMLELEMENT("CitiBranch", ipa.INT_BANK_BRANCH_ALT_NAME),
              XMLELEMENT("TransSeq_1",'0001'),
              XMLELEMENT("TransDate",(SELECT TO_CHAR(CHECK_DATE, 'YYMMDD' ) from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
              XMLELEMENT("TransRef", (SELECT CHECK_ID from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
              XMLELEMENT("InstCCY", (SELECT CURRENCY_CODE from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
              XMLELEMENT("InstAmt", (SELECT AMOUNT from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
              XMLELEMENT("CustBulk", ipa.INT_BANK_ACCOUNT_NUMBER),
              XMLELEMENT("TransCCY", (SELECT CURRENCY_CODE from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
              XMLELEMENT("CitiBranch", ipa.INT_BANK_BRANCH_ALT_NAME),
              --XMLELEMENT("CustName", ipa.PAYER_legal_entity_name),
             -- XMLELEMENT("CustName",  (SELECT       translate(PARTY_NAME,(REPLACE(translate(PARTY_NAME  ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32),  'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')  FROM HZ_PARTIES WHERE party_id= ipa.payer_party_id and rownum =1)),
              xmlelement("CustName",  nvl((select       translate(party_name,(replace(translate(party_name  ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32),  'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')  from hz_parties where party_id= ipa.payer_party_id and rownum =1),(select party_name from hz_parties where party_id= payer_party_id and rownum =1))),
              --XMLELEMENT("TargetSWIFT", ipa.ext_bank_number),
              xmlelement("TargetSWIFT", nvl((translate(ipa.ext_eft_swift_code,(replace(translate(ipa.ext_eft_swift_code ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')),ipa.ext_eft_swift_code)),
              --xmlelement("TgtAcct", ipa.int_bank_account_number),
               xmlelement("TgtAcct", nvl((translate(ipa.ext_bank_account_iban_number,(REPLACE(translate(ipa.ext_bank_account_iban_number ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')),ipa.ext_bank_account_iban_number)),

              --xmlelement("TgtName", translate(ipa.payee_name,(REPLACE(translate(ipa.payee_name ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')),
              xmlelement("TgtName", nvl((translate(ipa.payee_name,(REPLACE(translate(ipa.payee_name ,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'?????????????????????????????????????????????????????????????????????????')),ipa.payee_name)),
              XMLELEMENT("ROWNUM", ROWNUM),
              XMLELEMENT("payment_amout", ipa.payment_amount),
              xmlelement("TgtBankCde", ipa.int_bank_branch_number),
              --xmlelement("BatchNum", translate(ipa.payment_process_request_name,(REPLACE(translate(ipa.payment_process_request_name,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'??????????????????????????????????????????????????????????????????????????????????')),
              xmlelement("BatchNum", nvl((translate(ipa.payment_process_request_name,(replace(translate(ipa.payment_process_request_name,'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789?(/,)-.�:+'||chr(32), 'A'),'A' )),'??????????????????????????????????????????????????????????????????????????????????')),ipa.payment_process_request_name)),

             XMLELEMENT("country", (SELECT country from AP_CHECKS_ALL where PAYMENT_ID = ipa.payment_id and rownum =1)),
                     XMLELEMENT("Beneficary_bank_routing_code", IPA.EXT_BANK_NUMBER ),
            -- Added by Mahipal
                         XMLElement("Product_Code",'DFT'),
               XMLElement("Debit_Account_Country_Code", 'KR'),
               XMLElement("Debit_account_number", ipa.INT_bank_account_number),
               XMLElement("Payment_currency", 'KRW'),
               XMLElement("Payment_amount", REPLACE(ipa.payment_amount,'.','')||'.'),
               XMLElement("Value_date", to_char(to_date(ipa.payment_date,'DD-MON-YY'),'YYYYMMDD')),
               XMLElement("Payment_document_number", ipa.paper_document_number),
               XMLElement("Ordering_party_name", 'RIOT GAMES KOREA' ),
               XMLElement("Beneficary_Name", ipa.EXT_BANK_ACCOUNT_NAME ),
               XMLElement("Beneficary_Account_number", ipa.ext_bank_account_number ),
               XMLElement("Beneficary_bank_name", ipa.ext_bank_name ),
               XMLElement("Beneficary_bank_routing_code", ipa.ext_bank_number )))
      FROM IBY_PAYMENTS_ALL ipa
     WHERE ipa.payment_id  = p_payment_id;

BEGIN

OPEN l_pmt_ext_csr (p_payment_id);
FETCH l_pmt_ext_csr INTO l_pmt_ext_agg;
CLOSE l_pmt_ext_csr;

RETURN l_pmt_ext_agg;
  END Get_Pmt_Ext_Agg;


  --
  -- This API is called once per document payable.
  -- Implementor should construct the extract extension elements
  -- at the document level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  FUNCTION Get_Doc_Ext_Agg(p_document_payable_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Doc_Ext_Agg;


  --
  -- This API is called once per document payable line.
  -- Implementor should construct the extract extension elements
  -- at the doc line level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  -- Parameters:
  --   p_document_payable_id: primary key of IBY iby_docs_payable_all table
  --   p_line_number: calling app doc line number. For AP this is
  --   ap_invoice_lines_all.line_number.
  --
  -- The combination of p_document_payable_id and p_line_number
  -- can uniquely locate a document line.
  -- For example if the calling product of a doc is AP
  -- p_document_payable_id can locate
  -- iby_docs_payable_all/ap_documents_payable.calling_app_doc_unique_ref2,
  -- which is ap_invoice_all.invoice_id. The combination of invoice_id and
  -- p_line_number will uniquely identify the doc line.
  --
  FUNCTION Get_Docline_Ext_Agg(p_document_payable_id IN NUMBER, p_line_number IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  END Get_Docline_Ext_Agg;


  --
  -- This API is called once only for the payment process request.
  -- Implementor should construct the extract extension elements
  -- at the payment request level as a SQLX XML Aggregate
  -- and return the aggregate.
  --
  FUNCTION Get_Ppr_Ext_Agg(p_payment_service_request_id IN NUMBER)
  RETURN XMLTYPE
  IS
  BEGIN
    RETURN NULL;
  end get_ppr_ext_agg;
END IBY_FD_EXTRACT_EXT_PUB;
/


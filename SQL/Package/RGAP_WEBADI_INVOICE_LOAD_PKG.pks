CREATE OR REPLACE PACKAGE APPS.rgap_webadi_invoice_load_pkg
AS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Package is Used to upload data into staging table to AP invoice interface Table
REM
REM    Module               : RGAP
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
FUNCTION RGAP_UPLOAD_INVOICES(p_Run_id                  NUMBER,
			      p_Source       		VARCHAR2,
			      p_org_id                  NUMBER,
			      p_Org_name     		VARCHAR2,
			      p_Invoice_type 		VARCHAR2,
			      p_Vendor_name  		VARCHAR2,
			      p_Vendor_number 		VARCHAR2,
			      p_Vendor_site_name 	VARCHAR2,
			      p_Invoice_num   		VARCHAR2,
			      p_Invoice_amount     	NUMBER,
			      p_Invoice_currency_code   VARCHAR2,
			      p_Invoice_date       	    DATE,
			      p_Gl_date            	    DATE,
			      p_Description         	VARCHAR2,
			      p_Payment_method_code 	VARCHAR2,
			      p_Terms_id            	VARCHAR2,
			      p_Pay_group_lookup_code 	VARCHAR2,
			      p_Line_number         	NUMBER,
			      p_Line_Type           	VARCHAR2,
			      p_Line_amount         	NUMBER,
			      p_Gl_Account          	VARCHAR2,
   			      p_Line_Description        VARCHAR2,
			      p_text1                   VARCHAR2,
			      p_text2                   VARCHAR2,
			      p_text3                   VARCHAR2,
			      p_text4                   VARCHAR2,
			      p_text5                   VARCHAR2,
			      p_text6                   VARCHAR2,
			      p_text7                   VARCHAR2,
			      p_text8                   VARCHAR2,
			      p_text9                   VARCHAR2,
			      p_text10                  VARCHAR2,
			      p_text11                  VARCHAR2,
			      p_text12                  VARCHAR2,
			      p_text13                  VARCHAR2,
			      p_text14                  VARCHAR2,
			      p_text15                  VARCHAR2
				  )
RETURN VARCHAR2;
-- This Procedure is used to Populate data into AP Interface table.
PROCEDURE MAIN( pBatch_ID   IN NUMBER);

END RGAP_WEBADI_INVOICE_LOAD_PKG;
/


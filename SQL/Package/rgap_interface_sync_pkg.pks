CREATE OR REPLACE PACKAGE APPS.rgap_interface_sync_pkg
IS
   FUNCTION set_context (i_user_name IN VARCHAR2, i_resp_name IN VARCHAR2)
      RETURN VARCHAR2;

   PROCEDURE rgap_interface_monitor (errbuf OUT VARCHAR2, retcode OUT NUMBER);
END rgap_interface_sync_pkg;
/
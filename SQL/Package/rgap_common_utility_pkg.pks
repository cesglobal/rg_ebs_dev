CREATE OR REPLACE PACKAGE APPS.rgap_common_utility_pkg
    AUTHID DEFINER
AS
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose              : This Package is having Main Procedure for Validating the Data from Staging Table .
   --    Module               : RGAP
   --    Modification History :
    --       Date             Name             Version Number    Issue No   Revision Summary
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    14-Aug-2015    Srinivasa              1.0
   --    16-Feb-2016    Srinivasa              1.1               91 and 92 Update aginst issue added new procedure rgap_send_email to handle Email message
   --
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables
   g_log_level                  NUMBER := 0;


   FUNCTION rgap_get_html_report (p_query IN VARCHAR2)
      RETURN CLOB;

   --PROCEDURE rgap_sendmail (p_dest IN VARCHAR2, p_msg IN VARCHAR2,p_mailhost IN VARCHAR2,p_port Number,p_sender IN VARCHAR2);
   PROCEDURE rgap_sendmail (p_dest IN VARCHAR2,
                                                        p_subject IN VARCHAR2,
                                                        p_msg IN VARCHAR2,
                                                        p_mailhost   IN VARCHAR2,
                                                        p_port NUMBER,
                                                        p_sender IN VARCHAR2);

-- Start of version 1.1
  PROCEDURE rgap_send_email (p_dest IN VARCHAR2,
                                                        p_subject IN VARCHAR2,
                                                        p_msg IN VARCHAR2,
														p_msg2 IN VARCHAR2 DEFAULT NULL,
														p_msg3 IN VARCHAR2 DEFAULT NULL,
														p_msg4 IN VARCHAR2 DEFAULT NULL,
                                                        p_mailhost   IN VARCHAR2,
                                                        p_port NUMBER,
                                                        p_sender IN VARCHAR2);
-- End of version 1.1

 
 PROCEDURE  Incrm_seq(errbuf OUT VARCHAR2, 
                                             retcode OUT NUMBER,
                                             p_seq_name in varchar2,
                                             p_incrm_val in number);

END rgap_common_utility_pkg;
/
CREATE OR REPLACE PACKAGE APPS.rgpo_bursting_cp_sub_pkg
AS
   p_ou_name   VARCHAR2 (50);

   FUNCTION BeforeReport
      RETURN BOOLEAN;

   FUNCTION AfterReport (P_OU VARCHAR2)
      RETURN BOOLEAN;

   FUNCTION rgpo_rep_dt_cnt (P_OU VARCHAR2)
         RETURN number;


   FUNCTION rgpo_need_lines (p_po_num VARCHAR2)
      RETURN VARCHAR2;
END;
/


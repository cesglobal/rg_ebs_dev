CREATE OR REPLACE PACKAGE BODY APPS.rgap_coupa_vendorintf_pkg
AS
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Ram
   --    Purpose              : This Package is having Main Function to retuns Vendors information into the  Table of required columns.
   --
   --    Module               : RGAP
   --    Modification History :
   --       Date             Name             Version Number      Revision Summary
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    19-Aug-2015    Ram           1.0
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables--
   g_program_name   VARCHAR2 (30) := 'rgap_coupa_vendorintf_pkg';
   g_start_date     DATE:='01-Jan-1900';
   FUNCTION rgap_coupa_vendors_fnc (p_asof_date DATE DEFAULT NULL)
      RETURN rgap_vendor_nested_table
   IS
      l_ret          rgap_vendor_nested_table;
      l_as_of_date   DATE;
      l_run_date     DATE;
      l_def_date     DATE;
   BEGIN
      rgcomn_debug_pkg.
       log_message (
         P_LEVEL          => 0,
         P_PROGRAM_NAME   => g_program_name,
         P_MESSAGE        => 'rgap_coupa_vendors_fnc Enter Function');

      BEGIN
         SELECT lookup_code
           INTO g_org_id
           FROM applsys.fnd_lookup_values
          WHERE lookup_type LIKE 'RGAP_COUPA_VEND_OU_LKP' AND ROWNUM = 1;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            g_org_id := NULL;
         WHEN OTHERS
         THEN
            rgcomn_debug_pkg.
             log_message (P_LEVEL          => 0,
                          P_PROGRAM_NAME   => g_program_name,
                          P_MESSAGE        => 'Exception at deriving org_id');
      END;

      BEGIN
         SELECT MAX (LAST_RUN_TIME)
           INTO l_def_date
           FROM RGINTF_MONITOR_TAB
          WHERE INTERFACE_NAME = 'VEND_INTF_EBS_CP';
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            l_def_date := g_start_date;
         WHEN OTHERS
         THEN
            rgcomn_debug_pkg.
             log_message (
               P_LEVEL          => 0,
               P_PROGRAM_NAME   => g_program_name,
               P_MESSAGE        => 'Exception at deriving default date');
      END;

      SELECT CAST (
                MULTISET (
                   SELECT DISTINCT
                          VEND.VENDOR_NAME Supplier_Name,
                          VEND.VENDOR_NAME_ALT Display_Name,
                          DECODE (
                             SIGN (
                                SYSDATE - NVL (VEND.END_DATE_ACTIVE, SYSDATE)),
                             1, 'Y',
                             DECODE (
                                SIGN (
                                   SYSDATE
                                   - NVL (VEND.START_DATE_ACTIVE, SYSDATE)),
                                -1, 'Y',
                                DECODE (NVL (VEND.ENABLED_FLAG, 'Y'),
                                        'N', 'Y',
                                        'N')))
                             Inactive_Flag,
                          VEND.SEGMENT1 Supplier_Number,
                          VSITE.VENDOR_SITE_CODE Account_Number,
                          PPTY.JGZZ_FISCAL_CODE Tax_ID,
                          PPTY.TAX_REFERENCE Tax_Code,
                          PPTY.DUNS_NUMBER_C DUNS,
                          EMAIL.EMAIL_ADDRESS,
                          PHONE.RAW_PHONE_NUMBER Mobile_Number,
                          PHONE.RAW_PHONE_NUMBER Contact_Number,
                          FAX.RAW_PHONE_NUMBER Fax_Number,
                          PPTY1.person_first_name Contact_Name_Given,
                          PPTY1.person_last_name Contact_Name_Family,
                          VEND.VAT_REGISTRATION_NUM,
                          PPTY.ADDRESS1,
                          PPTY.ADDRESS2,
                          PPTY.CITY,
                          PPTY.STATE,
                          PPTY.POSTAL_CODE,
                          PPTY.COUNTRY,
                          VSITE.match_option,
                          VSITE.payment_method_lookup_code,
                          NULL Primary_Address_VAT,
                          Terms.NAME Terms,
                          NULL Invoice_Email
                     FROM AR.HZ_PARTIES# PPTY,
                          AP.AP_SUPPLIERS# VEND,
                          AR.HZ_PARTIES# PPTY1,
                          AP.AP_SUPPLIER_SITES_ALL# VSITE,
                          AR.HZ_RELATIONSHIPS# BCREL,
                          AP.AP_TERMS_TL# Terms,
                          AR.HZ_CONTACT_POINTS# FAX,
                          AR.HZ_CONTACT_POINTS# EMAIL,
                          AR.HZ_CONTACT_POINTS# PHONE
                    WHERE     VEND.VENDOR_ID = VSITE.VENDOR_ID
                          AND VEND.PARTY_ID = PPTY.PARTY_ID
                          AND BCREL.OBJECT_ID = PPTY1.PARTY_ID(+)
                          AND PPTY.PARTY_ID = BCREL.SUBJECT_ID(+)
                          AND VEND.TERMS_ID = Terms.TERM_ID(+)
                          AND Terms.LANGUAGE(+) = 'US'
                          AND PHONE.OWNER_TABLE_NAME(+) = 'HZ_PARTIES'
                          AND PHONE.OWNER_TABLE_ID(+) = BCREL.PARTY_ID
                          AND PHONE.CONTACT_POINT_TYPE(+) = 'PHONE'
                          AND PHONE.PHONE_LINE_TYPE(+) = 'GEN'
                          AND PHONE.STATUS(+) = 'A'
                          AND FAX.OWNER_TABLE_NAME(+) = 'HZ_PARTIES'
                          AND FAX.OWNER_TABLE_ID(+) = BCREL.PARTY_ID
                          AND FAX.CONTACT_POINT_TYPE(+) = 'PHONE'
                          AND FAX.PHONE_LINE_TYPE(+) = 'FAX'
                          AND FAX.STATUS(+) = 'A'
                          AND EMAIL.OWNER_TABLE_NAME(+) = 'HZ_PARTIES'
                          AND EMAIL.OWNER_TABLE_ID(+) = BCREL.PARTY_ID
                          AND EMAIL.CONTACT_POINT_TYPE(+) = 'EMAIL'
                          AND EMAIL.STATUS(+) = 'A'
                          AND (G_Org_Id IS NULL
                               OR EXISTS
                                     (SELECT 1
                                        FROM applsys.fnd_lookup_values
                                       WHERE lookup_code = vsite.org_id
                                             AND lookup_type LIKE
                                                    'RGAP_COUPA_VEND_OU_LKP'))
                          AND NVL (GREATEST (
                                         NVL (vend.last_update_date,
                                              g_start_date),
                                         NVL (ppty.last_update_date,
                                              g_start_date),
                                         NVL (ppty1.last_update_date,
                                              g_start_date),
                                         NVL (vsite.last_update_date,
                                              g_start_date),
                                         NVL (fax.last_update_date,
                                              g_start_date),
                                         NVL (terms.last_update_date,
                                              g_start_date),
                                         NVL (email.last_update_date,
                                              g_start_date),
                                         NVL (phone.last_update_date,
                                              g_start_date)),
                                   SYSDATE) >
                                 NVL (p_asof_date, g_start_date)
                          AND (p_asof_date IS NOT NULL
                               OR (NVL (
                                      GREATEST (
                                         NVL (vend.last_update_date,
                                              l_def_date),
                                         NVL (ppty.last_update_date,
                                              l_def_date),
                                         NVL (ppty1.last_update_date,
                                              l_def_date),
                                         NVL (vsite.last_update_date,
                                              l_def_date),
                                         NVL (fax.last_update_date,
                                              l_def_date),
                                         NVL (terms.last_update_date,
                                              l_def_date),
                                         NVL (email.last_update_date,
                                              l_def_date),
                                         NVL (phone.last_update_date,
                                              l_def_date)),
                                      SYSDATE) >
                                      (SELECT LAST_RUN_TIME
                                         FROM RGINTF_MONITOR_TAB
                                        WHERE INTERFACE_NAME =
                                                 G_Interface_name)))) AS rgap_vendor_nested_table)
        INTO l_ret
        FROM DUAL;

      SELECT NVL(p_asof_date,SYSDATE) INTO l_run_date FROM DUAL;

      rgap_extract_result (l_run_date, 'Success');

      RETURN (l_ret);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         rgap_extract_result (l_run_date, 'Failure');

         rgcomn_debug_pkg.
          log_message (P_LEVEL          => 0,
                       P_PROGRAM_NAME   => g_program_name,
                       P_MESSAGE        => 'NO Data Found ' || SQLERRM);
      WHEN OTHERS
      THEN
         rgap_extract_result (l_run_date, 'Failure');
         rgcomn_debug_pkg.
          log_message (
            P_LEVEL          => 0,
            P_PROGRAM_NAME   => g_program_name,
            P_MESSAGE        =>   'Others Exception Raised'
                               || '-'
                               || SQLCODE
                               || '  '
                               || SQLERRM);
   END rgap_coupa_vendors_fnc;


   PROCEDURE rgap_extract_result (p_date DATE, p_result VARCHAR2)
   IS
      PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
      UPDATE RGINTF_MONITOR_TAB
         SET LAST_RUN_TIME = p_date,
             last_update_date = SYSDATE,
             last_updated_by = fnd_global.user_id,
             run_status = p_result
       WHERE INTERFACE_NAME = G_Interface_name;

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         rgcomn_debug_pkg.
          log_message (
            P_LEVEL          => 0,
            P_PROGRAM_NAME   => g_program_name,
            P_MESSAGE        =>   'Others Exception Raised at update'
                               || '-'
                               || SQLCODE
                               || '  '
                               || SQLERRM);
   END rgap_extract_result;
END rgap_coupa_vendorintf_pkg;
/


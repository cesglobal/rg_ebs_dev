CREATE OR REPLACE PACKAGE BODY APPS.rgap_interface_sync_pkg IS
/* ------------------------------------------------------------------------------------------------------------------------------------------------------
 -- Author : Srinivasa
 -- Purpose : This Package will be used for Auto scheduling Riot Custom Inbound interfaces
 --
 -- Module : RGAP
 -- Interface Tables :
 -- Modification History :
 -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- Date Name Version Number Issue No 		 Revision 			Summary
 -- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
 -- 4-Jul-2016 Srinivasa 1.0										Initial Version
 -- 25-Jul-2016 Srinivasa 1.1  										Added Login to include rtf template Logic for XMLP reports.
 -- 14-Aug-2016 Srinivasa 1.2  										Added Email logic for success and failures.

 -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
 -- Global Variables--
   g_mailhost          VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   g_port              NUMBER := fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT');
   g_sender            VARCHAR2 (100) := 'donotreply@riotgames.com';
   g_intf_subject      varchar2(150);
   g_Interface_Email   VARCHAR2 (40); 
   g_orcl_Email        VARCHAR2 (40);
   
   FUNCTION set_context (i_user_name IN VARCHAR2, i_resp_name IN VARCHAR2) RETURN VARCHAR2 IS
   
      v_user_id        NUMBER;
      v_resp_id        NUMBER;
      v_resp_appl_id   NUMBER;
      v_return         VARCHAR2 (10) := 'T';

      /* Cursor to get the user id information based on the input user name */
      CURSOR cur_user IS
         SELECT user_id
           FROM fnd_user
          WHERE user_name = i_user_name;

      /* Cursor to get the responsibility information */
      CURSOR cur_resp
      IS
         SELECT responsibility_id, application_id
           FROM fnd_responsibility_tl
          WHERE responsibility_name = i_resp_name;
   BEGIN
      /* To get the user id details */
      OPEN cur_user;

      FETCH cur_user INTO v_user_id;

      IF cur_user%NOTFOUND
      THEN
         v_return := 'F';
      END IF;                                           --IF cur_user%NOTFOUND

      CLOSE cur_user;

      /* To get the responsibility and responsibility application id */
      OPEN cur_resp;

      FETCH cur_resp
      INTO v_resp_id, v_resp_appl_id;

      IF cur_resp%NOTFOUND
      THEN
         v_return := 'F';
      END IF;                                           --IF cur_resp%NOTFOUND

      CLOSE cur_resp;

      /* Setting the oracle applications context for the particular session */
      fnd_global.
       apps_initialize (user_id        => v_user_id,
                        resp_id        => v_resp_id,
                        resp_appl_id   => v_resp_appl_id);

      /* Setting the org context for the particular session */
      mo_global.set_policy_context ('S', fnd_profile.VALUE ('ORG_ID'));
      RETURN v_return;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 'F';
   END set_context;

   PROCEDURE rgap_interface_monitor (errbuf OUT VARCHAR2, retcode OUT NUMBER)
   IS
      v_request_id          NUMBER DEFAULT 0;
      v_context             VARCHAR2 (100);
      v_user_name           VARCHAR2 (100);
      v_responsibility      VARCHAR2 (200);
      v_conc_short_name     VARCHAR2 (150);
      v_application         VARCHAR2 (20);
      v_conc_desc           VARCHAR2 (250);
      v_param_count         NUMBER DEFAULT 0;
      l_request_set_exist   BOOLEAN := FALSE;
      l_request_id          INTEGER := 0;
      l_set_mode            BOOLEAN := FALSE;
      l_CONC_PROG_SUBMIT    BOOLEAN := FALSE;
      V_REQUEST_SET_EXIST   BOOLEAN := FALSE;
      srs_failed            EXCEPTION;
      submitprog_failed     EXCEPTION;
      setmode_failed        EXCEPTION;
      submitset_failed      EXCEPTION;
      l_start_date          VARCHAR2 (250);
      l_req_data            VARCHAR2 (10);
      l_wait_for_req        BOOLEAN;
      l_phase               VARCHAR2 (100);
      l_status              VARCHAR2 (100);
      l_dev_phase           VARCHAR2 (100);
      l_dev_status          VARCHAR2 (100);
      p_msg                 VARCHAR2(4000);
      p_errbuf              VARCHAR2 (4000);
      l_boolean  			BOOLEAN;
	  l_date                DATE;
	  l_run_date            DATE;



      /* Cursor to get the user id information based on the input user name */
      CURSOR cur_intf IS
         SELECT intf_run_by int_user,
                resp_to_run_intf int_resp,
                intf_object_name conc_name,
                request_type, 
                intf_argument1,
                intf_argument2,
                intf_argument3,
                intf_argument4,
                intf_argument5,
                intf_argument6,
                intf_argument7,
                intf_argument8,
                intf_argument9,
                intf_argument10,
                interface_name,
				interface_id
           FROM RGINTF_MONITOR_TAB
                WHERE UPPER (run_status) = 'READY'
                AND INTF_OBJECT_NAME IS NOT NULL;

      CURSOR cur_rs_stg IS
         SELECT intf_run_by int_user,
                resp_to_run_intf int_resp,
                intf_object_name conc_name,
                request_type,
                intf_argument1,
                intf_argument2,
                intf_argument3,
                intf_argument4,
                intf_argument5,
                intf_argument6,
                intf_argument7,
                intf_argument8,
                intf_argument9,
                intf_argument10,
                interface_name,
				interface_id
           FROM RGINTF_MONITOR_TAB
          WHERE UPPER (run_status) = 'READY'
                AND intf_object_name IS NOT NULL
                AND request_type IS NOT NULL;

      CURSOR cur_rs_prog (p_req_set VARCHAR2) IS
         SELECT fa.application_short_name,
                fcp.concurrent_program_name,
                STAGE_NAME
           FROM FND_REQUEST_SETS FRS,
                        FND_REQUEST_SET_STAGES FRSS,
                        FND_REQUEST_SET_PROGRAMS FRR,
                        fnd_concurrent_programs fcp,
                        fnd_application fa
          WHERE     FRS.request_set_id = FRSS.request_set_id
                AND frss.request_set_id = FRR.request_set_id
                AND frss.REQUEST_SET_STAGE_ID = frr.REQUEST_SET_STAGE_ID
                AND frr.concurrent_program_id = fcp.concurrent_program_id
                AND fcp.application_id = fa.application_id
                AND frs.request_set_name = p_req_set;
--- Version 1.1 Change Start 
     CURSOR cur_prog_template (p_req_name VARCHAR2) IS
        SELECT XDDV.data_source_code ,
       XDDV.data_source_name "Data_Definition",
       XDDV.description "Data_Definition_Description",
       XTT.template_name "Template_Name",
       XTT.description,
       XTB.template_type_code ,
       XL.file_name,
       xtb.application_short_name,
       xtb.template_code,
       xtb.default_language,
       xtb.DEFAULT_TERRITORY,
       xtb.DEFAULT_OUTPUT_TYPE
  FROM apps.XDO_DS_DEFINITIONS_VL XDDV,
       apps.XDO_TEMPLATES_B XTB,
       apps.XDO_TEMPLATES_TL XTT,
       apps.XDO_LOBS XL,
       apps.FND_APPLICATION_TL FAT,
       apps.FND_APPLICATION FA
 WHERE     XDDV.data_source_code =p_req_name
       AND XDDV.application_short_name = FA.application_short_name
       AND FAT.application_id = FA.application_id
       AND XTB.application_short_name = XDDV.application_short_name
       AND XDDV.data_source_code = XTB.data_source_code
       AND XTT.template_code = XTB.template_code
       AND XL.lob_code = XTB.template_code
       AND XL.xdo_file_type = XTB.template_type_code;
--- Version 1.1 Change End  
   BEGIN
      l_date:=sysdate;
	  Select  'Riot Interface Schedule Job ' into g_intf_subject from dual;
      Select meaning into g_Interface_Email from fnd_lookup_values  where lookup_type='RGAP_INTF_SYNC_NTF_MAIL' and lookup_code='INTF_MAIL' and rownum=1;
      Select meaning into g_orcl_Email from fnd_lookup_values  where lookup_type='RGAP_INTF_SYNC_NTF_MAIL' and lookup_code='ORCL_MAIL' and rownum=1;
     FOR rec_intf IN cur_intf
      LOOP
         BEGIN
            BEGIN
               SELECT fa.application_short_name, fct.description
                 INTO v_application, v_conc_desc
                 FROM fnd_application fa,
                             fnd_concurrent_programs fcp,
                             fnd_concurrent_programs_tl fct
                WHERE fa.application_id = fcp.application_id
                      AND fct.concurrent_program_id = fcp.concurrent_program_id
                      AND fct.language = 'US'
                      AND UPPER (concurrent_program_name) LIKE UPPER (rec_intf.conc_name);
            END;

            BEGIN
               SELECT COUNT (par.end_user_column_name)
                 INTO v_param_count
                 FROM fnd_concurrent_programs fcp,
                      fnd_descr_flex_col_usage_vl par
                WHERE UPPER (fcp.concurrent_program_name) = UPPER (rec_intf.conc_name)
                      AND NVL (fcp.Enabled_flag, 'Y') = 'Y'
                      AND par.descriptive_flexfield_name = '$SRS$.' || fcp.concurrent_program_name;
            END;

            -- Setting the context ----
            v_context := set_context (rec_intf.int_user, rec_intf.int_resp);

            IF v_context = 'F' THEN
               Fnd_File.put_line (Fnd_File.LOG, 'Error while setting the context');
            END IF;

            Fnd_File.
             put_line (Fnd_File.LOG, 'Submit Interface Concurrent Program');

            IF v_param_count = 0 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL);
                                                                                                       
            ELSIF v_param_count = 1 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1);
                                   
            ELSIF v_param_count = 2 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2);
            ELSIF v_param_count = 3 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3);
                                                                                                       
            ELSIF v_param_count = 4 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4);
                                                                                                       
            ELSIF v_param_count = 5 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5);
                                                                                                       
            ELSIF v_param_count = 6 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5,
                                                                                                       argument6     => rec_intf.intf_argument6);
                                                                                                       
            ELSIF v_param_count = 7 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5,
                                                                                                       argument6     => rec_intf.intf_argument6,
                                                                                                       argument7     => rec_intf.intf_argument7);
                                                                                                       
            ELSIF v_param_count = 8 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5,
                                                                                                       argument6     => rec_intf.intf_argument6,
                                                                                                       argument7     => rec_intf.intf_argument7,
                                                                                                       argument8     => rec_intf.intf_argument8);
                                                                                                       
            ELSIF v_param_count = 9 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5,
                                                                                                       argument6     => rec_intf.intf_argument6,
                                                                                                       argument7     => rec_intf.intf_argument7,
                                                                                                       argument8     => rec_intf.intf_argument8,
                                                                                                       argument9     => rec_intf.intf_argument9);
                                                                                                       
            ELSIF v_param_count = 10 THEN
            
               v_request_id := FND_REQUEST.SUBMIT_REQUEST (application   => v_application,
                                                                                                       program       => rec_intf.conc_name,
                                                                                                       description   => v_conc_desc,
                                                                                                       start_time    => NULL,
                                                                                                       sub_request   => NULL,
                                                                                                       argument1     => rec_intf.intf_argument1,
                                                                                                       argument2     => rec_intf.intf_argument2,
                                                                                                       argument3     => rec_intf.intf_argument3,
                                                                                                       argument4     => rec_intf.intf_argument4,
                                                                                                       argument5     => rec_intf.intf_argument5,
                                                                                                       argument6     => rec_intf.intf_argument6,
                                                                                                       argument7     => rec_intf.intf_argument7,
                                                                                                       argument8     => rec_intf.intf_argument8,
                                                                                                       argument9     => rec_intf.intf_argument9,
                                                                                                       argument10     => rec_intf.intf_argument10);
                                                                                                       
            END IF;

            COMMIT;

            DBMS_OUTPUT.PUT_LINE ('Request_id: ' || v_request_id);
            
            Fnd_File.put_line (Fnd_File.LOG, 'Request Submitted ' || v_request_id);

            UPDATE RGINTF_MONITOR_TAB
               SET RUN_STATUS = 'Success',
                   LAST_RUN_TIME = SYSDATE,
                   ATTRIBUTE1 = v_request_id
             WHERE interface_id = rec_intf.interface_id;
            COMMIT;
            
         EXCEPTION
            WHEN OTHERS THEN
               Fnd_File.
                put_line (Fnd_File.LOG, SQLCODE || ' Error :' || SQLERRM);
         END;
         
      END LOOP;

      FOR rec_rs_stage IN cur_rs_stg
      LOOP
         -- Setting the context ----
         v_context :=
            set_context (rec_rs_stage.int_user, rec_rs_stage.int_resp);

         l_set_mode := fnd_submit.set_mode (FALSE);

         IF (NOT l_set_mode) THEN
            RAISE setmode_failed;
         END IF;

         l_req_data := fnd_conc_global.request_data;


         IF v_context = 'F' THEN
            Fnd_File.
             put_line (Fnd_File.LOG, 'Error while setting the context');
         END IF;

         fnd_file.put_line (fnd_file.LOG, 'Calling set_request_set');
         
         V_REQUEST_SET_EXIST := FND_SUBMIT.set_request_set (application   => 'RGGL',
                                                                                                              request_set   => rec_rs_stage.conc_name);

         IF (NOT V_REQUEST_SET_EXIST) THEN
            RAISE srs_failed;
         END IF;

         FOR rec_rs_prog IN cur_rs_prog (rec_rs_stage.conc_name)
         LOOP
         
            fnd_file.put_line (fnd_file.LOG, 'Calling submit program stage ' || rec_rs_prog.STAGE_NAME);
			--- Version 1.1 Change Start
			For  rec_prog_template in cur_prog_template(rec_rs_prog.concurrent_program_name) 
			LOOP
			Fnd_File.put_line (Fnd_File.LOG, 'Setting up rtf template for '||rec_rs_prog.concurrent_program_name||','||rec_prog_template.template_code);
			   l_boolean :=
				fnd_submit.add_layout
			   (template_appl_name      => rec_prog_template.application_short_name,
				template_code           => rec_prog_template.template_code,
				template_language       => rec_prog_template.default_language,
				template_territory      => rec_prog_template.DEFAULT_TERRITORY,
				output_format           => rec_prog_template.DEFAULT_OUTPUT_TYPE
			   );
			 IF NOT l_boolean  THEN
				Fnd_File.put_line (Fnd_File.LOG, 'Adding Teamplate failed: ' || SQLERRM);
			 END IF;		 
			END LOOP;
			--- Version 1.1 Change End
            			
            l_CONC_PROG_SUBMIT := fnd_submit.submit_program (                                          rec_rs_prog.application_short_name,
                                                                                                                  rec_rs_prog.concurrent_program_name,
                                                                                                                  rec_rs_prog.stage_name);
			commit;																									  
	Fnd_File.put_line (Fnd_File.LOG, 'Setting up rtf template with Request ID ');
		
            IF (NOT l_CONC_PROG_SUBMIT) THEN
               RAISE submitprog_failed;
            END IF;
         END LOOP;

         fnd_file.put_line (fnd_file.LOG, 'Calling submit_set');

         --l_start_date is to schedule the request
         SELECT TO_CHAR (SYSDATE, 'DD - MON - YYYY HH24:MI:SS')
           INTO l_start_date
           FROM DUAL;

         l_request_id :=
            FND_SUBMIT.
             submit_set (start_time => l_start_date, sub_request => FALSE);
          v_request_id:=l_request_id;
         IF (l_request_id = 0)
         THEN
            RAISE submitset_failed;
         END IF;

         COMMIT;

         DBMS_OUTPUT.PUT_LINE ('Request_id: ' || l_request_id);
         Fnd_File.
          put_line (Fnd_File.LOG, 'Request Submitted ' || l_request_id);

         UPDATE RGINTF_MONITOR_TAB
            SET RUN_STATUS = 'Success',
                LAST_RUN_TIME = SYSDATE,
                ATTRIBUTE1 = v_request_id
          WHERE interface_id = rec_rs_stage.interface_id;


         l_wait_for_req :=
            fnd_concurrent.wait_for_request (request_id   => l_request_id,
                                             interval     => 2,
                                             max_wait     => 120,
                                             phase        => l_phase,
                                             status       => l_status,
                                             dev_phase    => l_dev_phase,
                                             dev_status   => l_dev_status,
                                             message   =>  p_msg);
      END LOOP;
      -- Version 1.2 Start
	  SELECT max(LAST_RUN_TIME) into l_run_date from RGINTF_MONITOR_TAB where INTF_DIRECTION='INBOUND';
	  
	  IF l_date> l_run_date then
	   Fnd_File.put_line (Fnd_File.LOG, 'No Interface(s) found for submition.');
       
         rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_Interface_Email,
            p_subject    => g_intf_subject,
            p_msg        => 'Interface scheduling job on EBS side ran successfully, there was no record in monitor table in Ready status to process.',
            p_msg2       => NULL,
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);
          ELSE
			Fnd_File.put_line (Fnd_File.LOG, 'Interface(s) found for submition.'||'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' AND last_run_time>'||''''||l_date||'''');
       
         rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_Interface_Email,
            p_subject    => g_intf_subject,
            p_msg        => rgap_common_utility_pkg.
                            rgap_get_html_report (
                              'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' AND last_run_time>= to_date('||''''||to_char(l_date,'DD-MON-YYYY HH24:MI:SS')||''',''DD-MON-YYYY HH24:MI:SS'')'),
            p_msg2       => 'Interface scheduling job on EBS side ran successfully. Following interface job(s) were submitted in this run.',
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);

 	END IF;

   EXCEPTION
      WHEN srs_failed
      THEN
         p_errbuf := 'Call to set_request_set failed: ' || fnd_message.get;
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
          rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_orcl_Email,
            p_subject    => g_intf_subject,
            p_msg        => rgap_common_utility_pkg.
                            rgap_get_html_report ( 'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' and upper(Run_Status)=''READY'''),
            p_msg2       => 'Interface scheduling job on EBS side could not submit the interface job.',
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);
        
      WHEN submitprog_failed
      THEN
         p_errbuf := 'Call to submit_program failed: ' || fnd_message.get;
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
          rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_orcl_Email,
            p_subject    => g_intf_subject,
            p_msg        => rgap_common_utility_pkg.
                            rgap_get_html_report ( 'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' and upper(Run_Status)=''READY'''),
            p_msg2       => 'Interface scheduling job on EBS side could not submit the interface job.',
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);
         
      WHEN submitset_failed
      THEN
         p_errbuf := 'Call to submit_set failed:' || fnd_message.get;
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
          rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_orcl_Email,
            p_subject    => g_intf_subject,
            p_msg        => rgap_common_utility_pkg.
                            rgap_get_html_report ( 'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' and upper(Run_Status)=''READY'''),
            p_msg2       => 'Interface scheduling job on EBS side could not submit the interface job.',
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);
         
      WHEN OTHERS
      THEN
         p_errbuf :=
            'Request set submission failed unknown error: ' || SQLERRM;
         retcode := 2;
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
         fnd_file.put_line (fnd_file.LOG, p_errbuf);
          rgap_common_utility_pkg.
          rgap_send_email (
            p_dest       => g_orcl_Email,
            p_subject    => g_intf_subject,
            p_msg        => rgap_common_utility_pkg.
                            rgap_get_html_report ( 'select Interface_Id "Interface_ID",INTF_DESCRIPTION "Interface_Name",attribute1 "Request_Id" from RGINTF_MONITOR_TAB WHERE INTF_DIRECTION=''INBOUND'' and upper(Run_Status)=''READY'''),
            p_msg2       => 'Interface scheduling job on EBS side could not submit the interface job.',
            p_msg3       => NULL,
            p_mailhost   => g_mailhost,
            p_port       => g_port,
            p_sender     => g_sender);
    -- Version 1.2 End     
   END rgap_interface_monitor;
   
   
END rgap_interface_sync_pkg;
/
CREATE OR REPLACE PACKAGE BODY APPS.rgar_invoice_upload_pkg AS
-- ** =====================================================
-- ** Copyright 2015 Riot Games Inc., Los Angeles, CA
-- ** All Right reserved.
-- ** EBS Greenfiled Implementation
-- ** =====================================================
-- This process will create transactions (invocies) in EBS AR system acceptiong
-- user input through Web ADI.
-- -----------------------------------------------------------------------------------
-- Programmer: Mahipal Ardam
-- -----------------------------------------------------------------------------------
--
-- Reference Documents
-- XXXX - Functional Design
-- XXXX - Technical Design
-- Please refer technical design for process flow and program flow.
-- ------------------------------------------------------------------------------------
--
-- Message Levels
-- ------------------
-- Main Program - 5
-- Exceptions - 2
--
-- Statuses
-- ----------
-- N - New
-- S - Success
-- E - Error/Exception
--
-- O B J E C T    D E T A I L S
-- -----------------------------------
-- Brief description of each object within this package is given below.
-- Refer Technical Design Document for further details.
-- Added two columns as CR on 04-Feb-2016 Exchnage rate type and Exchange rate.
-- Modified code to handle mutliple batches with mutliple customers in single upload.
--


 FUNCTION RGAR_INV_UPL(P_GROUP1NV_ID     IN NUMBER,
                       P_Legal_Entity    IN VARCHAR2,
                       P_Customer_Name   IN VARCHAR2,
                       P_INVOICE_DATE    IN DATE,
                       P_GL_DATE         IN DATE,
                       P_Currency_code   IN VARCHAR2,
                       P_Class           IN VARCHAR2,
                       P_Invoice_type    IN VARCHAR2,
                       P_Description     IN VARCHAR2,
                       P_Amount          IN NUMBER,
                       P_Reference       IN VARCHAR2,
                       P_Segment1        IN VARCHAR2,
                       P_Segment2        IN VARCHAR2,
                       P_Segment3        IN VARCHAR2,
                       P_Segment4        IN VARCHAR2,
                       P_Segment5        IN VARCHAR2,
                       P_Segment6        IN VARCHAR2,
                       P_Segment7        IN VARCHAR2,
                       P_Segment8        IN VARCHAR2,
                       P_Text1           IN VARCHAR2,  -- Term id
                       P_Text2           IN VARCHAR2,  -- Batch Name
                       P_Text3           IN VARCHAR2,  -- Exchange Rate Type
                       P_Text4           IN VARCHAR2,  -- Exchnage Rate
                       P_Text5           IN VARCHAR2,
                       P_Text6           IN DATE,
                       P_Text7           IN VARCHAR2,
                       P_Text8           IN VARCHAR2,
                       P_Text9           IN VARCHAR2,
                       P_Text10          IN VARCHAR2,
                       P_Text11          IN VARCHAR2,
                       P_Text12          IN VARCHAR2,
                       P_Text13          IN VARCHAR2,
                       P_Text14          IN VARCHAR2,
                       P_Text15          IN VARCHAR2) RETURN VARCHAR2 AS

        vError_Msg       VARCHAR2(2000):= NULL;
        vSys_Date        DATE      := SYSDATE;
        vcustomer_id     NUMBER;
        vCust_bill_to_id NUMBER;
        vOrg_id          NUMBER:= FND_PROFILE.VALUE('org_id');
        l_module_name   VARCHAR2 (40) := 'RGAR_INV_UPL';

        BEGIN

                rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Start of rgar_inv_upl Function');

            BEGIN

              SELECT hcaua.cust_acct_site_id
                INTO vCust_bill_to_id
                FROM hz_parties hp,
                     hz_cust_accounts_all hca,
                     hz_cust_acct_sites_all hcasa,
                     hz_cust_site_uses_all hcaua,
                     hz_party_sites hps,
                     hz_locations hl
               WHERE hp.party_id = hca.party_id
                 AND hca.cust_account_id = hcasa.cust_account_id
                 AND hcasa.cust_acct_site_id = hcaua.cust_acct_site_id
                 AND hps.party_id = hp.party_id
                 AND hcaua.site_use_code = 'BILL_TO'
                 AND hcaua.primary_flag = 'Y'
                 AND hcaua.org_id =  vOrg_id
                 AND hca.cust_account_id = P_Customer_Name
                 AND hps.location_id = hl.location_id
                 AND hps.IDENTIFYING_ADDRESS_FLAG ='Y';

                    rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Cust Bill to Id: '||vCust_bill_to_id);

            EXCEPTION
                WHEN OTHERS THEN
                vError_Msg := vError_Msg ||' '||'Bill to not found for pass Customer';

                rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving cust bill to id: '||sqlcode);

                rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving cust bill to id: '||sqlerrm);
            END;

            IF P_Text3 = 'User' and P_Text4 IS NULL
            THEN
            vError_Msg := vError_Msg ||' '||'Exchange Type is User, Please pass Exchange Rate';
            END IF;


            IF (vError_Msg IS NULL) THEN

                 rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Inserting record into stage table RGAR_INV_UPLOAD_STG');

                BEGIN
                    INSERT INTO RGAR_INV_UPLOAD_STG
                    (groupinv_id,
                     legal_entity,
                     customer_name,
                     Cust_bill_to_id,
                     invoice_date,
                     Gl_date,
                     currency_code,
                     Invoice_class,
                     Invoice_type,
                     Description,
                     amount,
                     reference,
                     segment1,
                     segment2,
                     segment3,
                     segment4,
                     segment5,
                     segment6,
                     segment7,
                     segment8,
                     attribute1, -- Term Id
                     attribute2, -- Batch Name
                     attribute3, -- Exchange Rate Type
                     attribute4, -- Exchnage Rate
                     attribute5, -- Call Reference
                     attribute6, -- Conversion Date
                     attribute7,
                     attribute8,
                     attribute9,
                     attribute10,
                     attribute11,
                     attribute12,
                     attribute13,
                     attribute14,
                     attribute15,
                     org_id,
                     status,
                     created_by ,
                     creation_date,
                     last_update_date,
                     last_updated_by
                    )
                    VALUES
                    (P_GROUP1NV_ID,
                     P_Legal_Entity,
                     P_Customer_Name,
                     vCust_bill_to_id,
                     P_INVOICE_DATE,
                     P_GL_DATE,
                     P_Currency_code,
                     P_Class,
                     P_Invoice_type,
                     P_Description,
                     P_Amount,
                     P_Reference,
                     P_Segment1,
                     P_Segment2,
                     P_Segment3,
                     P_Segment4,
                     P_Segment5,
                     P_Segment6,
                     P_Segment7,
                     P_Segment8,
                     P_Text1,
                     P_Text2,
                     P_Text3,
                     P_Text4,
                     P_Text5,
                     P_Text6,
                     P_Text7,
                     P_Text8,
                     P_Text9,
                     P_Text10,
                     P_Text11,
                     P_Text12,
                     P_Text13,
                     P_Text14,
                     P_Text15,
                     vOrg_id,
                     'N',
                     FND_GLOBAL.USER_ID,
                     vSys_Date,
                     vSys_Date,
                     FND_GLOBAL.USER_ID
                    );

                EXCEPTION
                    WHEN OTHERS THEN
                     vError_Msg := 'Unexpected error while inserting records: '||SQLERRM;

                     rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                P_PROGRAM_NAME   => l_module_name,
                                                                                P_MESSAGE        => 'Error while inserting into stage table: '||sqlcode);

                    rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                P_PROGRAM_NAME   => l_module_name,
                                                                                P_MESSAGE        => 'Error while inserting into stage table: '||sqlerrm);
                END;

            IF vError_Msg IS NULL THEN
                  RETURN NULL;
                ELSE
                  RETURN vError_Msg;
                END IF;
            ELSE
                IF INSTR(vError_Msg,'_') = 1 THEN
                  vError_Msg            := SUBSTR(vError_Msg,2);
                END IF;
                RETURN vError_Msg;
            END IF;
        EXCEPTION
        WHEN OTHERS THEN
                vError_Msg              := vError_Msg ||'_'||'Unexpected error while loading AP Invoices : '||SQLERRM;

                 rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error in rgar_inv_upl: '||sqlcode);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error in rgar_inv_upl: '||sqlerrm);

              IF INSTR(vError_Msg,'_') = 1 THEN
                vError_Msg            := SUBSTR(vError_Msg,2);
              END IF;
              RETURN vError_Msg;


        END RGAR_INV_UPL;

PROCEDURE RGAR_RECEIVABLE_IMPORT_PRG(pgroup_id IN NUMBER) IS

        lv_wait_req_id         BOOLEAN;
        lv_phase               VARCHAR2(100);
        lv_status              VARCHAR2(100);
        lv_dev_phase           VARCHAR2(100);
        lv_dev_status          VARCHAR2(100);
        lv_msg                 VARCHAR2(2000);
        lv_valid_request_id    NUMBER;
        lv_request_id          NUMBER;
        lv_vphase               VARCHAR2(100);
        lv_vstatus             VARCHAR2(100);
        lv_vdev_phase          VARCHAR2(100);
        lv_vdev_status         VARCHAR2(100);
        lv_vmsg                VARCHAR2(2000);
        lv_org_id              NUMBER := FND_PROFILE.VALUE('ORG_ID');
        vbatch_id              NUMBER;
        x_request_id           NUMBER;
        vl_trx_date            VARCHAR2(100);
        l_module_name   VARCHAR2 (40) := 'RGAR_RECEIVABLE_IMPORT_PRG';
        v_status_count         NUMBER := 0;


        BEGIN

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Start of rgar_receivable_import_prg');

             rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Submitting Concurrent Program to create Transactions');


            lv_request_id := fnd_request.submit_request (
                                    application   => 'RGAR',
                                    program       => 'RGAR_CREATE_INVOICE_BATCHES',
                                    description   => NULL,
                                    start_time    => sysdate,
                                    sub_request   => FALSE,
                                    argument1     => pgroup_id);

                rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Request Id: '||lv_request_id);

            /*BEGIN
                 SELECT batch_source_id
                   INTO vbatch_id
               FROM ra_batch_sources_all
              WHERE name = 'ADI UPLOAD RGI_US_USD_OU'
                AND org_id = lv_org_id;
                         --AND ROWNUM = 1;
            EXCEPTION
                WHEN OTHERS THEN
                vbatch_id := NULL;
              BEGIN
                SELECT batch_source_id
                    INTO vbatch_id
                FROM ra_batch_sources_all
                WHERE name = 'ADI UPLOAD RGI_US_USD_OU'
                  AND ROWNUM = 1;
              EXCEPTION
                WHEN OTHERS THEN
                vbatch_id :=NULL;
            END;
            END;

          BEGIN
            SELECT to_char(to_date(GL_DATE,'DD-MON-YY'),'YYYY/MM/DD HH24:MI:SS')
              INTO vl_trx_date
              FROM RGAR_INV_UPLOAD_STG
             WHERE groupinv_id = pgroup_id
               AND ROWNUM =1;
          EXCEPTION
            WHEN OTHERS THEN
            vl_trx_date := TO_CHAR (TRUNC (SYSDATE),'YYYY/MM/DD HH24:MI:SS');
          END;

             fnd_global.apps_initialize(  fnd_profile.value('user_id')
                                               , fnd_profile.value('resp_id')
                                               , fnd_profile.value('resp_appl_id')
                                               );
                lv_request_id := fnd_request.submit_request ('AR',
                                                             'RAXMTR',
                                                             NULL,
                                                             TO_CHAR (TRUNC (SYSDATE),'YYYY/MM/DD HH24:MI:SS'),
                                                             FALSE,
                                                             '1',
                                                             lv_org_id,
                                                             vbatch_id, --v_batch_data.batch_source_id,
                                                             'ADI UPLOAD RGI_US_USD_OU', --v_batch_data.NAME,
                                                             vl_trx_date, --TO_CHAR (TRUNC (SYSDATE),'YYYY/MM/DD HH24:MI:SS'),
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             NULL,
                                                             'Y',
                                                             NULL
                                                            );*/
              COMMIT;
              IF lv_request_id > 0 THEN
                 x_request_id := lv_request_id;


                    <<LOOP_START>>
                    SELECT COUNT(1)
                      INTO v_status_count
                      FROM fnd_concurrent_requests
                     WHERE request_id = lv_request_id
                       AND  phase_code = 'C'
                       AND  status_code = 'C';

                    IF v_status_count = 0
                    THEN

                        GOTO LOOP_START;

                    ELSE
                         BEGIN
                            INSERT INTO RGAR_INV_UPLOAD_HIST  (SELECT *
                                                                FROM RGAR_INV_UPLOAD_STG
                                                               WHERE GROUPINV_ID NOT IN (SELECT GROUPINV_ID
                                                                                           FROM RGAR_INV_UPLOAD_HIST));
                        COMMIT;
                        EXCEPTION
                            WHEN OTHERS THEN
                        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into RGAR_INV_UPLOAD_HIST History table:'||SQLERRM);
                        END;

                        BEGIN
                            EXECUTE IMMEDIATE 'TRUNCATE TABLE RGAR.RGAR_INV_UPLOAD_STG';
                        EXCEPTION
                            WHEN OTHERS THEN
                            FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while Truncating table :'||SQLERRM);
                        END;

                    END IF;

              ELSE
                        x_request_id := 0;

              END IF;

                /*    lv_wait_req_id:=fnd_concurrent.wait_for_request(  request_id  => lv_request_id
                                                                      , interval    => 10
                                                                      , max_wait    => 0
                                                                      , phase       => lv_vphase
                                                                      , status      => lv_vstatus
                                                                      , dev_phase   => lv_vdev_phase
                                                                      , dev_status  => lv_vdev_status
                                                                      , message     => lv_vmsg
                                                                       );
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_phase:   '||lv_vdev_phase);
                    --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'lv_dev_phase:   '||lv_vdev_phase);
                      IF lv_vdev_phase    = 'COMPLETE'
                      THEN
                          IF lv_vdev_status = 'WARNING'
                          OR lv_vdev_status = 'ERROR'
                          THEN
                              FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_status: ' || lv_vdev_status);
                              ----rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE =>'lv_dev_status: ' || lv_vdev_status);
                          ELSIF lv_vdev_status = 'NORMAL'
                          THEN
                              FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_status: ' || lv_vdev_status);
                              --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE =>'lv_dev_status: ' || lv_vdev_status);
                          END IF;
                      END IF;*/

        EXCEPTION
            WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occured while submitting Receivable Import Program(rgar_receivable_import_prg Procedure): ' || SQLERRM);

           rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Unexpected error in rgar_recivalbe_import_pkg-'|| '-'|| SQLCODE);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Unexpected error in rgar_recivalbe_import_pkg-'|| '-'|| SQLERRM);

        END RGAR_RECEIVABLE_IMPORT_PRG;



PROCEDURE RGAR_CREATE_INVOICE_BATCHES(errbuf     OUT varchar2,
                                                                                  retcode    OUT NUMBER,
                                                                                  p_batch_id IN  NUMBER) IS

           l_return_status        varchar2(1);
           l_msg_count            number;
           l_msg_data             varchar2(2000);
           l_batch_id             number;
           l_batch_source_rec  ar_invoice_api_pub.batch_source_rec_type;
           l_trx_header_tbl     ar_invoice_api_pub.trx_header_tbl_type;
           l_trx_lines_tbl        ar_invoice_api_pub.trx_line_tbl_type;
           l_trx_dist_tbl         ar_invoice_api_pub.trx_dist_tbl_type;
           l_trx_salescredits_tbl ar_invoice_api_pub.trx_salescredits_tbl_type;
           l_trx_created   number;
           l_cnt                  number;
           l_trx_header_id    number;
           l_trx_line_id          number;
           i                      NUMBER := 0;
           j                      NUMBER := 0;
           k                      NUMBER := 0;
           vOrg_id                NUMBER := FND_PROFILE.VALUE('ORG_ID');
           vUser_id               NUMBER := FND_GLOBAL.USER_ID;
           vResp_id               NUMBER := FND_GLOBAL.RESP_ID;
           vResp_Appl_id     NUMBER := FND_GLOBAL.RESP_APPL_ID;
           vrun_id                NUMBER;
           vbatch_source_id     NUMBER;
           v_batch_name           VARCHAR2(1000);
           l_con_date             gl_daily_rates.conversion_date%type;
           l_conv_rate            gl_daily_rates.conversion_rate%Type;
           l_currency_code        gl_sets_of_books.currency_code%type;
           l_conversion_type      GL_DAILY_CONVERSION_TYPES.conversion_type%type;
           l_module_name   VARCHAR2 (40) := 'RGAR_CREATE_INVOICE_BATCHES';
           v_batch_source varchar2(50) := 'RIOT INVOICE UPLOAD'; --'ADI UPLOAD RGI_US_USD_OU';
           v_tot_trx_cnt number;
           l_Init_Msg_List                  VARCHAR2(1) := FND_API.G_TRUE;
           l_Commit                            VARCHAR2(1) := FND_API.G_FALSE;

            l_trx_cnt number := 0;
            l_party_name varchar2(150);


               cursor cbatch IS
               select customer_trx_id,trx_number
               from ra_customer_trx_all
               where batch_id = l_batch_id;

                CURSOR cValidTxn IS
                SELECT distinct trx_header_id
                  From ar_trx_header_gt
                 WHERE trx_header_id not in
                       (SELECT trx_header_id FROM ar_trx_errors_gt);

               cursor list_errors is
               SELECT trx_header_id, trx_line_id, trx_salescredit_id, trx_dist_id,
               trx_contingency_id, error_message, invalid_value
               FROM ar_trx_errors_gt;

               CURSOR c_batch(p_run_id NUMBER)
               IS
               SELECT REFERENCE batch_name
                 FROM RGAR_INV_UPLOAD_STG
                 WHERE status = 'N'
                   AND groupinv_id = p_run_id
                GROUP BY REFERENCE;

               CURSOR c_main(p_run_id NUMBER,
                             p_batch_name VARCHAR2) IS
                SELECT customer_name,
                       invoice_type,
                       invoice_date,
                       gl_date,
                       attribute1,
                       legal_entity,
                       attribute2,
                       attribute3,
                       attribute4,
                       attribute5,
                       attribute6,
                       currency_code
                  FROM RGAR_INV_UPLOAD_STG
                 WHERE status = 'N'
                   AND groupinv_id = p_run_id
                   AND REFERENCE  = p_batch_name
              GROUP BY customer_name,
                       invoice_type,
                       invoice_date,
                       gl_date,
                       attribute1,
                       legal_entity,
                       attribute2,
                       attribute3,
                       attribute4,
                       attribute5,
                       attribute6,
                       currency_code;

            /*    CURSOR c_cur(p_run_id IN NUMBER) IS
                SELECT a.rowid as row_id, a.*
                  FROM RGAR_INV_UPLOAD_STG a
                 WHERE 1=1 --a.status = 'N'
                   AND a.groupinv_id = p_run_id;*/

            CURSOR c_cur(p_run_id NUMBER,
                                       p_batch_name VARCHAR2,
                                       p_customer_name VARCHAR2,
                                       p_currency_code VARCHAR2,
                                       p_invoice_date date,
                                       p_call_reference VARCHAR2,
                                       p_exchange_type VARCHAR2,
                                       p_exchange_date DATE,
                                       p_conversion_type gl_daily_conversion_types.conversion_type%type) IS
                            SELECT a.rowid as row_id, a.*
                              FROM RGAR_INV_UPLOAD_STG a
                                WHERE 1=1 --a.status = 'N'
                                     AND REFERENCE  = p_batch_name
                                     AND a.groupinv_id = p_run_id
                                     AND a.customer_name = p_customer_name
                                     AND a.currency_code = p_currency_code
                                     and a.invoice_date = p_invoice_date
                                     and nvl(a.attribute5, 'X') = nvl(p_call_reference, 'X')
                                     and nvl(a.attribute3, 'X') = nvl(p_exchange_type,'X')
                                     and nvl(a.attribute6, SYSDATE) = nvl(p_exchange_date,SYSDATE);

    BEGIN

        rgcomn_debug_pkg.log_message(p_level => g_log_level,
                                                                    p_program_name   => l_module_name,
                                                                    p_message        => 'Start of rgar_create_invoice_batches Procedure');

          fnd_file.put_line(fnd_file.output,'--------------------------------------------------------------------');
          fnd_file.put_line(fnd_file.output,'***Customer Transaction Creation Process through WEB-ADI Upload***');
          fnd_file.put_line(fnd_file.output,'--------------------------------------------------------------------');

          fnd_file.put_line(fnd_file.log,'--------------------------------------------------------------------');
          fnd_file.put_line(fnd_file.log,'***Customer Transaction Creation Process through WEB-ADI Upload***');
          fnd_file.put_line(fnd_file.log,'--------------------------------------------------------------------');

                        BEGIN
                            SELECT MAX(groupinv_id)
                              INTO vrun_id
                              FROM RGAR_INV_UPLOAD_STG
                             WHERE status = 'N'
                               AND org_id = vOrg_id;

                                rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                                    P_MESSAGE        => 'Derived group id: '||vrun_id);

                                 fnd_file.put_line(fnd_file.output,'Processing group id: '||vrun_id);
                                 fnd_file.put_line(fnd_file.log,'Processing group id: '||vrun_id);

                        EXCEPTION
                            WHEN OTHERS THEN
                            vrun_id := NULL;

                             fnd_file.put_line(fnd_file.log,  'Error while deriving group id: '||sqlcode);
                             fnd_file.put_line(fnd_file.log, 'Error while deriving group id: '||sqlerrm);

                            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                                    P_MESSAGE        => 'Error while deriving group id: '||sqlcode);

                            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                                    P_MESSAGE        => 'Error while deriving group id: '||sqlerrm);
                        END;


                            Begin
                                SELECT sum(count(*))
                                        into l_trx_cnt
                                    FROM RGAR_INV_UPLOAD_STG
                                    WHERE status = 'N'
                                    AND groupinv_id = vrun_id
                                        GROUP BY customer_name,
                                                   invoice_date,
                                                   attribute3,
                                                   currency_code;
                             exception
                                when no_data_found then

                                    l_trx_cnt := 0;

                              end;



                        fnd_file.put_line(fnd_file.log,'Total record uploaded: '||l_trx_cnt);
                        fnd_file.put_line(fnd_file.output,'Total record uploaded: '||l_trx_cnt);


    -- Commented By Mahipal on 19-JAN-2016 based on LEN and JJ Email.
       /* BEGIN

            SELECT reference
              INTO v_batch_name
              FROM RGAR_INV_UPLOAD_STG
             WHERE groupinv_id = p_batch_id
             GROUP BY reference;

             rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                          P_PROGRAM_NAME   => l_module_name,
                                                                          P_MESSAGE        => 'Derived batch name: '||v_batch_name);

              fnd_file.put_line(fnd_file.output,'User Batch Name: '||v_batch_name);

        EXCEPTION
            WHEN OTHERS THEN
            v_batch_name := NULL;

            fnd_file.put_line(fnd_file.log, 'Error while deriving batch name: '||sqlcode);
            fnd_file.put_line(fnd_file.log, 'Error while deriving batch name: '||sqlerrm);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving batch name: '||sqlcode);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving batch name: '||sqlerrm);

        END;*/

            BEGIN
                SELECT batch_source_id
                  INTO vbatch_source_id
               FROM ra_batch_sources_all
              WHERE name = v_batch_source
                AND org_id = vOrg_id;
                         --AND ROWNUM = 1;

                         rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Derivied batch source id: '||vbatch_source_id);

            EXCEPTION
                WHEN OTHERS THEN
                vbatch_source_id := NULL;

                  BEGIN
                    SELECT batch_source_id
                      INTO vbatch_source_id
                    FROM ra_batch_sources_all
                    WHERE name = v_batch_source
                      AND ROWNUM = 1;

                      rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Derivied batch source id: '||vbatch_source_id);

                  EXCEPTION
                    WHEN OTHERS THEN
                    vbatch_source_id :=NULL;

                    fnd_file.put_line(fnd_file.log,  'Error while deriving batch source id: '||sqlcode);
                    fnd_file.put_line(fnd_file.log, 'Error while deriving batch source id: '||sqlerrm);

                    rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving batch source id: '||sqlcode);

                    rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                            P_PROGRAM_NAME   => l_module_name,
                                                                            P_MESSAGE        => 'Error while deriving batch source id: '||sqlerrm);

                  END;
            END;

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Org id: '||vOrg_id);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'Setting policy context');

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'Setting apps initialization');

               -- c. Set the applications context
               mo_global.init('AR');
               mo_global.set_policy_context('S',vOrg_id);
               fnd_global.apps_initialize(vUser_id, vResp_id, vResp_Appl_id);

               -- d. Populate batch source information.
               l_batch_source_rec.batch_source_id := vbatch_source_id;

            FOR rec_batch IN c_batch(p_batch_id) -- Added by Mahipal 19-JAN-2016 for multiple batches
            LOOP
            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                         P_PROGRAM_NAME   => l_module_name,
                                         P_MESSAGE        => 'Derived batch name: '||rec_batch.batch_name);

            fnd_file.put_line(fnd_file.output,'User Batch Name: '||rec_batch.batch_name);
            fnd_file.put_line(fnd_file.log,'User Batch Name: '||rec_batch.batch_name);
            l_conversion_type := NULL;
            l_currency_code   := NULL;
               FOR rec IN c_main(p_batch_id,rec_batch.batch_name)
               LOOP
                i := i+1;
                l_trx_header_id := RA_CUSTOMER_TRX_S.NEXTVAL;
                -- l_trx_line_id   := RA_CUSTOMER_TRX_LINES_S.NEXTVAL;


                            SELECT hp.party_name
                                    into l_party_name
                             FROM hz_parties hp,
                                         hz_cust_accounts_all hca,
                                         hz_cust_acct_sites_all hcasa,
                                         hz_cust_site_uses_all hcaua,
                                         hz_party_sites hps,
                                         hz_locations hl
                           WHERE hp.party_id = hca.party_id
                             AND hca.cust_account_id = hcasa.cust_account_id
                             AND hcasa.cust_acct_site_id = hcaua.cust_acct_site_id
                             AND hps.party_id = hp.party_id
                             AND hcaua.site_use_code = 'BILL_TO'
                             AND hcaua.primary_flag = 'Y'
                             AND hcaua.org_id =  vOrg_id
                             AND hca.cust_account_id = rec.customer_name
                             AND hps.location_id = hl.location_id
                             AND hps.IDENTIFYING_ADDRESS_FLAG ='Y';



                   -- e. Populate header information for first invoice
                   fnd_file.put_line(fnd_file.log,'--------------------------------------------------------');
                   FND_FILE.PUT_LINE(FND_FILE.log,'Party Name     :'||l_party_name);
                   FND_FILE.PUT_LINE(FND_FILE.log,'Party Id       :'||rec.customer_name);
                   FND_FILE.PUT_LINE(FND_FILE.log,'Invoice Type   :'||rec.invoice_type);
                   FND_FILE.PUT_LINE(FND_FILE.log,'Currency Code  :'||rec.currency_code);
                   FND_FILE.PUT_LINE(FND_FILE.log,'Exchange Type  :'||rec.attribute3);
                   FND_FILE.PUT_LINE(FND_FILE.log,'Invoice Date   :'||rec.invoice_date);
                   FND_FILE.PUT_LINE(FND_FILE.log,'GL Date        :'||rec.gl_date);
                   FND_FILE.PUT_LINE(FND_FILE.log,'----------------------------------------------------');

                   l_trx_header_tbl(i).trx_header_id                           := l_trx_header_id;
                   l_trx_header_tbl(i).bill_to_customer_id                     := to_number(rec.customer_name);
                   l_trx_header_tbl(i).cust_trx_type_id                        := rec.invoice_type;
                   l_trx_header_tbl(i).trx_date                                := rec.invoice_date;
                   l_trx_header_tbl(i).gl_date                                 := rec.gl_date;
                   l_trx_header_tbl(i).term_id                                 := to_number(rec.attribute1);
                   l_trx_header_tbl(i).org_id                                  := vOrg_id;
                   l_trx_header_tbl(i).legal_entity_id                         := rec.legal_entity;
                   l_trx_header_tbl(i).comments                                := rec.attribute2;
                   l_trx_header_tbl(i).trx_currency                            := rec.currency_code;
                   l_trx_header_tbl(i).interface_header_attribute1             := rec.attribute5;
                   l_trx_header_tbl(i).exchange_date                           := NVL(rec.attribute6,rec.gl_date);

                   IF UPPER(rec.attribute3) =  UPPER('User') THEN

                        l_trx_header_tbl(i).exchange_rate_type                 := rec.attribute3;
                        l_trx_header_tbl(i).exchange_rate                      := to_number(rec.attribute4);

                   ELSE

                            BEGIN
                                    select gl.currency_code
                                        into l_currency_code
                                        from hr_operating_units hou,
                                                  gl_sets_of_books gl
                                        where hou.set_of_books_id = gl.set_of_books_id
                                        and hou.DEFAULT_LEGAL_CONTEXT_ID = rec.legal_entity
                                        and rownum =1;

                                    rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                P_PROGRAM_NAME   => l_module_name,
                                                                                P_MESSAGE        => 'Derivied currency code: '||l_currency_code);

                                    --fnd_file.put_line(fnd_file.output,'Derived currency code: '||l_currency_code);

                            EXCEPTION
                                        WHEN OTHERS THEN
                                        l_currency_code := NULL;
                                        FND_FILE.PUT_LINE(FND_FILE.log,'Error while fetching curreny code for legal entity:'||SQLERRM);

                                        fnd_file.put_line(fnd_file.log,  'Error while deriving curreny code: '||sqlcode);
                                        fnd_file.put_line(fnd_file.log, 'Error while deriving curreny code: '||sqlerrm);

                                         rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                                    P_MESSAGE        => 'Error while deriving currency code: '||sqlcode);

                                        rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                            P_PROGRAM_NAME   => l_module_name,
                                                                                            P_MESSAGE        => 'Error while deriving currency code: '||sqlerrm);

                                     END;

                                      iF l_currency_code = rec.currency_code then

                                            l_trx_header_tbl(i).exchange_rate_type :=  NULL;
                                            l_trx_header_tbl(i).exchange_date      :=  NULL;

                                                     /* IF l_currency_code <> rec.currency_code
                                                      THEN
                                                        BEGIN
                                                          select conversion_date , conversion_rate
                                                            INTO l_con_date,l_conv_rate
                                                          from gl_daily_rates
                                                          where from_currency = rec.currency_code
                                                            and to_currency = l_currency_code
                                                            and conversion_type = 'Corporate'
                                                            and conversion_date = rec.invoice_date;

                                                        EXCEPTION
                                                          WHEN OTHERS THEN
                                                          l_con_date := NULL;
                                                          l_conv_rate := NULL;
                                                          FND_FILE.PUT_LINE(FND_FILE.log,'For invoiced date conversion not defined:');
                                                      END;

                                                       l_trx_header_tbl(i).exchange_rate_type                      := 'User';
                                                       l_trx_header_tbl(i).exchange_rate                           := l_conv_rate;
                                                       l_trx_header_tbl(i).exchange_date                           := rec.invoice_date;

                                                      END IF;*/

                                       else

                                                      BEGIN
                                                        SELECT conversion_type
                                                        INTO   l_conversion_type
                                                        FROM GL_DAILY_CONVERSION_TYPEs
                                                        WHERE upper(user_conversion_type) = upper(NVL(rec.attribute3,'Corporate'));

                                                        EXCEPTION
                                                        WHEN OTHERS THEN
                                                        l_conversion_type := 'Corporate';

                                                      END;

                                                            l_trx_header_tbl(i).exchange_rate_type := l_conversion_type;


                                         end if;

                     END IF;

                      -- f. Populate lines information for first invoice
                      k:= 0;
                      FOR r_rec IN c_cur(p_batch_id, rec_batch.batch_name, rec.customer_name, rec.currency_code, rec.invoice_date, rec.attribute5,rec.attribute3,rec.attribute6,l_conversion_type)
                      LOOP
                          k := k + 1;
                          j := j + 1;
                          l_trx_line_id   := RA_CUSTOMER_TRX_LINES_S.NEXTVAL;

                         -- FND_FILE.PUT_LINE(FND_FILE.log,'j:'||j);
                         -- FND_FILE.PUT_LINE(FND_FILE.log,'Amount:'||r_rec.amount);

                          l_trx_lines_tbl(j).trx_header_id                            := l_trx_header_id;
                          l_trx_lines_tbl(j).trx_line_id                              := l_trx_line_id;
                          l_trx_lines_tbl(j).line_number                              := k;
                          --l_trx_lines_tbl(1).description := 'Product Description 1';
                          l_trx_lines_tbl(j).MEMO_LINE_ID                             := r_rec.description;
                          l_trx_lines_tbl(j).quantity_invoiced                        := 1;
                          l_trx_lines_tbl(j).unit_selling_price                       := r_rec.amount;
                          l_trx_lines_tbl(j).line_type                                := 'LINE';



                      END LOOP;

               END LOOP;


               rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Data initialization done calling API to Create Invoice');

                            -- k. Call the invoice api to create multiple invoices in a batch.
                        AR_INVOICE_API_PUB.create_invoice(p_api_version          => 1.0,
                                                                           p_init_msg_list => l_Init_Msg_List,
                                                                           p_commit => l_Commit,
                                                                           p_batch_source_rec     => l_batch_source_rec,
                                                                           p_trx_header_tbl       => l_trx_header_tbl,
                                                                           p_trx_lines_tbl        => l_trx_lines_tbl,
                                                                           p_trx_dist_tbl         => l_trx_dist_tbl,
                                                                           p_trx_salescredits_tbl => l_trx_salescredits_tbl,
                                                                           x_return_status        => l_return_status,
                                                                           x_msg_count            => l_msg_count,
                                                                           x_msg_data             => l_msg_data);
                                                                        COMMIT;

                            dbms_output.put_line('l_return_status:'||l_return_status);
                            fnd_file.put_line(fnd_file.log,'l_return_status:'||l_return_status);

                            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                          P_PROGRAM_NAME   => l_module_name,
                                                                                          P_MESSAGE        => 'API return status: '||l_return_status);

                          rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                      P_PROGRAM_NAME   => l_module_name,
                                                                                      P_MESSAGE        => 'API return msg count: '||l_msg_count);

                          rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                      P_PROGRAM_NAME   => l_module_name,
                                                                                      P_MESSAGE        => 'API return msg data: '||l_msg_data);

                          fnd_file.put_line(fnd_file.log,  'API return status: '||l_return_status);
                          --fnd_file.put_line(fnd_file.log, 'API return msg count: '||l_msg_count);
                          --fnd_file.put_line(fnd_file.log, 'API return msg data: '||l_msg_data);


               -- l. check for errors
               IF l_return_status = fnd_api.g_ret_sts_error OR
                   l_return_status = fnd_api.g_ret_sts_unexp_error THEN

                UPDATE     RGAR_INV_UPLOAD_STG
                   SET    status = 'E'
                 WHERE     groupinv_id = p_batch_id
                   AND  REFERENCE  = rec_batch.batch_name;
                COMMIT;

                  dbms_output.put_line('Error: API Could not create transactions,Please review log for error details.');
                  fnd_file.put_line(fnd_file.output,'Error: API Could not create transactions,Please review log for error details.');
                  rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                              P_PROGRAM_NAME   => l_module_name,
                                                                              P_MESSAGE        => 'API could not create invoice: '||l_msg_data);
               ELSE
                    UPDATE RGAR_INV_UPLOAD_STG
                       SET status = 'S'
                     WHERE groupinv_id = p_batch_id
                       AND REFERENCE = rec_batch.batch_name;
                    COMMIT;

                     rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'API returned with status (S), no errros.');

                     fnd_file.put_line(fnd_file.log, 'API Returned with status (S), no errors.');

                  -- m. check batch/invoices created
                  select distinct batch_id
                  into l_batch_id
                  from ar_trx_header_gt;

                    UPDATE ra_batches_all
                       SET name   = rec_batch.batch_name -- added By mahipal  removed v_batch_name
                     WHERE batch_id  = l_batch_id;
                    COMMIT;

                    commit;

                    SELECT count(trx_header_id)
                               into v_tot_trx_cnt
                        From ar_trx_header_gt
                        WHERE trx_header_id not in (SELECT trx_header_id
                                                                              FROM ar_trx_errors_gt);


                      if nvl(v_tot_trx_cnt, 0) > 0 then

                              fnd_file.put_line(fnd_file.output, 'Invoice(s) created successfully. Please review by Batch or Transactions.');
                              fnd_file.put_line(fnd_file.output,'----------------------------------------------------------');
                              fnd_file.put_line(fnd_file.output,'Total success records: '||v_tot_trx_cnt);
                              fnd_file.put_line(fnd_file.output,'----------------------------------------------------------');
                              fnd_file.put_line(fnd_file.log,'Total success records: '||v_tot_trx_cnt);
                              fnd_file.put_line(fnd_file.log,'-----------------------------------------------------');

                              l_batch_id := ar_invoice_api_pub.g_api_outputs.batch_id;
                              fnd_file.put_line(fnd_file.log, 'Batch Id: '||l_batch_id);

                            /*
                            For cValidTxnRec IN cvalidTxn
                            loop

                                 l_batch_id := ar_invoice_api_pub.g_api_outputs.batch_id;

                                  IF nvl(l_batch_id, -1) != -1 then

                                     dbms_output.put_line('Batch ID: ' ||l_batch_id);
                                     rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                                  P_PROGRAM_NAME   => l_module_name,
                                                                                                  P_MESSAGE        =>'Batch ID: ' ||l_batch_id);

                                      --fnd_file.put_line(fnd_file.output, 'Batch Id: '||l_batch_id);
                                      */

                                         for cBatchRec in cBatch
                                         loop

                                          dbms_output.put_line('Cust Trx Id: '||cBatchRec.customer_trx_id);
                                          rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                                      P_PROGRAM_NAME   => l_module_name,
                                                                                                      P_MESSAGE        =>'Cust Trx Id: ' ||cBatchRec.customer_trx_id);

                                           fnd_file.put_line(fnd_file.output, 'TRX Number: '||cBatchRec.trx_number);
                                           fnd_file.put_line(fnd_file.log, 'TRX Id: '||cBatchRec.customer_trx_id);

                                         end loop;

                                /*
                                  ELSE

                                     rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                                                 P_PROGRAM_NAME   => l_module_name,
                                                                                                 P_MESSAGE        =>'Could not generate Cust TRX Id');

                                     fnd_file.put_line(fnd_file.output,'Error: API Could not create transactions,Please review request log for error details.');
                                     fnd_file.put_line(fnd_file.output,'-------------------------------------------------------------');
                                  END IF;

                            End loop;
                            */

                      else

                          fnd_file.put_line(fnd_file.output,'No Transaction created in this Run. Please review request log for error details.');

                      end if;

               END IF;

        -- n. Within the batch, check if some invoices raised errors
       SELECT count(distinct trx_header_id) --count(*)
        INTO l_cnt
        FROM ar_trx_errors_gt;
            fnd_file.put_line(fnd_file.log,'------------------------------------------------------');
            fnd_file.put_line(fnd_file.log,'Total error records: '|| l_cnt);
            fnd_file.put_line(fnd_file.output,'----------------------------------------------------------');
            fnd_file.put_line(fnd_file.output,'Total error records: '|| l_cnt);

        IF l_cnt > 0 THEN
          dbms_output.put_line('Errors encountered, Please review and fix errors and re-run WEB-ADI Upload');
          fnd_file.put_line(fnd_file.log,'Errors encountered, Please review and fix errors and re-run WEB-ADI Upload');

              FOR i in list_errors LOOP
                 dbms_output.put_line('----------------------------------------------------');
                 dbms_output.put_line('Header ID = ' || to_char(i.trx_header_id));
                 dbms_output.put_line('Line ID = ' || to_char(i.trx_line_id));
                 dbms_output.put_line('Sales Credit ID = ' || to_char(i.trx_salescredit_id));
                 dbms_output.put_line('Dist Id = ' || to_char(i.trx_dist_id));
                 dbms_output.put_line('Contingency ID = ' || to_char(i.trx_contingency_id));
                 dbms_output.put_line('Message = ' || substr(i.error_message,1,80));
                 dbms_output.put_line('Invalid Value = ' || substr(i.invalid_value,1,80));
                 dbms_output.put_line('----------------------------------------------------');

                 FND_FILE.PUT_LINE(FND_FILE.LOG,'----------------------------------------------------');
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Header ID = ' || to_char(i.trx_header_id));
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Line ID = ' || to_char(i.trx_line_id));
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Sales Credit ID = ' || to_char(i.trx_salescredit_id));
                 --FND_FILE.PUT_LINE(FND_FILE.LOG,'Dist Id = ' || to_char(i.trx_dist_id));
                 --FND_FILE.PUT_LINE(FND_FILE.LOG,'Contingency ID = ' || to_char(i.trx_contingency_id));
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error Message = ' || substr(i.error_message,1,80));
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Invalid Value = ' || substr(i.invalid_value,1,80));
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'----------------------------------------------------');

              END LOOP;

        END IF;

       rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE =>'rgar_create_invoice_batches procedure completed.');
        fnd_file.put_line(fnd_file.output,'----------------------------------------------------------');

    l_trx_header_tbl.DELETE;
    l_trx_lines_tbl.DELETE;

    END LOOP; -- Added by Mahipal 19-JAN-2016 for multiple batches

    EXCEPTION
        WHEN OTHERS THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in rgar_create_invoice_batches procedure:'||SQLERRM);
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in rgar_create_invoice_batches procedure:'||SQLCODE);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'Error in rgar_create_invoice_batches procedure: '||sqlcode);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'Error in rgar_create_invoice_batches procedure: '||sqlerrm);

    END RGAR_CREATE_INVOICE_BATCHES;


PROCEDURE MAIN( pBatch_ID   IN NUMBER) IS

        vInvoice_id       NUMBER;
        vInvoice_line_id  NUMBER;
        vUser_id          NUMBER := FND_GLOBAL.USER_ID;
        vl_error_msg      LONG   := NULL;
        vOrg_id           NUMBER := FND_PROFILE.VALUE('ORG_ID');
        vl_batch_id       NUMBER;
        vl_run_id         NUMBER;
        vl_description    VARCHAR2(1000);
        vsets_of_books_id NUMBER;
        vrec              NUMBER:= 0;
        vtotalamount      NUMBER;
        vpercent          NUMBER;
        vrun_id           NUMBER;
        vdist_line_id     NUMBER;
        l_module_name   VARCHAR2 (40) := 'MAIN';

        CURSOR c_main(p_run_id NUMBER) IS
        SELECT customer_name
          FROM RGAR_INV_UPLOAD_STG
            WHERE status = 'N'
            AND groupinv_id = p_run_id
            GROUP BY customer_name;

        CURSOR c_cur(p_run_id NUMBER, p_customer_name VARCHAR2) IS
        SELECT a.rowid as row_id, a.*
          FROM RGAR_INV_UPLOAD_STG a
            WHERE a.status = 'N'
            AND a.groupinv_id = p_run_id
            AND a.customer_name = p_customer_name;

      BEGIN

        rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Start of WEB-ADI AR Main Procedure');

            BEGIN
                SELECT MAX(groupinv_id)
                  INTO vrun_id
                  FROM RGAR_INV_UPLOAD_STG
                 WHERE status = 'N'
                    AND org_id = vOrg_id;

            EXCEPTION
            when no_data_found then
            vrun_id := -1;
            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Not able to derive group Inv Id: '||vrun_id);

            WHEN OTHERS THEN
            vrun_id := -1;
            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving group Inv Id: '||sqlcode);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => 'Error while deriving group Inv Id: '||sqlerrm);
            END;

            /*BEGIN
                select set_of_books_id
                  into vsets_of_books_id
                  from hr_operating_units
                 where 1=1 --default_legal_context_id = 23273;
                   and organization_id = vOrg_id;
            EXCEPTION
                WHEN OTHERS THEN
                vsets_of_books_id := NULL;
            END;

            FOR rec_main IN c_main(vrun_id)
            LOOP
            vrec         := 0;
            vtotalamount := 0;

                FOR r_rec IN c_cur(vrun_id,rec_main.customer_name)
                LOOP
                vrec := vrec + 1;
                DBMS_OUTPUT.PUT_LINE('Entered into For loop');
                IF  vrec = 1
                THEN
                BEGIN
                    SELECT SUM(amount)
                      INTO vtotalamount
                      FROM RGAR_INV_UPLOAD_STG
                     WHERE status = 'N'
                       AND groupinv_id = vrun_id
                       AND customer_name = rec_main.customer_name;
                EXCEPTION
                    WHEN OTHERS THEN
                    vtotalamount := 0;
                END;
                END IF;


                    vInvoice_id := RA_CUSTOMER_TRX_LINES_S.NEXTVAL;

                    BEGIN
                        SELECT name
                          INTO vl_description
                          FROM AR_MEMO_LINES_ALL_vl
                         WHERE memo_line_id = r_rec.description
                   AND org_id  = vOrg_id;
                    EXCEPTION
                        WHEN OTHERS THEN
                        vl_description := NULL;
                  BEGIN
                    SELECT name
                            INTO vl_description
                            FROM AR_MEMO_LINES_ALL_vl
                            WHERE memo_line_id = r_rec.description
                      AND Rownum =1 ;
                  EXCEPTION
                          WHEN OTHERS THEN
                          vl_description := NULL;
                END;
                    END;

                BEGIN
              DBMS_OUTPUT.PUT_LINE('Entered into For begin');
                    INSERT INTO RA_INTERFACE_LINES_ALL
                            (   interface_line_id,
                                interface_line_context,
                                interface_line_attribute1,
                                interface_line_attribute2,
                                batch_source_name,
                                trx_number,
                                conversion_type,
                                --conversion_rate,
                                set_of_books_id,
                                line_number,
                                line_type,
                                description,
                                memo_line_id,
                                currency_code,
                                cust_trx_type_id,
                                orig_system_bill_customer_id,
                                orig_system_bill_address_id,
                                --ORIG_SYSTEM_SHIP_CUSTOMER_ID,
                                --ORIG_SYSTEM_SHIP_ADDRESS_ID,
                                --ORIG_SYSTEM_SOLD_CUSTOMER_ID,
                                trx_date,
                                gl_date,
                                term_id,
                                comments,
                                OVERRIDE_AUTO_ACCOUNTING_FLAG,
                                -- inventory_item_id,
                                quantity,
                                unit_selling_price,
                                amount,
                    HEADER_ATTRIBUTE_CATEGORY,
                    HEADER_ATTRIBUTE1,
                    HEADER_ATTRIBUTE2,
                                -- uom_code,
                                created_by,
                                creation_date,
                                last_update_date,
                                --last_update_login,
                    --INVOICING_RULE_NAME,
                                org_id)
                            VALUES
                               (vInvoice_id,                    --interface_line_id
                                'WEBADI',                       --interface_line_context
                                vInvoice_id,                    --interface_line_attribute1
                                 NULL,                            --interface_line_attribute2
                                'ADI UPLOAD RGI_US_USD_OU',     --batch_source_name
                                NULL,                           --trx_number
                                'Corporate',                    --conversion_type
                                vsets_of_books_id,              --set_of_books_id
                                vrec,                           --line_number
                                'LINE',                         --line_type
                                vl_description,                 --description
                                r_rec.description,              --memo_line_id
                                r_rec.currency_code,            --currency_code
                                r_rec.invoice_type,             --cust_trx_type_id
                                r_rec.customer_name,            --orig_system_bill_customer_id
                                r_rec.CUST_BILL_TO_ID,          --orig_system_bill_address_id
                                SYSDATE,                        --trx_date
                                r_rec.gl_date,                  --gl_date
                                to_number(r_rec.attribute1),    --term_id
                    NULL,                           --comments
                                'Y',                  --OVERRIDE_AUTO_ACCOUNTING_FLAG
                                1,                              --quantity
                                r_rec.amount,                   --unit_selling_price
                                r_rec.amount,                   --amount
                    'Notes',                        --HEADER_ATTRIBUTE_CATEGORY
                    r_rec.reference,                --HEADER_ATTRIBUTE1
                    NVL(r_rec.attribute2,'-'),      --HEADER_ATTRIBUTE2
                                vUser_id,                       --created_by
                                SYSDATE,                        --creation_date
                                SYSDATE,                        --last_update_date
                  --  'RG_ACCOUNTING_RULE',
                                vOrg_id                         --org_id
                                );
                COMMIT;
                EXCEPTION
                    WHEN OTHERS THEN
                    dbms_output.put_line('Error while inserting into interface header table:'||SQLERRM);
                    vl_error_msg := vl_error_msg ||'-'||'Error while inserting into Ap Interface table';
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
                END;
                DBMS_OUTPUT.PUT_LINE('out into begin');
                    IF (r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null AND
                       r_rec.segment1 is not null)
                    THEN
              vdist_line_id := RA_CUST_TRX_LINE_GL_DIST_S.NEXTVAL;
                    vpercent := ((NVL(r_rec.amount,0)/NVL(vtotalamount,0)) * 100);
                        BEGIN
                        INSERT INTO RA_INTERFACE_DISTRIBUTIONS_ALL
                                  (INTERFACE_DISTRIBUTION_ID,
                      INTERFACE_LINE_ID,
                      account_class,
                                    amount,
                                    percent,
                                    segment1,
                                    segment2,
                                    segment3,
                                    segment4,
                                    segment5,
                                    segment6,
                                    segment7,
                                    segment8,
                                    interface_line_context,
                                    interface_line_attribute1,
                                    interface_line_attribute2,
                                    interface_line_attribute3)
                                VALUES
                                 (vdist_line_id,
                      vInvoice_id,
                      'REV',
                                    r_rec.amount,
                                    100,
                                    r_rec.SEGMENT1,
                                    r_rec.SEGMENT2,
                                    r_rec.SEGMENT3,
                                    r_rec.SEGMENT4,
                                    r_rec.SEGMENT5,
                                    r_rec.SEGMENT6,
                                    r_rec.SEGMENT7,
                                    r_rec.SEGMENT8,
                                    'WEBADI',
                                    vInvoice_id,
                                    NULL,
                                    NULL
                                 );

                        COMMIT;
                        EXCEPTION
                            WHEN OTHERS THEN
                            FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into AR Distribution:'||SQLERRM);
                        END;
                    END IF;
                IF vl_error_msg IS NULL
                THEN
                    UPDATE RGAR_INV_UPLOAD_STG
                       SET status = 'S'
                     WHERE groupinv_id = r_rec.groupinv_id
                       AND rowid = r_rec.row_id;
                    COMMIT;
                DBMS_OUTPUT.PUT_LINE('Update statement');
                END IF;
                END LOOP; -- Line loop (Customer Loop)
            END LOOP; -- Main Loop*/

          IF nvl(vrun_id, -1) != -1 THEN

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                        P_PROGRAM_NAME   => l_module_name,
                                                                        P_MESSAGE        => 'Calling rgar_receivable_import_prg');

               RGAR_RECEIVABLE_IMPORT_PRG(vrun_id);

          END IF;

      EXCEPTION
            WHEN OTHERS THEN
            dbms_output.put_line('Unexpected Error:'||SQLERRM);
            FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occurred while creating AR Invoices: '||SQLERRM);

            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => ' Error in Main Procedure '|| '-'|| SQLCODE);
            rgcomn_debug_pkg.log_message(P_LEVEL => g_log_level,
                                                                    P_PROGRAM_NAME   => l_module_name,
                                                                    P_MESSAGE        => ' Error in Main Procedure '|| '-'|| SQLERRM);

      END MAIN;

   END RGAR_INVOICE_UPLOAD_PKG;
/
EXIT;
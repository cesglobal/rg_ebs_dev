CREATE OR REPLACE PACKAGE BODY APPS.rgap_emp_inv_upload_pkg
AS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Package is Used to load data from WEBADI to stage table and then Standrad AP Interface Table.
REM
REM    Module               : RGAP Payables
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM    29-Dec-2015     Srinivasa Ramuni          1.1             Add Tax Code in interface table for tax and item line type as per Maggie's email on 29-DEC-2015
REM --------------------------------------------------------------------------------------------------------------*/
FUNCTION RGAP_EMP_INV_UPLOAD
RETURN VARCHAR2
AS
lv_message VARCHAR2(100);
BEGIN
lv_message := NULL;
RETURN lv_message;
END RGAP_EMP_INV_UPLOAD;

PROCEDURE RGAP_EMP_INV_VALIDATION (P_GROUP_ID IN  NUMBER,
                                   X_status   OUT VARCHAR2)
IS
CURSOR C_cur
IS
SELECT rius.rowid as row_id, rius.*
  FROM RGAP_INV_UPLFORM_STG rius
 WHERE group_id = P_GROUP_ID
   AND status IN ('N','E','V');

vl_Invoice_type_exists NUMBER;
vl_card_num_exists     NUMBER;
vl_employee_num_exists NUMBER;
vl_company             VARCHAR2(60);
vl_account             VARCHAR2(60);
vl_discipline          VARCHAR2(60);
vl_territory           VARCHAR2(60);
vl_product             VARCHAR2(60);
vl_Initiative          VARCHAR2(60);
vl_segment1            VARCHAR2(60);
vl_segment2            VARCHAR2(60);
vl_segment3            VARCHAR2(60);
vl_segment4            VARCHAR2(60);
vl_segment5            VARCHAR2(60);
vl_segment6            VARCHAR2(60);
vl_TRADING_PARTNER     VARCHAR2(100);
vl_vendor_id           NUMBER;
vl_SUPPLIER_NUMBER     VARCHAR2(100);
vl_SUPPLIER_SITE_NAME  VARCHAR2(100);
vl_vendor_site_id      NUMBER;
Custom_exception       EXCEPTION;
vl_message             VARCHAR2(1000);
vl_invoice_count       NUMBER;
BEGIN
   --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'RGAP_EMP_INV_UPLOAD_PKG.RGAP_EMP_INV_VALIDATION');
   X_status := NULL;
   FOR r_cur IN c_cur
   LOOP
   vl_message := NULL;
  BEGIN
	IF  r_cur.invoice_type IN  ('COMPANY_CORP_CARD','EMPLOYEE_CORP_CARD')
	THEN
	    BEGIN
     	  SELECT aps.vendor_name,
			       aps.vendor_id,
			       aps.segment1,
			       apsa.vendor_site_code,
			       apsa.vendor_site_id
			  INTO vl_TRADING_PARTNER,
			       vl_vendor_id,
			       vl_SUPPLIER_NUMBER,
			       vl_SUPPLIER_SITE_NAME,
			       vl_vendor_site_id
			  FROM ap_suppliers aps,
			       ap_supplier_sites_all apsa,
                   fnd_lookup_values_vl flv
             WHERE flv.lookup_TYPE LIKE 'RGKR_CORPORATE_CARDS'
               AND flv.lookup_code = r_cur.card_num
               AND aps.vendor_id = apsa.vendor_id
               AND aps.vendor_id = to_number(flv.attribute1)
			   AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active)
                                  AND NVL(TRUNC(flv.end_date_active),TRUNC(SYSDATE))
               AND flv.enabled_flag = 'Y'
               AND ROWNUM =1;
        EXCEPTION
     	WHEN OTHERS THEN
     	    vl_TRADING_PARTNER    := NULL;
			vl_vendor_id          := NULL;
			vl_SUPPLIER_NUMBER    := NULL;
			vl_SUPPLIER_SITE_NAME := NULL;
			vl_vendor_site_id     := NULL;
     END;

	    UPDATE RGAP_INV_UPLFORM_STG
		   SET TRADING_PARTNER     = vl_TRADING_PARTNER,
		       VENDOR_ID           = vl_vendor_id,
			   SUPPLIER_NUMBER     = vl_SUPPLIER_NUMBER,
			   SUPPLIER_SITE_NAME  = vl_SUPPLIER_SITE_NAME,
			   VENDOR_SITE_ID      = vl_vendor_site_id
		 WHERE card_num =  r_cur.card_num
		   AND rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
	END IF;
	IF (r_cur.invoice_type = 'EMPLOYEE_NON_CORP_CARD' AND r_cur.EMPLOYEE_NUMBER IS NOT NULL)
	THEN

	    BEGIN
		   SELECT aps.vendor_name,
			       aps.vendor_id,
			       aps.segment1,
			       apsa.vendor_site_code,
			       apsa.vendor_site_id
			  INTO vl_TRADING_PARTNER,
			       vl_vendor_id,
			       vl_SUPPLIER_NUMBER,
			       vl_SUPPLIER_SITE_NAME,
			       vl_vendor_site_id
			  FROM ap_suppliers aps,
			       ap_supplier_sites_all apsa,
			       per_all_people_f papf
			 WHERE papf.party_id = aps.party_id
			   AND aps.vendor_id = apsa.vendor_id
			   AND papf.employee_number = to_char(r_cur.EMPLOYEE_NUMBER)
			   AND ROWNUM =1;
		EXCEPTION
			WHEN OTHERS THEN
			vl_TRADING_PARTNER    := NULL;
		    vl_vendor_id          := NULL;
		    vl_SUPPLIER_NUMBER    := NULL;
		    vl_SUPPLIER_SITE_NAME := NULL;
		    vl_vendor_site_id     := NULL;
			dbms_output.put_line('Error in employee number:'||SQLERRM);
		END;

		UPDATE RGAP_INV_UPLFORM_STG
		   SET TRADING_PARTNER     = vl_TRADING_PARTNER,
		       VENDOR_ID           = vl_vendor_id,
			   SUPPLIER_NUMBER     = vl_SUPPLIER_NUMBER,
			   SUPPLIER_SITE_NAME  = vl_SUPPLIER_SITE_NAME,
			   VENDOR_SITE_ID      = vl_vendor_site_id
		 WHERE 1=1 --card_num =  r_cur.card_num
		   AND rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;

    END IF;
  --------------------------------------------------

  IF r_cur.invoice_type IN ('COMPANY_CORP_CARD','EMPLOYEE_CORP_CARD')
    AND (r_cur.DOCUMENT_NUM  IS NULL
    OR r_cur.MERCHANT_NAME IS NULL)
  THEN
    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Please enter Support Document Number/Merchant Name'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type0');
    vl_message := 'E';
		RAISE Custom_exception;
  END IF;

  IF r_cur.account IN ('624320 ','624330')
     AND (r_cur.EMPLOYER IS NULL
     OR r_cur.PARTICIPANT IS NULL
     OR r_cur.PURPOSE    IS NULL
     OR r_cur.VENUE      IS NULL)
  THEN
  UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Please enter Actual User/Participant/Purpose/Venue'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type00');
		RAISE Custom_exception;
  END IF;

  IF r_cur.invoice_number IS NOT NULL AND r_cur.invoice_type ='EMPLOYEE_NON_CORP_CARD'
  THEN

	SELECT count(1)
	  INTO vl_invoice_count
	  FROM RGAP_INV_UPLFORM_STG
	 WHERE group_id = r_cur.group_id
	   AND vendor_id IN (SELECT aps.vendor_id
			              FROM ap_suppliers aps,
			                   per_all_people_f papf
			             WHERE papf.party_id = aps.party_id
			               AND papf.employee_number = to_char(r_cur.EMPLOYEE_NUMBER)
			               AND ROWNUM =1)
	   AND invoice_number = r_cur.invoice_number;

		IF vl_invoice_count > 1
		THEN
			UPDATE RGAP_INV_UPLFORM_STG
			   SET error_message = 'Invoice Number should be unique for the same supplier'
				   ,status       = 'E'
			 WHERE rowid = r_cur.row_id
			   AND group_id = r_cur.group_id;
			COMMIT;
		DBMS_OUTPUT.PUT_LINE('Invoice Type000');
		vl_message := 'E';
			RAISE Custom_exception;
		END IF;
	ELSIF r_cur.invoice_number IS NOT NULL AND r_cur.invoice_type IN  ('COMPANY_CORP_CARD','EMPLOYEE_CORP_CARD')
	THEN
	  SELECT count(1)
	  INTO vl_invoice_count
	  FROM RGAP_INV_UPLFORM_STG
	 WHERE group_id = r_cur.group_id
	   AND vendor_id IN (SELECT aps.vendor_id
			               FROM ap_suppliers aps,
                                fnd_lookup_values_vl flv
                          WHERE flv.lookup_TYPE LIKE 'RGKR_CORPORATE_CARDS'
                            AND flv.lookup_code = r_cur.card_num
                            AND aps.vendor_id = to_number(flv.attribute1)
			                AND TRUNC(SYSDATE) BETWEEN TRUNC(flv.start_date_active)
                                  AND NVL(TRUNC(flv.end_date_active),TRUNC(SYSDATE))
                            AND flv.enabled_flag = 'Y'
                            AND ROWNUM =1)
	   AND invoice_number = r_cur.invoice_number;

		IF vl_invoice_count > 1
		THEN
			UPDATE RGAP_INV_UPLFORM_STG
			   SET error_message = 'Invoice Number should be unique for the same supplier'
				   ,status       = 'E'
			 WHERE rowid = r_cur.row_id
			   AND group_id = r_cur.group_id;
			COMMIT;
		DBMS_OUTPUT.PUT_LINE('Invoice Type000');
		vl_message := 'E';
			RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
   -- Invoice type Validation and it should exists in RGKR_INVOICE_TYPE lookup
	IF r_cur.invoice_type IS NULL
	THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Invoice type not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type1');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.invoice_type IS NOT NULL
	THEN
		SELECT count(1)
          INTO vl_Invoice_type_exists
		  FROM fnd_lookup_values_vl
         WHERE lookup_type like 'RGKR_INVOICE_TYPE'
           AND TRUNC(SYSDATE) BETWEEN TRUNC(start_date_active)
                                  AND NVL(TRUNC(end_date_active),TRUNC(SYSDATE))
           AND enabled_flag = 'Y'
		   AND (UPPER(lookup_code) = UPPER(r_cur.invoice_type) OR upper(meaning) = UPPER(r_cur.invoice_type));

		IF vl_Invoice_type_exists = 0
		THEN
			UPDATE RGAP_INV_UPLFORM_STG
		       SET error_message = 'Invoice Type is not correct'
		          ,status       = 'E'
		     WHERE rowid = r_cur.row_id
		       AND group_id = r_cur.group_id
			     AND status IN ('N','E','V');
		    COMMIT;
        DBMS_OUTPUT.PUT_LINE('Invoice Type2');
        vl_message := 'E';
			RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- Validation for Card num  and it should exists in RGKR_CORPORATE_CARDS lookup
	IF  (r_cur.Card_num IS NULL AND r_cur.invoice_type IN ('COMPANY_CORP_CARD','EMPLOYEE_CORP_CARD'))
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Card Number not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type3');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF  (r_cur.Card_num IS NOT NULL AND r_cur.invoice_type IN ('COMPANY_CORP_CARD','EMPLOYEE_CORP_CARD'))
    THEN
		SELECT count(1)
          INTO vl_card_num_exists
          FROM fnd_lookup_values_vl
         WHERE lookup_TYPE LIKE 'RGKR_CORPORATE_CARDS'
           AND TRUNC(SYSDATE) BETWEEN TRUNC(start_date_active)
                                  AND NVL(TRUNC(end_date_active),TRUNC(SYSDATE))
           AND enabled_flag = 'Y'
		   AND lookup_code = r_cur.Card_num;

		IF vl_card_num_exists = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		       SET error_message = 'Card No is not Correct'
		          ,status        = 'E'
		     WHERE rowid = r_cur.row_id
		       AND group_id = r_cur.group_id
			   AND status IN ('N','E','V');
		    COMMIT;
        DBMS_OUTPUT.PUT_LINE('Invoice Type4');
        vl_message := 'E';
			RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- Validation for employee number exists or not n system.
	IF  (r_cur.Employee_number IS NULL AND r_cur.invoice_type ='EMPLOYEE_NON_CORP_CARD')
	THEN
	   UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Please eneter Employee Number'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type5');
    vl_message := 'E';
		RAISE Custom_exception;
  END IF;
	IF  (r_cur.Employee_number IS NOT NULL AND r_cur.invoice_type = 'EMPLOYEE_NON_CORP_CARD')
    THEN
		SELECT count(1)
		  INTO vl_employee_num_exists
		  FROM per_all_people_f
		 WHERE employee_number = r_cur.Employee_number
		   AND trunc(sysdate) BETWEEN Trunc(effective_start_date)
		                          AND Trunc(effective_end_date)
		   AND party_id in ( select party_id
                              from ap_suppliers aps,
                                   ap_supplier_sites_all apps
                             where aps.vendor_id = apps.vendor_id
                               and apps.org_id IN ( select organization_id
                                                      from hr_operating_units
                                                      where short_code ='RGK_KR_OU'));

		IF  vl_employee_num_exists = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		       SET error_message = 'This employee is not exists in Supplier Master'
		          ,status        = 'E'
		     WHERE rowid = r_cur.row_id
		       AND group_id = r_cur.group_id
			   AND status IN ('N','E','V');
		    COMMIT;
        DBMS_OUTPUT.PUT_LINE('Invoice Type6');
        vl_message := 'E';
			RAISE Custom_exception;
		END IF;

	END IF;
	--------------------------------------------------
	-- Validation for expense amount
	/*IF  r_cur.Expense_amount <= 0
    THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Expense amount should be greater than Zero'
		      ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type7');
    vl_message := 'E';
		RAISE Custom_exception;
  END IF;*/

	IF r_cur.Expense_amount IS NULL
	  THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Expense Amount is required'
		      ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    vl_message := 'E';
    DBMS_OUTPUT.PUT_LINE('Invoice Type7');
		RAISE Custom_exception;
	END IF;
	--validation for expense amount precision
	/*IF substr(r_cur.Expense_amount,instr(r_cur.Expense_amount,'.')+1,length(r_cur.Expense_amount)) > 0
	THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Expense amount Precision should be equal to Zero'
		      ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E');
		COMMIT;
		RAISE Custom_exception;
	END IF;*/
	--------------------------------------------------
	-- Validation for tax amount
	/*IF NVL(r_cur.Tax_amount,1) < 0
    THEN
    	UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Tax amount should be greater than Zero'
		      ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type8');
    vl_message := 'E';
		RAISE Custom_exception;
	END IF;*/

	/*IF NVL(r_cur.Tax_amount,0) > 0
    THEN
		IF substr(r_cur.Tax_amount,instr(r_cur.Tax_amount,'.')+1,length(r_cur.Tax_amount)) > 0
		THEN
			UPDATE RGAP_INV_UPLFORM_STG
			   SET error_message = 'Tax amount Precision should be equal to Zero'
				  ,status        = 'E'
			 WHERE rowid = r_cur.row_id
			   AND group_id = r_cur.group_id
			   AND status IN ('N','E');
			COMMIT;
			X_status := 'E';
		END IF;
	END IF;*/
	---------------------------------------------------
	-- COA validation for company
	IF r_cur.company IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Company Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type9');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.company IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_company
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('company');

	SELECT count(1)
	  INTO vl_segment1
	  FROM gl_code_combinations
	 WHERE segment1 = r_cur.company;*/
	 select count(1)
	  INTO vl_segment1
       from fnd_flex_values_vl ffvl,
            fnd_flex_value_sets ffvs
      where ffvl.flex_value_set_id = ffvs.flex_value_set_id
        and ffvs.flex_value_set_name ='RG_COMPANY'
		and ffvl.flex_value_meaning = r_cur.company
        and UPPER(ffvl.description) = UPPER('Korea');

		IF vl_segment1 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Company Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type10');
    vl_message := 'E';
    	RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- COA validation for account
	IF r_cur.account IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Account Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type11');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.account IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_account
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('account');

	SELECT count(1)
	  INTO vl_segment2
	  FROM gl_code_combinations
	 WHERE segment2 = r_cur.account;*/
     select count(1)
	  INTO vl_segment2
  from fnd_flex_values_vl ffvl,
       fnd_flex_value_sets ffvs
 where ffvl.flex_value_set_id = ffvs.flex_value_set_id
   and ffvs.flex_value_set_name ='RG_ACCOUNT'
   and ffvl.enabled_flag = 'Y'
   and ffvl.flex_value_meaning = r_cur.account
   and ffvl.COMPILED_VALUE_ATTRIBUTES like ('Y
Y
%');
		IF vl_segment2 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Account Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type12');
    vl_message := 'E';
		RAISE Custom_exception;
		ELSE
	IF  r_cur.account IN ('624320','624330')
     THEN
       IF (r_cur.employer IS NULL               OR
			    r_cur.participant IS NULL            OR
			    r_cur.purpose IS NULL                OR
			    r_cur.venue IS NULL )
		 THEN
		 UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Please enter ActualUser/participant/purpose/venue'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type14');
    vl_message := 'E';
		RAISE Custom_exception;
		END IF;
		END IF;
		END IF;
	END IF;
	--------------------------------------------------
	-- COA validation for discipline
	IF r_cur.discipline IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Discipline Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type15');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.discipline IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_discipline
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('discipline');

	SELECT count(1)
	  INTO vl_segment3
	  FROM gl_code_combinations
	 WHERE segment3 = r_cur.discipline;*/
    select count(1)
	  INTO vl_segment3
      from fnd_flex_values_vl ffvl,
           fnd_flex_value_sets ffvs
     where ffvl.flex_value_set_id = ffvs.flex_value_set_id
       and ffvs.flex_value_set_name ='RG_DISCIPLINE'
       and ffvl.enabled_flag = 'Y'
	   and ffvl.flex_value_meaning = r_cur.discipline
       and ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y');
		IF vl_segment3 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Discipline Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type16');
    vl_message := 'E';
    	RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- COA validation for territory
	IF r_cur.territory IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Territory Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type17');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.territory IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_territory
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('territory');

	SELECT count(1)
	  INTO vl_segment4
	  FROM gl_code_combinations
	 WHERE segment4 = r_cur.territory;*/
    select count(1)
	  INTO vl_segment4
      from fnd_flex_values_vl ffvl,
           fnd_flex_value_sets ffvs
     where ffvl.flex_value_set_id = ffvs.flex_value_set_id
       and ffvs.flex_value_set_name ='RG_TERRITORY'
       and ffvl.enabled_flag = 'Y'
	   and ffvl.flex_value_meaning = r_cur.territory
       and ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y');
		IF vl_segment4 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Territory Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type18');
    vl_message := 'E';
		RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- COA validation for product
	IF r_cur.product IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Product Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type19');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.product IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_product
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('product');

	SELECT count(1)
	  INTO vl_segment5
	  FROM gl_code_combinations
	 WHERE segment5 = r_cur.product;*/
	 select count(1)
	  INTO vl_segment5
       from fnd_flex_values_vl ffvl,
            fnd_flex_value_sets ffvs
      where ffvl.flex_value_set_id = ffvs.flex_value_set_id
        and ffvs.flex_value_set_name ='RG_PRODUCT'
        and ffvl.enabled_flag = 'Y'
		and ffvl.flex_value_meaning = r_cur.product
           and ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y');

		IF vl_segment5 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Product Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type20');
    vl_message := 'E';
		RAISE Custom_exception;
		END IF;
	END IF;
	--------------------------------------------------
	-- COA validation for Initiative
	IF r_cur.Initiative IS NULL
	THEN
	    UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Initiative Segment not popluated from Spread Sheet'
		       ,status       = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id;
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type21');
    vl_message := 'E';
		RAISE Custom_exception;
	ELSIF r_cur.Initiative IS NOT NULL
	THEN
	/*SELECT application_column_name
	  INTO vl_Initiative
      FROM FND_ID_FLEX_SEGMENTS_VL
     WHERE ID_FLEX_CODE = 'GL#'
       AND upper(segment_name) = UPPER('Initiative');

	SELECT count(1)
	  INTO vl_segment6
	  FROM gl_code_combinations
	 WHERE segment6 = r_cur.Initiative;*/
	 select count(1)
	   INTO vl_segment6
       from fnd_flex_values_vl ffvl,
            fnd_flex_value_sets ffvs
      where ffvl.flex_value_set_id = ffvs.flex_value_set_id
        and ffvs.flex_value_set_name ='RG_INITIATIVE'
        and ffvl.enabled_flag = 'Y'
        and ffvl.flex_value_meaning =  r_cur.Initiative
           and ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y');

		IF vl_segment6 = 0
		THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = 'Initiative Segment is not valid'
	    	  ,status        = 'E'
		 WHERE rowid = r_cur.row_id
		   AND group_id = r_cur.group_id
		   AND status IN ('N','E','V');
		COMMIT;
    DBMS_OUTPUT.PUT_LINE('Invoice Type22');
    vl_message := 'E';
		RAISE Custom_exception;
		END IF;
	END IF;
  IF X_status IS NULL
  THEN
  X_status := 'S';
  /*UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = NULL
	    	  ,status        = 'V'
		 WHERE rowid = r_cur.row_id
       AND group_id = P_GROUP_ID
		   AND status IN ('N','E','V');
		COMMIT;*/
  END IF;
   IF vl_message IS NULL
   THEN
   UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = NULL
	    	  ,status        = 'V'
		 WHERE rowid = r_cur.row_id
       AND group_id = P_GROUP_ID
		   AND status IN ('N','E','V');
		COMMIT;
   END IF;

	--------------------------------------------------
	EXCEPTION
	    WHEN Custom_exception THEN
		X_status := 'E';
    DBMS_OUTPUT.PUT_LINE('Invoice Type24');
		WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Invoice Type25');
    DBMS_OUTPUT.PUT_LINE('Error:'||SQLERRM);
		X_status := 'E';
	FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in validation Loop:'||SQLERRM);
	--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred while validating loop: '||SQLERRM);
	END;
	END LOOP;
    IF (X_status IS NULL OR X_status <> 'E')
	THEN
		UPDATE RGAP_INV_UPLFORM_STG
		   SET error_message = NULL
	    	  ,status        = 'V'
		 WHERE group_id = P_GROUP_ID
		   AND status IN ('N','E','V');
		COMMIT;
		X_status := 'S';
	END IF;
EXCEPTION
	WHEN OTHERS THEN
	X_status := 'E';
  DBMS_OUTPUT.PUT_LINE('Invoice Type26');
	FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in validation Procedure:'||SQLERRM);
	--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred while validating: '||SQLERRM);
END RGAP_EMP_INV_VALIDATION;
PROCEDURE RGAP_EMP_INV_INT(ERRBUF     OUT VARCHAR2,
                           RETCODE    OUT NUMBER,
                           P_GROUP_ID IN NUMBER,
                           p_batch    IN VARCHAR2,
                           p_gl_date  IN VARCHAR2
						  )
IS
CURSOR C_CUR
IS
SELECT rius.rowid as row_id, rius.*
  FROM RGAP_INV_UPLFORM_STG rius
 WHERE group_id = P_GROUP_ID
   AND status = 'V';

vInvoice_id      NUMBER;
vInvoice_line_id NUMBER;
vInvoice_tax_id  NUMBER;
vUser_id         NUMBER := FND_GLOBAL.USER_ID;
vOrg_id          NUMBER := FND_PROFILE.VALUE('ORG_ID');
vl_request_id    NUMBER;
v_gl_date        VARCHAR2(60);
vl_Invoice_type  VARCHAR2(100);
BEGIN
v_gl_date := to_char(to_date(substr(p_gl_date,1,10),'YYYY/MM/DD'),'DD-MON-YY');
 --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'RGAP_EMP_INV_UPLOAD_PKG.RGAP_EMP_INV_INT');
   FOR r_cur IN C_cur
   LOOP

	BEGIN
	vInvoice_id := AP_INVOICES_INTERFACE_S.NEXTVAL;
		IF r_cur.Expense_amount > 0
		THEN
		vl_Invoice_type := 'STANDARD';
		ELSIF r_cur.Expense_amount < 0
		THEN
		vl_Invoice_type := 'CREDIT';
		END IF;

	INSERT INTO AP_INVOICES_INTERFACE
				 (  INVOICE_ID,
					INVOICE_NUM,
					INVOICE_TYPE_LOOKUP_CODE,
					VENDOR_ID,
					VENDOR_NUM,
					VENDOR_SITE_ID,
					INVOICE_AMOUNT,
					INVOICE_CURRENCY_CODE,
					INVOICE_DATE,
					TERMS_ID,
					DESCRIPTION,
					SOURCE,
					PAY_GROUP_LOOKUP_CODE,
					GL_DATE,
					ORG_ID,
					PAYMENT_METHOD_CODE,
					ATTRIBUTE_CATEGORY,
					ATTRIBUTE11,
					ATTRIBUTE12,
					ATTRIBUTE13,
					ATTRIBUTE14,
					ATTRIBUTE15,
					group_id,
					CREATION_DATE,
					CREATED_BY,
					LAST_UPDATE_DATE,
					LAST_UPDATED_BY)
				VALUES
				   (vInvoice_id,
					r_cur.Invoice_number,
					vl_Invoice_type,
					r_cur.vendor_id,
					r_cur.Supplier_number,
					r_cur.vendor_site_id,
					(NVl(r_cur.Expense_amount,0) + NVL(r_cur.tax_amount,0)),
					'KRW',
					r_cur.Invoice_date,
					NULL,
					r_cur.Description,
					'KRSPREADSHEET',
					NULL,
					v_gl_date,
					vOrg_id,
					NULL,
					'RGK_KR_KRW_OU', --'JA.KR.APXINWKB.INVOICES',
					DECODE(r_cur.invoice_type,'COMPANY_CORP_CARD','C'
					                         ,'EMPLOYEE_CORP_CARD','C'
											 ,'EMPLOYEE_NON_CORP_CARD','O'
											 ,'Company Corporate Card','C'
											 ,'Employee Corporate Card','C'
											 ,'Employee Non Corporate Card','O'),
					r_cur.document_num,
					r_cur.Card_num,
					r_cur.Merchant_name,
					r_cur.Tax_registration_num,
					P_GROUP_ID,
					SYSDATE,
					vUser_id,
					SYSDATE,
					vUser_id);
		COMMIT;
		EXCEPTION
			WHEN OTHERS THEN
			FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
			--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
		END;

		vInvoice_line_id := AP_INVOICE_LINES_INTERFACE_S.NEXTVAL;

			BEGIN
			INSERT INTO AP_INVOICE_LINES_INTERFACE
			         ( INVOICE_ID,
					   INVOICE_LINE_ID,
					   Line_Number,
					   Line_Type_lookup_code,
					   Amount,
					   Description,
					   DIST_CODE_CONCATENATED,
					   ORG_ID,
					   ATTRIBUTE_CATEGORY,
					   ATTRIBUTE11,
					   ATTRIBUTE12,
					   ATTRIBUTE13,
					   ATTRIBUTE14,
					   TAX_CLASSIFICATION_CODE,
					   CREATION_DATE,
					   CREATED_BY,
					   LAST_UPDATE_DATE,
					   LAST_UPDATED_BY)
					VALUES
					   (vInvoice_id,
					    vInvoice_line_id,
						1,
						'ITEM',
						r_cur.Expense_amount,
						r_cur.Description,
						(r_cur.company||'.'||r_cur.account||'.'||r_cur.discipline||'.'||
						 r_cur.territory||'.'||r_cur.product||'.'||r_cur.Initiative||'.'||'000'||'.'||'0000'),
						vOrg_id,
						'RGK_KR_KRW_OU', --'JA.KR.APXINWKB.LINES',
						r_cur.employer,
						r_cur.Participant,
						r_cur.Purpose,
						r_cur.Venue,
						(CASE WHEN r_cur.tax_amount <> 0
						     THEN 'KR_VAT_10%_RECOVERABLE(INPUT)'
							 ELSE  NULL
						END),-- As per Maggie email we have included this tax code on 29-DEC-15 for line type ITEM., --'KRVAT_10%_RECOVERABLE(INPUT)',
						SYSDATE,
						vUser_id,
						SYSDATE,
						vUser_id);
			COMMIT;
			EXCEPTION
				WHEN OTHERS THEN
				FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
				--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
			END;
			IF r_cur.tax_amount <> 0
			THEN
			vInvoice_tax_id := AP_INVOICE_LINES_INTERFACE_S.NEXTVAL;
			BEGIN
			INSERT INTO AP_INVOICE_LINES_INTERFACE
			         ( INVOICE_ID,
					   INVOICE_LINE_ID,
					   Line_Number,
					   Line_Type_lookup_code,
					   Amount,
					   Description,
					   --DIST_CODE_CONCATENATED,
					   ORG_ID,
					  -- GLOBAL_ATTRIBUTE1,
					  -- GLOBAL_ATTRIBUTE2,
					  -- GLOBAL_ATTRIBUTE3,
					 --  GLOBAL_ATTRIBUTE4,
					   TAX_CLASSIFICATION_CODE,
					   CREATION_DATE,
					   CREATED_BY,
					   LAST_UPDATE_DATE,
					   LAST_UPDATED_BY)
					VALUES
					   (vInvoice_id,
					    vInvoice_tax_id,
						2,
						'TAX',
						r_cur.tax_amount,
						r_cur.Description,
						--(r_cur.company||'.'||r_cur.account||'.'||r_cur.discipline||'.'||
						-- r_cur.territory||'.'||r_cur.product||'.'||r_cur.Initiative||'.'||'000'||'.'||'0000'),
						vOrg_id,
					--	r_cur.employer,
					--	r_cur.Participant,
					--	r_cur.Purpose,
					--	r_cur.Venue,
						'KR_VAT_10%_RECOVERABLE(INPUT)',-- As per Maggie email we have changed this tax code on 29-DEC-15.'KRVAT_10%_RECOVERABLE(INPUT)',
						SYSDATE,
						vUser_id,
						SYSDATE,
						vUser_id);
			COMMIT;
			EXCEPTION
				WHEN OTHERS THEN
				FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
				--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred while Inseting AP Invoices into Interface Table: '||SQLERRM);
			END;
			END IF;
			UPDATE RGAP_INV_UPLFORM_STG
			   SET status = 'S'
			 WHERE rowid = r_cur.row_id
			   AND group_id = P_GROUP_ID;
			COMMIT;
	END LOOP;
	-- Calling Payable import Porgram.
	    BEGIN
		RGAP_PAYABLE_IMPORT_PRG(p_group_id   => P_GROUP_ID,
                                p_batch      => p_batch,
								x_request_id => vl_request_id);
		EXCEPTION
			WHEN OTHERS THEN
			FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while calling payable import Procedure:'||SQLERRM);
		END;
EXCEPTION
		WHEN OTHERS THEN
	FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in Procedure RGAP_EMP_INV_INT:'||SQLERRM);
	--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Unexpected error occurred in Procedure RGAP_EMP_INV_INT: '||SQLERRM);
END RGAP_EMP_INV_INT;
PROCEDURE RGAP_PAYABLE_IMPORT_PRG(p_group_id IN NUMBER,
                                  p_batch    IN VARCHAR2,
								                  x_request_id OUT NUMBER)
IS
lv_wait_req_id         BOOLEAN;
lv_phase               VARCHAR2(100);
lv_status              VARCHAR2(100);
lv_dev_phase           VARCHAR2(100);
lv_dev_status          VARCHAR2(100);
lv_msg                 VARCHAR2(2000);
lv_valid_request_id    NUMBER;
lv_request_id          NUMBER;
lv_vphase	           VARCHAR2(100);
lv_vstatus             VARCHAR2(100);
lv_vdev_phase          VARCHAR2(100);
lv_vdev_status         VARCHAR2(100);
lv_vmsg                VARCHAR2(2000);
v_vbatch_id            NUMBER;
BEGIN
	--rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'RGAP_EMP_INV_UPLOAD_PKG.RGAP_PAYABLE_IMPORT_PRG');

		fnd_global.apps_initialize(  fnd_profile.value('user_id')
                                       , fnd_profile.value('resp_id')
                                       , fnd_profile.value('resp_appl_id')
                                       );
            lv_request_id := fnd_request.submit_request(  application  =>'SQLAP'
                                                         , program      =>'APXIIMPT'
                                                         , description  => NULL
                                                         , start_time   => SYSDATE
                                                         , sub_request  => FALSE
                                                         , argument1    => NULL
                                                         , argument2    => 'KRSPREADSHEET'--lv_source_name
                                                         , argument3    => p_group_id --GROUP
                                                         , argument4    => p_batch    --lv_batch
                                                         , argument5    => 'Need AP Manager Approval' --'Project Hold'
                                                         , argument6    => 'Need AP Manager Approval' --'Project Hold'
                                                         , argument7    => NULL
                                                         , argument8    => NULL
                                                         , argument9    => NULL
                                                         , argument10   => NULL
                                                         , argument11   => NULL
                                                         , argument12   => NULL
                                                         , argument13   => NULL
                                                         , argument14   => NULL
                                                           );
			COMMIT;
			IF lv_request_id > 0
            THEN
				x_request_id := lv_request_id;
			ELSE
				x_request_id := 0;
			END IF;

			lv_wait_req_id:=fnd_concurrent.wait_for_request(  request_id  => lv_request_id
                                                              , interval    => 10
                                                              , max_wait    => 0
                                                              , phase       => lv_vphase
                                                              , status      => lv_vstatus
                                                              , dev_phase   => lv_vdev_phase
                                                              , dev_status  => lv_vdev_status
                                                              , message     => lv_vmsg
                                                               );
			FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_phase:   '||lv_vdev_phase);
            --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'lv_dev_phase:   '||lv_vdev_phase);
              IF lv_vdev_phase    = 'COMPLETE'
              THEN
                  IF lv_vdev_status = 'WARNING'
                  OR lv_vdev_status = 'ERROR'
                  THEN
                      FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_status: ' || lv_vdev_status);
					  ----rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE =>'lv_dev_status: ' || lv_vdev_status);
                  ELSIF lv_vdev_status = 'NORMAL'
                  THEN
                      FND_FILE.PUT_LINE(FND_FILE.LOG,'lv_dev_status: ' || lv_vdev_status);
					  --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE =>'lv_dev_status: ' || lv_vdev_status);
                  END IF;
              END IF;
EXCEPTION
	WHEN OTHERS THEN
	FND_FILE.PUT_LINE(FND_FILE.LOG,'Unexpected error occured while submitting Payable Import Program: ' || SQLERRM);
    --rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE =>'Unexpected error occured while submitting Payable Import Program:' || SQLERRM);
END RGAP_PAYABLE_IMPORT_PRG;
FUNCTION RGAP_UPLOAD(p_Group_id                     NUMBER
                    ,p_invoice_type                 VARCHAR2
					,p_Card_num         	        VARCHAR2
					,p_Employee_Name    	        VARCHAR2
					,p_Employee_number  	        VARCHAR2
					,p_Invoice_date                 DATE
					,p_Invoice_number               VARCHAR2
					,p_Expense_amount               NUMBER
					,p_Tax_amount                   NUMBER
					,p_Description                  VARCHAR2
					,p_company                      VARCHAR2
					,p_account                      VARCHAR2
					,p_discipline                   VARCHAR2
					,p_territory                    VARCHAR2
					,p_product                      VARCHAR2
					,p_Initiative                   VARCHAR2
					,p_document_num                 VARCHAR2
					,p_Merchant_name                VARCHAR2
					,p_Tax_registration_num         VARCHAR2
					,p_employer                     VARCHAR2
					,p_Participant                  VARCHAR2
					,p_Purpose                      VARCHAR2
					,p_Venue                        VARCHAR2
					,p_text1                        VARCHAR2
					,p_text2                   		VARCHAR2
					,p_text3                   		VARCHAR2
					,p_text4                   		VARCHAR2
					,p_text5                   		VARCHAR2
					,p_text6                   		VARCHAR2
					,p_text7                   		VARCHAR2
					,p_text8                   		VARCHAR2
					,p_text9                   		VARCHAR2
					,p_text10                  		VARCHAR2
					,p_text11                  		VARCHAR2
					,p_text12                  		VARCHAR2
					,p_text13                  		VARCHAR2
					,p_text14                  		VARCHAR2
					,p_text15                  		VARCHAR2
					)
RETURN VARCHAR2
AS
vError_Msg  VARCHAR2(2000);
vSys_Date   DATE      := SYSDATE;
vl_TRADING_PARTNER    VARCHAR2(100);
vl_vendor_id          NUMBER;
vl_SUPPLIER_NUMBER    VARCHAR2(60);
vl_SUPPLIER_SITE_NAME VARCHAR2(100);
vl_vendor_site_id     NUMBER;
BEGIN
  IF length(p_Description) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Description length';
	END IF;
	IF length(p_document_num) > 240
	THEN
	vError_Msg := vError_Msg||','||'Reduce Support Document Number length';
	END IF;
    IF length(p_Merchant_name) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Merchant Name length';
	END IF;
    IF length(p_Tax_registration_num) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Tax Registration Number length';
	END IF;
    IF length(p_employer) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Employer length';
	END IF;
    IF length(p_Participant) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Participant length';
	END IF;
	IF length(p_Purpose) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Purpose length';
	END IF;
	IF length(p_Venue) > 240
	THEN
	vError_Msg := vError_Msg||' '||'Reduce Venue length';
	END IF;
	IF (vError_Msg IS NULL)
    THEN
		BEGIN
		   SELECT aps.vendor_name,
			       aps.vendor_id,
			       aps.segment1,
			       apsa.vendor_site_code,
			       apsa.vendor_site_id
			  INTO vl_TRADING_PARTNER,
			       vl_vendor_id,
			       vl_SUPPLIER_NUMBER,
			       vl_SUPPLIER_SITE_NAME,
			       vl_vendor_site_id
			  FROM ap_suppliers aps,
			       ap_supplier_sites_all apsa,
			       per_all_people_f papf
			 WHERE papf.party_id = aps.party_id
			   AND aps.vendor_id = apsa.vendor_id
			   AND papf.employee_number = p_Employee_number
			   AND ROWNUM =1;
		EXCEPTION
			WHEN OTHERS THEN
			vl_TRADING_PARTNER := NULL;
		    vl_vendor_id       := NULL;
		    vl_SUPPLIER_NUMBER := NULL;
		    vl_SUPPLIER_SITE_NAME := NULL;
		    vl_vendor_site_id := NULL;
		END;
	--vl_group_id := RGAP_GROUP_ID_S.NEXTVAL
	    BEGIN
			INSERT INTO RGAP_INV_UPLFORM_STG
			       ( Group_id
					,invoice_type
					,Card_num
					,Employee_Name
					,Employee_number
                    ,TRADING_PARTNER
                    ,VENDOR_ID
                    ,SUPPLIER_NUMBER
                    ,SUPPLIER_SITE_NAME
                    ,VENDOR_SITE_ID
					,Invoice_date
					,Invoice_number
					,Expense_amount
					,Tax_amount
					,Description
					,company
					,account
					,discipline
					,territory
					,product
					,Initiative
					,document_num
					,Merchant_name
					,Tax_registration_num
					,employer
					,Participant
					,Purpose
					,Venue
					,status
					,Error_Message
					,attribute1
					,attribute2
					,attribute3
					,attribute4
					,attribute5
					,attribute6
					,attribute7
					,attribute8
					,attribute9
					,attribute10
					,attribute11
					,attribute12
					,attribute13
					,attribute14
					,attribute15
                    ,CREATION_DATE
					,CREATED_BY
					,LAST_UPDATE_DATE
					,LAST_UPDATED_BY
				   )
				VALUES
				   (p_Group_id,
				    p_invoice_type,
					p_Card_num,
					p_Employee_Name,
					p_Employee_number,
					vl_TRADING_PARTNER,
			        vl_vendor_id,
			        vl_SUPPLIER_NUMBER,
			        vl_SUPPLIER_SITE_NAME,
			        vl_vendor_site_id,
					p_Invoice_date,
					p_Invoice_number,
					p_Expense_amount,
					p_Tax_amount,
					p_Description,
					p_company,
					p_account,
					p_discipline,
					p_territory,
					p_product,
					p_Initiative,
					p_document_num,
					p_Merchant_name,
					p_Tax_registration_num,
					p_employer,
					p_Participant,
					p_Purpose,
					p_Venue,
					'N',
					NULL,
					p_text1,
					p_text2,
					p_text3,
					p_text4,
					p_text5,
					p_text6,
					p_text7,
					p_text8,
					p_text9,
					p_text10,
					p_text11,
					p_text12,
					p_text13,
					p_text14,
					p_text15,
					vSys_Date,
					FND_GLOBAL.USER_ID,
					vSys_Date,
					FND_GLOBAL.USER_ID
				   );

		EXCEPTION
			WHEN OTHERS THEN
			 vError_Msg := 'Unexpected error while inserting records: '||SQLERRM;
        END;

	IF vError_Msg IS NULL THEN
          RETURN NULL;
        ELSE
          RETURN vError_Msg;
        END IF;
    ELSE
        IF INSTR(vError_Msg,'_') = 1 THEN
          vError_Msg            := SUBSTR(vError_Msg,2);
        END IF;
        RETURN vError_Msg;
    END IF;

EXCEPTION
WHEN OTHERS THEN
vError_Msg              := vError_Msg ||'_'||'Unexpected error while loading AP Invoices : '||SQLERRM;
      IF INSTR(vError_Msg,'_') = 1 THEN
        vError_Msg            := SUBSTR(vError_Msg,2);
      END IF;
      RETURN vError_Msg;
END RGAP_UPLOAD;
PROCEDURE RGAP_RELEASE_INV_HOLD(ERRBUF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
								P_Batch  IN VARCHAR2)
IS
CURSOR c_cur(p_invoice_number varchar2)
IS
SELECT a.invoice_id, b.hold_lookup_code,a.org_id,b.held_by,a.invoice_num
  FROM ap_invoices_all a,
       ap_holds_all b
	   --ap_batches_all c
 WHERE 1=1 --c.batch_id = a.batch_id
   AND a.invoice_id = b.invoice_id
   AND a.invoice_num = p_invoice_number;
   l_sub_request_id NUMBER;

CURSOR cur_inv
IS
SELECT distinct invoice_number
  FROM RGAP_VALIDATE_INVOICES
 WHERE batch_name = P_Batch
   AND attribute1 = 'Y';
BEGIN

  FOR rec_inv IN cur_inv
  LOOP
	  FOR rec IN c_cur(rec_inv.invoice_number)
	  LOOP
		  BEGIN
		  FND_FILE.PUT_LINE(FND_FILE.LOG,'Operating Unit  :'||rec.org_id);
		  FND_FILE.PUT_LINE(FND_FILE.LOG,'Invoice Number  :'||rec.invoice_num);
		  FND_FILE.PUT_LINE(FND_FILE.LOG,'Hold lookup Code:'||rec.hold_lookup_code);
		  MO_GLOBAL.set_policy_context('S',rec.org_id);
			  AP_HOLDS_PKG.release_single_hold (X_invoice_id  => rec.invoice_id,
									  X_hold_lookup_code      => rec.hold_lookup_code,
									  X_release_lookup_code   => 'AP Manager Release',
									  X_held_by               => rec.held_by,
									  X_calling_sequence      => NULL);
		  EXCEPTION
		  WHEN OTHERS THEN
	      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error While Release hold for invoice Number:'||SQLERRM);
		  END;
		  /*l_sub_request_id :=
                apps.fnd_request.submit_request ('SQLAP',
                                                  'APPRVL',
                                                  'Invoice Validation',
                                                  NULL,
                                                  FALSE,
                                                  rec.org_id,
                                                  'All',
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  NULL,
                                                  rec.invoice_id,
                                                  NULL,
                                                  NULL
                                                --'N',
                                                --1000
                                                );
          COMMIT;
		  IF l_sub_request_id < 0
		  THEN
		  FND_FILE.PUT_LINE(FND_FILE.LOG,'Invoice Validation for Invoice Number Not submitted:'||rec.invoice_num);
		  ELSE
		  FND_FILE.PUT_LINE(FND_FILE.LOG,'Invoice Validation for Invoice Number submitted:'||rec.invoice_num||'Request Id:'||l_sub_request_id);
		  END IF;*/
	  END LOOP;
	  COMMIT;
END LOOP;
  BEGIN
		  EXECUTE IMMEDIATE 'TRUNCATE TABLE RGAP.RGAP_VALIDATE_INVOICES';
	EXCEPTION
	WHEN OTHERS THEN
	FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while Truncating table:'||SQLERRM);
	END;
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,'Error in Main:'||SQLERRM);
END	RGAP_RELEASE_INV_HOLD;
END RGAP_EMP_INV_UPLOAD_PKG;
/


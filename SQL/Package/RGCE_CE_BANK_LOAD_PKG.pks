CREATE OR REPLACE PACKAGE APPS.rgce_ce_bank_load_pkg
AS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Package is Used to load data into Cash Management Interface tables from files.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/

PROCEDURE SUBMIT_SHELL(p_request_id NUMBER);
PROCEDURE REUSB_BANK_SUB_CAP (
   piv_file_name     IN   VARCHAR2,
   pin_request_id    IN   NUMBER,
   piv_file_path     IN   VARCHAR2,
   piv_load_option   IN   VARCHAR2,
   piv_map_temp      IN   VARCHAR2,
   piv_bank_br_num   IN   VARCHAR2,
   piv_accnt_num     IN   VARCHAR2,
   piv_gl_date       IN   VARCHAR2
);
PROCEDURE RGCE_ERROR_REPORT (ERRBUF OUT VARCHAR2,
                             RETCODE OUT NUMBER);
PROCEDURE RGCE_BANL_INT_ERR_MAIL(p_request_id    NUMBER,
			         p_email_address VARCHAR2);
PROCEDURE SUBMIT_BANK_ERROR_PRC(x_request_id  OUT NUMBER);
-- Started by Mahipal on 03-FEB-16.
PROCEDURE RGCE_SEND_ERROR_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_SEND_SUCCESS_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_PROG_ERROR_MAIL(p_request_id NUMBER);
PROCEDURE RGCE_SEND_NOFILE_MAIL;
PROCEDURE RGCE_SEND_IMPORTERROR_MAIL(p_request_id NUMBER);
-- Ended By Mahipal.
PROCEDURE RGCE_ERRORS_STG;
PROCEDURE MAIN ( ERRBUF  OUT VARCHAR2,
                 RETCODE OUT NUMBER,
		 p_process_id IN VARCHAR2,
		 p_Mapping_id IN VARCHAR2);
END RGCE_CE_BANK_LOAD_PKG;
/
EXIT;
CREATE OR REPLACE PACKAGE APPS.rggl_daily_rates_pkg AS
------------------------------------------------------------------------
   -- Name...: RGGL_DAILY_RATES_PKG.pkb
   -- Desc...: This package is used to create daily rates in GL per business logic
   -- by inserting data into gl interface table and then calling rate import program
   --
   --
   -- History:
   --
   -- Date Name Version Number IssueNo Revision Summary
   -- ------------ -------------- --------------- --------- ------------------
   -- 15-Feb-2016 Mahipal Ardam 1.0 Created
   --
   -- ADDITIONAL NOTES
   -- ================
   --
   ------------------------------------------------------------------------

 g_log_level         NUMBER                  := 0;


PROCEDURE VALIDATE_STG;
PROCEDURE PROCESS_STG;
FUNCTION RGGL_first_day (p_date IN DATE) RETURN DATE;
FUNCTION RGGL_LAST_DAY(p_date IN DATE) RETURN DATE;
FUNCTION RGGL_NO_OF_DAYS(p_date IN DATE) RETURN NUMBER;
PROCEDURE RGGL_SEND_SUCCESS_MAIL;
PROCEDURE SUBMIT_RG_DAILY_IMPORT;
PROCEDURE RGGL_DAILY_RATE_RTP(ERRBUF  OUT VARCHAR2,
                              RETCODE OUT NUMBER);
PROCEDURE MAIN (ERRBUF OUT VARCHAR2,
                                 RETCODE OUT NUMBER);

PROCEDURE RGGL_PERIOD_AVG(ERRBUF  OUT VARCHAR2,
                                                         RETCODE OUT NUMBER);

PROCEDURE RGGL_CP_WAIT(P_REQ_ID  number);
END RGGL_DAILY_RATES_PKG;
/


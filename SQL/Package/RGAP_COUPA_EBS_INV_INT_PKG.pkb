CREATE OR REPLACE PACKAGE BODY APPS.rgap_coupa_ebs_inv_int_pkg IS
   ------------------------------------------------------------------------
   -- Name...: RGAP_COUPA_EBS_INV_INT_PKG.pkb
   -- Desc...: This package is used to process data from Coupa to EBS
   --
   --
   -- History:
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Date                       Name                        Version Number      Issue No       Revision                                     Summary
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   -- 13-Aug-2015           Praveen Yadiki                  1.0                                                                                Created initial version
   -- 16-Feb-2016           Srinivasa                            1.1                91 and 92                                              Check for Invoice coming for reprocess.Modified email procedure call
   --
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    16-May-2016    Prabhat                             1.3                                                                     1. Add logic to submit payable open interface by batch
   --                                                                                                                                                      2. Change site pick logic to exclude deactivated sites
   --                                                                                                                                                      3. introduce rollbak in interface table insert procedure to preven header insert
   --                                                                                                                                                         without line or partial line insert.
   --                                                                                                                                                     4. put logic to delete orphan stage line records
   --                                                                                                                                                     5. send completion email when there are no errors to notify
   --                                                                                                                                                     6. populate asset book type code in interface line table.
   --
   -- 22-Jun-2016   Prabhat                                 1.4                                                                     1. Fix interface table rejected records not geeting picked in payable open interface run
   --                                                                                                                                                         after grouping by batch id was introduced in May bucket..
   --                                                                                                                                                    2. When a invoice is already in EBS interface table stage table records are not getting
   --                                                                                                                                                        marked as 'E'. The record is not inserted into interface table but gets marked as 'P'.
   --                                                                                                                                                    3. Invoice already in EBS check moved from load_intf_data procedure to main as this
   --                                                                                                                                                        check was happening too late after all the validation.
   -- ADDITIONAL NOTES
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --
   -- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   -- Global Variables Declaration
             g_program_name   VARCHAR2 (30) := 'rgap_coupa_ebs_inv_int_pkg';
             g_source VARCHAR2 (30) := 'COUPA';

   -- Invoice Number Validation
   -- -----------------------------------
   FUNCTION get_inv_num_cnt (p_invoice_num IN ap_invoices_all.invoice_num%TYPE,
														p_vendor_id IN ap_suppliers.vendor_id%TYPE) RETURN NUMBER IS

                l_module_name VARCHAR2 (40) := 'Get_inv_num_cnt';
                l_invoice_num ap_invoices_all.invoice_num%TYPE;
                l_inv_num_count   NUMBER := 0;

   BEGIN

      Fnd_File.put_line (Fnd_File.LOG, 'Inside get_inv_num_cnt function to check if invoice created in EBS');
      Fnd_File.put_line (Fnd_File.LOG, 'Invoice Number: ' || p_invoice_num);

      BEGIN
         SELECT COUNT (aia.invoice_num)
           INTO l_inv_num_count
           FROM ap_invoices_all aia, ap_suppliers ap
          WHERE     aia.vendor_id = ap.vendor_id
                AND aia.invoice_num = p_invoice_num
                AND ap.vendor_id = p_vendor_id;

         Fnd_File.put_line (Fnd_File.LOG, 'Invoice in EBS: ' || l_inv_num_count);

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_inv_num_count := -1;
            RETURN (l_inv_num_count);

            Fnd_File.put_line (Fnd_File.LOG, 'Invoice not in EBS. OK to create new Invoice ');

            rgcomn_debug_pkg.log_message (p_level => g_log_level,
																		  p_program_name => l_module_name,
																		  p_message => 'Invoice not in EBS. OK to create new Invoice');

         WHEN OTHERS THEN
            l_inv_num_count := -1;
            RETURN (l_inv_num_count);

            Fnd_File.put_line (Fnd_File.LOG, 'Unable to get invoice count from EBS system. Please verify');
            rgcomn_debug_pkg.log_message (p_level => g_log_level,
																		  p_program_name => l_module_name,
																		  p_message => 'Unable to get invoice count from EBS system. Please verify');

      END;

      rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, 'Invoice num count: ' || l_inv_num_count);
      RETURN (l_inv_num_count);

   EXCEPTION
      WHEN OTHERS THEN
         l_inv_num_count := -1;
         RETURN (l_inv_num_count);

         rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, 'Program raised exception when validationg the invoice number');

   END get_inv_num_cnt;


   -- CCID Validation
   -- -----------------------
   FUNCTION get_ccid (p_ccid IN VARCHAR2,
										x_status OUT VARCHAR2,
										x_message OUT VARCHAR2) RETURN VARCHAR2 IS

      l_ccid          VARCHAR2 (100);
      l_module_name   VARCHAR2 (100) := 'Get_ccid_details';

   BEGIN
      SELECT code_combination_id
        INTO l_ccid
        FROM gl_code_combinations_kfv
       WHERE concatenated_segments = p_ccid;

      RETURN l_ccid;
      x_status := 0;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         x_status := 1;
         x_message := ' Exception No Data Found on CCID';
         rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, 'CCID Raised an Exception No Data Found');
         l_ccid := -1;
         RETURN l_ccid;

      WHEN OTHERS THEN
         x_status := 1;
         x_message := 'Error While Getting CCID values' || SQLERRM;
         rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, 'Error While Getting CCID values');
         l_ccid := -1;
         RETURN l_ccid;

   END get_ccid;


   --Waiting for a concurrent request
   -- ------------------------------------------
   FUNCTION wait_for_request (request_id IN NUMBER DEFAULT NULL,
													  phase OUT NOCOPY VARCHAR2,
													  status OUT NOCOPY VARCHAR2,
													  dev_phase OUT NOCOPY VARCHAR2,
													  dev_status OUT NOCOPY VARCHAR2,
													  MESSAGE OUT NOCOPY VARCHAR2) RETURN BOOLEAN IS

      call_status BOOLEAN;
      req_phase VARCHAR2 (30);
      rid NUMBER := request_id;
      i NUMBER;
      l_module_name VARCHAR2 (100) := 'wait_for_request';

   BEGIN
      LOOP
         call_status := fnd_concurrent.get_request_status (rid,
                                               NULL,
                                               NULL,
                                               phase,
                                               status,
                                               req_phase,
                                               dev_status,
                                               MESSAGE);

         IF (req_phase = 'COMPLETE') THEN
            dev_phase := req_phase;
            RETURN (call_status);
         END IF;

      -- Version 1.1 commented due to lot of debug messages during wait time
      /* rgcomn_debug_pkg.log_message (g_log_level,
      l_module_name,
      'WAIT_FOR_REQUEST'
      || 'WAITING FOR 10 more SECONDS'
      );
      */
      END LOOP;

   EXCEPTION

      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'FND_CONCURRENT.WAIT_FOR_REQUEST'|| 'FUNCTION ERRORED AS '|| SQLERRM);
         RETURN FALSE;

   END wait_for_request;


   FUNCTION Chk_Inv_exists (p_invoice_num   IN ap_invoices_interface.invoice_num%TYPE,
                                                    p_vend_num IN ap_invoices_interface.vendor_num%TYPE) RETURN NUMBER IS

          -- Variable
          -- -------------
          l_inv_count     NUMBER := 0;
          l_module_name   VARCHAR2 (40) := 'Chk_Inv_exists';

   BEGIN
      Fnd_File.put_line (Fnd_File.LOG, 'Inside chk_inv_exists');
      Fnd_File.put_line (Fnd_File.LOG, 'Invoice Number: ' || p_invoice_num);

      BEGIN
         SELECT COUNT (*)
           INTO l_inv_count
           FROM ap_invoices_interface
          WHERE     invoice_num = p_invoice_num
                AND vendor_num = p_vend_num
                AND ROWNUM = 1;

         Fnd_File.put_line (Fnd_File.LOG, 'Invoice count: ' || l_inv_count);

      EXCEPTION
         WHEN NO_DATA_FOUND THEN
            l_inv_count := -1;
      END;

      RETURN (l_inv_count);

   EXCEPTION
      WHEN OTHERS THEN
         l_inv_count := -1;
         RETURN (l_inv_count);

   END Chk_Inv_exists;



   PROCEDURE rgap_archive_records (p_request_id IN NUMBER) IS

      l_module_name   VARCHAR2 (50) := 'rgap_archive_records';

   BEGIN

      Fnd_File.put_line (Fnd_File.LOG, '---------------------------------------------');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting moving previousley processed records to archive table process');
      Fnd_File.put_line (Fnd_File.LOG, 'Inside rgap_archive_records');

      INSERT INTO rgap_cpebs_inv_hdr_hist
         SELECT *
           FROM rgap_cpebs_inv_hdr_stg
          WHERE processed_flag = 'P' AND request_id != p_request_id;

      DELETE rgap_cpebs_inv_hdr_stg
       WHERE processed_flag = 'P' AND request_id != p_request_id;

      INSERT INTO rgap_cpebs_inv_lines_hist
         SELECT *
           FROM rgap_cpebs_inv_lines_stg
          WHERE processed_flag = 'P' AND request_id != p_request_id;

      DELETE rgap_cpebs_inv_lines_stg
       WHERE processed_flag = 'P' AND request_id != p_request_id;

   EXCEPTION
      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Error executing archive code-' || SQLCODE);
         rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Error executing archive code-' || SQLERRM);

   END rgap_archive_records;



   -- Invoice date accounting date Validation
   PROCEDURE get_account_date (p_invoice_date IN rgap_cpebs_inv_hdr_stg.invoice_date%TYPE,
                                                            p_accnt_date OUT VARCHAR2,
                                                            x_status OUT VARCHAR2,
                                                            x_message OUT VARCHAR2) IS

      l_module_name   VARCHAR2 (40) := 'Get_account_date';
      l_accnt_date    VARCHAR2 (20);

   BEGIN
      Fnd_File.put_line (Fnd_File.LOG, 'Inside get_account_date procedure');
      Fnd_File.put_line (Fnd_File.LOG, 'Invoice Date: ' || p_invoice_date);

      SELECT DISTINCT TO_CHAR (TO_DATE (gps.period_name, 'MON-YY'), 'DD-MON-YY')
        INTO l_accnt_date
        FROM hr_operating_units hou, gl_period_statuses gps
             WHERE hou.set_of_books_id = gps.set_of_books_id
             AND gps.application_id = (SELECT application_id
                                                                FROM fnd_application
                                                                WHERE application_short_name = 'SQLAP')
             AND gps.closing_status = 'O'
             AND p_invoice_date BETWEEN TRUNC (gps.start_date) AND TRUNC (gps.end_date);

             Fnd_File.put_line (Fnd_File.LOG, 'Accounting date: ' || l_accnt_date);

              x_status := 'S';
              x_message := NULL;
              p_accnt_date := l_accnt_date;

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         x_status := 'E';
         x_message := 'Accounting Date is not in an open period' || p_invoice_date;
         p_accnt_date := NULL;

         Fnd_File.put_line (Fnd_File.LOG, 'Accounting Date is not in an open period');

         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	   p_program_name => l_module_name,
																	   p_message => 'Accounting Date is not in an open period');

      WHEN OTHERS THEN
                 x_status := 'E';
                 x_message := 'Error while deriving Accounting period' || p_invoice_date;
                 p_accnt_date := NULL;

         Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving Accounting period');

         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	   p_program_name => l_module_name,
																	   p_message => 'Error while deriving Accounting period');

   END get_account_date;


   PROCEDURE get_vendor_info (p_vend_num IN ap_suppliers.segment1%TYPE,
                                                          p_vendor_id OUT ap_suppliers.vendor_id%TYPE,
                                                          x_return_status OUT VARCHAR2,
                                                          x_message OUT VARCHAR2) IS

      l_vendor_id  ap_suppliers.vendor_id%TYPE;
      l_module_name   VARCHAR2 (50) := 'get_vendor_info';

   BEGIN
      Fnd_File.put_line (Fnd_File.LOG, 'Inside get_vendor_info procedure');
      Fnd_File.put_line (Fnd_File.LOG, 'vndor number: ' || p_vend_num);

       rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	 p_program_name => l_module_name,
																	 p_message => 'Vendor number' || p_vend_num);

      SELECT vendor_id
        INTO l_vendor_id
        FROM ap_suppliers
       WHERE segment1 = p_vend_num;

              x_return_status := 'S';
              x_message := NULL;
              p_vendor_id := l_vendor_id;

   EXCEPTION

      WHEN NO_DATA_FOUND THEN
         Fnd_File.put_line (Fnd_File.LOG, 'Vendor not defined in the EBS system. Please verify');
         x_return_status := 'E';
         x_message := 'Vendor not defined in the EBS system. Please verify';
         p_vendor_id := -1;

          rgcomn_debug_pkg.log_message (p_level => g_log_level,
																		p_program_name => l_module_name,
																		p_message => 'Vendor not defined in the EBS system. Please verify');

      WHEN OTHERS THEN

         Fnd_File.put_line (Fnd_File.LOG, 'Error in get_vendor_info: ' || SQLCODE || SQLERRM);
         x_return_status := 'E';
         x_message := SQLCODE || SQLERRM;
         p_vendor_id := -1;

          rgcomn_debug_pkg.log_message (p_level => g_log_level,
																		p_program_name => l_module_name,
																		p_message => 'Unable to derive vendor id');

   END;



   -- Sending exception report to users for stage table error records
   -- -----------------------------------------------------------------------------------
   PROCEDURE Send_stg_err_report (p_request_id rgap_cpebs_inv_hdr_stg.request_id%TYPE) IS

      l_errors NUMBER;
      l_module_name VARCHAR2 (50) := 'Send_stg_err_report';

   BEGIN

      Fnd_File.put_line (Fnd_File.LOG, 'Starting send_stg_err_report');

      SELECT COUNT (1)
        INTO l_errors
        FROM rgap_cpebs_inv_hdr_stg h,
                    rgap_cpebs_inv_lines_stg l
       WHERE h.invoice_id = l.invoice_id
        AND (h.processed_flag = 'E' OR l.processed_flag = 'E');
      --and h.request_id= p_request_id;

      IF l_errors > 0 THEN

         Fnd_File.put_line (Fnd_File.LOG, 'Error record exists in stage table. Total error records: '|| l_errors);
         Fnd_File.put_line (Fnd_File.LOG, 'Calling common utility to send error report via email');

         rgap_common_utility_pkg.rgap_send_email (p_dest => g_interface_email,
                                                                                            p_subject => g_stg_subject,
                                                                                            p_msg => rgap_common_utility_pkg.rgap_get_html_report ('SELECT distinct h.Vendor_num,
                                                                                                                                                                                                          h.invoice_num,
                                                                                                                                                                                                          h.invoice_amount,
                                                                                                                                                                                                          h.error_msg hdr_err_msg,
                                                                                                                                                                                                          l.error_msg line_err_msg,
                                                                                                                                                                                                          h.creation_date
                                                                                                                                                                                                          from rgap_cpebs_inv_hdr_stg h,
                                                                                                                                                                                                                    rgap_cpebs_inv_lines_stg l
                                                                                                                                                                                                           where h.invoice_id=l.invoice_id
                                                                                                                                                                                                           and (h.processed_flag = ''E'' OR l.processed_flag = ''E'')
                                                                                                                                                                                                           and h.request_id=l.request_id order by 1 asc'),
                                                                                            p_msg2 => g_coupa_msg2,
                                                                                            p_msg3 => g_coupa_msg3,
                                                                                            p_mailhost => g_mailhost,
                                                                                            p_port => g_port,
                                                                                            p_sender => g_sender);

      ELSE

         Fnd_File.put_line (Fnd_File.LOG, 'No Error record exists in stage table. Total error records: '|| l_errors);
         Fnd_File.put_line (Fnd_File.LOG, 'Calling common utility to send completion notice via email');

          rgap_common_utility_pkg.rgap_send_email (p_dest => g_interface_email,
																							p_subject => g_stg_nd_subject,
																							p_msg => g_nd_msg3,
																							p_msg2 => g_nd_msg1,
																							p_msg3 => null,
																							p_mailhost => g_mailhost,
																							p_port => g_port,
																							p_sender => g_sender);

      END IF;

   EXCEPTION
      WHEN OTHERS THEN

         Fnd_File.put_line (Fnd_File.LOG, 'Error while sending stage table error record email.');

         rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Error while sending stage table error record email.');

   END Send_stg_err_report;



   -- Invoice main validation program
   PROCEDURE validate_stage_recs (p_request_id NUMBER) IS

      CURSOR inv_hdr_info_cur (c_request_id NUMBER) IS
         SELECT rceihs.ROWID AS row_id,
                      rceihs.*
           FROM rgap_cpebs_inv_hdr_stg rceihs
           WHERE rceihs.processed_flag = 'N'
           AND request_id = c_request_id;

      CURSOR inv_line_info_cur (c_invoice_id    VARCHAR2,
                                                        c_request_id    NUMBER) IS
         SELECT rceils.ROWID AS row_id,
                      rceils.*
           FROM rgap_cpebs_inv_lines_stg rceils
           WHERE rceils.processed_flag = 'N'
                AND rceils.invoice_id = c_invoice_id
                AND request_id = c_request_id;

              l_vendor_id NUMBER;
              v_vendor_id NUMBER;
              l_vendorsite_id NUMBER;
              l_terms_id NUMBER;
              l_term_id NUMBER;
              l_vendor_name VARCHAR2 (100);
              l_invoice_exist VARCHAR2 (25);
              l_segment VARCHAR2 (100);
              l_ven_site_id NUMBER;
              l_invoice_cur_code VARCHAR2 (5);
              l_payment_method_code VARCHAR2 (100);
              l_line_lukup VARCHAR2 (100);
              l_ccid VARCHAR2 (150);
              l_status VARCHAR2 (1);
              l_l_status VARCHAR2 (1);
              l_msg VARCHAR2 (2000);
              l_l_msg VARCHAR2 (2000);
              l_invoice_num VARCHAR2 (150);
              l_inv_num_cnt NUMBER := 0;
              l_account_date VARCHAR2 (20);
              l_vendor_site_id NUMBER;
              ln_set_of_books_id NUMBER;
              l_last_name VARCHAR2 (50);
              l_first_name VARCHAR2 (50);
              l_error_msg VARCHAR2 (4000);
              l_l_error_msg VARCHAR2 (4000);
              l_error_count NUMBER;
              l_l_error_count NUMBER;
              l_module_name VARCHAR2 (50) := 'validate_stage_recs';
              l_inv_hdr_amt NUMBER;
              l_inv_line_amount NUMBER;
              l_tax_rate_code VARCHAR2 (150);
              l_tax_regime_code VARCHAR2 (150);
              l_tax_status_code VARCHAR2 (150);
              l_tax VARCHAR2 (150);
              l_tax_jurisdiction_code VARCHAR2 (150);
              l_vendor_num VARCHAR2 (100);
              l_vendor_site_code VARCHAR2 (100);
              l_ship_to_location_id NUMBER;
              l_state VARCHAR2 (100);
              l_acct_pay_ccid NUMBER;
              l_segment1 VARCHAR2 (100);
              l_request_id NUMBER := p_request_id;
              l_org_id NUMBER;

      BEGIN

       rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	 p_program_name => l_module_name,
																	 p_message => 'Inside Procedure Invoice_Main_val');

      Fnd_File.put_line (Fnd_File.output, 'Starting Stage record validation process');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting validate_stage_recs Procedure execution');

      Fnd_File.put_line (Fnd_File.LOG, 'Updating header stage records with request id');

      UPDATE rgap_cpebs_inv_hdr_stg
         SET request_id = l_request_id
         WHERE processed_flag = 'N'
         AND request_id IS NULL;

      Fnd_File.put_line (Fnd_File.LOG, 'Updating line stage records with request id');

      UPDATE rgap_cpebs_inv_lines_stg
         SET request_id = l_request_id
         WHERE processed_flag = 'N'
         AND request_id IS NULL;

      COMMIT;

      Fnd_File.put_line (Fnd_File.LOG, 'Header and Line records request id update complete');

      Fnd_File.put_line (Fnd_File.output, 'Starting Header records validation');
      Fnd_File.put_line (Fnd_File.LOG, '-----------------------------------------------');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting Header records validation');

      FOR rec_hdr IN inv_hdr_info_cur (l_request_id)
      LOOP
         l_status := NULL;
         l_msg := NULL;
         l_error_count := 0;
         l_error_msg := NULL;
         l_vendor_id := NULL;
         l_vendor_num := NULL;
         l_vendor_site_id := NULL;
         l_terms_id := NULL;
         l_invoice_cur_code := NULL;
         l_vendor_num := NULL;
         l_vendor_site_code := NULL;
         l_ship_to_location_id := NULL;
         l_acct_pay_ccid := NULL;
         l_org_id := NULL;

         BEGIN
            /*
            BEGIN
            SELECT vendor_id,
            segment1
            INTO l_vendor_id, l_vendor_num
            FROM ap_suppliers
            WHERE segment1 = rec_hdr.vendor_num;
            EXCEPTION
            WHEN OTHERS
            THEN
            l_vendor_id := NULL;
            l_vendor_num := NULL;
            rgcomn_debug_pkg.log_message
            (p_level => g_log_level,
            p_program_name => l_module_name,
            p_message => 'Vendor Id is null'
            );
            l_error_msg := 'Vendor Id is null';
            END;
            */

            IF rec_hdr.vendor_num IS NOT NULL THEN

               Fnd_File.put_line (Fnd_File.LOG, '-----------------------------------------------------');
               Fnd_File.put_line (Fnd_File.LOG, 'Calling get_vendor_info procedure to get vendor id');

                get_vendor_info (p_vend_num => rec_hdr.vendor_num,
												p_vendor_id => l_vendor_id,
												x_return_status => l_status,
												x_message => l_msg);

               IF NVL (l_status, 'X') = 'E' THEN

                  l_error_count := l_error_count + 1;
                  l_error_msg := l_msg;

                  Fnd_File.put_line (Fnd_File.LOG, 'Error count' || l_error_count);
                  Fnd_File.put_line (Fnd_File.LOG, 'Vendor not found in the system');
                  Fnd_File.put_line (Fnd_File.LOG, 'Return staus' || l_status);
                  Fnd_File.put_line (Fnd_File.LOG, 'Return message' ||l_error_msg);

               ELSE

                  Fnd_File.put_line (Fnd_File.LOG, 'Derived vendor id: '|| l_vendor_id);

               END IF;

            ELSE

               l_error_count := l_error_count + 1;
               l_error_msg := 'Vendor number in stage table is null';
               Fnd_File.put_line (Fnd_File.LOG, 'Error count' || l_error_count);
               Fnd_File.put_line (Fnd_File.LOG, 'Vendor number in stage table is null');

            END IF;


            -- Company code derivation
            -- ----------------------------------
            Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------');
            Fnd_File.put_line (Fnd_File.LOG, 'Derive company code from gl code combinations');

            BEGIN
               SELECT DISTINCT SUBSTR (dist_code_concatenated, 1, INSTR (dist_code_concatenated, '.') - 1)
                 INTO l_segment1
                 FROM rgap_cpebs_inv_lines_stg
                WHERE  invoice_id = rec_hdr.invoice_id
                      AND request_id = l_request_id
                      AND line_type_lookup_code = 'ITEM';

               Fnd_File.put_line (Fnd_File.LOG, 'company code: ' || l_segment1);

            EXCEPTION

               WHEN NO_DATA_FOUND THEN
                  l_error_count := l_error_count + 1;
                  l_error_msg := l_error_msg|| ','|| 'Code Combination Can not be null ';

                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				 p_program_name => l_module_name,
																				 p_message => 'Code Combination Can not be null, unable to derive company code');

               WHEN OTHERS THEN
                  l_error_count := l_error_count + 1;
                  l_segment1 := NULL;
                  l_error_msg := l_error_msg|| '-'|| 'Unable to derive comapny code for this invoice';

                   rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				 p_program_name => l_module_name,
																				 p_message => 'Unable to derive comapny code for this invoice');

            END;


            IF l_segment1 IS NOT NULL THEN

               Fnd_File.put_line (Fnd_File.LOG, 'Company segment derived from GLCC, will now derive information from supplier site.');


               -- Invoice payment term
               -- -----------------------------
               Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------');
               Fnd_File.put_line (Fnd_File.LOG, 'Deriving invoice payment term');

               BEGIN
                  SELECT apss.terms_id
                    INTO l_terms_id
                    FROM ap_supplier_sites_all apss,
                                 gl_code_combinations gcc,
                                 hz_party_sites hps
                    WHERE apss.accts_pay_code_combination_id = gcc.code_combination_id
                         AND apss.party_site_id=hps.party_site_id
                         AND apss.vendor_id = l_vendor_id
                         AND apss.inactive_date IS NULL
                         AND gcc.segment1 = l_segment1
                         AND hps.status = 'A'
                         AND ROWNUM = 1;

                  Fnd_File.put_line (Fnd_File.LOG, 'Derived term id: ' || l_terms_id);

               EXCEPTION

                  WHEN NO_DATA_FOUND THEN
                     l_terms_id := NULL;
                     l_error_count := l_error_count + 1;
                     l_error_msg := l_error_msg|| ','|| 'Unable to derive payment term from site.';

                     Fnd_File.put_line (Fnd_File.LOG,  'Unable to derive term id from vedor site for vendor'|| l_segment1);

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Unable to derive term id from vedor site for vendor'|| l_segment1);

                  WHEN OTHERS THEN
                     l_terms_id := NULL;
                     l_error_count := l_error_count + 1;
                     l_error_msg := l_error_msg|| ','|| 'Unable to derive payment term from site.';

                     Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving term id from vedor site for vendor'|| l_segment1);

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Error while deriving term id: '|| SQLCODE);

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Error while deriving term id: '|| SQLERRM);

               END;


               -- Derive invoice currency code
               -- -------------------------------------

               IF rec_hdr.invoice_currency_code IS NULL THEN

                  Fnd_File.put_line (Fnd_File.LOG, 'Invoice currency code is not specified will derive currency code from site.');

                  BEGIN
                     SELECT apss.invoice_currency_code
                       INTO l_invoice_cur_code
                       FROM ap_supplier_sites_all apss,
                                   gl_code_combinations gcc,
                                   hz_party_sites hps
                            WHERE apss.accts_pay_code_combination_id = gcc.code_combination_id
                            AND apss.party_site_id=hps.party_site_id
                            AND apss.vendor_id = l_vendor_id
                            AND gcc.segment1 = l_segment1
                            AND apss.inactive_date IS NULL
                            AND hps.status = 'A'
                            AND ROWNUM = 1;

                     Fnd_File.put_line (Fnd_File.LOG, 'Derived currency code: '|| l_invoice_cur_code);

                  EXCEPTION

                     WHEN NO_DATA_FOUND THEN
                        l_terms_id := NULL;
                        l_error_count := l_error_count + 1;
                        l_error_msg := l_error_msg|| ','|| 'Unable to derive payment currency code from site.';

                        Fnd_File.put_line (Fnd_File.LOG, 'Unable to derive currency code from vedor site for vendor '|| l_segment1);

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Unable to derive currency code from vedor site for vendor '|| l_segment1);

                     WHEN OTHERS THEN
                        l_terms_id := NULL;
                        l_error_count := l_error_count + 1;
                        l_error_msg := l_error_msg|| ','|| 'Unable to derive payment currency code from site.';

                        Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving currency codde from vedor site for vendor '|| l_segment1);

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Error while deriving currency code: '|| SQLCODE);

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Error while deriving currency code: '|| SQLERRM);

                  END;

               ELSE

                  l_invoice_cur_code := 'X';
                  Fnd_File.put_line (Fnd_File.LOG, 'Invoice currency code is specified by interface, not deriving currency code from site.');
                  Fnd_File.put_line (Fnd_File.LOG, 'Invoice currency code provided by coupa is: '|| rec_hdr.invoice_currency_code);

               END IF;


               -- Vendor Site Derivation
               -- ------------------------------
               Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------');
               Fnd_File.put_line (Fnd_File.LOG, 'Deriving vendor site infromation based on company code');

               BEGIN
                  SELECT assa.vendor_site_id,
                         --assa.terms_id,
                         --assa.invoice_currency_code,
                         assa.vendor_site_code,
                         assa.ship_to_location_id,
                         assa.org_id
                    INTO l_vendor_site_id,
                             l_vendor_site_code,
                             l_ship_to_location_id,
                             l_org_id
                    FROM ap_supplier_sites_all assa,
                                gl_code_combinations gcc,
                                hz_party_sites hps
                   WHERE assa.accts_pay_code_combination_id = gcc.code_combination_id
                         AND assa.party_site_id = hps.party_site_id
                         AND assa.vendor_id = l_vendor_id
                         AND gcc.segment1 = l_segment1
                         AND hps.status = 'A'
                         AND assa.inactive_date IS NULL
                         AND ROWNUM = 1;

                  Fnd_File.put_line (Fnd_File.LOG, 'Vendor site details derived');
                  Fnd_File.put_line (Fnd_File.LOG, 'Derived site code: ' || l_vendor_site_code);
                  Fnd_File.put_line (Fnd_File.LOG, 'Derived org id: ' || l_org_id);

               EXCEPTION

                  WHEN NO_DATA_FOUND THEN

                     l_error_count := l_error_count + 1;
                     l_error_msg := l_error_msg|| ','|| 'Unable to derive vendor site information';
                     l_vendor_id := NULL;
                     l_vendor_site_id := NULL;
                     --l_terms_id := NULL;
                     --l_invoice_cur_code := NULL;
                     l_vendor_num := NULL;
                     l_vendor_site_code := NULL;
                     l_ship_to_location_id := NULL;
                     l_org_id := NULL;

                     Fnd_File.put_line (Fnd_File.LOG, 'Unable to derive vendor site information');

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Unable to derive vendor site information');

                  WHEN OTHERS THEN

                     l_error_count := l_error_count + 1;
                     l_error_msg := l_error_msg|| ','|| 'Unable to derive vendor site information';
                     l_vendor_id := NULL;
                     l_vendor_site_id := NULL;
                     --l_terms_id := NULL;
                     --l_invoice_cur_code := NULL;
                     --l_vendor_num := NULL;
                     --l_vendor_site_code := NULL;
                     l_ship_to_location_id := NULL;
                     l_org_id := NULL;

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Error while deriving vendor site information: '|| SQLCODE);

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => 'Error while deriving vendor site information: '|| SQLERRM);

               END;

            ELSE

               Fnd_File.put_line (Fnd_File.LOG, 'Could not derive Company segment GLCC, would not be able to derive information from supplier site.');
               l_error_count := l_error_count + 1;
               l_error_msg := l_error_msg|| ','|| 'Could not derive Company segment GLCC, would not be able to derive information from supplier site.';

            END IF;


            /*------------------- Invoice Number Validation ------------*/
            /*
            SELECT COUNT(1)
            INTO l_invoice_num
            FROM ap_invoices_all aia, ap_suppliers ap
            WHERE aia.vendor_id = ap.vendor_id
            AND aia.invoice_num = rec_hdr.invoice_num
            AND ap.vendor_id = l_vendor_id;

            IF l_invoice_num > 0 THEN
            l_error_msg := l_error_msg ||','||'Invoice Number is already Exists';
            END IF;
            */


            -- Check if invoice exists
            -- -----------------------------
            Fnd_File.put_line (Fnd_File.LOG, '----------------------------------------------------------');
            Fnd_File.put_line (Fnd_File.LOG, 'Calling get_inv_num_cnt function to check if invoice created in EBS');

            l_inv_num_cnt := get_inv_num_cnt (p_invoice_num   => rec_hdr.invoice_num,
                                                                             p_vendor_id     => l_vendor_id);

            Fnd_File.put_line (Fnd_File.LOG,'Invoice count is: '|| l_inv_num_cnt|| '- For invoice: '|| rec_hdr.invoice_num);

            IF NVL (l_inv_num_cnt, -1) > 0 THEN

               Fnd_File.put_line (Fnd_File.LOG, 'Invoice already in EBS, can not create another Invocie with same number');
               l_error_count := l_error_count + 1;
               l_error_msg := l_error_msg|| '-'|| 'Invoice already in EBS, can not create another Invoice with same number';

            END IF;

            /*
            -- Get accounting date from open period
            -- ---------------------------------------------------
            Fnd_File.put_line(Fnd_File.log,'----------------------------------------------------------');
            Fnd_File.put_line(Fnd_File.log,'Calling get_account_date procedure to derive account date from a open period');

            get_account_date(p_invoice_date => rec_hdr.invoice_date,
            p_accnt_date => l_account_date,
            x_status => l_status,
            x_message => l_msg);

            Fnd_File.put_line(Fnd_File.log,'Accounting date: '||l_account_date);
            Fnd_File.put_line(Fnd_File.log,'Status from get_account_date: '||l_status);
            Fnd_File.put_line(Fnd_File.log,'Msg from get_account_date: '||l_msg);


            IF l_account_date IS NULL THEN
            l_error_count := l_error_count + 1;
            l_error_msg := l_error_msg||','||'Cound not derive accounting date from invoice date.';
            END IF;
            */



            --Invoice Type Validation
            -- ------------------------------
            Fnd_File.put_line (Fnd_File.LOG, '------------------------------------');
            Fnd_File.put_line (Fnd_File.LOG, 'Validating invoice type lookup code');

            IF rec_hdr.invoice_type_lookup_code IS NULL THEN

               Fnd_File.put_line (Fnd_File.LOG, 'Invoice type lookup code is null');

               l_error_count := l_error_count + 1;
               l_error_msg := l_error_msg || ' - ' || 'Invoice Type Code cannot be NULL ';

                rgcomn_debug_pkg.log_message (p_level => g_log_level,
																			  p_program_name => l_module_name,
																			  p_message => l_error_msg);

            END IF;

         END;


         -- Start of Line level validations
         -- --------------------------------------
         IF NVL (l_error_count, 0) = 0 THEN

            Fnd_File.put_line (Fnd_File.LOG, '-----------------------------------');
            Fnd_File.put_line (Fnd_File.LOG, 'Starting Line records validation');
            Fnd_File.put_line (Fnd_File.LOG, 'Header validation error count: ' || l_error_count);
            Fnd_File.put_line (Fnd_File.LOG, 'No error in Header records validation. Starting line level validation');

            l_l_status := NULL;
            l_l_msg := NULL;
            l_l_error_count := 0;
            l_l_error_msg := NULL;

            FOR rec_line IN inv_line_info_cur (rec_hdr.invoice_id, l_request_id)
            LOOP
               BEGIN
                  Fnd_File.put_line (Fnd_File.LOG, '-----------------------------');
                  Fnd_File.put_line (Fnd_File.LOG, 'Checking if line number is null');

                  -- Invoice Line Number Validation
                  -- -----------------------------------------
                  IF rec_line.line_number IS NULL THEN

                     l_l_error_count := l_l_error_count + 1;
                     l_l_error_msg := l_l_error_msg|| ' - '|| 'Invoice Line Number cannot be NULL';

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => l_l_error_msg);

                  END IF;

                  -- Invoice Line Type Validation
                  -- -------------------------------------
                  Fnd_File.put_line (Fnd_File.LOG, '-----------------------------');
                  Fnd_File.put_line (Fnd_File.LOG, 'Checking if line type is null');

                  IF rec_line.line_type_lookup_code IS NULL THEN

                     l_l_error_count := l_l_error_count + 1;
                     l_l_error_msg := l_l_error_msg|| ' - '|| 'Invoice Line Lookup code cannot be NULL ';

                      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					p_program_name => l_module_name,
																					p_message => l_l_error_msg);

                  END IF;

                  -- CCID Validation
                  /*IF rec_line.line_type_lookup_code != 'TAX' and rec_line.dist_code_concatenated IS NULL
                  THEN
                  l_l_error_msg := l_l_error_msg
                  || ' - '
                  || 'Invoice Line dist_code_concatenated cannot be NULL '
                  || ' Invoice Line dist_code_concatenated - '
                  || rec_line.dist_code_concatenated;
                  END IF;*/

                  /*l_ccid :=
                  get_ccid (rec_line.dist_code_concatenated,
                  l_l_status,
                  l_l_msg
                  );
                  l_l_error_count := l_l_status;
                  l_l_error_msg := l_l_msg || l_l_error_msg;
                  END IF;*/


                  BEGIN
                     Fnd_File.put_line (Fnd_File.LOG, '-----------------------------');
                     Fnd_File.put_line (Fnd_File.LOG, 'Validating if header and line amount matches');

                     SELECT ROUND (SUM (amount), 2)
                       INTO l_inv_line_amount
                       FROM rgap_cpebs_inv_lines_stg
                      WHERE  invoice_id = rec_line.invoice_id
                            AND request_id = l_request_id;

                     Fnd_File.put_line (Fnd_File.LOG, 'Invoice hdr Amt: ' || rec_hdr.invoice_amount);
                     Fnd_File.put_line (Fnd_File.LOG, 'Invoice line Amt: ' || l_inv_line_amount);

                     IF NVL (ROUND (rec_hdr.invoice_amount, 2), 0) <>
                           (  NVL (ROUND (l_inv_line_amount, 2), 0)
                            + NVL (ROUND (rec_hdr.control_amount, 2), 0)) THEN

                        l_l_error_count := l_l_error_count + 1;
                        l_l_error_msg := l_l_error_msg|| 'Invoice Header and line amount total do not match.';

                        Fnd_File.put_line (Fnd_File.LOG, 'Invoice Header and line amount total do not match.');
                     END IF;

                  EXCEPTION

                     WHEN OTHERS THEN
                        l_l_error_count := l_l_error_count + 1;
                        l_l_error_msg := l_l_error_msg|| 'Unable to match invoice header and line amount';

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => l_l_error_msg);

                  END;


                  IF rec_line.line_type_lookup_code = 'TAX' THEN

                     Fnd_File.put_line (Fnd_File.LOG, '-----------------------------');
                     Fnd_File.put_line (Fnd_File.LOG, 'Geting Tax details based on derived state');

                     BEGIN
                        BEGIN
                           l_state := NULL;

                           SELECT region_2
                             INTO l_state
                             FROM hr_locations
                            WHERE location_id = l_ship_to_location_id;

                        EXCEPTION
                           WHEN NO_DATA_FOUND THEN

                              l_l_error_count := l_l_error_count + 1;
                              l_l_error_msg := l_l_error_msg|| 'Unable derive state value from hr_locations for location_id'|| l_ship_to_location_id;
                              l_state := NULL;

                        END;

                        IF l_state IS NOT NULL THEN

                           SELECT zrb.tax_rate_code,
                                  zrb.tax_regime_code,
                                  zrb.tax_status_code,
                                  zrb.tax,
                                  zrb.tax_jurisdiction_code
                             INTO l_tax_rate_code,
                                  l_tax_regime_code,
                                  l_tax_status_code,
                                  l_tax,
                                  l_tax_jurisdiction_code
                             FROM zx_jurisdictions_vl zjv,
                                  zx_rates_b zrb,
                                  hz_geographies hg
                            WHERE     zrb.tax_regime_code =
                                         zjv.tax_regime_code
                                  AND zrb.tax = zjv.tax
                                  AND zrb.tax_jurisdiction_code =
                                         zjv.tax_jurisdiction_code
                                  AND zjv.zone_geography_id = hg.geography_id
                                  AND geography_code = l_state
                                  AND zrb.active_flag = 'Y'
                                  AND ROWNUM < 2;

                           UPDATE rgap_cpebs_inv_lines_stg
                              SET tax_rate_code = l_tax_rate_code,
                                  prorate_across_flag = 'Y',
                                  tax_regime_code = l_tax_regime_code,
                                  tax = l_tax,
                                  tax_jurisdiction_code =
                                     l_tax_jurisdiction_code,
                                  tax_status_code = l_tax_status_code
                            WHERE     invoice_id = rec_line.invoice_id
                                  AND line_type_lookup_code = 'TAX'
                                  AND request_id = l_request_id;

                           Fnd_File.put_line (Fnd_File.LOG, 'Tax details derived and updated in stage table');

                           COMMIT;

                        ELSE

                           l_l_error_count := l_l_error_count + 1;
                           l_l_error_msg := l_l_error_msg|| 'Unable derive state value from hr_locations for location_id'|| l_ship_to_location_id;

                        END IF;

                     EXCEPTION
                        WHEN OTHERS THEN
                           l_l_error_count := l_l_error_count + 1;
                           l_l_error_msg := l_l_error_msg|| 'Unable to update Line Tax Rate code'|| l_ship_to_location_id;

                            rgcomn_debug_pkg.log_message (p_level => g_log_level,
																						  p_program_name => l_module_name,
																						  p_message => 'Unable to update Line Tax Rate code');

                     END;
                  END IF;

               END;


               Fnd_File.put_line (Fnd_File.LOG, 'Line level validation completed');
               Fnd_File.put_line (Fnd_File.LOG, 'Line validation error count: ' || l_l_error_count);
               Fnd_File.put_line (Fnd_File.LOG, '-----------------------------');

               IF NVL (l_l_error_count, 0) > 0 THEN

                  BEGIN
                     Fnd_File.put_line (Fnd_File.LOG, 'Updating header record status to E as line records have validation errros');

                     UPDATE rgap_cpebs_inv_hdr_stg
                        SET processed_flag = 'E',
                               error_msg = l_l_error_msg,
                               last_update_date = SYSDATE,
                               last_updated_by = fnd_global.user_id
                               WHERE invoice_id = rec_hdr.invoice_id
                               AND request_id = l_request_id;

                     COMMIT;
                  EXCEPTION

                     WHEN OTHERS THEN

                        Fnd_File.put_line (Fnd_File.LOG, 'Error while updating Header error flag');

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Error while updating Header error flag');

                  END;

                  BEGIN
                     Fnd_File.put_line (Fnd_File.LOG, 'Updating line record status to E as line records have validation errros');

                     UPDATE rgap_cpebs_inv_lines_stg
                        SET processed_flag = 'E',
                               error_msg = l_l_error_msg,
                               last_update_date = SYSDATE,
                               last_updated_by = fnd_global.user_id
                            WHERE invoice_id = rec_line.invoice_id
                            --and line_number=rec_line.line_number
                            AND request_id = l_request_id;

                      /*UPDATE rgap_cpebs_inv_lines_stg
                      set processed_flag = 'E',
                      last_update_date = sysdate,
                      last_updated_by = fnd_global.user_id
                      where invoice_id = rec_line.invoice_id
                      and line_number=rec_line.line_number
                      and processed_flag='N' ;*/

                  EXCEPTION

                     WHEN OTHERS THEN

                        Fnd_File.put_line (Fnd_File.LOG, 'Error while updating Line flag');

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Error while updating Line flag');

                  END;

                  COMMIT;
                  EXIT;
               -- else for line error count
               -- error count is 0

               ELSE
                  Fnd_File.put_line (Fnd_File.LOG, 'Line validation error count should be zero');
                  Fnd_File.put_line (Fnd_File.LOG, 'Line validation error count: ' || l_l_error_count);

                  /*
                  --Requester first and last name updated for header records
                  -- ---------------------------------------------------------------------------
                  IF rec_hdr.requester_first_name IS NULL
                  AND rec_hdr.requester_last_name IS NULL THEN

                  BEGIN
                  SELECT last_name, first_name
                  INTO l_last_name, l_first_name
                  FROM per_all_people_f
                  WHERE person_id IN (
                  SELECT employee_id
                  FROM fnd_user
                  WHERE user_id =
                  rec_hdr.requester_id);

                  UPDATE rgap_cpebs_inv_hdr_stg
                  SET requester_first_name = l_first_name,
                  requester_last_name = l_last_name
                  WHERE invoice_id = rec_hdr.invoice_id
                  and request_id=l_request_id;

                  EXCEPTION

                  WHEN OTHERS THEN

                  rgcomn_debug_pkg.log_message
                  (p_level => g_log_level,
                  p_program_name => l_module_name,
                  p_message => 'Header Update for requester first and last name is errored out');

                  END;
                  END IF;
                  */



                  BEGIN
                     Fnd_File.put_line (Fnd_File.LOG, 'Updating Header record as validated, ready for interface table insert');

                     UPDATE rgap_cpebs_inv_hdr_stg
                        SET processed_flag = 'P',
                            vendor_id = l_vendor_id,
                            invoice_currency_code = DECODE (l_invoice_cur_code, 'X', rec_hdr.invoice_currency_code, l_invoice_cur_code),
                            last_update_date = SYSDATE,
                            last_updated_by = fnd_global.user_id,
                            --vendor_num = rec_hdr.vendor_num,
                            vendor_site_code = l_vendor_site_code,
                            vendor_site_id = l_vendor_site_id,
                            terms_date = NVL (terms_date, SYSDATE),
                            org_id = l_org_id
                      WHERE invoice_id = rec_hdr.invoice_id -- and processed_flag='N'
                            AND request_id = l_request_id;

                  EXCEPTION

                     WHEN OTHERS THEN
                        Fnd_File.put_line (Fnd_File.LOG, 'Header Update is validate records eerored out');

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Header Update is validate records eerored out');

                  END;

                  BEGIN
                     Fnd_File.put_line (Fnd_File.LOG, 'Updating Line record as validated, ready for interface table insert');
                     Fnd_File.put_line (Fnd_File.LOG, '-------------------------------------------------------');

                     UPDATE rgap_cpebs_inv_lines_stg
                        SET processed_flag = 'P',
                            last_update_date = SYSDATE,
                            last_updated_by = fnd_global.user_id,
                            org_id = l_org_id
                      WHERE     invoice_id = rec_line.invoice_id
                            AND line_number = rec_line.line_number
                            --and processed_flag='N'
                            AND request_id = l_request_id;

                  EXCEPTION

                     WHEN OTHERS THEN

                        Fnd_File.put_line (Fnd_File.LOG, 'Line Update is validate records errored out');

                         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																					   p_program_name => l_module_name,
																					   p_message => 'Line Update is validate records errored out');

                  END;

                  /*
                  --Requester first and last name updated for line records
                  IF rec_line.requester_first_name IS NULL
                  AND rec_line.requester_last_name IS NULL
                  THEN
                  BEGIN
                  SELECT last_name, first_name
                  INTO l_last_name, l_first_name
                  FROM per_all_people_f
                  WHERE person_id IN (
                  SELECT employee_id
                  FROM fnd_user
                  WHERE user_id =
                  rec_line.requester_id);

                  UPDATE rgap_cpebs_inv_lines_stg
                  SET requester_first_name = l_first_name,
                  requester_last_name = l_last_name
                  WHERE invoice_id = rec_line.invoice_id
                  AND line_number = rec_line.line_number
                  AND request_id=l_request_id;
                  EXCEPTION
                  WHEN OTHERS
                  THEN
                  rgcomn_debug_pkg.log_message
                  (p_level => g_log_level,
                  p_program_name => l_module_name,
                  p_message => 'Lines Update for requester first and last name is errored out'
                  );
                  END;
                  END IF;
                  */

                  COMMIT;
               END IF;

            END LOOP;
         -- else for header error count
         -- error found in header validation

         ELSE

            Fnd_File.put_line (Fnd_File.LOG, 'Header validation error count: ' || l_error_count);
            Fnd_File.put_line (Fnd_File.LOG, 'Error in Header records validation.');

            BEGIN
               Fnd_File.put_line (Fnd_File.LOG, 'Updating header record as error');
               Fnd_File.put_line (Fnd_File.LOG, 'Error msg String: ' || l_error_msg);

               UPDATE rgap_cpebs_inv_hdr_stg
                  SET processed_flag = 'E',
                      error_msg = l_error_msg,
                      last_update_date = SYSDATE,
                      last_updated_by = fnd_global.user_id
                WHERE invoice_id = rec_hdr.invoice_id --and processed_flag='N'
                AND request_id = l_request_id;

               COMMIT;

            EXCEPTION

               WHEN OTHERS THEN

                  Fnd_File.put_line (Fnd_File.LOG, 'Header Update is header records error flag eerored out');

                   rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				 p_program_name => l_module_name,
																				 p_message => 'Header Update is header records error flag eerored out');

            END;

            BEGIN
               Fnd_File.put_line (Fnd_File.LOG, 'Updating Line record as error');

               UPDATE rgap_cpebs_inv_lines_stg
                  SET processed_flag = 'E',
                      last_update_date = SYSDATE,
                      last_updated_by = fnd_global.user_id
                WHERE invoice_id = rec_hdr.invoice_id --and processed_flag='N'
                      AND request_id = l_request_id;

               COMMIT;

            EXCEPTION
               WHEN OTHERS THEN

                  Fnd_File.put_line (Fnd_File.LOG, 'Line Update is header records errored out');

                   rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				 p_program_name => l_module_name,
																				 p_message => 'Line Update is header records errored out');

            END;

            COMMIT;
         END IF;
      END LOOP;

      COMMIT;

   EXCEPTION

      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message (g_log_level, l_module_name || '.' || 'invoice_main_val', 'Validation Procedure errored out');

   END validate_stage_recs;


   --Load data form staging tables tom Interaface tables
   -- --------------------------------------------------------------------
   PROCEDURE load_intf_data (p_request_id NUMBER) IS

      v_invoice_exists NUMBER;
      l_module_name VARCHAR2 (100) := 'load_intf_data';
      l_err_flag VARCHAR2 (10);
      l_pres NUMBER;
      linv_line_insert_err   EXCEPTION;

      CURSOR cur_load_hdr IS
         SELECT h.ROWID hrow_id,
                      h.*
           FROM rgap_cpebs_inv_hdr_stg h
          WHERE h.processed_flag = 'P'
                AND h.request_id = p_request_id
                AND NOT EXISTS (SELECT 1
                                                FROM rgap_cpebs_inv_lines_stg l
                                                WHERE h.invoice_id = l.invoice_id
                                                AND l.processed_flag = 'E'
                                                AND l.request_id = p_request_id);

      CURSOR cur_load_line (p_invoice_id VARCHAR2) IS
         SELECT l.ROWID lrow_id,
                      l.*
           FROM rgap_cpebs_inv_lines_stg l
          WHERE l.invoice_id = p_invoice_id
                AND l.processed_flag = 'P'
                AND l.request_id = p_request_id;

   BEGIN
      Fnd_File.put_line (Fnd_File.output, 'Starting Procedure to insert data into open interface table from stage table');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting Procedure (load_intf_data) to insert data into open interface table from stage table');

      rgcomn_debug_pkg.log_message (g_log_level, l_module_name, 'Load Data Start of Stage table data into Oracle Interface Tables');

      FOR load_hdr IN cur_load_hdr
      LOOP
         BEGIN
            /*-------------------Vaidation for checking interface tables----------------
            BEGIN
            SELECT 1
            INTO v_invoice_exists
            FROM apps.ap_invoices_interface
            WHERE invoice_num = load_hdr.invoice_num
            ---AND vendor_num = load_hdr.vendor_num
            AND ROWNUM = 1;

            v_invoice_exists := 1;

            EXCEPTION
            WHEN NO_DATA_FOUND THEN
            v_invoice_exists := 0;

            WHEN OTHERS THEN
            v_invoice_exists := -2;

            END; */


             /*
            Fnd_File.put_line (Fnd_File.LOG, 'Checking if invoice#: '|| load_hdr.invoice_num|| ' already in open interface table. Calling chk_inv_exists Function');

            v_invoice_exists := chk_inv_exists (p_invoice_num   => load_hdr.invoice_num,
                                                                            p_vend_num      => load_hdr.vendor_num);

            Fnd_File.put_line (Fnd_File.LOG, 'Invoice count in open Interface table: ' || v_invoice_exists);
            */

            l_err_flag := 'N';

             /*
            IF v_invoice_exists > 0 THEN

               rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, load_hdr.invoice_num|| '-'|| load_hdr.vendor_num|| 'Invoice Already Exists in Invoice Interface Table');

               Fnd_File.put_line (Fnd_File.output, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| load_hdr.invoice_num);
               Fnd_File.put_line (Fnd_File.LOG, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| load_hdr.invoice_num);

                                 -- write stage table update here
                                 UPDATE rgap_cpebs_inv_hdr_stg
                                 SET processed_flag = 'E',
                                        error_msg = 'Invoice already in open Interface table. This invoice will not be inserted',
                                        last_update_date = SYSDATE,
                                        last_updated_by = fnd_global.user_id
                                        WHERE invoice_id = load_hdr.invoice_id
                                        AND request_id = p_request_id;

                                 UPDATE rgap_cpebs_inv_lines_stg
                                 SET processed_flag = 'E',
                                        error_msg = 'Invoice already in open Interface table. This invoice will not be inserted',
                                        last_update_date = SYSDATE,
                                        last_updated_by = fnd_global.user_id
                                        WHERE invoice_id = load_hdr.invoice_id
                                        AND request_id = p_request_id;
             */

            --ELSE

               -- Currency precision
               BEGIN
                  SELECT NVL (precision, 2)
                    INTO l_pres
                    FROM fnd_currencies
                         WHERE currency_code = load_hdr.invoice_currency_code
                         AND ROWNUM = 1;

               EXCEPTION
                  WHEN NO_DATA_FOUND THEN
                     l_pres := 2;

                  WHEN OTHERS THEN
                     rgcomn_debug_pkg.log_message (P_LEVEL => g_log_level,
                                                                                   P_PROGRAM_NAME => l_module_name,
                                                                                   P_MESSAGE =>  'Error while deriving currency precision for currency code: '|| load_hdr.invoice_currency_code|| '-'|| SQLCODE|| ' '|| SQLERRM);

                     Fnd_File.put_line (Fnd_File.LOG, 'Error while deriving currency precision.');

               END;


               Fnd_File.put_line (Fnd_File.LOG, 'Inserting Invoice#: '|| load_hdr.invoice_num|| ' in header Interface table.');
               SAVEPOINT inv_insert;

               BEGIN
                  INSERT INTO ap_invoices_interface (invoice_id,
                                                operating_unit,
                                                attribute1,
                                                pay_group_lookup_code,
                                                vendor_name,
                                                vendor_id,
                                                vendor_num,
                                                vendor_site_code,
                                                vendor_site_id,
                                                requester_id,
                                                requester_first_name,
                                                requester_last_name,
                                                invoice_type_lookup_code,
                                                invoice_date,
                                                --terms_date,
                                                invoice_num,
                                                invoice_amount,
                                                invoice_currency_code,
                                                po_number,
                                                attribute6,
                                                SOURCE,
                                                description,
                                                control_amount,
                                                terms_name,
                                                accts_pay_code_combination_id,
                                                GROUP_ID,
                                                payment_method_code,
                                                org_id,
                                                terms_id,
                                                calc_tax_during_import_flag,
                                                invoice_received_date,
                                                created_by,
                                                creation_date,
                                                last_updated_by,
                                                last_update_date)
                  VALUES (ap_invoices_interface_s.NEXTVAL,
                                  load_hdr.operating_unit,
                                  load_hdr.attribute1,
                                  load_hdr.pay_group_lookup_code,
                                  load_hdr.vendor_name,
                                  load_hdr.vendor_id,
                                  load_hdr.vendor_num,
                                  load_hdr.vendor_site_code,
                                  load_hdr.vendor_site_id,
                                  load_hdr.requester_id,
                                  load_hdr.requester_first_name,
                                  load_hdr.requester_last_name,
                                  load_hdr.invoice_type_lookup_code,
                                  load_hdr.invoice_date,
                                  --load_hdr.terms_date,
                                  load_hdr.invoice_num,
                                  ROUND (load_hdr.invoice_amount, l_pres),
                                  load_hdr.invoice_currency_code,
                                  load_hdr.po_number,
                                  load_hdr.attribute6,
                                  load_hdr.SOURCE,
                                  load_hdr.description,
                                  ROUND (load_hdr.control_amount, l_pres),
                                  load_hdr.terms_name,
                                  load_hdr.accts_pay_code_combination_id,
                                  load_hdr.GROUP_ID,
                                  load_hdr.payment_method_code,
                                  load_hdr.org_id,
                                  load_hdr.terms_id,
                                  load_hdr.calc_tax_during_import_flag,
                                  load_hdr.invoice_received_date,
                                  fnd_global.user_id,
                                  SYSDATE,
                                  fnd_global.user_id,
                                  SYSDATE);

               EXCEPTION
                  WHEN OTHERS THEN
                     l_err_flag := 'Y';

                     rgcomn_debug_pkg.log_message (
                        g_log_level,
                        l_module_name,
                           load_hdr.invoice_num
                        || '-'
                        || load_hdr.vendor_num
                        || 'Exception'
                        || SUBSTR (SQLERRM, 1, 40)
                        || 'Interface Header records not inserted');

                     Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface heder. Invoice num: '|| load_hdr.invoice_num);
                     Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface heder. Invoice num: '|| SQLERRM);

               END;


               IF NVL (l_err_flag, 'N') <> 'Y' THEN

                  Fnd_File.put_line (Fnd_File.LOG, 'Starting record insertion into Open Interface Line table');
                  Fnd_File.put_line (Fnd_File.LOG, 'Inserting Invoice#: '|| load_hdr.invoice_num|| ' lines in open Interface table.');

                  FOR load_line IN cur_load_line (load_hdr.invoice_id)
                  LOOP
                     BEGIN
                        Fnd_File.put_line (Fnd_File.LOG, 'Invocie_line_id and num: '|| load_line.invoice_line_id|| ' and '|| load_line.line_number);

                        INSERT INTO ap_invoice_lines_interface (
                                  invoice_id,
                                  invoice_line_id,
                                  line_type_lookup_code,
                                  line_number,
                                  description,
                                  amount,
                                  accounting_date,
                                  po_number,
                                  po_line_number,
                                  po_line_id,
                                  po_unit_of_measure,
                                  unit_of_meas_lookup_code,
                                  quantity_invoiced,
                                  unit_price,
                                  dist_code_combination_id,
                                  dist_code_concatenated,
                                  po_shipment_num,
                                  requester_id,
                                  requester_first_name,
                                  requester_last_name,
                                  ship_to_location_code,
                                  line_group_number,
                                  prorate_across_flag,
                                  tax_regime_code,
                                  tax,
                                  tax_jurisdiction_code,
                                  tax_status_code,
                                  org_id,
                                  tax_rate_code,
                                  amount_includes_tax_flag,
                                  incl_in_taxable_line_flag,
                                  product_category,
                                  attribute_category,
                                  attribute1,
                                  attribute15, -- E98 Prabhat
                                  last_updated_by,
                                  last_update_date,
                                  created_by,
                                  creation_date,
                                  asset_book_type_code) --1.3#6 prabhat
                  VALUES (ap_invoices_interface_s.CURRVAL,
                                ap_invoice_lines_interface_s.NEXTVAL,
                                load_line.line_type_lookup_code,
                                load_line.line_number,
                                load_line.description,
                                ROUND (load_line.amount, l_pres),
                                load_line.accounting_date,
                                load_line.po_number,
                                load_line.po_line_number,
                                load_line.po_line_id,
                                load_line.po_unit_of_measure,
                                load_line.unit_of_meas_lookup_code,
                                load_line.quantity_invoiced,
                                load_line.unit_price,
                                load_line.dist_code_combination_id,
                                load_line.dist_code_concatenated,
                                load_line.po_shipment_num,
                                load_line.requester_id,
                                load_line.requester_first_name,
                                load_line.requester_last_name,
                                load_line.ship_to_location_code,
                                load_line.line_group_number,
                                load_line.prorate_across_flag,
                                load_line.tax_regime_code,
                                load_line.tax,
                                load_line.tax_jurisdiction_code,
                                load_line.tax_status_code,
                                load_line.org_id,
                                load_line.tax_rate_code,
                                load_line.amount_includes_tax_flag,
                                load_line.incl_in_taxable_line_flag,
                                load_line.product_category,
                                'US Context',                  --'RG US Data',
                                load_line.attribute1,
                                load_line.attribute15,
                                fnd_global.user_id,
                                SYSDATE,
                                fnd_global.user_id,
                                SYSDATE,
                                load_line.asset_book_type_code);
                     --COMMIT;

                     EXCEPTION

                        WHEN OTHERS THEN
                           rgcomn_debug_pkg.log_message (g_log_level, l_module_name, load_hdr.invoice_num|| '-'|| load_line.line_number|| 'Exception'|| SUBSTR (SQLERRM, 1, 40)|| 'Interface line records not inserted');

                           Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface line table. Invoice num: '|| load_hdr.invoice_num);
                           Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface line table. Invoice num: '|| SQLCODE);
                           Fnd_File.put_line (Fnd_File.LOG, 'Error while Inserting Invoice in open Interface line table. Invoice num: '|| SQLERRM);

                     END;
                  END LOOP;

               ELSE

                  ROLLBACK TO inv_insert;

               END IF;
            --END IF;

         EXCEPTION
            WHEN linv_line_insert_err THEN
               ROLLBACK TO inv_insert;

               UPDATE rgap_cpebs_inv_hdr_stg
                  SET processed_flag = 'E',
                         error_msg = 'Error while Inserting Invoice in open Interface line.'
                WHERE processed_flag = 'P'
                      AND invoice_id = load_hdr.invoice_id
                      AND request_id = p_request_id;

               UPDATE rgap_cpebs_inv_lines_stg
                  SET processed_flag = 'E',
                      error_msg = 'Error while Inserting Invoice in open Interface line.'
                WHERE processed_flag = 'P'
                      AND invoice_id = load_hdr.invoice_id
                      AND request_id = p_request_id;
         END;
      END LOOP;

      COMMIT;

                --rgap_load_ivoices(p_request_id);

   EXCEPTION

      WHEN OTHERS THEN

         rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || 'load_intf_data', 'Load stage to interface table program errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

         Fnd_File.put_line (Fnd_File.LOG, 'Error in load_intf_data procedure : ' || SQLCODE);
         Fnd_File.put_line (Fnd_File.LOG, 'Error in load_intf_data procedure: ' || SQLERRM);

   END load_intf_data;



   PROCEDURE import_invoices (p_request_id NUMBER) IS

      ---PRAGMA AUTONOMOUS_TRANSACTION;
      l_module_name   VARCHAR2 (30) := 'import_invoices';
      l_request_id    NUMBER;
      l_errors_cnt    NUMBER;
      l_phase         VARCHAR2 (80);
      l_status        VARCHAR2 (80);
      l_phase_code    VARCHAR2 (50);
      l_status_code   VARCHAR2 (50);
      l_message       VARCHAR2 (500);
      l_call_status   BOOLEAN;
      l_batch_id      VARCHAR2 (80);

      CURSOR grp_id_cur(p_request_id NUMBER) IS
           SELECT GROUP_ID
             FROM rgap_cpebs_inv_hdr_stg h
            WHERE h.processed_flag = 'P'
            AND h.request_id = p_request_id
            GROUP BY GROUP_ID
            union
              select group_id
              from ap_invoices_interface ai
              where ai.status is null
              and ai.group_id is not null
              group by group_id;

   BEGIN

      rgcomn_debug_pkg.log_message (g_log_level, l_module_name, '******************** Process Starts :'|| TO_CHAR (SYSDATE, 'DD:MON:YYYY HH:MI:SS')|| ' ******************** ');

      Fnd_File.put_line (Fnd_File.output, 'Starting Procedure to call Open Invoice Import Program');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting rgap_load_invoices Procedure execution');

          /*
          SELECT DISTINCT GROUP_ID
          INTO l_batch_id
          FROM rgap_cpebs_inv_hdr_stg h
          WHERE     h.processed_flag = 'P'
          AND h.request_id = p_request_id
          AND ROWNUM = 1;
          */

      BEGIN
         FOR grp_id_rec IN grp_id_cur (p_request_id)
         LOOP
            BEGIN
               SELECT responsibility_id
                 INTO g_resp_id
                 FROM fnd_responsibility
                WHERE UPPER (responsibility_key) LIKE g_resp_key
                      AND ROWNUM = 1;

               Fnd_File.put_line (Fnd_File.LOG, 'Responsibility Id: ' || g_resp_id);

               SELECT user_id
                 INTO g_user_id
                 FROM fnd_user
                WHERE user_name = g_user_name;

               Fnd_File.put_line (Fnd_File.LOG, 'User Id: ' || g_user_id);

               SELECT application_id
                 INTO g_resp_appl_id
                 FROM fnd_responsibility
                WHERE responsibility_id = g_resp_id;

               Fnd_File.put_line (Fnd_File.LOG, 'Responsibility Application Id: ' || g_resp_appl_id);

               IF fnd_profile.VALUE ('USERNAME') IS NULL THEN

                  fnd_global.apps_initialize (g_user_id, g_resp_id, g_resp_appl_id);
                  mo_global.set_policy_context ('S', fnd_profile.VALUE ('ORG_ID'));

               ELSE

                  FND_GLOBAL.Apps_Initialize (FND_GLOBAL.USER_ID, FND_GLOBAL.RESP_ID, FND_GLOBAL.RESP_APPL_ID);

               END IF;
            END;

            fnd_request.set_org_id (fnd_profile.VALUE ('ORG_ID'));


            l_request_id := fnd_request.submit_request (application   => 'SQLAP',
                                                                                              program       => 'APXIIMPT',
                                                                                              description   => 'Payables Open Interface Import',
                                                                                              start_time    => NULL,
                                                                                              sub_request   => FALSE,
                                                                                              argument1     => fnd_profile.VALUE ('ORG_ID'),
                                                                                              argument2     => g_source,                         -- source
                                                                                              argument3     => grp_id_rec.GROUP_ID, --NULL,                      -- group
                                                                                              argument4     => grp_id_rec.GROUP_ID, --l_batch_id, --g_source ||'-'|| TO_CHAR (SYSDATE,'DDMONYY')||p_request_id,--'N/A', -- batch_name
                                                                                              argument5     => NULL,                          -- hold_name
                                                                                              argument6     => NULL,                        -- hold_reason
                                                                                              argument7     => NULL,                      -- gl_date
                                                                                              argument8     => 'N',                         -- purge
                                                                                              argument9     => 'N',                        -- trace_switch
                                                                                              argument10    => 'N',                       -- debug_switch
                                                                                              argument11    => 'N',                       -- summarize report
                                                                                              argument12    => 1000,                   -- commit_batch_size
                                                                                              argument13    => g_user_id,
                                                                                              argument14    => g_user_id);
            COMMIT;

            Fnd_File.put_line (Fnd_File.output, 'Payable Open Invoice import request id: ' || l_request_id);
            Fnd_File.put_line (Fnd_File.LOG, 'Payable Open Invoice import request id: ' || l_request_id);
            Fnd_File.put_line (Fnd_File.output,'Batch Name: '|| grp_id_rec.GROUP_ID);
            Fnd_File.put_line (Fnd_File.LOG,'Batch Name: '|| grp_id_rec.GROUP_ID);

            IF l_request_id <> 0
            THEN
               -- DELETE FROM RGAP_CPEBS_INV_HDR_STG WHERE PROCESSED_FLAG = 'V';

               --- DELETE FROM RGAP_CPEBS_INV_LINES_STG where processed_flag = 'V';
               rgcomn_debug_pkg.log_message (
                  p_level          => g_log_level,
                  p_program_name   => l_module_name,
                  p_message        =>    ' Payables open interface concurrent program submitted with Request Request_Id:'|| '-'|| l_request_id);

               l_call_status := wait_for_request (request_id   => l_request_id,
                                                                             phase        => l_phase,
                                                                             status       => l_status,
                                                                            dev_phase    => l_phase_code,
                                                                            dev_status   => l_status_code,
                                                                            MESSAGE      => l_message);
               COMMIT;

               Fnd_File.put_line (Fnd_File.LOG, 'Waiting for Open Invoice Import program completion Request id: '|| l_request_id);

               rgcomn_debug_pkg.log_message (
                  g_log_level,
                  l_module_name,
                     g_source
                  || 'variables :'
                  || l_phase
                  || ':'
                  || l_status
                  || ':'
                  || l_phase_code
                  || ':'
                  || l_status_code
                  || ':'
                  || l_message);

               IF l_call_status = TRUE THEN

                  IF l_status IN ('Cancelled', 'Error', 'Terminated') THEN
                     rgcomn_debug_pkg.log_message (
                        g_log_level,
                        l_module_name,
                           g_source
                        || ' payable open interface import errored for request id '
                        || l_request_id
                        || ' ended with status'
                        || l_status);

                     Fnd_File.put_line (Fnd_File.LOG, 'Open Invoice Import program did not complete successfully');

                  ELSE
                     rgcomn_debug_pkg.log_message (
                        g_log_level,
                        l_module_name,
                        g_source || 'calling tie back program');

                     Fnd_File.put_line (Fnd_File.LOG, 'Calling Tieback program');
                  END IF;

               ELSE

                  rgcomn_debug_pkg.log_message (
                     g_log_level,
                     l_module_name,
                     g_source || 'WAITING WENT TO FALSE');

                  Fnd_File.put_line (Fnd_File.LOG, 'Waiting returned FALSE status.');

               END IF;

            ELSE
               rgcomn_debug_pkg.log_message (
                  g_log_level,
                  l_module_name,
                     g_source
                  || ' Couldn''t submit the request, please launch '
                  || '''Payables Open Interface Import'' program manually for source '
                  || g_source);

               Fnd_File.put_line (Fnd_File.output, 'Unable to submit Payable Open Invoice import request');
               Fnd_File.put_line (Fnd_File.LOG, 'Unable to submit Payable Open Invoice import request');
            END IF;
         END LOOP;

         COMMIT;

      EXCEPTION
         WHEN OTHERS THEN
            rgcomn_debug_pkg.log_message (
               p_level          => g_log_level,
               p_program_name   => l_module_name,
               p_message        =>    ' Exception at launching Payables open interface concurrent program '
                                   || '-'
                                   || SQLCODE
                                   || ' '
                                   || SQLERRM);


            Fnd_File.put_line (Fnd_File.LOG, 'Error in rgap_load_invoices Procedure: ' || SQLCODE);
            Fnd_File.put_line (Fnd_File.LOG, 'Error in rgap_load_invoices Procedure: ' || SQLERRM);

            rgap_common_utility_pkg.rgap_send_email (p_dest       => g_interface_email,
                                                                                               p_subject    => g_intf_subject,
                                                                                               p_msg        =>    ' Could not submit Payables Open Interface concurrent program'|| SQLCODE|| '-'|| SQLERRM,
                                                                                               p_msg2       => g_orcl_msg2,
                                                                                               p_mailhost   => g_mailhost,
                                                                                               p_port       => g_port,
                                                                                               p_sender     => g_sender);

      END;


      BEGIN
         SELECT COUNT (*)
           INTO l_errors_cnt
           FROM (SELECT air.*
                   FROM ap_interface_rejections air,
                        ap_invoices_interface aii
                  WHERE aii.invoice_id = air.parent_id
                        AND air.parent_table = 'AP_INVOICES_INTERFACE'
                        AND aii.request_id = l_request_id
                        AND aii.SOURCE = 'COUPA'
                 UNION
                 SELECT air.*
                   FROM ap_interface_rejections air,
                        ap_invoice_lines_interface aii,
                        ap_invoices_interface ai
                  WHERE aii.invoice_line_id = air.parent_id
                        AND aii.invoice_id = ai.invoice_id
                        AND air.parent_table = 'AP_INVOICE_LINES_INTERFACE'
                        AND ai.request_id = l_request_id
                        AND ai.SOURCE = 'COUPA');

         Fnd_File.put_line (Fnd_File.LOG, 'Total error record count after open interface run : '|| l_errors_cnt);

         IF l_errors_cnt > 0 THEN

            Fnd_File.put_line (Fnd_File.LOG, 'Error records in interface rejection table, preparing report');
            Fnd_File.put_line (Fnd_File.LOG, 'Error records in interface rejection table, preparing report');

            BEGIN
               rgap_common_utility_pkg.rgap_send_email (
                  p_dest       => g_interface_email,
                  p_subject    => g_intf_subject,
                  p_msg        => rgap_common_utility_pkg.rgap_get_html_report (
                                    'SELECT aii.vendor_num,
                                                 aii.invoice_num,
                                                 aii.invoice_amount,
                                                 air.reject_lookup_code,
                                                 aii.creation_date
                                     FROM ap_interface_rejections air,
                                                 ap_invoices_interface aii
                                     WHERE aii.invoice_id = air.parent_id AND
                                     air.parent_table = ''AP_INVOICES_INTERFACE'' AND
                                     aii.source = ''COUPA''
                                     UNION
                                     SELECT ai.vendor_num,
                                                  ai.invoice_num,
                                                  ai.invoice_amount,
                                                  air.reject_lookup_code,
                                                  aii.creation_date
                                     FROM ap_interface_rejections air,
                                     ap_invoice_lines_interface aii,
                                     ap_invoices_interface ai
                                     WHERE aii.invoice_line_id = air.parent_id AND
                                     aii.invoice_id=ai.invoice_id AND
                                     air.parent_table = ''AP_INVOICE_LINES_INTERFACE'' AND
                                     ai.source = ''COUPA'''),
                  p_msg2       => g_orcl_msg2,
                  p_msg3       => g_orcl_msg3,
                  p_mailhost   => g_mailhost,
                  p_port       => g_port,
                  p_sender     => g_sender);

            EXCEPTION

               WHEN OTHERS THEN
                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
                                                                                 p_program_name   => ' rgap_common_utility_pkg.rgap_send_email',
                                                                                 p_message =>    ' Exception while sending mail interface errored records throgh user.'|| '-'|| SQLCODE|| ' '|| SQLERRM);

                  Fnd_File.put_line (Fnd_File.LOG, 'Error while creating error reprt: '|| SQLCODE|| '-'|| SQLERRM);

            END;
         END IF;
      END;

   END import_invoices;


   --Calling Main validation program and load interface program
   -- --------------------------------------------------------------------
   PROCEDURE rgap_inv_main (errbuf OUT VARCHAR2, retcode OUT NUMBER) IS

              l_module_name VARCHAR2 (50) := 'rgap_inv_main';
              l_ret_status VARCHAR2 (1) := NULL;
              l_ret_message VARCHAR2 (250) := NULL;
              l_inv_records_found NUMBER;
              l_inv_line_records_found NUMBER;
              l_request_id NUMBER := fnd_global.conc_request_id;
              l_invoice_num rgap_cpebs_inv_hdr_stg.invoice_num%TYPE;
              l_line_count NUMBER;
              l_validated_count NUMBER := 0;
              l_prev_rec_cnt NUMBER;
              l_stg_error NUMBER;
              l_orcl_error NUMBER;
              l_inv_exists NUMBER := 0;

              CURSOR line_chk_cur (p_request_id NUMBER) IS
              SELECT COUNT (*), invoice_num
                     FROM rgap_cpebs_inv_hdr_stg
                     WHERE processed_flag = 'N'
                     AND request_id = p_request_id
                     GROUP BY invoice_num;

             CURSOR hdr_cur(p_request_id NUMBER) IS
             SELECT h.ROWID hrow_id,
                          h.*
                     FROM rgap_cpebs_inv_hdr_stg h
                     WHERE h.processed_flag = 'N'
                     AND h.request_id = p_request_id
                     AND NOT EXISTS (SELECT 1
                                                    FROM rgap_cpebs_inv_lines_stg l
                                                    WHERE h.invoice_id = l.invoice_id
                                                    AND l.processed_flag = 'E'
                                                    AND l.request_id = p_request_id);

   BEGIN

      Fnd_File.put_line (Fnd_File.output, '****************************************************************');
      Fnd_File.put_line (Fnd_File.output, 'Starting Coupa Invoice Data Import and Auto Invoice Creation Process');
      Fnd_File.put_line (Fnd_File.LOG, '****************************************************************');
      Fnd_File.put_line (Fnd_File.LOG, 'Starting Coupa Invoice Data Import and Auto Invoice Creation Process');

      Fnd_File.put_line (Fnd_File.output, 'Process Reqest Id: ' || l_request_id);
      Fnd_File.put_line (Fnd_File.LOG, 'Process Reqest Id:' || l_request_id);


      -- move previous processed record to archive after each run
      SELECT COUNT (*)
        INTO l_prev_rec_cnt
        FROM rgap_cpebs_inv_hdr_stg
        WHERE processed_flag = 'P'
        AND request_id != l_request_id;

      IF NVL (l_prev_rec_cnt, 0) > 0 THEN

         Fnd_File.put_line (Fnd_File.output, 'Calling process to archive previous processed stage records.');
         Fnd_File.put_line (Fnd_File.LOG, 'Calling process to archive previous processed stage records.');
         Fnd_File.put_line (Fnd_File.LOG, 'Prev record to be archived: ' || l_prev_rec_cnt);

         rgap_archive_records (l_request_id);

      ELSE

         Fnd_File.put_line (Fnd_File.output, 'No previous processed record to archive.');
         Fnd_File.put_line (Fnd_File.LOG, 'No previous processed record to archive.');

      END IF;


      --Count Total No.of Records in the staging table
      -- -----------------------------------------------------------
      SELECT COUNT (*)
        INTO l_inv_records_found
        FROM rgap_cpebs_inv_hdr_stg
        WHERE processed_flag = 'N'
        AND request_id IS NULL;

      Fnd_File.put_line (Fnd_File.output, 'Total Invoice header records in stage table: '|| l_inv_records_found);
      Fnd_File.put_line (Fnd_File.LOG, 'Total Invoice header records in stage table: '|| l_inv_records_found);

      rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	p_program_name => l_module_name,
																	p_message => 'Number of invoices found for Import :'|| l_inv_records_found);

      -- Start of Version 1.1
      DELETE FROM rgap_cpebs_inv_lines_stg rhs
            WHERE NVL (rhs.processed_flag, 'N') <> 'N'
                  AND EXISTS (SELECT 1
                                        FROM rgap_cpebs_inv_hdr_stg rci
                                        WHERE rci.processed_flag = 'N'
                                        AND rci.request_id IS NULL
                                        AND rci.invoice_num = rhs.invoice_num);

      DELETE FROM rgap_cpebs_inv_hdr_stg rhs
            WHERE NVL (rhs.processed_flag, 'N') <> 'N'
                  AND EXISTS (SELECT 1
                                        FROM rgap_cpebs_inv_hdr_stg rci
                                        WHERE rci.processed_flag = 'N'
                                        AND rci.request_id IS NULL
                                        AND rci.invoice_num = rhs.invoice_num);

      DELETE FROM rgap_cpebs_inv_lines_stg rls
            WHERE NVL (rls.processed_flag, 'N') NOT IN ('N', 'P')
            AND INVOICE_ID IN (SELECT INVOICE_ID
                                                FROM rgap_cpebs_inv_hdr_stg rhs
                                                WHERE NVL (rhs.processed_flag, 'N') NOT IN ('N', 'P')
                                                AND EXISTS (SELECT 1
                                                                        FROM ap_invoices_all a,
                                                                                    ap_invoice_lines_all b,
                                                                                    hr_operating_units hou
                                                                             WHERE a.invoice_id = b.invoice_id
                                                                             AND a.invoice_num = rhs.invoice_num
                                                                             AND a.vendor_id =  (SELECT vendor_id
                                                                                                                    FROM po_vendors
                                                                                                                    WHERE segment1 = rhs.vendor_num)
                                                AND a.org_id = hou.organization_id
                                                AND hou.organization_id IN (SELECT lookup_code
                                                                                                      FROM fnd_lookup_values
                                                                                                      WHERE lookup_type = 'RGAP_COUPA_PMT_OU_LKP')));

      DELETE FROM rgap_cpebs_inv_hdr_stg rhs
            WHERE NVL (rhs.processed_flag, 'N') NOT IN ('N', 'P')
             AND EXISTS (SELECT 1
                                    FROM ap_invoices_all a,
                                                ap_invoice_lines_all b,
                                                hr_operating_units hou
                                     WHERE a.invoice_id = b.invoice_id
                                     AND a.invoice_num = rhs.invoice_num
                                     AND a.vendor_id = (SELECT vendor_id
                                                                        FROM po_vendors
                                                                        WHERE segment1 = rhs.vendor_num)
                                     AND a.org_id = hou.organization_id
                                     AND hou.organization_id IN (SELECT lookup_code
                                                                                            FROM fnd_lookup_values
                                                                                            WHERE lookup_type = 'RGAP_COUPA_PMT_OU_LKP'));


         -- new delete to remove orphan line records from stage line table
         -- ----------------------------------------------------------------------------------
         delete from rgap_cpebs_inv_lines_stg rls
            where nvl (rls.processed_flag, 'N') not in ('N', 'P')
            and rls.invoice_id not in (select invoice_id
                                                            from rgap_cpebs_inv_hdr_stg rhs
                                                            where nvl (rhs.processed_flag, 'N') not in ('N', 'P'));


      COMMIT;
      -- End of Version 1.1

      IF l_inv_records_found > 0 THEN

         -- Verify if there is a header record in stage table without line
         FOR line_chk_rec IN line_chk_cur (l_request_id)
         LOOP
            BEGIN
               SELECT COUNT (*)
                 INTO l_line_count
                 FROM rgap_cpebs_inv_lines_stg ls
                WHERE ls.invoice_num = line_chk_rec.invoice_num
                      AND processed_flag = 'N'
                      AND request_id = l_request_id;

            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  l_line_count := 0;

            END;

            IF l_line_count = 0 THEN

               UPDATE rgap_cpebs_inv_hdr_stg
                  SET processed_flag = 'E',
                      error_msg = 'This Invoice has no line in lines table, can not be processed'
                      WHERE invoice_num = line_chk_rec.invoice_num
                            AND processed_flag = 'N'
                            AND request_id IS NULL;

            END IF;

         END LOOP;


         Fnd_File.put_line(Fnd_File.LOG, 'Checking if invoice already in interface table.');
         for hdr_rec in hdr_cur(l_request_id)
         loop
            begin

             Fnd_File.put_line (Fnd_File.LOG, 'Checking if invoice#: '|| hdr_rec.invoice_num|| ' already in open interface table. Calling chk_inv_exists Function');

             l_inv_exists := chk_inv_exists (p_invoice_num => hdr_rec.invoice_num,
                                                                    p_vend_num => hdr_rec.vendor_num);

            Fnd_File.put_line (Fnd_File.LOG, 'Invoice count in open Interface table: ' || l_inv_exists);

                 IF l_inv_exists > 0 THEN

                       rgcomn_debug_pkg.log_message (g_log_level, g_program_name || '.' || l_module_name, hdr_rec.invoice_num|| '-'|| hdr_rec.vendor_num|| 'Invoice Already Exists in Invoice Interface Table');

                       Fnd_File.put_line (Fnd_File.output, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| hdr_rec.invoice_num);
                       Fnd_File.put_line (Fnd_File.LOG, 'Invoice already in open Interface table. This invoice will not be inserted. Invoice num: '|| hdr_rec.invoice_num);

                                         -- write stage table update here
                                         UPDATE rgap_cpebs_inv_hdr_stg
                                         SET processed_flag = 'E',
                                                error_msg = 'Invoice already in open Interface table. This invoice will not be inserted',
                                                last_update_date = SYSDATE,
                                                last_updated_by = fnd_global.user_id
                                                WHERE invoice_id = hdr_rec.invoice_id
                                                AND request_id = l_request_id;

                                         UPDATE rgap_cpebs_inv_lines_stg
                                         SET processed_flag = 'E',
                                                error_msg = 'Invoice already in open Interface table. This invoice will not be inserted',
                                                last_update_date = SYSDATE,
                                                last_updated_by = fnd_global.user_id
                                                WHERE invoice_id = hdr_rec.invoice_id
                                                AND request_id = l_request_id;

                 end if;
             end;

         end loop;
         Fnd_File.put_line (Fnd_File.LOG, 'End of pre-validation process of stage header and line records');


         SELECT COUNT (1)
           INTO l_inv_line_records_found
           FROM rgap_cpebs_inv_lines_stg
           WHERE processed_flag = 'N'
           AND request_id IS NULL;

         Fnd_File.put_line (Fnd_File.output, 'Total Invoice line records in stage table: '|| l_inv_line_records_found);
         Fnd_File.put_line (Fnd_File.LOG, 'Total Invoice line records in stage table: '|| l_inv_line_records_found);

         rgcomn_debug_pkg.log_message (p_level  => g_log_level,
																	   p_program_name  => l_module_name,
																	   p_message  =>  'Number of invoice lines found for Import :'|| l_inv_line_records_found);

         IF l_inv_line_records_found > 0 THEN

            Fnd_File.put_line (Fnd_File.output, 'Starting Main Process');
            Fnd_File.put_line (Fnd_File.LOG, 'Starting Main Process');

            rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	     p_program_name   => l_module_name,
																	     p_message => 'Starting Main Process');

            BEGIN
               Fnd_File.put_line (Fnd_File.output, 'Calling process to validate data in stage table before insert to open interface table');
               Fnd_File.put_line (Fnd_File.LOG, 'Calling process to validate data in stage table before insert to open interface table');
               Fnd_File.put_line (Fnd_File.LOG, 'Calling validate_stage_recs Procedure...');

               rgcomn_debug_pkg.log_message (p_level => g_log_level,
																			 p_program_name   => l_module_name,
																			 p_message => 'Calling Validation Procedure validate_stage_recs');

               valIdate_stage_recs (l_request_id);

               Fnd_File.put_line (Fnd_File.LOG, 'Back from validate_stage_recs. Procedure completed.');

            EXCEPTION

               WHEN OTHERS THEN
                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				p_program_name => l_module_name,
																				p_message  =>  'Validation proceudre validate_stage_recs errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

                  rgcomn_debug_pkg.log_cp_message (p_level     => 0, p_message   => 'Validation proceudre is errored out');

            END;

            BEGIN
               SELECT COUNT (*)
                 INTO l_validated_count
                 FROM rgap_cpebs_inv_hdr_stg
                 WHERE processed_flag = 'P'
                 AND request_id = l_request_id;

               Fnd_File.put_line (Fnd_File.LOG, 'Total validated record found: ' || l_validated_count);

               rgcomn_debug_pkg.log_message (p_level => g_log_level,
																			 p_program_name   => l_module_name,
																			 p_message => 'Total validated record found: '|| l_validated_count);

            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                  l_validated_count := 0;

            END;

            IF NVL (l_validated_count, 0) > 0 THEN

               Fnd_File.put_line (Fnd_File.LOG, 'Total validated record: ' || l_validated_count);

               BEGIN
                  Fnd_File.put_line (Fnd_File.output, 'Call process to load invoice data in open interface table');


                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				p_program_name => l_module_name,
																				p_message => 'Calling Procedure load_intf_data');

                  Fnd_File.put_line (Fnd_File.output, 'Calling process to transfer data from stage to open interface table');
                  Fnd_File.put_line (Fnd_File.LOG, 'Calling process to transfer data from stage to open interface table');
                  Fnd_File.put_line (Fnd_File.LOG, 'Calling load_intf_data Procedure...');

                  load_intf_data (l_request_id);

                  Fnd_File.put_line (Fnd_File.LOG, 'Back from load_inf_date. Procedure completed.');

               EXCEPTION
                  WHEN OTHERS THEN
                     rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				   p_program_name   => l_module_name,
																				   p_message  =>  'Stage to interface validation load_intf_data procedure errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling load_intf_data procedure.' || SQLCODE);
                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling load_intf_data procedure.' || SQLERRM);

               END;



               BEGIN
                  Fnd_File.put_line (Fnd_File.output, 'Calling process to run open invoice interface import program');
                  Fnd_File.put_line (Fnd_File.LOG, 'Calling process to run open invoice interface program');
                  Fnd_File.put_line (Fnd_File.LOG, 'Calling import_invoices Procedure...');

                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				p_program_name => l_module_name,
																				p_message => 'Calling Procedure import_invoices');

                  import_invoices (l_request_id);

                  Fnd_File.put_line (Fnd_File.LOG, 'Back from import_invoices. Procedure completed.');

               EXCEPTION

                  WHEN OTHERS THEN

                     rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				   p_program_name => l_module_name,
																				   p_message => 'Open invoice import procedure import_invoices errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling lmport_invoices procedure.' || SQLCODE);
                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling lmport_invoices procedure.' || SQLERRM);

               END;


               BEGIN
                  Fnd_File.put_line (Fnd_File.output, 'Calling process to send stage error report');
                  Fnd_File.put_line (Fnd_File.LOG, 'Calling process send_stg_err_report to send stage error report');

                  rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				p_program_name => l_module_name,
																				p_message => 'Calling process to send stage error report');

                  send_stg_err_report (l_request_id);

                  Fnd_File.put_line (Fnd_File.LOG, 'Back from send stage error report. Procedure completed.');

               EXCEPTION

                  WHEN OTHERS THEN
                     rgcomn_debug_pkg.log_message (p_level => g_log_level,
																				   p_program_name => l_module_name,
																				   p_message =>    'process to send stage error report errored out'|| '-'|| SQLCODE|| ' '|| SQLERRM);

                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling process to send stage error report.'|| SQLCODE);
                     Fnd_File.put_line (Fnd_File.LOG, 'Error calling process to send stage error report.'|| SQLERRM);

               END;

            ELSE

               send_stg_err_report (l_request_id);

               Fnd_File.put_line (Fnd_File.output, 'No validated Invoice in stage table for insert into open interface table');
               Fnd_File.put_line (Fnd_File.output, 'Coupa Invoice import process complete');
               Fnd_File.put_line (Fnd_File.output, '------------------------------------------------------');
               Fnd_File.put_line (Fnd_File.LOG, 'No validated Invoice in stage table for insert into open interface table');
               Fnd_File.put_line (Fnd_File.LOG, 'Coupa Invoice import process complete');
               Fnd_File.put_line (Fnd_File.output, '------------------------------------------------------');

               rgcomn_debug_pkg.log_message (p_level => g_log_level,
																			 p_program_name => l_module_name,
																			 p_message => 'No validated Invoice in stage table for insert into open interface table');

            END IF;

         END IF;

     ELSE

         SELECT COUNT (1)
           INTO l_stg_error
           FROM rgap_cpebs_inv_hdr_stg rh,
                       rgap_cpebs_inv_lines_stg rl
          WHERE rh.invoice_id = rl.invoice_id
                AND (rh.processed_flag = 'E' OR rl.processed_flag = 'E');

         SELECT COUNT (1)
           INTO l_orcl_error
           FROM ap_invoices_interface
          WHERE source = 'COUPA' AND STATUS = 'REJECTED';

         Fnd_File.put_line (Fnd_File.LOG, 'No new record in stage table to process.');
         Fnd_File.put_line (Fnd_File.LOG, 'Stage error record : '||l_stg_error);
         Fnd_File.put_line (Fnd_File.LOG, 'Interface error record : '||l_orcl_error);

         IF l_stg_error > 0 THEN

              Fnd_File.put_line (Fnd_File.LOG, 'Stage error record exists calling send_stg_err_report.');
              send_stg_err_report (l_request_id);

         END IF;

         IF l_orcl_error > 0 THEN

              Fnd_File.put_line (Fnd_File.LOG, 'Interface error record exists sending interface error report.');

            rgap_common_utility_pkg.rgap_send_email (p_dest => g_interface_email,
                                                                                              p_subject => g_intf_subject,
                                                                                              p_msg => rgap_common_utility_pkg.rgap_get_html_report (
                                                                                                                 'SELECT aii.vendor_num,aii.invoice_num,aii.invoice_amount,air.reject_lookup_code,aii.creation_date
                                                                                                                 FROM ap_interface_rejections air,
                                                                                                                             ap_invoices_interface aii
                                                                                                                 WHERE aii.invoice_id = air.parent_id AND
                                                                                                                 air.parent_table = ''AP_INVOICES_INTERFACE'' AND
                                                                                                                 aii.SOURCE = ''COUPA''
                                                                                                                 UNION SELECT ai.vendor_num,ai.invoice_num,ai.invoice_amount,air.reject_lookup_code,aii.creation_date
                                                                                                                 FROM ap_interface_rejections air,
                                                                                                                             ap_invoice_lines_interface aii,
                                                                                                                             ap_invoices_interface ai
                                                                                                                 WHERE aii.invoice_line_id = air.parent_id AND
                                                                                                                 aii.invoice_id=ai.invoice_id AND
                                                                                                                 air.parent_table = ''AP_INVOICE_LINES_INTERFACE'' AND
                                                                                                                 ai.SOURCE = ''COUPA'''),
                                                                                               p_msg2       => g_orcl_msg2,
                                                                                               p_msg3       => g_orcl_msg3,
                                                                                               p_mailhost   => g_mailhost,
                                                                                               p_port       => g_port,
                                                                                               p_sender     => g_sender);

         END IF;

         Fnd_File.put_line (Fnd_File.output, 'No new record in stage table for processing. Process Complete.');
         Fnd_File.put_line (Fnd_File.LOG, 'No new record in stage table for processing. Process Complete. Sending completion notification.');

         rgap_common_utility_pkg.rgap_send_email (p_dest       => g_interface_email,
                                                                                              p_subject    => g_stg_nd_subject,
                                                                                              p_msg        => g_nd_msg2,
                                                                                              p_msg2       => g_nd_msg1,
                                                                                              p_msg3       => null,
                                                                                              p_mailhost   => g_mailhost,
                                                                                              p_port       => g_port,
                                                                                              p_sender     => g_sender);

         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	   p_program_name => l_module_name,
																	   p_message => 'Header and line stage tables there is no records to processd.');

      END IF;

   EXCEPTION

      WHEN NO_DATA_FOUND THEN

         rgcomn_debug_pkg.log_message (p_level => g_log_level,
																	   p_program_name => l_module_name,
																	   p_message => 'Header and line stage tables there is no records to processd.');

         Fnd_File.put_line (Fnd_File.output, 'No new record in stage table for processing. Main Process Complete.');
         Fnd_File.put_line (Fnd_File.LOG, 'No new record in stage table for processing. rgap_inv_main Process Complete.');

      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message (
            p_level => g_log_level,
            p_program_name => l_module_name,
            p_message => 'Header and line stage tables there is no records to processd.'|| '-'|| SQLCODE|| ' '|| SQLERRM);

         Fnd_File.put_line (Fnd_File.output, 'Error while executing main process.');
         Fnd_File.put_line (Fnd_File.LOG, 'Error while executing main process - rgap_inv_main.' || SQLCODE);
         Fnd_File.put_line (Fnd_File.LOG, 'Error while executing main process - rgap_inv_main.' || SQLERRM);

   END rgap_inv_main;

END rgap_coupa_ebs_inv_int_pkg;
/


CREATE OR REPLACE PACKAGE APPS.rgar_invoice_upload_pkg AS

g_log_level NUMBER := 0;

 FUNCTION RGAR_INV_UPL(P_GROUP1NV_ID     IN NUMBER,
                       P_Legal_Entity    IN VARCHAR2,
                       P_Customer_Name   IN VARCHAR2,
                       P_INVOICE_DATE    IN DATE,
                       P_GL_DATE         IN DATE,
                       P_Currency_code   IN VARCHAR2,
                       P_Class           IN VARCHAR2,
                       P_Invoice_type    IN VARCHAR2,
                       P_Description     IN VARCHAR2,
                       P_Amount          IN NUMBER,
                       P_Reference       IN VARCHAR2,
                       P_Segment1        IN VARCHAR2,
                       P_Segment2        IN VARCHAR2,
                       P_Segment3        IN VARCHAR2,
                       P_Segment4        IN VARCHAR2,
                       P_Segment5        IN VARCHAR2,
                       P_Segment6        IN VARCHAR2,
                       P_Segment7        IN VARCHAR2,
                       P_Segment8        IN VARCHAR2,
                       P_Text1           IN VARCHAR2,
                       P_Text2           IN VARCHAR2,
                       P_Text3           IN VARCHAR2,
                       P_Text4           IN VARCHAR2,
                       P_Text5           IN VARCHAR2,
                       P_Text6           IN DATE,
                       P_Text7           IN VARCHAR2,
                       P_Text8           IN VARCHAR2,
                       P_Text9           IN VARCHAR2,
                       P_Text10          IN VARCHAR2,
                       P_Text11          IN VARCHAR2,
                       P_Text12          IN VARCHAR2,
                       P_Text13          IN VARCHAR2,
                       P_Text14          IN VARCHAR2,
                       P_Text15          IN VARCHAR2)
RETURN VARCHAR2;
PROCEDURE MAIN( pBatch_ID   IN NUMBER);
PROCEDURE RGAR_RECEIVABLE_IMPORT_PRG(pgroup_id IN NUMBER);
PROCEDURE RGAR_CREATE_INVOICE_BATCHES(errbuf     OUT varchar2,
                                      retcode    OUT NUMBER,
                                                         p_batch_id IN  NUMBER);
END RGAR_INVOICE_UPLOAD_PKG;
/
EXIT;
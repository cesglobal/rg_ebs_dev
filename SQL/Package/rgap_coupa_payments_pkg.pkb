CREATE OR REPLACE PACKAGE BODY APPS.rgap_coupa_payments_pkg
AS
   /* ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose              : This Package is having Main Function to retuns payment information into the  Table of required columns.
   --
   --    Module               : RGAP
   --    19-Aug-2015    Srinivasa           1.0
   --
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Modification History :
   --       Date             	Name             Version Number      Revision Summary
   --     11/11/15		Prabhat			1.1							 Added additional logic to pick partially updated records
   --     2/16/16          Prabhat			1.2							 Changed pick logic to look for last_udate_date from ap_checks_all table
   --																						  instead of existing logic of checking from ap_invoice_payments_all as this table was
																							  getting updated multiple times. Approved change logged as #5 enhancement.
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables--
   g_program_name   VARCHAR2 (30) := 'RAP_coupa_payments_pkg';

   FUNCTION rgap_coupa_payments_fnc (p_asof_date DATE DEFAULT NULL)
      RETURN rgap_payment_nested_table
   IS
      l_ret   rgap_payment_nested_table;
	  l_run_date date;
   BEGIN
      rgcomn_debug_pkg.
       log_message (
         P_LEVEL          => 0,
         P_PROGRAM_NAME   => g_program_name,
         P_MESSAGE        => 'rgap_coupa_payments_fnc Enter Function');

      BEGIN
      SELECT lookup_code
        INTO g_org_id
      FROM fnd_lookup_values
      WHERE lookup_type LIKE 'RGAP_COUPA_PMT_OU_LKP' AND ROWNUM = 1;
      EXCEPTION
       WHEN NO_DATA_FOUND
         THEN
			g_org_id:=NULL;
        WHEN OTHERS
         THEN
            rgcomn_debug_pkg.
             log_message (P_LEVEL          => 0,
                          P_PROGRAM_NAME   => g_program_name,
                          P_MESSAGE        => 'Exception at deriving org_id');
      END;

      SELECT CAST (
                MULTISET (
                   SELECT DISTINCT
                          NVL (VEND.VENDOR_NAME, PRTY.PARTY_NAME) Supplier_Name,
                          NVL (VEND.SEGMENT1, PRTY.PARTY_NUMBER) Supplier_Number,
                          POH.SEGMENT1 PO_Number,
                          INV.INVOICE_NUM Invoice_Number,
						  INV.ATTRIBUTE1 Legacy_Inv_Num,
                          CHK.CLEARED_DATE Paid_Infull_Date,
                          --CHK.AMOUNT Amount_Paid,
                          inpay.amount Amount_Paid,
                          CHK.CHECK_NUMBER Check_Number,
                          CHK.CHECK_DATE Payment_Date,
                          CHK.PAYMENT_METHOD_CODE Payment_Method_Lookup_Code,
                          CHK.Description Note
                     FROM APPLSYS.FND_LOOKUP_VALUES Payment_Type,
								 AR.HZ_PARTIES# PRTY,
								 AP.AP_SUPPLIERS# VEND,
								 XLA.XLA_AE_LINES# XLINE,
								 XLA.XLA_AE_HEADERS# XHEAD,
								 XLA.XLA_EVENTS# XEVNT,
								 XLA.XLA_TRANSACTION_ENTITIES# TENTY,
								 GL.GL_LEDGERS# BOOK,
								 PO.PO_HEADERS_ALL# POH,
								 AP.AP_CHECKS_ALL# CHK,
								 AP.AP_INVOICES_ALL# INV,
								 AP.AP_INVOICE_PAYMENTS_ALL# INPAY
                    WHERE INV.INVOICE_ID = INPAY.INVOICE_ID
                          AND INPAY.CHECK_ID = CHK.CHECK_ID
                          AND INV.SET_OF_BOOKS_ID = BOOK.LEDGER_ID
                          AND INV.PO_HEADER_ID = POH.PO_HEADER_ID(+)
                          AND TENTY.SOURCE_ID_INT_1 = CHK.CHECK_ID
                          AND TENTY.APPLICATION_ID = 200
                          AND TENTY.ENTITY_CODE = 'AP_PAYMENTS'
                          AND TENTY.ENTITY_ID = XHEAD.ENTITY_ID(+)
                          AND TENTY.LEDGER_ID = XHEAD.LEDGER_ID(+)
                          AND XHEAD.EVENT_ID = XEVNT.EVENT_ID(+)
                          AND XHEAD.APPLICATION_ID = XEVNT.APPLICATION_ID(+)
                          AND (XEVNT.EVENT_TYPE_CODE IS NULL
                                   OR XEVNT.EVENT_TYPE_CODE IN ('PAYMENT CREATED', 'REFUND RECORDED'))
                          AND XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID(+)
                          AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID(+)
                          AND XLINE.ACCOUNTING_CLASS_CODE(+) = 'FUTURE_DATED_PMT'
                          AND NVL (CHK.ORG_ID, NVL (INPAY.ORG_ID, -9999)) = NVL (INPAY.ORG_ID, -9999)
                          AND NVL (INV.ORG_ID, NVL (INPAY.ORG_ID, -9999)) = NVL (INPAY.ORG_ID, -9999)
                          AND VEND.VENDOR_ID(+) = INV.VENDOR_ID
                          AND PRTY.PARTY_ID(+) = INV.PARTY_ID
                          AND CHK.PAYMENT_TYPE_FLAG = Payment_Type.LOOKUP_CODE(+)
                          AND Payment_Type.LOOKUP_TYPE(+) = 'PAYMENT TYPE'
                          AND Payment_Type.VIEW_APPLICATION_ID = 200
                          AND (G_Org_Id is null or exists (SELECT 1
																					 FROM fnd_lookup_values
																						WHERE lookup_code= INV.ORG_ID
																						AND lookup_type LIKE 'RGAP_COUPA_PMT_OU_LKP'))
                          --AND NVL (INPAY.LAST_UPDATE_DATE, SYSDATE) > NVL (p_asof_date, '01-Jan-1900')
                          AND NVL (CHK.LAST_UPDATE_DATE, SYSDATE) > NVL (p_asof_date, '01-Jan-1900')
                          AND (p_asof_date IS NOT NULL
                               --OR (NVL (INPAY.LAST_UPDATE_DATE, SYSDATE) > (SELECT LAST_RUN_TIME
                               OR (NVL (CHK.LAST_UPDATE_DATE, SYSDATE) > (SELECT LAST_RUN_TIME
																														FROM RGINTF_MONITOR_TAB
																															WHERE INTERFACE_NAME = G_Interface_name)))
                          AND Payment_Type.LANGUAGE(+) = 'US') AS rgap_payment_nested_table)
        INTO l_ret
        FROM DUAL;

	   select sysdate
      into l_run_date from dual;
      rgap_extract_result (l_run_date, 'Success');
      RETURN (l_ret);

   EXCEPTION
      WHEN NO_DATA_FOUND THEN
         rgap_extract_result (l_run_date, 'Failure');
         rgcomn_debug_pkg.log_message (P_LEVEL          => 0,
																		P_PROGRAM_NAME   => g_program_name,
																		P_MESSAGE        => 'NO Data Found ' || SQLERRM);

      WHEN OTHERS THEN
         rgap_extract_result (l_run_date, 'Failure');
         rgcomn_debug_pkg.log_message (P_LEVEL          => 0,
																	   P_PROGRAM_NAME   => g_program_name,
																		P_MESSAGE        =>   'Others Exception Raised'|| '-'|| '  '|| SQLERRM);
   END rgap_coupa_payments_fnc;


   PROCEDURE rgap_extract_result (p_date DATE, p_result VARCHAR2) IS
      PRAGMA AUTONOMOUS_TRANSACTION;

   BEGIN
      UPDATE RGINTF_MONITOR_TAB
         SET LAST_RUN_TIME = p_date,
             last_update_date = sysdate,
             last_updated_by =  fnd_global.user_id,
             run_status = p_result
       WHERE INTERFACE_NAME = G_Interface_name;

      COMMIT;
   EXCEPTION
      WHEN OTHERS THEN

         rgcomn_debug_pkg.log_message (P_LEVEL          => 0,
																	   P_PROGRAM_NAME   => g_program_name,
																		P_MESSAGE        =>   'Others Exception Raised at update'|| '-'|| SQLCODE|| '  '|| SQLERRM);
   END rgap_extract_result;

END rgap_coupa_payments_pkg;
/


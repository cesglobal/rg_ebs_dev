CREATE OR REPLACE PACKAGE APPS.RGCOMN_DEBUG_PKG AUTHID CURRENT_USER AS

-----------------------------------------------------------------------------------------------------------------------------
-- This is a Common Debug Package for all custom PL/SQL program created as  part of
-- RIOT ERP Implementation project. Any related Debug  programs that are generic
-- in nature should reside in this package.
-- --------------------------------------------------------------------------------------------------------------------------
--
-- MODIFICATION HISTORY
-- Date         SIR         Person              Comments
-- -------        -------       -------------          ------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------

PROCEDURE LOG_MESSAGE(P_LEVEL IN NUMBER,
                                                          P_PROGRAM_NAME IN VARCHAR2,
                                                          P_MESSAGE IN VARCHAR2);

PROCEDURE LOG_CP_MESSAGE(P_LEVEL IN NUMBER, P_MESSAGE IN VARCHAR2);


END RGCOMN_DEBUG_PKG;
/


CREATE OR REPLACE PACKAGE BODY APPS.RGAP_COMMON_UTILITY_PKG
AS
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose              : This Package is having Procedures Common utilities can be used across all interface/conversion programs.
   --
   --    Module               : RGAP
   --    Interface Tables     :
   --    Modification History :
   --       Date             Name             Version Number    Issue No   Revision Summary
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    14-Aug-2015    Srinivasa              1.0
   --    16-Feb-2016    Srinivasa              1.1               91 and 92 Update aginst issue added new procedure rgap_send_email to handle Email message
   --    11-May-2016 Prabhat                1.2   added a new procedure to take care of sequence increment while coupa and concur code is enhanced
   --    30-May-2016 Prabhat                1.3   added a if condition in rgap_send_email procedure to display error message text conditionally. 
   --
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables--
   g_rep_id         NUMBER := fnd_global.resp_id;
   g_rep_appl_id    NUMBER := fnd_global.resp_appl_id;
   g_program_name   VARCHAR2 (30) := 'rgap_common_utility_pkg'; 
   
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Purpose              : The Function rgap_get_html_report will be used to extract error data from stage table to html mail report .
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

   FUNCTION rgap_get_html_report (p_query IN VARCHAR2)
      RETURN CLOB
   IS
      ctxh             DBMS_XMLGEN.ctxhandle;
      xslt_tranfsorm   XMLTYPE;
      l_mail_body      CLOB;
   BEGIN
      ctxh := DBMS_XMLGEN.newcontext (p_query);

      -- XSLT Transformation to HTML
      xslt_tranfsorm :=
         NEW XMLTYPE (
                '
            <xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
              <xsl:template match="/ROWSET">
             <table  border="1">
              <tr>
                 <xsl:for-each select="ROW[1]/*">
               <th style="text-align:left;"> <xsl:value-of select="name()"/></th>
               </xsl:for-each>
                 <xsl:apply-templates/>
             </tr>
            </table>
             </xsl:template>
             <xsl:template match="ROW">
           <tr><xsl:apply-templates/></tr>
             </xsl:template>
             <xsl:template match="ROW/*">
           <td style="text-align:left;"><xsl:value-of select="."/></td>
             </xsl:template>
           </xsl:stylesheet>');

      DBMS_XMLGEN.setnullhandling (ctxh, DBMS_XMLGEN.empty_tag);

      DBMS_XMLGEN.setxslt (ctxh, xslt_tranfsorm);

      l_mail_body := DBMS_XMLGEN.getxml (ctxh);

      DBMS_XMLGEN.closecontext (ctxh);
      RETURN l_mail_body;
   EXCEPTION
      WHEN OTHERS
      THEN
         rgcomn_debug_pkg.
          log_message (
            P_LEVEL          => g_log_level,
            P_PROGRAM_NAME   => 'rgap_common_utility_pkg.rgap_get_html_report',
            P_MESSAGE        => 'Error Occured while constructing html email'
                               || '-'
                               || SQLCODE
                               || '  '
                               || SQLERRM);
   END rgap_get_html_report;

   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Purpose              : The Procedure rgap_sendmail will be used to send mail from database .
   -- This procedure will be used to email interface table error/exceptions
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

   PROCEDURE rgap_sendmail (p_dest IN VARCHAR2,
                                                        p_subject IN VARCHAR2,
                                                        p_msg IN VARCHAR2,
                                                        p_mailhost   IN VARCHAR2,
                                                        p_port NUMBER,
                                                        p_sender IN VARCHAR2) IS

      mail_conn   UTL_SMTP.connection;
      r  UTL_SMTP.replies;

      v_instance_name varchar2(20);

   BEGIN

      select name
      into v_instance_name
      from v$database;

      mail_conn := UTL_SMTP.open_connection (p_mailhost, p_port);
      r := UTL_SMTP.ehlo (mail_conn, p_mailhost);
      UTL_SMTP.mail (mail_conn, p_sender);
      UTL_SMTP.rcpt (mail_conn, p_dest);
      UTL_SMTP.open_data (mail_conn);
      UTL_SMTP.
      write_data (mail_conn, 'MIME-version: 1.0' || CHR (13) || CHR (10));
      UTL_SMTP.write_data(mail_conn,
                                              'Content-Type: text/html; charset=ISO-8859-15' || CHR (13) || CHR (10));
                                                UTL_SMTP. write_data (mail_conn,
                                               'Content-Transfer-Encoding: 8bit' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'From: ' || p_sender || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn,'Subject: '|| p_subject ||'-['||v_instance_name||']'|| CHR (13) || CHR (10)); --'Concurr Expenses Raised error during Oracle Import'
                                                UTL_SMTP.write_data (mail_conn, 'To: ' || p_dest || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, p_msg || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'Please note these are cumulative errors showing all of the error/exception records.' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.' || CHR (13) || CHR (10));
                                                UTL_SMTP.close_data (mail_conn);
                                                UTL_SMTP.quit (mail_conn);
   EXCEPTION
      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message(P_LEVEL          => g_log_level,
																	  P_PROGRAM_NAME   => 'rgap_common_utility_pkg.rgap_sendmail',
																      P_MESSAGE        =>  'Error Occured while sending email'|| '-'|| SQLCODE|| '  '|| SQLERRM);

   END rgap_sendmail;


--- Start Version 1.1
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Purpose              : The Procedure rgap_send_email will be used to send mail from database .
   -- This procedure will be used to email stage table error/exceptions
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/

   PROCEDURE rgap_send_email (p_dest IN VARCHAR2,
                                                        p_subject IN VARCHAR2,
                                                        p_msg IN VARCHAR2,
														p_msg2 IN VARCHAR2 DEFAULT NULL,
														p_msg3 IN VARCHAR2 DEFAULT NULL,
														p_msg4 IN VARCHAR2 DEFAULT NULL,
                                                        p_mailhost   IN VARCHAR2,
                                                        p_port NUMBER,
                                                        p_sender IN VARCHAR2) IS

      mail_conn   UTL_SMTP.connection;
      r  UTL_SMTP.replies;

      v_instance_name varchar2(20);

   BEGIN

      select name
      into v_instance_name
      from v$database;

      mail_conn := UTL_SMTP.open_connection (p_mailhost, p_port);
      r := UTL_SMTP.ehlo (mail_conn, p_mailhost);
      UTL_SMTP.mail (mail_conn, p_sender);
      UTL_SMTP.rcpt (mail_conn, p_dest);
      UTL_SMTP.open_data (mail_conn);
      UTL_SMTP.
      write_data (mail_conn, 'MIME-version: 1.0' || CHR (13) || CHR (10));
      UTL_SMTP.write_data(mail_conn,
                                              'Content-Type: text/html; charset=ISO-8859-15' || CHR (13) || CHR (10));
                                                UTL_SMTP. write_data (mail_conn,
                                               'Content-Transfer-Encoding: 8bit' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'From: ' || p_sender || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn,'Subject: '|| p_subject ||'-['||v_instance_name||']'|| CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'To: ' || p_dest || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, p_msg2 || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
												IF p_msg3 is NOT NULL THEN
												UTL_SMTP.write_data (mail_conn, p_msg3);
												END IF;
												UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
												IF p_msg4 is NOT NULL THEN
												UTL_SMTP.write_data (mail_conn, p_msg4);
												END IF;
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, p_msg || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                IF p_msg3 is NOT NULL THEN
                                                UTL_SMTP.write_data (mail_conn, 'Please note these are cumulative errors showing all of the error/exception records.' || CHR (13) || CHR (10));
                                                end if;
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, '<br>' || CHR (13) || CHR (10));
                                                UTL_SMTP.write_data (mail_conn, 'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.' || CHR (13) || CHR (10));
                                                UTL_SMTP.close_data (mail_conn);
                                                UTL_SMTP.quit (mail_conn);
   EXCEPTION
      WHEN OTHERS THEN
         rgcomn_debug_pkg.log_message(P_LEVEL          => g_log_level,
																	  P_PROGRAM_NAME   => 'rgap_common_utility_pkg.rgap_send_email',
																	  P_MESSAGE        =>  'Error Occured while sending email'|| '-'|| SQLCODE|| '  '|| SQLERRM);
   END rgap_send_email;
 --End  of Version 1.1


 PROCEDURE  Incrm_seq(errbuf OUT VARCHAR2,
                                             retcode OUT NUMBER,
                                             p_seq_name in varchar2,
                                             p_incrm_val in number) IS


              v_sql varchar2(1000);
              v_val number;

         begin

             Fnd_File.put_line (Fnd_File.LOG, 'Sequence name: '||p_seq_name);
             Fnd_File.put_line (Fnd_File.LOG, 'Increment By: '||p_incrm_val);

            for i in 1 .. p_incrm_val
            loop

                execute immediate
                ' select '||p_seq_name||'.NEXTVAL from dual' into v_val;

                Fnd_File.put_line (Fnd_File.LOG, 'Incremented value: '||v_val);

             end loop;

         exception
         when others then
         rgcomn_debug_pkg.log_message(p_level          => g_log_level,
																	  p_program_name   => 'rgap_common_utility_pkg.Incrm_seq',
																	  p_message        =>  'Error Occured while incrementing sequence'|| '-'|| sqlcode|| '  '|| sqlerrm);
        end;


END rgap_common_utility_pkg;
/
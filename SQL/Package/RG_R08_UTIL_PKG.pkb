CREATE OR REPLACE PACKAGE BODY APPS.RG_R08_UTIL_PKG
AS

FUNCTION GET_TRANS_NUM (p_header in number, p_line in varchar2) return varchar2 is
 l_trans_num varchar2(240);
begin
  SELECT
       XENTY.TRANSACTION_NUMBER Transaction_Number
       into l_trans_num
  FROM GL.GL_JE_CATEGORIES_TL GLCAT,
       GL.GL_CODE_COMBINATIONS ACCTS,
       APPLSYS.FND_APPLICATION_TL APPL,
       APPLSYS.fnd_lookup_values Accounting_Def_Owner_Type,
       APPLSYS.fnd_lookup_values Accounting_Class,
       APPLSYS.fnd_lookup_values Account_Type,
       XLE.XLE_ENTITY_PROFILES LENTY,
       AP.AP_SUPPLIER_SITES_ALL SSITE,
       AP.AP_SUPPLIERS SUPP,
       AR.HZ_CUST_ACCOUNTS CUSTA,
       AR.HZ_PARTY_SITES PSITE,
       AR.HZ_PARTIES PRTY,
       AR.HZ_CUST_SITE_USES_ALL SUSES,
       AR.HZ_CUST_ACCT_SITES_ALL ASITE,
       FUN.FUN_SEQ_VERSIONS RSEQV,
       FUN.FUN_SEQ_HEADERS RSEQH,
       FUN.FUN_SEQ_VERSIONS ASEQV,
       FUN.FUN_SEQ_HEADERS ASEQH,
       APPLSYS.FND_DOCUMENT_SEQUENCES DSEQ,
       XLA.XLA_PRODUCT_RULES_TL PRULT,
       XLA.XLA_PRODUCT_RULES_B PRULE,
       XLA.XLA_EVENT_CLASSES_TL EVCLS,
       XLA.XLA_EVENT_TYPES_TL EVTYP,
       XLA.XLA_TRANSACTION_ENTITIES XENTY,
       XLA.XLA_EVENTS XEVNT,
       GL.GL_IMPORT_REFERENCES GLREF,
       XLA.XLA_AE_HEADERS XHEAD,
       XLA.XLA_AE_LINES XLINE
 WHERE     XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID
       AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID
       AND GLREF.GL_SL_LINK_ID(+) = XLINE.GL_SL_LINK_ID
       AND GLREF.GL_SL_LINK_TABLE(+) = XLINE.GL_SL_LINK_TABLE
       AND XEVNT.ENTITY_ID = XHEAD.ENTITY_ID
       AND XEVNT.EVENT_ID = XHEAD.EVENT_ID
       AND XEVNT.APPLICATION_ID = XHEAD.APPLICATION_ID
       AND XENTY.ENTITY_ID = XEVNT.ENTITY_ID
       AND XENTY.APPLICATION_ID = XEVNT.APPLICATION_ID
       AND EVTYP.APPLICATION_ID(+) = XEVNT.APPLICATION_ID
       AND EVTYP.EVENT_TYPE_CODE(+) = XEVNT.EVENT_TYPE_CODE
       AND EVTYP.LANGUAGE(+) LIKE 'US'
       AND EVCLS.APPLICATION_ID(+) = EVTYP.APPLICATION_ID
       AND EVCLS.ENTITY_CODE(+) = EVTYP.ENTITY_CODE
       AND EVCLS.EVENT_CLASS_CODE(+) = EVTYP.EVENT_CLASS_CODE
       AND EVCLS.LANGUAGE(+) LIKE 'US'
       AND PRULE.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULE.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULE.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULE.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND PRULT.LANGUAGE(+) LIKE 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULT.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULT.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULT.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND DSEQ.DOC_SEQUENCE_ID(+) = XHEAD.DOC_SEQUENCE_ID
       AND DSEQ.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND ASEQH.SEQ_HEADER_ID(+) = ASEQV.SEQ_HEADER_ID
       AND ASEQV.SEQ_VERSION_ID(+) = XHEAD.COMPLETION_ACCT_SEQ_VERSION_ID
       AND RSEQH.SEQ_HEADER_ID(+) = RSEQV.SEQ_HEADER_ID
       AND RSEQV.SEQ_VERSION_ID(+) = XHEAD.CLOSE_ACCT_SEQ_VERSION_ID
       AND SUSES.SITE_USE_ID(+) = XLINE.PARTY_SITE_ID
       AND ASITE.CUST_ACCT_SITE_ID(+) = SUSES.CUST_ACCT_SITE_ID
       AND CUSTA.CUST_ACCOUNT_ID(+) = XLINE.PARTY_ID
       AND PRTY.PARTY_ID(+) = CUSTA.PARTY_ID
       AND PSITE.PARTY_SITE_ID(+) = ASITE.PARTY_SITE_ID
       AND SSITE.VENDOR_SITE_ID(+) = XLINE.PARTY_SITE_ID
       AND SUPP.VENDOR_ID(+) = XLINE.PARTY_ID
       AND LENTY.LEGAL_ENTITY_ID(+) = XENTY.LEGAL_ENTITY_ID
       AND APPL.APPLICATION_ID = XLINE.APPLICATION_ID
       AND APPL.LANGUAGE LIKE 'US'
       AND ACCTS.CODE_COMBINATION_ID = XLINE.CODE_COMBINATION_ID
       AND GLCAT.JE_CATEGORY_NAME = XHEAD.JE_CATEGORY_NAME
       AND GLCAT.LANGUAGE LIKE 'US'
       AND XLINE.ACCOUNTING_CLASS_CODE = Accounting_Class.LOOKUP_CODE(+)
       AND Accounting_Class.LOOKUP_TYPE(+) = 'XLA_ACCOUNTING_CLASS'
       AND Accounting_Class.LANGUAGE(+) = 'US'
       AND ACCTS.ACCOUNT_TYPE = Account_Type.LOOKUP_CODE(+)
       AND Account_Type.LOOKUP_TYPE(+) = 'ACCOUNT TYPE'
       AND Account_Type.LANGUAGE(+) = 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE =
              Accounting_Def_Owner_Type.LOOKUP_CODE(+)
       AND Accounting_Def_Owner_Type.LOOKUP_TYPE(+) = 'XLA_OWNER_TYPE'
       AND Accounting_Def_Owner_Type.LANGUAGE(+) = 'US'
       AND Accounting_Def_Owner_Type.view_application_id = 602
       AND Accounting_Class.view_application_id = 602
       and GLREF.JE_HEADER_ID = p_header
       and GLREF.JE_LINE_NUM = p_line;
--srw.message('1212','l_trans_num-'||l_trans_num);
return  l_trans_num;
exception
    when others then
    return null;
end GET_TRANS_NUM;

function GET_DESC(p_header in number, p_line in varchar2, p_source in varchar2, p_cat in varchar2) return varchar2 is
 l_trans_num varchar2(240);
 l_desc varchar2(240);
begin

if p_source = 'Receivables' and p_cat = 'Receipts' then
   select l.description
	 into l_desc
	 from gl_je_lines l
	    , gl_je_headers h
    where l.JE_HEADER_ID = p_header
      and l.JE_LINE_NUM = p_line
      and l.JE_HEADER_ID =h.JE_HEADER_ID;
    return l_desc;
elsif p_source = 'Assets' then
    select l.description
	  into l_desc
	  from gl_je_lines l
	     , gl_je_headers h
     where l.JE_HEADER_ID = p_header
       and l.JE_LINE_NUM = p_line
       and l.JE_HEADER_ID =h.JE_HEADER_ID;
    return l_desc;
elsif p_source = 'Payables' and p_cat = 'Purchase Invoices' then
    select l.description
	  into l_desc
	  from gl_je_lines l
	     , gl_je_headers h
     where l.JE_HEADER_ID = p_header
       and l.JE_LINE_NUM = p_line
       and l.JE_HEADER_ID =h.JE_HEADER_ID;
    return l_desc;
elsif p_source = 'Payables' and p_cat = 'Payments' then
    select l.description
	  into l_desc
	  from gl_je_lines l
	     , gl_je_headers h
     where l.JE_HEADER_ID = p_header
       and l.JE_LINE_NUM = p_line
       and l.JE_HEADER_ID =h.JE_HEADER_ID;
    return l_desc;
elsif p_source = 'Receivables' and p_cat = 'Sales Invoices' then
    SELECT rcl.description
      into l_desc
      FROM GL.GL_JE_CATEGORIES_TL GLCAT,
      	   GL.GL_CODE_COMBINATIONS ACCTS,
           APPLSYS.FND_APPLICATION_TL APPL,
           APPLSYS.fnd_lookup_values Accounting_Def_Owner_Type,
           APPLSYS.fnd_lookup_values Accounting_Class,
           APPLSYS.fnd_lookup_values Account_Type,
           XLE.XLE_ENTITY_PROFILES LENTY,
           AP.AP_SUPPLIER_SITES_ALL SSITE,
           AP.AP_SUPPLIERS SUPP,
           AR.HZ_CUST_ACCOUNTS CUSTA,
           AR.HZ_PARTY_SITES PSITE,
           AR.HZ_PARTIES PRTY,
           AR.HZ_CUST_SITE_USES_ALL SUSES,
           AR.HZ_CUST_ACCT_SITES_ALL ASITE,
           FUN.FUN_SEQ_VERSIONS RSEQV,
           FUN.FUN_SEQ_HEADERS RSEQH,
           FUN.FUN_SEQ_VERSIONS ASEQV,
           FUN.FUN_SEQ_HEADERS ASEQH,
           APPLSYS.FND_DOCUMENT_SEQUENCES DSEQ,
           XLA.XLA_PRODUCT_RULES_TL PRULT,
           XLA.XLA_PRODUCT_RULES_B PRULE,
           XLA.XLA_EVENT_CLASSES_TL EVCLS,
           XLA.XLA_EVENT_TYPES_TL EVTYP,
           XLA.XLA_TRANSACTION_ENTITIES XENTY,
           XLA.XLA_EVENTS XEVNT,
           GL.GL_IMPORT_REFERENCES GLREF,
           XLA.XLA_AE_HEADERS XHEAD,
           XLA.XLA_AE_LINES XLINE,
		   RA_CUSTOMER_TRX_ALL rc,
		   RA_CUSTOMER_TRX_LINES_all rcl
     WHERE XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID
       AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID
       AND GLREF.GL_SL_LINK_ID(+) = XLINE.GL_SL_LINK_ID
       AND GLREF.GL_SL_LINK_TABLE(+) = XLINE.GL_SL_LINK_TABLE
       AND XEVNT.ENTITY_ID = XHEAD.ENTITY_ID
       AND XEVNT.EVENT_ID = XHEAD.EVENT_ID
       AND XEVNT.APPLICATION_ID = XHEAD.APPLICATION_ID
       AND XENTY.ENTITY_ID = XEVNT.ENTITY_ID
       AND XENTY.APPLICATION_ID = XEVNT.APPLICATION_ID
       AND EVTYP.APPLICATION_ID(+) = XEVNT.APPLICATION_ID
       AND EVTYP.EVENT_TYPE_CODE(+) = XEVNT.EVENT_TYPE_CODE
       AND EVTYP.LANGUAGE(+) LIKE 'US'
       AND EVCLS.APPLICATION_ID(+) = EVTYP.APPLICATION_ID
       AND EVCLS.ENTITY_CODE(+) = EVTYP.ENTITY_CODE
       AND EVCLS.EVENT_CLASS_CODE(+) = EVTYP.EVENT_CLASS_CODE
       AND EVCLS.LANGUAGE(+) LIKE 'US'
       AND PRULE.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULE.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULE.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULE.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND PRULT.LANGUAGE(+) LIKE 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULT.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULT.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULT.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND DSEQ.DOC_SEQUENCE_ID(+) = XHEAD.DOC_SEQUENCE_ID
       AND DSEQ.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND ASEQH.SEQ_HEADER_ID(+) = ASEQV.SEQ_HEADER_ID
       AND ASEQV.SEQ_VERSION_ID(+) = XHEAD.COMPLETION_ACCT_SEQ_VERSION_ID
       AND RSEQH.SEQ_HEADER_ID(+) = RSEQV.SEQ_HEADER_ID
       AND RSEQV.SEQ_VERSION_ID(+) = XHEAD.CLOSE_ACCT_SEQ_VERSION_ID
       AND SUSES.SITE_USE_ID(+) = XLINE.PARTY_SITE_ID
       AND ASITE.CUST_ACCT_SITE_ID(+) = SUSES.CUST_ACCT_SITE_ID
       AND CUSTA.CUST_ACCOUNT_ID(+) = XLINE.PARTY_ID
       AND PRTY.PARTY_ID(+) = CUSTA.PARTY_ID
       AND PSITE.PARTY_SITE_ID(+) = ASITE.PARTY_SITE_ID
       AND SSITE.VENDOR_SITE_ID(+) = XLINE.PARTY_SITE_ID
       AND SUPP.VENDOR_ID(+) = XLINE.PARTY_ID
       AND LENTY.LEGAL_ENTITY_ID(+) = XENTY.LEGAL_ENTITY_ID
       AND APPL.APPLICATION_ID = XLINE.APPLICATION_ID
       AND APPL.LANGUAGE LIKE 'US'
       AND ACCTS.CODE_COMBINATION_ID = XLINE.CODE_COMBINATION_ID
       AND GLCAT.JE_CATEGORY_NAME = XHEAD.JE_CATEGORY_NAME
       AND GLCAT.LANGUAGE LIKE 'US'
       AND XLINE.ACCOUNTING_CLASS_CODE = Accounting_Class.LOOKUP_CODE(+)
       AND Accounting_Class.LOOKUP_TYPE(+) = 'XLA_ACCOUNTING_CLASS'
       AND Accounting_Class.LANGUAGE(+) = 'US'
       AND ACCTS.ACCOUNT_TYPE = Account_Type.LOOKUP_CODE(+)
       AND Account_Type.LOOKUP_TYPE(+) = 'ACCOUNT TYPE'
       AND Account_Type.LANGUAGE(+) = 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE =    Accounting_Def_Owner_Type.LOOKUP_CODE(+)
       AND Accounting_Def_Owner_Type.LOOKUP_TYPE(+) = 'XLA_OWNER_TYPE'
       AND Accounting_Def_Owner_Type.LANGUAGE(+) = 'US'
       AND Accounting_Def_Owner_Type.view_application_id = 602
       AND Accounting_Class.view_application_id = 602
       and GLREF.JE_HEADER_ID = p_header
       and GLREF.JE_LINE_NUM = p_line
       and XENTY.source_id_int_1=RC.customer_trx_id
       and rc.customer_trx_id=rcl.customer_trx_id
	   and rcl.description is not null
       and rownum=1
	   order by rcl.customer_trx_line_id;

    return l_desc;
elsif p_source = 'Receivables' and p_cat = 'Adjustment' then
  	SELECT acr.activity_name
	  into l_desc
      FROM GL.GL_JE_CATEGORIES_TL GLCAT,
		   GL.GL_CODE_COMBINATIONS ACCTS,
		   APPLSYS.FND_APPLICATION_TL APPL,
		   APPLSYS.fnd_lookup_values Accounting_Def_Owner_Type,
		   APPLSYS.fnd_lookup_values Accounting_Class,
		   APPLSYS.fnd_lookup_values Account_Type,
		   XLE.XLE_ENTITY_PROFILES LENTY,
		   AP.AP_SUPPLIER_SITES_ALL SSITE,
		   AP.AP_SUPPLIERS SUPP,
		   AR.HZ_CUST_ACCOUNTS CUSTA,
		   AR.HZ_PARTY_SITES PSITE,
		   AR.HZ_PARTIES PRTY,
		   AR.HZ_CUST_SITE_USES_ALL SUSES,
		   AR.HZ_CUST_ACCT_SITES_ALL ASITE,
		   FUN.FUN_SEQ_VERSIONS RSEQV,
		   FUN.FUN_SEQ_HEADERS RSEQH,
		   FUN.FUN_SEQ_VERSIONS ASEQV,
		   FUN.FUN_SEQ_HEADERS ASEQH,
		   APPLSYS.FND_DOCUMENT_SEQUENCES DSEQ,
		   XLA.XLA_PRODUCT_RULES_TL PRULT,
		   XLA.XLA_PRODUCT_RULES_B PRULE,
		   XLA.XLA_EVENT_CLASSES_TL EVCLS,
		   XLA.XLA_EVENT_TYPES_TL EVTYP,
		   XLA.XLA_TRANSACTION_ENTITIES XENTY,
		   XLA.XLA_EVENTS XEVNT,
		   GL.GL_IMPORT_REFERENCES GLREF,
		   XLA.XLA_AE_HEADERS XHEAD,
		   XLA.XLA_AE_LINES XLINE,ar_adjustments_v ACR
     WHERE XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID
       AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID
       AND GLREF.GL_SL_LINK_ID(+) = XLINE.GL_SL_LINK_ID
       AND GLREF.GL_SL_LINK_TABLE(+) = XLINE.GL_SL_LINK_TABLE
       AND XEVNT.ENTITY_ID = XHEAD.ENTITY_ID
       AND XEVNT.EVENT_ID = XHEAD.EVENT_ID
       AND XEVNT.APPLICATION_ID = XHEAD.APPLICATION_ID
       AND XENTY.ENTITY_ID = XEVNT.ENTITY_ID
       AND XENTY.APPLICATION_ID = XEVNT.APPLICATION_ID
       AND EVTYP.APPLICATION_ID(+) = XEVNT.APPLICATION_ID
       AND EVTYP.EVENT_TYPE_CODE(+) = XEVNT.EVENT_TYPE_CODE
       AND EVTYP.LANGUAGE(+) LIKE 'US'
       AND EVCLS.APPLICATION_ID(+) = EVTYP.APPLICATION_ID
       AND EVCLS.ENTITY_CODE(+) = EVTYP.ENTITY_CODE
       AND EVCLS.EVENT_CLASS_CODE(+) = EVTYP.EVENT_CLASS_CODE
       AND EVCLS.LANGUAGE(+) LIKE 'US'
       AND PRULE.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULE.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULE.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULE.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND PRULT.LANGUAGE(+) LIKE 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULT.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULT.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULT.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND DSEQ.DOC_SEQUENCE_ID(+) = XHEAD.DOC_SEQUENCE_ID
       AND DSEQ.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND ASEQH.SEQ_HEADER_ID(+) = ASEQV.SEQ_HEADER_ID
       AND ASEQV.SEQ_VERSION_ID(+) = XHEAD.COMPLETION_ACCT_SEQ_VERSION_ID
       AND RSEQH.SEQ_HEADER_ID(+) = RSEQV.SEQ_HEADER_ID
       AND RSEQV.SEQ_VERSION_ID(+) = XHEAD.CLOSE_ACCT_SEQ_VERSION_ID
       AND SUSES.SITE_USE_ID(+) = XLINE.PARTY_SITE_ID
       AND ASITE.CUST_ACCT_SITE_ID(+) = SUSES.CUST_ACCT_SITE_ID
       AND CUSTA.CUST_ACCOUNT_ID(+) = XLINE.PARTY_ID
       AND PRTY.PARTY_ID(+) = CUSTA.PARTY_ID
       AND PSITE.PARTY_SITE_ID(+) = ASITE.PARTY_SITE_ID
       AND SSITE.VENDOR_SITE_ID(+) = XLINE.PARTY_SITE_ID
       AND SUPP.VENDOR_ID(+) = XLINE.PARTY_ID
       AND LENTY.LEGAL_ENTITY_ID(+) = XENTY.LEGAL_ENTITY_ID
       AND APPL.APPLICATION_ID = XLINE.APPLICATION_ID
       AND APPL.LANGUAGE LIKE 'US'
       AND ACCTS.CODE_COMBINATION_ID = XLINE.CODE_COMBINATION_ID
       AND GLCAT.JE_CATEGORY_NAME = XHEAD.JE_CATEGORY_NAME
       AND GLCAT.LANGUAGE LIKE 'US'
       AND XLINE.ACCOUNTING_CLASS_CODE = Accounting_Class.LOOKUP_CODE(+)
       AND Accounting_Class.LOOKUP_TYPE(+) = 'XLA_ACCOUNTING_CLASS'
       AND Accounting_Class.LANGUAGE(+) = 'US'
       AND ACCTS.ACCOUNT_TYPE = Account_Type.LOOKUP_CODE(+)
       AND Account_Type.LOOKUP_TYPE(+) = 'ACCOUNT TYPE'
       AND Account_Type.LANGUAGE(+) = 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE =
              Accounting_Def_Owner_Type.LOOKUP_CODE(+)
       AND Accounting_Def_Owner_Type.LOOKUP_TYPE(+) = 'XLA_OWNER_TYPE'
       AND Accounting_Def_Owner_Type.LANGUAGE(+) = 'US'
       AND Accounting_Def_Owner_Type.view_application_id = 602
       AND Accounting_Class.view_application_id = 602
       and GLREF.JE_HEADER_ID = p_header
       and GLREF.JE_LINE_NUM = p_line
       and XENTY.source_id_int_1=ACR.adjustment_id;
  return l_desc;
elsif p_source = 'Receivables' and p_cat = 'Misc Receipts' then
	SELECT acr.activity
	  into l_desc
      FROM GL.GL_JE_CATEGORIES_TL GLCAT,
		   GL.GL_CODE_COMBINATIONS ACCTS,
		   APPLSYS.FND_APPLICATION_TL APPL,
		   APPLSYS.fnd_lookup_values Accounting_Def_Owner_Type,
		   APPLSYS.fnd_lookup_values Accounting_Class,
		   APPLSYS.fnd_lookup_values Account_Type,
		   XLE.XLE_ENTITY_PROFILES LENTY,
		   AP.AP_SUPPLIER_SITES_ALL SSITE,
		   AP.AP_SUPPLIERS SUPP,
		   AR.HZ_CUST_ACCOUNTS CUSTA,
		   AR.HZ_PARTY_SITES PSITE,
		   AR.HZ_PARTIES PRTY,
		   AR.HZ_CUST_SITE_USES_ALL SUSES,
		   AR.HZ_CUST_ACCT_SITES_ALL ASITE,
		   FUN.FUN_SEQ_VERSIONS RSEQV,
		   FUN.FUN_SEQ_HEADERS RSEQH,
		   FUN.FUN_SEQ_VERSIONS ASEQV,
		   FUN.FUN_SEQ_HEADERS ASEQH,
		   APPLSYS.FND_DOCUMENT_SEQUENCES DSEQ,
		   XLA.XLA_PRODUCT_RULES_TL PRULT,
		   XLA.XLA_PRODUCT_RULES_B PRULE,
		   XLA.XLA_EVENT_CLASSES_TL EVCLS,
		   XLA.XLA_EVENT_TYPES_TL EVTYP,
		   XLA.XLA_TRANSACTION_ENTITIES XENTY,
		   XLA.XLA_EVENTS XEVNT,
		   GL.GL_IMPORT_REFERENCES GLREF,
		   XLA.XLA_AE_HEADERS XHEAD,
		   XLA.XLA_AE_LINES XLINE,AR_CASH_RECEIPTS_V ACR
     WHERE XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID
       AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID
       AND GLREF.GL_SL_LINK_ID(+) = XLINE.GL_SL_LINK_ID
       AND GLREF.GL_SL_LINK_TABLE(+) = XLINE.GL_SL_LINK_TABLE
       AND XEVNT.ENTITY_ID = XHEAD.ENTITY_ID
       AND XEVNT.EVENT_ID = XHEAD.EVENT_ID
       AND XEVNT.APPLICATION_ID = XHEAD.APPLICATION_ID
       AND XENTY.ENTITY_ID = XEVNT.ENTITY_ID
       AND XENTY.APPLICATION_ID = XEVNT.APPLICATION_ID
       AND EVTYP.APPLICATION_ID(+) = XEVNT.APPLICATION_ID
       AND EVTYP.EVENT_TYPE_CODE(+) = XEVNT.EVENT_TYPE_CODE
       AND EVTYP.LANGUAGE(+) LIKE 'US'
       AND EVCLS.APPLICATION_ID(+) = EVTYP.APPLICATION_ID
       AND EVCLS.ENTITY_CODE(+) = EVTYP.ENTITY_CODE
       AND EVCLS.EVENT_CLASS_CODE(+) = EVTYP.EVENT_CLASS_CODE
       AND EVCLS.LANGUAGE(+) LIKE 'US'
       AND PRULE.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULE.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULE.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULE.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND PRULT.LANGUAGE(+) LIKE 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULT.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULT.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULT.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND DSEQ.DOC_SEQUENCE_ID(+) = XHEAD.DOC_SEQUENCE_ID
       AND DSEQ.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND ASEQH.SEQ_HEADER_ID(+) = ASEQV.SEQ_HEADER_ID
       AND ASEQV.SEQ_VERSION_ID(+) = XHEAD.COMPLETION_ACCT_SEQ_VERSION_ID
       AND RSEQH.SEQ_HEADER_ID(+) = RSEQV.SEQ_HEADER_ID
       AND RSEQV.SEQ_VERSION_ID(+) = XHEAD.CLOSE_ACCT_SEQ_VERSION_ID
       AND SUSES.SITE_USE_ID(+) = XLINE.PARTY_SITE_ID
       AND ASITE.CUST_ACCT_SITE_ID(+) = SUSES.CUST_ACCT_SITE_ID
       AND CUSTA.CUST_ACCOUNT_ID(+) = XLINE.PARTY_ID
       AND PRTY.PARTY_ID(+) = CUSTA.PARTY_ID
       AND PSITE.PARTY_SITE_ID(+) = ASITE.PARTY_SITE_ID
       AND SSITE.VENDOR_SITE_ID(+) = XLINE.PARTY_SITE_ID
       AND SUPP.VENDOR_ID(+) = XLINE.PARTY_ID
       AND LENTY.LEGAL_ENTITY_ID(+) = XENTY.LEGAL_ENTITY_ID
       AND APPL.APPLICATION_ID = XLINE.APPLICATION_ID
       AND APPL.LANGUAGE LIKE 'US'
       AND ACCTS.CODE_COMBINATION_ID = XLINE.CODE_COMBINATION_ID
       AND GLCAT.JE_CATEGORY_NAME = XHEAD.JE_CATEGORY_NAME
       AND GLCAT.LANGUAGE LIKE 'US'
       AND XLINE.ACCOUNTING_CLASS_CODE = Accounting_Class.LOOKUP_CODE(+)
       AND Accounting_Class.LOOKUP_TYPE(+) = 'XLA_ACCOUNTING_CLASS'
       AND Accounting_Class.LANGUAGE(+) = 'US'
       AND ACCTS.ACCOUNT_TYPE = Account_Type.LOOKUP_CODE(+)
       AND Account_Type.LOOKUP_TYPE(+) = 'ACCOUNT TYPE'
       AND Account_Type.LANGUAGE(+) = 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE =
              Accounting_Def_Owner_Type.LOOKUP_CODE(+)
       AND Accounting_Def_Owner_Type.LOOKUP_TYPE(+) = 'XLA_OWNER_TYPE'
       AND Accounting_Def_Owner_Type.LANGUAGE(+) = 'US'
       AND Accounting_Def_Owner_Type.view_application_id = 602
       AND Accounting_Class.view_application_id = 602
       and GLREF.JE_HEADER_ID = p_header
       and GLREF.JE_LINE_NUM = p_line
       and XENTY.source_id_int_1=ACR.cash_receipt_id;
  return l_desc;
else
SELECT (SELECT ad.description
          FROM xla_distribution_links xdl,
               AP_INVOICE_DISTRIBUTIONS_ALL ad
               --,ap_invoice_distributions_all aid
         WHERE     xdl.event_id = XEVNT.event_id
               AND xdl.application_id = xline.application_id
               AND xdl.ae_header_id = xline.ae_header_id
               AND xdl.ae_line_num = xline.ae_line_num
               AND xdl.source_distribution_type = 'AP_INV_DIST'
               AND xdl.source_distribution_id_num_1 =
                      ad.invoice_distribution_id
               AND ad.invoice_id = xdl.applied_to_source_id_num_1
               AND ROWNUM = 1
        UNION
        SELECT cc.description
          FROM xla_distribution_links xdl, CE.CE_CASHFLOWS CC
         WHERE     xdl.event_id = XEVNT.event_id
               AND xdl.application_id = xline.application_id
               AND xdl.ae_header_id = xline.ae_header_id
               AND xdl.ae_line_num = xline.ae_line_num
               AND xdl.source_distribution_type = 'CE_TRANS'
               AND cc.cashflow_id = xdl.SOURCE_DISTRIBUTION_ID_NUM_1
               AND EVCLS.ENTITY_CODE = 'CE_CASHFLOWS'
               AND ROWNUM = 1
        UNION
        SELECT aid.description
          FROM xla_distribution_links xdl,
               AP_PAYMENT_HIST_DISTS ad,
               ap_invoice_distributions_all aid
         WHERE     xdl.event_id = XEVNT.event_id
               AND xdl.application_id = xline.application_id
               AND xdl.ae_header_id = xline.ae_header_id
               AND xdl.ae_line_num = xline.ae_line_num
               AND xdl.source_distribution_type = 'AP_PMT_DIST'
               AND xdl.source_distribution_id_num_1 =
                      ad.invoice_distribution_id
               AND ad.invoice_distribution_id = ad.invoice_distribution_id
               AND ROWNUM = 1
        UNION
        SELECT ail.description
          FROM xla_distribution_links xdl,
               RA_CUST_TRX_LINE_GL_DIST_ALL ad,
               ra_customer_trx_lines_all ail
         WHERE     xdl.event_id = XEVNT.event_id
               AND xdl.application_id = xline.application_id
               AND xdl.ae_header_id = xline.ae_header_id
               AND xdl.ae_line_num = xline.ae_line_num
               AND xdl.source_distribution_type =
                      'RA_CUST_TRX_LINE_GL_DIST_ALL'
               AND xdl.source_distribution_id_num_1 =
                      ad.cust_trx_line_gl_dist_id
               AND ail.CUSTOMER_TRX_LINE_ID = ad.CUSTOMER_TRX_LINE_ID
               AND ail.org_id = ad.org_id
               AND ROWNUM = 1
        UNION
        SELECT ail.description
          FROM xla_distribution_links xdl,
               AR_DISTRIBUTIONS_ALL ad,
               ra_customer_trx_lines_all ail
         WHERE     xdl.event_id = XEVNT.event_id
               AND xdl.application_id = xline.application_id
               AND xdl.ae_header_id = xline.ae_header_id
               AND xdl.ae_line_num = xline.ae_line_num
               AND xdl.source_distribution_type = 'AR_DISTRIBUTIONS_ALL'
               AND ad.source_table = 'RA'
               AND xdl.source_distribution_id_num_1 = ad.line_id
               AND ail.CUSTOMER_TRX_LINE_ID = ad.REF_CUSTOMER_TRX_LINE_ID
               AND ail.org_id = ad.org_id
               AND ROWNUM = 1)
          INTO  l_trans_num
  FROM GL.GL_JE_CATEGORIES_TL GLCAT,
       GL.GL_CODE_COMBINATIONS ACCTS,
       APPLSYS.FND_APPLICATION_TL APPL,
       APPLSYS.fnd_lookup_values Accounting_Def_Owner_Type,
       APPLSYS.fnd_lookup_values Accounting_Class,
       APPLSYS.fnd_lookup_values Account_Type,
       XLE.XLE_ENTITY_PROFILES LENTY,
       AP.AP_SUPPLIER_SITES_ALL SSITE,
       AP.AP_SUPPLIERS SUPP,
       AR.HZ_CUST_ACCOUNTS CUSTA,
       AR.HZ_PARTY_SITES PSITE,
       AR.HZ_PARTIES PRTY,
       AR.HZ_CUST_SITE_USES_ALL SUSES,
       AR.HZ_CUST_ACCT_SITES_ALL ASITE,
       FUN.FUN_SEQ_VERSIONS RSEQV,
       FUN.FUN_SEQ_HEADERS RSEQH,
       FUN.FUN_SEQ_VERSIONS ASEQV,
       FUN.FUN_SEQ_HEADERS ASEQH,
       APPLSYS.FND_DOCUMENT_SEQUENCES DSEQ,
       XLA.XLA_PRODUCT_RULES_TL PRULT,
       XLA.XLA_PRODUCT_RULES_B PRULE,
       XLA.XLA_EVENT_CLASSES_TL EVCLS,
       XLA.XLA_EVENT_TYPES_TL EVTYP,
       XLA.XLA_TRANSACTION_ENTITIES XENTY,
       XLA.XLA_EVENTS XEVNT,
       GL.GL_IMPORT_REFERENCES GLREF,
       XLA.XLA_AE_HEADERS XHEAD,
       XLA.XLA_AE_LINES XLINE
 WHERE     XHEAD.AE_HEADER_ID = XLINE.AE_HEADER_ID
       AND XHEAD.APPLICATION_ID = XLINE.APPLICATION_ID
       AND GLREF.GL_SL_LINK_ID(+) = XLINE.GL_SL_LINK_ID
       AND GLREF.GL_SL_LINK_TABLE(+) = XLINE.GL_SL_LINK_TABLE
       AND XEVNT.ENTITY_ID = XHEAD.ENTITY_ID
       AND XEVNT.EVENT_ID = XHEAD.EVENT_ID
       AND XEVNT.APPLICATION_ID = XHEAD.APPLICATION_ID
       AND XENTY.ENTITY_ID = XEVNT.ENTITY_ID
       AND XENTY.APPLICATION_ID = XEVNT.APPLICATION_ID
       AND EVTYP.APPLICATION_ID(+) = XEVNT.APPLICATION_ID
       AND EVTYP.EVENT_TYPE_CODE(+) = XEVNT.EVENT_TYPE_CODE
       AND EVTYP.LANGUAGE(+) LIKE 'US'
       AND EVCLS.APPLICATION_ID(+) = EVTYP.APPLICATION_ID
       AND EVCLS.ENTITY_CODE(+) = EVTYP.ENTITY_CODE
       AND EVCLS.EVENT_CLASS_CODE(+) = EVTYP.EVENT_CLASS_CODE
       AND EVCLS.LANGUAGE(+) LIKE 'US'
       AND PRULE.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULE.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULE.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULE.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND PRULT.LANGUAGE(+) LIKE 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE(+) = XHEAD.PRODUCT_RULE_TYPE_CODE
       AND PRULT.PRODUCT_RULE_CODE(+) = XHEAD.PRODUCT_RULE_CODE
       AND PRULT.AMB_CONTEXT_CODE(+) = XHEAD.AMB_CONTEXT_CODE
       AND PRULT.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND DSEQ.DOC_SEQUENCE_ID(+) = XHEAD.DOC_SEQUENCE_ID
       AND DSEQ.APPLICATION_ID(+) = XHEAD.APPLICATION_ID
       AND ASEQH.SEQ_HEADER_ID(+) = ASEQV.SEQ_HEADER_ID
       AND ASEQV.SEQ_VERSION_ID(+) = XHEAD.COMPLETION_ACCT_SEQ_VERSION_ID
       AND RSEQH.SEQ_HEADER_ID(+) = RSEQV.SEQ_HEADER_ID
       AND RSEQV.SEQ_VERSION_ID(+) = XHEAD.CLOSE_ACCT_SEQ_VERSION_ID
       AND SUSES.SITE_USE_ID(+) = XLINE.PARTY_SITE_ID
       AND ASITE.CUST_ACCT_SITE_ID(+) = SUSES.CUST_ACCT_SITE_ID
       AND CUSTA.CUST_ACCOUNT_ID(+) = XLINE.PARTY_ID
       AND PRTY.PARTY_ID(+) = CUSTA.PARTY_ID
       AND PSITE.PARTY_SITE_ID(+) = ASITE.PARTY_SITE_ID
       AND SSITE.VENDOR_SITE_ID(+) = XLINE.PARTY_SITE_ID
       AND SUPP.VENDOR_ID(+) = XLINE.PARTY_ID
       AND LENTY.LEGAL_ENTITY_ID(+) = XENTY.LEGAL_ENTITY_ID
       AND APPL.APPLICATION_ID = XLINE.APPLICATION_ID
       AND APPL.LANGUAGE LIKE 'US'
       AND ACCTS.CODE_COMBINATION_ID = XLINE.CODE_COMBINATION_ID
       AND GLCAT.JE_CATEGORY_NAME = XHEAD.JE_CATEGORY_NAME
       AND GLCAT.LANGUAGE LIKE 'US'
       AND XLINE.ACCOUNTING_CLASS_CODE = Accounting_Class.LOOKUP_CODE(+)
       AND Accounting_Class.LOOKUP_TYPE(+) = 'XLA_ACCOUNTING_CLASS'
       AND Accounting_Class.LANGUAGE(+) = 'US'
       AND ACCTS.ACCOUNT_TYPE = Account_Type.LOOKUP_CODE(+)
       AND Account_Type.LOOKUP_TYPE(+) = 'ACCOUNT TYPE'
       AND Account_Type.LANGUAGE(+) = 'US'
       AND PRULT.PRODUCT_RULE_TYPE_CODE =
              Accounting_Def_Owner_Type.LOOKUP_CODE(+)
       AND Accounting_Def_Owner_Type.LOOKUP_TYPE(+) = 'XLA_OWNER_TYPE'
       AND Accounting_Def_Owner_Type.LANGUAGE(+) = 'US'
       AND Accounting_Def_Owner_Type.view_application_id = 602
       AND Accounting_Class.view_application_id = 602
	   /*and XLINE.LEDGER_ID=to_number(:P_LEDGER_ID)
	   AND   XLINE.CURRENCY_CODE = :P_LEDGER_CURRENCY
	   AND   XLINE.CURRENCY_CODE = decode(:P_CURRENCY_TYPE, 'S','STAT',:P_LEDGER_CURRENCY)*/
       and GLREF.JE_HEADER_ID = p_header
       and GLREF.JE_LINE_NUM = p_line;
	   RETURN  l_trans_num;
end if;
exception
when others then
declare
l_trans_num varchar2(240);
begin
  select description
    into l_trans_num
	from gl_je_lines
   where JE_HEADER_ID = p_header
     and JE_LINE_NUM = p_line;
  return l_trans_num;
EXCEPTION
WHEN OTHERS THEN
RETURN NULL;
end;

END GET_DESC;
END RG_R08_UTIL_PKG;
/


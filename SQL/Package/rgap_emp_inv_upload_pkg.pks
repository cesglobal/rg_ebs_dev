CREATE OR REPLACE PACKAGE APPS.rgap_emp_inv_upload_pkg
AS
FUNCTION RGAP_EMP_INV_UPLOAD
RETURN VARCHAR2;
PROCEDURE RGAP_EMP_INV_VALIDATION(P_GROUP_ID IN NUMBER,
                                  X_status   OUT VARCHAR2);
PROCEDURE RGAP_EMP_INV_INT(ERRBUF     OUT VARCHAR2,
                           RETCODE    OUT NUMBER,
						   P_GROUP_ID IN NUMBER,
                           p_batch    IN VARCHAR2,
						   p_gl_date  IN VARCHAR2);
PROCEDURE RGAP_RELEASE_INV_HOLD(ERRBUF OUT VARCHAR2,
                                RETCODE OUT NUMBER,
								P_Batch  IN VARCHAR2);
PROCEDURE RGAP_PAYABLE_IMPORT_PRG(p_group_id IN NUMBER,
                                  p_batch    IN VARCHAR2,
								  x_request_id OUT NUMBER);
FUNCTION RGAP_UPLOAD(p_Group_id                     NUMBER
                    ,p_invoice_type                 VARCHAR2
					,p_Card_num         	        VARCHAR2
					,p_Employee_Name    	        VARCHAR2
					,p_Employee_number  	        VARCHAR2
					,p_Invoice_date                 DATE
					,p_Invoice_number               VARCHAR2
					,p_Expense_amount               NUMBER
					,p_Tax_amount                   NUMBER
					,p_Description                  VARCHAR2
					,p_company                      VARCHAR2
					,p_account                      VARCHAR2
					,p_discipline                   VARCHAR2
					,p_territory                    VARCHAR2
					,p_product                      VARCHAR2
					,p_Initiative                   VARCHAR2
					,p_document_num                 VARCHAR2
					,p_Merchant_name                VARCHAR2
					,p_Tax_registration_num         VARCHAR2
					,p_employer                     VARCHAR2
					,p_Participant                  VARCHAR2
					,p_Purpose                      VARCHAR2
					,p_Venue                        VARCHAR2
					,p_text1                        VARCHAR2
					,p_text2                   		VARCHAR2
					,p_text3                   		VARCHAR2
					,p_text4                   		VARCHAR2
					,p_text5                   		VARCHAR2
					,p_text6                   		VARCHAR2
					,p_text7                   		VARCHAR2
					,p_text8                   		VARCHAR2
					,p_text9                   		VARCHAR2
					,p_text10                  		VARCHAR2
					,p_text11                  		VARCHAR2
					,p_text12                  		VARCHAR2
					,p_text13                  		VARCHAR2
					,p_text14                  		VARCHAR2
					,p_text15                  		VARCHAR2
					)
RETURN VARCHAR2;
END RGAP_EMP_INV_UPLOAD_PKG;
/


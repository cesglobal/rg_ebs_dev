CREATE OR REPLACE PACKAGE BODY APPS.rgap_webadi_invoice_load_pkg
AS
   /* -----------------------------------------------------------------------------------------------------------------
   REM    Author               : Srinivasa Ramuni
   REM    Purpose              : This Package is Used to upload data into staging table to AP invoice interface Table
   REM
   REM    Module               : RGAP
   REM    Interface Tables     :
   REM    Modification History :
   REM       Date             Name             Version Number      Revision Summary
   REM ---------------------------------------------------------------------------------------------------------------
   REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
   REM --------------------------------------------------------------------------------------------------------------*/
   -- This Function is used to upload data into staging table from WEBADI with validations.
   FUNCTION RGAP_UPLOAD_INVOICES (p_Run_id                   NUMBER,
                                  p_Source                   VARCHAR2,
                                  p_org_id                   NUMBER,
                                  p_Org_name                 VARCHAR2,
                                  p_Invoice_type             VARCHAR2,
                                  p_Vendor_name              VARCHAR2, --RGAP_VENDOR_NAME
                                  p_Vendor_number            VARCHAR2,
                                  p_Vendor_site_name         VARCHAR2,
                                  p_Invoice_num              VARCHAR2,
                                  p_Invoice_amount           NUMBER,
                                  p_Invoice_currency_code    VARCHAR2,
                                  p_Invoice_date             DATE,
                                  p_Gl_date                  DATE,
                                  p_Description              VARCHAR2,
                                  p_Payment_method_code      VARCHAR2,
                                  p_Terms_id                 VARCHAR2,
                                  p_Pay_group_lookup_code    VARCHAR2,
                                  p_Line_number              NUMBER,
                                  p_Line_Type                VARCHAR2,
                                  p_Line_amount              NUMBER,
                                  p_Gl_Account               VARCHAR2,
                                  p_Line_Description         VARCHAR2,
                                  p_text1                    VARCHAR2,
                                  p_text2                    VARCHAR2,
                                  p_text3                    VARCHAR2,
                                  p_text4                    VARCHAR2,
                                  p_text5                    VARCHAR2,
                                  p_text6                    VARCHAR2,
                                  p_text7                    VARCHAR2,
                                  p_text8                    VARCHAR2,
                                  p_text9                    VARCHAR2,
                                  p_text10                   VARCHAR2,
                                  p_text11                   VARCHAR2,
                                  p_text12                   VARCHAR2,
                                  p_text13                   VARCHAR2,
                                  p_text14                   VARCHAR2,
                                  p_text15                   VARCHAR2)
      RETURN VARCHAR2
   AS
      /* -----------------------------------------------------------------------------------------------------------------
      REM    Author               : Srinivasa Ramuni
      REM    Purpose              : This Function is Used to upload data into staging table From WEBADI with Validations.
      REM
      REM    Module               : RGAP
      REM    Interface Tables     :
      REM    Modification History :
      REM       Date             Name             Version Number      Revision Summary
      REM ---------------------------------------------------------------------------------------------------------------
      REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
      REM --------------------------------------------------------------------------------------------------------------*/
      vError_Msg        VARCHAR2 (2000);
      vOrg_ID           NUMBER;
      vInvoice_type     VARCHAR2 (240);
      vVendor_id        VARCHAR2 (240);
      vVendor_name      VARCHAR2 (240);
      vVendor_num       VARCHAR2 (240);
      vVendor_site_id   VARCHAR2 (240);
      vCurrency_code    VARCHAR2 (240);
      vPayment_code     VARCHAR2 (240);
      vTerm_id          VARCHAR2 (240);
      vPaygroup_code    VARCHAR2 (240);
      vgl_account       VARCHAR2 (100);
      vdate_invoice     VARCHAR2 (100);
      vl_invoice_num    NUMBER;
      vGl_date          NUMBER;
      vCcid             NUMBER;
      vSob_id           NUMBER;
      vSys_Date         DATE := SYSDATE;
      vCreated_by       NUMBER;
      vl_Errbuf         VARCHAR2 (100);
      vl_retcode        NUMBER;
   BEGIN
      --Validation for OU Name
      BEGIN
         SELECT organization_id
           INTO vOrg_ID
           FROM HR_OPERATING_UNITS
          WHERE 1 = 1 AND ORGANIZATION_ID = p_org_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            vOrg_ID := NULL;
            vError_Msg := 'Invalid Operating Unit';
      END;

      --Validation For Invoice TYPE
      IF p_Invoice_type IS NOT NULL
      THEN
         BEGIN
            SELECT lookup_code
              INTO vInvoice_type
              FROM fnd_lookups
             WHERE     lookup_type = 'IBY_DOCUMENT_TYPES'
                   AND lookup_code = p_Invoice_type
                   AND enabled_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               vInvoice_type := NULL;
               vError_Msg := vError_Msg || '_' || 'Invalid Invoice Type';
         END;
      ELSIF p_Invoice_type IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Eneter Invoice Type';
      END IF;

      --Validation For Vendor Name
      IF p_Vendor_name IS NOT NULL
      THEN
         BEGIN
            SELECT vendor_id, Vendor_name, segment1
              INTO vVendor_id, vVendor_name, vVendor_num
              FROM ap_suppliers
             WHERE segment1 = p_Vendor_name AND enabled_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               vVendor_id := NULL;
               vVendor_num := NULL;
               vVendor_name := NULL;
               vError_Msg := vError_Msg || '_' || 'Invalid Vendor Name';
         END;
      ELSIF p_Vendor_name IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Enter Vendor Name';
      END IF;

      --Validation For Vendor NUMBER
      /* IF p_Vendor_number IS NOT NULL
       THEN
        BEGIN
       SELECT segment1
         INTO vVendor_num
         FROM ap_suppliers
        WHERE vendor_id  = p_Vendor_number
          AND enabled_flag = 'Y';
        EXCEPTION
        WHEN OTHERS THEN
       vVendor_num    := NULL;
       vError_Msg     := vError_Msg ||'_'||'Invalid Vendor Number';
        END;
       ELSIF p_Vendor_number IS NULL
       THEN
       vError_Msg     := vError_Msg ||'_'||'Enter Vendor Number';
       END IF;*/
      --Validation For Vendor Site Name
      IF p_Vendor_site_name IS NOT NULL
      THEN
         BEGIN
            SELECT vendor_site_id
              INTO vVendor_site_id
              FROM ap_supplier_sites_all
             WHERE     vendor_site_code = p_Vendor_site_name
                   AND vendor_id = vVendor_id
                   AND org_id = vOrg_ID;
         EXCEPTION
            WHEN OTHERS
            THEN
               vVendor_site_id := NULL;
               vError_Msg := vError_Msg || '_' || 'Invalid Vendor Site Name';
         END;
      ELSIF p_Vendor_site_name IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Enter Vendor Site Name';
      END IF;

      --Vaidation For Invoice NUMBER
      IF p_Invoice_num IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Enter Invoice Number';
      END IF;

      --Vaidation For Invoice Amount
      IF (p_Invoice_type = 'STANDARD' AND p_Invoice_amount <= 0)
      THEN
         vError_Msg :=
               vError_Msg
            || '_'
            || 'Standard Invoice Amount should be greater than zero';
      ELSIF (p_Invoice_type = 'CREDIT' AND p_Invoice_amount > 0)
      THEN
         vError_Msg :=
               vError_Msg
            || '_'
            || 'Credit Memo Invoice Amount should be less than zero';
      END IF;

      IF p_Invoice_currency_code IS NULL
      THEN
         vError_Msg :=
            vError_Msg || '_' || 'Enter a Value Invoice Currency Code';
      END IF;

      --Validation For Invoice currency code
      /* IF p_Invoice_currency_code IS NOT NULL
       THEN
       BEGIN
         SELECT invoice_currency_code
              INTO vCurrency_code
              FROM ap_supplier_sites_all
             WHERE vendor_site_code = p_Vendor_site_name
         AND invoice_currency_code = p_Invoice_currency_code
         AND vendor_id             = vVendor_id
         AND org_id                = vOrg_ID
         ;
          EXCEPTION
          WHEN OTHERS THEN
            vCurrency_code     := NULL;
      vError_Msg         := vError_Msg ||'_'||'Invalid Currency Code';
       END;
       END IF; */
      --Validation For Pay GROUP
      IF p_Payment_method_code IS NOT NULL
      THEN
         BEGIN
            SELECT lookup_code
              INTO vPayment_code
              FROM fnd_lookup_values
             WHERE     lookup_type = 'PAYMENT METHOD'
                   AND lookup_code = p_Payment_method_code
                   AND enabled_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               vPayment_code := NULL;
               vError_Msg :=
                  vError_Msg || '_' || 'Invalid Payment Method code';
         END;
      END IF;

      --Validation For Payment Terms
      IF p_Terms_id IS NOT NULL
      THEN
         BEGIN
            SELECT term_id
              INTO vTerm_id
              FROM ap_terms
             WHERE term_id = p_Terms_id AND enabled_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               vTerm_id := NULL;
               vError_Msg := vError_Msg || '_' || 'Invalid Payment Terms';
         END;
      END IF;

      --Validation For Pay Group
      IF p_Pay_group_lookup_code IS NOT NULL
      THEN
         BEGIN
            SELECT lookup_code
              INTO vPaygroup_code
              FROM FND_LOOKUP_VALUES_VL
             WHERE     lookup_type = 'PAY GROUP'
                   AND lookup_code = p_Pay_group_lookup_code
                   AND enabled_flag = 'Y';
         EXCEPTION
            WHEN OTHERS
            THEN
               vPaygroup_code := NULL;
               vError_Msg := vError_Msg || '_' || 'Invalid Payment Group';
         END;
      END IF;

      --Validation For GL ACCOUNT
      /* IF p_Gl_Account IS NOT NULL
       THEN
          BEGIN
         SELECT CONCATENATED_SEGMENTS
              INTO vgl_account
              FROM gl_code_combinations_kfv
             WHERE CONCATENATED_SEGMENTS = p_Gl_Account
               AND enabled_flag = 'Y';
          EXCEPTION
          WHEN OTHERS THEN
            vPaygroup_code     := NULL;
      vError_Msg         := vError_Msg ||'_'||'Invalid GL Account Combination';
       END;*/
      IF p_Gl_Account IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Enter GL Account Combination';
      END IF;

      /*IF (p_text1 IS NULL or
          p_text2 IS NULL or
          p_text3 IS NULL or
          p_text4 IS NULL or
          p_text5 IS NULL or
          p_text6 IS NULL or
          p_text7 IS NULL or
          p_text8 IS NULL)
      THEN
      vError_Msg         := vError_Msg ||'_'||'Enter Segments one of the segment is missing';
      END IF;*/

      IF p_Line_Type IS NULL
      THEN
         vError_Msg := vError_Msg || '_' || 'Enter Invoice Line Type';
      END IF;

      IF p_text1 IS NOT NULL
      THEN
         BEGIN
            SELECT TO_DATE (lower(p_text1), 'mm/dd/yyyy') INTO vdate_invoice FROM DUAL;
         EXCEPTION
            WHEN OTHERS
            THEN
               vError_Msg :=
                  vError_Msg || '_' || 'Enter in Date format like DD-MON-YY';
               vdate_invoice := NULL;
         END;
      END IF;

      IF p_Gl_date IS NOT NULL
      THEN
         BEGIN
            SELECT COUNT (*)
              INTO vGl_date
              FROM gl_period_statuses
             WHERE p_Gl_date BETWEEN start_date AND end_date
                   AND application_id IN
                          (SELECT application_id
                             FROM fnd_application
                            WHERE application_short_name = 'SQLAP')
                   AND set_of_books_id = (SELECT set_of_books_id
                                            FROM hr_operating_units
                                           WHERE organization_id = vOrg_ID)
                   AND closing_status = 'O';
         EXCEPTION
            WHEN OTHERS
            THEN
               vGl_date := NULL;
               vError_Msg :=
                  vError_Msg || '_'
                  || 'Invalid GL DATE Please provide date with in Open periods';
         END;

         IF vGl_date = 0
         THEN
            vError_Msg := vError_Msg || '_' || 'GL Date not in Open periods';
         END IF;
      END IF;


      IF (vError_Msg IS NULL)
      THEN
         BEGIN
            INSERT INTO RGAP_WEDADI_INVOICES_STG (Run_id,
                                                  Source,
                                                  org_id,
                                                  Org_name,
                                                  Invoice_type,
                                                  Vendor_name,
                                                  Vendor_id,
                                                  Vendor_number,
                                                  Vendor_site_name,
                                                  Vendor_site_id,
                                                  Invoice_num,
                                                  Invoice_amount,
                                                  Invoice_currency_code,
                                                  Invoice_date,
                                                  Gl_date,
                                                  Description,
                                                  Payment_method_code,
                                                  Terms_id,
                                                  Pay_group_lookup_code,
                                                  Line_number,
                                                  Line_Type,
                                                  Line_amount,
                                                  Gl_Account,
                                                  Line_Description,
                                                  attribute1,
                                                  attribute2,
                                                  attribute3,
                                                  attribute4,
                                                  attribute5,
                                                  attribute6,
                                                  attribute7,
                                                  attribute8,
                                                  attribute9,
                                                  attribute10,
                                                  attribute11,
                                                  attribute12,
                                                  attribute13,
                                                  attribute14,
                                                  attribute15,
                                                  process_flag,
                                                  Creation_date,
                                                  Created_by,
                                                  Last_updated_by,
                                                  Last_update_date)
                 VALUES (p_Run_id                                     --Run_id
                                 ,
                         p_Source                                     --Source
                                 ,
                         vOrg_ID                                      --org_id
                                ,
                         p_Org_name                                 --Org_name
                                   ,
                         vInvoice_type                          --Invoice_type
                                      ,
                         vVendor_name                            --Vendor_name
                                     ,
                         vVendor_id                                --Vendor_id
                                   ,
                         vVendor_num                           --Vendor_number
                                    ,
                         p_Vendor_site_name                 --Vendor_site_name
                                           ,
                         vVendor_site_id                      --Vendor_site_id
                                        ,
                         p_Invoice_num                           --Invoice_num
                                      ,
                         p_Invoice_amount                     --Invoice_amount
                                         ,
                         UPPER (p_Invoice_currency_code) --Invoice_currency_code
                                                        ,
                         p_Invoice_date                         --Invoice_date
                                       ,
                         p_Gl_date                                   --Gl_date
                                  ,
                         p_Description                           --Description
                                      ,
                         vPayment_code                   --Payment_method_code
                                      ,
                         vTerm_id                                   --Terms_id
                                 ,
                         vPaygroup_code                --Pay_group_lookup_code
                                       ,
                         p_Line_number                           --Line_number
                                      ,
                         p_Line_Type                               --Line_Type
                                    ,
                         p_Line_amount                           --Line_amount
                                      ,
                         p_Gl_Account --(p_text1||'.'||p_text2||'.'||p_text3||'.'||p_text4||'.'||p_text5||'.'||p_text6||'.'||p_text7||'.'||p_text8)                 --Gl_Account
                                     ,
                         p_Line_Description                 --Line_Description
                                           ,
                         vdate_invoice                            --attribute1
                                      ,
                         NULL                                     --attribute2
                             ,
                         NULL                                     --attribute3
                             ,
                         NULL                                     --attribute4
                             ,
                         NULL                                     --attribute5
                             ,
                         NULL                                     --attribute6
                             ,
                         NULL                                     --attribute7
                             ,
                         NULL                                     --attribute8
                             ,
                         NULL                                     --attribute9
                             ,
                         NULL                                    --attribute10
                             ,
                         NULL                                    --attribute11
                             ,
                         NULL                                    --attribute12
                             ,
                         NULL                                    --attribute13
                             ,
                         NULL                                    --attribute14
                             ,
                         NULL                                    --attribute15
                             ,
                         'N'                                    --process_flag
                            ,
                         vSys_Date                             --Creation_date
                                  ,
                         FND_GLOBAL.USER_ID                       --Created_by
                                           ,
                         FND_GLOBAL.USER_ID                  --Last_updated_by
                                           ,
                         vSys_Date                          --Last_update_date
                                  );
         EXCEPTION
            WHEN OTHERS
            THEN
               vError_Msg :=
                  'Unexpected error while inserting records: ' || SQLERRM;
         END;

         IF vError_Msg IS NULL
         THEN
            RETURN NULL;
         ELSE
            RETURN vError_Msg;
         END IF;
      ELSE
         IF INSTR (vError_Msg, '_') = 1
         THEN
            vError_Msg := SUBSTR (vError_Msg, 2);
         END IF;

         RETURN vError_Msg;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         vError_Msg :=
               vError_Msg
            || '_'
            || 'Unexpected error while loading AP Invoices : '
            || SQLERRM;

         IF INSTR (vError_Msg, '_') = 1
         THEN
            vError_Msg := SUBSTR (vError_Msg, 2);
         END IF;

         RETURN vError_Msg;
   END RGAP_UPLOAD_INVOICES;

   --This Procedure is used to populate data into AP Invoice interface table.
   PROCEDURE MAIN (pBatch_ID IN NUMBER)
   IS
      /* -----------------------------------------------------------------------------------------------------------------
      REM    Author               : Srinivasa Ramuni
      REM    Purpose              : This Procedure is Used to upload data into AP Invoice Interface table From Staging.
      REM
      REM    Module               : RGAP
      REM    Interface Tables     :
      REM    Modification History :
      REM       Date             Name             Version Number      Revision Summary
      REM ---------------------------------------------------------------------------------------------------------------
      REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
      REM --------------------------------------------------------------------------------------------------------------*/
      CURSOR cur_main (p_run_id NUMBER)
      IS
           SELECT rwis.vendor_id,
                  rwis.vendor_site_id,
                  rwis.org_id,
                  rwis.invoice_num
             FROM RGAP_WEDADI_INVOICES_STG rwis
            WHERE run_id = p_run_id AND process_flag = 'N'
         GROUP BY vendor_id,
                  vendor_site_id,
                  org_id,
                  invoice_num
         ORDER BY invoice_num;

      CURSOR cur_header (
         p_vendor_id         NUMBER,
         p_vendor_site_id    NUMBER,
         p_org_id            NUMBER,
         p_run_id            NUMBER,
         p_invoice_num       VARCHAR2)
      IS
           SELECT Source,
                  org_id,
                  invoice_type,
                  vendor_id,
                  Vendor_number,
                  vendor_site_id,
                  invoice_num,
                  invoice_amount,
                  invoice_currency_code,
                  invoice_date,
                  gl_date,
                  description,
                  payment_method_code,
                  terms_id,
                  pay_group_lookup_code,
                  attribute1
             FROM RGAP_WEDADI_INVOICES_STG
            WHERE     run_id = p_run_id
                  AND process_flag = 'N'
                  AND vendor_id = p_vendor_id
                  AND vendor_site_id = p_vendor_site_id
                  AND org_id = p_org_id
                  AND invoice_num = p_invoice_num
         GROUP BY Source,
                  org_id,
                  invoice_type,
                  vendor_id,
                  Vendor_number,
                  vendor_site_id,
                  invoice_num,
                  invoice_amount,
                  invoice_currency_code,
                  invoice_date,
                  gl_date,
                  description,
                  payment_method_code,
                  terms_id,
                  pay_group_lookup_code,
                  attribute1;

      CURSOR cur_lines (
         p_vendor_id         NUMBER,
         p_vendor_site_id    NUMBER,
         p_org_id            NUMBER,
         p_run_id            NUMBER,
         p_invoice_num       VARCHAR2)
      IS
           SELECT line_number,
                  line_type,
                  line_amount,
                  gl_account,
                  line_description
             FROM RGAP_WEDADI_INVOICES_STG
            WHERE     run_id = p_run_id
                  AND process_flag = 'N'
                  AND vendor_id = p_vendor_id
                  AND vendor_site_id = p_vendor_site_id
                  AND org_id = p_org_id
                  AND invoice_num = p_invoice_num
         ORDER BY line_number;

      vInvoice_id        NUMBER;
      vInvoice_line_id   NUMBER;
      vUser_id           NUMBER := FND_GLOBAL.USER_ID;
      vl_error_msg       LONG := NULL;
      vl_batch_id        NUMBER;
      vl_run_id          NUMBER;
   BEGIN
      IF pBatch_ID IS NULL
      THEN
         BEGIN
            SELECT MAX (RUN_ID)
              INTO vl_batch_id
              FROM RGAP_WEDADI_INVOICES_STG
             WHERE process_flag = 'N';
         EXCEPTION
            WHEN OTHERS
            THEN
               vl_batch_id := NULL;
         END;

         vl_run_id := vl_batch_id;
      ELSE
         vl_run_id := pBatch_ID;
      END IF;



      FOR rec_main IN cur_main (vl_run_id)
      LOOP                                                  -- Main loop Start
         FOR rec_header IN cur_header (rec_main.vendor_id,
                                       rec_main.vendor_site_id,
                                       rec_main.org_id,
                                       vl_run_id,
                                       rec_main.invoice_num)
         LOOP                                            -- headers loop Start
            vl_error_msg := NULL;
            vInvoice_id := AP_INVOICES_INTERFACE_S.NEXTVAL;

            BEGIN
               INSERT INTO AP_INVOICES_INTERFACE (INVOICE_ID,
                                                  INVOICE_NUM,
                                                  INVOICE_TYPE_LOOKUP_CODE,
                                                  VENDOR_ID,
                                                  VENDOR_NUM,
                                                  VENDOR_SITE_ID,
                                                  INVOICE_AMOUNT,
                                                  INVOICE_CURRENCY_CODE,
                                                  INVOICE_DATE,
                                                  INVOICE_RECEIVED_DATE,
                                                  TERMS_ID,
                                                  DESCRIPTION,
                                                  SOURCE,
                                                  PAY_GROUP_LOOKUP_CODE,
                                                  GL_DATE,
                                                  ORG_ID,
                                                  PAYMENT_METHOD_CODE,
                                                  EXCHANGE_RATE_TYPE,
                                                  EXCHANGE_DATE,
                                                  CREATION_DATE,
                                                  CREATED_BY,
                                                  LAST_UPDATE_DATE,
                                                  LAST_UPDATED_BY)
                    VALUES (vInvoice_id,
                            rec_header.invoice_num,
                            rec_header.invoice_type,
                            rec_header.vendor_id,
                            rec_header.Vendor_number,
                            rec_header.vendor_site_id,
                            rec_header.invoice_amount,
                            rec_header.invoice_currency_code,
                            rec_header.invoice_date,
                            rec_header.attribute1,
                            rec_header.terms_id,
                            rec_header.description,
                            rec_header.Source,
                            rec_header.pay_group_lookup_code,
                            rec_header.gl_date,
                            rec_header.org_id,
                            rec_header.payment_method_code,
                            'Corporate',
                            rec_header.invoice_date,
                            SYSDATE,
                            vUser_id,
                            SYSDATE,
                            vUser_id);

               COMMIT;
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.
                   put_line (
                     'Error while inserting into interface header table:'
                     || SQLERRM);
                  vl_error_msg :=
                        vl_error_msg
                     || '-'
                     || 'Error while inserting into Ap Interface table';
                  FND_FILE.
                   PUT_LINE (
                     FND_FILE.LOG,
                     'Unexpected error occurred while Inseting AP Invoices into Interface Table: '
                     || SQLERRM);
            END;

            FOR rec_lines IN cur_lines (rec_header.vendor_id,
                                        rec_header.vendor_site_id,
                                        rec_header.org_id,
                                        vl_run_id,
                                        rec_header.invoice_num)
            LOOP                                           -- Lines loop START
               vInvoice_line_id := AP_INVOICE_LINES_INTERFACE_S.NEXTVAL;

               BEGIN
                  INSERT
                    INTO AP_INVOICE_LINES_INTERFACE (INVOICE_ID,
                                                     INVOICE_LINE_ID,
                                                     Line_Number,
                                                     Line_Type_lookup_code,
                                                     Amount,
                                                     Description,
                                                     DIST_CODE_CONCATENATED,
                                                     ORG_ID,
                                                     CREATION_DATE,
                                                     CREATED_BY,
                                                     LAST_UPDATE_DATE,
                                                     LAST_UPDATED_BY)
                  VALUES (vInvoice_id,
                          vInvoice_line_id,
                          rec_lines.line_number,
                          rec_lines.line_type,
                          rec_lines.line_amount,
                          rec_lines.line_description,
                          rec_lines.gl_account,
                          rec_main.org_id,
                          SYSDATE,
                          vUser_id,
                          SYSDATE,
                          vUser_id);

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.
                      put_line (
                        'Error while inserting into interface Lines table:'
                        || SQLERRM);
                     vl_error_msg :=
                        vl_error_msg || '-'
                        || 'Error while inserting into Ap invoice interface lines';
                     FND_FILE.
                      PUT_LINE (
                        FND_FILE.LOG,
                        'Unexpected error occurred while Inseting AP Invoices into Interface Table: '
                        || SQLERRM);
               END;

               IF vl_error_msg IS NULL
               THEN
                  UPDATE RGAP_WEDADI_INVOICES_STG
                     SET process_flag = 'S'
                   WHERE     RUN_ID = vl_run_id
                         AND vendor_id = rec_main.vendor_id
                         AND vendor_site_id = rec_main.vendor_site_id
                         AND org_id = rec_main.org_id
                         AND invoice_num = rec_main.invoice_num
                         AND line_number = rec_lines.line_number
                         AND process_flag = 'N';

                  COMMIT;
               ELSE
                  UPDATE RGAP_WEDADI_INVOICES_STG
                     SET process_flag = 'E'
                   WHERE     RUN_ID = vl_run_id
                         AND vendor_id = rec_main.vendor_id
                         AND vendor_site_id = rec_main.vendor_site_id
                         AND org_id = rec_main.org_id
                         AND line_number = rec_lines.line_number
                         AND invoice_num = rec_main.invoice_num
                         AND process_flag = 'N';

                  COMMIT;
               END IF;
            END LOOP;                                            -- Lines loop
         END LOOP;                                              -- Header loop
      /*IF vl_error_msg IS NULL
      THEN
       UPDATE RGAP_WEDADI_INVOICES_STG
          SET process_flag = 'S'
        WHERE RUN_ID       = vl_run_id
          AND vendor_id    = rec_main.vendor_id
          AND vendor_site_id = rec_main.vendor_site_id
          AND org_id         = rec_main.org_id
          AND process_flag   = 'N';
       COMMIT;
      ELSE
       UPDATE RGAP_WEDADI_INVOICES_STG
          SET process_flag = 'E'
        WHERE RUN_ID       = vl_run_id
          AND vendor_id    = rec_main.vendor_id
          AND vendor_site_id = rec_main.vendor_site_id
          AND org_id         = rec_main.org_id
          AND process_flag   = 'N';
       COMMIT;
      END IF;*/

      END LOOP;                                                   -- Main loop
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Unexpected Error:' || SQLERRM);
         FND_FILE.
          PUT_LINE (
            FND_FILE.LOG,
            'Unexpected error occurred while creating AP Invoices: '
            || SQLERRM);
   --RetCode := 2;
   END MAIN;
END RGAP_WEBADI_INVOICE_LOAD_PKG;
/


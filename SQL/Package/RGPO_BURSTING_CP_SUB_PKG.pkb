CREATE OR REPLACE PACKAGE BODY APPS.rgpo_bursting_cp_sub_pkg
AS
   FUNCTION BeforeReport
      RETURN BOOLEAN
   IS
      l_request_id   NUMBER := 0;
      l_cp_req_id    NUMBER := 0;
   BEGIN
      --SRW.USER_EXIT('FND SRWINIT');
      RETURN (TRUE);
   END;
   FUNCTION AfterReport (P_OU VARCHAR2)
      RETURN BOOLEAN
   IS
      l_request_id   NUMBER := 0;
      l_cp_req_id    NUMBER := 0;
      l_rec_cnt      NUMBER := 0;
   BEGIN
      l_cp_req_id := fnd_global.conc_request_id;
      l_rec_cnt := rgpo_rep_dt_cnt (P_OU);
      IF l_rec_cnt > 0
      THEN
         l_request_id :=
            FND_REQUEST.SUBMIT_REQUEST ('XDO',
                                        'XDOBURSTREP',
                                        '',
                                        '',
                                        FALSE,
                                        'Y',
                                        l_cp_req_id,
                                        'Y',
                                        CHR (0),
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '',
                                        '');
      --SRW.USER_EXIT('FND SRWEXIT');
      END IF;
      RETURN (TRUE);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN FALSE;
   END;
   FUNCTION rgpo_rep_dt_cnt (P_OU VARCHAR2)
      RETURN NUMBER
   IS
      l_rec_cnt   NUMBER := 0;
   BEGIN
      BEGIN
         SELECT COUNT (0)
           INTO l_rec_cnt
           FROM (  SELECT COUNT (0)
                     FROM PO_HEADERS_ALL PHA,
                          PO_LINES_ALL PLA,
                          PO_LINE_LOCATIONS_ALL PLLA,
                          PO_REQ_DISTRIBUTIONS_ALL PRDA,
                          PO_DISTRIBUTIONS_ALL PDA,
                          PO_REQUISITION_HEADERS_ALL PRHA,
                          PO_REQUISITION_LINES_ALL PRLA,
                          PER_ALL_PEOPLE_F PAPF,
                          fnd_user fu,
                          hr_operating_units hr,
                          fnd_lookup_values flv
                    WHERE     PHA.PO_HEADER_ID = PLA.PO_HEADER_ID
                          AND PLA.PO_LINE_ID = PLLA.PO_LINE_ID
                          AND PLLA.LINE_LOCATION_ID = PDA.LINE_LOCATION_ID
                          AND prha.created_by = fu.user_id
                          AND PAPF.PERSON_ID = fu.employee_id
                          AND PRHA.REQUISITION_HEADER_ID =
                                 PRLA.REQUISITION_HEADER_ID
                          AND PRLA.REQUISITION_LINE_ID =
                                 PRDA.REQUISITION_LINE_ID
                          AND PRDA.DISTRIBUTION_ID = PDA.REQ_DISTRIBUTION_ID
                          AND SYSDATE BETWEEN papf.effective_start_date
                                          AND papf.effective_end_date
                          AND pha.org_id = hr.organization_id
                          AND NOT EXISTS
                                     (SELECT DISTINCT rsh.receipt_num
                                        FROM rcv_shipment_headers rsh,
                                             rcv_shipment_lines rsl,
                                             rcv_transactions rt
                                       WHERE rsh.shipment_header_id =
                                                rsl.shipment_header_id
                                             AND rsh.shipment_header_id =
                                                    rt.shipment_header_id
                                             AND rsl.shipment_line_id =
                                                    rt.shipment_line_id
                                             AND rt.po_header_id =
                                                    pha.po_header_id
                                             AND rt.po_line_id = pla.po_line_id)
                          AND TRUNC (SYSDATE) = TRUNC (PLLA.NEED_BY_DATE) - 3
                          AND hr.NAME = flv.lookup_code
                          AND flv.lookup_type = NVL (p_ou, flv.lookup_type)
                          AND NVL (flv.end_date_active, SYSDATE + 1) > SYSDATE
                          AND NVL (flv.enabled_flag, 'N') = 'Y'
                 GROUP BY PHA.SEGMENT1,
                          PRHA.SEGMENT1,
                          PLLA.NEED_BY_DATE,
                          PAPF.FULL_NAME,
                          PAPF.EMAIL_ADDRESS,
                          hr.name);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rec_cnt := 0;
      END;
      RETURN l_rec_cnt;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;
   FUNCTION rgpo_need_lines (p_po_num VARCHAR2)
      RETURN VARCHAR2
   IS
      l_line_str   VARCHAR2 (25) := NULL;
      CURSOR c1
      IS
           SELECT pla.line_num
             FROM PO_HEADERS_ALL pha,
                  PO_LINES_ALL pla,
                  PO_LINE_LOCATIONS_ALL plla
            WHERE     pha.PO_HEADER_ID = pla.PO_HEADER_ID
                  AND PLA.PO_LINE_ID = PLLA.PO_LINE_ID
                  AND pha.segment1 = p_po_num
                  AND TRUNC (SYSDATE) = TRUNC (PLLA.NEED_BY_DATE) - 3
         ORDER BY pla.line_num;
   BEGIN
      FOR i IN c1
      LOOP
         BEGIN
            IF l_line_str IS NULL
            THEN
               l_line_str := i.line_num;
            ELSE
               l_line_str := l_line_str || ',' || i.line_num;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               Fnd_File.
                put_line (Fnd_File.LOG, 'line number is return error');
         END;
      END LOOP;
      RETURN (l_line_str);
   END;
END;
/


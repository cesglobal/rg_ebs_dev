CREATE OR REPLACE PACKAGE BODY APPS.RGCOMN_DEBUG_PKG AS
-----------------------------------------------------------------------------------------------------------------------------
-- This is a Common Debug Package for all custom PL/SQL program created as  part of
-- RIOT ERP Implementation project. Any related Debug  programs that are generic
-- in nature should reside in this package.
-- --------------------------------------------------------------------------------------------------------------------------
--
-- MODIFICATION HISTORY
-- ------------------------------------
-- Date         SIR         Person              Comments
-- -------        -------       -------------          ------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------------

l_debug_level NUMBER := 0;


PROCEDURE LOG_MESSAGE(P_LEVEL IN NUMBER,
                                                          P_PROGRAM_NAME IN VARCHAR2,
                                                          P_MESSAGE IN VARCHAR2) IS
----------------------------------------------------------------------------------------------------------
-- This is a Procedure that based on the Current Debug Level and the level of
-- the message will write to the custom debug table. The message will be
-- logged if the Message Level is <= the debug level
---------------------------------------------------------------------------------------------------------

PRAGMA AUTONOMOUS_TRANSACTION;

    BEGIN

    l_debug_level := nvl(FND_PROFILE.VALUE('RG_COMN_DEBUG_LEVEL'),0);

        IF p_level <= l_debug_level THEN

           INSERT INTO RGAP.RGCOMN_DEBUG_LOG
                                    (SEQUENCE,
                                     CREATED_BY,
                                     DEBUG_DATE,
                                     LOGIN_ID,
                                     DEBUG_LEVEL,
                                     PROGRAM_NAME,
                                     MESSAGE)
                      VALUES(RGAP.RGCOMN_DLOG_SEQ.nextval,
                                      FND_GLOBAL.user_id,
                                      SYSDATE,
                                      FND_GLOBAL.login_id,
                                      p_level,
                                      SUBSTR(p_program_name,1,50),
                                      p_message);

           COMMIT;

        END IF;

    END LOG_MESSAGE;


PROCEDURE LOG_CP_MESSAGE(P_LEVEL IN NUMBER, P_MESSAGE IN VARCHAR2) IS
--------------------------------------------------------------------------------------------------------
-- This is a Procedure that based on the Current Debug Level and the level
-- of the message will write to the Concurrent request log. The message
-- will be logged if the Message Level is <= the debug level
--------------------------------------------------------------------------------------------------------

    BEGIN

    l_debug_level := nvl(FND_PROFILE.VALUE('RG_COMN_DEBUG_LEVEL'),0);

        IF p_level <= l_debug_level THEN

           FND_FILE.PUT_LINE(FND_FILE.LOG,'DEBUG:' || p_level || ' Time: ' || to_char(sysdate,'HH24:MI:SS') || ': ' || p_message);

        END IF;


    END LOG_CP_MESSAGE;


END RGCOMN_DEBUG_PKG;
/


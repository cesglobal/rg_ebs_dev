CREATE OR REPLACE PACKAGE APPS.rgap_concurr_invintf_pkg  AUTHID DEFINER AS
   /* ------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Author               : Srinivasa
   --    Purpose             : This Package is having Main Procedure for Validating the Data from Staging Table
   --
   --    Module               : RGAP
   --    Interface Tables     :
   --    Modification History :
   -- ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    Date                      Name             Version Number      Issue No             Revision             Summary
   -- -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    14-Aug-2015    Srinivasa           1.0
   --    16-Feb-2016    Srinivasa            1.1                            91 and 92                               Changed Procedure call for sendemail.
   -- --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    12-May-2016  Prabhat                 1.2                                                                                Re-arrange validation logic to display error message
   --                                                                                                                                                in proper order
   -- ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    12-May-2016  Prabhat                 1.2                                                                                1. Introduce oracle std sequence use to avoid sequence number for
   --                                                                                                                                                    invoice_id and invoice_line_id coming from ODS. This should also
   --                                                                                                                                                    be in line with coupa code
   -- ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
   --    16-May-2016 Prabhat               1.3                                                                              1. Add logic to submit payable open interface by batch
   --                                                                                                                                              2. Change site pick logic to exclude deactivated sites
   --                                                                                                                                              3. introduce rollbak in interface table insert procedure to preven header insert
   --                                                                                                                                                   without line or partial line insert.
   --                                                                                                                                              4. put logic to delete orphan stage line records.
   --                                                                                                                                              5. send completion email when there are no errors to notify
   --    30-Jun-2016 Srinivasa             1.4                                                                              Increased variable g_Interface_Email length to 100 from 40 
   --                                                                                                                                  
   -- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
   -- Global Variables
   g_user_id                    fnd_user.user_id%TYPE;
   g_resp_id                    NUMBER;
   g_resp_appl_id               NUMBER;
   g_invrecords_processed       NUMBER := 0;
   g_invrecords_rejected        NUMBER := 0;
   g_invlinerecords_processed   NUMBER := 0;
   g_invlinerecords_rejected    NUMBER := 0;
   g_mailhost                   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   g_port                       NUMBER := fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT');
   g_sender            VARCHAR2 (100) := 'donotreply@riotgames.com';
   g_Interface_Email            VARCHAR2 (100) := fnd_profile.VALUE ('RGAP_CONCUR_EMAIL');
   g_log_level                  NUMBER := 0;
   g_user_name                  VARCHAR2 (40) := fnd_profile.VALUE ('RGAP_IRELAND_USER');
   g_resp_key                   VARCHAR2 (40)  := fnd_profile.VALUE ('RGAP_IRELAND_RESP_KEY');
   g_org_id                     Number:=FND_PROFILE.VALUE ('ORG_ID');
   g_stg_subject varchar2(150) := 'Concur to Oracle EBS Invoice Interface (Stage) Error/Exception Report';
   g_intf_subject varchar2(150) := 'Concur to Oracle EBS Invoice Interface (Intf) Error/Exception Report';
   g_orcl_msg2  varchar2(150) := 'Following table lists error/exception records in interface table(s).';
   g_orcl_msg3  varchar2(350) := 'These records were rejected during open interface run. Please correct them in interface table within Oracle EBS and re-run Payable Open Interface or delete exception/error record(s) from interface table and retransmit from Concur.';
   g_concur_msg2 varchar2(150) := 'Following table lists error/exception records in interface stage table(s).';
   g_concur_msg3  varchar2(250) := 'These records were rejected during pre-validation process. Please correct them in Concur and re-transmit.';
   g_stg_nd_subject varchar2(150) := 'Concur to Oracle EBS Invoice Interface completion Report';
   g_nd_msg1 varchar2(150) := 'Concur to Oracle EBS Invoice Interface Completed Successfully';
   g_nd_msg2 varchar2(150) := 'No new records were found in stage table for processing. Next run of this interface will be in an hour.';
   g_nd_msg3 varchar2(150) := 'No error/exception were generated in this run of interface.';


   PROCEDURE rgap_invoice_val_prc(p_request_id in number);

   PROCEDURE rgap_concinv_main_prc (errbuf    OUT VARCHAR2,
                                    retcode   OUT VARCHAR2);
   PROCEDURE rgap_concinv_error_prc (errbuf    OUT VARCHAR2,
                                    retcode   OUT VARCHAR2);
END rgap_concurr_invintf_pkg;
/


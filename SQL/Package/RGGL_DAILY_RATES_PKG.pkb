CREATE OR REPLACE PACKAGE BODY APPS.rggl_daily_rates_pkg
AS
   ------------------------------------------------------------------------
   -- Name...: RGGL_DAILY_RATES_PKG.pkb
   -- Desc...: This package is used to create daily rates in GL per business logic
   -- by inserting data into gl interface table and then calling rate import program
   --
   --
   -- History:
   --
   -- Date Name Version Number IssueNo Revision Summary
   -- ------------ -------------- --------------- --------- ------------------
   -- 15-Feb-2016 Mahipal Ardam 1.0 Created
   --
   -- ADDITIONAL NOTES
   -- ================
   -- 20-APR-2016
   -- Enhancements done
   -- 1. Delink Period avergare calculation from daily rate calculation
   -- 2. Introduce tolerance in the code to abort process if there are more data
   -- 3. Report at the end of the process
   --
   -- 4. Prabhat - 6/28 July Bucket
   -- changes to send no data email  rather than sending limit mail when there was no data to process
   -- 5. Srini A - 6/28 July Bucket
   -- Chnages done to the shell script not to send report when there is no data to process
   -----------------------------------------------------------------------------------------------------------------------


   -- function to derive first day of month for a given date
   FUNCTION RGGL_first_day (p_date IN DATE)
      RETURN DATE
   IS
      l_date   DATE;
   BEGIN
      SELECT   TO_DATE (p_date, 'DD-MON-RRRR')
               - (TO_NUMBER (TO_CHAR (TO_DATE (p_date, 'DD-MON-RRRR'), 'DD'))
                  - 1)
        INTO   l_date
        FROM   DUAL;

      RETURN l_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_date := NULL;
         RETURN l_date;
   END RGGL_first_day;


   -- function to derive last day of month for a given date
   FUNCTION RGGL_LAST_DAY (p_date IN DATE)
      RETURN DATE
   IS
      l_date   DATE;
   BEGIN
      SELECT   LAST_DAY (p_date) INTO l_date FROM DUAL;

      RETURN l_date;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_date := NULL;
         RETURN l_date;
   END RGGL_LAST_DAY;


   -- function to derive number of days in a month for a given date
   FUNCTION RGGL_NO_OF_DAYS (p_date IN DATE)
      RETURN NUMBER
   IS
      l_number   NUMBER;
   BEGIN
      SELECT   (TO_DATE (RGGL_LAST_DAY (p_date))
                - TO_DATE (RGGL_first_day (p_date)))
               + 1
        INTO   l_number
        FROM   DUAL;

      RETURN l_number;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_number := 0;
         RETURN l_number;
   END RGGL_NO_OF_DAYS;



   PROCEDURE rggl_archive_records
   IS
      l_module_name   VARCHAR2 (50) := 'rggl_archive_records';
      l_arch_cnt      NUMBER := 0;
   BEGIN
      Fnd_File.put_line (Fnd_File.LOG,
                         '---------------------------------------------');
      Fnd_File.put_line (
         Fnd_File.LOG,
         'Starting moving previousley processed records to archive table process'
      );
      Fnd_File.put_line (Fnd_File.LOG, 'Inside rggl_archive_records');

      SELECT   COUNT ( * )
        INTO   l_arch_cnt
        FROM   rggl_daily_rate_stg
       WHERE   rec_status IN ('S', 'E');

      Fnd_File.put_line (Fnd_File.LOG,
                         'Total records to be archived: ' || l_arch_cnt);

      INSERT INTO rggl_daily_rate_hist
         SELECT   *
           FROM   rggl_daily_rate_stg
          WHERE   rec_status IN ('S', 'E');

      DELETE   rggl_daily_rate_stg
       WHERE   rec_status IN ('S', 'E');

      COMMIT;
   EXCEPTION
      WHEN OTHERS
      THEN
         rgcomn_debug_pkg.log_message (
            g_log_level,
            l_module_name,
            'Error executing archive code-' || SQLCODE
         );

         rgcomn_debug_pkg.log_message (
            g_log_level,
            l_module_name,
            'Error executing archive code-' || SQLERRM
         );
   END rggl_archive_records;



   -- This procedure validates incoming records in stage table
   -- before records are processed and inserted in interface table
   PROCEDURE VALIDATE_STG
   IS
      CURSOR cur
      IS
         SELECT   drs.ROWID AS row_id, drs.*
           FROM   RGGl_daily_rate_stg drs
          WHERE   drs.rec_status = 'N';

      /*
        AND EXISTS ( SELECT '1'
                       FROM fnd_currencies
                      WHERE currency_code = from_currency
                        AND enabled_flag  = 'Y')
        AND EXISTS ( SELECT '1'
                       FROM fnd_currencies
                      WHERE currency_code = to_currency
                        AND enabled_flag  = 'Y');
     */

      l_message           VARCHAR2 (4000);
      l_from_currency     RGGL_daily_rate_stg.from_currency%TYPE;
      l_to_currency       RGGL_daily_rate_stg.to_currency%TYPE;
      l_count             NUMBER;
      l_conversion_date   RGGL_daily_rate_stg.conversion_date%TYPE;
   BEGIN
      FOR rec IN CUR
      LOOP
         l_message := NULL;

         BEGIN
            SELECT   currency_code
              INTO   l_from_currency
              FROM   fnd_currencies
             WHERE   enabled_flag = 'Y' AND currency_code = rec.from_currency;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'From currency defined:' || rec.from_currency);
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line (
                  'From currency not defined:' || rec.from_currency
               );
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'From currency not defined:' || rec.from_currency
               );
               l_message :=
                     l_message
                  || ','
                  || 'From Currency not defined:'
                  || rec.from_currency;
         END;

         BEGIN
            SELECT   currency_code
              INTO   l_to_currency
              FROM   fnd_currencies
             WHERE   enabled_flag = 'Y' AND currency_code = rec.to_currency;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'To currency defined:' || rec.to_currency);
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line (
                  'To currency not defined:' || rec.to_currency
               );
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'To currency not defined:' || rec.to_currency
               );
               l_message :=
                     l_message
                  || ','
                  || 'To Currency not defined:'
                  || rec.to_currency;
         END;

         l_conversion_date := TO_DATE (rec.conversion_date, 'DD-MON-RR') + 1;

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'l_conversion_date:' || l_conversion_date);

         BEGIN
            SELECT   COUNT (1)
              INTO   l_count
              FROM   gl_daily_rates
             WHERE       from_currency = rec.from_currency
                     AND to_currency = rec.to_currency
                     AND conversion_date = l_conversion_date
                     AND conversion_type = rec.conversion_type;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_count := 0;
         END;

         IF l_count <> 0
         THEN
            l_message :=
                  l_message
               || ','
               || 'Rate already created in system for :'
               || rec.from_currency
               || '  To: '
               || rec.to_currency
               || ' for Conv Type: '
               || rec.conversion_type
               || ' and Conv Date: '
               || l_conversion_date;
         END IF;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Message:' || l_message);

         IF l_message IS NULL
         THEN
            UPDATE   RGGl_daily_rate_stg
               SET   rec_status = 'V', rec_message = NULL
             --  ,rec_status  = 'V'
             WHERE       from_currency = rec.from_currency
                     AND to_currency = rec.to_currency
                     AND ROWID = rec.row_id
                     AND rec_status = 'N';

            COMMIT;
         ELSIF l_message IS NOT NULL
         THEN
            UPDATE   RGGl_daily_rate_stg
               SET   rec_status = 'E',
                     rec_message = SUBSTR (l_message, 1, 4000)
             --  ,rec_status  = 'E'
             WHERE       from_currency = rec.from_currency
                     AND to_currency = rec.to_currency
                     AND ROWID = rec.row_id
                     AND rec_status = 'N';

            COMMIT;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error in Validate Procedure:' || SQLERRM);
         DBMS_OUTPUT.put_line ('Error in Validate Procedure:' || SQLERRM);
   END VALIDATE_STG;



   -- This procedure processes validated stage table records
   -- and inserts them in GL interface table to create Corporate, Period Average and Period End Rate
   PROCEDURE PROCESS_STG
   IS
      CURSOR cur
      IS
         SELECT   drs.ROWID AS row_id, drs.*
           FROM   RGGl_daily_rate_stg drs
          WHERE   drs.rec_status = 'V';

      l_last_day              DATE;
      l_last_day_Month        DATE;
      l_last_day_Month1       DATE;
      i                       NUMBER := 0;
      l_NO_DAYS               NUMBER := 0;
      l_NO_DAYS_MONTH         NUMBER := 0;
      l_NO_DAYS_MONTH1        NUMBER := 0;
      l_weekday               VARCHAR2 (30);
      l_weekend               VARCHAR2 (30);
      l_message               VARCHAR2 (4000);
      l_success_cnt           NUMBER := 0;
      l_periodaverage_rate    NUMBER;
      l_periodaverage_rate1   NUMBER;
      l_periodaverage_rate2   NUMBER;
      l_periodaverage_rate3   NUMBER;
      l_periodaverage_rate4   NUMBER;
      l_periodaverage_rate5   NUMBER;
      l_conversion_date       RGGL_daily_rate_stg.conversion_date%TYPE;
      l_conversion_date1      RGGL_daily_rate_stg.conversion_date%TYPE;
   BEGIN
      FOR rec IN cur
      LOOP
         --SELECT LAST_DAY(SYSDATE) INTO l_last_day FROM DUAL;

         SELECT   LAST_DAY (rec.conversion_date) INTO l_last_day FROM DUAL;

         SELECT   TRIM (TO_CHAR (rec.conversion_date, 'Day'))
           INTO   l_weekday
           FROM   DUAL;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Week Day:' || l_weekday);

         SELECT   TRIM (TO_CHAR (rec.conversion_date, 'Day'))
           INTO   l_weekend
           FROM   DUAL;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Weekend Day:' || l_weekend);


         IF l_weekday IN ('Monday', 'Tuesday', 'Wednesday', 'Thursday')
         THEN
            BEGIN
               INSERT INTO GL_DAILY_RATES_INTERFACE (FROM_CURRENCY,
                                                     TO_CURRENCY,
                                                     USER_CONVERSION_TYPE,
                                                     CONVERSION_RATE,
                                                     FROM_CONVERSION_DATE,
                                                     TO_CONVERSION_DATE,
                                                     MODE_FLAG,
                                                     INVERSE_CONVERSION_RATE)
                 VALUES   (rec.from_currency,
                           rec.to_currency,
                           'Corporate'    --Corporate  PeriodAverage PeriodEnd
                                      ,
                           rec.conversion_rate,
                           rec.conversion_date + 1,
                           rec.conversion_date + 1,
                           'I',
                           rec.inverse_conversion_rate);
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for Corporate in Weekday:'
                                       || SQLERRM);
                  FND_FILE.PUT_LINE (
                     FND_FILE.LOG,
                     'Error while inserting data into GL Daily Interface for Corporate in Weekday:'
                     || SQLERRM
                  );
                  l_message :=
                     l_message || ','
                     || 'Error while inserting data into GL Daily Interface for Corporate in Weekday:'
                     || SQLERRM;
            END;

            l_conversion_date1 :=
               TO_DATE (rec.conversion_date, 'DD-MON-RR') + 1;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Weekday Month End:' || l_conversion_date1);

            SELECT   LAST_DAY (l_conversion_date1)
              INTO   l_last_day_Month1
              FROM   DUAL;
         /*IF l_last_day_Month1 = l_conversion_date1
         THEN
         -- Below is commented because for period average we have written in separate procedure.
             l_NO_DAYS_MONTH1 := RGGL_NO_OF_DAYS(l_conversion_date1);

             FND_FILE.PUT_LINE(FND_FILE.LOG,'l_NO_DAYS_MONTH1:'||l_NO_DAYS_MONTH1);

             BEGIN
                 SELECT SUM(conversion_rate) --/l_NO_DAYS
                   INTO l_periodaverage_rate1
                   FROM GL_DAILY_RATES_INTERFACE
                  WHERE from_currency = rec.from_currency
                    AND to_currency   = rec.to_currency
                    AND user_conversion_type = 'Corporate'
                    AND from_conversion_date >= RGGL_first_day(l_conversion_date1)
                    AND to_conversion_date <= RGGL_LAST_DAY(l_conversion_date1);
                FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate1:'||l_periodaverage_rate1);
             EXCEPTION
                 WHEN OTHERS THEN
                 l_periodaverage_rate1 := 0;
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                 DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                 l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 1:'||SQLERRM;
             END;

             BEGIN
                 SELECT SUM(conversion_rate)
                   INTO l_periodaverage_rate2
                   FROM gl_daily_rates
                  WHERE from_currency = rec.from_currency
                    AND to_currency   = rec.to_currency
                    AND conversion_type = 'Corporate'
                    AND conversion_date BETWEEN RGGL_first_day(l_conversion_date1)
                                            AND RGGL_LAST_DAY(l_conversion_date1);
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate2:'||l_periodaverage_rate2);
             EXCEPTION
                 WHEN OTHERS THEN
                 l_periodaverage_rate2 := 0;
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                 DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                 l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 2:'||SQLERRM;
             END;

             l_periodaverage_rate := (NVL(l_periodaverage_rate1,0) + NVL(l_periodaverage_rate2,0))/l_NO_DAYS_MONTH1;

             FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate:'||l_periodaverage_rate);

             BEGIN
                 INSERT INTO GL_DAILY_RATES_INTERFACE
                           (  FROM_CURRENCY
                             ,TO_CURRENCY
                             ,USER_CONVERSION_TYPE
                             ,CONVERSION_RATE
                             ,FROM_CONVERSION_DATE
                             ,TO_CONVERSION_DATE
                             ,MODE_FLAG
                             ,INVERSE_CONVERSION_RATE
                           )
                     VALUES
                           ( rec.from_currency
                            ,rec.to_currency
                            ,'PeriodAverage'        --Corporate  PeriodAverage PeriodEnd
                            ,l_periodaverage_rate
                            ,l_conversion_date1
                            ,l_conversion_date1
                            ,'I'
                            ,rec.inverse_conversion_rate
                           );

             EXCEPTION
                 WHEN OTHERS THEN
                 DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodAverage5:'||SQLERRM);
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM);
                 l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM;
             END;

             BEGIN
                 INSERT INTO GL_DAILY_RATES_INTERFACE
                           (  FROM_CURRENCY
                             ,TO_CURRENCY
                             ,USER_CONVERSION_TYPE
                             ,CONVERSION_RATE
                             ,FROM_CONVERSION_DATE
                             ,TO_CONVERSION_DATE
                             ,MODE_FLAG
                             ,INVERSE_CONVERSION_RATE
                           )
                     VALUES
                           ( rec.from_currency
                            ,rec.to_currency
                            ,'PeriodEnd'        --Corporate  PeriodAverage PeriodEnd
                            ,rec.conversion_rate
                            ,l_conversion_date1
                            ,l_conversion_date1
                            ,'I'
                            ,rec.inverse_conversion_rate
                           );
             EXCEPTION
                 WHEN OTHERS THEN
                 DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                 l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM;
             END;
     END IF;*/
         ELSIF l_weekend = 'Friday'                    --,'Saturday','Sunday')
         THEN
            FOR i IN 1 .. 3
            LOOP
               FND_FILE.PUT_LINE (FND_FILE.LOG, 'i              :' || i);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'From Currency  :' || rec.from_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'To Currnecy    :' || rec.to_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Rate           :' || rec.conversion_rate);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Conversion Date:' || rec.conversion_date);

               BEGIN
                  INSERT INTO GL_DAILY_RATES_INTERFACE (
                                                           FROM_CURRENCY,
                                                           TO_CURRENCY,
                                                           USER_CONVERSION_TYPE,
                                                           CONVERSION_RATE,
                                                           FROM_CONVERSION_DATE,
                                                           TO_CONVERSION_DATE,
                                                           MODE_FLAG,
                                                           INVERSE_CONVERSION_RATE
                             )
                    VALUES   (rec.from_currency,
                              rec.to_currency,
                              'Corporate' --Corporate  PeriodAverage PeriodEnd
                                         ,
                              rec.conversion_rate,
                              rec.conversion_date + i,
                              rec.conversion_date + i,
                              'I',
                              rec.inverse_conversion_rate);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for Corporate in Weekend:'
                                          || SQLERRM);
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while inserting data into GL Daily Interface for Corporate in Weekend:'
                        || SQLERRM
                     );
                     l_message :=
                        l_message || ','
                        || 'Error while inserting data into GL Daily Interface for Corporate in Weekend:'
                        || SQLERRM;
               END;

               l_conversion_date :=
                  TO_DATE (rec.conversion_date, 'DD-MON-RR') + i;

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'WeekEnd Month End:' || l_conversion_date);

               SELECT   LAST_DAY (l_conversion_date)
                 INTO   l_last_day_Month
                 FROM   DUAL;
            /*IF l_last_day_Month = l_conversion_date
            THEN
            -- Below is commented because for period average we have written in separate procedure.
                l_NO_DAYS_MONTH := RGGL_NO_OF_DAYS(l_conversion_date);

                FND_FILE.PUT_LINE(FND_FILE.LOG,'l_NO_DAYS_MONTH:'||l_NO_DAYS_MONTH);

                BEGIN
                    SELECT SUM(conversion_rate) --/l_NO_DAYS
                      INTO l_periodaverage_rate3
                      FROM GL_DAILY_RATES_INTERFACE
                     WHERE from_currency = rec.from_currency
                       AND to_currency   = rec.to_currency
                       AND user_conversion_type = 'Corporate'
                       AND from_conversion_date >= RGGL_first_day(l_conversion_date)
                       AND to_conversion_date <= RGGL_LAST_DAY(l_conversion_date);
                   FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate3:'||l_periodaverage_rate3);
                EXCEPTION
                    WHEN OTHERS THEN
                    l_periodaverage_rate3 := 0;
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                    DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                    l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 1:'||SQLERRM;
                END;

                BEGIN
                    SELECT SUM(conversion_rate)
                      INTO l_periodaverage_rate4
                      FROM gl_daily_rates
                     WHERE from_currency = rec.from_currency
                       AND to_currency   = rec.to_currency
                       AND conversion_type = 'Corporate'
                       AND conversion_date BETWEEN RGGL_first_day(l_conversion_date)
                                               AND RGGL_LAST_DAY(l_conversion_date);
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate4:'||l_periodaverage_rate4);
                EXCEPTION
                    WHEN OTHERS THEN
                    l_periodaverage_rate4 := 0;
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                    DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                    l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 2:'||SQLERRM;
                END;

                l_periodaverage_rate5 := (NVL(l_periodaverage_rate3,0) + NVL(l_periodaverage_rate4,0))/l_NO_DAYS_MONTH;

                FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate5:'||l_periodaverage_rate5);

                BEGIN
                    INSERT INTO GL_DAILY_RATES_INTERFACE
                              (  FROM_CURRENCY
                                ,TO_CURRENCY
                                ,USER_CONVERSION_TYPE
                                ,CONVERSION_RATE
                                ,FROM_CONVERSION_DATE
                                ,TO_CONVERSION_DATE
                                ,MODE_FLAG
                                ,INVERSE_CONVERSION_RATE
                              )
                        VALUES
                              ( rec.from_currency
                               ,rec.to_currency
                               ,'PeriodAverage'        --Corporate  PeriodAverage PeriodEnd
                               ,l_periodaverage_rate5
                               ,l_conversion_date
                               ,l_conversion_date
                               ,'I'
                               ,rec.inverse_conversion_rate
                              );

                EXCEPTION
                    WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodAverage5:'||SQLERRM);
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM);
                    l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM;
                END;

                BEGIN
                    INSERT INTO GL_DAILY_RATES_INTERFACE
                              (  FROM_CURRENCY
                                ,TO_CURRENCY
                                ,USER_CONVERSION_TYPE
                                ,CONVERSION_RATE
                                ,FROM_CONVERSION_DATE
                                ,TO_CONVERSION_DATE
                                ,MODE_FLAG
                                ,INVERSE_CONVERSION_RATE
                              )
                        VALUES
                              ( rec.from_currency
                               ,rec.to_currency
                               ,'PeriodEnd'        --Corporate  PeriodAverage PeriodEnd
                               ,rec.conversion_rate
                               ,l_conversion_date
                               ,l_conversion_date
                               ,'I'
                               ,rec.inverse_conversion_rate
                              );
                EXCEPTION
                    WHEN OTHERS THEN
                    DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                    FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                    l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM;
                END;
        END IF;*/

            END LOOP;
         END IF;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Last day :' || l_last_day);
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Last day conversion :' || rec.conversion_date);

         --  This commented because we have taken care in separate procedure.
         /*    IF  l_last_day = (rec.conversion_date)+1
             THEN



             l_NO_DAYS := RGGL_NO_OF_DAYS(rec.conversion_date);

             FND_FILE.PUT_LINE(FND_FILE.LOG,'l_NO_DAYS:'||l_NO_DAYS);

             BEGIN
                 SELECT SUM(conversion_rate) --/l_NO_DAYS
                   INTO l_periodaverage_rate1
                   FROM GL_DAILY_RATES_INTERFACE
                  WHERE from_currency = rec.from_currency
                    AND to_currency   = rec.to_currency
                    AND user_conversion_type = 'Corporate'
                    AND from_conversion_date >= RGGL_first_day(rec.conversion_date)
                    AND to_conversion_date <= RGGL_LAST_DAY(rec.conversion_date);
                FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate1:'||l_periodaverage_rate1);
             EXCEPTION
                 WHEN OTHERS THEN
                 l_periodaverage_rate1 := 0;
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                 DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 1:'||SQLERRM);
                 l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 1:'||SQLERRM;
             END;

             BEGIN
                 SELECT SUM(conversion_rate)
                   INTO l_periodaverage_rate2
                   FROM gl_daily_rates
                  WHERE from_currency = rec.from_currency
                    AND to_currency   = rec.to_currency
                    AND conversion_type = 'Corporate'
                    AND conversion_date BETWEEN RGGL_first_day(rec.conversion_date)
                                            AND RGGL_LAST_DAY(rec.conversion_date);
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate2:'||l_periodaverage_rate2);
             EXCEPTION
                 WHEN OTHERS THEN
                 l_periodaverage_rate1 := 0;
                 FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                 DBMS_OUTPUT.PUT_LINE('Error while fetching Sum of amount for Period Average 2:'||SQLERRM);
                 l_message := l_message ||','||'Error while fetching Sum of amount for Period Average 2:'||SQLERRM;
             END;

             l_periodaverage_rate := (l_periodaverage_rate1 + l_periodaverage_rate2)/l_NO_DAYS;

             FND_FILE.PUT_LINE(FND_FILE.LOG,'l_periodaverage_rate:'||l_periodaverage_rate);

                 BEGIN
                     INSERT INTO GL_DAILY_RATES_INTERFACE
                               (  FROM_CURRENCY
                                 ,TO_CURRENCY
                                 ,USER_CONVERSION_TYPE
                                 ,CONVERSION_RATE
                                 ,FROM_CONVERSION_DATE
                                 ,TO_CONVERSION_DATE
                                 ,MODE_FLAG
                                 ,INVERSE_CONVERSION_RATE
                               )
                         VALUES
                               ( rec.from_currency
                                ,rec.to_currency
                                ,'PeriodAverage'        --Corporate  PeriodAverage PeriodEnd
                                ,l_periodaverage_rate
                                ,rec.conversion_date+1
                                ,rec.conversion_date+1
                                ,'I'
                                ,rec.inverse_conversion_rate
                               );

                 EXCEPTION
                     WHEN OTHERS THEN
                     DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM);
                     FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM);
                     l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodAverage:'||SQLERRM;
                 END;

                 BEGIN
                     INSERT INTO GL_DAILY_RATES_INTERFACE
                               (  FROM_CURRENCY
                                 ,TO_CURRENCY
                                 ,USER_CONVERSION_TYPE
                                 ,CONVERSION_RATE
                                 ,FROM_CONVERSION_DATE
                                 ,TO_CONVERSION_DATE
                                 ,MODE_FLAG
                                 ,INVERSE_CONVERSION_RATE
                               )
                         VALUES
                               ( rec.from_currency
                                ,rec.to_currency
                                ,'PeriodEnd'        --Corporate  PeriodAverage PeriodEnd
                                ,rec.conversion_rate
                                ,rec.conversion_date+1
                                ,rec.conversion_date+1
                                ,'I'
                                ,rec.inverse_conversion_rate
                               );
                 EXCEPTION
                     WHEN OTHERS THEN
                     DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                     FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM);
                     l_message := l_message ||','||'Error while inserting data into GL Daily Interface for PeriodEnd:'||SQLERRM;
                 END;

             END IF;*/
         -- This condition for period end and period Average
         IF l_message IS NULL
         THEN
            UPDATE   RGGl_daily_rate_stg
               SET   rec_status = 'S',
                     rec_message = NULL,
                     PROCESSED_DATE = SYSDATE
             WHERE       from_currency = rec.from_currency
                     AND to_currency = rec.to_currency
                     AND ROWID = rec.row_id
                     AND rec_status = 'V';

            COMMIT;
         ELSE
            UPDATE   RGGl_daily_rate_stg
               SET   rec_status = 'E',
                     rec_message = l_message,
                     PROCESSED_DATE = SYSDATE
             WHERE       from_currency = rec.from_currency
                     AND to_currency = rec.to_currency
                     AND ROWID = rec.row_id
                     AND rec_status = 'V';

            COMMIT;
         END IF;
      END LOOP;
   /*
       SELECT count(1)
         INTO l_success_cnt
         FROM RGGl_daily_rate_stg
        WHERE rec_status      = 'S'
          AND PROCESSED_DATE  = sysdate;

       FND_FILE.PUT_LINE(FND_FILE.LOG,'l_success_cnt:'||l_success_cnt);

       IF l_success_cnt <> 0
       THEN
       BEGIN
           INSERT INTO rggl.rggl_daily_rate_hist
                       (from_currency,
                        to_currency,
                        conversion_date,
                        conversion_type,
                        conversion_rate,
                        status_code,
                        rec_status,
                        rec_message,
                        INVERSE_CONVERSION_RATE,
                        processed_date,
                        creation_date,
                        created_by,
                        last_update_date,
                        last_updated_by
                       )
              (SELECT from_currency,
                           to_currency,
                           conversion_date,
                           conversion_type,
                           conversion_rate,
                           status_code,
                           rec_status,
                           rec_message,
                           INVERSE_CONVERSION_RATE,
                           processed_date,
                           creation_date,
                           created_by,
                           last_update_date,
                           last_updated_by
                      FROM RGGL.RGGl_daily_rate_stg
                     WHERE status_code = 'S'
                       AND NOT EXISTS ( SELECT '1'
                                          FROM rggl.rggl_daily_rate_hist a
                                         WHERE a.from_currency = from_currency
                                           AND a.to_currency   = to_currency
                                           AND TRUNC(a.conversion_date) = TRUNC(conversion_date)));
       EXCEPTION
           WHEN OTHERS THEN
           FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while inserting data into History Table:'||SQLERRM);
           DBMS_OUTPUT.PUT_LINE('Error while inserting data into History Table:'||SQLERRM);
       END;
       COMMIT;
       END IF;
       */

   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error in Process Procedure:' || SQLERRM);
   END PROCESS_STG;

   -- This procedure is used to calculate Period Average from Gl Daily rates.
   PROCEDURE RGGL_PERIOD_AVG (ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER)
   IS
      CURSOR CUR
      IS
         SELECT   *
           FROM   gl_daily_rates a
          WHERE   TRUNC (a.CREATION_DATE) = TRUNC (SYSDATE)
                  AND a.conversion_type = 'Corporate';

      /*AND EXISTS ( SELECT '1'
                     FROM RGGl_daily_rate_stg
                    WHERE a.from_currency = from_currency
                      AND a.to_currency   = to_currency);*/

      l_last_day_Month    DATE;
      l_sum_amount        NUMBER;
      l_no_of_days        NUMBER;
      l_period_average    NUMBER;
      l_sum_amount1       NUMBER;
      l_no_of_days1       NUMBER;
      l_period_average1   NUMBER;
      l_int_count         NUMBER;
      l_exists_int        NUMBER := 0;
      l_period_end_amt    NUMBER := 0;
   BEGIN
      FOR REC IN CUR
      LOOP
         l_last_day_Month := NULL;
         l_sum_amount := NULL;
         l_no_of_days := NULL;
         l_period_average := NULL;
         l_sum_amount1 := NULL;
         l_no_of_days1 := NULL;
         l_period_average1 := NULL;
         l_exists_int := NULL;
         l_period_end_amt := NULL;

         SELECT   LAST_DAY (rec.conversion_date)
           INTO   l_last_day_Month
           FROM   DUAL;

         IF l_last_day_Month = rec.conversion_date
         THEN
            -- This condition to avoid overlapping records for period average.
            BEGIN
               SELECT   COUNT (1)
                 INTO   l_exists_int
                 FROM   GL_DAILY_RATES_INTERFACE
                WHERE   (from_currency = rec.to_currency
                         OR from_currency = rec.from_currency)
                        AND (to_currency = rec.from_currency
                             OR to_currency = rec.to_currency)
                        AND user_conversion_type = 'PeriodAverage'
                        AND FROM_CONVERSION_DATE = rec.conversion_date;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_exists_int := 0;
            END;

            IF l_exists_int = 0
            THEN
               -- Period Average calculation for conversion rate
               BEGIN
                  SELECT   COUNT (1)
                    INTO   l_no_of_days
                    FROM   gl_daily_rates
                   WHERE       from_currency = rec.from_currency
                           AND to_currency = rec.to_currency
                           AND conversion_type = 'Corporate'
                           AND conversion_date BETWEEN RGGL_first_day (
                                                          rec.conversion_date
                                                       )
                                                   AND  RGGL_LAST_DAY(rec.conversion_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while fetching no. of days for passed month:'
                        || SQLERRM
                     );
               END;

               BEGIN
                  SELECT   NVL (SUM (conversion_rate), 0)
                    INTO   l_sum_amount
                    FROM   gl_daily_rates
                   WHERE       from_currency = rec.from_currency
                           AND to_currency = rec.to_currency
                           AND conversion_type = 'Corporate'
                           AND conversion_date BETWEEN RGGL_first_day (
                                                          rec.conversion_date
                                                       )
                                                   AND  RGGL_LAST_DAY(rec.conversion_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while fetching sum of conversion_rate:'
                        || SQLERRM
                     );
               END;

               l_period_average := l_sum_amount / l_no_of_days;

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'From Currency   :' || rec.from_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'To Currency     :' || rec.to_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'Period Average  :' || l_period_average);

               -- Period average calculation for inverse rate
               BEGIN
                  SELECT   COUNT (1)
                    INTO   l_no_of_days1
                    FROM   gl_daily_rates
                   WHERE       from_currency = rec.to_currency
                           AND to_currency = rec.from_currency
                           AND conversion_type = 'Corporate'
                           AND conversion_date BETWEEN RGGL_first_day (
                                                          rec.conversion_date
                                                       )
                                                   AND  RGGL_LAST_DAY(rec.conversion_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while fetching no. of days for passed month:'
                        || SQLERRM
                     );
               END;

               BEGIN
                  SELECT   NVL (SUM (conversion_rate), 0)
                    INTO   l_sum_amount1
                    FROM   gl_daily_rates
                   WHERE       from_currency = rec.to_currency
                           AND to_currency = rec.from_currency
                           AND conversion_type = 'Corporate'
                           AND conversion_date BETWEEN RGGL_first_day (
                                                          rec.conversion_date
                                                       )
                                                   AND  RGGL_LAST_DAY(rec.conversion_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while fetching sum of conversion_rate:'
                        || SQLERRM
                     );
               END;

               --l_period_average1 := 1/l_period_average;
               l_period_average1 := l_sum_amount1 / l_no_of_days1;

               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'From Currency   :' || rec.to_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'To Currency     :' || rec.from_currency);
               FND_FILE.PUT_LINE (FND_FILE.LOG,
                                  'InverseAverage  :' || l_period_average1);

               BEGIN
                  INSERT INTO GL_DAILY_RATES_INTERFACE (
                                                           FROM_CURRENCY,
                                                           TO_CURRENCY,
                                                           USER_CONVERSION_TYPE,
                                                           CONVERSION_RATE,
                                                           FROM_CONVERSION_DATE,
                                                           TO_CONVERSION_DATE,
                                                           MODE_FLAG,
                                                           INVERSE_CONVERSION_RATE
                             )
                    VALUES   (rec.from_currency,
                              rec.to_currency,
                              'PeriodAverage'                  --PeriodAverage
                                             ,
                              l_period_average,
                              rec.conversion_date,
                              rec.conversion_date,
                              'I',
                              l_period_average1);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodAverage:'
                                          || SQLERRM);
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while inserting data into GL Daily Interface for PeriodAverage:'
                        || SQLERRM
                     );
               END;

               BEGIN
                  SELECT   NVL (conversion_rate, 0)
                    INTO   l_period_end_amt
                    FROM   gl_daily_rates
                   WHERE       from_currency = rec.to_currency
                           AND to_currency = rec.from_currency
                           AND conversion_type = 'Corporate'
                           AND conversion_date =
                                 RGGL_LAST_DAY (rec.conversion_date);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_period_end_amt := 0;
               END;

               BEGIN
                  INSERT INTO GL_DAILY_RATES_INTERFACE (
                                                           FROM_CURRENCY,
                                                           TO_CURRENCY,
                                                           USER_CONVERSION_TYPE,
                                                           CONVERSION_RATE,
                                                           FROM_CONVERSION_DATE,
                                                           TO_CONVERSION_DATE,
                                                           MODE_FLAG,
                                                           INVERSE_CONVERSION_RATE
                             )
                    VALUES   (rec.from_currency,
                              rec.to_currency,
                              'PeriodEnd' --Corporate  PeriodAverage PeriodEnd
                                         ,
                              rec.conversion_rate,
                              rec.conversion_date,
                              rec.conversion_date,
                              'I',
                              (1 / rec.conversion_rate));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     DBMS_OUTPUT.PUT_LINE('Error while inserting data into GL Daily Interface for PeriodEnd:'
                                          || SQLERRM);
                     FND_FILE.PUT_LINE (
                        FND_FILE.LOG,
                        'Error while inserting data into GL Daily Interface for PeriodEnd:'
                        || SQLERRM
                     );
               END;
            END IF; -- This condition is to check weather the same combination exists in interface table to avoid duplicate.
         END IF;

         COMMIT;
      END LOOP;

      -- This procedure will submit standard Daily Rates Concurrent Program to process into Base tables

      SUBMIT_RG_DAILY_IMPORT;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_int_count
           FROM   GL_DAILY_RATES_INTERFACE
          WHERE   ERROR_CODE IS NOT NULL;
      --AND USER_CONVERSION_TYPE = 'PeriodAverage';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_int_count := 0;
      END;

      IF l_int_count = 0
      THEN
         RGGL_SEND_SUCCESS_MAIL;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error in Period Average Procedure:' || SQLERRM);
   END RGGL_PERIOD_AVG;



   -- This Procedure is used to send an email to users for no records found in staging table.
   PROCEDURE RGGL_SEND_NORECORDS_MAIL
   IS
      v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
      v_Recipient   VARCHAR2 (1000);         -- := 'ext.mardam@riotgames.com';
      v_subject     VARCHAR2 (1000);
      v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
      v_Mail_Conn   UTL_SMTP.Connection;
      crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
      vctr          NUMBER := 0;
      vLine         VARCHAR2 (32767);
      l_database    VARCHAR2 (100);
      l_email       VARCHAR2 (100);
      l_file_name   VARCHAR2 (1000);
      v_file_name   VARCHAR2 (1000);
   BEGIN
      BEGIN
         SELECT   Name INTO l_database FROM v$database;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_database := NULL;
            DBMS_OUTPUT.put_line ('Error in data base fetch:' || SQLERRM);
      END;

      BEGIN
         SELECT   meaning
           INTO   l_email
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATE_EMAIL_NOTIFY'
                  AND enabled_flag = 'Y'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_email := NULL;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Error while fetching email address:' || SQLERRM
            );
      END;


      v_subject :=
         'GL Daily Rate Interface' || '-' || '[' || l_database || ']';
      v_Recipient := l_email;
      --v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);


      v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
      UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
      UTL_SMTP.Mail (v_Mail_Conn, v_From);
      UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

      UTL_SMTP.Data (
         v_Mail_Conn,
            --   'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf
            'From: '
         || v_From
         || crlf
         || 'Subject: '
         || v_Subject
         || crlf
         || 'To: '
         || v_Recipient
         || crlf
         || 'MIME-Version: 1.0'
         || crlf
         ||                                          -- Use MIME mail standard
           'Content-Type: multipart/mixed;'
         || crlf
         || ' boundary="-----SECBOUND"'
         || crlf
         || crlf
         || '-------SECBOUND'
         || crlf
         || 'Content-Type: text/html;'
         || crlf
         || 'Content-Transfer_Encoding: 7bit'
         || crlf
         || crlf
         || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
         || crlf
         || 'There was <strong>no record in stage table</strong> to process in this run.<p>'
         || crlf
         || CHR (10)
         || ' '
         || '<br>'
         || '<br>'
         ||                                                    -- Message body
           'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
         || crlf
         || '-------SECBOUND--'                               -- End MIME mail
      );
      UTL_SMTP.Quit (v_mail_conn);
   EXCEPTION
      WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
      THEN
         raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
   END RGGL_SEND_NORECORDS_MAIL;


   PROCEDURE RGGL_SEND_NOERRORS_MAIL
   IS
      v_From                     VARCHAR2 (80) := 'donotreply@riotgames.com';
      v_Recipient                VARCHAR2 (1000);
      v_subject                  VARCHAR2 (1000);
      v_Mail_Host                VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
      v_Mail_Conn                UTL_SMTP.Connection;
      crlf                       VARCHAR2 (2) := CHR (13) || CHR (10);
      vctr                       NUMBER := 0;
      vLine                      VARCHAR2 (32767);
      l_database                 VARCHAR2 (100);
      l_email                    VARCHAR2 (100);
      l_file_name                VARCHAR2 (1000);
      v_file_name                VARCHAR2 (1000);
      l_total_record_staging     NUMBER := 0;
      l_total_processed_record   NUMBER := 0;
      l_total_error_record       NUMBER := 0;
   BEGIN
      BEGIN
         SELECT   Name INTO l_database FROM v$database;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_database := NULL;
            DBMS_OUTPUT.put_line ('Error in data base fetch:' || SQLERRM);
      END;

      BEGIN
         SELECT   meaning
           INTO   l_email
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATE_EMAIL_NOTIFY'
                  AND enabled_flag = 'Y'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_email := NULL;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Error while fetching email address:' || SQLERRM
            );
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_record_staging
           FROM   RGGl_daily_rate_stg
          WHERE   TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_record_staging := 0;
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_processed_record
           FROM   RGGl_daily_rate_stg
          WHERE       TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND rec_status = 'S'
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_processed_record := 0;
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_error_record
           FROM   RGGl_daily_rate_stg
          WHERE       TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND rec_status = 'E'
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_error_record := 0;
      END;


      v_subject :=
         'GL Daily Rate Interface' || '-' || '[' || l_database || ']';
      v_Recipient := l_email;
      --v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);


      v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
      UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
      UTL_SMTP.Mail (v_Mail_Conn, v_From);
      UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

      UTL_SMTP.Data (
         v_Mail_Conn,
            --   'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf
            'From: '
         || v_From
         || crlf
         || 'Subject: '
         || v_Subject
         || crlf
         || 'To: '
         || v_Recipient
         || crlf
         || 'MIME-Version: 1.0'
         || crlf
         ||                                          -- Use MIME mail standard
           'Content-Type: multipart/mixed;'
         || crlf
         || ' boundary="-----SECBOUND"'
         || crlf
         || crlf
         || '-------SECBOUND'
         || crlf
         || 'Content-Type: text/html;'
         || crlf
         || 'Content-Transfer_Encoding: 7bit'
         || crlf
         || crlf
         || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
         || crlf
         || 'All records were <strong>processed successfully </strong> in this run.<p>'
         || crlf
         || 'Total record in stage table   :'
         || l_total_record_staging
         || '.<p>'
         || crlf
         || 'Total processed records       :'
         || l_total_processed_record
         || '. <p>'
         || crlf
         || 'Total error/exception records :'
         || l_total_error_record
         || '.<p>'
         || CHR (10)
         || ' '
         || '<br>'
         || '<br>'
         ||                                                    -- Message body
           'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
         || crlf
         || '-------SECBOUND--'                               -- End MIME mail
      );
      UTL_SMTP.Quit (v_mail_conn);
   EXCEPTION
      WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
      THEN
         raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
   END RGGL_SEND_NOERRORS_MAIL;


   -- This procedure is used to send an email to user regarding errors/exceptions
   PROCEDURE RGGL_SEND_ERROR_MAIL
   IS
      v_From                     VARCHAR2 (80) := 'donotreply@riotgames.com';
      v_Recipient                VARCHAR2 (1000);
      v_subject                  VARCHAR2 (1000);
      v_Mail_Host                VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
      v_Mail_Conn                UTL_SMTP.Connection;
      crlf                       VARCHAR2 (2) := CHR (13) || CHR (10);
      vctr                       NUMBER := 0;
      vLine                      VARCHAR2 (32767);
      l_database                 VARCHAR2 (100);
      l_email                    VARCHAR2 (100);
      l_file_name                VARCHAR2 (1000);
      v_file_name                VARCHAR2 (1000);
      l_total_record_staging     NUMBER := 0;
      l_total_processed_record   NUMBER := 0;
      l_total_error_record       NUMBER := 0;

      CURSOR c
      IS
         SELECT   from_currency,
                  to_currency,
                  conversion_date,
                  conversion_type,
                  rec_status,
                  LTRIM (rec_message, ',') MESSAGE_TEXT
           FROM   RGGl_daily_rate_stg
          WHERE       rec_status = 'E'
                  AND TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
   BEGIN
      BEGIN
         SELECT   Name INTO l_database FROM v$database;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_database := NULL;
            DBMS_OUTPUT.put_line ('Error in data base fetch:' || SQLERRM);
      END;

      BEGIN
         SELECT   meaning
           INTO   l_email
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATE_EMAIL_NOTIFY'
                  AND enabled_flag = 'Y'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_email := NULL;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Error while fetching email address:' || SQLERRM
            );
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_record_staging
           FROM   RGGl_daily_rate_stg
          WHERE   TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_record_staging := 0;
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_processed_record
           FROM   RGGl_daily_rate_stg
          WHERE       TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND rec_status = 'S'
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_processed_record := 0;
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_error_record
           FROM   RGGl_daily_rate_stg
          WHERE       TRUNC (creation_date) = TRUNC (SYSDATE)
                  AND rec_status = 'E'
                  AND NVL (attribute1, 'NR') != 'R'; -- SANNE Added on 11/July/2016
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_error_record := 0;
      END;



      v_subject :=
         'GL Daily Rate Interface' || '-' || '[' || l_database || ']';
      v_Recipient := l_email;


      vLine :=
         'From Currency, To Currency, Conversion Date, Conversion Type, Message Text';

      FOR crec IN c
      LOOP
         BEGIN
            IF LENGTH (vLine) > 32767
            THEN
               vLine := vLine;
            ELSE
               vLine :=
                     vLine
                  || CHR (10)
                  || crec.from_currency
                  || ','
                  || crec.to_currency
                  || ','
                  || crec.conversion_date
                  || ','
                  || crec.conversion_type
                  || ','
                  || crec.MESSAGE_TEXT;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               IF (SQLCODE = -06502)
               THEN
                  vLine := vLine;
                  v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
                  UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
                  UTL_SMTP.Mail (v_Mail_Conn, v_From);
                  UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

                  UTL_SMTP.Data (
                     v_Mail_Conn,
                        --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
                        'From: '
                     || v_From
                     || crlf
                     || 'Subject: '
                     || v_Subject
                     || crlf
                     || 'To: '
                     || v_Recipient
                     || crlf
                     || 'MIME-Version: 1.0'
                     || crlf
                     ||                              -- Use MIME mail standard
                       'Content-Type: multipart/mixed;'
                     || crlf
                     || ' boundary="-----SECBOUND"'
                     || crlf
                     || crlf
                     || '-------SECBOUND'
                     || crlf
                     || 'Content-Type: text/html;'
                     || crlf
                     || 'Content-Transfer_Encoding: 7bit'
                     || crlf
                     || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
                     || crlf
                     || 'Some of the stage records could <strong>not be processed </strong> in this run. Please review attached exception report and take necessary action. <p>'
                     || crlf
                     || 'Total record in stage table   :'
                     || l_total_record_staging
                     || '.<p>'
                     || crlf
                     || 'Total processed records       :'
                     || l_total_processed_record
                     || '. <p>'
                     || crlf
                     || 'Total error/exception records :'
                     || l_total_error_record
                     || '.<p>'
                     || CHR (10)
                     || crlf
                     || ' '
                     || '<br>'
                     || '<br>'
                     ||                                        -- Message body
                       'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
                     || crlf
                     || crlf
                     || '-------SECBOUND'
                     || crlf
                     || 'Content-Type: text/plain;'
                     || crlf
                     || ' name="excel.csv"'
                     || crlf
                     || 'Content-Transfer_Encoding: 8bit'
                     || crlf
                     || 'Content-Disposition: attachment;'
                     || crlf
                     || ' filename="RGDaily_Rates_Error_Report.csv"'
                     || crlf
                     || crlf
                     || vLine
                     || crlf
                     ||                               -- Content of attachment
                       crlf
                     || '-------SECBOUND--'                   -- End MIME mail
                  );
                  vLine :=
                     'Statement Number, Bank Account Number, Creation Date, Message Text';
                  UTL_SMTP.Quit (v_mail_conn);
               END IF;
         END;
      END LOOP;

      v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
      UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
      UTL_SMTP.Mail (v_Mail_Conn, v_From);
      UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

      UTL_SMTP.Data (
         v_Mail_Conn,
            --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
            'From: '
         || v_From
         || crlf
         || 'Subject: '
         || v_Subject
         || crlf
         || 'To: '
         || v_Recipient
         || crlf
         || 'MIME-Version: 1.0'
         || crlf
         ||                                          -- Use MIME mail standard
           'Content-Type: multipart/mixed;'
         || crlf
         || ' boundary="-----SECBOUND"'
         || crlf
         || crlf
         || '-------SECBOUND'
         || crlf
         || 'Content-Type: text/html;'
         || crlf
         || 'Content-Transfer_Encoding: 7bit'
         || crlf
         || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
         || crlf
         || 'Some of the stage records could <strong>not be processed </strong> in this run. Please review attached exception report and take necessary action. <p>'
         || crlf
         || 'Total record in stage table : '
         || l_total_record_staging
         || '.<p>'
         || crlf
         || 'Total processed records : '
         || l_total_processed_record
         || '. <p>'
         || crlf
         || 'Total error/exception records : '
         || l_total_error_record
         || '.<p>'
         || CHR (10)
         || crlf
         || ' '
         || '<br>'
         || '<br>'
         ||                                                    -- Message body
           'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
         || crlf
         || crlf
         || '-------SECBOUND'
         || crlf
         || 'Content-Type: text/plain;'
         || crlf
         || ' name="excel.csv"'
         || crlf
         || 'Content-Transfer_Encoding: 8bit'
         || crlf
         || 'Content-Disposition: attachment;'
         || crlf
         || ' filename="RGDaily_Rates_Error_Report.csv"'
         || crlf
         || crlf
         || vLine
         || crlf
         ||                                           -- Content of attachment
           crlf
         || '-------SECBOUND--'                               -- End MIME mail
      );
      UTL_SMTP.Quit (v_mail_conn);
   EXCEPTION
      WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
      THEN
         raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
   END RGGL_SEND_ERROR_MAIL;


   -- This Procedure is used to send success report to distribution group
   PROCEDURE RGGL_SEND_SUCCESS_MAIL
   IS
      v_From                     VARCHAR2 (80) := 'donotreply@riotgames.com';
      v_Recipient                VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
      v_subject                  VARCHAR2 (1000);
      v_Mail_Host                VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
      v_Mail_Conn                UTL_SMTP.Connection;
      crlf                       VARCHAR2 (2) := CHR (13) || CHR (10);
      vctr                       NUMBER := 0;
      vLine                      VARCHAR2 (32767);
      l_database                 VARCHAR2 (100);
      l_email                    VARCHAR2 (100);
      l_file_name                VARCHAR2 (1000);
      v_file_name                VARCHAR2 (1000);
      l_total_record_staging     NUMBER := 0;
      l_total_processed_record   NUMBER := 0;
      l_total_error_record       NUMBER := 0;

      CURSOR c
      IS
           SELECT   from_currency,
                    to_currency,
                    conversion_date,
                    c.user_conversion_type conversion_type,
                    conversion_rate,
                    rate_source_code,
                    user_name
             FROM   gl_daily_rates a, fnd_user b, gl_daily_conversion_types c
            WHERE       a.created_by = b.user_id
                    AND a.conversion_type = c.conversion_type
                    AND TRUNC (a.creation_date) = TRUNC (SYSDATE)
                    AND TRUNC (a.conversion_date) >=
                          (SELECT   MIN (conversion_date) + 1
                             FROM   rggl_daily_rate_stg
                            WHERE   rec_status = 'S'
                                    AND NVL (attribute1, 'NR') != 'R') -- SANNE Added on 11/July/2016
         ORDER BY   conversion_date,
                    from_currency,
                    to_currency,
                    a.creation_date DESC;

      l_rec_cnt                  NUMBER := 0;                         -- sanne
   BEGIN
      -------SANNE Added below code on July 7 2016
      BEGIN
         SELECT   COUNT (0)
           INTO   l_rec_cnt
           FROM   gl_daily_rates a, fnd_user b, gl_daily_conversion_types c
          WHERE       a.created_by = b.user_id
                  AND a.conversion_type = c.conversion_type
                  AND TRUNC (a.creation_date) = TRUNC (SYSDATE)
                  AND TRUNC (a.conversion_date) >=
                        (SELECT   MIN (conversion_date) + 1
                           FROM   rggl_daily_rate_stg
                          WHERE   rec_status = 'S'
                                  AND NVL (attribute1, 'NR') != 'R') -- SANNE Added on 11/July/2016
                  AND ROWNUM < 2;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_rec_cnt := 0;
      END;


      IF l_rec_cnt > 0
      THEN
         BEGIN
            SELECT   Name INTO l_database FROM v$database;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_database := NULL;
               DBMS_OUTPUT.put_line ('Error in data base fetch:' || SQLERRM);
         END;

         BEGIN
            SELECT   meaning
              INTO   l_email
              FROM   fnd_common_lookups
             WHERE       lookup_type LIKE 'RGGL_DAILY_RATE_SUCCESS_NOTIFY'
                     AND enabled_flag = 'Y'
                     AND NVL (end_date_active, SYSDATE) >= SYSDATE;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_email := NULL;
               FND_FILE.PUT_LINE (
                  FND_FILE.LOG,
                  'Error while fetching email address:' || SQLERRM
               );
         END;

         v_subject :=
            'GL Daily Rate Interface' || '-' || '[' || l_database || ']';
         v_Recipient := l_email;


         vLine :=
            'From Currency, To Currency, Conversion Date, Conversion Type, Conversion Rate, Rate Source Code, User Name';

         FOR crec IN c
         LOOP
            BEGIN
               IF LENGTH (vLine) > 32767
               THEN
                  vLine := vLine;
               ELSE
                  vLine :=
                        vLine
                     || CHR (10)
                     || crec.from_currency
                     || ','
                     || crec.to_currency
                     || ','
                     || crec.conversion_date
                     || ','
                     || crec.conversion_type
                     || ','
                     || crec.conversion_rate
                     || ','
                     || crec.rate_source_code
                     || ','
                     || crec.user_name;
               END IF;
            EXCEPTION
               WHEN OTHERS
               THEN
                  IF (SQLCODE = -06502)
                  THEN
                     vLine := vLine;
                     v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
                     UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
                     UTL_SMTP.Mail (v_Mail_Conn, v_From);
                     UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

                     UTL_SMTP.Data (
                        v_Mail_Conn,
                           --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
                           'From: '
                        || v_From
                        || crlf
                        || 'Subject: '
                        || v_Subject
                        || crlf
                        || 'To: '
                        || v_Recipient
                        || crlf
                        || 'MIME-Version: 1.0'
                        || crlf
                        ||                           -- Use MIME mail standard
                          'Content-Type: multipart/mixed;'
                        || crlf
                        || ' boundary="-----SECBOUND"'
                        || crlf
                        || crlf
                        || '-------SECBOUND'
                        || crlf
                        || 'Content-Type: text/html;'
                        || crlf
                        || 'Content-Transfer_Encoding: 7bit'
                        || crlf
                        || crlf
                        || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
                        || crlf
                        || 'Report of successfully created exchange rates is <strong> attached </strong> for review. <p>'
                        || crlf
                        || ' '
                        || '<br>'
                        || '<br>'
                        ||                                     -- Message body
                          'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
                        || crlf
                        || crlf
                        || '-------SECBOUND'
                        || crlf
                        || 'Content-Type: text/plain;'
                        || crlf
                        || ' name="excel.csv"'
                        || crlf
                        || 'Content-Transfer_Encoding: 8bit'
                        || crlf
                        || 'Content-Disposition: attachment;'
                        || crlf
                        || ' filename="RGDaily_Rates_Success_Report.csv"'
                        || crlf
                        || crlf
                        || vLine
                        || crlf
                        ||                            -- Content of attachment
                          crlf
                        || '-------SECBOUND--'                -- End MIME mail
                     );
                     vLine :=
                        'Statement Number, Bank Account Number, Creation Date, Message Text';
                     UTL_SMTP.Quit (v_mail_conn);
                  END IF;
            END;
         END LOOP;

         v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
         UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
         UTL_SMTP.Mail (v_Mail_Conn, v_From);
         UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

         UTL_SMTP.Data (
            v_Mail_Conn,
               --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
               'From: '
            || v_From
            || crlf
            || 'Subject: '
            || v_Subject
            || crlf
            || 'To: '
            || v_Recipient
            || crlf
            || 'MIME-Version: 1.0'
            || crlf
            ||                                       -- Use MIME mail standard
              'Content-Type: multipart/mixed;'
            || crlf
            || ' boundary="-----SECBOUND"'
            || crlf
            || crlf
            || '-------SECBOUND'
            || crlf
            || 'Content-Type: text/html;'
            || crlf
            || 'Content-Transfer_Encoding: 7bit'
            || crlf
            || crlf
            || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
            || crlf
            || 'Report of successfully created exchange rates is <strong> attached </strong> for review. <p>'
            || crlf
            || ' '
            || '<br>'
            || '<br>'
            ||                                                 -- Message body
              'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
            || crlf
            || crlf
            || '-------SECBOUND'
            || crlf
            || 'Content-Type: text/plain;'
            || crlf
            || ' name="excel.csv"'
            || crlf
            || 'Content-Transfer_Encoding: 8bit'
            || crlf
            || 'Content-Disposition: attachment;'
            || crlf
            || ' filename="RGDaily_Rates_Success_Report.csv"'
            || crlf
            || crlf
            || vLine
            || crlf
            ||                                        -- Content of attachment
              crlf
            || '-------SECBOUND--'                            -- End MIME mail
         );
         UTL_SMTP.Quit (v_mail_conn);
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG, ' Unable to send Success mail: ');
      END IF;
   ---------------Sanne Coded ended--------
   EXCEPTION
      WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
      THEN
         raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
   END RGGL_SEND_SUCCESS_MAIL;


   -- This procedure is used to send an email to distribution group for limit+tolerance count mismatch with staging count or records in N status.
   PROCEDURE RGGL_SEND_LIMIT_MAIL
   IS
      v_From                 VARCHAR2 (80) := 'donotreply@riotgames.com';
      v_Recipient            VARCHAR2 (1000);
      v_subject              VARCHAR2 (1000);
      v_Mail_Host            VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
      v_Mail_Conn            UTL_SMTP.Connection;
      crlf                   VARCHAR2 (2) := CHR (13) || CHR (10);
      vctr                   NUMBER := 0;
      vLine                  VARCHAR2 (32767);
      l_database             VARCHAR2 (100);
      l_email                VARCHAR2 (100);
      l_file_name            VARCHAR2 (1000);
      v_file_name            VARCHAR2 (1000);
      l_limit                NUMBER := 0;
      l_Tolerance            NUMBER := 0;
      l_total_stage_record   NUMBER := 0;
   BEGIN
      BEGIN
         SELECT   Name INTO l_database FROM v$database;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_database := NULL;
            DBMS_OUTPUT.put_line ('Error in data base fetch:' || SQLERRM);
      END;

      BEGIN
         SELECT   meaning
           INTO   l_email
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATE_EMAIL_NOTIFY'
                  AND enabled_flag = 'Y'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_email := NULL;
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Error while fetching email address:' || SQLERRM
            );
      END;

      BEGIN
         SELECT   meaning
           INTO   l_limit
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATES_LIMIT'
                  AND enabled_flag = 'Y'
                  AND lookup_code = 'LIMIT'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_limit := 0;
      END;

      BEGIN
         SELECT   meaning
           INTO   l_Tolerance
           FROM   fnd_common_lookups
          WHERE       lookup_type LIKE 'RGGL_DAILY_RATES_LIMIT'
                  AND enabled_flag = 'Y'
                  AND lookup_code = 'TOLERANCE'
                  AND NVL (end_date_active, SYSDATE) >= SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_Tolerance := 0;
      END;

      BEGIN
         SELECT   COUNT (1)
           INTO   l_total_stage_record
           FROM   RGGl_daily_rate_stg
          WHERE   rec_status = 'N' --AND TRUNC(creation_date) = TRUNC(sysdate)
                                  ;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_total_stage_record := 0;
      END;


      v_subject :=
         'GL Daily Rate Interface' || '-' || '[' || l_database || ']';
      v_Recipient := l_email;


      v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
      UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
      UTL_SMTP.Mail (v_Mail_Conn, v_From);
      UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

      UTL_SMTP.Data (
         v_Mail_Conn,
            --   'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf
            'From: '
         || v_From
         || crlf
         || 'Subject: '
         || v_Subject
         || crlf
         || 'To: '
         || v_Recipient
         || crlf
         || 'MIME-Version: 1.0'
         || crlf
         ||                                          -- Use MIME mail standard
           'Content-Type: multipart/mixed;'
         || crlf
         || ' boundary="-----SECBOUND"'
         || crlf
         || crlf
         || '-------SECBOUND'
         || crlf
         || 'Content-Type: text/html;'
         || crlf
         || 'Content-Transfer_Encoding: 7bit'
         || crlf
         || crlf
         || 'GL Daily Rate interface to create exchange rates ran as per schedule.<p>'
         || 'Total number of records in stage table is not within <strong> allowed limit.  </strong> Please validate records if required adjust record allowed or tolerance limit and run interface manually. <p>'
         || crlf
         || 'Record Limit                 :'
         || l_limit
         || '.<p>'
         || crlf
         || 'Tolerance Limit(%)             :'
         || l_Tolerance
         || '. <p>'
         || crlf
         || 'Total records in staging table :'
         || l_total_stage_record
         || '.<p>'
         || CHR (10)
         || ' '
         || '<br>'
         || '<br>'
         ||                                                    -- Message body
           'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
         || crlf
         || '-------SECBOUND--'                               -- End MIME mail
      );
      UTL_SMTP.Quit (v_mail_conn);
   EXCEPTION
      WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
      THEN
         raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
   END RGGL_SEND_LIMIT_MAIL;


   -- This procedure submits standard rate impot program
   PROCEDURE SUBMIT_RG_DAILY_IMPORT
   IS
      l_REQUEST_ID               NUMBER;
      l_APPLICATION_SHORT_NAME   VARCHAR2 (50);
      l_sqlerrm                  VARCHAR2 (1000);
      v_cnt                      NUMBER;
      V_REQUEST_STATUS           BOOLEAN;
      V_PHASE                    VARCHAR2 (4000);
      V_STATUS                   VARCHAR2 (4000);
      V_DEV_PHASE                VARCHAR2 (4000);
      V_DEV_STATUS               VARCHAR2 (4000);
      V_MESSAGE                  VARCHAR2 (4000);
      l_count                    NUMBER;
   BEGIN
      SELECT   COUNT (1) INTO l_count FROM GL_DAILY_RATES_INTERFACE;

      IF l_count <> 0
      THEN
         l_REQUEST_ID :=
            fnd_request.submit_request (application   => 'SQLGL',
                                        program       => 'GLDRICCP',
                                        description   => NULL,
                                        start_time    => NULL,
                                        sub_request   => FALSE,
                                        argument1     => NULL);
         COMMIT;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Request Id:' || l_REQUEST_ID);

         V_REQUEST_STATUS :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (REQUEST_ID   => l_REQUEST_ID,
                                             INTERVAL     => 2,
                                             MAX_WAIT     => 0,
                                             PHASE        => V_PHASE,
                                             STATUS       => V_STATUS,
                                             DEV_PHASE    => V_DEV_PHASE,
                                             DEV_STATUS   => V_DEV_STATUS,
                                             MESSAGE      => V_MESSAGE);
      ELSE
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'No Records exists in GL Daily Rates Interface');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error in Import Program:' || SQLERRM);
   END SUBMIT_RG_DAILY_IMPORT;


   -- Main proedure registered as concurrent program
   PROCEDURE MAIN (ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER)
   IS
      l_count             NUMBER := 0;
      l_tot_error_count   NUMBER := 0;
      l_int_count         NUMBER := 0;
      l_error_count       NUMBER := 0;
      l_stg_cnt           NUMBER := 0;
      l_Tolerance         NUMBER := 0;
      l_limit             NUMBER := 0;
      l_upper_limit       NUMBER := 0;
      l_lower_limit       NUMBER := 0;
      RGGL_EXCEPTION EXCEPTION;
   BEGIN
      FND_FILE.PUT_LINE (FND_FILE.LOG, 'Starting MAIN Process');

      BEGIN
         SELECT   COUNT (1)
           INTO   l_stg_cnt
           FROM   RGGl_daily_rate_stg
          WHERE   1 = 1                --TRUNC(creation_date) = TRUNC(sysdate)
                       AND rec_status = 'N';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_stg_cnt := 0;
      END;

      FND_FILE.PUT_LINE (FND_FILE.LOG,
                         'Total new stage record: ' || l_stg_cnt);

      IF l_stg_cnt = 0
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
            'No new reord in stage table to process in this run'
         );
         RGGL_SEND_NORECORDS_MAIL;
      ELSE
         BEGIN
            SELECT   NVL (meaning, 0)
              INTO   l_limit
              FROM   fnd_common_lookups
             WHERE       lookup_type LIKE 'RGGL_DAILY_RATES_LIMIT'
                     AND enabled_flag = 'Y'
                     AND lookup_code = 'LIMIT'
                     AND NVL (end_date_active, SYSDATE) >= SYSDATE;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_limit := 0;
         END;

         FND_FILE.PUT_LINE (FND_FILE.LOG, 'Total record Limit: ' || l_limit);

         BEGIN
            SELECT   NVL (meaning, 0)
              INTO   l_Tolerance
              FROM   fnd_common_lookups
             WHERE       lookup_type LIKE 'RGGL_DAILY_RATES_LIMIT'
                     AND enabled_flag = 'Y'
                     AND lookup_code = 'TOLERANCE'
                     AND NVL (end_date_active, SYSDATE) >= SYSDATE;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_Tolerance := 0;
         END;

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Tolerance allowed in (%): ' || l_Tolerance);

         l_upper_limit := l_limit + ROUND (l_limit * l_Tolerance / 100);
         l_lower_limit := l_limit - ROUND (l_limit * l_Tolerance / 100);

         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Total upper limit allowed: ' || l_upper_limit);
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Total lower limit allowed: ' || l_lower_limit);


         --IF  l_total_limit = l_stg_cnt THEN

         IF (l_stg_cnt <= l_upper_limit) AND (l_stg_cnt >= l_lower_limit)
         THEN
            FND_FILE.PUT_LINE (
               FND_FILE.OUTPUT,
               'Total record in stage table for processing is within Limit'
            );
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'Total record in stage table for processing is within Limit'
            );

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Calling rggl_archive_records procedure');
            rggl_archive_records;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Calling VALIDATE_STG procedure');
            VALIDATE_STG;

            FND_FILE.PUT_LINE (FND_FILE.LOG, 'Calling PROCESS_STG');
            PROCESS_STG;

            FND_FILE.PUT_LINE (FND_FILE.LOG,
                               'Calling SUBMIT_RG_DAILY_IMPORT');
            SUBMIT_RG_DAILY_IMPORT;



            BEGIN
               SELECT   COUNT (1)
                 INTO   l_error_count
                 FROM   RGGl_daily_rate_stg
                WHERE   TRUNC (creation_date) = TRUNC (SYSDATE)
                        AND rec_status = 'E';
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_error_count := 0;
            END;

            BEGIN
               SELECT   COUNT (1)
                 INTO   l_int_count
                 FROM   GL_DAILY_RATES_INTERFACE
                WHERE   ERROR_CODE IS NOT NULL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_int_count := 0;
            END;

            l_tot_error_count := l_int_count + l_error_count;

            IF l_tot_error_count = 0
            THEN
               RGGL_SEND_NOERRORS_MAIL;
            ELSE
               RGGL_SEND_ERROR_MAIL;
            END IF;
         ELSE
            FND_FILE.PUT_LINE (
               FND_FILE.LOG,
               'GL Daily Rate interface to create rates was aborted. Total number of records in stage table is not within allowed limit'
            );
            RGGL_SEND_LIMIT_MAIL;
            retcode := 2;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (FND_FILE.LOG,
                            'Error in Main Program:' || SQLERRM);
         DBMS_OUTPUT.put_line ('Error in Main Program:' || SQLERRM);
   END MAIN;



   PROCEDURE RGGL_DAILY_RATE_RTP (ERRBUF OUT VARCHAR2, RETCODE OUT NUMBER)
   IS
      CURSOR c
      IS
           SELECT   from_currency,
                    to_currency,
                    conversion_date,
                    c.user_conversion_type conversion_type,
                    conversion_rate,
                    rate_source_code,
                    user_name
             FROM   gl_daily_rates a, fnd_user b, gl_daily_conversion_types c
            WHERE       a.created_by = b.user_id
                    AND a.conversion_type = c.conversion_type
                    AND ROWNUM < 50001
                    AND TRUNC (a.creation_date) = TRUNC (SYSDATE)
                    AND TRUNC (a.conversion_date) >=
                          (SELECT   MIN (conversion_date) + 1
                             FROM   rggl_daily_rate_stg
                            WHERE   rec_status = 'S'
                                    AND NVL (attribute1, 'NR') != 'R') -- SANNE Added on 11/July/2016
         ORDER BY   conversion_date,
                    from_currency,
                    to_currency,
                    a.creation_date DESC;

      l_email     VARCHAR2 (3000) := NULL;
      l_req       NUMBER := 0;
      l_rec_cnt   NUMBER := 0;
   BEGIN
      fnd_file.put_line (fnd_file.OUTPUT, '<?xml version = "1.0"?>');
      fnd_file.put_line (fnd_file.OUTPUT, '<RGGL_DAILY_RATES>');

      FOR rec IN c
      LOOP
         fnd_file.put_line (fnd_file.OUTPUT, '<G_MAIN>');
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<FROM_CURRENCY><![CDATA['
            || rec.from_currency
            || ']]></FROM_CURRENCY>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<TO_CURRENCY><![CDATA['
            || rec.to_currency
            || ']]></TO_CURRENCY>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<CONVERSION_DATE><![CDATA['
            || rec.conversion_date
            || ']]></CONVERSION_DATE>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<CONVERSION_TYPE><![CDATA['
            || rec.conversion_type
            || ']]></CONVERSION_TYPE>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<CONVERSION_RATE><![CDATA['
            || rec.conversion_rate
            || ']]></CONVERSION_RATE>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
               '<RATE_SOURCE_CODE><![CDATA['
            || rec.rate_source_code
            || ']]></RATE_SOURCE_CODE>'
         );
         fnd_file.put_line (
            fnd_file.OUTPUT,
            '<USER_NAME><![CDATA[' || rec.user_name || ']]></USER_NAME>'
         );
         fnd_file.put_line (fnd_file.OUTPUT, '</G_MAIN>');
         l_rec_cnt := l_rec_cnt + 1;
      END LOOP;

      fnd_file.put_line (fnd_file.OUTPUT, '</RGGL_DAILY_RATES>');

      BEGIN
         SELECT   meaning
           INTO   l_email
           FROM   FND_LOOKUP_values
          WHERE   LOOKUP_TYPE = 'RG_DAILY_RATES_EMAIL' AND lookup_code = 1;
      EXCEPTION
         WHEN OTHERS
         THEN
            l_email := NULL;
      END;


      IF l_rec_cnt > 1
      THEN
         -- SANNE Added below update statement on 11/July/2016
         UPDATE   RGGl_daily_rate_stg
            SET   ATTRIBUTE1 = 'R'
          WHERE       1 = 1
                  AND rec_status = 'S'
                  AND TRUNC(CREATION_DATE) = TRUNC (SYSDATE);


         BEGIN
            l_req :=
               fnd_request.submit_request (
                  application   => 'RGGL',
                  program       => 'RGGL_DAILY_RATES_RPT_SHELL',
                  description   => 'Riot Daily Rates Success Report Email Program',
                  start_time    => SYSDATE,
                  sub_request   => FALSE,
                  argument1     => fnd_global.CONC_REQUEST_ID,
                  argument2     => l_email
               );

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_req := 0;
         END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'Error while executing Success Report Procedure : '
            || SQLCODE
            || '-'
            || SQLERRM
         );
   END RGGL_DAILY_RATE_RTP;


   PROCEDURE RGGL_CP_WAIT (P_REQ_ID NUMBER)
   IS
      L_PHASE_CODE   VARCHAR2 (300) := NULL;
   BEGIN
      LOOP
         BEGIN
            SELECT   PHASE_CODE
              INTO   L_PHASE_CODE
              FROM   fnd_concurrent_requests
             WHERE   request_id = P_REQ_ID;                          --652548;
         EXCEPTION
            WHEN OTHERS
            THEN
               l_PHASE_CODE := 'E';
         END;

         EXIT WHEN (l_PHASE_CODE IN ('C', 'I', 'E'));
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         FND_FILE.PUT_LINE (
            FND_FILE.LOG,
               'Error while executing wait Procedure : '
            || SQLCODE
            || '-'
            || SQLERRM
         );
   END RGGL_CP_WAIT;
END RGGL_DAILY_RATES_PKG;
/


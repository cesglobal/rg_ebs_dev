CREATE OR REPLACE PACKAGE BODY APPS.rgce_ce_bank_load_pkg
AS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Package is Used to load data Cash Management Interface tables from files.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/

PROCEDURE SUBMIT_SHELL(p_request_id NUMBER) IS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Procedure is used to submit shell script to load data from files to staging table.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
        X_ERR_MSG            VARCHAR2 (4000);
        l_REQUEST_ID        NUMBER;
        l_APPLICATION_SHORT_NAME   VARCHAR2 (50);
        v_cnt                            number;
       V_REQUEST_STATUS           BOOLEAN;
       V_PHASE                    VARCHAR2 (4000);
       V_STATUS                   VARCHAR2 (4000);
       V_DEV_PHASE          VARCHAR2 (4000);
       V_DEV_STATUS         VARCHAR2 (4000);
       V_MESSAGE               VARCHAR2 (4000);
       l_module_name varchar2(50) := 'SUBMIT_SHELL';

    BEGIN
    X_ERR_MSG := NULL;
     rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Entered Into Shell Script Procedure');
     rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Entered Into Shell Script Procedure');

        l_REQUEST_ID := FND_REQUEST.SUBMIT_REQUEST ('RGCE',
                                     'RGCE_BANK_FILE_SQLDR',
                                     NULL,
                                     SYSDATE,
                                     FALSE,
                                     NULL,
                                     p_request_id
                                    );
         COMMIT;
      rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Shell Script Execution Completed with request id :'||l_request_id);
      rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Shell Script Execution Completed with request id :'||l_request_id);
        V_REQUEST_STATUS :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (REQUEST_ID      => l_REQUEST_ID,
                                             INTERVAL        => 2,
                                             MAX_WAIT        => 0,
                                             PHASE           => V_PHASE,
                                             STATUS          => V_STATUS,
                                             DEV_PHASE       => V_DEV_PHASE,
                                             DEV_STATUS      => V_DEV_STATUS,
                                             MESSAGE         => V_MESSAGE
                                            );


    EXCEPTION
        WHEN NO_DATA_FOUND THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error While Submitting Sql Loader:'||SQLERRM);
      rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Error While Submitting Sql Loader:'||SQLERRM);
      rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'FAIL to load data into Staging table');

         X_ERR_MSG :=  'FAIL to load data into Staging table';

    END SUBMIT_SHELL;


PROCEDURE REUSB_BANK_SUB_CAP (
   piv_file_name     IN   VARCHAR2,
   pin_request_id    IN   NUMBER,
   piv_file_path     IN   VARCHAR2,
   piv_load_option   IN   VARCHAR2,
   piv_map_temp      IN   VARCHAR2,
   piv_bank_br_num   IN   VARCHAR2,
   piv_accnt_num     IN   VARCHAR2,
   piv_gl_date       IN   VARCHAR2
   )AS
    /* -----------------------------------------------------------------------------------------------------------------
    REM    Author               : Srinivasa Ramuni
    REM    Purpose              : This Procedure is Used to load data into Cash Management Interface tables from staging.
    REM
    REM    Module               : RGCE Cash Management
    REM    Interface Tables     :
    REM    Modification History :
    REM       Date             Name             Version Number      Revision Summary
    REM ---------------------------------------------------------------------------------------------------------------
    REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
    REM --------------------------------------------------------------------------------------------------------------*/
        X_ERR_MSG                 VARCHAR2 (4000);
        l_REQUEST_ID               NUMBER;
        l_APPLICATION_SHORT_NAME   VARCHAR2 (50);
        l_sqlerrm                  VARCHAR2(1000);
        v_cnt                      number;
       V_REQUEST_STATUS           BOOLEAN;
       V_PHASE                    VARCHAR2 (4000);
       V_STATUS                   VARCHAR2 (4000);
       V_DEV_PHASE                VARCHAR2 (4000);
       V_DEV_STATUS               VARCHAR2 (4000);
       V_MESSAGE                  VARCHAR2 (4000);
       l_module_name varchar2(50) := 'REUSB_BANK_SUB_CAP';

    BEGIN
       DBMS_OUTPUT.put_line ('STATEMENT FILENAME :' || piv_file_name);
       fnd_file.put_line(fnd_file.log,'STATEMENT FILENAME :' || piv_file_name);
       rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'STATEMENT FILENAME :' || piv_file_name);
       DBMS_OUTPUT.put_line('  ****** START OF BANK STATEMENT LOADER PROGRAM STATUS ******');
       fnd_file.put_line(fnd_file.log,'  ****** START OF BANK STATEMENT LOADER PROGRAM STATUS ******');
       rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => '  ****** START OF BANK STATEMENT LOADER PROGRAM');

        BEGIN
          DBMS_OUTPUT.put_line('Submit the concurrent progam "Bank Statement Loader".');
          fnd_file.put_line(fnd_file.log,'Submit the concurrent progam "Bank Statement Loader".');
          rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Submit the concurrent progam "Bank Statement Loader".');

        rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Standrad Program Call');
        rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Bank Interface Standrad Program Call');
          l_REQUEST_ID :=
             fnd_request.submit_request (application      => 'CE',
                                         program          => 'CESQLLDR',
                                         description      => NULL,
                                         start_time       => NULL,
                                         sub_request      => FALSE,
                                         argument1        => piv_load_option,
                                         argument2        => piv_map_temp,
                                         argument3        => piv_file_name,
                                         argument4        => piv_file_path,
                                         argument5        => piv_bank_br_num,
                                         argument6        => piv_accnt_num,
                                         argument7        => TO_CHAR(TO_DATE(piv_gl_date,'DD-MON-RRRR'),'yyyy/mm/dd hh24:mi:ss'),
                                         argument8        => NULL,
                                         argument9        => NULL,
                                         argument10        => NULL,
                                         argument11       => NULL,
                                         argument12       => 'N',
                                         argument13       => NULL,
                                         argument14       => NULL
                                        );
          COMMIT;
          rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Standrad Program End');
          rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Bank Interface Standrad Program End');
           V_REQUEST_STATUS :=
                FND_CONCURRENT.WAIT_FOR_REQUEST (REQUEST_ID      => l_REQUEST_ID,
                                                 INTERVAL        => 2,
                                                 MAX_WAIT        => 0,
                                                 PHASE           => V_PHASE,
                                                 STATUS          => V_STATUS,
                                                 DEV_PHASE       => V_DEV_PHASE,
                                                 DEV_STATUS      => V_DEV_STATUS,
                                                 MESSAGE         => V_MESSAGE
                                                );
        END;

    EXCEPTION
       WHEN OTHERS THEN
       l_sqlerrm := SQLERRM;
          DBMS_OUTPUT.put_line ('  ' || 'OTHERS EXCEPTION' || SQLERRM (SQLCODE));
          fnd_file.put_line(fnd_file.log,'  ' || 'OTHERS EXCEPTION' || SQLERRM (SQLCODE));
           rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Standrad Error due to:'||l_sqlerrm);
           rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Bank Interface Standrad Error due to:'||l_sqlerrm);
    END REUSB_BANK_SUB_CAP;


PROCEDURE RGCE_ERROR_REPORT (ERRBUF OUT VARCHAR2,
                             RETCODE OUT NUMBER)
IS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Procedure is Used to build a report for error records.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
x_rec_val  NUMBER:= 0;
x_no_rec   VARCHAR2(10);

CURSOR C_CUR
IS
 SELECT  cse.statement_number
        ,'XXXXXX'||substr(cse.bank_account_num, 7) bank_account_num
        ,cse.creation_date
        ,substr(cse.message_text,1,instr(cse.message_text, 't'))||' XXXXXX'||substr(cse.message_text, instr(cse.message_text, 't')+7) message_text
 FROM RGCE_SQLLDR_ERRORS_STG cse
 WHERE cse.status = 'E';
/*SELECT cshi.statement_number
      ,cshi.bank_account_num
      ,cshi.statement_date
      ,cshi.bank_name
      ,cshi.bank_branch_name
      ,cse.message_text
 FROM CE_SQLLDR_ERRORS cse,
      ce_statement_headers_int cshi
 WHERE cse.statement_number = cshi.statement_number
   AND cse.status = 'E'
   --AND TRUNC(cse.creation_date) = Trunc(sysdate)
GROUP BY cshi.statement_number
         ,cshi.bank_account_num
         ,cshi.statement_date
         ,cshi.bank_name
         ,cshi.bank_branch_name
         ,cse.message_text;*/
BEGIN
  fnd_file.put_line (fnd_file.OUTPUT, '<?xml version = "1.0"?>');
  fnd_file.put_line (fnd_file.OUTPUT, '<G_MAIN>');
  FOR R_CUR IN C_CUR
  LOOP
  x_rec_val := x_rec_val + 1;
  fnd_file.put_line(fnd_file.log,'x_rec_val:'||x_rec_val);
  fnd_file.put_line (fnd_file.OUTPUT, '<G_DTLS_INFO>');
  fnd_file.put_line (fnd_file.OUTPUT, '<STATEMENT_NUMBER><![CDATA[' || r_cur.statement_number || ']]></STATEMENT_NUMBER>');
  fnd_file.put_line (fnd_file.OUTPUT, '<BANK_ACCOUNT_NUM><![CDATA[' || r_cur.bank_account_num || ']]></BANK_ACCOUNT_NUM>');
  --fnd_file.put_line (fnd_file.OUTPUT, '<STATEMENT_DATE><![CDATA[' || r_cur.statement_date || ']]></STATEMENT_DATE>');
  --fnd_file.put_line (fnd_file.OUTPUT, '<BANK_NAME><![CDATA[' || r_cur.bank_name || ']]></BANK_NAME>');
  fnd_file.put_line (fnd_file.OUTPUT, '<CREATION_DATE><![CDATA[' || r_cur.creation_date || ']]></CREATION_DATE>');
  fnd_file.put_line (fnd_file.OUTPUT, '<MESSAGE_TEXT><![CDATA[' || r_cur.message_text || ']]></MESSAGE_TEXT>');
  fnd_file.put_line (fnd_file.OUTPUT, '</G_DTLS_INFO>');

  END LOOP;
  IF x_rec_val = 0 THEN
        fnd_file.put_line (fnd_file.OUTPUT, 'No Data Found ...');
        SELECT 'Y' INTO x_no_rec FROM DUAL;
        fnd_file.put_line (fnd_file.OUTPUT, '<NO_REC>' || x_no_rec || '</NO_REC>');
        fnd_file.put_line (fnd_file.OUTPUT, '</G_MAIN>');
     ELSE
        fnd_file.put_line (fnd_file.OUTPUT, '</G_MAIN>');
        fnd_file.put_line (fnd_file.LOG, 'End of XML TAG Creation...');
  END IF;
EXCEPTION
  WHEN OTHERS THEN
  FND_FILE.PUT_LINE(FND_FILE.LOG,'Error While Writing the Report:'||SQLERRM);
END RGCE_ERROR_REPORT;
PROCEDURE SUBMIT_BANK_ERROR_PRC(x_request_id  OUT NUMBER)
IS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Procedure is Used to call xml template and submit report.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
X_ERR_MSG                 VARCHAR2 (4000);
l_REQUEST_ID               NUMBER;
l_APPLICATION_SHORT_NAME   VARCHAR2 (50);
v_cnt                      number;
      V_REQUEST_STATUS           BOOLEAN;
      V_PHASE                    VARCHAR2 (4000);
      V_STATUS                   VARCHAR2 (4000);
      V_DEV_PHASE                VARCHAR2 (4000);
      V_DEV_STATUS               VARCHAR2 (4000);
      V_MESSAGE                  VARCHAR2 (4000);
      v_layout                   BOOLEAN;

    BEGIN
    X_ERR_MSG := NULL;


       BEGIN

        fnd_file.put_line(fnd_file.log,'Submitting Bank Interface Error Report');
        rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Error Report XML Template');
        v_layout :=
         fnd_request.add_layout
                              (template_appl_name      => 'RGCE',
                               template_code           => 'RGCE_ERROR_REPORT',
                               template_language       => 'ENG',
                               template_territory      => NULL,
                               output_format           => 'EXCEL'
                              );
        rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Error Report Concurrent Program');

        l_REQUEST_ID := FND_REQUEST.SUBMIT_REQUEST ('RGCE',
                                     'RGCE_ERROR_REPORT',
                                     NULL,
                                     SYSDATE,
                                     FALSE
                                    );
         COMMIT;
         x_request_id := l_REQUEST_ID;
        V_REQUEST_STATUS :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (REQUEST_ID      => l_REQUEST_ID,
                                             INTERVAL        => 2,
                                             MAX_WAIT        => 0,
                                             PHASE           => V_PHASE,
                                             STATUS          => V_STATUS,
                                             DEV_PHASE       => V_DEV_PHASE,
                                             DEV_STATUS      => V_DEV_STATUS,
                                             MESSAGE         => V_MESSAGE
                                            );
        EXCEPTION
            WHEN OTHERS THEN
            FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while submitting Bank Interface Error Report:'||SQLERRM);
        END;


    EXCEPTION
        WHEN NO_DATA_FOUND
      THEN
         X_ERR_MSG :=
            'RGCE BANK INTERFACE ERROR Report';
END SUBMIT_BANK_ERROR_PRC;
PROCEDURE RGCE_BANL_INT_ERR_MAIL(p_request_id    NUMBER,
                                 p_email_address VARCHAR2)
IS
/* -----------------------------------------------------------------------------------------------------------------
REM    Author               : Srinivasa Ramuni
REM    Purpose              : This Procedure is Used to send an email notification after completion of Error Report.
REM
REM    Module               : RGCE Cash Management
REM    Interface Tables     :
REM    Modification History :
REM       Date             Name             Version Number      Revision Summary
REM ---------------------------------------------------------------------------------------------------------------
REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
REM --------------------------------------------------------------------------------------------------------------*/
X_ERR_MSG                 VARCHAR2 (4000);
l_REQUEST_ID               NUMBER;
l_APPLICATION_SHORT_NAME   VARCHAR2 (50);
v_cnt                      number;
      V_REQUEST_STATUS           BOOLEAN;
      V_PHASE                    VARCHAR2 (4000);
      V_STATUS                   VARCHAR2 (4000);
      V_DEV_PHASE                VARCHAR2 (4000);
      V_DEV_STATUS               VARCHAR2 (4000);
      V_MESSAGE                  VARCHAR2 (4000);

    BEGIN
    X_ERR_MSG := NULL;

    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Email Request');

        l_REQUEST_ID := FND_REQUEST.SUBMIT_REQUEST ('RGCE',
                                     'RGCEBANKINTSENDMAIL',
                                     NULL,
                                     SYSDATE,
                                     FALSE,
                                     --NULL,
                                     p_request_id,
                                     p_email_address
                                    );
         COMMIT;
        V_REQUEST_STATUS :=
            FND_CONCURRENT.WAIT_FOR_REQUEST (REQUEST_ID      => l_REQUEST_ID,
                                             INTERVAL        => 2,
                                             MAX_WAIT        => 0,
                                             PHASE           => V_PHASE,
                                             STATUS          => V_STATUS,
                                             DEV_PHASE       => V_DEV_PHASE,
                                             DEV_STATUS      => V_DEV_STATUS,
                                             MESSAGE         => V_MESSAGE
                                            );


    EXCEPTION
        WHEN NO_DATA_FOUND
      THEN
         X_ERR_MSG :=
            'RGCE Bank Int Error Mail Fail';
END RGCE_BANL_INT_ERR_MAIL;
PROCEDURE RGCE_ERRORS_STG
  IS

  CURSOR CUR
  IS
  SELECT *
    FROM CE_SQLLDR_ERRORS
   WHERE bank_account_num NOT IN( SELECT bank_account_num
                                    FROM RGCE_SQLLDR_ERRORS_STG);
BEGIN
  FOR REC IN CUR
  LOOP
      BEGIN
          INSERT INTO RGCE_SQLLDR_ERRORS_STG
          ( STATEMENT_NUMBER,
            BANK_ACCOUNT_NUM,
            REC_NO,
            MESSAGE_TEXT,
            STATUS,
            CREATED_BY,
            CREATION_DATE,
            LAST_UPDATED_BY,
            LAST_UPDATE_DATE,
            LAST_UPDATE_LOGIN,
            BANK_ACCT_CURRENCY_CODE)
            VALUES
           ( REC.STATEMENT_NUMBER,
             REC.BANK_ACCOUNT_NUM,
             REC.REC_NO,
             REC.MESSAGE_TEXT,
             REC.STATUS,
             REC.CREATED_BY,
             REC.CREATION_DATE,
             REC.LAST_UPDATED_BY,
             REC.LAST_UPDATE_DATE,
             REC.LAST_UPDATE_LOGIN,
             REC.BANK_ACCT_CURRENCY_CODE);
      EXCEPTION
      WHEN OTHERS THEN
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error While inserting data into error staging table:'||SQLERRM);
      DBMS_OUTPUT.PUT_LINE('Error While inserting data into error staging table:'||SQLERRM);
      END;
  END LOOP;
  COMMIT;
EXCEPTION
WHEN OTHERS THEN
FND_FILE.PUT_LINE(FND_FILE.LOG,'Error In RGCE_ERRORS_STG Procedure:'||SQLERRM);
DBMS_OUTPUT.PUT_LINE('Error In RGCE_ERRORS_STG Procedure:'||SQLERRM);
END RGCE_ERRORS_STG;

PROCEDURE MAIN ( ERRBUF  OUT VARCHAR2,
                                         RETCODE OUT NUMBER,
                                         p_process_id IN VARCHAR2,
                                         p_Mapping_id IN VARCHAR2) IS
        /* -----------------------------------------------------------------------------------------------------------------
        REM    Author               : Srinivasa Ramuni
        REM    Purpose              : This Procedure is Used to call multiple procedures in sequence order.
        REM
        REM    Module               : RGCE Cash Management
        REM    Interface Tables     :
        REM    Modification History :
        REM       Date             Name             Version Number      Revision Summary
        REM ---------------------------------------------------------------------------------------------------------------
        REM    29-Sep-2015     Srinivasa Ramuni          1.0             Created.
        REM --------------------------------------------------------------------------------------------------------------*/
        l_request_id      fnd_concurrent_requests.request_id%type := fnd_global.conc_request_id;
        l_succ_cnt        NUMBER := 0;
        l_err_cnt         NUMBER := 0;
        l_org_count       NUMBER := 0;
        l_comp_count      NUMBER := 0;
        l_error_count     NUMBER := 0;
        l_file_count      NUMBER := 0;
        l_Insertfile_cnt  NUMBER := 0;
        l_rep1_request_id fnd_concurrent_requests.request_id%type;
        l_rep2_request_id fnd_concurrent_requests.request_id%type;
        l_directory_path  VARCHAR2(100);
        l_database_name   VARCHAR2(30);
        l_sqlerrm         VARCHAR2(3000);
        l_variable       fnd_env_context.variable_name%type;
        l_value           fnd_env_context.value%type;
        l_variable1    fnd_env_context.variable_name%type;
        l_value1         fnd_env_context.value%type;
        l_email          VARCHAR2(1000);
        l_error_email_count NUMBER:=0;
        NO_FILE_EXISTS      EXCEPTION;
        l_ines_count        NUMBER:=0;
        l_count_1           NUMBER:=0;
        l_count_2           NUMBER:=0;
        l_count_3           NUMBER:=0;
        l_count_4           NUMBER:=0;

        l_module_name varchar2(50) := 'main';

        Cursor c_cur(p_request_id NUMBER) IS
        SELECT file_name filename,file_path
            FROM RGCE_STMT_INT_TMP
                WHERE request_id = p_request_id
                GROUP BY file_name,file_path;

        CURSOR c_orgcur IS
        SELECT cbaua.org_id actual_org,
               cba.bank_account_id,
               cshi.org_id interface_org_id,
               cshi.bank_account_num bank_account
          FROM ce_bank_acct_uses_all cbaua,
               ce_bank_accounts cba,
               ce_statement_headers_int cshi,
               ce_bank_branches_v cbbv
         WHERE cbaua.bank_account_id = cba.bank_account_id
           AND cba.bank_account_num  = cshi.bank_account_num
           AND cbbv.branch_party_id  = cba.bank_branch_id
           AND cbbv.bank_name        = cshi.bank_name
           AND cbbv.BANK_BRANCH_NAME = cshi.bank_branch_name
           AND cshi.record_status_flag <>  'T'
           AND cshi.org_id <> cbaua.org_id
          GROUP BY cbaua.org_id,cba.bank_account_id,cshi.org_id,cshi.bank_account_num;

    -- Added below query to remove leading zero's in lines interface table on bank_trx_number coloumn.
        CURSOR curlines(p_statement_num VARCHAR2)
        IS
        SELECT  bank_account_num,statement_number,line_number,trx_code,bank_trx_number
          FROM ce_statement_lines_interface
         WHERE statement_number = p_statement_num
           AND bank_trx_number LIKE '0%'
      ORDER BY line_number;

        CURSOR curheaders
        IS
        SELECT b.statement_number
          FROM ce_statement_headers_int a,
               ce_statement_lines_interface b
         WHERE a.statement_number = b.statement_number
           --AND b.bank_trx_number LIKE '0%'
      GROUP BY b.statement_number;


BEGIN

    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Program Started');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Starting Bank Interface Program');
    EXECUTE IMMEDIATE 'Truncate Table RGCE.RGCE_STMT_INT_TMP';

    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Header Shell script Request Id:'||l_request_id);
    fnd_file.put_line(fnd_file.log,'Request Id:'||l_request_id);
    fnd_file.put_line(fnd_file.log,'Header Shell Script Started:'||l_request_id);
    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Shell Script for request id:'||l_request_id);

    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Calling SUBMIT_SHELL');

    SUBMIT_SHELL(p_request_id => l_request_id);
    COMMIT;
    dbms_lock.sleep(2);
    BEGIN
     SELECT count(1)
       INTO l_Insertfile_cnt
       FROM RGCE_STMT_INT_TMP
      WHERE request_id = l_request_id;
     -- GROUP BY file_name,file_path;
    EXCEPTION
        WHEN OTHERS THEN
        l_Insertfile_cnt := 0;
    END;

    IF l_Insertfile_cnt = 0
    THEN
    -- If no file placed in Inbound directory  it will send mail saying same thing.
    RGCE_SEND_NOFILE_MAIL;
    RAISE NO_FILE_EXISTS;
    END IF;

    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Standard Bank Loader Program');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Calling Standard Bank Loader Program');

    FOR r_cur IN c_cur(l_request_id)
    LOOP
            BEGIN
                REUSB_BANK_SUB_CAP (
                piv_file_name     => r_cur.filename,
                pin_request_id    => NULL,
                piv_file_path     => r_cur.file_path, --l_directory_path,
                piv_load_option   => p_process_id,
                piv_map_temp      => p_Mapping_id,
                piv_bank_br_num   => NULL,
                piv_accnt_num     => NULL,
                piv_gl_date       => SYSDATE);

            COMMIT;

            EXCEPTION
                WHEN OTHERS THEN
                l_sqlerrm := SQLERRM;
                rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Error While submitting Program for File name :'||r_cur.filename||'ERROR:'||l_sqlerrm);
                rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message =>  'Error While submitting Program for File name :'||r_cur.filename||'ERROR:'||l_sqlerrm);

            END;
    dbms_lock.sleep(2);
    END LOOP;

     SELECT  count(distinct file_name)
       INTO  l_file_count
            FROM RGCE_STMT_INT_TMP
            WHERE request_id = l_request_id;
            FND_FILE.PUT_LINE(FND_FILE.LOG,'File Name for this request id :'||l_file_count);
            rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'File Name for this request id :'||l_file_count);

    <<MAIN>>
    SELECT COUNT(1)
      INTO l_comp_count
      FROM fnd_concurrent_requests
     WHERE parent_request_id IN ( SELECT request_id
                                    FROM fnd_concurrent_requests
                                   WHERE parent_request_id = l_request_id)
                                     AND concurrent_program_id IN ( SELECT concurrent_program_id
                                                                      FROM fnd_concurrent_programs
                                                                     WHERE concurrent_program_name = 'CEBSLDR')
     and status_code = 'C';

    SELECT COUNT(1)
      INTO l_error_count
      FROM fnd_concurrent_requests
     WHERE parent_request_id IN ( SELECT request_id
                                    FROM fnd_concurrent_requests
                                   WHERE parent_request_id = l_request_id)
                                     AND concurrent_program_id IN ( SELECT concurrent_program_id
                                                                      FROM fnd_concurrent_programs
                                                                     WHERE concurrent_program_name = 'CEBSLDR')
     and status_code = 'E';

    IF (l_comp_count + l_error_count) <> l_file_count THEN

    GOTO MAIN;

    ELSE

    FND_FILE.PUT_LINE(FND_FILE.LOG,'Concurrent Program count :'||l_comp_count);
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Concurrent Program count :'||l_comp_count);
    SELECT count(1)
      INTO l_org_count
      FROM ce_bank_acct_uses_all cbaua,
           ce_bank_accounts cba,
           ce_statement_headers_int cshi,
           ce_bank_branches_v cbbv
     WHERE cbaua.bank_account_id = cba.bank_account_id
       AND cba.bank_account_num  = cshi.bank_account_num
       AND cbbv.branch_party_id  = cba.bank_branch_id
       AND cbbv.bank_name        = cshi.bank_name
       AND cbbv.BANK_BRANCH_NAME = cshi.bank_branch_name
       AND cshi.record_status_flag <> 'T'
       AND cshi.org_id <> cbaua.org_id;

    FND_FILE.PUT_LINE(FND_FILE.LOG,'Different Org id exists for bank accounts:'||l_org_count);
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Different Org id exists for bank accounts:'||l_org_count);
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Updating org id to bank account org id in interface table');

        IF l_org_count > 0 THEN
              BEGIN
                      FOR R_ORGCUR IN C_ORGCUR
                      LOOP

                      UPDATE ce_statement_headers_int
                        SET org_id = R_ORGCUR.actual_org
                        WHERE record_status_flag <> 'T'
                        AND bank_account_num = R_ORGCUR.bank_account;

                    END LOOP;
                COMMIT;

              EXCEPTION
              WHEN OTHERS THEN
              DBMS_OUTPUT.PUT_LINE('Error while updating org on Interface table :'||SQLERRM);
              fnd_file.put_line(fnd_file.log,'Error while updating org on Interface table :'||SQLERRM);
              rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Error while updating org on Interface table :'||SQLERRM);
              END;

        END IF;
        -- Start by Mahipal on 17-Feb-2016 to remove leading Zero on bank trx number columns
        FOR recheaders IN curheaders
        LOOP
            BEGIN
                SELECT  count(1)
                  INTO  l_ines_count
                 FROM ce_statement_lines_interface
                WHERE statement_number = recheaders.statement_number
                  AND bank_trx_number LIKE '0%' ;
            EXCEPTION
                WHEN OTHERS THEN
                l_ines_count := 0;
                FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching the lines count:'||SQLERRM);
            END;
                IF l_ines_count <> 0
                THEN
                    FOR reclines IN curlines(recheaders.statement_number)
                    LOOP
                        UPDATE ce_statement_lines_interface
                           SET bank_trx_number  = trim(leading '0' from reclines.bank_trx_number)
                         WHERE bank_account_num = reclines.bank_account_num
                           AND statement_number = reclines.statement_number
                           AND line_number      = reclines.line_number
                           AND trx_code         = reclines.trx_code
                           AND bank_trx_number  = reclines.bank_trx_number;
                        COMMIT;
                    END LOOP;
                END IF;
        END LOOP;
        -- Ended by Mahipal on 17-Feb-2016.

    END IF;

    SELECT count(1)
      INTO l_Err_cnt
      FROM CE_SQLLDR_ERRORS cse,
           ce_statement_headers_int cshi
     WHERE cse.statement_number = cshi.statement_number
       AND cse.status = 'E';
       --AND TRUNC(cse.creation_date) = Trunc(sysdate);

    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Program Error count:'||l_Err_cnt);
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Bank Interface Program Error count:'||l_Err_cnt);
    FND_FILE.PUT_LINE(FND_FILE.LOG,'Bank Statement Error Records:'||l_Err_cnt);

    SELECT COUNT(1)
      INTO l_error_email_count
      FROM fnd_concurrent_requests
     WHERE parent_request_id IN ( SELECT request_id
                                    FROM fnd_concurrent_requests
                                   WHERE parent_request_id = l_request_id)
                                     AND concurrent_program_id IN ( SELECT concurrent_program_id
                                                                      FROM fnd_concurrent_programs
                                                                     WHERE concurrent_program_name = 'CEBSLDR')
     and status_code = 'E';

    IF l_error_email_count <> 0
    THEN
     RGCE_PROG_ERROR_MAIL(l_request_id);
    END IF;

    IF l_Err_cnt > 0
    THEN
      RGCE_ERRORS_STG; -- This procedure is called to load error details into error staging table for reporting purpose.
    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;
   -- rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Error Report');
   -- rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Before Calling Bank Interface Error Report');
   -- SUBMIT_BANK_ERROR_PRC(x_request_id  => l_rep2_request_id);
    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Error Report EMAIL Call');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Before Calling Bank Interface Error Report EMAIL Call');
   -- RGCE_BANL_INT_ERR_MAIL(p_request_id => l_rep2_request_id, p_email_address => l_email); --'ext.mardam@riotgames.com');
    RGCE_SEND_ERROR_MAIL(l_request_id);
    ELSE
    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Before Calling Bank Interface Scuccess Report EMAIL Call');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Before Calling Bank Interface Success Report EMAIL Call');
    RGCE_SEND_SUCCESS_MAIL(l_request_id);
    END IF;
    -- Started for Import Error Report
    BEGIN
        SELECT count(1)
          INTO l_count_1
          FROM CE_STATEMENT_HEADERS csh,
               CE_STATEMENT_LINES   csl,
               CE_RECONCILIATION_ERRORS cre,
               CE_BANK_ACCOUNTS cba,
               CE_BANK_BRANCHES_V cbbv,
               FND_NEW_MESSAGES fnm
         WHERE csh.statement_header_id        = csl.statement_header_id
           AND csh.statement_header_id        = cre.statement_header_id
           AND csl.statement_line_id          = cre.statement_line_id
           AND csh.bank_account_id            = cba.bank_account_id
           AND cbbv.branch_party_id           = cba.bank_branch_id
           AND cre.message_name               = fnm.message_name
           AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE);

        SELECT count(1)
          INTO l_count_2
          FROM CE_STATEMENT_HEADERS csh,
               CE_RECONCILIATION_ERRORS cre,
               CE_BANK_ACCOUNTS cba,
               CE_BANK_BRANCHES_V cbbv,
               FND_NEW_MESSAGES fnm
         WHERE csh.statement_header_id        = cre.statement_header_id
           AND csh.bank_account_id            = cba.bank_account_id
           AND cbbv.branch_party_id           = cba.bank_branch_id
           AND cre.message_name               = fnm.message_name
           AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE);

        SELECT count(1)
        INTO l_count_3
        FROM ce_statement_headers_int csh,
             ce_header_interface_errors cre,
             CE_BANK_ACCOUNTS cba,
             CE_BANK_BRANCHES_V cbbv,
             FND_NEW_MESSAGES fnm
       WHERE csh.statement_number           = cre.statement_number
         AND csh.bank_account_num            = cba.bank_account_num
         AND cbbv.branch_party_id           = cba.bank_branch_id
         AND cre.message_name               = fnm.message_name
         AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE);


        SELECT count(1)
          INTO l_count_4
          FROM ce_statement_headers csh,
               ce_header_interface_errors cre,
               CE_BANK_ACCOUNTS cba,
               CE_BANK_BRANCHES_V cbbv,
               FND_NEW_MESSAGES fnm
         WHERE csh.statement_number           = cre.statement_number
           AND csh.bank_account_id            = cba.bank_account_id
           AND cbbv.branch_party_id           = cba.bank_branch_id
           AND cre.message_name               = fnm.message_name
           AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE);
    EXCEPTION
        WHEN OTHERS THEN
        FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching Error count after Import Program:'||SQLERRM);
    END;

    IF  (l_count_1 <> 0 OR l_count_2 <> 0 OR l_count_3 <> 0 OR l_count_4 <> 0)
    THEN

        RGCE_SEND_IMPORTERROR_MAIL(l_request_id);
    END IF;
    -- Ended for Import Error Report
    rgcomn_debug_pkg.log_cp_message(P_LEVEL => 0, P_MESSAGE => 'Bank Interface Program Completed.');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Bank Interface Program Completed.');

EXCEPTION
    WHEN NO_FILE_EXISTS THEN
    DBMS_OUTPUT.PUT_LINE('No Files Exists in Inbound Directory to Process');
    fnd_file.put_line(fnd_file.log,'No Files Exists in Inbound Directory to Process');
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'No Files Exists in Inbound Directory to Process');
    WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error in Main :'||SQLERRM);
    fnd_file.put_line(fnd_file.log,'Error in Main :'||SQLERRM);
    rgcomn_debug_pkg.log_message(p_level => 0, p_program_name => l_module_name, p_message => 'Error in Main :'||SQLERRM);
END MAIN;


-- Added by Mahipal on 03-FEB-16.
PROCEDURE RGCE_SEND_ERROR_MAIL(p_request_id NUMBER)
IS
   v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
   v_Recipient   VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
   v_subject     VARCHAR2 (1000);
   v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   v_Mail_Conn   UTL_SMTP.Connection;
   crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
   vctr          NUMBER := 0;
   vLine         VARCHAR2 (32767);
   l_database    VARCHAR2(100);
   l_email       VARCHAR2(100);
   l_file_name   VARCHAR2(1000);
   v_file_name   VARCHAR2(1000);

CURSOR c
IS
 SELECT  cse.statement_number
        ,'XXXXXX'||substr(cse.bank_account_num, 7) bank_account_num
        ,cse.creation_date
        ,substr(cse.message_text,1,instr(cse.message_text, 't'))||' XXXXXX'||substr(cse.message_text, instr(cse.message_text, 't')+7) message_text
 FROM RGCE_SQLLDR_ERRORS_STG cse
 WHERE cse.status = 'E';

    Cursor c_cur(v_request_id NUMBER) IS
        SELECT file_name filename
          FROM RGCE_STMT_INT_TMP
         WHERE request_id = v_request_id
      GROUP BY file_name;

 BEGIN
    BEGIN
        Select Name
          INTO l_database
          FROM v$database;
    EXCEPTION
        WHEN OTHERS THEN
        l_database := NULL;
        dbms_output.put_line('Error in data base fetch:'||SQLERRM);
    END;

    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;

    l_file_name := NULL;

    FOR rec IN c_cur(p_request_id)
    LOOP

--    l_file_name := rec.filename ||chr(10)|| chr(9)|| chr(9)|| l_file_name;
  l_file_name := rec.filename ||'<p>'|| l_file_name;

    END LOOP;


 v_subject := 'Bank Statement Interface Report'||'-'||'['||l_database||']';
 v_Recipient := l_email;
 v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);

            vLine := 'Statement Number, Bank Account Number, Creation Date, Message Text';
    FOR crec IN c
    LOOP
        BEGIN

            IF LENGTH (vLine) > 32767
            THEN
                vLine := vLine;
            ELSE
                vLine :=
                    vLine
                || CHR (10)
                || crec.statement_number
                || ','
                || crec.bank_account_num
                || ','
                || crec.creation_date
                || ','
                || crec.message_text;
        END IF;
        EXCEPTION
         WHEN OTHERS
         THEN
            IF (SQLCODE = -06502)
            THEN
               vLine := vLine;
               v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
               UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
               UTL_SMTP.Mail (v_Mail_Conn, v_From);
               UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

            UTL_SMTP.
               Data (
                  v_Mail_Conn,
                   --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
                   'From: '
                  || v_From
                  || crlf
                  || 'Subject: '
                  || v_Subject
                  || crlf
                  || 'To: '
                  || v_Recipient
                  || crlf
                  || 'MIME-Version: 1.0'
                  || crlf
                  ||                                 -- Use MIME mail standard
                    'Content-Type: multipart/mixed;'
                  || crlf
                  || ' boundary="-----SECBOUND"'
                  || crlf
                  || crlf
                  || '-------SECBOUND'
                  || crlf
                  || 'Content-Type: text/html;'
                  || crlf
                  || 'Content-Transfer_Encoding: 7bit'
                  || crlf
                  || crlf
                  || 'Bank statement interface to import statement file(s) received from bank(s) completed successfully.<p>'
                     || crlf
                     ||'Statements are posted in interface table and is ready for Reconciliation into Oracle Cash Management system.<p>'
                     || crlf
                     ||'Some of the statements could not be imported. Attached please find Error/Exception Report for review and necessary action. <p>'
                     || chr(10)
                  || ' '
                  ||'<br>'
                  ||'<br>'
                  || v_file_name
                  || '<p>'
                  || chr(10)
                  || crlf
                  ||' '
                  ||'<br>'
                  ||'<br>'
                  ||                                           -- Message body
                    'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
                  || crlf
                  || crlf
                  || '-------SECBOUND'
                  || crlf
                  || 'Content-Type: text/plain;'
                  || crlf
                  || ' name="excel.csv"'
                  || crlf
                  || 'Content-Transfer_Encoding: 8bit'
                  || crlf
                  || 'Content-Disposition: attachment;'
                  || crlf
                  || ' filename="Bank_Statement_Error_Report.csv"'
                  || crlf
                  || crlf
                  || vLine
                  || crlf
                  ||                                  -- Content of attachment
                    crlf
                  || '-------SECBOUND--'                      -- End MIME mail
                                        );
                vLine := 'Statement Number, Bank Account Number, Creation Date, Message Text';
               UTL_SMTP.Quit (v_mail_conn);
            END IF;
        END;
    END LOOP;

    v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
   UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
   UTL_SMTP.Mail (v_Mail_Conn, v_From);
   UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

   UTL_SMTP.
   Data (
      v_Mail_Conn,
       --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
       'From: '
      || v_From
      || crlf
      || 'Subject: '
      || v_Subject
      || crlf
      || 'To: '
      || v_Recipient
      || crlf
      || 'MIME-Version: 1.0'
      || crlf
      ||                                             -- Use MIME mail standard
        'Content-Type: multipart/mixed;'
      || crlf
      || ' boundary="-----SECBOUND"'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/html;'
      || crlf
      || 'Content-Transfer_Encoding: 7bit'
      || crlf
      || crlf
      || 'Bank statement interface to import statement file(s) received from bank(s) completed successfully.<p>'
      || crlf
      ||'Statements are posted in interface table and is ready for Reconciliation into Oracle Cash Management system.<p>'
      || crlf
      ||'Some of the statements could not be Imported. Attached please find Error/Exception Report for review and necessary action.<p>'
      || chr(10)
      || ' '
      ||'<br>'
      ||'<br>'
      || v_file_name
      || '<p>'
      || chr(10)
      || crlf
      ||' '
      ||'<br>'
      ||'<br>'
      ||                                                       -- Message body
        'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/plain;'
      || crlf
      || ' name="excel.csv"'
      || crlf
      || 'Content-Transfer_Encoding: 8bit'
      || crlf
      || 'Content-Disposition: attachment;'
      || crlf
      || ' filename="Bank_Statement_Error_Report.csv"'
      || crlf
      || crlf
      || vLine
      || crlf
      ||                                              -- Content of attachment
        crlf
      || '-------SECBOUND--'                                  -- End MIME mail
                            );
   UTL_SMTP.Quit (v_mail_conn);
EXCEPTION
   WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
   THEN
      raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
END RGCE_SEND_ERROR_MAIL;

PROCEDURE RGCE_SEND_IMPORTERROR_MAIL(p_request_id NUMBER)
IS
   v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
   v_Recipient   VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
   v_subject     VARCHAR2 (1000);
   v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   v_Mail_Conn   UTL_SMTP.Connection;
   crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
   vctr          NUMBER := 0;
   vLine         VARCHAR2 (32767);
   l_database    VARCHAR2(100);
   l_email       VARCHAR2(100);
   l_file_name   VARCHAR2(1000);
   v_file_name   VARCHAR2(1000);

CURSOR c
IS
    SELECT csh.statement_number,
      -- csh.statement_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       (case when length(cba.bank_account_num) = 10
                       THEN 'XXXXXX'||substr(cba.bank_account_num, 7)
                       when length(cba.bank_account_num) = 9
                       then 'XXXXX'||substr(cba.bank_account_num, 6)
                       when length(cba.bank_account_num) > 10
                       then 'XXXXXXX'||substr(cba.bank_account_num, 8)
                       when length(cba.bank_account_num) < 9
                       then 'XXXXX'||substr(cba.bank_account_num, 5)
                  end)bank_account_num,
       cre.creation_date,
       --cba.bank_account_num,
       fnm.message_text
  FROM CE_STATEMENT_HEADERS csh,
       CE_STATEMENT_LINES   csl,
       CE_RECONCILIATION_ERRORS cre,
       CE_BANK_ACCOUNTS cba,
       CE_BANK_BRANCHES_V cbbv,
       FND_NEW_MESSAGES fnm
 WHERE csh.statement_header_id        = csl.statement_header_id
   AND csh.statement_header_id        = cre.statement_header_id
   AND csl.statement_line_id          = cre.statement_line_id
   AND csh.bank_account_id            = cba.bank_account_id
   AND cbbv.branch_party_id           = cba.bank_branch_id
   AND cre.message_name               = fnm.message_name
   AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE)
   GROUP BY csh.statement_number,
       --csh.statement_date,
       --csh.gl_date,
       cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       cba.bank_account_num,
       fnm.message_text
UNION ALL
SELECT csh.statement_number,
       --csh.statement_date,
       --cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       (case when length(cba.bank_account_num) = 10
                       THEN 'XXXXXX'||substr(cba.bank_account_num, 7)
                       when length(cba.bank_account_num) = 9
                       then 'XXXXX'||substr(cba.bank_account_num, 6)
                       when length(cba.bank_account_num) > 10
                       then 'XXXXXXX'||substr(cba.bank_account_num, 8)
                       when length(cba.bank_account_num) < 9
                       then 'XXXX'||substr(cba.bank_account_num, 5)
                  end)bank_account_num,
       cre.creation_date,
       --cba.bank_account_num,
       fnm.message_text
  FROM CE_STATEMENT_HEADERS csh,
       CE_RECONCILIATION_ERRORS cre,
       CE_BANK_ACCOUNTS cba,
       CE_BANK_BRANCHES_V cbbv,
       FND_NEW_MESSAGES fnm
 WHERE csh.statement_header_id        = cre.statement_header_id
   AND csh.bank_account_id            = cba.bank_account_id
   AND cbbv.branch_party_id           = cba.bank_branch_id
   AND cre.message_name               = fnm.message_name
   AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE)
   GROUP BY csh.statement_number,
       --csh.statement_date,
       --csh.gl_date,
       cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       cba.bank_account_num,
       fnm.message_text
UNION ALL
  SELECT csh.statement_number,
         --csh.statement_date,
         --cre.creation_date,
         --cbbv.bank_name,
         --cbbv.bank_branch_name,
         --cba.bank_account_name,
        (case when length(cba.bank_account_num) = 10
                       THEN 'XXXXXX'||substr(cba.bank_account_num, 7)
                       when length(cba.bank_account_num) = 9
                       then 'XXXXX'||substr(cba.bank_account_num, 6)
                       when length(cba.bank_account_num) > 10
                       then 'XXXXXXX'||substr(cba.bank_account_num, 8)
                       when length(cba.bank_account_num) < 9
                       then 'XXXX'||substr(cba.bank_account_num, 5)
                  end)bank_account_num,
         cre.creation_date,
         --cba.bank_account_num,
         fnm.message_text
  FROM ce_statement_headers_int csh,
       ce_header_interface_errors cre,
       CE_BANK_ACCOUNTS cba,
       CE_BANK_BRANCHES_V cbbv,
       FND_NEW_MESSAGES fnm
 WHERE csh.statement_number           = cre.statement_number
   AND csh.bank_account_num            = cba.bank_account_num
   AND cbbv.branch_party_id           = cba.bank_branch_id
   AND cre.message_name               = fnm.message_name
   AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE)
   GROUP BY csh.statement_number,
       --csh.statement_date,
       cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       cba.bank_account_num,
       fnm.message_text
UNION ALL
SELECT csh.statement_number,
       --csh.statement_date,
       --cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       (case when length(cba.bank_account_num) = 10
                       THEN 'XXXXXX'||substr(cba.bank_account_num, 7)
                       when length(cba.bank_account_num) = 9
                       then 'XXXXX'||substr(cba.bank_account_num, 6)
                       when length(cba.bank_account_num) > 10
                       then 'XXXXXXX'||substr(cba.bank_account_num, 8)
                       when length(cba.bank_account_num) < 9
                       then 'XXXX'||substr(cba.bank_account_num, 5)
                  end)bank_account_num,
       cre.creation_date,
       --cba.bank_account_num,
       fnm.message_text
  FROM ce_statement_headers csh,
       ce_header_interface_errors cre,
       CE_BANK_ACCOUNTS cba,
       CE_BANK_BRANCHES_V cbbv,
       FND_NEW_MESSAGES fnm
 WHERE csh.statement_number           = cre.statement_number
   AND csh.bank_account_id            = cba.bank_account_id
   AND cbbv.branch_party_id           = cba.bank_branch_id
   AND cre.message_name               = fnm.message_name
   AND TRUNC(cre.creation_date)       = TRUNC(SYSDATE)
   GROUP BY csh.statement_number,
       --csh.statement_date,
       cre.creation_date,
       --cbbv.bank_name,
       --cbbv.bank_branch_name,
       --cba.bank_account_name,
       cba.bank_account_num,
       fnm.message_text
   ;
 /*SELECT  cse.statement_number
        ,'XXXXXX'||substr(cse.bank_account_num, 7) bank_account_num
        ,cse.creation_date
        ,substr(cse.message_text,1,instr(cse.message_text, 't'))||' XXXXXX'||substr(cse.message_text, instr(cse.message_text, 't')+7) message_text
 FROM RGCE_SQLLDR_ERRORS_STG cse
 WHERE cse.status = 'E';*/

    Cursor c_cur(v_request_id NUMBER) IS
        SELECT file_name filename
          FROM RGCE_STMT_INT_TMP
         WHERE request_id = v_request_id
      GROUP BY file_name;

 BEGIN
    BEGIN
        Select Name
          INTO l_database
          FROM v$database;
    EXCEPTION
        WHEN OTHERS THEN
        l_database := NULL;
        dbms_output.put_line('Error in data base fetch:'||SQLERRM);
    END;

    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;

    l_file_name := NULL;

    FOR rec IN c_cur(p_request_id)
    LOOP

--    l_file_name := rec.filename ||chr(10)|| chr(9)|| chr(9)|| l_file_name;
  l_file_name := rec.filename ||'<p>'|| l_file_name;

    END LOOP;


 v_subject := 'Bank Statement Interface Report'||'-'||'['||l_database||']';
 v_Recipient := l_email;
 v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);

            vLine := 'Statement Number, Bank Account Number, Creation Date, Message Text';
    FOR crec IN c
    LOOP
        BEGIN

            IF LENGTH (vLine) > 32767
            THEN
                vLine := vLine;
            ELSE
                vLine :=
                    vLine
                || CHR (10)
                || crec.statement_number
                || ','
                || crec.bank_account_num
                || ','
                || crec.creation_date
                || ','
                || crec.message_text;
        END IF;
        EXCEPTION
         WHEN OTHERS
         THEN
            IF (SQLCODE = -06502)
            THEN
               vLine := vLine;
               v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
               UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
               UTL_SMTP.Mail (v_Mail_Conn, v_From);
               UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

            UTL_SMTP.
               Data (
                  v_Mail_Conn,
                   --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
                   'From: '
                  || v_From
                  || crlf
                  || 'Subject: '
                  || v_Subject
                  || crlf
                  || 'To: '
                  || v_Recipient
                  || crlf
                  || 'MIME-Version: 1.0'
                  || crlf
                  ||                                 -- Use MIME mail standard
                    'Content-Type: multipart/mixed;'
                  || crlf
                  || ' boundary="-----SECBOUND"'
                  || crlf
                  || crlf
                  || '-------SECBOUND'
                  || crlf
                  || 'Content-Type: text/html;'
                  || crlf
                  || 'Content-Transfer_Encoding: 7bit'
                  || crlf
                  || crlf
                  || 'Bank statement interface to Imported statement file(s) received from bank(s) completed successfully.<p>'
                     || crlf
                     ||'Statements are imported into Oracle Cash Management system.<p>'
                     || crlf
                     ||'Some of the statements could not be Imported. Attached please find Error/Exception Report for review and necessary action. <p>'
                     || chr(10)
                  || ' '
                  ||'<br>'
                  ||'<br>'
                  || v_file_name
                  || '<p>'
                  || chr(10)
                  || crlf
                  ||' '
                  ||'<br>'
                  ||'<br>'
                  ||                                           -- Message body
                    'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
                  || crlf
                  || crlf
                  || '-------SECBOUND'
                  || crlf
                  || 'Content-Type: text/plain;'
                  || crlf
                  || ' name="excel.csv"'
                  || crlf
                  || 'Content-Transfer_Encoding: 8bit'
                  || crlf
                  || 'Content-Disposition: attachment;'
                  || crlf
                  || ' filename="Bank_Statement_Error_Report.csv"'
                  || crlf
                  || crlf
                  || vLine
                  || crlf
                  ||                                  -- Content of attachment
                    crlf
                  || '-------SECBOUND--'                      -- End MIME mail
                                        );
                vLine := 'Statement Number, Bank Account Number, Creation Date, Message Text';
               UTL_SMTP.Quit (v_mail_conn);
            END IF;
        END;
    END LOOP;

    v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
   UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
   UTL_SMTP.Mail (v_Mail_Conn, v_From);
   UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

   UTL_SMTP.
   Data (
      v_Mail_Conn,
       --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
       'From: '
      || v_From
      || crlf
      || 'Subject: '
      || v_Subject
      || crlf
      || 'To: '
      || v_Recipient
      || crlf
      || 'MIME-Version: 1.0'
      || crlf
      ||                                             -- Use MIME mail standard
        'Content-Type: multipart/mixed;'
      || crlf
      || ' boundary="-----SECBOUND"'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/html;'
      || crlf
      || 'Content-Transfer_Encoding: 7bit'
      || crlf
      || crlf
      || 'Bank statement interface to Import statement file(s) received from bank(s) completed successfully.<p>'
      || crlf
      ||'Statements are posted in interface table and is ready for Reconciliation into Oracle Cash Management system.<p>'
      || crlf
      ||'Some of the statements could not be Imported. Attached please find Error/Exception Report for review and necessary action.<p>'
      || chr(10)
      || ' '
      ||'<br>'
      ||'<br>'
      || v_file_name
      || '<p>'
      || chr(10)
      || crlf
      ||' '
      ||'<br>'
      ||'<br>'
      ||                                                       -- Message body
        'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/plain;'
      || crlf
      || ' name="excel.csv"'
      || crlf
      || 'Content-Transfer_Encoding: 8bit'
      || crlf
      || 'Content-Disposition: attachment;'
      || crlf
      || ' filename="Bank_Statement_Error_Report.csv"'
      || crlf
      || crlf
      || vLine
      || crlf
      ||                                              -- Content of attachment
        crlf
      || '-------SECBOUND--'                                  -- End MIME mail
                            );
   UTL_SMTP.Quit (v_mail_conn);
EXCEPTION
   WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
   THEN
      raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
END RGCE_SEND_IMPORTERROR_MAIL;

-- Added by Mahipal on 03-FEB-16.
PROCEDURE RGCE_SEND_SUCCESS_MAIL(p_request_id NUMBER)
IS
   v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
   v_Recipient   VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
   v_subject     VARCHAR2 (1000);
   v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   v_Mail_Conn   UTL_SMTP.Connection;
   crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
   vctr          NUMBER := 0;
   vLine         VARCHAR2 (32767);
   l_database    VARCHAR2(100);
   l_email       VARCHAR2(100);
   l_file_name   VARCHAR2(1000);
   v_file_name   VARCHAR2(1000);

   CURSOR c_cur(v_request_id NUMBER)
   IS
    SELECT file_name filename
      FROM RGCE_STMT_INT_TMP
     WHERE request_id = v_request_id
  GROUP BY file_name;

 BEGIN
    BEGIN
        Select Name
          INTO l_database
          FROM v$database;
    EXCEPTION
        WHEN OTHERS THEN
        l_database := NULL;
        dbms_output.put_line('Error in data base fetch:'||SQLERRM);
    END;

    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;

    l_file_name := NULL;

    FOR rec IN c_cur(p_request_id)
    LOOP

--    l_file_name := rec.filename ||chr(10)|| chr(9)|| chr(9)||l_file_name;
  l_file_name := rec.filename ||'<p>'|| l_file_name;

    END LOOP;

 v_subject := 'Bank Statement Interface Report'||'-'||'['||l_database||']';
 v_Recipient := l_email;
 v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);


   v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
   UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
   UTL_SMTP.Mail (v_Mail_Conn, v_From);
   UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

   UTL_SMTP.
   Data (
      v_Mail_Conn,
        --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
       'From: '
      || v_From
      || crlf
      || 'Subject: '
      || v_Subject
      || crlf
      || 'To: '
      || v_Recipient
      || crlf
      || 'MIME-Version: 1.0'
      || crlf
      ||                                             -- Use MIME mail standard
        'Content-Type: multipart/mixed;'
      || crlf
      || ' boundary="-----SECBOUND"'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/html;'
      || crlf
      || 'Content-Transfer_Encoding: 7bit'
      || crlf
      || crlf
      || 'Bank statement interface to import statement file(s) received from bank(s) completed successfully.<p>'
      || crlf
      || 'No error/exception were generated in this run. Statements are posted in interface table and is ready for Reconciliation into Oracle Cash Management system.<p>'
      || crlf
      || chr(10)
      || ' '
      ||'<br>'
      ||'<br>'
      || v_file_name
      || ' '
      || chr(10)
      ||' '
      ||'<br>'
      ||'<br>'
      ||                                                       -- Message body
        'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
      || crlf
      || '-------SECBOUND--'                                  -- End MIME mail
                            );
   UTL_SMTP.Quit (v_mail_conn);
EXCEPTION
   WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
   THEN
      raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
END RGCE_SEND_SUCCESS_MAIL;

PROCEDURE RGCE_PROG_ERROR_MAIL(p_request_id NUMBER)
IS
   v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
   v_Recipient   VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
   v_subject     VARCHAR2 (1000);
   v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   v_Mail_Conn   UTL_SMTP.Connection;
   crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
   vctr          NUMBER := 0;
   vLine         VARCHAR2 (32767);
   l_database    VARCHAR2(100);
   l_email       VARCHAR2(100);
   l_file_name   VARCHAR2(1000);
   v_file_name   VARCHAR2(1000);

   CURSOR c_cur(v_request_id NUMBER)
   IS
    SELECT file_name filename
      FROM RGCE_STMT_INT_TMP
     WHERE request_id = v_request_id
  GROUP BY file_name;

  CURSOR cur(v_request_id NUMBER)
  IS
    SELECT TRIM ('/' from  substr(argument3,instr(argument3,'/',-1),length(argument3))) filename
      FROM fnd_concurrent_requests
     WHERE parent_request_id IN ( SELECT request_id
                                    FROM fnd_concurrent_requests
                                   WHERE parent_request_id = v_request_id)
                                     AND concurrent_program_id IN ( SELECT concurrent_program_id
                                                                      FROM fnd_concurrent_programs
                                                                     WHERE concurrent_program_name = 'CEBSLDR')
     and status_code = 'E';

 BEGIN
    BEGIN
        Select Name
          INTO l_database
          FROM v$database;
    EXCEPTION
        WHEN OTHERS THEN
        l_database := NULL;
        dbms_output.put_line('Error in data base fetch:'||SQLERRM);
    END;

    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;

    l_file_name := NULL;

    FOR rec IN c_cur(p_request_id)
    LOOP

    --l_file_name := rec.filename ||chr(10)|| chr(9)|| chr(9)||l_file_name;
  l_file_name := rec.filename ||'<p>'|| l_file_name;

    END LOOP;

 v_subject := 'Bank Statement Interface Report'||'-'||'['||l_database||']';
 v_Recipient := l_email;
 --v_file_name := 'File Processed: ' ||' '|| TRIM(Trailing ',' FROM l_file_name);
 v_file_name := '<strong>File Names:</strong> ' ||' '|| TRIM(Trailing ',' FROM l_file_name);


   v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
   UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
   UTL_SMTP.Mail (v_Mail_Conn, v_From);
   UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

   UTL_SMTP.
   Data (
      v_Mail_Conn,
       --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
       'From: '
      || v_From
      || crlf
      || 'Subject: '
      || v_Subject
      || crlf
      || 'To: '
      || v_Recipient
      || crlf
      || 'MIME-Version: 1.0'
      || crlf
      ||                                             -- Use MIME mail standard
        'Content-Type: multipart/mixed;'
      || crlf
      || ' boundary="-----SECBOUND"'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/html;'
      || crlf
      || 'Content-Transfer_Encoding: 7bit'
      || crlf
      || crlf
      || 'Bank statement interface to import statement file(s) received from bank(s) did not complete successfully.<p> One of the program completed in error/warning. Please contact <strong>Oracle IT Support Center</strong> for resolution and re-run.<p>'
      || crlf
      || '<p>'
      || chr(10)
      || ' '
      ||'<br>'
      ||'<br>'
      || v_file_name
      || ' '
      || chr(10)
      ||' '
      ||'<br>'
      ||'<br>'
      ||                                                       -- Message body
        'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
      || crlf
      || '-------SECBOUND--'                                  -- End MIME mail
                            );
   UTL_SMTP.Quit (v_mail_conn);
EXCEPTION
   WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
   THEN
      raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
END RGCE_PROG_ERROR_MAIL;
PROCEDURE RGCE_SEND_NOFILE_MAIL
IS
   v_From        VARCHAR2 (80) := 'donotreply@riotgames.com';
   v_Recipient   VARCHAR2 (1000); -- := 'ext.mardam@riotgames.com';
   v_subject     VARCHAR2 (1000);
   v_Mail_Host   VARCHAR2 (30) := fnd_profile.VALUE ('RGAP_SMTP_SERVER');
   v_Mail_Conn   UTL_SMTP.Connection;
   crlf          VARCHAR2 (2) := CHR (13) || CHR (10);
   vctr          NUMBER := 0;
   vLine         VARCHAR2 (32767);
   l_database    VARCHAR2(100);
   l_email       VARCHAR2(100);
   l_file_name   VARCHAR2(1000);
   v_file_name   VARCHAR2(1000);

 /*  CURSOR c_cur(v_request_id NUMBER)
   IS
    SELECT file_name filename
      FROM RGCE_STMT_INT_TMP
     WHERE request_id = v_request_id
  GROUP BY file_name;*/

 BEGIN
    BEGIN
        Select Name
          INTO l_database
          FROM v$database;
    EXCEPTION
        WHEN OTHERS THEN
        l_database := NULL;
        dbms_output.put_line('Error in data base fetch:'||SQLERRM);
    END;

    BEGIN
      SELECT meaning
        INTO l_email
        FROM fnd_common_lookups
       WHERE lookup_type LIKE 'RGCE_BANKINT_USER_EMAIL_NOTIFY'
         AND enabled_flag = 'Y'
         AND NVL(end_date_active,sysdate) >= SYSDATE;
    EXCEPTION
      WHEN OTHERS THEN
      l_email := NULL;
      FND_FILE.PUT_LINE(FND_FILE.LOG,'Error while fetching email address:'||SQLERRM);
    END;

    l_file_name := NULL;

    /*FOR rec IN c_cur(p_request_id)
    LOOP

    l_file_name := rec.filename ||chr(10)|| chr(9)|| chr(9)||l_file_name;

    END LOOP;*/

 v_subject := 'Bank Statement Interface Report'||'-'||'['||l_database||']';
 v_Recipient := l_email;
 --v_file_name := ' ............'; --'File Processed: ' ||' '|| TRIM(Trailing ',' FROM l_file_name);


   v_Mail_Conn := UTL_SMTP.Open_Connection (v_Mail_Host, 25);
   UTL_SMTP.Helo (v_Mail_Conn, v_Mail_Host);
   UTL_SMTP.Mail (v_Mail_Conn, v_From);
   UTL_SMTP.Rcpt (v_Mail_Conn, v_Recipient);

   UTL_SMTP.
   Data (
      v_Mail_Conn,
       --'Date: '|| TO_CHAR (SYSDATE, 'Dy, DD Mon YYYY hh24:mi:ss')|| crlf||
      'From: '
      || v_From
      || crlf
      || 'Subject: '
      || v_Subject
      || crlf
      || 'To: '
      || v_Recipient
      || crlf
      || 'MIME-Version: 1.0'
      || crlf
      ||                                             -- Use MIME mail standard
        'Content-Type: multipart/mixed;'
      || crlf
      || ' boundary="-----SECBOUND"'
      || crlf
      || crlf
      || '-------SECBOUND'
      || crlf
      || 'Content-Type: text/html;'
      || crlf
      || 'Content-Transfer_Encoding: 7bit'
      || crlf
      || crlf
      || 'Bank statement interface to import statement file(s) received from bank(s) ran as per schedule.<p> There was <strong>no file in inbound folder </strong> to process in this run.<p>'
      || crlf
      || ' '
      || chr(10)
      || ' '
      || chr(10)
      || ' '
      ||'<br>'
      ||'<br>'
      ||                                                      -- Message body
        'This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to <strong>Oracle IT Support Center</strong> and it will be investigated.'
      || crlf
      || '-------SECBOUND--'                                  -- End MIME mail
                            );
   UTL_SMTP.Quit (v_mail_conn);
EXCEPTION
   WHEN UTL_SMTP.Transient_Error OR UTL_SMTP.Permanent_Error
   THEN
      raise_application_error (-20000, 'Unable to send mail: ' || SQLERRM);
END RGCE_SEND_NOFILE_MAIL;
END RGCE_CE_BANK_LOAD_PKG;
/
EXIT;
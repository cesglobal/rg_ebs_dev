Hi,

Attached please find today's Bank Statement Import Interface Error Report. Please review and take necessary action.


IMPORTANT: This is an automated message. Please do not respond. If there are any discrepancies in what this notification states report it to Global IT Support Center and it will be investigated.
Thanks,
Riot Games Technical Team.
LOAD DATA
INFILE '$FILE'
APPEND
INTO table RGCE_STMT_INT_TMP
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
TRAILING NULLCOLS
(REC_ID_NO 
 ,COLUMN1          
 ,COLUMN2          
 ,COLUMN3          
 ,COLUMN4          
 ,COLUMN5          
 ,COLUMN6          
 ,COLUMN7          
 ,COLUMN8          
 ,COLUMN9          
 ,COLUMN10         
 ,COLUMN11         
 ,COLUMN12         
 ,COLUMN13         
 ,COLUMN14         
 ,COLUMN15         
 ,COLUMN16         
 ,COLUMN17         
 ,COLUMN18         
 ,COLUMN19         
 ,COLUMN20         
 ,COLUMN21         
 ,COLUMN22         
 ,COLUMN23         
 ,COLUMN24         
 ,COLUMN25         
 ,COLUMN26         
 ,COLUMN27         
 ,COLUMN28         
 ,COLUMN29         
 ,COLUMN30         
 ,COLUMN31         
 ,COLUMN32         
 ,COLUMN33         
 ,COLUMN34         
 ,COLUMN35         
 ,CREATED_BY                 CONSTANT '-1'     
 ,CREATION_DATE              sysdate
 ,LAST_UPDATED_BY            CONSTANT '-1'
 ,LAST_UPDATE_DATE           sysdate
 ,LAST_UPDATE_LOGIN          CONSTANT '-1'
 ,COLUMN36         
 )
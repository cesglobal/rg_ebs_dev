REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: RGAP_COUPA_INVOICES.trg
REM PURPOSE : To create the custom RGAP_COUPA_INVOICES trigger
REM PARAMETERS : Tablespace
REM CALLED :
REM CALLED BY :
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 05-AUG-15 1.0 INITIAL RELEASE
REM
REM ======================================================================

DROP TRIGGER APPS.RGINTF_MONITOR_TRG_CP;

CREATE OR REPLACE TRIGGER APPS.rgintf_monitor_trg_cp
   AFTER INSERT OR UPDATE
   ON "RGAP"."RGINTF_MONITOR_TAB"
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   l_interface_email   VARCHAR2 (240)
                          := FND_PROFILE.VALUE ('RGAP_COUPA_EMAIL');
   l_errbuf            VARCHAR2 (120);
   l_retcode           VARCHAR2 (120);
BEGIN
   IF :NEW.INTERFACE_NAME = 'INV_INTF_CP_EBS'
   THEN
      rgap_coupa_ebs_inv_int_pkg.rgap_inv_main (l_errbuf, l_retcode);
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      rgap_common_utility_pkg.
       rgap_sendmail (
         l_interface_email,
         'Invoking Invoice Interface from Coupa to Oracle failed, Please check Errors'
         || SQLCODE
         || '-'
         || SQLERRM,
         fnd_profile.VALUE ('RGAP_SMTP_SERVER'),
         fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT'),
         'noreply@riotgames.com');
      rgcomn_debug_pkg.log_message (
									P_LEVEL          => 0,
									P_PROGRAM_NAME   => 'rgintf_monitor_trg.rgap_sendmail',
									P_MESSAGE        =>   'Error Occured while calling Invoice Import'
														|| '-'
														|| SQLCODE
														|| '  '
														|| SQLERRM);
END;
/
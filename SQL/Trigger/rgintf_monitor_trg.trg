-- Script Name : rgintf_monitor_trg.trg
-- Purpose     : Following tasks are performed for Interface loading:
--               1. Creates triggers for each interface program.
-- Version     : 1.0

-- Copyright Riot Games  All Rights Reserved

-- MODIFICATION HISTORY
-- Person       Date       Comments
-- ---------    ------     ------------------------------------------
-- Srinivasa    09/29/2015 Created
DROP TRIGGER APPS.RGINTF_MONITOR_TRG;

CREATE OR REPLACE TRIGGER APPS.rgintf_monitor_trg
   AFTER INSERT OR UPDATE
   ON "RGAP"."RGINTF_MONITOR_TAB"
   REFERENCING NEW AS NEW OLD AS OLD
   FOR EACH ROW
DECLARE
   l_interface_email   VARCHAR2 (240)
                          := FND_PROFILE.VALUE ('RGAP_ORACLE_EMAIL');
   l_errbuf            VARCHAR2 (120);
   l_retcode           VARCHAR2 (120);
BEGIN
   IF :NEW.INTERFACE_NAME = 'INV_INTF_CR_EBS'
   THEN
      rgap_concurr_invintf_pkg.rgap_concinv_main_prc (l_errbuf, l_retcode);
   END IF;
EXCEPTION
   WHEN OTHERS
   THEN
      rgap_common_utility_pkg.
       rgap_sendmail (
         l_interface_email,
         'Invoking Invoice Interface from Concurr to Oracle failed, Please check Errors'
         || SQLCODE
         || '-'
         || SQLERRM,
         fnd_profile.VALUE ('RGAP_SMTP_SERVER'),
         fnd_profile.VALUE ('RGAP_SMTP_SERVER_PORT'),
         'noreply@riotgames.com');
      rgcomn_debug_pkg.
       log_message (
         P_LEVEL          => 0,
         P_PROGRAM_NAME   => 'rgintf_monitor_trg.rgap_sendmail',
         P_MESSAGE        =>   'Error Occured while calling Invoice Import'
                            || '-'
                            || SQLCODE
                            || '  '
                            || SQLERRM);
END;
/


REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: .RGGL_ODS_GLCODE_SEGM_VIEW.sql
REM PURPOSE : To create COA Description custom view 
REM PARAMETERS : 
REM CALLED :
REM CALLED BY : ODS Team
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 29-OCT-15 1.0 INITIAL RELEASE
REM
REM ======================================================================
DROP VIEW APPS.RGGL_ODS_GLCODE_SEGM_V;

/* Formatted on 23-12-2015 01:03:18 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW APPS.RGGL_ODS_GLCODE_SEGM_V
(
   FLEX_VALUE_SET_NAME,
   FLEX_VALUE_ID,
   VALUE,
   DESCRIPTION,
   ENABLED_FLAG,
   HIERARCHY_CODE,
   "PARENT ACC",
   BUDGET,
   POST,
   TYPE,
   CNTL,
   RECON,
   LAST_UPDATED_BY,
   LAST_UPDATE_DATE
)
AS
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_COMPANY'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_ACCOUNT'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_DISCIPLINE'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_TERRITORY'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_PRODUCT'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_INITIATIVE'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+)
   UNION
   SELECT   DISTINCT
            ffvs1.flex_value_set_name,
            ffval1.flex_value_id,
            ffval1.flex_value "VALUE",
            ffvtl1.description,
            ffval1.enabled_flag,
            fh.hierarchy_code,
            ffval1.summary_flag "PARENT ACC",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 1, 1)
               "BUDGET",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 3, 1) "POST",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 5, 1) "TYPE",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 7, 1) "CNTL",
            SUBSTR (TO_CHAR (ffval1.compiled_value_attributes), 9, 1) "RECON",
            ffval1.last_updated_by,
            ffval1.last_update_date
     FROM   fnd_flex_values ffval1,
            fnd_flex_values_tl ffvtl1,
            fnd_flex_value_sets ffvs1,
            fnd_id_flex_segments seg,
            fnd_flex_hierarchies_vl fh
    WHERE       ffval1.flex_value_set_id(+) = ffvs1.flex_value_set_id
            AND seg.flex_value_set_id = ffvs1.flex_value_set_id
            AND ffval1.flex_value_id = ffvtl1.flex_value_id(+)
            AND ffvs1.flex_value_set_name = 'RG_FUTURE'
            AND ffval1.structured_hierarchy_level = fh.hierarchy_id(+);



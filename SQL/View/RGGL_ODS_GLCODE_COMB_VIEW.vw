REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: .RGGL_ODS_GLCODE_COMB_VIEW.sql
REM PURPOSE : To create COA Description custom view 
REM PARAMETERS : 
REM CALLED :
REM CALLED BY : ODS Team
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 29-OCT-15 1.0 INITIAL RELEASE
REM
REM ======================================================================
DROP VIEW APPS.RGGL_ODS_GLCODE_COMB_V;

/* Formatted on 23-12-2015 01:07:19 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW APPS.RGGL_ODS_GLCODE_COMB_V
(
   CODE_COMBINATION_ID,
   CHART_OF_ACCOUNTS_ID,
   DETAIL_POSTING_ALLOWED_FLAG,
   DETAIL_BUDGETING_ALLOWED_FLAG,
   ACCOUNT_TYPE,
   ACCOUNT_DET_TYPE,
   ENABLED_FLAG,
   SUMMARY_FLAG,
   SEGMENT1,
   SEGMENT2,
   SEGMENT3,
   SEGMENT4,
   SEGMENT5,
   SEGMENT6,
   SEGMENT7,
   SEGMENT8,
   CONCATENATED_SEGMENTS,
   CONCAT_SEG_DESCRIPTION
)
AS
   SELECT   gv.code_combination_id,
            gv.chart_of_accounts_id,
            detail_posting_allowed_flag,
            detail_budgeting_allowed_flag,
            account_type,
            show_account_type account_det_type,
            gv.enabled_flag,
            gv.summary_flag,
            gv.segment1,
            gv.segment2,
            gv.segment3,
            gv.segment4,
            gv.segment5,
            gv.segment6,
            gv.segment7,
            gv.segment8,
            kv.concatenated_segments,
            gl_flexfields_pkg.get_concat_description (
               kv.chart_of_accounts_id,
               kv.code_combination_id
            )
               concat_seg_description
     FROM   gl_code_combinations_v gv, gl_code_combinations_kfv kv
    WHERE   gv.code_combination_id = kv.code_combination_id;


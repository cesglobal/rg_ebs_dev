DROP VIEW APPS.RGFA_ASSET_DETAILS_V;

/* Formatted on 30-12-2015 13:47:38 (QP5 v5.149.1003.31008) */
CREATE OR REPLACE FORCE VIEW APPS.RGFA_ASSET_DETAILS_V
(
   ASSET_CATEGORY,
   ASSET_ID,
   ASSET_NUMBER,
   ASSET_DESCRIPTION,
   IN_SERVICE_DATE,
   SUPPLIER,
   INVOICE_NUMBER,
   LIFE_IN_MONTHS,
   ASSET_BOOK,
   CURRENT_PERIOD_DEPRECIATION,
   LEDGER_ID,
   PERIOD,
   PERIOD_COUNTER,
   YTD_DEPRECIATION,
   START_DATE,
   END_DATE
)
AS
   WITH FDDH
          AS (SELECT                    /*+ INDEX(DPRND FA_DEPRN_DETAIL_N2) */
                    ASSET.ASSET_ID Asset_ID,
                       ASSEL.DESCRIPTION Asset_Description,
                       ASSET.ASSET_NUMBER Asset_Number,
                       PER.BOOK_TYPE_CODE Book_Name,
                       DIST.DISTRIBUTION_ID Distribution_Id,
                       BOOK.SET_OF_BOOKS_ID Ledger_Id,
                       DPRND.PERIOD_COUNTER Period_Counter,
                       DPRND.DEPRN_AMOUNT Depreciation_Amount,
                       PER.PERIOD_NAME Period_Name,
                       PER.PERIOD_NUM Period_Number,
                       DPRND.YTD_DEPRN Ytd_Depreciation_Amount
                FROM   FA.FA_ADDITIONS_B ASSET,
                       FA.FA_ADDITIONS_TL ASSEL,
                       FA.FA_DEPRN_DETAIL DPRND,
                       FA.FA_DISTRIBUTION_HISTORY DIST,
                       FA.FA_DEPRN_PERIODS PER,
                       FA.FA_ASSET_HISTORY AHIST,
                       FA.FA_DEPRN_PERIODS LASTP,
                       FA.FA_BOOK_CONTROLS BOOK
               WHERE       PER.BOOK_TYPE_CODE = DPRND.BOOK_TYPE_CODE
                       AND PER.PERIOD_COUNTER = DPRND.PERIOD_COUNTER
                       AND DPRND.ASSET_ID = ASSET.ASSET_ID
                       AND DIST.DISTRIBUTION_ID = DPRND.DISTRIBUTION_ID
                       AND AHIST.ASSET_ID(+) = DPRND.ASSET_ID
                       AND DPRND.DEPRN_RUN_DATE >= AHIST.DATE_EFFECTIVE(+)
                       AND DPRND.DEPRN_RUN_DATE <
                             NVL (AHIST.DATE_INEFFECTIVE(+), SYSDATE)
                       AND LASTP.BOOK_TYPE_CODE(+) = DPRND.BOOK_TYPE_CODE
                       AND NVL (LASTP.PERIOD_COUNTER, PER.PERIOD_COUNTER) =
                             (SELECT   MAX (FDP.PERIOD_COUNTER)
                                FROM   FA.FA_DEPRN_PERIODS FDP
                               WHERE   FDP.BOOK_TYPE_CODE =
                                          DPRND.BOOK_TYPE_CODE
                                       AND FDP.PERIOD_CLOSE_DATE IS NOT NULL)
                       AND ASSET.ASSET_ID = ASSEL.ASSET_ID
                       AND NVL (DIST.DATE_INEFFECTIVE, '31-DEC-4712') >
                             per.CALENDAR_PERIOD_CLOSE_DATE
                       AND book.book_type_code = dprnd.book_type_code)
     SELECT   fac.segment1 Asset_Category,
              FAB.ASSET_ID Asset_ID,
              FDDH.ASSET_NUMBER Asset_Number,
              FDDH.ASSET_DESCRIPTION Asset_Description,
              (SELECT   fb.date_placed_in_service
                 FROM   apps.fa_books fb
                WHERE       fab.asset_id = fb.asset_id
                        AND fb.book_type_code = fddh.book_name
                        AND fb.date_ineffective IS NULL
                        AND fb.transaction_header_id_out IS NULL)
                 In_Service_Date,
              (SELECT   asp.VENDOR_NAME
                 FROM   FA.FA_ASSET_INVOICES FAI,
                        FA.FA_TRANSACTION_HEADERS FTH,
                        FA.FA_BOOKS FB,
                        ap.ap_suppliers asp
                WHERE   FAI.ASSET_ID = FAB.ASSET_ID
                        AND FAI.DATE_INEFFECTIVE IS NULL
                        AND FAI.INVOICE_TRANSACTION_ID_IN =
                              FTH.INVOICE_TRANSACTION_ID
                        AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                        AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                        AND FAB.ASSET_ID = FB.ASSET_ID
                        AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                        AND FB.DATE_INEFFECTIVE IS NULL
                        AND asp.vendor_id = FAI.PO_VENDOR_ID
                        AND ROWNUM <= 1)
                 Supplier,
              (SELECT   FAI.INVOICE_NUMBER
                 FROM   FA.FA_ASSET_INVOICES FAI,
                        FA.FA_TRANSACTION_HEADERS FTH,
                        FA.FA_BOOKS FB
                WHERE   FAI.ASSET_ID = FAB.ASSET_ID
                        AND FAI.DATE_INEFFECTIVE IS NULL
                        AND FAI.INVOICE_TRANSACTION_ID_IN =
                              FTH.INVOICE_TRANSACTION_ID
                        AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                        AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                        AND FAB.ASSET_ID = FB.ASSET_ID
                        AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                        AND FB.DATE_INEFFECTIVE IS NULL
                        AND ROWNUM <= 1)
                 INVOICE_NUMBER,
              (SELECT   fb.life_in_months
                 FROM   fa.fa_books fb
                WHERE       fab.asset_id = fb.asset_id
                        AND fb.book_type_code = fddh.book_name
                        AND ROWNUM <= 1)
                 Life_In_Months,
              FDDH.BOOK_NAME Asset_Book,
              FDDH.depreciation_amount Current_Period_Depreciation,
              --FDDH.DISTRIBUTION_ID Distribution_ID,-- 102371 --commented on 161215
              FDDH.LEDGER_ID Ledger_Id,
              FDDH.period_name Period,
              FDDH.period_counter Period_Counter,
              fddh.ytd_depreciation_amount ytd_depreciation,
              (SELECT   start_date
                 FROM   FA_CALENDAR_PERIODS
                WHERE   period_name = FDDH.period_name)
                 Start_date,
              (SELECT   End_date
                 FROM   FA_CALENDAR_PERIODS
                WHERE   period_name = FDDH.period_name)
                 End_date
       FROM   FDDH, FA.FA_ADDITIONS_B FAB, FA_CATEGORIES_B fac
      WHERE   FDDH.ASSET_ID = FAB.ASSET_ID
              AND fab.asset_category_id = fac.category_id
   ORDER BY   1, 2 ASC;
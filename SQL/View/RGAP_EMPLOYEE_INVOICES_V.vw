REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM

REM PROGRAM NAME: RGINTF_MONITOR_SEQ.sql
REM PURPOSE : create custom sequence to generate unique id for interface_id column in
REM RGINTF_MONITOR_TAB table.
REM CALLED :
REM CALLED BY :
REM

REM UPDATE HISTORY
REM

REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 18-AUG-15 1.0 INITIAL RELEASE
REM

REM ======================================================================

DROP VIEW APPS.RGAP_EMPLOYEE_INVOICES_V;

/* Formatted on 21-12-2015 21:12:39 (QP5 v5.115.810.9015) */
CREATE OR REPLACE FORCE VIEW APPS.RGAP_EMPLOYEE_INVOICES_V
(
   BATCH_NAME,
   INVOICE_TYPE,
   INVOICE_NUMBER,
   SUPPLIER_NAME,
   SUPPLIER_NUMBER,
   SUPPLIER_SITE,
   INVOICE_CURRENCY,
   INVOICE_AMOUNT,
   LINE_DESCRIPTION,
   INVOICE_DATE,
   TERMS,
   PAY_GROUP,
   GL_ACCOUNT,
   EMPLOYEE_INVOICE_TYPE,
   DOCUMENT_NUM,
   CARD_NUMBER,
   MERCHANT_NAME,
   TAX_NUMBER,
   EMPLOYER,
   PARTICIPANT,
   PURPOSE,
   VENUE,
   OPERATING_UNIT,
   GL_DATE,
   BATCH_ID,
   WFAPPROVAL_STATUS,
   SOURCE,
   INVOICE_ID,
   LINE_NUMBER
)
AS
     SELECT   aba.batch_name,
              fl.meaning Invoice_type,
              aia.invoice_num invoice_number,
              aps.vendor_name supplier_name,
              aps.segment1 supplier_number,
              apps.vendor_site_code supplier_site,
              aia.invoice_currency_code invoice_currency,
              aid.total_dist_amount invoice_amount,
              aid.description Line_description,
              aia.invoice_date,
              apt.Name Terms,
              flv.meaning pay_group,
              gcc.concatenated_segments Gl_account,
              aia.global_attribute1 Employee_invoice_type,
              aia.global_attribute2 document_num,
              aia.global_attribute3 card_number,
              aia.global_attribute4 merchant_name,
              aia.global_attribute5 tax_number,
              aid.description employer,
              NULL participant,
              NULL purpose,
              NULL venue,
              hou.name operating_unit,
              aid.ACCOUNTING_DATE gl_date,
              aia.batch_id,
              aia.wfapproval_status,
              aia.source,
              aia.invoice_id,
              aid.distribution_line_number line_number
       FROM   ap_invoices_all aia,
              --ap_invoice_lines_all aila,
              ap_invoice_distributions_all aid,
              ap_suppliers aps,
              ap_supplier_sites_all apps,
              ap_terms apt,
              fnd_lookups fl,
              fnd_lookup_values_vl flv,
              gl_code_combinations_kfv gcc,
              hr_operating_units hou,
              ap_batches_all aba
      WHERE       1 = 1                     --aia.invoice_id = aila.invoice_id
              AND aia.vendor_id = aps.vendor_id
              AND aia.vendor_site_id = apps.vendor_site_id
              AND aia.vendor_id = apps.vendor_id
              AND aps.vendor_id = apps.vendor_id
              AND aia.terms_id = apt.term_id
              AND fl.lookup_type = 'IBY_DOCUMENT_TYPES'
              AND fl.lookup_code = aia.invoice_type_lookup_code
              AND flv.lookup_type = 'PAY GROUP'
              AND aid.amount <> 0
              AND flv.lookup_code = aia.pay_group_lookup_code
              AND gcc.code_combination_id = aid.dist_code_combination_id
              --  AND aia.invoice_id = aid.invoice_id
              AND aia.org_id = hou.organization_id
              AND aid.org_id = aia.org_id
              AND apps.org_id = aia.org_id
              AND aia.batch_id = aba.batch_id
              --AND aid.batch_id = aia.batch_id--100316
              AND APPS.Ap_Invoices_Pkg.GET_APPROVAL_STATUS (
                    aia.INVOICE_ID,
                    aia.INVOICE_AMOUNT,
                    aia.PAYMENT_STATUS_FLAG,
                    aia.INVOICE_TYPE_LOOKUP_CODE
                 ) = 'NEEDS REAPPROVAL'
              AND EXISTS
                    (SELECT   '1'
                       FROM   AP_HOLDS_ALL a1
                      WHERE       a1.invoice_id = aia.invoice_id
                              AND a1.invoice_id = aid.invoice_id
                              AND a1.release_lookup_code IS NULL
                              AND a1.HOLD_LOOKUP_CODE =
                                    'Need AP Manager Approval')
              AND NOT EXISTS
                    (SELECT   '1'
                       FROM   AP_HOLDS_ALL a2
                      WHERE       a2.invoice_id = aia.invoice_id
                              AND a2.invoice_id = aid.invoice_id
                              AND a2.release_lookup_code IS NULL
                              AND a2.HOLD_LOOKUP_CODE <>
                                    'Need AP Manager Approval')
              AND hou.Name = 'RGK_KR_KRW_OU'
   ORDER BY   aia.invoice_num, aid.distribution_line_number;
   /


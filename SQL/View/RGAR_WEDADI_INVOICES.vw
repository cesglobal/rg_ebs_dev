REM =======================================================================
REM = Copyright (C) 2015 RIOT Games Inc.
REM = Los Angeles, CA
REM = All rights reserved
REM =======================================================================
REM
REM PROGRAM NAME: RGCOMN_DEBUG_LOG.sql
REM PURPOSE : To create the custom RGAP_INV_UPLFORM_STG table
REM PARAMETERS : Tablespace
REM CALLED :
REM CALLED BY :
REM
REM UPDATE HISTORY
REM
REM DATE NAME DESCRIPTION
REM --------- --------------- -------------------------------------
REM 05-AUG-15 1.0 INITIAL RELEASE
REM
REM ======================================================================

DROP VIEW APPS.RGAR_COMPANY_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_COMPANY_V
(
   FLEX_VALUE_MEANING,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE   ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_COMPANY'
   --and UPPER(ffvl.description) = UPPER('Korea')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_ACCOUNT_V;


CREATE OR REPLACE FORCE VIEW APPS.RGAR_ACCOUNT_V
(
   SEGMENT2,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning segment2, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_ACCOUNT'
              AND ffvl.enabled_flag = 'Y'
              AND ffvl.COMPILED_VALUE_ATTRIBUTES LIKE ('Y
Y
%')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_DISCIPLINE_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_DISCIPLINE_V
(
   SEGMENT3,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning segment3, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_DISCIPLINE'
              AND ffvl.enabled_flag = 'Y'
              AND ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_TERRITORY_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_TERRITORY_V
(
   SEGMENT4,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning segment4, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_TERRITORY'
              AND ffvl.enabled_flag = 'Y'
              AND ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_PRODUCT_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_PRODUCT_V
(
   SEGMENT5,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning segment5, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_PRODUCT'
              AND ffvl.enabled_flag = 'Y'
              AND ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_INITIATIVE_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_INITIATIVE_V
(
   SEGMENT6,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning segment6, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name = 'RG_INITIATIVE'
              AND ffvl.enabled_flag = 'Y'
              AND ffvl.COMPILED_VALUE_ATTRIBUTES = ('Y
Y')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_INTERCOMPANY_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_INTERCOMPANY_V
(
   FLEX_VALUE_MEANING,
   DESCRIPTION
)
AS
     SELECT   ffvl.flex_value_meaning, ffvl.description
       FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
      WHERE   ffvl.flex_value_set_id = ffvs.flex_value_set_id
              AND ffvs.flex_value_set_name LIKE 'RG_INTERCOMPANY'
   --and UPPER(ffvl.description) = UPPER('Korea')
   ORDER BY   flex_value_meaning;

DROP VIEW APPS.RGAR_FUTURE_V;

CREATE OR REPLACE FORCE VIEW APPS.RGAR_FUTURE_V
(
   SEGMENT6,
   DESCRIPTION
)
AS
   SELECT   ffvl.flex_value_meaning segment6, ffvl.description
     FROM   fnd_flex_values_vl ffvl, fnd_flex_value_sets ffvs
    WHERE       ffvl.flex_value_set_id = ffvs.flex_value_set_id
            AND ffvs.flex_value_set_name = 'RG_FUTURE'
            AND ffvl.enabled_flag = 'Y';


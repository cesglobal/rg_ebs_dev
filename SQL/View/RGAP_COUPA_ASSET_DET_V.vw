/* Formatted on 11-08-2016 22:54:59 (QP5 v5.149.1003.31008) */
DROP VIEW APPS.RGAP_COUPA_ASSET_DET_V;


CREATE OR REPLACE FORCE VIEW APPS.RGAP_COUPA_ASSET_DET_V
AS
   WITH FDDH
           AS (SELECT                   /*+ INDEX(DPRND FA_DEPRN_DETAIL_N2) */
                     ASSET.ASSET_ID Asset_ID,
                      ASSET.TAG_NUMBER TAG_NUMBER,
                      ASSET.SERIAL_NUMBER SERIAL_NUMBER,
                      DIST.UNITS_ASSIGNED QUANTITY,
                      ASSEL.DESCRIPTION Asset_Description,
                      ASSET.ASSET_NUMBER Asset_Number,
                      ASSET.ASSET_Type Asset_Type,
                      ASSET.MANUFACTURER_NAME Manufacturer_Name,
                      ASSET.ATTRIBUTE_CATEGORY_CODE Category,
                      ASSET.CREATION_DATE Creation_Date,
                      PER.BOOK_TYPE_CODE Book_Name,
                      DIST.DISTRIBUTION_ID Distribution_Id,
                      BOOK.SET_OF_BOOKS_ID Ledger_Id,
                      DPRND.PERIOD_COUNTER Period_Counter,
                      DPRND.DEPRN_AMOUNT Depreciation_Amount,
                      (SELECT MAX (fdd.deprn_reserve)
                         FROM apps.fa_deprn_detail fdd,
                              FA.FA_DEPRN_PERIODS LASTP1
                        WHERE     fdd.asset_id = ASSET.asset_id
                              AND fdd.book_type_code = DPRND.BOOK_TYPE_CODE
                              AND fdd.period_counter = LASTP.PERIOD_COUNTER
                              AND LASTP.PERIOD_COUNTER =
                                     LASTP1.PERIOD_COUNTER
                              AND fdd.book_type_code = LASTP1.book_type_code
                              AND EXTRACT (YEAR FROM SYSDATE) = FISCAL_YEAR)
                         YTD,
                      PER.PERIOD_NAME Period_Name,
                      PER.PERIOD_NUM Period_Number,
                      DPRND.YTD_DEPRN Ytd_Depreciation_Amount,
                         fl.segment1
                      || '-'
                      || fl.segment2
                      || '-'
                      || fl.segment3
                      || '-'
                      || fl.segment4
                      || '-'
                      || fl.segment5
                      || fl.segment6
                         location,
                         fak.segment1
                      || '-'
                      || fak.segment2
                      || '-'
                      || fak.segment3
                         Asset_Key,
                         gcc.segment1
                      || '.'
                      || gcc.segment2
                      || '.'
                      || gcc.segment3
                      || '.'
                      || gcc.segment4
                      || '.'
                      || gcc.segment5
                      || '.'
                      || gcc.segment6
                      || '.'
                      || gcc.segment7
                      || '.'
                      || gcc.segment8
                         gl_codes,
                      gcc.segment1 Asset_Company,
                      gcc.segment2 Asset_Account,
                      gcc.segment3 Asset_Discpline
                 FROM FA.FA_ADDITIONS_B ASSET,
                      FA.FA_ADDITIONS_TL ASSEL,
                      FA.FA_DEPRN_DETAIL DPRND,
                      FA.FA_DISTRIBUTION_HISTORY DIST,
                      FA.FA_LOCATIONS FL,
                      FA.FA_DEPRN_PERIODS PER,
                      FA.FA_ASSET_HISTORY AHIST,
                      FA.FA_DEPRN_PERIODS LASTP,
                      FA.FA_BOOK_CONTROLS BOOK,
                      FA.FA_ASSET_KEYWORDS FAK,
                      GL_CODE_COMBINATIONS GCC
                WHERE     PER.BOOK_TYPE_CODE = DPRND.BOOK_TYPE_CODE
                      AND PER.PERIOD_COUNTER = DPRND.PERIOD_COUNTER
                      AND DPRND.ASSET_ID = ASSET.ASSET_ID
                      AND dist.code_combination_id = gcc.code_combination_id
                      AND FAK.CODE_COMBINATION_ID = ASSET.ASSET_KEY_CCID
                      AND DIST.DISTRIBUTION_ID = DPRND.DISTRIBUTION_ID
                      AND DIST.LOCATION_ID = FL.LOCATION_ID
                      AND AHIST.ASSET_ID(+) = DPRND.ASSET_ID
                      AND DIST.DATE_INEFFECTIVE IS NULL
                      AND DPRND.DEPRN_RUN_DATE >= AHIST.DATE_EFFECTIVE(+)
                      AND DPRND.DEPRN_RUN_DATE <
                             NVL (AHIST.DATE_INEFFECTIVE(+), SYSDATE)
                      AND LASTP.BOOK_TYPE_CODE(+) = DPRND.BOOK_TYPE_CODE
                      AND NVL (LASTP.PERIOD_COUNTER, PER.PERIOD_COUNTER) =
                             (SELECT MAX (FDP.PERIOD_COUNTER)
                                FROM FA.FA_DEPRN_PERIODS FDP
                               WHERE FDP.BOOK_TYPE_CODE =
                                        DPRND.BOOK_TYPE_CODE
                                     AND FDP.PERIOD_CLOSE_DATE IS NOT NULL)
                      AND ASSET.ASSET_ID = ASSEL.ASSET_ID
                      AND NVL (DIST.DATE_INEFFECTIVE, '31-DEC-4712') >
                             per.CALENDAR_PERIOD_CLOSE_DATE
                      AND book.book_type_code = dprnd.book_type_code)
     SELECT fac.segment1 Asset_Category,
            FAB.ASSET_ID Asset_ID,
            FDDH.ASSET_NUMBER Asset_Number,
            FDDH.ASSET_Type Asset_Type,
            FDDH.Asset_Company,
            FDDH.Asset_Account,
            FDDH.Asset_Discpline,
            FDDH.Creation_date Creation_Date,
            FDDH.ASSET_DESCRIPTION Asset_Description,
            FDDH.Manufacturer_Name Manufacturer_Name,
            FDDH.Category,
            FDDH.Location,
            FDDH.Asset_Key,
            FDDH.Gl_Codes,
            FDDH.YTD YTD,
            FDDH.TAG_NUMBER TAG_NUMBER,
            FDDH.SERIAL_NUMBER SERIAL_NUMBER,
            FDDH.QUANTITY QUANTITY,
            (SELECT fb.date_placed_in_service
               FROM apps.fa_books fb
              WHERE     fab.asset_id = fb.asset_id
                    AND fb.book_type_code = fddh.book_name
                    AND fb.date_ineffective IS NULL
                    AND fb.transaction_header_id_out IS NULL)
               In_Service_Date,
            (SELECT asp.VENDOR_NAME
               FROM FA.FA_ASSET_INVOICES FAI,
                    FA.FA_TRANSACTION_HEADERS FTH,
                    FA.FA_BOOKS FB,
                    ap.ap_suppliers asp
              WHERE FAI.ASSET_ID = FAB.ASSET_ID
                    AND FAI.DATE_INEFFECTIVE IS NULL
                    AND FAI.INVOICE_TRANSACTION_ID_IN =
                           FTH.INVOICE_TRANSACTION_ID
                    AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                    AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                    AND FAB.ASSET_ID = FB.ASSET_ID
                    AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                    AND FB.DATE_INEFFECTIVE IS NULL
                    AND asp.vendor_id = FAI.PO_VENDOR_ID
                    AND ROWNUM <= 1)
               Supplier,
            (SELECT FAI.INVOICE_NUMBER
               FROM FA.FA_ASSET_INVOICES FAI,
                    FA.FA_TRANSACTION_HEADERS FTH,
                    FA.FA_BOOKS FB
              WHERE FAI.ASSET_ID = FAB.ASSET_ID
                    AND FAI.DATE_INEFFECTIVE IS NULL
                    AND FAI.INVOICE_TRANSACTION_ID_IN =
                           FTH.INVOICE_TRANSACTION_ID
                    AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                    AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                    AND FAB.ASSET_ID = FB.ASSET_ID
                    AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                    AND FB.DATE_INEFFECTIVE IS NULL
                    AND ROWNUM <= 1)
               INVOICE_NUMBER,
            (SELECT FAI.DESCRIPTION
               FROM FA.FA_ASSET_INVOICES FAI,
                    FA.FA_TRANSACTION_HEADERS FTH,
                    FA.FA_BOOKS FB
              WHERE FAI.ASSET_ID = FAB.ASSET_ID
                    AND FAI.DATE_INEFFECTIVE IS NULL
                    AND FAI.INVOICE_TRANSACTION_ID_IN =
                           FTH.INVOICE_TRANSACTION_ID
                    AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                    AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                    AND FAB.ASSET_ID = FB.ASSET_ID
                    AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                    AND FB.DATE_INEFFECTIVE IS NULL
                    AND ROWNUM <= 1)
               DESCRIPTION,
            (SELECT fb.life_in_months
               FROM fa.fa_books fb
              WHERE     fab.asset_id = fb.asset_id
                    AND fb.book_type_code = fddh.book_name
                    AND ROWNUM <= 1)
               Life_In_Months,
            (SELECT fb.deprn_method_code
               FROM fa.fa_books fb
              WHERE     fab.asset_id = fb.asset_id
                    AND fb.book_type_code = fddh.book_name
                    AND ROWNUM <= 1)
               deprn_method_code,
            (SELECT fb.cost
               FROM fa.fa_books fb
              WHERE     fab.asset_id = fb.asset_id
                    AND fb.book_type_code = fddh.book_name
                    AND ROWNUM <= 1)
               cost,
            FDDH.BOOK_NAME Asset_Book,
            FDDH.depreciation_amount Current_Period_Depreciation,
            --FDDH.DISTRIBUTION_ID Distribution_ID,-- 102371 --commented on 161215
            FDDH.LEDGER_ID Ledger_Id,
            FDDH.period_name Period,
            FDDH.period_counter Period_Counter,
            fddh.ytd_depreciation_amount ytd_depreciation,
            (SELECT start_date
               FROM FA_CALENDAR_PERIODS
              WHERE period_name = FDDH.period_name)
               Start_date,
            (SELECT End_date
               FROM FA_CALENDAR_PERIODS
              WHERE period_name = FDDH.period_name)
               End_date,
            (SELECT AIL.ATTRIBUTE15
               FROM FA.FA_ASSET_INVOICES FAI,
                    FA.FA_TRANSACTION_HEADERS FTH,
                    FA.FA_BOOKS FB,
                    AP.AP_INVOICE_LINES_ALL AIL
              WHERE     FAI.ASSET_ID = FAB.ASSET_ID
                    AND FAI.DATE_INEFFECTIVE IS NULL
                    AND FAI.INVOICE_ID = AIL.INVOICE_ID
                    AND FAI.INVOICE_LINE_NUMBER = AIL.LINE_NUMBER
                    AND FAI.INVOICE_TRANSACTION_ID_IN =
                           FTH.INVOICE_TRANSACTION_ID
                    AND FTH.TRANSACTION_TYPE_CODE = 'ADDITION'
                    AND FTH.BOOK_TYPE_CODE = FB.BOOK_TYPE_CODE
                    AND FAB.ASSET_ID = FB.ASSET_ID
                    AND FB.BOOK_TYPE_CODE = FDDH.BOOK_NAME
                    AND FB.DATE_INEFFECTIVE IS NULL
                    AND ROWNUM <= 1)
               Coupa_Expense_Acct,
            FCB.ASSET_COST_ACCT,
            GLCC.SEGMENT1,
            GLCC.segment2,
            glcc.segment3,
            glcc.segment4,
            glcc.segment5,
            glcc.segment6,
            glcc.segment8
       FROM FDDH,
            FA.FA_ADDITIONS_B FAB,
            FA_CATEGORIES_B fac,
            FA.FA_CATEGORY_BOOKS FCB,
            GL_CODE_COMBINATIONS GLCC
      WHERE     FDDH.ASSET_ID = FAB.ASSET_ID
            AND fab.asset_category_id = fac.category_id
            AND FAB.asset_category_id = FCB.category_id
            AND FCB.ASSET_COST_ACCOUNT_CCID = GLCC.CODE_COMBINATION_ID
   ORDER BY 1, 2 ASC;
   
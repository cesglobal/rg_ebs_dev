declare
xx number :=0;
l_ext_pmt_party_id number :=0;
L_instrument_id NUMBER :=0;

cursor c1 is
select * from rgap_supp_emp_stg
where entity = 'Korea'
and ora_vendor_id not in (2401,2404)
and VENDOR_SITE_STATUS_CODE = 'CREATED';

l_group_id              VARCHAR2 (300) := NULL;
      l_resp_id               NUMBER;
      l_resp_appl_id          NUMBER;
      l_user_id               NUMBER;
   BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE UPPER (user_name) = UPPER ('SYSADMIN');

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_resp_appl_id
        FROM fnd_responsibility_vl
       WHERE application_id = 200
             AND responsibility_name = 'Payables Manager';

      mo_global.init ('SQLAP');
      fnd_global.
       apps_initialize (user_id        => l_user_id,
                        resp_id        => l_resp_id,
                        resp_appl_id   => l_resp_appl_id);

FOR I IN  C1 LOOP

begin
SELECT  iepa.ext_payee_id
into l_ext_pmt_party_id
  FROM AP_SUPPLIERS PO,
       AP_SUPPLIER_SITES_ALL POSS,
       iby_external_payees_all iepa
        WHERE     PO.VENDOR_ID = POSS.VENDOR_ID
       AND PO.EMPLOYEE_ID IS NOT NULL       
       AND POSS.vendor_site_id = iepa.supplier_site_id
       AND IEPA.ORG_TYPE = 'OPERATING_UNIT'
       and PO.VENDOR_ID = i.ORA_VENDOR_ID;
exception
when others then
l_ext_pmt_party_id := 0;

end;


begin
select EXT_BANK_ACCOUNT_ID
INTO L_instrument_id
from iby_ext_bank_accounts
       where BANK_ACCOUNT_NUM in (select ACCOUNT_NUMBER from RGAP_SUPP_EMP_STG where entity = 'Korea' and ora_vendor_id not in (2401,2404)
       AND ORA_VENDOR_ID = i.ORA_VENDOR_ID)  
       AND NOT EXISTS (SELECT 1 FROM iby_pmt_instr_uses_all WHERE instrument_id =  EXT_BANK_ACCOUNT_ID)
       ;
exception
when others then
l_ext_pmt_party_id := 0;

end;

   SELECT iby_pmt_instr_uses_all_s.NEXTVAL INTO xx FROM DUAL;
   
   DBMS_OUTPUT.PUT_LINE (XX);

    IF L_instrument_id !=0 AND l_ext_pmt_party_id !=0 THEN

   INSERT INTO iby_pmt_instr_uses_all (instrument_payment_use_id,
                                       ext_pmt_party_id,
                                       instrument_type,
                                       instrument_id,
                                       payment_function,
                                       payment_flow,
                                       order_of_preference,
                                       debit_auth_flag,
                                       debit_auth_method,
                                       debit_auth_reference,
                                       debit_auth_begin,
                                       debit_auth_end,
                                       start_date,
                                       end_date,
                                       created_by,
                                       creation_date,
                                       last_updated_by,
                                       last_update_date,
                                       last_update_login,
                                       object_version_number)
        VALUES (xx,
                l_ext_pmt_party_id,
                'BANKACCOUNT',
                L_instrument_id,
                'PAYABLES_DISB',
                'DISBURSEMENTS',
                1,
                NULL,
                NULL,
                NULL,
                NULL,
                NULL,
                NVL (NULL, SYSDATE),
                NULL,
                fnd_global.user_id,
                SYSDATE,                                       -- bug 13881024
                fnd_global.user_id,
                SYSDATE,
                fnd_global.login_id,
                1);
                
    END IF;
    
             END LOOP;
END;


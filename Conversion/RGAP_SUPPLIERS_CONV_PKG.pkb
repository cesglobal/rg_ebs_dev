CREATE OR REPLACE PACKAGE BODY APPS.rgap_suppliers_conv_pkg
--/********************************************************************************/
--/* */
--/* File Name : XXRG.XXRG_AP_SUPPLIERS_CNV_PKG.pkb */
--/* Object Name : Supplier Conversion */
--/* Created On : 14-Jun-2011 */
--/* Created By : Srinivas Anne */
--/* Purpose : */
--/* */
--/********************************************************************************/
--/* Modification History: */
--/* Version Author Date Comments */
--/* ------- ----------- ----------- ---------------- */
--/********************************************************************************/
IS
   -- ***************************************************************
   -- Declaring the Public variables for WHO Columns and request id
   -- ***************************************************************
   pv_last_update_date        DATE := TRUNC (SYSDATE);
   pv_last_updated_by_num     NUMBER
                                 := TO_NUMBER (fnd_profile.VALUE ('user_id'));
   pv_creation_date           DATE := TRUNC (SYSDATE);
   pv_created_by_num          NUMBER
                                 := TO_NUMBER (fnd_profile.VALUE ('user_id'));
   pv_last_update_login_num   NUMBER
      := TO_NUMBER (fnd_profile.VALUE ('login_id'));
   pv_request_id              NUMBER
      := TO_NUMBER (fnd_profile.VALUE ('conc_request_id'));

   -- ************************************************
   -- Procedure to print messages or notes in the
   -- LOG file of the concurrent program
   -- ************************************************
   PROCEDURE LOG (p_msgtxt_in IN VARCHAR2)
   IS
   BEGIN
      IF fnd_global.conc_login_id = -1
      THEN
         DBMS_OUTPUT.put_line (p_msgtxt_in);
      ELSE
         fnd_file.put_line (fnd_file.LOG, p_msgtxt_in);
      END IF;
   END LOG;

   -- *************************************************
   -- Procedure to print messages or notes in the
   -- OUTPUT file of the concurrent program
   -- ************************************************
   PROCEDURE output (p_msgtxt_in IN VARCHAR2)
   IS
   BEGIN
      IF fnd_global.conc_login_id = -1
      THEN
         DBMS_OUTPUT.put_line (p_msgtxt_in);
      ELSE
         fnd_file.put_line (fnd_file.output, p_msgtxt_in);
      END IF;
   END output;

   -- ***************************************************
   -- Procedure to derive the account code combination id
   -- ***************************************************
   PROCEDURE get_ccid (p_concat_segments   IN     VARCHAR2,
                       p_ccid                 OUT NUMBER,
                       p_msg                  OUT VARCHAR2)
   IS
      lv_concat_segments   VARCHAR2 (259);
      lv_coa_id            NUMBER;
      lv_set_of_books_id   NUMBER;
   BEGIN
      -- ********************************************************
      -- Derive chart of accounts id for the given Operating Unit
      -- ********************************************************
      BEGIN
         lv_coa_id := NULL;

         SELECT gl.chart_of_accounts_id
           INTO lv_coa_id
           FROM gl_ledgers gl, hr_operating_units hou
          WHERE hou.organization_id = fnd_profile.VALUE ('ORG_ID')
                AND hou.set_of_books_id = gl.ledger_id;
      EXCEPTION
         WHEN OTHERS
         THEN
            lv_coa_id := NULL;
      END;

      -- ***************************************************
      -- Derive the CCID for the given concatenated segments
      -- ***************************************************
      p_ccid :=
         fnd_flex_ext.
          get_ccid (
            application_short_name   => 'SQLGL',
            key_flex_code            => 'GL#',
            structure_number         => lv_coa_id,
            validation_date          => TO_CHAR (SYSDATE, 'DD-MON-YYYY'),
            concatenated_segments    => p_concat_segments);
      p_msg := fnd_message.get;
      LOG (p_msg);
   EXCEPTION
      WHEN OTHERS
      THEN
         p_ccid := -1;
         p_msg := SQLERRM;
   END get_ccid;

   -- ************************************************
   -- Procedure to print messages or notes in the
   -- LOG file of the concurrent program
   -- ************************************************
   PROCEDURE vendor_rec_duplicate
   IS
   BEGIN
      BEGIN
         UPDATE rgap_supp_stg
            SET rec_status = 'DUPLICATE'
          WHERE ROWID NOT IN (  SELECT MAX (ROWID)
                                  FROM rgap_supp_stg
                              GROUP BY line_num,
                                       multiple_sites,
                                       legacy_vend_id,
                                       ora_vendor_id,
                                       IDENTIFIER,
                                       vendor_name,
                                       tax_regn_num,
                                       taxpayer_id,
                                       allow_withhold_tax,
                                       --INV_WITHHOLD_TAX_GRP,
                                       tax_regn_num1,
                                       vendor_type);
      EXCEPTION
         WHEN OTHERS
         THEN
            LOG (' Exception raised during Updation for Duplicate:');
            LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
      END;

      COMMIT;
   END vendor_rec_duplicate;

   PROCEDURE supplier_record_count
   IS
      lv_supp_rec_val        NUMBER := 0;
      lv_supp_rec_err        NUMBER := 0;
      lv_dist_supp_rec_val   NUMBER := 0;
      lv_supp_total_rec      NUMBER := 0;
   -- ****************************************************************************
   -- To Count the TOTAL no of Validated and Errored records in the staging table
   -- ****************************************************************************
   BEGIN
      SELECT COUNT (1) INTO lv_supp_total_rec FROM rgap_supp_stg;

      SELECT COUNT (1)
        INTO lv_supp_rec_val
        FROM rgap_supp_stg
       WHERE rec_status = 'VALIDATED';

      SELECT COUNT (1)
        INTO lv_supp_rec_err
        FROM rgap_supp_stg
       WHERE rec_status = 'ERROR';

      SELECT COUNT (1)
        INTO lv_dist_supp_rec_val
        FROM rgap_supp_stg
       WHERE rec_status = 'DUPLICATE';

      LOG (
         '***************************************************************************************');
      LOG (' ');
      LOG (
         ' Total Validated Records and Errored Records in the Staging Table ');
      LOG (' ');
      LOG ('Total Supplier Records to be loaded : ' || lv_supp_total_rec);
      LOG ('Total Supplier Validated Records Count : ' || lv_supp_rec_val);
      LOG (
         'Total Supplier Validation Failed Records Count : '
         || lv_supp_rec_err);
      LOG ('Total Duplicate Records Count : ' || lv_dist_supp_rec_val);
      LOG (' ');
      LOG (
         '***************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in supplier_record_count PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supplier_record_count;

   PROCEDURE supplier_record_exists
   IS
   -- ******************************
   -- Check for the Vendor if Exists
   -- ******************************
   BEGIN
      UPDATE rgap_supp_stg a
         SET rec_status = 'ERROR',
             rec_message = 'Supplier already exist in Oracle'
       WHERE rec_status = 'NEW'
             AND (IDENTIFIER) IN (SELECT (vendor_name)
                                    FROM apps.ap_suppliers b
                                   WHERE (a.IDENTIFIER) = (b.vendor_name));
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (
            ' Exception raised during Updation for supplier and site which already exists in Oracle:');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supplier_record_exists;

   -- *******************************************
   -- Procedure for Tax.
   -- *******************************************
   PROCEDURE validate_tax (lv_retcode   OUT NUMBER,
                           p_party_id       NUMBER,
                           p_amt_ytd        NUMBER,
                           p_taxid          VARCHAR2,
                           p_tax_ref        VARCHAR2)
   IS
      l_init_msg_list                 VARCHAR2 (200);
      l_organization_rec              apps.hz_party_v2pub.organization_rec_type;
      l_party_rec                     apps.hz_party_v2pub.party_rec_type;
      l_party_object_version_number   NUMBER;
      x_profile_id                    NUMBER;
      l_error_message                 VARCHAR2 (2000);
      l_msg_index_out                 NUMBER;
      x_return_status                 VARCHAR2 (200);
      x_msg_count                     NUMBER;
      x_msg_data                      VARCHAR2 (200);
      l_organization_name             VARCHAR2 (200);
      l_created_by_module             VARCHAR2 (200);
   BEGIN
      l_init_msg_list := 1.0;
      l_party_rec.party_id := p_party_id;
      l_party_rec.attribute4 := 'Valid';
      l_organization_rec.party_rec := l_party_rec;
      x_profile_id := NULL;
      x_return_status := NULL;
      x_msg_count := NULL;
      x_msg_data := NULL;

      BEGIN
         SELECT object_version_number
           INTO l_party_object_version_number
           FROM hz_parties
          WHERE party_id = l_party_rec.party_id AND status = 'A';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_party_object_version_number := 0;
      END;

      BEGIN
         SELECT organization_name, created_by_module
           INTO l_organization_name, l_created_by_module
           FROM hz_organization_profiles
          WHERE party_id = p_party_id
                AND SYSDATE BETWEEN effective_start_date
                                AND NVL (effective_end_date, SYSDATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_organization_name := NULL;
            l_created_by_module := NULL;
      END;

      IF l_organization_name IS NOT NULL
      THEN
         l_organization_rec.organization_name := l_organization_name;
         l_organization_rec.created_by_module := l_created_by_module;
         l_organization_rec.jgzz_fiscal_code := p_taxid;
         l_organization_rec.curr_fy_potential_revenue := p_amt_ytd;
         l_organization_rec.tax_reference := p_tax_ref;
         DBMS_OUTPUT.
          put_line (
            'l_party_object_version_number - > '
            || l_party_object_version_number);
         apps.hz_party_v2pub.
          update_organization (
            p_init_msg_list                 => apps.fnd_api.g_true,
            p_organization_rec              => l_organization_rec,
            p_party_object_version_number   => l_party_object_version_number,
            x_profile_id                    => x_profile_id,
            x_return_status                 => x_return_status,
            x_msg_count                     => x_msg_count,
            x_msg_data                      => x_msg_data);
         DBMS_OUTPUT.put_line ('x_profile_id ' || x_profile_id);
         lv_retcode := 0;

         IF x_msg_count > 1
         THEN
            FOR i IN 1 .. x_msg_count
            LOOP
               apps.fnd_msg_pub.get (p_msg_index       => i,
                                     p_encoded         => fnd_api.g_false,
                                     p_data            => x_msg_data,
                                     p_msg_index_out   => l_msg_index_out);

               IF l_error_message IS NULL
               THEN
                  l_error_message := SUBSTR (x_msg_data, 1, 250);
                  lv_retcode := 0;
               ELSE
                  l_error_message :=
                     l_error_message || ' /' || SUBSTR (x_msg_data, 1, 250);
               END IF;
            END LOOP;

            DBMS_OUTPUT.
             put_line ('*****************************************');
            DBMS_OUTPUT.put_line ('API Error : ' || l_error_message);
            DBMS_OUTPUT.
             put_line ('*****************************************');
            ROLLBACK;
         ELSE
            DBMS_OUTPUT.
             put_line ('*****************************************');
            DBMS_OUTPUT.
             put_line (
                  'Attribute4 for Party : '
               || l_party_rec.party_id
               || ' Updated Successfully ');
            DBMS_OUTPUT.
             put_line ('*****************************************');
            COMMIT;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.
          put_line ('Unexpected Error ' || SUBSTR (SQLERRM, 1, 250));
         lv_retcode := 1;
   END;

   -- *******************************************
   -- Procedure to validate the Suppliers Data.
   -- *******************************************
   PROCEDURE validate_suppliers (lv_retcode OUT NUMBER)
   IS
      -- ******************************
      -- Local variable declarations
      -- ******************************
      lv_dist_supp_rec_stg            NUMBER := 0;
      lv_supplier_rec_stg             NUMBER := 0;
      lv_supp_rec_val                 NUMBER := 0;
      lv_supp_rec_err                 NUMBER := 0;
      lv_supp_error_msg               rgap_supp_stg.rec_message%TYPE;
      --lv_supp_site_error_msg rgap_supp_stg.site_error_message%type;
      --lv_site_cont_error_msg rgap_supp_stg.cont_error_message%type;
      lv_rec_status                   rgap_supp_stg.rec_status%TYPE;
      lv_dummy_vendor_type            fnd_lookup_values_vl.lookup_code%TYPE;
      lv_dummy_fob                    fnd_lookup_values_vl.lookup_code%TYPE;
      lv_dummy_ship_via_code          org_freight_tl.freight_code%TYPE;
      lv_terms_id                     ap_terms_tl.term_id%TYPE;
      lv_ship_to_location_code        hr_locations.location_code%TYPE;
      lv_bill_to_location_code        hr_locations.location_code%TYPE;
      lv_pay_group_lookup_code        fnd_lookup_values_vl.lookup_code%TYPE;
      lv_payment_method_lookup_code   fnd_lookup_values_vl.lookup_code%TYPE;
      lv_liability_ccid               gl_code_combinations.code_combination_id%TYPE;
      lv_prepay_ccid                  gl_code_combinations.code_combination_id%TYPE;
      lv_prepay_msg                   VARCHAR2 (2000);
      lv_liability_msg                VARCHAR2 (2000);
      lv_supplier_notif               rgap_supp_stg.supplier_notif_method%TYPE;
      lv_country_code                 rgap_supp_stg.country_code%TYPE;
      lv_dist_supp_rec_val            NUMBER := 0;
      lv_dist_supp_rec_err            NUMBER := 0;
      ln_supp_site_comb               NUMBER;
      ln_length_vendor_site           NUMBER;
      l_installed_flag                VARCHAR2 (10);
      l_language                      VARCHAR2 (30);
      valid_language_flag             BOOLEAN := TRUE;
      lv_tax_retcode                  VARCHAR2 (300) := NULL;

      -- *******************************************************************************
      -- Declaring staging table cursor (Selecting the legacy columns from staging table)
      -- ********************************************************************************
      CURSOR cur_supp
      IS
           SELECT *
             FROM rgap_supp_stg
            WHERE rec_status = 'NEW'
         ORDER BY line_num, IDENTIFIER, vendor_name;
   BEGIN
      -- ************************************************************
      -- To count the total no of new records in the staging table
      -- ************************************************************
      SELECT COUNT (DISTINCT IDENTIFIER)
        INTO lv_dist_supp_rec_stg
        FROM rgap_supp_stg
       WHERE rec_status = 'NEW';

      LOG (
         '************************************************************************************');
      LOG (' ');
      LOG (' Summary of Distinct Supplier records ');
      LOG (' ');
      LOG (
         ' Total Distinct supplier Records Count : ' || lv_dist_supp_rec_stg);
      LOG (' ');
      LOG (
         '************************************************************************************');

      -- ****************************************
      -- Open Cursor for Suppliers Validation
      -- ****************************************
      FOR supp_rec IN cur_supp
      LOOP
         LOG (' lOOP ');
         -- *******************************
         -- Start of validation section
         -- *******************************
         lv_supp_error_msg := NULL;
         --lv_supp_site_error_msg := NULL;
         --lv_site_cont_error_msg := NULL;
         lv_rec_status := 'VALIDATED';
         lv_dummy_vendor_type := NULL;
         lv_dummy_fob := NULL;
         lv_dummy_ship_via_code := NULL;
         lv_terms_id := NULL;
         lv_ship_to_location_code := NULL;
         lv_bill_to_location_code := NULL;
         lv_pay_group_lookup_code := NULL;
         lv_payment_method_lookup_code := NULL;
         lv_liability_ccid := NULL;
         lv_liability_msg := NULL;
         lv_prepay_ccid := NULL;
         lv_prepay_msg := NULL;
         lv_supplier_notif := NULL;
         lv_country_code := NULL;
         ln_length_vendor_site := NULL;
         -- ************************************
         -- To Check if the vendor type is NULL
         -- ************************************
         LOG ('supp_rec.vendor_type ' || supp_rec.vendor_type);

         IF supp_rec.vendor_type IS NOT NULL
         THEN
            BEGIN
               SELECT lookup_code
                 INTO lv_dummy_vendor_type
                 FROM fnd_lookup_values_vl
                WHERE UPPER (lookup_type) = UPPER ('VENDOR TYPE')
                      AND UPPER (meaning) =
                             UPPER (
                                NVL (supp_rec.vendor_type,
                                     supp_rec.vendor_type));

               LOG ('lv_dummy_vendor_type ' || lv_dummy_vendor_type);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.vendor_type
                     || '-Vendor Type lookup code does not exist;';
               WHEN OTHERS
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.vendor_type
                     || '-error while validating Vendor lookup code:';
                  LOG (
                     ' Exception raised in Vendor Type lookup validation code :');
                  LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
            END;
         ELSE
            lv_rec_status := 'ERROR';
            lv_supp_error_msg :=
               lv_supp_error_msg || 'Vendor Type can not be NULL' || '-';
         END IF;

         LOG ('lv_supp_error_msg 1 ' || lv_supp_error_msg);

         -- *********************************
         -- validate Payment Terms
         -- *********************************
         IF supp_rec.terms_code IS NOT NULL
         THEN
            BEGIN
               SELECT term_id
                 INTO lv_terms_id
                 FROM ap_terms_tl
                WHERE UPPER (NAME) = UPPER (supp_rec.terms_code)
                      AND LANGUAGE = USERENV ('LANG');

               LOG ('term_id ' || lv_terms_id);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.terms_code
                     || '- payment terms name does not exist;';
                  lv_terms_id := NULL;
               WHEN OTHERS
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.terms_code
                     || '-error while validating terms name:';
                  lv_terms_id := NULL;
                  LOG (' Exception raised in terms code validation code :');
                  LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
            END;
         END IF;

         LOG ('lv_supp_error_msg 2 ' || lv_supp_error_msg);

         -- *********************************
         -- validate Country Code
         -- *********************************
         IF supp_rec.country_name IS NOT NULL
         THEN
            BEGIN
               SELECT territory_code
                 INTO lv_country_code
                 FROM fnd_territories_tl
                WHERE UPPER (territory_short_name) =
                         UPPER (supp_rec.country_name)
                      AND LANGUAGE = USERENV ('LANG');              -----'US';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.country_name
                     || '- Country name does not exist;';
                  lv_terms_id := NULL;
               WHEN OTHERS
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.country_name
                     || '-error while validating Country:';
                  lv_terms_id := NULL;
                  LOG (' Exception raised in Country code validation code :');
                  LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
            END;
         ELSE
            lv_country_code := NULL;
         END IF;

         LOG ('lv_supp_error_msg 3 ' || lv_supp_error_msg);

         -- *********************************
         -- validate Language Installed
         -- *********************************

         /* IF supp_rec.lang IS NOT NULL
         THEN
         BEGIN
         SELECT language_code,installed_flag
         INTO l_language,l_installed_flag
         FROM apps.fnd_languages
         WHERE upper(nls_language) = upper(REPLACE(REPLACE(REPLACE(supp_rec.lang, CHR(10)), CHR(13)),''''))
         AND nvl(Installed_flag,'I') in ('I','B','D');
         EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         lv_REC_STATUS := 'ERROR';
         lv_supp_site_error_msg := lv_supp_site_error_msg
         || supp_rec.lang
         || ' - Language is not Installed;';
         WHEN OTHERS
         THEN
         lv_REC_STATUS := 'ERROR';
         lv_supp_site_error_msg := lv_supp_site_error_msg
         || supp_rec.lang
         || ' - Language is not Installed:';
         LOG (' Exception raised in Language validation code :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;
         END IF;
         */

         -- *********************************
         -- Derive Prepay Account CCID
         -- *********************************
         IF supp_rec.email_address IS NOT NULL
         THEN
            lv_supplier_notif := 'EMAIL';
         ELSE
            lv_supplier_notif := 'PRINT';
         END IF;

         -- *******************************************************
         -- Update Site Contacts staging table with derived values
         -- *******************************************************
         BEGIN
            UPDATE rgap_supp_stg
               SET /*receipt_required_flag =
               DECODE (supp_rec.match_approval_level,
               '3-Way', 'Y',
               NULL
               ),*/
                  terms_id = lv_terms_id,
                   vendor_type_code = lv_dummy_vendor_type,
                   --liability_ccid = lv_liability_ccid,
                   --prepay_ccid = lv_prepay_ccid,
                   --supplier_notif_method = lv_supplier_notif,
                   --country_code = lv_country_code,
                   --REC_STATUS = lv_REC_STATUS,
                   rec_message = lv_supp_error_msg,
                   --site_error_message = lv_supp_site_error_msg,
                   --cont_error_message = lv_site_cont_error_msg,
                   creation_date = pv_creation_date,
                   created_by = pv_created_by_num,
                   last_update_date = pv_last_update_date,
                   last_updated_by = pv_last_updated_by_num,
                   request_id = pv_request_id,                             --,
                   rec_status = 'VALIDATED'
             WHERE     IDENTIFIER = supp_rec.IDENTIFIER
                   AND line_num = supp_rec.line_num
                   AND rec_status = 'NEW';

            LOG (' UPDATE ');
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG ('Error while updating the staging table rgap_supp_stg:');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         COMMIT;
      END LOOP;                                      -- End Loop For Suppliers
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         LOG (' Exception raised in Suppliers Validation PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END validate_suppliers;

   -- *******************************************
   -- Procedure to Create The suppliers Data.
   -- *******************************************
   PROCEDURE enable_or_create_suppliers
   IS
      -- Declaration for Suppliers
      v_party_id               hz_parties.party_id%TYPE;
      v_supp_return_status     VARCHAR2 (100);
      v_supp_msg_count         NUMBER;
      v_supp_status            VARCHAR2 (1);
      v_supp_msg_data          VARCHAR2 (2000);
      v_supp_msg_data1         VARCHAR2 (2000);
      v_supp_msg_index_out     NUMBER;
      v_supp_timezone_id       NUMBER;
      v_supplier_rec           ap_vendor_pub_pkg.r_vendor_rec_type;
      v_vendor_id              NUMBER;
      lv_party_id              NUMBER;
      lv_vendor_id             NUMBER;
      lv_crt_vendor_id         NUMBER;
      lv_vend_created_rec      NUMBER;
      lv_vend_party_id         NUMBER;
      lv_supp_name             VARCHAR2 (240);
      l_tax_ret_code           NUMBER;
      lv_p_vendor_id           NUMBER := 0;
      lv_p_vend_party_id       NUMBER := 0;
      l_all_wh_tax             VARCHAR2 (300) := 0;
      l_inv_group_id           NUMBER := 0;
      l_pay_group_id           NUMBER := 0;
      l_party_tax_profile_id   NUMBER := 0;
      v_process_flag           VARCHAR2 (300) := NULL;
      l_type_1099              VARCHAR2 (300) := NULL;
      l_vhd_paymethod          VARCHAR2 (300) := NULL;
      l_vhd_gp_database        VARCHAR2 (300) := NULL;
      l_vhd_legacy_vend_id     VARCHAR2 (300) := NULL;
      l_vhd_bank_name          VARCHAR2 (300) := NULL;
      l_vhd_paymethod_code     VARCHAR2 (300) := NULL;
      l_set_of_books_id        NUMBER;

      CURSOR cur_supp_create
      IS
           SELECT *
             FROM rgap_supp_stg supp
            WHERE supp.rec_status = 'VALIDATED' AND vendor_rec_status IS NULL
         ORDER BY supp.line_num, supp.IDENTIFIER, supp.vendor_name;
   BEGIN
      DBMS_OUTPUT.put_line ('CREATE SUPP API');

      FOR supp_rec_create IN cur_supp_create
      LOOP
         fnd_msg_pub.initialize;
         lv_party_id := NULL;
         lv_supp_name := NULL;
         lv_vendor_id := NULL;
         lv_crt_vendor_id := NULL;
         lv_vend_party_id := NULL;
         l_all_wh_tax := NULL;
         l_party_tax_profile_id := NULL;

         BEGIN
            /*SELECT hp.party_id
            INTO lv_party_id
            FROM hz_parties hp
            WHERE upper(party_name) = upper(supp_rec_create.vendor_name)
            AND hp.party_type = 'ORGANIZATION'
            AND hp.hq_branch_ind = 'HQ';

            SELECT hp.party_id,party_name
            INTO lv_party_id,LV_SUPP_NAME
            FROM hz_parties hp
            WHERE DUNS_NUMBER_C = (SELECT ENQUIRY_DUNS -------UPPER(TRIM(ORGANIZATION_NAME))
            FROM APPS.XXHSC_MET_SUPPLIER_DNB_EXT
            WHERE UPPER(TRIM(CUSTOMER_BUSINESS_NAME)) = UPPER(TRIM(supp_rec_create.vendor_name))
            )
            AND hp.party_type = 'ORGANIZATION'
            AND hp.hq_branch_ind = 'HQ';
            */
            SELECT hca.party_id
              INTO lv_party_id
              FROM apps.hz_cust_accounts hca
             WHERE UPPER (orig_system_reference) =
                      UPPER (supp_rec_create.legacy_customer_num);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lv_party_id := NULL;
            WHEN OTHERS
            THEN
               lv_party_id := NULL;
               LOG (
                  ' Exception raised in while deriving the party_id : '
                  || supp_rec_create.IDENTIFIER);
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         lv_supp_name := supp_rec_create.IDENTIFIER;

         BEGIN
            SELECT supp.vendor_id, supp.party_id
              INTO lv_vendor_id, lv_vend_party_id
              FROM ap_suppliers supp
             WHERE (vendor_name) = (supp_rec_create.IDENTIFIER);
         ----upper(supp_rec_create.vendor_name);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lv_vendor_id := NULL;
            WHEN OTHERS
            THEN
               lv_vendor_id := NULL;
               LOG (
                  ' Exception raised in while deriving the vendor id : '
                  || supp_rec_create.vendor_name);
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         IF supp_rec_create.allow_withhold_tax IS NOT NULL
         THEN
            BEGIN
               SELECT DECODE (UPPER (supp_rec_create.allow_withhold_tax),
                              'YES', 'Y',
                              'NO', 'N',
                              'Y', 'Y',
                              'N', 'N',
                              NULL)
                 INTO l_all_wh_tax
                 FROM DUAL;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_all_wh_tax := NULL;
                  LOG (
                     'Allow_Withholding_Tax FLAG is not valid'
                     || supp_rec_create.allow_withhold_tax);
                  LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
            /*l_error_message :=
            l_error_message || 'Allow_Withholding_Tax FLAG is not valid';*/
            END;

            IF supp_rec_create.inv_withhold_tax_grp IS NOT NULL
            THEN
               BEGIN
                  SELECT GROUP_ID
                    INTO l_inv_group_id
                    FROM ap_awt_group_taxes_all
                   WHERE tax_name = supp_rec_create.inv_withhold_tax_grp;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_inv_group_id := NULL;
                     LOG ('VAT CODE is not valid');
               END;
            ELSE
               l_inv_group_id := NULL;
            END IF;
         ELSE
            l_all_wh_tax := NULL;
         END IF;

         ---- Validating VAT CODE -----
         /*IF supp_rec_create.alternate_supplier_name IS NOT NULL
         THEN
         BEGIN
         SELECT supp.vendor_id, supp.party_id
         INTO lv_p_vendor_id, lv_p_vend_party_id
         FROM ap_suppliers supp
         WHERE UPPER (vendor_name) =
         UPPER (TRIM (supp_rec_create.alternate_supplier_name));
         ----upper(supp_rec_create.vendor_name);
         EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
         lv_p_vendor_id := NULL;
         WHEN OTHERS
         THEN
         lv_p_vendor_id := NULL;
         LOG
         ( ' Exception raised in while deriving the Parent vendor id : '
         || supp_rec_create.alternate_supplier_name
         );
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;
         else
         lv_p_vendor_id := null;
         lv_p_vend_party_id := null;
         END IF;
         */
         BEGIN
            IF lv_vendor_id IS NULL
            THEN
               IF supp_rec_create.type_1099 IS NOT NULL
               THEN
                  v_supplier_rec.federal_reportable_flag := 'Y';

                  BEGIN
                     SELECT income_tax_type
                       INTO l_type_1099
                       FROM ap_income_tax_types
                      WHERE income_tax_type =
                               UPPER ('MISC' || supp_rec_create.box_1099);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        v_supplier_rec.federal_reportable_flag := NULL;
                        l_type_1099 := NULL;
                  END;

                  v_supplier_rec.type_1099 := l_type_1099;
               ELSE
                  v_supplier_rec.federal_reportable_flag := NULL;
                  v_supplier_rec.type_1099 := NULL;
               END IF;

               v_supp_msg_count := 0;                --for error message count
               v_supplier_rec.party_id := lv_party_id;
               --v_supplier_rec.vendor_name := supp_rec_create.vendor_name;
               v_supplier_rec.vendor_name := (lv_supp_name);
               v_supplier_rec.vendor_name_alt :=
                  supp_rec_create.alternate_supplier_name;
               v_supplier_rec.vendor_type_lookup_code :=
                  supp_rec_create.vendor_type_code;

               --v_supplier_rec.terms_id := supp_rec_create.terms_id;
               BEGIN
                  SELECT MAX (ho.set_of_books_id)
                    INTO l_set_of_books_id
                    FROM rgap_supp_stg rsg,
                         rgap_supp_site_stg rssg,
                         hr_operating_units ho
                   WHERE     rsg.legacy_vend_id = rssg.legacy_vendor_id
                         AND rsg.IDENTIFIER = rssg.vendor_name
                         AND ho.NAME = rssg.operating_unit
                         AND supp_rec_create.legacy_vend_id =
                                rsg.legacy_vend_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     --l_vhd_paymethod := null;
                     l_set_of_books_id :=
                        fnd_profile.VALUE ('GL_SET_OF_BKS_ID');
               --l_vhd_LEGACY_VEND_ID := null;
               --l_vhd_bank_name := null;
               END;

               v_supplier_rec.set_of_books_id := l_set_of_books_id;
               --fnd_profile.VALUE ('GL_SET_OF_BKS_ID');

               -- v_supplier_rec.ATTRIBUTE1 := supp_rec_create.LEGACY_VEND_ID;
               v_supplier_rec.payment_currency_code :=
                  supp_rec_create.invoice_currency_code;
               v_supplier_rec.invoice_currency_code :=
                  supp_rec_create.payment_currency_code;
               --v_supplier_rec.receipt_required_flag := supp_rec_create.receipt_required_flag;
               v_supplier_rec.inspection_required_flag := 'N';
               v_supplier_rec.offset_tax_flag := 'Y';
               v_supplier_rec.vat_registration_num :=
                  supp_rec_create.tax_regn_num;
               DBMS_OUTPUT.
                put_line (
                  'supp_rec_create.TAX_REGN_NUM '
                  || supp_rec_create.tax_regn_num);

               IF supp_rec_create.inv_withhold_tax_grp IS NOT NULL
               THEN
                  v_supplier_rec.allow_awt_flag := 'Y';
               ELSE
                  IF (UPPER (supp_rec_create.allow_withhold_tax) = 'YES'
                      OR UPPER (supp_rec_create.allow_withhold_tax) = 'Y')
                  THEN
                     v_supplier_rec.allow_awt_flag := 'Y';
                  ELSE
                     v_supplier_rec.allow_awt_flag := NULL;
                  END IF;
               END IF;

               --v_supplier_rec.awt_group_id := l_inv_group_id;
               BEGIN
                  SELECT          --max(TRIM(rssg.payment_method_lookup_code))
                        MAX (rssg.gp_database)
                    --,max(rsg.LEGACY_VEND_ID)
                    --,max(rssg.bank_name)
                    INTO                                     --l_vhd_paymethod
                        l_vhd_gp_database
                    --,l_vhd_LEGACY_VEND_ID
                    --,l_vhd_bank_name
                    FROM rgap_supp_stg rsg, rgap_supp_site_stg rssg
                   WHERE rsg.legacy_vend_id = rssg.legacy_vendor_id
                         AND rsg.IDENTIFIER = rssg.vendor_name
                         AND supp_rec_create.legacy_vend_id =
                                rsg.legacy_vend_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     --l_vhd_paymethod := null;
                     l_vhd_gp_database := NULL;
               --l_vhd_LEGACY_VEND_ID := null;
               --l_vhd_bank_name := null;
               END;

               IF l_vhd_gp_database != ('United States')
               THEN
                  IF l_vhd_gp_database = ('Korea')
                  THEN
                     v_supplier_rec.terms_date_basis := 'Invoice';
                  ELSE
                     v_supplier_rec.terms_date_basis := NULL;
                  END IF;
               ELSE
                  v_supplier_rec.terms_date_basis := 'Invoice Received';
               END IF;

               /*BEGIN
               SELECT payment_method_code
               INTO l_vhd_paymethod_CODE
               --v_supplier_site_rec. .default_pmt_method
               FROM iby_payment_methods_vl
               WHERE UPPER (payment_method_name) =
               UPPER (l_vhd_paymethod);
               EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
               l_vhd_paymethod_CODE := NULL;
               -- v_supplier_site_rec.ext_payee_rec.default_pmt_method := NULL;

               END;

               IF l_vhd_paymethod_CODE IS NULL
               THEN
               IF l_vhd_GP_DATABASE = ('United States')
               THEN
               v_supplier_rec.ext_payee_rec.default_pmt_method :=
               'CHECK';
               ELSE
               IF l_vhd_bank_name IS NOT NULL
               THEN
               v_supplier_rec.ext_payee_rec.default_pmt_method := 'EFT'
               ;
               ELSE
               v_supplier_rec.ext_payee_rec.default_pmt_method :=
               'WIRE';
               END IF;
               END IF;
               ELSE
               v_supplier_rec.ext_payee_rec.default_pmt_method :=
               l_vhd_paymethod_CODE;
               END IF;

               */

               ---v_supplier_rec.ext_payee_rec.default_pmt_method := 'CHECK';

               --v_supplier_rec.PARENT_VENDOR_ID := lv_P_vendor_id;
               /* IF REPLACE (
               REPLACE (
               REPLACE (supp_rec_create.allow_withhold_tax,
               CHR (10)),
               CHR (13)),
               '''') = 'Y'
               THEN
               v_supplier_rec.allow_awt_flag := 'Y';
               END IF;
               */
               /*if REPLACE(REPLACE(REPLACE(supp_rec_create.withhold_tax, CHR(10)), CHR(13)),'''') is not null then
               v_supplier_rec.ALLOW_AWT_FLAG := 'Y';
               end if; */
               --v_supplier_rec.AUTO_TAX_CALC_FLAG := 'Y'; --Added on 4th JAN2013 for Egypt Supplirs, need to be confirm for other countries
               DBMS_OUTPUT.
                put_line ('upper(LV_SUPP_NAME) ' || UPPER (lv_supp_name));
               ap_vendor_pub_pkg.
                create_vendor (
                  p_api_version        => 1.0,
                  p_init_msg_list      => fnd_api.g_false,
                  p_commit             => fnd_api.g_false,
                  p_validation_level   => fnd_api.g_valid_level_full,
                  x_return_status      => v_supp_return_status,
                  x_msg_count          => v_supp_msg_count,
                  -- error message count
                  x_msg_data           => v_supp_msg_data,
                  -- error message
                  p_vendor_rec         => v_supplier_rec,
                  x_vendor_id          => v_vendor_id,
                  -- vendor id created
                  x_party_id           => v_party_id       -- party id created
                                                    );
               COMMIT;

               -------------------
               IF (supp_rec_create.taxpayer_id IS NOT NULL
                   OR supp_rec_create.tax_regn_num IS NOT NULL)
               THEN
                  BEGIN
                     validate_tax (l_tax_ret_code,
                                   v_party_id,
                                   NULL,
                                   supp_rec_create.taxpayer_id,
                                   supp_rec_create.tax_regn_num);
                     COMMIT;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_tax_ret_code := 1;
                        LOG ('l_tax_ret_code --> ' || l_tax_ret_code);
                        COMMIT;
                  END;
               END IF;

               /*IF supp_rec_create.tax_regn_num IS NOT NULL
               THEN*/
               BEGIN
                  SELECT zptp.party_tax_profile_id
                    INTO l_party_tax_profile_id
                    FROM zx_party_tax_profile zptp
                   WHERE     1 = 1
                         AND zptp.party_id = v_party_id
                         AND zptp.party_type_code = 'THIRD_PARTY';
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_party_tax_profile_id := 0;
               END;

               IF l_party_tax_profile_id != 0
               THEN
                  BEGIN
                     zx_party_tax_profile_pkg.
                      update_row -- This API is for Associated party_tax_profile_id on supplier level
                                 (
                        p_party_tax_profile_id           => l_party_tax_profile_id,
                        p_collecting_authority_flag      => NULL,
                        p_provider_type_code             => NULL,
                        p_create_awt_dists_type_code     => NULL,
                        p_create_awt_invoices_type_cod   => NULL,
                        p_tax_classification_code        => NULL,
                        p_self_assess_flag               => 'Y',
                        p_allow_offset_tax_flag          => NULL,
                        p_rep_registration_number        => NULL,
                        p_effective_from_use_le          => NULL,
                        p_record_type_code               => NULL,
                        p_request_id                     => NULL,
                        p_attribute1                     => NULL,
                        p_attribute2                     => NULL,
                        p_attribute3                     => NULL,
                        p_attribute4                     => NULL,
                        p_attribute5                     => NULL,
                        p_attribute6                     => NULL,
                        p_attribute7                     => NULL,
                        p_attribute8                     => NULL,
                        p_attribute9                     => NULL,
                        p_attribute10                    => NULL,
                        p_attribute11                    => NULL,
                        p_attribute12                    => NULL,
                        p_attribute13                    => NULL,
                        p_attribute14                    => NULL,
                        p_attribute15                    => NULL,
                        p_attribute_category             => NULL,
                        p_party_id                       => v_party_id,
                        p_program_login_id               => NULL,
                        p_party_type_code                => NULL,
                        p_supplier_flag                  => NULL,
                        p_customer_flag                  => NULL,
                        p_site_flag                      => NULL,
                        p_process_for_applicability_fl   => 'Y',
                        p_rounding_level_code            => NULL,
                        p_rounding_rule_code             => NULL,
                        p_withholding_start_date         => NULL,
                        p_inclusive_tax_flag             => NULL,
                        p_allow_awt_flag                 => NULL,
                        p_use_le_as_subscriber_flag      => NULL,
                        p_legal_establishment_flag       => NULL,
                        p_first_party_le_flag            => NULL,
                        p_reporting_authority_flag       => NULL,
                        x_return_status                  => v_process_flag,
                        p_registration_type_code         => NULL,
                        p_country_code                   => NULL);
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        LOG ('VAT No Created');
                  END;
               END IF;

               LOG ('v_process_flag ' || v_process_flag);

               --END IF;

               --------------------
               IF v_supp_return_status != apps.fnd_api.g_ret_sts_success
               THEN
                  IF (fnd_msg_pub.count_msg > 0)
                  THEN
                     FOR i IN 1 .. fnd_msg_pub.count_msg
                     LOOP
                        fnd_msg_pub.
                         get (p_msg_index       => i,
                              p_data            => v_supp_msg_data,
                              p_encoded         => 'F',
                              p_msg_index_out   => v_supp_msg_index_out);
                        v_supp_msg_data1 :=
                           v_supp_msg_data1 || ' ' || v_supp_msg_data;
                     END LOOP;
                  END IF;
               END IF;

               IF v_supp_return_status = 'S'
               THEN
                  lv_crt_vendor_id := v_vendor_id;

                  UPDATE rgap_supp_stg
                     SET vendor_rec_status = 'CREATED',
                         ora_vendor_id = lv_crt_vendor_id,
                         ora_party_id = v_party_id
                   WHERE     (IDENTIFIER) = (supp_rec_create.IDENTIFIER)
                         AND rec_status = 'VALIDATED'
                         AND line_num = supp_rec_create.line_num;
               ELSE
                  UPDATE rgap_supp_stg
                     SET vendor_rec_status = 'ERROR',
                         rec_message = v_supp_msg_data
                   WHERE UPPER (IDENTIFIER) =
                            UPPER (supp_rec_create.IDENTIFIER)
                         AND rec_status = 'VALIDATED'
                         AND line_num = supp_rec_create.line_num;

                  LOG (
                     ' Supplier Which got error:'
                     || supp_rec_create.IDENTIFIER);
                  LOG (' Error Message :' || v_supp_msg_data);
               END IF;
            ELSE
               lv_crt_vendor_id := lv_vendor_id;

               UPDATE rgap_supp_stg
                  SET vendor_rec_status = 'CREATED',
                      ora_vendor_id = lv_crt_vendor_id,
                      ora_party_id = lv_vend_party_id
                WHERE     (IDENTIFIER) = (supp_rec_create.IDENTIFIER)
                      AND rec_status = 'VALIDATED'
                      AND line_num = supp_rec_create.line_num;
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG (
                  ' Exception raised in Enabling or creating Supplier PRC :');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;
      END LOOP;

      SELECT COUNT (1)
        INTO lv_vend_created_rec
        FROM rgap_supp_stg
       WHERE vendor_rec_status = 'CREATED';

      LOG (
         '***************************************************************************************');
      LOG (' ');
      LOG (' Total Supplier Records Created ');
      LOG (' ');
      LOG ('Total Supplier Records Created Count : ' || lv_vend_created_rec);
      LOG (' ');
      LOG (
         '***************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in Enabling or creating Supplier PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END enable_or_create_suppliers;

   -- ************************************************
   -- Procedure to print messages or notes in the
   -- LOG file of the concurrent program
   -- ************************************************
   PROCEDURE vendor_site_rec_duplicate
   IS
   BEGIN
      BEGIN
         UPDATE rgap_supp_site_stg
            SET status_code = 'DUPLICATE'
          WHERE ROWID NOT IN (  SELECT MAX (ROWID)
                                  FROM rgap_supp_site_stg
                              GROUP BY operating_unit,
                                       operating_unit_id,
                                       vendor_name,
                                       vendor_id,
                                       trusted_vendor,
                                       legacy_vendor_id,
                                       vendor_site_code,
                                       vendor_site_code_alt,
                                       vendor_site_id,
                                       country,
                                       country_code,
                                       address_line1,
                                       address_line2,
                                       address_line3,
                                       address_line4,
                                       address_lines_alt,
                                       county,
                                       city,
                                       state,
                                       zip,
                                       area_code,
                                       phone,
                                       fax,
                                       fax_area_code,
                                       telex,
                                       ship_via_val,
                                       ship_via_code,
                                       terms_name,
                                       terms_id,
                                       customer_num,
                                       ship_to_location_code,
                                       bill_to_location_code,
                                       ship_to_location_id,
                                       bill_to_location_id,
                                       ship_via_lookup_meaning,
                                       ship_via_lookup_code,
                                       freight_terms_val,
                                       freight_terms_lookup_code,
                                       fob_lookup_val,
                                       fob_lookup_code,
                                       inactive_date,
                                       terms_date_basis,
                                       distribution_set_id,
                                       accts_pay_code_combination,
                                       accts_pay_code_combination_id,
                                       prepay_code_combination,
                                       prepay_code_combination_id,
                                       payment_method_lookup_val,
                                       payment_method_lookup_code,
                                       pay_group_lookup_val,
                                       pay_group_lookup_code,
                                       payment_priority,
                                       invoice_amount_limit,
                                       pay_date_basis_val,
                                       pay_date_basis_lookup_code,
                                       always_take_disc_flag,
                                       invoice_currency_code,
                                       payment_currency_code,
                                       purchasing_site_flag,
                                       rfq_only_site_flag,
                                       pay_site_flag,
                                       attention_ar_flag,
                                       hold_all_payments_flag,
                                       hold_future_payments_flag,
                                       hold_reason,
                                       hold_unmatched_invoices_flag,
                                       tax_reporting_site_flag,
                                       validation_number,
                                       exclude_freight_from_discount,
                                       bank_charge_bearer,
                                       org_id,
                                       check_digits,
                                       allow_awt_flag,
                                       awt_group_val,
                                       awt_group_id,
                                       pay_awt_group_val,
                                       pay_awt_group_id,
                                       default_pay_site_val,
                                       default_pay_site_id,
                                       pay_on_code,
                                       pay_on_receipt_summary_code,
                                       tp_header_id,
                                       ece_tp_location_code,
                                       pcard_site_flag,
                                       match_option,
                                       country_of_origin,
                                       country_of_origin_code,
                                       future_dated_payment_ccid,
                                       create_debit_memo_flag,
                                       supplier_notif_method,
                                       email_address,
                                       primary_pay_site_flag,
                                       shipping_control,
                                       selling_company_identifier,
                                       gapless_inv_num_flag,
                                       location_name,
                                       location_id,
                                       party_site_id,
                                       org_name,
                                       duns_number,
                                       address_style,
                                       LANGUAGE,
                                       withhold_tax,
                                       province,
                                       default_terms_id,
                                       awt_group_name,
                                       pay_awt_group_name,
                                       distribution_set_name,
                                       default_dist_set_id,
                                       default_ship_to_loc_id,
                                       default_bill_to_loc_id,
                                       tolerance_id,
                                       tolerance_name,
                                       vendor_interface_id,
                                       vendor_site_interface_id,
                                       retainage_rate,
                                       services_tolerance_id,
                                       services_tolerance_name,
                                       shipping_location_id,
                                       vat_code,
                                       vat_registration_num,
                                       remittance_email,
                                       edi_id_number,
                                       edi_payment_format,
                                       edi_transaction_handling,
                                       edi_payment_method,
                                       edi_remittance_method,
                                       edi_remittance_instruction,
                                       party_site_name,
                                       offset_tax_flag,
                                       auto_tax_calc_flag,
                                       remit_advice_delivery_method,
                                       remit_advice_fax,
                                       cage_code,
                                       legal_business_name,
                                       doing_bus_as_name,
                                       division_name,
                                       small_business_code,
                                       ccr_comments,
                                       debarment_start_date,
                                       debarment_end_date,
                                       ap_tax_rounding_rule,
                                       amount_includes_tax_flag,
                                       bank_country,
                                       bank_name,
                                       branch_name,
                                       bank_branch,
                                       bank_code_kr,
                                       account_name,
                                       iban,
                                       swift_code,
                                       tax_schedule_id,
                                       branch_type,
                                       sort_code,
                                       bank_account,
                                       gp_database,
                                       attribute1,
                                       attribute2,
                                       attribute3,
                                       attribute4,
                                       attribute5,
                                       attribute6,
                                       attribute7,
                                       attribute8,
                                       attribute9,
                                       attribute10);
      EXCEPTION
         WHEN OTHERS
         THEN
            LOG (' Exception raised during Updation for Duplicate:');
            LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
      END;

      COMMIT;
   END vendor_site_rec_duplicate;

   PROCEDURE supp_site_comb_apps
   IS
      -- *********************************************************************
      -- To Update the Records having the sampe supplier with same vendor_site
      -- *********************************************************************
      v_count   NUMBER;

      CURSOR c1 (
         p_vendor_name        IN VARCHAR2,
         p_vendor_site_code   IN VARCHAR2)
      IS
         SELECT ROWID,
                vendor_name,
                SUBSTR (city, 1, 14) vendor_site_code,
                status_code
           FROM rgap_supp_site_stg
          WHERE     vendor_name = p_vendor_name
                AND city = p_vendor_site_code
                AND status_code = 'NEW';
   BEGIN
      FOR rec IN (  SELECT vendor_name, SUBSTR (city, 1, 14) vendor_site_code
                      FROM rgap_supp_site_stg a
                     WHERE status_code = 'NEW'
                  GROUP BY vendor_name, SUBSTR (city, 1, 14))
      LOOP
         BEGIN
            v_count := 0;

            SELECT COUNT (1)
              INTO v_count
              FROM ap_suppliers b, ap_supplier_sites_all c
             WHERE     b.vendor_id = c.vendor_id
                   --AND c.org_id = fnd_profile.VALUE ('ORG_ID')
                   AND (b.vendor_name) = (rec.vendor_name)
                   AND c.vendor_site_code LIKE rec.vendor_site_code || '%';
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               v_count := 0;
         END;

         IF v_count <> 0
         THEN
            FOR i IN c1 (rec.vendor_name, rec.vendor_site_code)
            LOOP
               UPDATE rgap_supp_site_stg a
                  SET vendor_site_code = SUBSTR (city, 1, 14) || (v_count + 1)
                WHERE     vendor_name = i.vendor_name
                      AND vendor_site_code = i.vendor_site_code
                      AND ROWID = i.ROWID
                      AND status_code = 'NEW';

               COMMIT;
               v_count := v_count + 1;
            END LOOP;
         END IF;

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in supp_site_comb_apps PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supp_site_comb_apps;

   PROCEDURE supp_site_comb
   IS
   -- *********************************************************************
   -- To Update the Records having the sampe supplier with same vendor_site
   -- *********************************************************************
   BEGIN
      FOR rec IN (  SELECT vendor_name, vendor_site_code
                      FROM rgap_supp_site_stg a
                     WHERE status_code = 'NEW'
                  GROUP BY vendor_name, vendor_site_code
                    HAVING COUNT (1) > 1)
      LOOP
         UPDATE rgap_supp_site_stg a
            SET vendor_site_code = vendor_site_code || (ROWNUM + 1)
          WHERE vendor_name IN
                   (  SELECT vendor_name
                        FROM rgap_supp_site_stg a
                       WHERE     vendor_name = rec.vendor_name
                             AND vendor_site_code = rec.vendor_site_code
                             AND status_code = 'NEW'
                    GROUP BY vendor_name, vendor_site_code
                      HAVING COUNT (1) > 1)
                AND vendor_site_code LIKE rec.vendor_site_code || '%'
                AND ROWID NOT IN
                       (  SELECT MAX (ROWID)
                            FROM rgap_supp_site_stg a
                           WHERE     vendor_name = rec.vendor_name
                                 AND vendor_site_code = rec.vendor_site_code
                                 AND status_code = 'NEW'
                        GROUP BY vendor_name, vendor_site_code
                          HAVING COUNT (1) > 1)
                AND status_code = 'NEW';

         COMMIT;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in supp_site_comb PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supp_site_comb;

   PROCEDURE supplier_site_record_count
   IS
      lv_supp_rec_val        NUMBER := 0;
      lv_supp_rec_err        NUMBER := 0;
      lv_dist_supp_rec_val   NUMBER := 0;
      lv_supp_total_rec      NUMBER := 0;
   -- ****************************************************************************
   -- To Count the TOTAL no of Validated and Errored records in the staging table
   -- ****************************************************************************
   BEGIN
      SELECT COUNT (1) INTO lv_supp_total_rec FROM rgap_supp_site_stg;

      SELECT COUNT (1)
        INTO lv_supp_rec_val
        FROM rgap_supp_site_stg
       WHERE status_code = 'VALIDATED';

      SELECT COUNT (1)
        INTO lv_supp_rec_err
        FROM rgap_supp_site_stg
       WHERE status_code = 'ERROR';

      SELECT COUNT (1)
        INTO lv_dist_supp_rec_val
        FROM rgap_supp_site_stg
       WHERE status_code = 'DUPLICATE';

      LOG (
         '***************************************************************************************');
      LOG (' ');
      LOG (
         ' Total Validated Records and Errored Records in the Staging Table ');
      LOG (' ');
      LOG ('Total Supplier Records to be loaded : ' || lv_supp_total_rec);
      LOG ('Total Supplier Validated Records Count : ' || lv_supp_rec_val);
      LOG (
         'Total Supplier Validation Failed Records Count : '
         || lv_supp_rec_err);
      LOG ('Total Duplicate Records Count : ' || lv_dist_supp_rec_val);
      LOG (' ');
      LOG (
         '***************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in supplier_record_count PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supplier_site_record_count;

   PROCEDURE supplier_site_record_exists
   IS
   -- ******************************
   -- Check for the Vendor Site if Exists
   -- ******************************
   BEGIN
      UPDATE rgap_supp_site_stg a
         SET status_code = 'ERROR',
             supp_site_error = 'Supplier and site already exist in Oracle'
       WHERE status_code = 'NEW'
             AND UPPER (vendor_name) IN
                    (SELECT UPPER (vendor_name)
                       FROM apps.ap_suppliers b
                      WHERE UPPER (a.vendor_name) = UPPER (b.vendor_name))
             AND UPPER (vendor_site_code) IN
                    (SELECT UPPER (vendor_site_code)
                       FROM apps.ap_suppliers aps,
                            apps.ap_supplier_sites_all apss
                      WHERE aps.vendor_id = apss.vendor_id
                            AND UPPER (a.vendor_name) =
                                   UPPER (aps.vendor_name)
                            AND UPPER (a.vendor_site_code) =
                                   UPPER (apss.vendor_site_code)
                            AND org_id = fnd_profile.VALUE ('ORG_ID'))
             AND UPPER (address_line1) IN
                    (SELECT UPPER (address_line1)
                       FROM apps.ap_suppliers aps,
                            apps.ap_supplier_sites_all apss
                      WHERE aps.vendor_id = apss.vendor_id
                            AND UPPER (a.vendor_name) =
                                   UPPER (aps.vendor_name)
                            AND UPPER (a.vendor_site_code) =
                                   UPPER (apss.vendor_site_code)
                            AND org_id = fnd_profile.VALUE ('ORG_ID'));
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (
            ' Exception raised during Updation for supplier and site which already exists in Oracle:');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END supplier_site_record_exists;

   PROCEDURE validate_site_data (p_errcode   OUT NUMBER,
                                 p_errmess   OUT VARCHAR2)
   IS
      -- Declaration for supplier sites
      l_site_return_status    VARCHAR2 (1000);
      l_site_msg_count        NUMBER;
      l_site_status           VARCHAR2 (1);
      l_site_msg_data         VARCHAR2 (2000);
      l_site_msg_data1        VARCHAR2 (5000);
      l_site_msg_index_out    NUMBER;
      l_site_timezone_id      NUMBER;
      v_supplier_site_rec     ap_vendor_pub_pkg.r_vendor_site_rec_type;
      l_party_site_id         NUMBER;
      l_vendor_site_id        NUMBER;
      l_location_id           NUMBER;
      l_vend_sit_crt_rec      NUMBER;
      l_status                VARCHAR2 (1000);
      l_errcode               VARCHAR2 (300) := NULL;
      l_errmess               VARCHAR2 (300) := NULL;
      l_verify_flag           VARCHAR2 (10);
      l_error_message         VARCHAR2 (1000);
      l_vendor_site_code      VARCHAR2 (35);
      l_vendor_interface_id   NUMBER;
      l_vendor_id             NUMBER;
      l_territory_code        VARCHAR2 (10);
      l_pay_group             VARCHAR2 (100);
      l_invoice_currency      VARCHAR2 (10);
      l_payment_currency      VARCHAR2 (10);
      l_pay_methods           VARCHAR2 (100);
      l_org_id                NUMBER (10);
      l_term_id               NUMBER (10);
      l_auto_tax_calc_flag    VARCHAR2 (10);

      CURSOR c_supp_date
      IS
         SELECT rgss.ROWID, rgss.*
           FROM rgap_supp_site_stg rgss
          WHERE status_code = 'NEW';
   BEGIN
      FOR s1 IN c_supp_date
      LOOP
         l_verify_flag := NULL;
         l_error_message := NULL;
         l_vendor_id := NULL;

         ----Retriving the vendor id for the vendor -------------------------
         BEGIN
            SELECT vendor_id
              INTO l_vendor_id
              FROM ap_suppliers
             WHERE LTRIM (RTRIM ( (vendor_name))) =
                      LTRIM (RTRIM ( (s1.vendor_name)));

            IF l_vendor_id IS NULL
            THEN
               l_verify_flag := 'E';
               l_error_message := 'Vendor is not exist in system';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_verify_flag := 'E';
               l_error_message := 'Vendor is not exist in system';
            WHEN OTHERS
            THEN
               fnd_file.
                put_line (fnd_file.LOG,
                          'Vendor validation Issue ' || SQLERRM);
         END;

         fnd_file.put_line (fnd_file.LOG, 'Vendor_name ' || s1.vendor_name);

         --------------------Validating territory code--------------------------------
         IF s1.country IS NOT NULL
         THEN
            BEGIN
               SELECT territory_code
                 INTO l_territory_code
                 FROM fnd_territories_tl
                WHERE LTRIM (RTRIM (UPPER (territory_short_name))) =
                         LTRIM (RTRIM (UPPER (s1.country)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message := l_error_message || ' Invalid Country';
                  NULL;
            END;
         ELSE
            l_territory_code := NULL;
         END IF;

         fnd_file.
          put_line (fnd_file.LOG, 'l_territory_code ' || l_territory_code);

         IF s1.pay_group_lookup_code IS NOT NULL
         THEN
            --------------------------- validating Pay group------------------------------------------------------------
            BEGIN
               SELECT lookup_code
                 INTO l_pay_group
                 FROM po_lookup_codes
                WHERE lookup_type = 'PAY GROUP'
                      AND LTRIM (RTRIM (UPPER (lookup_code))) =
                             LTRIM (RTRIM (UPPER (s1.pay_group_lookup_code)));
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'Pay Group not exists ');
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Pay group is invalid ';
            END;
         ELSE
            l_pay_group := NULL;
         END IF;

         ------------------------------validating Invoice currency--------------------------------------------------------------
         IF s1.invoice_currency_code IS NOT NULL
         THEN
            BEGIN
               SELECT currency_code
                 INTO l_invoice_currency
                 FROM fnd_currencies
                WHERE LTRIM (RTRIM (UPPER (currency_code))) =
                         LTRIM (RTRIM (UPPER (s1.invoice_currency_code)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || ' Invoice Currency Code is not Valid';
            END;
         ELSE
            l_invoice_currency := NULL;
         END IF;

         ---------------------------------- validating payment currency---------------------------------------------------------
         IF s1.payment_currency_code IS NOT NULL
         THEN
            BEGIN
               SELECT currency_code
                 INTO l_payment_currency
                 FROM fnd_currencies
                WHERE LTRIM (RTRIM (UPPER (currency_code))) =
                         LTRIM (RTRIM (UPPER (s1.payment_currency_code)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'payment Currency Code is not Valid';
            END;
         ELSE
            l_payment_currency := NULL;
         END IF;

         ------------------------------ validating payment method----------------------------------------------------------------
         IF s1.pay_group_lookup_code IS NOT NULL
         THEN
            BEGIN
               SELECT payment_method_code
                 INTO l_pay_methods
                 FROM iby_payment_methods_vl iby
                WHERE iby.inactive_date IS NULL
                      AND LTRIM (RTRIM (UPPER (payment_method_code))) =
                             LTRIM (RTRIM (UPPER (s1.pay_group_lookup_code)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Payment method is not valid';
            END;
         ELSE
            l_pay_methods := 'CHECK';
         END IF;

         -------------------------------Validating Payment terms-------------------------------------------------------------------
         IF s1.terms_name IS NOT NULL
         THEN
            BEGIN
               SELECT term_id
                 INTO l_term_id
                 FROM ap_terms
                WHERE LTRIM (RTRIM (UPPER (NAME))) =
                         LTRIM (RTRIM (UPPER (s1.terms_name)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Payment Term is not valid';
            END;
         ELSE
            l_term_id := NULL;
         END IF;

         ---------------------------------Retriving org_id for the operating unit-------------------------------------------------------------------
         IF s1.operating_unit IS NOT NULL
         THEN
            BEGIN
               SELECT organization_id
                 INTO l_org_id
                 FROM hr_operating_units
                WHERE LTRIM (RTRIM (UPPER (NAME))) =
                         LTRIM (RTRIM (UPPER (s1.operating_unit)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || ' Operating Unit is Invalid';
            END;
         ELSE
            l_org_id := NULL;
         END IF;

         fnd_file.put_line (fnd_file.LOG, 'l_org_id ' || l_org_id);

         ----------------------------Validating vendor site code-------------------------------------------------------------------------------------
         IF s1.vendor_site_code IS NOT NULL
         THEN
            BEGIN
               SELECT vendor_site_code
                 INTO l_vendor_site_code
                 FROM po_vendor_sites_all a, po_vendors b
                WHERE org_id = l_org_id
                      AND TRIM (vendor_site_code) =
                             TRIM (s1.vendor_site_code)
                      AND a.vendor_id = b.vendor_id
                      AND TRIM (UPPER (b.vendor_name)) =
                             TRIM (UPPER (s1.vendor_name));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_vendor_site_code := NULL;
            END;

            IF l_vendor_site_code IS NOT NULL
            THEN
               l_verify_flag := 'E';
               l_error_message :=
                  l_error_message || ' Vendor Site is already existing';
            END IF;
         END IF;

         IF l_error_message IS NOT NULL
         THEN
            UPDATE rgap_supp_site_stg
               SET error_message = l_error_message, status_code = 'ERROR'
             WHERE ROWID = s1.ROWID;

            p_errcode := 1;
            p_errmess := l_error_message;
         ELSE
            UPDATE rgap_supp_site_stg
               SET vendor_id = l_vendor_id,
                   country_code = l_territory_code,
                   terms_id = l_term_id,
                   operating_unit_id = l_org_id,
                   status_code = 'VALIDATED',
                   vendor_site_code = l_vendor_site_code
             WHERE ROWID = s1.ROWID;

            p_errcode := 0;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errcode := 1;
         p_errmess := SQLERRM;
   END;

   -- Procedure to create new supplier site
   PROCEDURE create_vender_site_contact (p_err_code             OUT VARCHAR2,
                                         p_err_mess             OUT VARCHAR2,
                                         p_vendor_id                NUMBER,
                                         p_vendor_site_id           NUMBER,
                                         p_party_site_id            NUMBER,
                                         p_org_id                   NUMBER,
                                         p_contact_first_name       VARCHAR2,
                                         p_contact_last_name        VARCHAR2,
                                         p_phone_no                 VARCHAR2,
                                         p_ph_ext                   VARCHAR2,
                                         p_alt_ph_no                VARCHAR2,
                                         p_email                    VARCHAR2)
   IS
      p_api_version         NUMBER;
      p_init_msg_list       VARCHAR2 (200);
      p_commit              VARCHAR2 (200);
      p_validation_level    NUMBER;
      x_return_status       VARCHAR2 (200);
      x_msg_count           NUMBER;
      x_msg_data            VARCHAR2 (200);
      lr_vend_cont          apps.ap_vendor_pub_pkg.r_vendor_contact_rec_type;
      x_vendor_contact_id   NUMBER;
      x_per_party_id        NUMBER;
      x_rel_party_id        NUMBER;
      x_rel_id              NUMBER;
      x_org_contact_id      NUMBER;
      x_party_site_id       NUMBER;
      l_msg                 VARCHAR2 (200);
   BEGIN
      -- Initialize apps session
      fnd_global.apps_initialize (1396, 20639, 200);
      mo_global.init ('SQLAP');
      fnd_client_info.set_org_context (p_org_id);
      -- Assign Basic Values
      p_api_version := 1.0;
      p_init_msg_list := fnd_api.g_true;
      p_commit := fnd_api.g_true;
      p_validation_level := fnd_api.g_valid_level_full;
      -- Assign Contact Details
      lr_vend_cont.vendor_id := p_vendor_id;                          --13006;
      lr_vend_cont.vendor_contact_id := NULL;
      lr_vend_cont.vendor_site_id := p_vendor_site_id;                --10042;
      --lr_vend_cont.vendor_id := 13003;
      lr_vend_cont.per_party_id := NULL;
      lr_vend_cont.relationship_id := NULL;
      lr_vend_cont.rel_party_id := NULL;
      lr_vend_cont.party_site_id := p_party_site_id;
      lr_vend_cont.org_contact_id := NULL;
      --lr_vend_cont.org_party_site_id := NULL;
      lr_vend_cont.person_first_name := p_contact_last_name;
      lr_vend_cont.person_middle_name := NULL;
      lr_vend_cont.person_last_name := p_contact_first_name;
      --lr_vend_cont.person_title := 'Mr.';
      lr_vend_cont.organization_name_phonetic := NULL;
      lr_vend_cont.person_first_name_phonetic := NULL;
      lr_vend_cont.person_last_name_phonetic := NULL;
      lr_vend_cont.inactive_date := NULL;
      --lr_vend_cont.url := 'www.test.com';
      lr_vend_cont.email_address := p_email;
      lr_vend_cont.vendor_contact_interface_id := NULL;
      lr_vend_cont.vendor_interface_id := NULL;
      lr_vend_cont.vendor_site_code := NULL;
      lr_vend_cont.org_id := p_org_id;
      lr_vend_cont.operating_unit_name := NULL;
      lr_vend_cont.prefix := NULL;
      --lr_vend_cont.PHONE := p_phone_no||' '||p_ph_ext;
      lr_vend_cont.alt_phone := p_alt_ph_no;
      ap_vendor_pub_pkg.
       create_vendor_contact (p_api_version          => p_api_version,
                              p_init_msg_list        => p_init_msg_list,
                              p_commit               => p_commit,
                              p_validation_level     => p_validation_level,
                              x_return_status        => x_return_status,
                              x_msg_count            => x_msg_count,
                              x_msg_data             => x_msg_data,
                              p_vendor_contact_rec   => lr_vend_cont,
                              x_vendor_contact_id    => x_vendor_contact_id,
                              x_per_party_id         => x_per_party_id,
                              x_rel_party_id         => x_rel_party_id,
                              x_rel_id               => x_rel_id,
                              x_org_contact_id       => x_org_contact_id,
                              x_party_site_id        => x_party_site_id);
      DBMS_OUTPUT.put_line ('X_RETURN_STATUS = ' || x_return_status);
      DBMS_OUTPUT.put_line ('X_MSG_COUNT = ' || x_msg_count);
      DBMS_OUTPUT.put_line ('X_MSG_DATA = ' || x_msg_data);
      DBMS_OUTPUT.put_line ('X_VENDOR_CONTACT_ID = ' || x_vendor_contact_id);
      DBMS_OUTPUT.put_line ('X_PER_PARTY_ID = ' || x_per_party_id);
      DBMS_OUTPUT.put_line ('X_REL_PARTY_ID = ' || x_rel_party_id);
      DBMS_OUTPUT.put_line ('X_REL_ID = ' || x_rel_id);
      DBMS_OUTPUT.put_line ('X_ORG_CONTACT_ID = ' || x_org_contact_id);
      DBMS_OUTPUT.put_line ('X_PARTY_SITE_ID = ' || x_party_site_id);

      IF (x_return_status <> fnd_api.g_ret_sts_success)
      THEN
         FOR i IN 1 .. fnd_msg_pub.count_msg
         LOOP
            l_msg :=
               fnd_msg_pub.get (p_msg_index => i, p_encoded => fnd_api.g_false);
            DBMS_OUTPUT.put_line ('The API call failed with error ' || l_msg);
            p_err_mess := l_msg;
         END LOOP;
      ELSE
         DBMS_OUTPUT.put_line ('The API call ended with SUCESSS status');
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_err_mess := SQLERRM;
   END;

   ------------------
   PROCEDURE supp_site_cont_details
   AS
      p_contact_point_rec   hz_contact_point_v2pub.contact_point_rec_type;
      p_edi_rec             hz_contact_point_v2pub.edi_rec_type;
      p_email_rec           hz_contact_point_v2pub.email_rec_type;
      p_phone_rec           hz_contact_point_v2pub.phone_rec_type;
      p_telex_rec           hz_contact_point_v2pub.telex_rec_type;
      p_web_rec             hz_contact_point_v2pub.web_rec_type;
      x_return_status       VARCHAR2 (2000);
      x_msg_count           NUMBER;
      x_msg_data            VARCHAR2 (2000);
      x_contact_point_id    NUMBER;
      l_error               VARCHAR2 (300) := NULL;

      CURSOR c1
      IS
         SELECT a.ROWID, a.*
           FROM rgap_supp_site_stg a
          WHERE ph_email_status = 'NEW'
                AND vendor_site_status_code = 'CREATED';
   BEGIN
      FOR i IN c1
      LOOP
         l_error := NULL;

         -- Setting the Context --
         --mo_global.init('AR');
         /*fnd_global.apps_initialize ( user_id => 1318
         ,resp_id => 50559
         ,resp_appl_id => 222);*/
         --mo_global.set_policy_context('S',204);
         --fnd_global.set_nls_context('AMERICAN');
         IF i.phone IS NOT NULL
         THEN
            -- Initializing the Mandatory API parameters
            p_contact_point_rec.contact_point_type := 'PHONE';
            p_contact_point_rec.owner_table_name := 'HZ_PARTY_SITES';
            p_contact_point_rec.owner_table_id := i.party_site_id;
            p_contact_point_rec.primary_flag := 'Y';
            --p_contact_point_rec.contact_point_purpose := 'BUSINESS';
            p_contact_point_rec.created_by_module := 'BO_API';
            --p_phone_rec.phone_area_code := NULL;
            --p_phone_rec.phone_country_code := '1';
            p_phone_rec.phone_number := i.phone;
            p_phone_rec.phone_line_type := 'GEN';
            DBMS_OUTPUT.
             put_line (
               'Calling the API hz_contact_point_v2pub.create_contact_point');
            hz_contact_point_v2pub.
             create_contact_point (
               p_init_msg_list       => fnd_api.g_true,
               p_contact_point_rec   => p_contact_point_rec,
               p_edi_rec             => p_edi_rec,
               p_email_rec           => p_email_rec,
               p_phone_rec           => p_phone_rec,
               p_telex_rec           => p_telex_rec,
               p_web_rec             => p_web_rec,
               x_contact_point_id    => x_contact_point_id,
               x_return_status       => x_return_status,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);

            IF x_return_status = fnd_api.g_ret_sts_success
            THEN
               COMMIT;
               DBMS_OUTPUT.
                put_line ('Creation of Contact Point is Successful ');
               DBMS_OUTPUT.put_line ('Output information ....');
               DBMS_OUTPUT.
                put_line ('x_contact_point_id = ' || x_contact_point_id);
            ELSE
               DBMS_OUTPUT.
                put_line (
                  'Creation of Contact Point got failed:' || x_msg_data);
               ROLLBACK;

               FOR i IN 1 .. x_msg_count
               LOOP
                  x_msg_data :=
                     fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                  DBMS_OUTPUT.put_line (i || ') ' || x_msg_data);
                  l_error := l_error || 'PHONE ERROR';
               END LOOP;
            END IF;
         END IF;

         IF i.email_address IS NOT NULL
         THEN
            p_contact_point_rec.contact_point_type := 'EMAIL';
            p_contact_point_rec.owner_table_name := 'HZ_PARTY_SITES';
            p_contact_point_rec.owner_table_id := i.party_site_id;
            p_contact_point_rec.primary_flag := 'Y';
            --p_contact_point_rec.contact_point_purpose := 'BUSINESS';
            p_contact_point_rec.created_by_module := 'BO_API';
            p_email_rec.email_format := 'MAILTEXT';
            p_email_rec.email_address := i.email_address;
            DBMS_OUTPUT.
             put_line (
               'Calling the API hz_contact_point_v2pub.create_contact_point');
            hz_contact_point_v2pub.
             create_contact_point (
               p_init_msg_list       => fnd_api.g_true,
               p_contact_point_rec   => p_contact_point_rec,
               p_edi_rec             => p_edi_rec,
               p_email_rec           => p_email_rec,
               p_phone_rec           => p_phone_rec,
               p_telex_rec           => p_telex_rec,
               p_web_rec             => p_web_rec,
               x_contact_point_id    => x_contact_point_id,
               x_return_status       => x_return_status,
               x_msg_count           => x_msg_count,
               x_msg_data            => x_msg_data);

            IF x_return_status = fnd_api.g_ret_sts_success
            THEN
               COMMIT;
               DBMS_OUTPUT.
                put_line ('Creation of Contact Point is Successful ');
               DBMS_OUTPUT.put_line ('Output information ....');
               DBMS_OUTPUT.
                put_line ('x_contact_point_id = ' || x_contact_point_id);
            ELSE
               DBMS_OUTPUT.
                put_line (
                  'Creation of Contact Point got failed:' || x_msg_data);
               ROLLBACK;

               FOR i IN 1 .. x_msg_count
               LOOP
                  x_msg_data :=
                     fnd_msg_pub.get (p_msg_index => i, p_encoded => 'F');
                  DBMS_OUTPUT.put_line (i || ') ' || x_msg_data);
                  l_error := l_error || 'EMAIL ERROR';
               END LOOP;
            END IF;
         END IF;

         DBMS_OUTPUT.put_line ('Completion of API');

         IF l_error IS NOT NULL
         THEN
            UPDATE rgap_supp_site_stg a
               SET ph_email_status = 'ERROR - ' || l_error
             WHERE ph_email_status = 'NEW' AND ROWID = i.ROWID;
         ELSE
            UPDATE rgap_supp_site_stg a
               SET ph_email_status = 'CREATED'
             WHERE ph_email_status = 'NEW' AND ROWID = i.ROWID;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (SQLERRM);
   END supp_site_cont_details;

   -------------------

   -- Procedure to create new supplier site
   PROCEDURE create_vendor_site
   IS
      -- Declaration for supplier sites
      v_site_return_status    VARCHAR2 (1000);
      v_site_msg_count        NUMBER;
      v_site_status           VARCHAR2 (1);
      v_site_msg_data         VARCHAR2 (2000);
      v_site_msg_data1        VARCHAR2 (5000);
      v_site_msg_index_out    NUMBER;
      v_site_timezone_id      NUMBER;
      v_supplier_site_rec     ap_vendor_pub_pkg.r_vendor_site_rec_type;
      v_party_site_id         NUMBER;
      v_vendor_site_id        NUMBER;
      v_location_id           NUMBER;
      lv_vend_sit_crt_rec     NUMBER;
      lv_status               VARCHAR2 (1000);
      lv_err_buff             VARCHAR2 (300) := NULL;
      l_vendor_site_code      VARCHAR2 (300) := NULL;
      l_tax_class             VARCHAR2 (300) := NULL;
      l_payment_lookup_code   VARCHAR2 (300) := NULL;

      CURSOR cur_site_cont
      IS
           SELECT supp.ROWID, supp.*
             FROM rgap_supp_site_stg supp
            WHERE supp.status_code = 'VALIDATED'
                  AND vendor_site_status_code IS NULL
         ORDER BY supp.record_id, supp.vendor_name;

      l_group_id              VARCHAR2 (300) := NULL;
      l_resp_id               NUMBER;
      l_resp_appl_id          NUMBER;
      l_user_id               NUMBER;
   BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE UPPER (user_name) = UPPER ('SYSADMIN');

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_resp_appl_id
        FROM fnd_responsibility_vl
       WHERE application_id = 200
             AND responsibility_name = 'Payables Manager';

      mo_global.init ('SQLAP');
      fnd_global.
       apps_initialize (user_id        => l_user_id,
                        resp_id        => l_resp_id,
                        resp_appl_id   => l_resp_appl_id);

      FOR cur_site_cont_rec IN cur_site_cont
      LOOP
         fnd_msg_pub.initialize;
         DBMS_OUTPUT.
          put_line (
            ' VENDOR SITE CODE UPDATED TO ' || cur_site_cont_rec.city);

         BEGIN
            v_site_msg_count := 0;
            v_site_return_status := NULL;
            v_site_msg_data := NULL;
            l_vendor_site_code := NULL;
            v_site_return_status := NULL;
            l_payment_lookup_code := NULL;

            IF cur_site_cont_rec.gp_database = ('United States')
            THEN
               l_vendor_site_code :=
                  'RGI_'
                  || REPLACE (
                        NVL (cur_site_cont_rec.vendor_site_code,
                             SUBSTR (cur_site_cont_rec.city, 1, 10)),
                        CHR (32),
                        '_');
            ELSIF cur_site_cont_rec.gp_database = ('RG Merch')
            THEN
               l_vendor_site_code :=
                  'RGM_'
                  || REPLACE (
                        NVL (cur_site_cont_rec.vendor_site_code,
                             SUBSTR (cur_site_cont_rec.city, 1, 10)),
                        CHR (32),
                        '_');
            ELSIF cur_site_cont_rec.gp_database = ('RG Direct')
            THEN
               l_vendor_site_code :=
                  'RGD_'
                  || REPLACE (
                        NVL (cur_site_cont_rec.vendor_site_code,
                             SUBSTR (cur_site_cont_rec.city, 1, 10)),
                        CHR (32),
                        '_');
            ELSIF cur_site_cont_rec.gp_database = ('RG Pictures')
            THEN
               l_vendor_site_code :=
                  'RGP_'
                  || REPLACE (
                        NVL (cur_site_cont_rec.vendor_site_code,
                             SUBSTR (cur_site_cont_rec.city, 1, 10)),
                        CHR (32),
                        '_');
            ELSE
               l_vendor_site_code :=
                  (REPLACE (
                      NVL (cur_site_cont_rec.vendor_site_code,
                           SUBSTR (cur_site_cont_rec.city, 1, 10)),
                      CHR (32),
                      '_'));
            END IF;

            DBMS_OUTPUT.
             put_line (' VENDOR SITE CODE UPDATED TO ' || l_vendor_site_code);

            ---- Validating VAT CODE -----
            IF cur_site_cont_rec.tax_schedule_id IS NOT NULL
            THEN
               BEGIN
                  SELECT meaning
                    INTO l_tax_class
                    FROM fnd_lookups
                   WHERE lookup_type = 'ZX_OUTPUT_CLASSIFICATIONS'
                         AND UPPER (meaning) =
                                UPPER (
                                   TRIM (cur_site_cont_rec.tax_schedule_id));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_tax_class := NULL;
               END;
            ELSE
               l_tax_class := NULL;
            END IF;

            ----- Supplier Site Tax Details ----
            IF cur_site_cont_rec.gp_database NOT IN
                  ('United States', 'Korea')
            THEN
               v_supplier_site_rec.ap_tax_rounding_rule := 'N';
               v_supplier_site_rec.auto_tax_calc_flag := 'Y';
               v_supplier_site_rec.offset_tax_flag := 'Y';
               v_supplier_site_rec.amount_includes_tax_flag := 'N';
            ELSE
               v_supplier_site_rec.ap_tax_rounding_rule := NULL;
               v_supplier_site_rec.auto_tax_calc_flag := NULL;
               v_supplier_site_rec.offset_tax_flag := NULL;
               v_supplier_site_rec.amount_includes_tax_flag := NULL;
            END IF;

            --v_supplier_rec.allow_awt_flag := 'Y';
            IF l_tax_class IS NOT NULL
            THEN
               v_supplier_site_rec.vat_code := l_tax_class;
            --v_supplier_site_rec.allow_awt_flag := 'Y';
            ELSE
               v_supplier_site_rec.vat_code := NULL;
               v_supplier_site_rec.allow_awt_flag := NULL;
            END IF;

            v_supplier_site_rec.vendor_site_code :=
               SUBSTR (l_vendor_site_code, 1, 15);
            v_supplier_site_rec.address_line1 :=
               cur_site_cont_rec.address_line1;
            v_supplier_site_rec.address_line2 :=
               cur_site_cont_rec.address_line2;
            v_supplier_site_rec.address_line3 :=
               cur_site_cont_rec.address_line3;
            v_supplier_site_rec.address_line4 :=
               cur_site_cont_rec.address_line4;
            v_supplier_site_rec.city := cur_site_cont_rec.city;
            v_supplier_site_rec.county := cur_site_cont_rec.county;
            v_supplier_site_rec.state := cur_site_cont_rec.state;
            v_supplier_site_rec.country := cur_site_cont_rec.country_code;
            v_supplier_site_rec.zip := cur_site_cont_rec.zip;
            v_supplier_site_rec.area_code :=
               REPLACE (
                  REPLACE (REPLACE (cur_site_cont_rec.area_code, CHR (10)),
                           CHR (13)),
                  '''');
            --v_supplier_site_rec.phone := NVL(cur_site_cont_rec.phone,'2323232');
            v_supplier_site_rec.fax := cur_site_cont_rec.fax;
            v_supplier_site_rec.fax_area_code :=
               REPLACE (
                  REPLACE (
                     REPLACE (cur_site_cont_rec.fax_area_code, CHR (10)),
                     CHR (13)),
                  '''');
            v_supplier_site_rec.fob_lookup_code :=
               cur_site_cont_rec.fob_lookup_code;
            v_supplier_site_rec.ship_via_lookup_code :=
               cur_site_cont_rec.ship_via_code;
            v_supplier_site_rec.ship_to_location_code :=
               cur_site_cont_rec.ship_to_location_code;
            v_supplier_site_rec.bill_to_location_code :=
               cur_site_cont_rec.bill_to_location_code;
            v_supplier_site_rec.payment_currency_code :=
               cur_site_cont_rec.invoice_currency_code;
            v_supplier_site_rec.invoice_currency_code :=
               cur_site_cont_rec.payment_currency_code;
            v_supplier_site_rec.pay_group_lookup_code :=
               cur_site_cont_rec.pay_group_lookup_code;
            v_supplier_site_rec.party_site_name :=
               REPLACE (cur_site_cont_rec.city, CHR (32), '_');

            /*IF cur_site_cont_rec.GP_DATABASE = ('Korea') THEN
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice';
            ELSE
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice Received';
            END IF;
            */
            IF LENGTH (NVL (cur_site_cont_rec.LANGUAGE, 0)) >= 3
            THEN
               -- IF cur_site_cont_rec.lang IS NOT NULL THEN
               v_supplier_site_rec.LANGUAGE :=
                  REPLACE (
                     REPLACE (REPLACE (cur_site_cont_rec.LANGUAGE, CHR (10)),
                              CHR (13)),
                     '''');
            END IF;

            BEGIN
               SELECT payment_method_code
                 INTO l_payment_lookup_code
                 --v_supplier_site_rec. .default_pmt_method
                 FROM iby_payment_methods_vl
                WHERE UPPER (payment_method_name) =
                         UPPER (cur_site_cont_rec.payment_method_lookup_code);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_payment_lookup_code := NULL;
            -- v_supplier_site_rec.ext_payee_rec.default_pmt_method := NULL;
            END;

            IF l_payment_lookup_code IS NULL
            THEN
               IF cur_site_cont_rec.gp_database = ('United States')
               THEN
                  v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                     'CHECK';
               ELSE
                  IF cur_site_cont_rec.bank_name IS NOT NULL
                  THEN
                     v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                        'EFT';
                  ELSE
                     v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                        'WIRE';
                  END IF;
               END IF;
            ELSE
               v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                  l_payment_lookup_code;
            END IF;

            v_supplier_site_rec.purchasing_site_flag :=
               cur_site_cont_rec.purchasing_site_flag;
            v_supplier_site_rec.pay_site_flag :=
               cur_site_cont_rec.pay_site_flag;
            v_supplier_site_rec.address_style :=
               cur_site_cont_rec.address_style;
            v_supplier_site_rec.match_option := cur_site_cont_rec.match_option;
            v_supplier_site_rec.supplier_notif_method :=
               cur_site_cont_rec.supplier_notif_method;
            v_supplier_site_rec.email_address :=
               cur_site_cont_rec.email_address;
            v_supplier_site_rec.phone := cur_site_cont_rec.phone;
            v_supplier_site_rec.remittance_email :=
               cur_site_cont_rec.remittance_email;

            SELECT DECODE (cur_site_cont_rec.remittance_email,
                           NULL, 'PRINTED',
                           'EMAIL')
              INTO v_supplier_site_rec.remit_advice_delivery_method
              FROM DUAL;

            v_supplier_site_rec.terms_id := cur_site_cont_rec.terms_id;

            IF cur_site_cont_rec.terms_id IS NOT NULL
            THEN
               IF cur_site_cont_rec.gp_database != ('United States')
               THEN
                  IF cur_site_cont_rec.gp_database = ('Korea')
                  THEN
                     v_supplier_site_rec.terms_date_basis := 'Invoice';
                  ELSE
                     v_supplier_site_rec.terms_date_basis := NULL;
                  END IF;
               ELSE
                  v_supplier_site_rec.terms_date_basis := 'Invoice Received';
               END IF;
            ELSE
               --v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice';
               IF cur_site_cont_rec.gp_database != ('United States')
               THEN
                  v_supplier_site_rec.terms_date_basis := 'Invoice';
               ELSE
                  v_supplier_site_rec.terms_date_basis := 'Invoice Received';
               END IF;
            END IF;

            v_supplier_site_rec.vendor_id := cur_site_cont_rec.vendor_id;
            v_supplier_site_rec.vendor_site_code_alt :=
               cur_site_cont_rec.legacy_vendor_id;
            v_supplier_site_rec.org_id := cur_site_cont_rec.operating_unit_id;
            DBMS_OUTPUT.
             put_line (
               'cur_site_cont_rec.operating_unit_id --> '
               || cur_site_cont_rec.operating_unit_id);
            v_supplier_site_rec.prepay_code_combination_id :=
               cur_site_cont_rec.prepay_code_combination_id;
            v_supplier_site_rec.accts_pay_code_combination_id :=
               cur_site_cont_rec.accts_pay_code_combination_id;
            v_supplier_site_rec.vat_registration_num :=
               cur_site_cont_rec.vat_registration_num;

            /*v_supplier_site_rec.attribute1 :=
            cur_site_cont_rec.legacy_vendor_id;*/

            -- Added After discussion with Ravi

            -- v_n_msg_index_out := NULL;
            /*
            if REPLACE(REPLACE(REPLACE(cur_site_cont_rec.withhold_tax, CHR(10)), CHR(13)),'''') is not null then
            v_supplier_site_rec.ALLOW_AWT_FLAG := 'Y';
            SELECT GROUP_ID into v_supplier_site_rec.AWT_GROUP_ID FROM APPS.AP_AWT_GROUPS WHERE upper(NAME) = upper(REPLACE(REPLACE(REPLACE(cur_site_cont_rec.withhold_tax, CHR(10)), CHR(13)),'''')) AND INACTIVE_DATE IS NULL;
            end if;
            */
            /*IF REPLACE (
            REPLACE (
            REPLACE (cur_site_cont_rec.withhold_tax, CHR (10)),
            CHR (13)),
            '''') = 'Y'*/
            IF cur_site_cont_rec.awt_group_val IS NOT NULL
            THEN
               v_supplier_site_rec.allow_awt_flag := 'Y';

               BEGIN
                  SELECT GROUP_ID
                    INTO l_group_id         --v_supplier_site_rec.awt_group_id
                    FROM apps.ap_awt_groups
                   WHERE (NAME) = (cur_site_cont_rec.awt_group_val)
                         AND inactive_date IS NULL;

                  v_supplier_site_rec.awt_group_id := l_group_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_supplier_site_rec.allow_awt_flag := NULL;
                     v_supplier_site_rec.awt_group_id := NULL;
               END;
            ELSE
               IF cur_site_cont_rec.allow_awt_flag = 'Y'
               THEN
                  v_supplier_site_rec.allow_awt_flag := 'Y';
               ELSE
                  v_supplier_site_rec.allow_awt_flag := NULL;
               END IF;

               v_supplier_site_rec.allow_awt_flag := NULL;
               v_supplier_site_rec.awt_group_id := NULL;
            END IF;

            IF REPLACE (
                  REPLACE (
                     REPLACE (cur_site_cont_rec.trusted_vendor, CHR (10)),
                     CHR (13)),
                  '''') = 'Y'
            THEN
               v_supplier_site_rec.auto_tax_calc_flag := 'N';
            END IF;

            ap_vendor_pub_pkg.
             create_vendor_site (
               p_api_version        => 1.0,
               p_init_msg_list      => fnd_api.g_false,
               p_commit             => fnd_api.g_false,
               p_validation_level   => fnd_api.g_valid_level_full,
               x_return_status      => v_site_return_status,
               x_msg_count          => v_site_msg_count,
               x_msg_data           => v_site_msg_data,
               p_vendor_site_rec    => v_supplier_site_rec,
               x_vendor_site_id     => v_vendor_site_id,
               x_party_site_id      => v_party_site_id,
               x_location_id        => v_location_id);
            COMMIT;
            LOG ('v_vendor_site_id ' || v_vendor_site_id);

            /*IF v_party_site_id IS NOT NULL THEN
            if cur_site_cont_rec.v_cnt_phone_no1 is not null then

            supp_site_cont_details (v_party_site_id,null,cur_site_cont_rec.v_cnt_phone_no1);
            commit;
            end if;
            if cur_site_cont_rec.EMAIL_ADDRESS is not null then

            supp_site_cont_details (v_party_site_id,cur_site_cont_rec.EMAIL_ADDRESS,null);
            commit;
            end if;
            END IF;*/
            IF v_site_return_status != apps.fnd_api.g_ret_sts_success
            THEN
               IF (fnd_msg_pub.count_msg > 0)
               THEN
                  FOR i IN 1 .. fnd_msg_pub.count_msg
                  LOOP
                     fnd_msg_pub.
                      get (p_msg_index       => i,
                           p_data            => v_site_msg_data,
                           p_encoded         => 'F',
                           p_msg_index_out   => v_site_msg_index_out);
                     v_site_msg_data1 :=
                        v_site_msg_data1 || ' ' || v_site_msg_data;
                  END LOOP;
               END IF;
            END IF;

            IF cur_site_cont_rec.v_contact_name IS NOT NULL
               AND v_party_site_id IS NOT NULL
            THEN
               BEGIN
                  create_vender_site_contact (
                     lv_err_buff,
                     v_site_msg_data,
                     cur_site_cont_rec.vendor_id,
                     v_vendor_site_id,
                     v_party_site_id,
                     cur_site_cont_rec.operating_unit_id,
                     REPLACE (cur_site_cont_rec.v_contact_name, CHR (32)),
                     NULL,
                     cur_site_cont_rec.v_cnt_phone_no1,
                     cur_site_cont_rec.v_ext,
                     cur_site_cont_rec.v_cnt_phone_no2,
                     cur_site_cont_rec.contact_email);
                  DBMS_OUTPUT.put_line ('v_site_msg_data' || v_site_msg_data);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     v_site_msg_data := v_site_msg_data || SQLERRM;
               END;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG (' Exception raised in loop :');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         IF v_site_return_status = 'S'
         THEN
            UPDATE rgap_supp_site_stg
               SET vendor_site_status_code = 'CREATED',
                   vendor_site_id = v_vendor_site_id,
                   party_site_id = v_party_site_id,
                   location_id = v_location_id,
                   vendor_site_code = l_vendor_site_code
             WHERE UPPER (vendor_name) =
                      UPPER (cur_site_cont_rec.vendor_name)
                   AND status_code = 'VALIDATED'
                   AND record_id = cur_site_cont_rec.record_id;

            COMMIT;
         /* IF v_party_site_id IS NOT NULL THEN
         if cur_site_cont_rec.v_cnt_phone_no1 is not null then

         supp_site_cont_details (v_party_site_id,null,'2314324');
         commit;
         end if;
         if cur_site_cont_rec.EMAIL_ADDRESS is not null then

         supp_site_cont_details (v_party_site_id,'SELECT@TEST.COM',null);
         commit;
         end if;
         END IF;*/
         ELSE
            UPDATE rgap_supp_site_stg
               SET vendor_site_status_code = 'ERROR',
                   error_message =
                      v_site_msg_data || ' ' || v_site_return_status
             WHERE UPPER (vendor_name) =
                      UPPER (cur_site_cont_rec.vendor_name)
                   AND status_code = 'VALIDATED'
                   AND record_id = cur_site_cont_rec.record_id;

            LOG (' supplier : API ' || cur_site_cont_rec.vendor_name);
            LOG (
               ' Site which got errored out for supplier :'
               || cur_site_cont_rec.vendor_site_code);
            LOG (' Error Message :' || v_site_msg_data);
         END IF;

         COMMIT;
      END LOOP;

      SELECT COUNT (1)
        INTO lv_vend_sit_crt_rec
        FROM rgap_supp_site_stg
       WHERE vendor_site_status_code = 'CREATED';

      LOG (
         '***************************************************************************************');
      LOG (' ');
      LOG (' Total Supplier Site Records Created ');
      LOG (' ');
      LOG (
         'Total Supplier Site Records Created Count : '
         || lv_vend_sit_crt_rec);
      LOG (' ');
      LOG (
         '***************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in creating site Cont PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END create_vendor_site;

   PROCEDURE assign_bank_accnts
   IS
      v_error_reason               VARCHAR2 (2000) := NULL;
      v_return_status              VARCHAR2 (2000) := NULL;
      v_msg_count                  NUMBER := NULL;
      v_msg_data                   VARCHAR2 (3000) := NULL;
      v_out_mesg                   apps.iby_fndcpt_common_pub.result_rec_type;
      v_response                   iby_fndcpt_common_pub.result_rec_type;
      v_result_rec_type            iby_fndcpt_common_pub.result_rec_type;
      v_extbank_rec_type           iby_ext_bankacct_pub.extbank_rec_type;
      v_extbankbranch_rec_type     iby_ext_bankacct_pub.extbankbranch_rec_type;
      v_bank_acct_rec              apps.iby_ext_bankacct_pub.extbankacct_rec_type;
      v_assign                     apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
      v_payee_rec                  apps.iby_disbursement_setup_pub.payeecontext_rec_type;
      v_branch_type                VARCHAR2 (2000);
      v_bank_id                    NUMBER;
      v_branch_id                  NUMBER;
      exec_bank_acct               EXCEPTION;
      v_supplier_site_id           NUMBER;
      v_party_site_id              NUMBER;
      v_currency_code              VARCHAR2 (5);
      v_bank_ctry                  VARCHAR2 (5);
      v_bank_name                  VARCHAR2 (50);
      v_bank_branch                VARCHAR2 (50);
      v_msg_dummy                  VARCHAR2 (3000);
      p_location_rec               hz_location_v2pub.location_rec_type;
      v_location_id                NUMBER;
      p_party_site_rec             hz_party_site_v2pub.party_site_rec_type;
      v_party_site_number          NUMBER;
      l_bank_party_id              NUMBER;
      l_branch_party_id            NUMBER;
      l_vendor_id                  NUMBER;
      l_vendor_site_id             NUMBER;
      l_acct                       NUMBER;
      lc_output                    VARCHAR2 (3000);
      lc_msg_dummy                 VARCHAR2 (3000);
      lc_return_status             VARCHAR2 (3000);
      --lc_msg_data VARCHAR2 (3000);
      --v_error_reason VARCHAR2 (2000) := NULL;
      --v_return_status VARCHAR2 (2000) := NULL;
      --v_msg_count NUMBER := NULL;
      --v_msg_data VARCHAR2 (2000) := NULL;
      v_out_mesg                   apps.iby_fndcpt_common_pub.result_rec_type;
      --v_response iby_fndcpt_common_pub.result_rec_type;
      --v_result_rec_type iby_fndcpt_common_pub.result_rec_type;

      --v_extbank_rec_type iby_ext_bankacct_pub.extbank_rec_type;
      --v_extbankbranch_rec_type iby_ext_bankacct_pub.extbankbranch_rec_type;
      v_bank_acct_rec              apps.iby_ext_bankacct_pub.extbankacct_rec_type;
      v_assign                     apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
      v_payee_rec                  apps.iby_disbursement_setup_pub.payeecontext_rec_type;
      --v_branch_type VARCHAR2 (2000);

      --v_bank_id NUMBER;
      --v_branch_id NUMBER;
      exec_bank_acct               EXCEPTION;
      v_supplier_site_id           NUMBER;
      v_party_site_id              NUMBER;
      v_currency_code              VARCHAR2 (5);
      --v_bank_ctry VARCHAR2 (5);
      --v_bank_name VARCHAR2 (50);
      --v_bank_branch VARCHAR2 (50);
      v_msg_dummy                  VARCHAR2 (3000);
      p_location_rec               hz_location_v2pub.location_rec_type;
      v_location_id                NUMBER;
      p_party_site_rec             hz_party_site_v2pub.party_site_rec_type;
      v_party_site_number          NUMBER;
      l_bank_party_id              NUMBER;
      l_branch_party_id            NUMBER;
      l_vendor_id                  NUMBER;
      l_vendor_site_id             NUMBER;
      l_acct                       NUMBER;
      --lc_output VARCHAR2 (3000);
      --lc_msg_dummy VARCHAR2 (3000);
      lc_return_status             VARCHAR2 (3000);
      lc_msg_data                  VARCHAR2 (3000);
      papiversion                  NUMBER := 1.0;
      pinitmsglist                 VARCHAR2 (1) := 'F';
      xreturnstatus                VARCHAR2 (2000);
      xmsgcount                    NUMBER (5);
      xmsgdata                     VARCHAR2 (2000);
      vbankname                    VARCHAR2 (100);
      vvendorname                  VARCHAR2 (100);
      xresponse                    iby_fndcpt_common_pub.result_rec_type;
      pextbankacctrec              iby_ext_bankacct_pub.extbankacct_rec_type;
      vsupplierpartyid             NUMBER;
      -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid                      NUMBER;           -- EXISTING BANK PARTY ID
      vbankbranchid                NUMBER;         -- EXISTING BRANCH PARTY ID
      xacctid                      NUMBER;
      pcount                       NUMBER;
      --vinstrumentid number;
      --pInstrument IBY_FNDCPT_SETUP_PUB.PMTINSTRUMENT_REC_TYPE;
      xassignid                    NUMBER;
      ppayee                       iby_disbursement_setup_pub.payeecontext_rec_type;
      passignmentattribs           iby_fndcpt_setup_pub.pmtinstrassignment_rec_type;
      pinstrument                  iby_fndcpt_setup_pub.pmtinstrument_rec_type;
      vinstrumentid                NUMBER;

      CURSOR c_bank
      IS
         SELECT DISTINCT TRIM ( (mas.vendor_name)) vendor_name,
                         TRIM ( (vendor_site_code)) vendor_site_code,
                         TRIM ( (country_code)) country,
                         TRIM ( (bank_name)) bank_name,
                         TRIM ( (bank_country)) bank_country,
                         TRIM (NVL (mas.bank_code_kr, NULL)) bank_code,
                         vendor_site_id,
                         mas.ROWID b_row_id
           FROM rgap_supp_site_stg mas
          WHERE     bank_status = 'NEW'
                AND vendor_site_status_code = 'CREATED'
                AND account_name IS NOT NULL
                AND bank_account IS NOT NULL;

      CURSOR c_branch
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                rss.BANK_BRANCH bank_branch_no,
                BANK_BRANCH BANK_BRANCH_NUM,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.bank_country))
                   branch_bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift_code bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                rss.account_name,
                rss.bank_account,
                BANK_CODE_KR
           FROM rgap_supp_site_stg rss
          WHERE     bank_branch_status = 'NEW'
                AND vendor_site_status_code = 'CREATED'
                AND account_name IS NOT NULL
                AND bank_account IS NOT NULL;

      CURSOR c_bank_bra_ass
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                BANK_BRANCH BANK_BRANCH_NUM,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.bank_country))
                   bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift_code bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                rss.account_name,
                rss.bank_account,
                operating_unit_id,
                vendor_id,
                vendor_site_id,
                gp_database,
                iban,
                currency_id,
                BANK_CODE_KR
           FROM rgap_supp_site_stg rss
          WHERE     bank_branch_ass_status = 'NEW'
                AND vendor_site_status_code = 'CREATED'
                AND account_name IS NOT NULL
                AND bank_account IS NOT NULL;

      CURSOR c_bank_ven_bra_ass
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.bank_country))
                   bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift_code bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                rss.account_name,
                rss.bank_account,
                rss.party_site_id,
                rss.vendor_site_id,
                rss.operating_unit_id,
                BANK_CODE_KR
           FROM rgap_supp_site_stg rss
          WHERE     bank_vend_ass_status = 'NEW'
                AND vendor_site_status_code = 'CREATED'
                AND account_name IS NOT NULL
                AND bank_account IS NOT NULL;

      l_bank_cnt_code              VARCHAR2 (300) := NULL;
      l_party_site_id              NUMBER := NULL;
      l_e_bank_branch_ass_status   VARCHAR2 (300) := NULL;
      l_e_bank_vend_ass_status     VARCHAR2 (300) := NULL;
   BEGIN
      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Bank Creation code*/
         BEGIN
            FOR rec_c_bank IN c_bank
            LOOP
               v_bank_id := NULL;
               v_bank_ctry := NULL;
               v_bank_name := NULL;
               v_bank_branch := NULL;
               DBMS_OUTPUT.
                put_line ('Bank Loop Started for ' || rec_c_bank.bank_name);
               --DBMS_OUTPUT.PUT_LINE ('Start Of CRAETE BANK');
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;
               v_extbank_rec_type.object_version_number := 1.0;
               v_extbank_rec_type.bank_name := rec_c_bank.bank_name;
               -- v_extbank_rec_type.bank_number := rec_c_bank.bank_routing_num;
               v_extbank_rec_type.institution_type := 'BANK';

               IF rec_c_bank.bank_country IS NOT NULL
               THEN
                  BEGIN
                     SELECT territory_code
                       INTO l_bank_cnt_code
                       FROM fnd_territories_tl
                      WHERE UPPER (territory_short_name) =
                               UPPER (TRIM (rec_c_bank.bank_country))
                            AND LANGUAGE = USERENV ('LANG');        -----'US';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        lc_msg_data :=
                              lc_msg_data
                           || rec_c_bank.bank_country
                           || '- Country name does not exist;';
                     WHEN OTHERS
                     THEN
                        lc_msg_data :=
                              lc_msg_data
                           || rec_c_bank.bank_country
                           || '-error while validating Country:';
                  END;
               ELSE
                  l_bank_cnt_code := NULL;
               END IF;

               v_extbank_rec_type.country_code := /*rec_c_bank.bank_country;*/
                                                 l_bank_cnt_code;
               v_extbank_rec_type.description := NULL;
               v_extbank_rec_type.bank_number := rec_c_bank.bank_code;

               --v_extbank_rec_type.bank_name :=rec_c_bank.bsb;
               BEGIN
                  iby_ext_bankacct_pub.
                   create_ext_bank (p_api_version     => 1.0,
                                    p_init_msg_list   => fnd_api.g_true,
                                    p_ext_bank_rec    => v_extbank_rec_type,
                                    x_bank_id         => v_bank_id,
                                    x_return_status   => v_return_status,
                                    x_msg_count       => v_msg_count,
                                    x_msg_data        => v_msg_data,
                                    x_response        => v_response);

                  COMMIT;

                  UPDATE rgap_supp_site_stg
                     SET bank_id = v_bank_id,
                         bank_status = 'CREATED API',
                         bank_error = v_msg_data
                   WHERE ROWID = rec_c_bank.b_row_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     NULL;
               END;

               DBMS_OUTPUT.
                put_line ('Bank return status ' || v_return_status);
               DBMS_OUTPUT.put_line ('Bank id ' || v_bank_id);

               IF (v_return_status <> 'S')
               THEN
                  FOR i IN 1 .. v_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           apps.fnd_api.g_false,
                                           lc_msg_data,
                                           lc_msg_dummy);
                     lc_output :=
                        lc_output
                        || (   TO_CHAR (i)
                            || ': '
                            || SUBSTR (lc_msg_data, 1, 250));
                  END LOOP;

                  LOG ('Error Occured while Creating Bank: ' || lc_msg_data);
                  -- DBMs_OUTPUT.PUT_LINE( v_return_status );
                  --DBMs_OUTPUT.PUT_LINE( lc_msg_data);
                  DBMS_OUTPUT.put_line (' test s' || v_bank_id);

                  UPDATE rgap_supp_site_stg
                     SET bank_id = v_bank_id, bank_status = 'CREATED'
                   WHERE ROWID = rec_c_bank.b_row_id;

                  COMMIT;
               ELSE
                  DBMS_OUTPUT.put_line (' test f ' || v_bank_id);

                  UPDATE rgap_supp_site_stg
                     SET bank_id = v_bank_id,
                         bank_error = lc_msg_data,
                         bank_status = 'ERROR'
                   WHERE ROWID = rec_c_bank.b_row_id;

                  COMMIT;
               END IF;

               COMMIT;
               fnd_file.
                put_line (fnd_file.LOG, 'BANK API ERROR-' || v_error_reason);
               fnd_file.put_line (fnd_file.LOG, 'End of CRAETE BANK');
               fnd_file.
                put_line (fnd_file.LOG,
                          'Bank Loop Ended for ' || rec_c_bank.bank_name);
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Bank Branch Creation code*/
         BEGIN
            FOR rec_c_branch IN c_branch
            LOOP
               v_branch_id := NULL;
               v_bank_id := NULL;
               v_bank_ctry := NULL;
               v_bank_name := NULL;
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;

               BEGIN
                  DBMS_OUTPUT.
                   put_line (
                     'Retriving the bank_id:' || rec_c_branch.bank_name);
                  DBMS_OUTPUT.
                   put_line (
                     'rec_c_branch.BRANCH_bank_country:'
                     || rec_c_branch.country);

                  SELECT b.bank_party_id, b.home_country, b.bank_name
                    INTO v_bank_id, v_bank_ctry, v_bank_name
                    FROM iby_ext_banks_v b
                   WHERE 1 = 1
                         AND TRIM (UPPER (b.bank_name)) =
                                TRIM (UPPER (rec_c_branch.bank_name))
                         AND TRIM (NVL (bank_number, 1)) =
                                TRIM (NVL (rec_c_branch.BANK_CODE_KR, 1)) -- BANK_CODE_KR
                         AND TRIM ( (b.home_country)) =
                                TRIM ( (rec_c_branch.branch_bank_country));

                  DBMS_OUTPUT.
                   put_line ('Retriving the bank_id:' || v_bank_id);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     DBMS_OUTPUT.
                      put_line (
                           'Bank Not exists in the system :'
                        || rec_c_branch.bank_name
                        || SQLERRM);
                  WHEN OTHERS
                  THEN
                     -- retcode := 1;
                     DBMS_OUTPUT.
                      put_line (
                           'Bank Not exists in the system :'
                        || rec_c_branch.bank_name
                        || SQLERRM);
               END;

               DBMS_OUTPUT.
                put_line (
                     'Start BRANCH API'
                  || ' '
                  || rec_c_branch.branch_name
                  || ' '
                  || v_bank_id);

               IF rec_c_branch.branch_bank_country = 'US'
               THEN
                  v_branch_type := 'ABA';
               ELSE
                  v_branch_type := 'OTHER';
               END IF;

               DBMS_OUTPUT.put_line ('creating new branch through api');
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;
               v_extbankbranch_rec_type.bank_party_id := v_bank_id;
               v_extbankbranch_rec_type.branch_name :=
                  rec_c_branch.bank_branch;
               v_extbankbranch_rec_type.branch_type := v_branch_type;
               v_extbankbranch_rec_type.branch_number := rec_c_branch.bank_branch_no;
               v_extbankbranch_rec_type.bic := rec_c_branch.bank_swiftcode;
               DBMS_OUTPUT.
                put_line (
                     'Bank ID - Branch name'
                  || v_bank_id
                  || ' - '
                  || rec_c_branch.branch_name);
               DBMS_OUTPUT.put_line ('BEFORE CREATE BRANCH ');
               iby_ext_bankacct_pub.
                create_ext_bank_branch (
                  p_api_version           => 1.0,
                  p_init_msg_list         => fnd_api.g_true,
                  p_ext_bank_branch_rec   => v_extbankbranch_rec_type,
                  x_branch_id             => v_branch_id,
                  x_return_status         => v_return_status,
                  x_msg_count             => v_msg_count,
                  x_msg_data              => v_msg_data,
                  x_response              => v_result_rec_type);


               COMMIT;
               DBMS_OUTPUT.
                put_line ('Branch return status ' || v_return_status);
               DBMS_OUTPUT.put_line ('Bank id ' || v_branch_id);

               IF (v_return_status <> 'S')
               THEN
                  FOR i IN 1 .. v_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           apps.fnd_api.g_false,
                                           lc_msg_data,
                                           lc_msg_dummy);
                     lc_output :=
                        lc_output
                        || (   TO_CHAR (i)
                            || ': '
                            || SUBSTR (lc_msg_data, 1, 250));
                  END LOOP;

                  DBMS_OUTPUT.
                   put_line (
                     'Error Occured while Creating Bank Branch: '
                     || lc_msg_data);
                  DBMS_OUTPUT.
                   put_line (
                     'v_branch_id --> ' || v_branch_id || lc_msg_data);

                  UPDATE rgap_supp_site_stg
                     SET bank_branch_status = 'ERROR',
                         bank_branch_error = lc_msg_data,
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id;

                  COMMIT;
               ELSE
                  DBMS_OUTPUT.put_line ('v_branch_id F --> ' || v_branch_id);

                  UPDATE rgap_supp_site_stg
                     SET bank_branch_status = 'CREATED',
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id;
               END IF;

               COMMIT;
               DBMS_OUTPUT.put_line ('End of CRAETE BRANCH');
               DBMS_OUTPUT.
                put_line (
                  'Bank BRANCH Loop Ended for ' || rec_c_branch.branch_name);
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;
         DBMS_OUTPUT.put_line ('c_bank_bra_ass vvendorname ');

         BEGIN
            FOR i IN c_bank_bra_ass
            LOOP
               vvendorname := i.vendor_name;
               vbankname := i.bank_name;
               DBMS_OUTPUT.
                put_line ('c_bank_bra_ass vvendorname ' || i.vendor_name);

               BEGIN
                  SELECT party_id
                    INTO vsupplierpartyid
                    FROM ap_suppliers
                   WHERE TRIM (UPPER (vendor_name)) =
                            TRIM (UPPER (i.vendor_name));

                  DBMS_OUTPUT.put_line (vsupplierpartyid);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vsupplierpartyid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Supplier Details '
                        || SQLERRM);
               END;

               BEGIN
                  SELECT bank_party_id, branch_party_id
                    INTO vbankid, vbankbranchid
                    FROM ce_bank_branches_v
                   WHERE     UPPER (bank_name) = TRIM (UPPER (i.bank_name))
                         AND bank_branch_name = TRIM (i.bank_branch)
                         AND NVL (bank_number, 1) = NVL (i.BANK_CODE_KR, 1)
                         AND NVL (BRANCH_NUMBER, 1) =
                                NVL (i.BANK_BRANCH_NUM, 1);

                  DBMS_OUTPUT.put_line (vbankid || ',' || vbankbranchid);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vbankid := 0;
                     vbankbranchid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Bank Branch Details '
                        || SQLERRM);
               END;

               BEGIN
                  SELECT party_site_id
                    INTO l_party_site_id
                    FROM ap_supplier_sites_all
                   WHERE vendor_id = i.vendor_id
                         AND vendor_site_id = i.vendor_site_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_party_site_id := NULL;
               END;

               ---
               pextbankacctrec.object_version_number := 1.0;
               pextbankacctrec.acct_owner_party_id := vsupplierpartyid;
               pextbankacctrec.bank_account_name := (i.account_name);
               pextbankacctrec.bank_account_num := i.bank_account;
               pextbankacctrec.bank_id := vbankid;
               pextbankacctrec.branch_id := vbankbranchid;
               pextbankacctrec.start_date := SYSDATE;
               pextbankacctrec.country_code := i.bank_country;
               pextbankacctrec.iban := i.iban;

               IF i.gp_database = 'Korea'
               THEN
                  pextbankacctrec.currency := 'KRW';
               ELSE
                  pextbankacctrec.currency := i.currency_id;
               END IF;

               --pExtBankAcctRec.currency := 'USD';
               pextbankacctrec.foreign_payment_use_flag := 'Y';
               iby_ext_bankacct_pub.
                create_ext_bank_acct (
                  p_api_version         => papiversion,
                  p_init_msg_list       => pinitmsglist,
                  p_ext_bank_acct_rec   => pextbankacctrec,
                  p_association_level   => 'SS',
                  p_supplier_site_id    => i.vendor_site_id,
                  p_party_site_id       => l_party_site_id,
                  p_org_id              => i.operating_unit_id,
                  p_org_type            => 'OPERATING_UNIT',
                  --Bug7136876: new parameter
                  x_acct_id             => xacctid,
                  x_return_status       => xreturnstatus,
                  x_msg_count           => xmsgcount,
                  x_msg_data            => xmsgdata,
                  x_response            => xresponse);
               COMMIT;
               DBMS_OUTPUT.put_line ('xReturnStatus :' || xreturnstatus);

               /* DBMS_OUTPUT.put_line ('xMsgCount :' || xmsgcount);
               DBMS_OUTPUT.put_line ('xMsgData :' || xmsgdata);
               DBMS_OUTPUT.put_line ('xAcctID :' || xacctid);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Code :' || xresponse.result_code);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Category :' || xresponse.result_category);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Message :' || xresponse.result_message);
               */
               IF xreturnstatus = 'S'
               THEN
                  UPDATE rgap_supp_site_stg
                     SET bank_branch_ass_status = 'CREATED'
                   WHERE ROWID = i.br_row_id;

                  COMMIT;
               ELSE
                  IF xmsgcount > 1
                  THEN
                     FOR i IN 1 .. xmsgcount
                     LOOP
                        DBMS_OUTPUT.
                         put_line (
                           i || '.'
                           || SUBSTR (
                                 fnd_msg_pub.
                                  get (p_encoded => fnd_api.g_false),
                                 1,
                                 255));
                     END LOOP;

                     UPDATE rgap_supp_site_stg
                        SET bank_branch_ass_status = 'ERROR',
                            bank_br_ass_status_error = 'Not Assigned'
                      WHERE ROWID = i.br_row_id;

                     COMMIT;
                  END IF;
               --ROLLBACK;
               END IF;
            ----
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Vendor Bank and Branch assignment code*/
         BEGIN
            FOR i IN c_bank_ven_bra_ass
            LOOP
               vvendorname := i.vendor_name;
               vbankname := i.bank_name;

               BEGIN
                  SELECT bank_party_id, branch_party_id
                    INTO vbankid, vbankbranchid
                    FROM ce_bank_branches_v
                   WHERE UPPER (bank_name) = UPPER (TRIM (vbankname))
                         AND (TRIM (bank_branch_name)) =
                                (TRIM (i.branch_name));
               -- Replace with your Bank Name
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vbankid := 0;
                     vbankbranchid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while Bank Branch Details ' || SQLERRM);
               END;

               -- SELECT EXISTING Supplier
               BEGIN
                  SELECT ext_bank_account_id
                    INTO vinstrumentid
                    FROM iby_ext_bank_accounts
                   WHERE branch_id = vbankbranchid AND bank_id = vbankid
                         AND (TRIM (bank_account_name)) =
                                (TRIM (i.account_name));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vinstrumentid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching BANK ACCOUNT Details '
                        || SQLERRM);
               END;

               -- SELECT EXISTING Supplier
               BEGIN
                  SELECT party_id
                    INTO vsupplierpartyid
                    FROM ap_suppliers
                   WHERE TRIM (UPPER (vendor_name)) =
                            TRIM (UPPER (vvendorname));
               -- Replace with your Supplier Name
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vsupplierpartyid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Supplier Details '
                        || SQLERRM);
               END;

               pinstrument.instrument_type := 'BANKACCOUNT';
               pinstrument.instrument_id := vinstrumentid;
               --<bank account id> IBY_EXT_BANKACCT_PUB.CREATE_EXT_BANK_ACCT API account id
               passignmentattribs.start_date := SYSDATE;
               passignmentattribs.instrument := pinstrument;
               ppayee.party_id := vsupplierpartyid;
               ppayee.party_site_id := i.party_site_id;
               ppayee.supplier_site_id := i.vendor_site_id;
               ppayee.payment_function := 'PAYABLES_DISB';
               ppayee.org_id := i.operating_unit_id;        -- Organization ID

               -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID -- which is party_id of the supplier

               /*

               p_payee.Supplier_Site_id :=4394811;--4296811; --- vendor Site ID
               p_payee.Party_Id := 4466079;-- Party ID of Supplier;
               p_payee.Party_Site_Id :=4359562;-- Party Side ID
               p_payee.Payment_Function := 'PAYABLES_DISB';
               p_payee.Org_Id := 86; -- Organization ID
               p_payee.Org_Type := 'OPERATING_UNIT';
               p_instrument.Instrument_Id := 74409;--x_acct_id; -- Ext Bank Account ID
               p_instrument.Instrument_Type := 'BANKACCOUNT';
               --p_assignment_attribs.priority := 1;
               p_assignment_attribs.end_date := SYSDATE+3;
               p_assignment_attribs.Instrument := p_instrument;
               x_msg_count := 0;
               x_msg_data := null;
               x_return_status := null;
               x_response_rec := null;

               */

               /*iby_disbursement_setup_pub.
               set_payee_instr_assignment (
               p_api_version => 1.0,
               p_init_msg_list => fnd_api.g_false,
               p_commit => fnd_api.g_true,
               x_return_status => xreturnstatus,
               x_msg_count => xmsgcount,
               x_msg_data => xmsgdata,
               p_payee => ppayee,
               p_assignment_attribs => passignmentattribs,
               x_assign_id => xassignid,
               x_response => xresponse);
              */
               --COMMIT;

               /*DBMS_OUTPUT.
               put_line ('xReturnStatus :' || xreturnstatus);
               DBMS_OUTPUT.put_line ('xMsgCount :' || xmsgcount);
               DBMS_OUTPUT.put_line ('x_assign_id :' || xassignid);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Code :' || xresponse.result_code);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Category :' || xresponse.result_category);
               DBMS_OUTPUT.
               put_line (
               'xResponse.Result_Message :' || xresponse.result_message);
               */
               IF xreturnstatus = 'S'
               THEN
                  UPDATE rgap_supp_site_stg
                     SET bank_vend_ass_status = 'CREATED'
                   WHERE ROWID = i.br_row_id;

                  COMMIT;
               ELSE
                  IF xmsgcount > 1
                  THEN
                     FOR i IN 1 .. xmsgcount
                     LOOP
                        DBMS_OUTPUT.
                         put_line (
                           i || '.'
                           || SUBSTR (
                                 fnd_msg_pub.
                                  get (p_encoded => fnd_api.g_false),
                                 1,
                                 255));
                     END LOOP;

                     UPDATE rgap_supp_site_stg
                        SET bank_vend_ass_status = 'ERROR'
                      WHERE ROWID = i.br_row_id;
                  END IF;

                  COMMIT;
               END IF;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
      END;
   END assign_bank_accnts;

   -- *********************************************************************************************
   -- MAIN Procedure To Call the validation program and Insertion Program Based on Parameter Value
   -- *********************************************************************************************
   PROCEDURE main (x_errbuf OUT VARCHAR2, x_retcode OUT NUMBER)
   AS
      lv_retcode   NUMBER := 0;
      lv_errmess   VARCHAR2 (3000) := NULL;
      lv_errcode   VARCHAR2 (300) := 0;
   BEGIN
      vendor_rec_duplicate;
      supplier_record_exists;
      LOG (
         '******************** EXECUTION OF SUPPLIER VALIDATION PROCEDURE *****************');
      validate_suppliers (lv_retcode);

      IF lv_retcode = 1
      THEN
         x_retcode := 1;
      ELSE
         x_retcode := 0;
      END IF;

      LOG ('x_retcode ' || x_retcode);
      supplier_record_count;
      LOG (
         '******************** EXECUTION OF SUPPLIER CREATION PROCEDURE *****************');
      enable_or_create_suppliers;
      COMMIT;
      LOG (
         '******************** EXECUTION OF SUPPLIER SITE CREATION PROCEDURE *****************');
      vendor_site_rec_duplicate;
      supplier_site_record_exists;
      supp_site_comb_apps;
      COMMIT;
      supp_site_comb;
      LOG (
         '******************** EXECUTION OF SUPPLIER VALIDATION PROCEDURE *****************');
      validate_site_data (lv_retcode, lv_errmess);

      IF lv_retcode = 1
      THEN
         x_retcode := 1;
      ELSE
         x_retcode := 0;
      END IF;

      supplier_site_record_count;
      LOG (
         '******************** EXECUTION OF SUPPLIER SITE CREATION PROCEDURE *****************');

      IF x_retcode = 0
      THEN
         create_vendor_site;
      END IF;

      --supp_site_cont_details;
      COMMIT;
      assign_bank_accnts;
      COMMIT;
   END main;

   -----Employee Vendor Conversion -----

   -- ************************************************
   -- Procedure to print messages or notes in the
   -- LOG file of the concurrent program
   -- ************************************************
   PROCEDURE emp_vendor_rec_duplicate
   IS
   BEGIN
      BEGIN
         UPDATE rgap_supp_emp_stg
            SET rec_status = 'DUPLICATE'
          WHERE ROWID NOT IN (  SELECT MAX (ROWID)
                                  FROM rgap_supp_emp_stg
                              GROUP BY entity,
                                       TRIM (vendor_name)      --,GP_Vendor_ID
                                                         ,
                                       employee_name,
                                       employee_number,
                                       vendor_type,
                                       site_name,
                                       country_name,
                                       bank_name,
                                       branch_name,
                                       account_number,
                                       account_name,
                                       terms_code,
                                       payment_method,
                                       pay_group,
                                       iban,
                                       currency,
                                       branch_number,
                                       bank_code,
                                       swift,
                                       address1,
                                       address2,
                                       address3,
                                       address4);
      EXCEPTION
         WHEN OTHERS
         THEN
            LOG (' Exception raised during Updation for Duplicate:');
            LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
      END;

      COMMIT;
   END emp_vendor_rec_duplicate;

   PROCEDURE emp_supplier_record_count
   IS
      lv_supp_rec_val        NUMBER := 0;
      lv_supp_rec_err        NUMBER := 0;
      lv_dist_supp_rec_val   NUMBER := 0;
      lv_supp_total_rec      NUMBER := 0;
   -- ****************************************************************************
   -- To Count the TOTAL no of Validated and Errored records in the staging table
   -- ****************************************************************************
   BEGIN
      SELECT COUNT (1) INTO lv_supp_total_rec FROM rgap_supp_emp_stg;

      SELECT COUNT (1)
        INTO lv_supp_rec_val
        FROM rgap_supp_emp_stg
       WHERE rec_status = 'VALIDATED';

      SELECT COUNT (1)
        INTO lv_supp_rec_err
        FROM rgap_supp_emp_stg
       WHERE rec_status = 'ERROR';

      SELECT COUNT (1)
        INTO lv_dist_supp_rec_val
        FROM rgap_supp_emp_stg
       WHERE rec_status = 'DUPLICATE';
   /*
   LOG (
   '***************************************************************************************');
   LOG (' ');
   LOG (
   ' Total Validated Records and Errored Records in the Staging Table ');
   LOG (' ');
   LOG ('Total Supplier Records to be loaded : ' || lv_supp_total_rec);
   LOG ('Total Supplier Validated Records Count : ' || lv_supp_rec_val);
   LOG (
   'Total Supplier Validation Failed Records Count : '
   || lv_supp_rec_err);
   LOG ('Total Duplicate Records Count : ' || lv_dist_supp_rec_val);
   LOG (' ');
   LOG (
   '***************************************************************************************');
   */
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in supplier_record_count PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END emp_supplier_record_count;

   PROCEDURE emp_supplier_record_exists
   IS
   -- ******************************
   -- Check for the Vendor if Exists
   -- ******************************
   BEGIN
      UPDATE rgap_supp_emp_stg a
         SET rec_status = 'ERROR',
             rec_message = 'Supplier already exist in Oracle'
       WHERE rec_status = 'NEW'
             AND TRIM (vendor_name) IN
                    (SELECT TRIM (vendor_name)
                       FROM apps.ap_suppliers b
                      WHERE TRIM (a.vendor_name) = TRIM (b.vendor_name));
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (
            ' Exception raised during Updation for supplier and site which already exists in Oracle:');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END emp_supplier_record_exists;

   -- *******************************************
   -- Procedure for Tax.
   -- *******************************************
   PROCEDURE emp_validate_tax (lv_retcode   OUT NUMBER,
                               p_party_id       NUMBER,
                               p_amt_ytd        NUMBER,
                               p_taxid          VARCHAR2,
                               p_tax_ref        VARCHAR2)
   IS
      l_init_msg_list                 VARCHAR2 (200);
      l_organization_rec              apps.hz_party_v2pub.organization_rec_type;
      l_party_rec                     apps.hz_party_v2pub.party_rec_type;
      l_party_object_version_number   NUMBER;
      x_profile_id                    NUMBER;
      l_error_message                 VARCHAR2 (2000);
      l_msg_index_out                 NUMBER;
      x_return_status                 VARCHAR2 (200);
      x_msg_count                     NUMBER;
      x_msg_data                      VARCHAR2 (200);
      l_organization_name             VARCHAR2 (200);
      l_created_by_module             VARCHAR2 (200);
   BEGIN
      l_init_msg_list := 1.0;
      l_party_rec.party_id := p_party_id;
      l_party_rec.attribute4 := 'Valid';
      l_organization_rec.party_rec := l_party_rec;
      x_profile_id := NULL;
      x_return_status := NULL;
      x_msg_count := NULL;
      x_msg_data := NULL;

      BEGIN
         SELECT object_version_number
           INTO l_party_object_version_number
           FROM hz_parties
          WHERE party_id = l_party_rec.party_id AND status = 'A';
      EXCEPTION
         WHEN OTHERS
         THEN
            l_party_object_version_number := 0;
      END;

      BEGIN
         SELECT organization_name, created_by_module
           INTO l_organization_name, l_created_by_module
           FROM hz_organization_profiles
          WHERE party_id = p_party_id
                AND SYSDATE BETWEEN effective_start_date
                                AND NVL (effective_end_date, SYSDATE);
      EXCEPTION
         WHEN OTHERS
         THEN
            l_organization_name := NULL;
            l_created_by_module := NULL;
      END;

      IF l_organization_name IS NOT NULL
      THEN
         l_organization_rec.organization_name := l_organization_name;
         l_organization_rec.created_by_module := l_created_by_module;
         l_organization_rec.jgzz_fiscal_code := p_taxid;
         l_organization_rec.curr_fy_potential_revenue := p_amt_ytd;
         l_organization_rec.tax_reference := p_tax_ref;
         DBMS_OUTPUT.
          put_line (
            'l_party_object_version_number - > '
            || l_party_object_version_number);
         apps.hz_party_v2pub.
          update_organization (
            p_init_msg_list                 => apps.fnd_api.g_true,
            p_organization_rec              => l_organization_rec,
            p_party_object_version_number   => l_party_object_version_number,
            x_profile_id                    => x_profile_id,
            x_return_status                 => x_return_status,
            x_msg_count                     => x_msg_count,
            x_msg_data                      => x_msg_data);
         DBMS_OUTPUT.put_line ('x_profile_id ' || x_profile_id);
         lv_retcode := 0;

         IF x_msg_count > 1
         THEN
            FOR i IN 1 .. x_msg_count
            LOOP
               apps.fnd_msg_pub.get (p_msg_index       => i,
                                     p_encoded         => fnd_api.g_false,
                                     p_data            => x_msg_data,
                                     p_msg_index_out   => l_msg_index_out);

               IF l_error_message IS NULL
               THEN
                  l_error_message := SUBSTR (x_msg_data, 1, 250);
                  lv_retcode := 0;
               ELSE
                  l_error_message :=
                     l_error_message || ' /' || SUBSTR (x_msg_data, 1, 250);
               END IF;
            END LOOP;

            DBMS_OUTPUT.
             put_line ('*****************************************');
            DBMS_OUTPUT.put_line ('API Error : ' || l_error_message);
            DBMS_OUTPUT.
             put_line ('*****************************************');
            ROLLBACK;
         ELSE
            DBMS_OUTPUT.
             put_line ('*****************************************');
            DBMS_OUTPUT.
             put_line (
                  'Attribute4 for Party : '
               || l_party_rec.party_id
               || ' Updated Successfully ');
            DBMS_OUTPUT.
             put_line ('*****************************************');
            COMMIT;
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.
          put_line ('Unexpected Error ' || SUBSTR (SQLERRM, 1, 250));
         lv_retcode := 1;
   END;

   -- *******************************************
   -- Procedure to validate the Suppliers Data.
   -- *******************************************
   PROCEDURE emp_validate_suppliers (lv_retcode OUT NUMBER)
   IS
      -- ******************************
      -- Local variable declarations
      -- ******************************
      lv_dist_supp_rec_stg            NUMBER := 0;
      lv_supplier_rec_stg             NUMBER := 0;
      lv_supp_rec_val                 NUMBER := 0;
      lv_supp_rec_err                 NUMBER := 0;
      lv_supp_error_msg               rgap_supp_emp_stg.rec_message%TYPE;
      --lv_supp_site_error_msg rgap_supp_EMP_stg.site_error_message%type;
      --lv_site_cont_error_msg rgap_supp_EMP_stg.cont_error_message%type;
      lv_rec_status                   rgap_supp_emp_stg.rec_status%TYPE;
      lv_dummy_vendor_type            fnd_lookup_values_vl.lookup_code%TYPE;
      lv_dummy_fob                    fnd_lookup_values_vl.lookup_code%TYPE;
      lv_dummy_ship_via_code          org_freight_tl.freight_code%TYPE;
      lv_terms_id                     ap_terms_tl.term_id%TYPE;
      lv_ship_to_location_code        hr_locations.location_code%TYPE;
      lv_bill_to_location_code        hr_locations.location_code%TYPE;
      lv_pay_group_lookup_code        fnd_lookup_values_vl.lookup_code%TYPE;
      lv_payment_method_lookup_code   fnd_lookup_values_vl.lookup_code%TYPE;
      lv_liability_ccid               gl_code_combinations.code_combination_id%TYPE;
      lv_prepay_ccid                  gl_code_combinations.code_combination_id%TYPE;
      lv_prepay_msg                   VARCHAR2 (2000);
      lv_liability_msg                VARCHAR2 (2000);
      -- lv_supplier_notif rgap_supp_EMP_stg.supplier_notif_method%TYPE;
      lv_country_code                 rgap_supp_emp_stg.country_code%TYPE;
      lv_dist_supp_rec_val            NUMBER := 0;
      lv_dist_supp_rec_err            NUMBER := 0;
      ln_supp_site_comb               NUMBER;
      ln_length_vendor_site           NUMBER;
      l_installed_flag                VARCHAR2 (10);
      l_language                      VARCHAR2 (30);
      valid_language_flag             BOOLEAN := TRUE;
      lv_tax_retcode                  VARCHAR2 (300) := NULL;

      -- *******************************************************************************
      -- Declaring staging table cursor (Selecting the legacy columns from staging table)
      -- ********************************************************************************
      CURSOR cur_supp
      IS
           SELECT *
             FROM rgap_supp_emp_stg
            WHERE rec_status = 'NEW'
         ORDER BY vendor_name;
   BEGIN
      -- ************************************************************
      -- To count the total no of new records in the staging table
      -- ************************************************************
      SELECT COUNT (DISTINCT TRIM (vendor_name))
        INTO lv_dist_supp_rec_stg
        FROM rgap_supp_emp_stg
       WHERE rec_status = 'NEW';

      /*LOG (
      '************************************************************************************');
      LOG (' ');
      LOG (' Summary of Distinct Supplier records ');
      LOG (' ');
      LOG (
      ' Total Distinct supplier Records Count : ' || lv_dist_supp_rec_stg);
      LOG (' ');
      LOG (
      '************************************************************************************');
      */
      -- ****************************************
      -- Open Cursor for Suppliers Validation
      -- ****************************************
      FOR supp_rec IN cur_supp
      LOOP
         LOG (' lOOP ');
         -- *******************************
         -- Start of validation section
         -- *******************************
         lv_supp_error_msg := NULL;
         --lv_supp_site_error_msg := NULL;
         --lv_site_cont_error_msg := NULL;
         lv_rec_status := 'VALIDATED';
         lv_dummy_vendor_type := NULL;
         lv_dummy_fob := NULL;
         lv_dummy_ship_via_code := NULL;
         lv_terms_id := NULL;
         lv_ship_to_location_code := NULL;
         lv_bill_to_location_code := NULL;
         lv_pay_group_lookup_code := NULL;
         lv_payment_method_lookup_code := NULL;
         lv_liability_ccid := NULL;
         lv_liability_msg := NULL;
         lv_prepay_ccid := NULL;
         lv_prepay_msg := NULL;
         -- lv_supplier_notif := NULL;
         lv_country_code := NULL;
         ln_length_vendor_site := NULL;

         -- ************************************
         -- To Check if the vendor type is NULL
         -- ************************************
         --LOG ('supp_rec.vendor_type ' || supp_rec.vendor_type);
         IF supp_rec.vendor_type IS NOT NULL
         THEN
            BEGIN
               SELECT lookup_code
                 INTO lv_dummy_vendor_type
                 FROM fnd_lookup_values_vl
                WHERE UPPER (lookup_type) = UPPER ('VENDOR TYPE')
                      AND UPPER (meaning) =
                             UPPER (
                                NVL (supp_rec.vendor_type,
                                     supp_rec.vendor_type));
            --LOG ('lv_dummy_vendor_type ' || lv_dummy_vendor_type);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.vendor_type
                     || '-Vendor Type lookup code does not exist;';
               WHEN OTHERS
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.vendor_type
                     || '-error while validating Vendor lookup code:';
            /*LOG (
            ' Exception raised in Vendor Type lookup validation code :');
            LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));*/
            END;
         ELSE
            lv_rec_status := 'ERROR';
            lv_supp_error_msg :=
               lv_supp_error_msg || 'Vendor Type can not be NULL' || '-';
         END IF;

         -- LOG ('lv_supp_error_msg 1 ' || lv_supp_error_msg);

         --LOG ('lv_supp_error_msg 2 ' || lv_supp_error_msg);

         -- *********************************
         -- validate Country Code
         -- *********************************
         IF supp_rec.country_name IS NOT NULL
         THEN
            BEGIN
               SELECT territory_code
                 INTO lv_country_code
                 FROM fnd_territories_tl
                WHERE UPPER (territory_short_name) =
                         UPPER (supp_rec.country_name)
                      AND LANGUAGE = USERENV ('LANG');              -----'US';
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.country_name
                     || '- Country name does not exist;';
                  lv_terms_id := NULL;
               WHEN OTHERS
               THEN
                  lv_rec_status := 'ERROR';
                  lv_supp_error_msg :=
                        lv_supp_error_msg
                     || supp_rec.country_name
                     || '-error while validating Country:';
                  lv_terms_id := NULL;
            --LOG (' Exception raised in Country code validation code :');
            --LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
            END;
         ELSE
            lv_country_code := NULL;
         END IF;

         --LOG ('lv_supp_error_msg 3 ' || lv_supp_error_msg);

         -- *******************************************************
         -- Update Site Contacts staging table with derived values
         -- *******************************************************
         BEGIN
            UPDATE rgap_supp_emp_stg
               SET /*receipt_required_flag =
               DECODE (supp_rec.match_approval_level,
               '3-Way', 'Y',
               NULL
               ),*/
                  terms_id = lv_terms_id,
                   vendor_type_code = lv_dummy_vendor_type,
                   country_code = lv_country_code,
                   rec_message = lv_supp_error_msg,
                   rec_status = 'VALIDATED'
             WHERE     TRIM (vendor_name) = TRIM (supp_rec.vendor_name)
                   AND line_num = supp_rec.line_num
                   AND rec_status = 'NEW';

            --LOG (' UPDATE VAL');
            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG (
                  'Error while updating the staging table rgap_supp_EMP_stg:');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         COMMIT;
      END LOOP;                                      -- End Loop For Suppliers
   EXCEPTION
      WHEN OTHERS
      THEN
         ROLLBACK;
         LOG (' Exception raised in Suppliers Validation PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END emp_validate_suppliers;

   -- *******************************************
   -- Procedure to Create The suppliers Data.
   -- *******************************************
   PROCEDURE emp_enable_or_create_suppliers
   IS
      -- Declaration for Suppliers
      v_party_id               hz_parties.party_id%TYPE;
      v_supp_return_status     VARCHAR2 (100);
      v_supp_msg_count         NUMBER;
      v_supp_status            VARCHAR2 (1);
      v_supp_msg_data          VARCHAR2 (2000);
      v_supp_msg_data1         VARCHAR2 (2000);
      v_supp_msg_index_out     NUMBER;
      v_supp_timezone_id       NUMBER;
      v_supplier_rec           ap_vendor_pub_pkg.r_vendor_rec_type;
      v_vendor_id              NUMBER;
      lv_party_id              NUMBER;
      lv_vendor_id             NUMBER;
      lv_crt_vendor_id         NUMBER;
      lv_vend_created_rec      NUMBER;
      lv_vend_party_id         NUMBER;
      lv_supp_name             VARCHAR2 (240);
      l_tax_ret_code           NUMBER;
      lv_p_vendor_id           NUMBER := 0;
      lv_p_vend_party_id       NUMBER := 0;
      l_all_wh_tax             VARCHAR2 (300) := 0;
      l_inv_group_id           NUMBER := 0;
      l_pay_group_id           NUMBER := 0;
      l_party_tax_profile_id   NUMBER := 0;
      v_process_flag           VARCHAR2 (300) := NULL;
      l_type_1099              VARCHAR2 (300) := NULL;
      l_vhd_paymethod          VARCHAR2 (300) := NULL;
      l_vhd_gp_database        VARCHAR2 (300) := NULL;
      l_vhd_legacy_vend_id     VARCHAR2 (300) := NULL;
      l_vhd_bank_name          VARCHAR2 (300) := NULL;
      l_vhd_paymethod_code     VARCHAR2 (300) := NULL;
      l_person_id              NUMBER := NULL;
      l_pay_group              VARCHAR2 (300) := NULL;
      l_verify_flag            VARCHAR2 (300) := NULL;
      l_error_message          VARCHAR2 (300) := NULL;
      lv_rec_status            VARCHAR2 (300) := NULL;
      lv_supp_error_msg        VARCHAR2 (300) := NULL;
      lv_rec_status            VARCHAR2 (300) := NULL;
      lv_terms_id              NUMBER := NULL;
      l_set_of_books_id        NUMBER := NULL;

      CURSOR cur_supp_create
      IS
           SELECT *
             FROM rgap_supp_emp_stg supp
            WHERE supp.rec_status = 'VALIDATED' AND vendor_rec_status IS NULL
         ORDER BY supp.line_num, supp.vendor_name;

      l_resp_id                NUMBER;
      l_resp_appl_id           NUMBER;
      l_user_id                NUMBER;
      l_emp_name               VARCHAR2 (300) := NULL;
   BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE UPPER (user_name) = UPPER ('SYSADMIN');

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_resp_appl_id
        FROM fnd_responsibility_vl
       WHERE application_id = 200
             AND responsibility_name = 'Payables Manager';

      mo_global.init ('SQLAP');
      fnd_global.
       apps_initialize (user_id        => l_user_id,
                        resp_id        => l_resp_id,
                        resp_appl_id   => l_resp_appl_id);
      DBMS_OUTPUT.put_line ('CREATE SUPP API');

      FOR supp_rec_create IN cur_supp_create
      LOOP
         fnd_msg_pub.initialize;
         lv_party_id := NULL;
         lv_supp_name := NULL;
         lv_vendor_id := NULL;
         lv_crt_vendor_id := NULL;
         lv_vend_party_id := NULL;
         l_all_wh_tax := NULL;
         l_party_tax_profile_id := NULL;
         lv_supp_name := TRIM (supp_rec_create.vendor_name);

         BEGIN
            SELECT supp.vendor_id, supp.party_id
              INTO lv_vendor_id, lv_vend_party_id
              FROM ap_suppliers supp
             WHERE TRIM (vendor_name) = TRIM (supp_rec_create.vendor_name);
         ----upper(supp_rec_create.vendor_name);
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               lv_vendor_id := NULL;
            WHEN OTHERS
            THEN
               lv_vendor_id := NULL;
         /*LOG (
         ' Exception raised in while deriving the vendor id : '
         || supp_rec_create.vendor_name);
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));*/
         END;

         l_all_wh_tax := 'Y';

         BEGIN
            IF lv_vendor_id IS NULL
            THEN
               BEGIN
                  SELECT set_of_books_id
                    INTO l_set_of_books_id
                    FROM hr_operating_units
                   WHERE NAME = supp_rec_create.operating_unit;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_set_of_books_id := NULL;
               END;

               v_supp_msg_count := 0;                --for error message count
               --v_supplier_rec.party_id := lv_party_id;
               v_supplier_rec.vendor_name :=
                  TRIM (supp_rec_create.vendor_name);
               --v_supplier_rec.vendor_name := (lv_supp_name);
               v_supplier_rec.vendor_type_lookup_code :=
                  supp_rec_create.vendor_type_code;
               v_supplier_rec.set_of_books_id := l_set_of_books_id;

               --fnd_profile.VALUE ('GL_SET_OF_BKS_ID');

               -- v_supplier_rec.inspection_required_flag := 'N';
               -- v_supplier_rec.OFFSET_TAX_FLAG := 'Y';

               -- TERMS

               -- v_supplier_rec.PAY_GROUP := l_pay_group;

               IF supp_rec_create.employee_number IS NOT NULL
               THEN
                  BEGIN
                     SELECT MAX (person_id), MAX (FULL_NAME)
                       INTO l_person_id, l_emp_name
                       FROM per_all_people_f
                      WHERE employee_number = supp_rec_create.employee_number --AND SYSDATE BETWEEN EFFECTIVE_START_DATE AND EFFECTIVE_END_DATE
                                                                             ;
                  EXCEPTION
                     WHEN OTHERS
                     THEN
                        l_person_id := NULL;
                  END;

                  v_supplier_rec.employee_id := l_person_id;
               ELSE
                  v_supplier_rec.employee_id := l_person_id;
                  l_person_id := NULL;
                  l_emp_name := NULL;
               END IF;

               DBMS_OUTPUT.put_line ('L_PERSON_ID ' || l_person_id);
            --v_supplier_rec.allow_awt_flag := null;
            END IF;

            DBMS_OUTPUT.
             put_line ('upper(LV_SUPP_NAME) ' || UPPER (lv_supp_name));

            IF supp_rec_create.entity = 'Korea'
            THEN
               v_supplier_rec.terms_date_basis := 'Invoice';
               v_supplier_rec.vendor_name := l_emp_name;
               v_supplier_rec.allow_awt_flag := 'N';

               BEGIN
                  UPDATE rgap_supp_emp_stg
                     SET vendor_name = l_emp_name
                   WHERE line_num = supp_rec_create.line_num;

                  COMMIT;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     ROLLBACK;
               END;
            ELSE
               v_supplier_rec.terms_date_basis := NULL;
               v_supplier_rec.vendor_name :=
                  TRIM (supp_rec_create.vendor_name);
               l_emp_name := NULL;
            END IF;

            BEGIN
               ap_vendor_pub_pkg.
                create_vendor (
                  p_api_version        => 1.0,
                  p_init_msg_list      => fnd_api.g_false,
                  p_commit             => fnd_api.g_false,
                  p_validation_level   => fnd_api.g_valid_level_full,
                  x_return_status      => v_supp_return_status,
                  x_msg_count          => v_supp_msg_count,
                  -- error message count
                  x_msg_data           => v_supp_msg_data,
                  -- error message
                  p_vendor_rec         => v_supplier_rec,
                  x_vendor_id          => v_vendor_id,
                  -- vendor id created
                  x_party_id           => v_party_id       -- party id created
                                                    );
            EXCEPTION
               WHEN OTHERS
               THEN
                  DBMS_OUTPUT.put_line (v_supp_msg_data);
            END;

            COMMIT;

            --LOG ('v_process_flag ' || v_process_flag);

            --END IF;

            --------------------
            IF v_supp_return_status != apps.fnd_api.g_ret_sts_success
            THEN
               IF (fnd_msg_pub.count_msg > 0)
               THEN
                  FOR i IN 1 .. fnd_msg_pub.count_msg
                  LOOP
                     fnd_msg_pub.
                      get (p_msg_index       => i,
                           p_data            => v_supp_msg_data,
                           p_encoded         => 'F',
                           p_msg_index_out   => v_supp_msg_index_out);
                     v_supp_msg_data1 :=
                        v_supp_msg_data1 || ' ' || v_supp_msg_data;
                     DBMS_OUTPUT.put_line ('ERROR -- > ' || v_supp_msg_data1);
                  END LOOP;
               END IF;
            END IF;

            IF v_supp_return_status = 'S'
            THEN
               lv_crt_vendor_id := v_vendor_id;

               UPDATE rgap_supp_emp_stg
                  SET vendor_rec_status = 'CREATED',
                      ora_vendor_id = lv_crt_vendor_id,
                      ora_party_id = v_party_id
                WHERE TRIM (vendor_name) = TRIM (supp_rec_create.vendor_name)
                      AND rec_status = 'VALIDATED'
                      AND line_num = supp_rec_create.line_num;
            ELSE
               UPDATE rgap_supp_emp_stg
                  SET vendor_rec_status = 'ERROR',
                      rec_message = v_supp_msg_data
                WHERE TRIM (vendor_name) = TRIM (supp_rec_create.vendor_name)
                      AND rec_status = 'VALIDATED'
                      AND line_num = supp_rec_create.line_num;
            /*
            LOG (
            ' Supplier Which got error:'
            || supp_rec_create.VENDOR_NAME);
            LOG (' Error Message :' || v_supp_msg_data);*/
            END IF;

            COMMIT;
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG (
                  ' Exception raised in Enabling or creating Supplier PRC :');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;
      END LOOP;

      SELECT COUNT (1)
        INTO lv_vend_created_rec
        FROM rgap_supp_emp_stg
       WHERE vendor_rec_status = 'CREATED';
   /*LOG (
   '***************************************************************************************');
   LOG (' ');
   LOG (' Total Supplier Records Created ');
   LOG (' ');
   LOG ('Total Supplier Records Created Count : ' || lv_vend_created_rec);
   LOG (' ');
   LOG (
   '***************************************************************************************');*/
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in Enabling or creating Supplier PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END emp_enable_or_create_suppliers;

   PROCEDURE emp_validate_site_data (p_errcode   OUT NUMBER,
                                     p_errmess   OUT VARCHAR2)
   IS
      -- Declaration for supplier sites
      l_site_return_status    VARCHAR2 (1000);
      l_site_msg_count        NUMBER;
      l_site_status           VARCHAR2 (1);
      l_site_msg_data         VARCHAR2 (2000);
      l_site_msg_data1        VARCHAR2 (5000);
      l_site_msg_index_out    NUMBER;
      l_site_timezone_id      NUMBER;
      v_supplier_site_rec     ap_vendor_pub_pkg.r_vendor_site_rec_type;
      l_party_site_id         NUMBER;
      l_vendor_site_id        NUMBER;
      l_location_id           NUMBER;
      l_vend_sit_crt_rec      NUMBER;
      l_status                VARCHAR2 (1000);
      l_errcode               VARCHAR2 (300) := NULL;
      l_errmess               VARCHAR2 (300) := NULL;
      l_verify_flag           VARCHAR2 (10);
      l_error_message         VARCHAR2 (1000);
      l_vendor_site_code      VARCHAR2 (35);
      l_vendor_interface_id   NUMBER;
      l_vendor_id             NUMBER;
      l_territory_code        VARCHAR2 (10);
      l_pay_group             VARCHAR2 (100);
      l_invoice_currency      VARCHAR2 (10);
      l_payment_currency      VARCHAR2 (10);
      l_pay_methods           VARCHAR2 (100);
      l_org_id                NUMBER (10);
      l_term_id               NUMBER (10);
      l_auto_tax_calc_flag    VARCHAR2 (10);

      CURSOR c_supp_date
      IS
         SELECT rgss.ROWID, rgss.*
           FROM rgap_supp_emp_stg rgss
          WHERE status_code = 'NEW';
   BEGIN
      FOR s1 IN c_supp_date
      LOOP
         l_verify_flag := NULL;
         l_error_message := NULL;
         l_vendor_id := NULL;

         ----Retriving the vendor id for the vendor -------------------------
         BEGIN
            SELECT vendor_id
              INTO l_vendor_id
              FROM ap_suppliers
             WHERE LTRIM (RTRIM ( (vendor_name))) =
                      LTRIM (RTRIM ( (s1.vendor_name)));

            IF l_vendor_id IS NULL
            THEN
               l_verify_flag := 'E';
               l_error_message := 'Vendor is not exist in system';
            END IF;
         EXCEPTION
            WHEN NO_DATA_FOUND
            THEN
               l_verify_flag := 'E';
               l_error_message := 'Vendor is not exist in system';
            WHEN OTHERS
            THEN
               fnd_file.
                put_line (fnd_file.LOG,
                          'Vendor validation Issue ' || SQLERRM);
         END;

         fnd_file.put_line (fnd_file.LOG, 'Vendor_name ' || s1.vendor_name);

         --------------------Validating territory code--------------------------------
         IF s1.country_name IS NOT NULL
         THEN
            BEGIN
               SELECT territory_code
                 INTO l_territory_code
                 FROM fnd_territories_tl
                WHERE LTRIM (RTRIM (UPPER (territory_short_name))) =
                         LTRIM (RTRIM (UPPER (s1.country_name)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message := l_error_message || ' Invalid Country';
                  NULL;
            END;
         ELSE
            l_territory_code := NULL;
         END IF;

         fnd_file.
          put_line (fnd_file.LOG, 'l_territory_code ' || l_territory_code);

         IF s1.pay_group IS NOT NULL
         THEN
            --------------------------- validating Pay group------------------------------------------------------------
            BEGIN
               SELECT lookup_code
                 INTO l_pay_group
                 FROM po_lookup_codes
                WHERE lookup_type = 'PAY GROUP'
                      AND LTRIM (RTRIM (UPPER (lookup_code))) =
                             LTRIM (RTRIM (UPPER (s1.pay_group)));
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  fnd_file.put_line (fnd_file.LOG, 'Pay Group not exists ');
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Pay group is invalid ';
            END;
         ELSE
            l_pay_group := NULL;
         END IF;

         ------------------------------ validating payment method----------------------------------------------------------------
         IF s1.payment_method IS NOT NULL
         THEN
            BEGIN
               SELECT payment_method_code
                 INTO l_pay_methods
                 FROM iby_payment_methods_vl iby
                WHERE iby.inactive_date IS NULL
                      AND LTRIM (RTRIM (UPPER (payment_method_name))) =
                             LTRIM (RTRIM (UPPER (s1.payment_method)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Payment method is not valid';
            END;
         ELSE
            l_pay_methods := 'CHECK';
         END IF;

         -------------------------------Validating Payment terms-------------------------------------------------------------------
         IF s1.terms_code IS NOT NULL
         THEN
            BEGIN
               SELECT term_id
                 INTO l_term_id
                 FROM ap_terms
                WHERE LTRIM (RTRIM (UPPER (NAME))) =
                         LTRIM (RTRIM (UPPER (s1.terms_code)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || 'Payment Term is not valid';
            END;
         ELSE
            l_term_id := NULL;
         END IF;

         ---------------------------------Retriving org_id for the operating unit-------------------------------------------------------------------
         IF s1.operating_unit IS NOT NULL
         THEN
            BEGIN
               SELECT organization_id
                 INTO l_org_id
                 FROM hr_operating_units
                WHERE LTRIM (RTRIM (UPPER (NAME))) =
                         LTRIM (RTRIM (UPPER (s1.operating_unit)));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_verify_flag := 'E';
                  l_error_message :=
                     l_error_message || ' Operating Unit is Invalid';
            END;
         ELSE
            l_org_id := NULL;
         END IF;

         fnd_file.put_line (fnd_file.LOG, 'l_org_id ' || l_org_id);

         ----------------------------Validating vendor site code-------------------------------------------------------------------------------------
         IF s1.vendor_site_code IS NOT NULL
         THEN
            BEGIN
               SELECT vendor_site_code
                 INTO l_vendor_site_code
                 FROM po_vendor_sites_all a, po_vendors b
                WHERE org_id = l_org_id
                      AND TRIM (vendor_site_code) =
                             TRIM (s1.vendor_site_code)
                      AND a.vendor_id = b.vendor_id
                      AND TRIM (UPPER (b.vendor_name)) =
                             TRIM (UPPER (s1.vendor_name));
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_vendor_site_code := NULL;
            END;

            IF l_vendor_site_code IS NOT NULL
            THEN
               l_verify_flag := 'E';
               l_error_message :=
                  l_error_message || ' Vendor Site is already existing';
            END IF;
         END IF;

         IF l_error_message IS NOT NULL
         THEN
            UPDATE rgap_supp_emp_stg
               SET error_message = l_error_message, status_code = 'ERROR'
             WHERE ROWID = s1.ROWID;

            p_errcode := 1;
            p_errmess := l_error_message;
         ELSE
            UPDATE rgap_supp_emp_stg
               SET ora_vendor_id = l_vendor_id,
                   country_code = l_territory_code,
                   terms_id = l_term_id,
                   operating_unit_id = l_org_id,
                   status_code = 'VALIDATED'
             --,vendor_site_code = l_vendor_site_code
             WHERE ROWID = s1.ROWID;

            p_errcode := 0;
         END IF;
      END LOOP;
   EXCEPTION
      WHEN OTHERS
      THEN
         p_errcode := 1;
         p_errmess := SQLERRM;
   END;

   -- Procedure to create new supplier site
   -- Procedure to create new supplier site
   PROCEDURE emp_create_vendor_site
   IS
      -- Declaration for supplier sites
      v_site_return_status    VARCHAR2 (1000);
      v_site_msg_count        NUMBER;
      v_site_status           VARCHAR2 (1);
      v_site_msg_data         VARCHAR2 (2000);
      v_site_msg_data1        VARCHAR2 (5000);
      v_site_msg_index_out    NUMBER;
      v_site_timezone_id      NUMBER;
      v_supplier_site_rec     ap_vendor_pub_pkg.r_vendor_site_rec_type;
      v_party_site_id         NUMBER;
      v_vendor_site_id        NUMBER;
      v_location_id           NUMBER;
      lv_vend_sit_crt_rec     NUMBER;
      lv_status               VARCHAR2 (1000);
      lv_err_buff             VARCHAR2 (300) := NULL;
      l_vendor_site_code      VARCHAR2 (300) := NULL;
      l_tax_class             VARCHAR2 (300) := NULL;
      l_payment_lookup_code   VARCHAR2 (300) := NULL;

      CURSOR cur_site_cont
      IS
           SELECT supp.ROWID, supp.*
             FROM rgap_supp_emp_stg supp
            WHERE supp.status_code = 'VALIDATED'
                  AND vendor_site_status_code IS NULL
         ORDER BY supp.line_num, supp.vendor_name;

      l_group_id              VARCHAR2 (300) := NULL;
      l_resp_id               NUMBER;
      l_resp_appl_id          NUMBER;
      l_user_id               NUMBER;
   BEGIN
      SELECT user_id
        INTO l_user_id
        FROM fnd_user
       WHERE UPPER (user_name) = UPPER ('SYSADMIN');

      SELECT responsibility_id, application_id
        INTO l_resp_id, l_resp_appl_id
        FROM fnd_responsibility_vl
       WHERE application_id = 200
             AND responsibility_name = 'Payables Manager';

      mo_global.init ('SQLAP');
      fnd_global.
       apps_initialize (user_id        => l_user_id,
                        resp_id        => l_resp_id,
                        resp_appl_id   => l_resp_appl_id);

      FOR cur_site_cont_rec IN cur_site_cont
      LOOP
         fnd_msg_pub.initialize;

         BEGIN
            v_site_msg_count := 0;
            v_site_return_status := NULL;
            v_site_msg_data := NULL;
            l_vendor_site_code := NULL;
            v_site_return_status := NULL;
            l_payment_lookup_code := NULL;
            l_vendor_site_code := 'OFFICE';
            DBMS_OUTPUT.put_line ('create_vendor_site');
            v_supplier_site_rec.vendor_site_code :=
               SUBSTR (l_vendor_site_code, 1, 15);

            BEGIN
               SELECT payment_method_code
                 INTO l_payment_lookup_code
                 --v_supplier_site_rec. .default_pmt_method
                 FROM iby_payment_methods_vl
                WHERE UPPER (payment_method_name) =
                         UPPER (cur_site_cont_rec.payment_method);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  l_payment_lookup_code := NULL;
            -- v_supplier_site_rec.ext_payee_rec.default_pmt_method := NULL;
            END;

            IF l_payment_lookup_code IS NULL
            THEN
               IF cur_site_cont_rec.entity = ('United States')
               THEN
                  v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                     'CHECK';
               ELSE
                  IF cur_site_cont_rec.bank_name IS NOT NULL
                  THEN
                     v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                        'EFT';
                  ELSE
                     v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                        'WIRE';
                  END IF;
               END IF;
            ELSE
               v_supplier_site_rec.ext_payee_rec.default_pmt_method :=
                  l_payment_lookup_code;
            END IF;

            v_supplier_site_rec.purchasing_site_flag := NULL;
            v_supplier_site_rec.pay_site_flag := 'Y';
            --v_supplier_site_rec.terms_id := cur_site_cont_rec.terms_id;

            /*IF cur_site_cont_rec.terms_id IS NOT NULL
            THEN

            IF cur_site_cont_rec.ENTITY != ('United States')
            THEN
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice';
            ELSE
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice Received';
            END IF;

            ELSE
            --v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice';
            IF cur_site_cont_rec.ENTITY != ('United States')
            THEN
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice';
            ELSE
            v_supplier_site_rec.TERMS_DATE_BASIS := 'Invoice Received';
            END IF;
            END IF;
            */
            v_supplier_site_rec.vendor_id := cur_site_cont_rec.ora_vendor_id;
            v_supplier_site_rec.org_id := cur_site_cont_rec.operating_unit_id;
            DBMS_OUTPUT.put_line ('create_vendor_site');



            IF cur_site_cont_rec.ENTITY != ('Korea')
            THEN
               v_supplier_site_rec.allow_awt_flag := 'N';
            ELSE
               v_supplier_site_rec.allow_awt_flag := NULL;
            END IF;


            BEGIN
               ap_vendor_pub_pkg.
                create_vendor_site (
                  p_api_version        => 1.0,
                  p_init_msg_list      => fnd_api.g_false,
                  p_commit             => fnd_api.g_false,
                  p_validation_level   => fnd_api.g_valid_level_full,
                  x_return_status      => v_site_return_status,
                  x_msg_count          => v_site_msg_count,
                  x_msg_data           => v_site_msg_data,
                  p_vendor_site_rec    => v_supplier_site_rec,
                  x_vendor_site_id     => v_vendor_site_id,
                  x_party_site_id      => v_party_site_id,
                  x_location_id        => v_location_id);
            EXCEPTION
               WHEN OTHERS
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET vendor_site_status_code = 'APIERROR',
                         error_message =
                            v_site_msg_data || ' ' || v_site_return_status
                   WHERE UPPER (vendor_name) =
                            UPPER (cur_site_cont_rec.vendor_name)
                         AND status_code = 'VALIDATED'
                         AND line_num = cur_site_cont_rec.line_num;
            END;

            COMMIT;
            LOG ('v_vendor_site_id ' || v_vendor_site_id);

            IF v_site_return_status != apps.fnd_api.g_ret_sts_success
            THEN
               IF (fnd_msg_pub.count_msg > 0)
               THEN
                  FOR i IN 1 .. fnd_msg_pub.count_msg
                  LOOP
                     fnd_msg_pub.
                      get (p_msg_index       => i,
                           p_data            => v_site_msg_data,
                           p_encoded         => 'F',
                           p_msg_index_out   => v_site_msg_index_out);
                     v_site_msg_data1 :=
                        v_site_msg_data1 || ' ' || v_site_msg_data;
                     DBMS_OUTPUT.put_line (v_site_msg_data1);
                  END LOOP;
               END IF;
            END IF;
         EXCEPTION
            WHEN OTHERS
            THEN
               LOG (' Exception raised in loop :');
               LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
         END;

         IF v_site_return_status = 'S'
         THEN
            UPDATE rgap_supp_emp_stg
               SET vendor_site_status_code = 'CREATED',
                   vendor_site_id = v_vendor_site_id,
                   party_site_id = v_party_site_id,
                   --location_id = v_location_id,
                   vendor_site_code = l_vendor_site_code
             WHERE UPPER (vendor_name) =
                      UPPER (cur_site_cont_rec.vendor_name)
                   AND status_code = 'VALIDATED'
                   AND line_num = cur_site_cont_rec.line_num;

            COMMIT;
         /* IF v_party_site_id IS NOT NULL THEN
         if cur_site_cont_rec.v_cnt_phone_no1 is not null then

         supp_site_cont_details (v_party_site_id,null,'2314324');
         commit;
         end if;
         if cur_site_cont_rec.EMAIL_ADDRESS is not null then

         supp_site_cont_details (v_party_site_id,'SELECT@TEST.COM',null);
         commit;
         end if;
         END IF;*/
         ELSE
            UPDATE rgap_supp_emp_stg
               SET vendor_site_status_code = 'ERROR',
                   error_message =
                      v_site_msg_data || ' ' || v_site_return_status
             WHERE UPPER (vendor_name) =
                      UPPER (cur_site_cont_rec.vendor_name)
                   AND status_code = 'VALIDATED'
                   AND line_num = cur_site_cont_rec.line_num;

            LOG (' supplier : API ' || cur_site_cont_rec.vendor_name);
            LOG (
               ' Site which got errored out for supplier :'
               || cur_site_cont_rec.vendor_site_code);
            LOG (' Error Message :' || v_site_msg_data);
         END IF;

         COMMIT;
      END LOOP;

      SELECT COUNT (1)
        INTO lv_vend_sit_crt_rec
        FROM rgap_supp_emp_stg
       WHERE vendor_site_status_code = 'CREATED';

      LOG (
         '***************************************************************************************');
      LOG (' ');
      LOG (' Total Supplier Site Records Created ');
      LOG (' ');
      LOG (
         'Total Supplier Site Records Created Count : '
         || lv_vend_sit_crt_rec);
      LOG (' ');
      LOG (
         '***************************************************************************************');
   EXCEPTION
      WHEN OTHERS
      THEN
         LOG (' Exception raised in creating site Cont PRC :');
         LOG (' SQLERRM: ' || SUBSTR (SQLERRM, 1, 250));
   END emp_create_vendor_site;

   PROCEDURE emp_assign_bank_accnts
   IS
      v_error_reason             VARCHAR2 (2000) := NULL;
      v_return_status            VARCHAR2 (2000) := NULL;
      v_msg_count                NUMBER := NULL;
      v_msg_data                 VARCHAR2 (3000) := NULL;
      v_out_mesg                 apps.iby_fndcpt_common_pub.result_rec_type;
      v_response                 iby_fndcpt_common_pub.result_rec_type;
      v_result_rec_type          iby_fndcpt_common_pub.result_rec_type;
      v_extbank_rec_type         iby_ext_bankacct_pub.extbank_rec_type;
      v_extbankbranch_rec_type   iby_ext_bankacct_pub.extbankbranch_rec_type;
      v_bank_acct_rec            apps.iby_ext_bankacct_pub.extbankacct_rec_type;
      v_assign                   apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
      v_payee_rec                apps.iby_disbursement_setup_pub.payeecontext_rec_type;
      v_branch_type              VARCHAR2 (2000);
      v_bank_id                  NUMBER;
      v_branch_id                NUMBER;
      exec_bank_acct             EXCEPTION;
      v_supplier_site_id         NUMBER;
      v_party_site_id            NUMBER;
      v_currency_code            VARCHAR2 (5);
      v_bank_ctry                VARCHAR2 (5);
      v_bank_name                VARCHAR2 (50);
      v_bank_branch              VARCHAR2 (50);
      v_msg_dummy                VARCHAR2 (3000);
      p_location_rec             hz_location_v2pub.location_rec_type;
      v_location_id              NUMBER;
      p_party_site_rec           hz_party_site_v2pub.party_site_rec_type;
      v_party_site_number        NUMBER;
      l_bank_party_id            NUMBER;
      l_branch_party_id          NUMBER;
      l_vendor_id                NUMBER;
      l_vendor_site_id           NUMBER;
      l_acct                     NUMBER;
      lc_output                  VARCHAR2 (3000);
      lc_msg_dummy               VARCHAR2 (3000);
      lc_return_status           VARCHAR2 (3000);
      v_out_mesg                 apps.iby_fndcpt_common_pub.result_rec_type;
      v_bank_acct_rec            apps.iby_ext_bankacct_pub.extbankacct_rec_type;
      v_assign                   apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
      v_payee_rec                apps.iby_disbursement_setup_pub.payeecontext_rec_type;
      exec_bank_acct             EXCEPTION;
      v_supplier_site_id         NUMBER;
      v_party_site_id            NUMBER;
      v_currency_code            VARCHAR2 (5);
      v_msg_dummy                VARCHAR2 (3000);
      p_location_rec             hz_location_v2pub.location_rec_type;
      v_location_id              NUMBER;
      p_party_site_rec           hz_party_site_v2pub.party_site_rec_type;
      v_party_site_number        NUMBER;
      l_bank_party_id            NUMBER;
      l_branch_party_id          NUMBER;
      l_vendor_id                NUMBER;
      l_vendor_site_id           NUMBER;
      l_acct                     NUMBER;
      --lc_output VARCHAR2 (3000);
      --lc_msg_dummy VARCHAR2 (3000);
      lc_return_status           VARCHAR2 (3000);
      lc_msg_data                VARCHAR2 (3000);
      papiversion                NUMBER := 1.0;
      pinitmsglist               VARCHAR2 (1) := 'F';
      xreturnstatus              VARCHAR2 (2000);
      xmsgcount                  NUMBER (5);
      xmsgdata                   VARCHAR2 (2000);
      vbankname                  VARCHAR2 (100);
      vvendorname                VARCHAR2 (100);
      xresponse                  iby_fndcpt_common_pub.result_rec_type;
      pextbankacctrec            iby_ext_bankacct_pub.extbankacct_rec_type;
      vsupplierpartyid           NUMBER;
      -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid                    NUMBER;             -- EXISTING BANK PARTY ID
      vbankbranchid              NUMBER;           -- EXISTING BRANCH PARTY ID
      xacctid                    NUMBER;
      pcount                     NUMBER;
      --vinstrumentid number;
      --pInstrument IBY_FNDCPT_SETUP_PUB.PMTINSTRUMENT_REC_TYPE;
      xassignid                  NUMBER;
      ppayee                     iby_disbursement_setup_pub.payeecontext_rec_type;
      passignmentattribs         iby_fndcpt_setup_pub.pmtinstrassignment_rec_type;
      pinstrument                iby_fndcpt_setup_pub.pmtinstrument_rec_type;
      vinstrumentid              NUMBER;

      CURSOR c_bank
      IS
         SELECT DISTINCT TRIM ( (mas.vendor_name)) vendor_name,
                         TRIM ( (vendor_site_code)) vendor_site_code,
                         TRIM ( (country_code)) country,
                         TRIM ( (bank_name)) bank_name,
                         TRIM ( (country_name)) bank_country,
                         TRIM (mas.bank_code) bank_code,
                         vendor_site_id,
                         mas.ROWID b_row_id
           FROM rgap_supp_emp_stg mas
          WHERE bank_status = 'NEW' AND vendor_rec_status = 'CREATED';

      CURSOR c_branch
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.country_name))
                   branch_bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                TRIM (rss.account_name) account_name,
                rss.account_number operating_unit_id,
                ora_vendor_id,
                entity,
                iban,
                currency,
                vendor_site_id,
                party_site_id,
                TRIM (rss.bank_code) bank_code,
                Branch_Number
           FROM rgap_supp_emp_stg rss
          WHERE bank_branch_status = 'NEW' AND vendor_rec_status = 'CREATED';

      CURSOR c_bank_bra_ass
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.country_name))
                   bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                TRIM (rss.account_name) account_name,
                rss.account_number,
                -- OPERATING_UNIT_ID,
                ora_vendor_id,
                --VENDOR_SITE_ID,
                entity,
                iban,
                currency,
                vendor_site_id,
                party_site_id,
                operating_unit_id,
                TRIM (rss.bank_code) bank_code,
                BRANCH_NUMBER
           FROM rgap_supp_emp_stg rss
          WHERE bank_branch_ass_status = 'NEW'
                AND vendor_rec_status = 'CREATED';

      CURSOR c_bank_ven_bra_ass
      IS
         SELECT DISTINCT
                TRIM ( (rss.vendor_name)) vendor_name,
                --TRIM ( (rss.vendor_site_code)) vendor_site_code,
                TRIM ( (rss.country_code)) country,
                TRIM ( (rss.bank_name)) bank_name,
                rss.branch_name bank_branch,
                (SELECT territory_code
                   FROM fnd_territories_vl
                  WHERE territory_short_name = TRIM (rss.country_name))
                   bank_country,
                ROWID br_row_id,
                NULL intermediary_bank,
                NULL sort_code,
                swift bank_swiftcode,
                NULL abi,
                NULL cab,
                NULL bank_routing_num,
                rss.branch_name branch_name,
                NULL bank_countrol_key,
                NULL bsb,
                TRIM (rss.account_name) account_name,
                rss.account_number,
                rss.party_site_id,
                rss.vendor_site_id,
                rss.operating_unit_id,
                TRIM (rss.bank_code) bank_code,
                BRANCH_NUMBER
           FROM rgap_supp_emp_stg rss
          WHERE bank_vend_ass_status = 'NEW'
                AND vendor_rec_status = 'CREATED';

      l_bank_cnt_code            VARCHAR2 (300) := NULL;
      l_party_site_id            NUMBER := NULL;
   BEGIN
      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Bank Creation code*/
         BEGIN
            FOR rec_c_bank IN c_bank
            LOOP
               DBMS_OUTPUT.put_line ('HI');
               v_bank_id := NULL;
               v_bank_ctry := NULL;
               v_bank_name := NULL;
               v_bank_branch := NULL;
               DBMS_OUTPUT.
                put_line ('Bank Loop Started for ' || rec_c_bank.bank_name);
               DBMS_OUTPUT.put_line ('Start Of CRAETE BANK');
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;
               v_extbank_rec_type.object_version_number := 1.0;
               v_extbank_rec_type.bank_name := rec_c_bank.bank_name;
               -- v_extbank_rec_type.bank_number := rec_c_bank.bank_routing_num;
               v_extbank_rec_type.institution_type := 'BANK';

               IF rec_c_bank.bank_country IS NOT NULL
               THEN
                  BEGIN
                     SELECT territory_code
                       INTO l_bank_cnt_code
                       FROM fnd_territories_tl
                      WHERE UPPER (territory_short_name) =
                               UPPER (TRIM (rec_c_bank.bank_country))
                            AND LANGUAGE = USERENV ('LANG');        -----'US';
                  EXCEPTION
                     WHEN NO_DATA_FOUND
                     THEN
                        lc_msg_data :=
                              lc_msg_data
                           || rec_c_bank.bank_country
                           || '- Country name does not exist;';
                     WHEN OTHERS
                     THEN
                        lc_msg_data :=
                              lc_msg_data
                           || rec_c_bank.bank_country
                           || '-error while validating Country:';
                  END;
               ELSE
                  l_bank_cnt_code := NULL;
               END IF;

               v_extbank_rec_type.country_code := /*rec_c_bank.bank_country;*/
                                                 l_bank_cnt_code;
               v_extbank_rec_type.description := NULL;
               v_extbank_rec_type.bank_number := rec_c_bank.bank_code;

               --v_extbank_rec_type.bank_name :=rec_c_bank.bsb;
               BEGIN
                  iby_ext_bankacct_pub.
                   create_ext_bank (p_api_version     => 1.0,
                                    p_init_msg_list   => fnd_api.g_true,
                                    p_ext_bank_rec    => v_extbank_rec_type,
                                    x_bank_id         => v_bank_id,
                                    x_return_status   => v_return_status,
                                    x_msg_count       => v_msg_count,
                                    x_msg_data        => v_msg_data,
                                    x_response        => v_response);

                  UPDATE rgap_supp_emp_stg
                     SET bank_id = v_bank_id,
                         bank_status = 'CREATED',
                         bank_error = v_msg_data || '-' || v_return_status
                   WHERE ROWID = rec_c_bank.b_row_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     UPDATE rgap_supp_emp_stg
                        SET bank_id = v_bank_id, bank_status = 'APIERROR'
                      WHERE ROWID = rec_c_bank.b_row_id;
               END;

               DBMS_OUTPUT.
                put_line ('Bank return status ' || v_return_status);
               DBMS_OUTPUT.put_line ('Bank id ' || v_bank_id);

               IF (v_return_status <> 'S')
               THEN
                  FOR i IN 1 .. v_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           apps.fnd_api.g_false,
                                           lc_msg_data,
                                           lc_msg_dummy);
                     lc_output :=
                        lc_output
                        || (   TO_CHAR (i)
                            || ': '
                            || SUBSTR (lc_msg_data, 1, 250));
                  END LOOP;

                  fnd_file.
                   put_line (
                     fnd_file.LOG,
                     'Error Occured while Creating Bank: ' || lc_msg_data);
                  -- DBMs_OUTPUT.PUT_LINE( v_return_status );
                  --DBMs_OUTPUT.PUT_LINE( lc_msg_data);
                  DBMS_OUTPUT.put_line (' test s' || v_bank_id);

                  UPDATE rgap_supp_emp_stg
                     SET bank_id = v_bank_id,
                         bank_status = 'CREATED',
                         bank_error = v_msg_data
                   WHERE ROWID = rec_c_bank.b_row_id;

                  COMMIT;
               ELSE
                  DBMS_OUTPUT.put_line (' test f ' || v_bank_id);

                  UPDATE rgap_supp_emp_stg
                     SET bank_id = v_bank_id,
                         bank_error = lc_msg_data,
                         bank_status = 'ERROR'
                   WHERE ROWID = rec_c_bank.b_row_id;

                  COMMIT;
               END IF;

               COMMIT;
               fnd_file.
                put_line (fnd_file.LOG, 'BANK API ERROR-' || v_error_reason);
               fnd_file.put_line (fnd_file.LOG, 'End of CRAETE BANK');
               fnd_file.
                put_line (fnd_file.LOG,
                          'Bank Loop Ended for ' || rec_c_bank.bank_name);
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Bank Branch Creation code*/
         BEGIN
            FOR rec_c_branch IN c_branch
            LOOP
               v_branch_id := NULL;
               v_bank_id := NULL;
               v_bank_ctry := NULL;
               v_bank_name := NULL;
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;

               BEGIN
                  DBMS_OUTPUT.
                   put_line (
                     'Retriving the bank_id:' || rec_c_branch.bank_name);
                  DBMS_OUTPUT.
                   put_line (
                     'rec_c_branch.BRANCH_bank_country:'
                     || rec_c_branch.country);

                  SELECT b.bank_party_id, b.home_country, b.bank_name
                    INTO v_bank_id, v_bank_ctry, v_bank_name
                    FROM iby_ext_banks_v b
                   WHERE 1 = 1
                         AND TRIM (UPPER (b.bank_name)) =
                                TRIM (UPPER (rec_c_branch.bank_name))
                         AND TRIM (NVL (bank_number, 1)) =
                                TRIM (NVL (rec_c_branch.bank_code, 1))
                         AND TRIM ( (b.home_country)) =
                                TRIM ( (rec_c_branch.branch_bank_country));

                  DBMS_OUTPUT.
                   put_line ('Retriving the bank_id:' || v_bank_id);
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     DBMS_OUTPUT.
                      put_line (
                           'Bank Not exists in the system :'
                        || rec_c_branch.bank_name
                        || SQLERRM);
                  WHEN OTHERS
                  THEN
                     -- retcode := 1;
                     DBMS_OUTPUT.
                      put_line (
                           'Bank Not exists in the system :'
                        || rec_c_branch.bank_name
                        || SQLERRM);
               END;

               DBMS_OUTPUT.
                put_line (
                     'Start BRANCH API'
                  || ' '
                  || rec_c_branch.branch_name
                  || ' '
                  || v_bank_id);
               v_branch_type := 'OTHER';
               DBMS_OUTPUT.put_line ('creating new branch through api');
               v_error_reason := NULL;
               v_return_status := NULL;
               v_msg_count := NULL;
               v_msg_data := NULL;
               v_extbankbranch_rec_type.bank_party_id := v_bank_id;
               v_extbankbranch_rec_type.branch_name :=
                  rec_c_branch.bank_branch;
               v_extbankbranch_rec_type.branch_number :=
                  rec_c_branch.Branch_Number;
               v_extbankbranch_rec_type.branch_type := v_branch_type;
               v_extbankbranch_rec_type.bic := rec_c_branch.bank_swiftcode;
               DBMS_OUTPUT.
                put_line (
                     'Bank ID - Branch name'
                  || v_bank_id
                  || ' - '
                  || rec_c_branch.branch_name);
               DBMS_OUTPUT.put_line ('BEFORE CREATE BRANCH ');

               BEGIN
                  iby_ext_bankacct_pub.
                   create_ext_bank_branch (
                     p_api_version           => 1.0,
                     p_init_msg_list         => fnd_api.g_true,
                     p_ext_bank_branch_rec   => v_extbankbranch_rec_type,
                     x_branch_id             => v_branch_id,
                     x_return_status         => v_return_status,
                     x_msg_count             => v_msg_count,
                     x_msg_data              => v_msg_data,
                     x_response              => v_result_rec_type);

                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_status = 'CREATED',
                         bank_branch_error = lc_msg_data,
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     UPDATE rgap_supp_emp_stg
                        SET bank_branch_status = 'ERROR',
                            bank_branch_error = 'ERROR',
                            bank_branch_id = v_branch_id
                      WHERE ROWID = rec_c_branch.br_row_id;
               END;

               COMMIT;
               DBMS_OUTPUT.
                put_line ('Branch return status ' || v_return_status);
               DBMS_OUTPUT.put_line ('Bank id ' || v_branch_id);

               IF (v_return_status <> 'S')
               THEN
                  FOR i IN 1 .. v_msg_count
                  LOOP
                     apps.fnd_msg_pub.get (i,
                                           apps.fnd_api.g_false,
                                           lc_msg_data,
                                           lc_msg_dummy);
                     lc_output :=
                        lc_output
                        || (   TO_CHAR (i)
                            || ': '
                            || SUBSTR (lc_msg_data, 1, 250));
                  END LOOP;

                  DBMS_OUTPUT.
                   put_line (
                     'Error Occured while Creating Bank Branch: '
                     || lc_msg_data);
                  DBMS_OUTPUT.
                   put_line (
                     'v_branch_id --> ' || v_branch_id || lc_msg_data);

                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_status = 'ERROR',
                         bank_branch_error = lc_msg_data,
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id
                         AND bank_branch_status = 'NEW';

                  COMMIT;
               ELSE
                  DBMS_OUTPUT.put_line ('v_branch_id F --> ' || v_branch_id);

                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_status = 'CREATED',
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id
                         AND bank_branch_status = 'NEW';
               END IF;

               COMMIT;
               DBMS_OUTPUT.put_line ('End of CRAETE BRANCH');
               DBMS_OUTPUT.
                put_line (
                  'Bank BRANCH Loop Ended for ' || rec_c_branch.branch_name);
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;
         DBMS_OUTPUT.put_line ('c_bank_bra_ass vvendorname ');

         BEGIN
            FOR i IN c_bank_bra_ass
            LOOP
               vvendorname := TRIM (i.vendor_name);
               vbankname := i.bank_name;
               DBMS_OUTPUT.
                put_line ('c_bank_bra_ass i.vendor_name ' || i.vendor_name);

               BEGIN
                  SELECT party_id
                    INTO vsupplierpartyid
                    FROM ap_suppliers
                   WHERE TRIM (vendor_name) = TRIM (i.vendor_name);

                  DBMS_OUTPUT.put_line (vsupplierpartyid);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vsupplierpartyid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Supplier Details '
                        || SQLERRM);
               END;

               BEGIN
                  SELECT bank_party_id, branch_party_id
                    INTO vbankid, vbankbranchid
                    FROM ce_bank_branches_v
                   WHERE     UPPER (bank_name) = UPPER (i.bank_name)
                         AND bank_branch_name = i.bank_branch
                         AND NVL (bank_number, 1) = NVL (i.bank_code, 1)
                         AND NVL (BRANCH_NUMBER, 1) =
                                NVL (i.BRANCH_NUMBER, 1);

                  DBMS_OUTPUT.put_line (vbankid || ',' || vbankbranchid);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vbankid := 0;
                     vbankbranchid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Bank Branch Details '
                        || SQLERRM);
               END;

               BEGIN
                  SELECT party_site_id
                    INTO l_party_site_id
                    FROM ap_supplier_sites_all
                   WHERE vendor_id = i.ora_vendor_id
                         AND vendor_site_id = i.vendor_site_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     l_party_site_id := NULL;
               END;

               ---
               pextbankacctrec.object_version_number := 1.0;
               pextbankacctrec.acct_owner_party_id := vsupplierpartyid;
               pextbankacctrec.bank_account_name := TRIM (i.account_name);
               pextbankacctrec.bank_account_num := TRIM (i.account_number);
               pextbankacctrec.bank_id := vbankid;
               pextbankacctrec.branch_id := vbankbranchid;
               pextbankacctrec.start_date := SYSDATE;
               pextbankacctrec.country_code := i.bank_country;
               pextbankacctrec.iban := i.iban;
               pextbankacctrec.currency := i.currency;
               --pExtBankAcctRec.currency := 'USD';
               pextbankacctrec.foreign_payment_use_flag := 'Y';

               BEGIN
                  iby_ext_bankacct_pub.
                   create_ext_bank_acct (
                     p_api_version         => papiversion,
                     p_init_msg_list       => pinitmsglist,
                     p_ext_bank_acct_rec   => pextbankacctrec,
                     p_association_level   => 'SS',
                     p_supplier_site_id    => i.vendor_site_id,
                     p_party_site_id       => l_party_site_id,
                     p_org_id              => i.operating_unit_id,
                     p_org_type            => 'OPERATING_UNIT',
                     --Bug7136876: new parameter
                     x_acct_id             => xacctid,
                     x_return_status       => xreturnstatus,
                     x_msg_count           => xmsgcount,
                     x_msg_data            => xmsgdata,
                     x_response            => xresponse);

                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_ass_status = 'APICREATED',
                         bank_br_ass_status_error =
                            xmsgdata || '-' || xreturnstatus
                   WHERE ROWID = i.br_row_id;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     UPDATE rgap_supp_emp_stg
                        SET bank_branch_ass_status = 'APIERROR',
                            bank_br_ass_status_error = xmsgdata
                      WHERE ROWID = i.br_row_id;
               END;

               --COMMIT;
               DBMS_OUTPUT.put_line ('xReturnStatus :' || xreturnstatus);

               IF xreturnstatus = 'S'
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_ass_status = 'CREATED'
                   WHERE ROWID = i.br_row_id
                         AND bank_branch_ass_status = 'NEW';

                  COMMIT;
               ELSE
                  IF xmsgcount > 1
                  THEN
                     FOR i IN 1 .. xmsgcount
                     LOOP
                        DBMS_OUTPUT.
                         put_line (
                           i || '.'
                           || SUBSTR (
                                 fnd_msg_pub.
                                  get (p_encoded => fnd_api.g_false),
                                 1,
                                 255));
                     END LOOP;

                     UPDATE rgap_supp_emp_stg
                        SET bank_branch_ass_status = 'ERROR',
                            bank_br_ass_status_error = 'Not Assigned'
                      WHERE ROWID = i.br_row_id
                            AND bank_branch_ass_status = 'NEW';

                     COMMIT;
                  END IF;
               --ROLLBACK;
               END IF;
            ----
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('error ' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;

      BEGIN
         v_bank_id := NULL;
         lc_output := NULL;
         lc_msg_dummy := NULL;
         v_bank_ctry := NULL;
         v_bank_branch := NULL;
         v_branch_id := NULL;
         lc_msg_data := NULL;
         v_error_reason := NULL;
         v_return_status := NULL;
         v_msg_count := NULL;
         v_msg_data := NULL;
         v_branch_type := NULL;
         v_bank_name := NULL;
         vbankname := NULL;
         vvendorname := NULL;
         vsupplierpartyid := NULL;     -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
         vbankid := NULL;                            -- EXISTING BANK PARTY ID
         vbankbranchid := NULL;                    -- EXISTING BRANCH PARTY ID
         xreturnstatus := NULL;
         xmsgcount := NULL;
         xmsgdata := NULL;
         vinstrumentid := NULL;

         --V_EXTBANK_REC_TYPE := NULL;
         --V_EXTBANK_REC_TYPE := NULL;

         /*Vendor Bank and Branch assignment code*/
         BEGIN
            FOR i IN c_bank_ven_bra_ass
            LOOP
               vvendorname := TRIM (i.vendor_name);
               vbankname := i.bank_name;

               BEGIN
                  SELECT bank_party_id, branch_party_id
                    INTO vbankid, vbankbranchid
                    FROM ce_bank_branches_v
                   WHERE UPPER (bank_name) = UPPER (vbankname)
                         AND bank_branch_name = i.branch_name
                         AND NVL (bank_number, 1) =
                                TRIM (NVL (i.bank_code, 1))
                         AND NVL (BRANCH_NUMBER, 1) =
                                TRIM (NVL (I.BRANCH_NUMBER, 1));
               -- Replace with your Bank Name
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vbankid := 0;
                     vbankbranchid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Bank Branch Details '
                        || SQLERRM);
               END;

               -- SELECT EXISTING Supplier
               BEGIN
                  SELECT ext_bank_account_id
                    INTO vinstrumentid
                    FROM iby_ext_bank_accounts
                   WHERE     branch_id = vbankbranchid
                         AND bank_id = vbankid
                         AND bank_account_name = TRIM (i.account_name)
                         AND TRIM (bank_account_num) =
                                TRIM (i.account_number);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vinstrumentid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'Exception while fetching Supplier BANK ' || SQLERRM);
               END;

               -- SELECT EXISTING Supplier
               BEGIN
                  SELECT party_id
                    INTO vsupplierpartyid
                    FROM ap_suppliers
                   WHERE TRIM (vendor_name) = TRIM (i.vendor_name);
               -- Replace with your Supplier Name
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     vsupplierpartyid := 0;
                     DBMS_OUTPUT.
                      put_line (
                        'vsupplierpartyid Exception while fetching Supplier Details '
                        || SQLERRM);
               END;

               pinstrument.instrument_type := 'BANKACCOUNT';
               pinstrument.instrument_id := vinstrumentid;
               --<bank account id> IBY_EXT_BANKACCT_PUB.CREATE_EXT_BANK_ACCT API account id
               passignmentattribs.start_date := SYSDATE;
               passignmentattribs.instrument := pinstrument;
               ppayee.party_id := vsupplierpartyid;
               ppayee.party_site_id := i.party_site_id;
               ppayee.supplier_site_id := i.vendor_site_id;
               ppayee.payment_function := 'PAYABLES_DISB';
               ppayee.org_id := i.operating_unit_id;        -- Organization ID
               -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID -- which is party_id of the supplier
               /* iby_disbursement_setup_pub.
               set_payee_instr_assignment (
               p_api_version => 1.0,
               p_init_msg_list => fnd_api.g_false,
               p_commit => fnd_api.g_true,
               x_return_status => xreturnstatus,
               x_msg_count => xmsgcount,
               x_msg_data => xmsgdata,
               p_payee => ppayee,
               p_assignment_attribs => passignmentattribs,
               x_assign_id => xassignid,
               x_response => xresponse);*/

               COMMIT;

               IF xreturnstatus = 'S'
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET bank_vend_ass_status = 'CREATED'
                   WHERE ROWID = i.br_row_id;

                  COMMIT;
               ELSE
                  IF xmsgcount > 1
                  THEN
                     FOR i IN 1 .. xmsgcount
                     LOOP
                        DBMS_OUTPUT.
                         put_line (
                           i || '.'
                           || SUBSTR (
                                 fnd_msg_pub.
                                  get (p_encoded => fnd_api.g_false),
                                 1,
                                 255));
                     END LOOP;

                     UPDATE rgap_supp_emp_stg
                        SET bank_vend_ass_status = 'ERROR'
                      WHERE ROWID = i.br_row_id;
                  END IF;

                  COMMIT;
               END IF;
            END LOOP;
         EXCEPTION
            WHEN OTHERS
            THEN
               DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
         END;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
      END;
   END emp_assign_bank_accnts;

   -- *********************************************************************************************
   -- MAIN Procedure To Call the validation program and Insertion Program Based on Parameter Value
   -- *********************************************************************************************
   PROCEDURE emp_main (x_errbuf OUT VARCHAR2, x_retcode OUT NUMBER)
   AS
      lv_retcode   NUMBER := 0;
      lv_errmess   VARCHAR2 (3000) := NULL;
      lv_errcode   VARCHAR2 (300) := 0;
   BEGIN
      emp_vendor_rec_duplicate;
      --emp_supplier_record_exists;
      LOG (
         '******************** EXECUTION OF SUPPLIER VALIDATION PROCEDURE *****************');
      emp_validate_suppliers (lv_retcode);

      IF lv_retcode = 1
      THEN
         x_retcode := 1;
      ELSE
         x_retcode := 0;
      END IF;

      LOG ('x_retcode ' || x_retcode);
      emp_supplier_record_count;
      LOG (
         '******************** EXECUTION OF SUPPLIER CREATION PROCEDURE *****************');
      emp_enable_or_create_suppliers;
      COMMIT;
      emp_validate_site_data (lv_retcode, lv_errmess);
      COMMIT;
      emp_create_vendor_site;
      COMMIT;
      emp_assign_bank_accnts;
      COMMIT;
   END emp_main;
---------------------------------------
END rgap_suppliers_conv_pkg;
/


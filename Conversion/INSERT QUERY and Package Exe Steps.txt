Emp as Supplier
---------------
355 records
-------------
select * from rgap_supp_emp_stg_1;

Package Name
------------

DECLARE
   x_errbuf    VARCHAR2 (3000) := NULL;
   x_retcode   NUMBER := 0;
BEGIN
   rgap_suppliers_conv_pkg.emp_main (x_errbuf, x_retcode);
   DBMS_OUTPUT.put_line (x_errbuf);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (SQLERRM);
END;
/

select * 
from rgap_supp_emp_stg 
where BANK_BRANCH_ASS_STATUS != 'ERROR'

=========================================================================================
=========================================================================================
Supplier
---------

Raw table
---------
rgpo_vend_raw_lines_prd
rgpo_vend_raw_hdr_prd

Stage table
------------
copy this table to production
------------------------------
rgap_supp_stg_PRD
rgap_supp_site_stg_PRD


Vendor Headers Insert Statement
-------------------------------

INSERT INTO rgap_supp_stg (LINE_NUM,ALTERNATE_SUPPLIER_NAME,
                              MULTIPLE_SITES,
                              LEGACY_VEND_ID,
                              IDENTIFIER,
                              VENDOR_NAME,
                              TAX_REGN_NUM,
                              TAXPAYER_ID,
                              VENDOR_TYPE,
                              ALLOW_WITHHOLD_TAX,
                              TYPE_1099,
                              BOX_1099,
                              REC_STATUS)
      SELECT A.LINE_NUM,a.ALT_SUPP_NAME,
              A.MULTIPLE_SITES,
              A.LEGACY_VEND_ID,
              UPPER(TRIM(A.IDENTIFIER)),
              TRIM(A.VENDOR_NAME),
              NVL (A.KR_TAX_REGN_NUM, A.TAX_REGN_NUM) ,
              A.TAX_ID_NUM,
              NVL(A.VEND_TYPE,'Supplier'),
              A.allow_withhold_tax,
              (select TYPE_1099 from rgpo_vend_raw_lines_cnv1 where LEGACY_VEND_ID= a.LEGACY_VEND_ID and IDENTIFIER= a.IDENTIFIER) TYPE_1099,
              (select BOX_1099 from rgpo_vend_raw_lines_cnv1 where LEGACY_VEND_ID= a.LEGACY_VEND_ID and IDENTIFIER= a.IDENTIFIER) BOX_1099,
                'NEW'
         FROM  rgpo_vend_raw_hdr_cnv1 A
         --where legacy_vend_id not in ('US003581','IE000427','KP000407')
         order by A.LINE_NUM

 
 
 Vendor Sites Insert Statement
-------------------------------
 
 INSERT INTO rgap_supp_site_stg (RECORD_ID,
                                    OPERATING_UNIT,
                                    VENDOR_NAME,
                                    LEGACY_VENDOR_ID,
                                    COUNTRY,
                                    ADDRESS_LINE1,
                                    ADDRESS_LINE2,
                                    ADDRESS_LINE3,
                                    ADDRESS_LINE4,
                                    --COUNTRY,
                                    CITY,
                                    STATE,
                                    ZIP,
                                    PHONE,
                                    TERMS_NAME,
                                    PAYMENT_METHOD_LOOKUP_VAL,
                                    PAY_GROUP_LOOKUP_VAL,
                                    PURCHASING_SITE_FLAG,
                                    PAY_SITE_FLAG,
                                    EMAIL_ADDRESS,
                                    BANK_COUNTRY,
                                    BANK_NAME,
                                    BRANCH_NAME,
                                    BANK_BRANCH,
                                    BANK_CODE_KR,
                                    ACCOUNT_NAME,
                                    IBAN,
                                    SWIFT_CODE,
                                    TAX_SCHEDULE_ID,
                                    BANK_ACCOUNT,
                                    BANK_STATUS,
                                    BANK_BRANCH_STATUS,
                                    BANK_BRANCH_ASS_STATUS,
                                    BANK_VEND_ASS_STATUS,
                                    STATUS_CODE,
                                    GP_DATABASE,
                                    awt_group_val,
                                    V_CONTACT_NAME,
                                    PH_EMAIL_STATUS,
                                    ALLOW_AWT_FLAG)
  (SELECT TRIM (B.LINE_NUM),
               --'KOREA_OU',          --'IRELAND_OU',--'KOREA_OU',--'IRELAND_OU',
                decode(GP_DATABASE,
 'Ireland','RGL_IE_EUR_OU', --
 'Ireland Merch','MER_IE_EUR_OU', --
 'Korea','RGK_KR_KRW_OU', --
 'United States','RGI_US_USD_OU', --
 'Germany','RGG_DE_EUR_OU', --
 'RG Merch','RGI_US_USD_OU', -- 
 'Australia','RGA_AU_AUD_OU', -- 
 'RG Direct','RGI_US_USD_OU', --
 'Malta','RGP_MT_EUR_OU',
 'HK Merch','RGI_US_USD_OU', --
 'UK','RGS_GB_GBP_OU', --
 'RG Pictures','RGI_US_USD_OU', --
 null) OU,              
               TRIM(UPPER(B.IDENTIFIER)),
               TRIM (B.LEGACY_VEND_ID),
               TRIM (B.COUNTRY),
               decode(TRIM (B.ADDRESS1),'N/A','To be changed later_'||B.LEGACY_VEND_ID,TRIM (B.ADDRESS1)),
               TRIM (B.ADDRESS2),
               TRIM (B.ADDRESS3),
               TRIM (B.ADDRESS4),
               --TRIM (B.COUNTRY),
               TRIM (B.CITY),
               TRIM (B.STATE),
               TRIM (B.ZIP_CODE),
               TRIM (B.PHONE_NUM1),
               (SELECT NAME FROM AP_TERMS WHERE UPPER(NAME) = UPPER(DECODE(TRIM(terms),'Net 7 days','Net 7','NET 60 DAYS','Net 60',
               'EOM','Net 30',TRIM(terms))))TERMS,
               TRIM (B.PAYMENT_METHOD),
               TRIM (B.PAY_GROUP),
               'Y',
               'Y',
               TRIM (B.EMAIL),
               TRIM (B.BANK_COUNTRY),
               TRIM (B.CNTRY_BANK_NAME),
               TRIM (B.BRANCH_NAME),
               TRIM (B.BANK_BRANCH),
               TRIM (B.BANK_CODE),
               TRIM (NVL(DECODE(B.ACCT_NAME,'',NULL,B.ACCT_NAME),NULL)),
               TRIM (B.IBAN),
               TRIM (B.SWIFT_CODE),
               --B.BANK_CODE,
               TRIM (B.TAX_SCHLD_ID),
               TRIM (NVL(DECODE(B.ACCT_NUMBER,'',NULL,B.ACCT_NUMBER),NULL)),
               'NEW',
               'NEW',
               'NEW',
               'NEW',
               'NEW',
               TRIM (B.GP_DATABASE),
               TRIM (A.INV_WITHHOLD_TAX_GRP),
               decode(B.GP_DATABASE,'Korea',null, DECODE (TRIM (B.VENDOR_CONTACT),'N/A', NULL,TRIM (B.VENDOR_CONTACT))
                          ),
               'NEW',
               DECODE (UPPER (A.ALLOW_WITHHOLD_TAX),
                       'YES', 'Y',
                       'NO', 'N',
                       NULL)
          FROM rgpo_vend_raw_hdr_cnv1 A, rgpo_vend_raw_lines_cnv1 B
         WHERE UPPER(A.IDENTIFIER) = UPPER(B.IDENTIFIER)
         --AND A.LEGACY_VEND_ID NOT in  ('US003581','IE000427','KP000407')
         --AND B.GP_DATABASE = 'Ireland'
      );

      
      
      Update Statement
      ------------------
			      UPDATE rgap_supp_site_stg
                                         SET ERROR_MESSAGE = 'Invalid address line',
                                           STATUS_CODE = 'ERROR'
                                           where STATUS_CODE = 'NEW'
                                   AND ADDRESS_LINE1 LIKE 'N/A%';
                                   
Update Statement
------------------                                   
                                   UPDATE rgap_supp_site_stg
                                         SET ERROR_MESSAGE = 'Invalid Terms',
                                           STATUS_CODE = 'ERROR'
                                           where STATUS_CODE = 'NEW'
                                   AND terms_name is null;
                                   
     
                                     
                                     
Package Name
------------

DECLARE
   x_errbuf    VARCHAR2 (3000) := NULL;
   x_retcode   NUMBER := 0;
BEGIN
   rgap_suppliers_conv_pkg.main (x_errbuf, x_retcode);
   DBMS_OUTPUT.put_line (x_errbuf);
EXCEPTION
   WHEN OTHERS
   THEN
      DBMS_OUTPUT.put_line (SQLERRM);
END;
/


SELECT * FROM rgap_supp_site_stg
                                  where 1=1
                                  and VENDOR_NAME like '%_PDATA'
                                  --and OPERATING_UNIT = 'RGA_AU_AUD_OU'
                                 --and STATUS_CODE != 'ERROR'
                                 AND BANK_BRANCH_ASS_STATUS = 'ERROR'
                                 AND vendor_site_status_code = 'CREATED'
CREATE OR REPLACE PACKAGE APPS.RGAP_SUPPLIERS_CONV_PKG
IS
 -- Procedure to display the information to the log file.

 PROCEDURE LOG (p_msgtxt_in IN VARCHAR2);

 -- Procedure to display the information to the out file.

 PROCEDURE output (p_msgtxt_in IN VARCHAR2);

 -- Procedure to update the duplicate

 PROCEDURE vendor_rec_duplicate;

 -- Procedure to find the supplier record count

 PROCEDURE supplier_record_count;

 -- Procedure to Call all the validation Required fields

 PROCEDURE validate_suppliers (lv_retcode OUT NUMBER);

 -- Procedure to Enable the created party as supplier or crete new supplier

 PROCEDURE enable_or_create_suppliers;


 PROCEDURE vendor_site_rec_duplicate;

 -- Procedure to update the supplier site Combination in apps

 PROCEDURE supp_site_comb_apps;

 -- Procedure to update the supplier site Combination

 PROCEDURE supp_site_comb;

 -- Procedure to create new supplier site

 PROCEDURE create_vender_site_contact (p_err_code OUT VARCHAR2,
 p_err_mess OUT VARCHAR2,
 p_vendor_id NUMBER,
 p_vendor_site_id NUMBER,
 p_party_site_id NUMBER,
 p_org_id NUMBER,
 p_contact_first_name VARCHAR2,
 p_contact_last_name VARCHAR2,
 p_Phone_No VARCHAR2,
 P_ph_EXT VARCHAR2,
 p_alt_Ph_No VARCHAR2,
 p_Email VARCHAR2);

 PROCEDURE create_vendor_site;

 PROCEDURE assign_bank_accnts;

 -- Procedure to perform validation or Insertion based on the parameter value


 PROCEDURE main (x_errbuf OUT VARCHAR2, x_retcode OUT NUMBER);



 PROCEDURE emp_vendor_rec_duplicate;

 -- Procedure to find the supplier record count
 PROCEDURE emp_supplier_record_count;

 -- Procedure to Call all the validation Required fields
 PROCEDURE emp_validate_suppliers (lv_retcode OUT NUMBER);

 -- Procedure to Enable the created party as supplier or crete new supplier
 PROCEDURE emp_enable_or_create_suppliers;

 PROCEDURE emp_validate_site_data (p_errcode OUT NUMBER,
 p_errmess OUT VARCHAR2);

 PROCEDURE emp_create_vendor_site;

 PROCEDURE emp_assign_bank_accnts;

 -- Procedure to perform validation or Insertion based on the parameter value
 PROCEDURE emp_main (x_errbuf OUT VARCHAR2, x_retcode OUT NUMBER);
END RGAP_SUPPLIERS_CONV_PKG;
/


DECLARE
   v_error_reason             VARCHAR2 (2000) := NULL;
   v_return_status            VARCHAR2 (2000) := NULL;
   v_msg_count                NUMBER := NULL;
   v_msg_data                 VARCHAR2 (3000) := NULL;
   v_out_mesg                 apps.iby_fndcpt_common_pub.result_rec_type;
   v_response                 iby_fndcpt_common_pub.result_rec_type;
   v_result_rec_type          iby_fndcpt_common_pub.result_rec_type;
   v_extbank_rec_type         iby_ext_bankacct_pub.extbank_rec_type;
   v_extbankbranch_rec_type   iby_ext_bankacct_pub.extbankbranch_rec_type;
   v_bank_acct_rec            apps.iby_ext_bankacct_pub.extbankacct_rec_type;
   v_assign                   apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
   v_payee_rec                apps.iby_disbursement_setup_pub.payeecontext_rec_type;
   v_branch_type              VARCHAR2 (2000);
   v_bank_id                  NUMBER;
   v_branch_id                NUMBER;
   exec_bank_acct             EXCEPTION;
   v_supplier_site_id         NUMBER;
   v_party_site_id            NUMBER;
   v_currency_code            VARCHAR2 (5);
   v_bank_ctry                VARCHAR2 (5);
   v_bank_name                VARCHAR2 (50);
   v_bank_branch              VARCHAR2 (50);
   v_msg_dummy                VARCHAR2 (3000);
   p_location_rec             hz_location_v2pub.location_rec_type;
   v_location_id              NUMBER;
   p_party_site_rec           hz_party_site_v2pub.party_site_rec_type;
   v_party_site_number        NUMBER;
   l_bank_party_id            NUMBER;
   l_branch_party_id          NUMBER;
   l_vendor_id                NUMBER;
   l_vendor_site_id           NUMBER;
   l_acct                     NUMBER;
   lc_output                  VARCHAR2 (3000);
   lc_msg_dummy               VARCHAR2 (3000);
   lc_return_status           VARCHAR2 (3000);
   v_out_mesg                 apps.iby_fndcpt_common_pub.result_rec_type;
   v_bank_acct_rec            apps.iby_ext_bankacct_pub.extbankacct_rec_type;
   v_assign                   apps.iby_fndcpt_setup_pub.pmtinstrassignment_tbl_type;
   v_payee_rec                apps.iby_disbursement_setup_pub.payeecontext_rec_type;
   exec_bank_acct             EXCEPTION;
   v_supplier_site_id         NUMBER;
   v_party_site_id            NUMBER;
   v_currency_code            VARCHAR2 (5);
   v_msg_dummy                VARCHAR2 (3000);
   p_location_rec             hz_location_v2pub.location_rec_type;
   v_location_id              NUMBER;
   p_party_site_rec           hz_party_site_v2pub.party_site_rec_type;
   v_party_site_number        NUMBER;
   l_bank_party_id            NUMBER;
   l_branch_party_id          NUMBER;
   l_vendor_id                NUMBER;
   l_vendor_site_id           NUMBER;
   l_acct                     NUMBER;
   --lc_output VARCHAR2 (3000);
   --lc_msg_dummy VARCHAR2 (3000);
   lc_return_status           VARCHAR2 (3000);
   lc_msg_data                VARCHAR2 (3000);
   papiversion                NUMBER := 1.0;
   pinitmsglist               VARCHAR2 (1) := 'F';
   xreturnstatus              VARCHAR2 (2000);
   xmsgcount                  NUMBER (5);
   xmsgdata                   VARCHAR2 (2000);
   vbankname                  VARCHAR2 (100);
   vvendorname                VARCHAR2 (100);
   xresponse                  iby_fndcpt_common_pub.result_rec_type;
   pextbankacctrec            iby_ext_bankacct_pub.extbankacct_rec_type;
   vsupplierpartyid           NUMBER;
   -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
   vbankid                    NUMBER;                -- EXISTING BANK PARTY ID
   vbankbranchid              NUMBER;              -- EXISTING BRANCH PARTY ID
   xacctid                    NUMBER;
   pcount                     NUMBER;
   --vinstrumentid number;
   --pInstrument IBY_FNDCPT_SETUP_PUB.PMTINSTRUMENT_REC_TYPE;
   xassignid                  NUMBER;
   ppayee                     iby_disbursement_setup_pub.payeecontext_rec_type;
   passignmentattribs         iby_fndcpt_setup_pub.pmtinstrassignment_rec_type;
   pinstrument                iby_fndcpt_setup_pub.pmtinstrument_rec_type;
   vinstrumentid              NUMBER;

   CURSOR c_bank
   IS
      SELECT DISTINCT TRIM ( (mas.vendor_name)) vendor_name,
                      TRIM ( (vendor_site_code)) vendor_site_code,
                      TRIM ( (country_code)) country,
                      TRIM ( (bank_name)) bank_name,
                      TRIM ( (country_name)) bank_country,
                      TRIM (mas.bank_code) bank_code,
                      vendor_site_id,
                      mas.ROWID b_row_id
        FROM rgap_supp_emp_stg mas
       WHERE     bank_status = 'NEW'
             AND vendor_rec_status = 'CREATED'
             AND entity = 'Australia';

   CURSOR c_branch
   IS
      SELECT DISTINCT TRIM ( (rss.vendor_name)) vendor_name,
                      TRIM ( (rss.vendor_site_code)) vendor_site_code,
                      TRIM ( (rss.country_code)) country,
                      TRIM ( (rss.bank_name)) bank_name,
                      rss.branch_name bank_branch,
                      (SELECT territory_code
                         FROM fnd_territories_vl
                        WHERE territory_short_name = TRIM (rss.country_name))
                         branch_bank_country,
                      ROWID br_row_id,
                      NULL intermediary_bank,
                      NULL sort_code,
                      swift bank_swiftcode,
                      NULL abi,
                      NULL cab,
                      NULL bank_routing_num,
                      rss.branch_name branch_name,
                      NULL bank_countrol_key,
                      NULL bsb,
                      TRIM (rss.account_name) account_name,
                      rss.account_number operating_unit_id,
                      ora_vendor_id,
                      entity,
                      iban,
                      currency,
                      vendor_site_id,
                      party_site_id,
                      TRIM (rss.bank_code) bank_code,
                      Branch_Number
        FROM rgap_supp_emp_stg rss
       WHERE     bank_branch_status = 'NEW'
             AND vendor_rec_status = 'CREATED'
             AND entity = 'Australia';

   CURSOR c_bank_bra_ass
   IS
      SELECT DISTINCT TRIM ( (rss.vendor_name)) vendor_name,
                      TRIM ( (rss.vendor_site_code)) vendor_site_code,
                      TRIM ( (rss.country_code)) country,
                      TRIM ( (rss.bank_name)) bank_name,
                      rss.branch_name bank_branch,
                      (SELECT territory_code
                         FROM fnd_territories_vl
                        WHERE territory_short_name = TRIM (rss.country_name))
                         bank_country,
                      ROWID br_row_id,
                      NULL intermediary_bank,
                      NULL sort_code,
                      swift bank_swiftcode,
                      NULL abi,
                      NULL cab,
                      NULL bank_routing_num,
                      rss.branch_name branch_name,
                      NULL bank_countrol_key,
                      NULL bsb,
                      TRIM (rss.account_name) account_name,
                      rss.account_number,
                      -- OPERATING_UNIT_ID,
                      ora_vendor_id,
                      --VENDOR_SITE_ID,
                      entity,
                      iban,
                      currency,
                      vendor_site_id,
                      party_site_id,
                      operating_unit_id,
                      TRIM (rss.bank_code) bank_code,
                      BRANCH_NUMBER
        FROM rgap_supp_emp_stg rss
       WHERE     bank_branch_ass_status = 'NEW'
             AND vendor_rec_status = 'CREATED'
             AND entity = 'Australia';

   CURSOR c_bank_ven_bra_ass
   IS
      SELECT DISTINCT TRIM ( (rss.vendor_name)) vendor_name,
                      --TRIM ( (rss.vendor_site_code)) vendor_site_code,
                      TRIM ( (rss.country_code)) country,
                      TRIM ( (rss.bank_name)) bank_name,
                      rss.branch_name bank_branch,
                      (SELECT territory_code
                         FROM fnd_territories_vl
                        WHERE territory_short_name = TRIM (rss.country_name))
                         bank_country,
                      ROWID br_row_id,
                      NULL intermediary_bank,
                      NULL sort_code,
                      swift bank_swiftcode,
                      NULL abi,
                      NULL cab,
                      NULL bank_routing_num,
                      rss.branch_name branch_name,
                      NULL bank_countrol_key,
                      NULL bsb,
                      TRIM (rss.account_name) account_name,
                      rss.account_number,
                      rss.party_site_id,
                      rss.vendor_site_id,
                      rss.operating_unit_id,
                      TRIM (rss.bank_code) bank_code,
                      BRANCH_NUMBER
        FROM rgap_supp_emp_stg rss
       WHERE     bank_vend_ass_status = 'NEW'
             AND vendor_rec_status = 'CREATED'
             AND entity = 'Australia';

   l_bank_cnt_code            VARCHAR2 (300) := NULL;
   l_party_site_id            NUMBER := NULL;
   l_group_id                 VARCHAR2 (300) := NULL;
   l_resp_id                  NUMBER;
   l_resp_appl_id             NUMBER;
   l_user_id                  NUMBER;
BEGIN
   SELECT user_id
     INTO l_user_id
     FROM fnd_user
    WHERE UPPER (user_name) = UPPER ('SYSADMIN');

   SELECT responsibility_id, application_id
     INTO l_resp_id, l_resp_appl_id
     FROM fnd_responsibility_vl
    WHERE application_id = 200 AND responsibility_name = 'Payables Manager';

   mo_global.init ('SQLAP');
   fnd_global.apps_initialize (user_id        => l_user_id,
                               resp_id        => l_resp_id,
                               resp_appl_id   => l_resp_appl_id);

   BEGIN
      v_bank_id := NULL;
      lc_output := NULL;
      lc_msg_dummy := NULL;
      v_bank_ctry := NULL;
      v_bank_branch := NULL;
      v_branch_id := NULL;
      lc_msg_data := NULL;
      v_error_reason := NULL;
      v_return_status := NULL;
      v_msg_count := NULL;
      v_msg_data := NULL;
      v_branch_type := NULL;
      v_bank_name := NULL;
      vbankname := NULL;
      vvendorname := NULL;
      vsupplierpartyid := NULL;        -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid := NULL;                               -- EXISTING BANK PARTY ID
      vbankbranchid := NULL;                       -- EXISTING BRANCH PARTY ID
      xreturnstatus := NULL;
      xmsgcount := NULL;
      xmsgdata := NULL;
      vinstrumentid := NULL;

      --V_EXTBANK_REC_TYPE := NULL;
      --V_EXTBANK_REC_TYPE := NULL;

      /*Bank Creation code*/
      BEGIN
         FOR rec_c_bank IN c_bank
         LOOP
            DBMS_OUTPUT.put_line ('HI');
            v_bank_id := NULL;
            v_bank_ctry := NULL;
            v_bank_name := NULL;
            v_bank_branch := NULL;
            DBMS_OUTPUT.put_line (
               'Bank Loop Started for ' || rec_c_bank.bank_name);
            DBMS_OUTPUT.put_line ('Start Of CRAETE BANK');
            v_error_reason := NULL;
            v_return_status := NULL;
            v_msg_count := NULL;
            v_msg_data := NULL;
            v_extbank_rec_type.object_version_number := 1.0;
            v_extbank_rec_type.bank_name := rec_c_bank.bank_name;
            -- v_extbank_rec_type.bank_number := rec_c_bank.bank_routing_num;
            v_extbank_rec_type.institution_type := 'BANK';

            IF rec_c_bank.bank_country IS NOT NULL
            THEN
               BEGIN
                  SELECT territory_code
                    INTO l_bank_cnt_code
                    FROM fnd_territories_tl
                   WHERE     UPPER (territory_short_name) =
                                UPPER (TRIM (rec_c_bank.bank_country))
                         AND LANGUAGE = USERENV ('LANG');           -----'US';
               EXCEPTION
                  WHEN NO_DATA_FOUND
                  THEN
                     lc_msg_data :=
                           lc_msg_data
                        || rec_c_bank.bank_country
                        || '- Country name does not exist;';
                  WHEN OTHERS
                  THEN
                     lc_msg_data :=
                           lc_msg_data
                        || rec_c_bank.bank_country
                        || '-error while validating Country:';
               END;
            ELSE
               l_bank_cnt_code := NULL;
            END IF;

            v_extbank_rec_type.country_code :=    /*rec_c_bank.bank_country;*/
                                              l_bank_cnt_code;
            v_extbank_rec_type.description := NULL;
            v_extbank_rec_type.bank_number := rec_c_bank.bank_code;

            --v_extbank_rec_type.bank_name :=rec_c_bank.bsb;
            BEGIN
               iby_ext_bankacct_pub.create_ext_bank (
                  p_api_version     => 1.0,
                  p_init_msg_list   => fnd_api.g_true,
                  p_ext_bank_rec    => v_extbank_rec_type,
                  x_bank_id         => v_bank_id,
                  x_return_status   => v_return_status,
                  x_msg_count       => v_msg_count,
                  x_msg_data        => v_msg_data,
                  x_response        => v_response);

               UPDATE rgap_supp_emp_stg
                  SET bank_id = v_bank_id,
                      bank_status = 'CREATED',
                      bank_error = v_msg_data || '-' || v_return_status
                WHERE ROWID = rec_c_bank.b_row_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET bank_id = v_bank_id, bank_status = 'APIERROR'
                   WHERE ROWID = rec_c_bank.b_row_id;
            END;

            DBMS_OUTPUT.put_line ('Bank return status ' || v_return_status);
            DBMS_OUTPUT.put_line ('Bank id ' || v_bank_id);

            IF (v_return_status <> 'S')
            THEN
               FOR i IN 1 .. v_msg_count
               LOOP
                  apps.fnd_msg_pub.get (i,
                                        apps.fnd_api.g_false,
                                        lc_msg_data,
                                        lc_msg_dummy);
                  lc_output :=
                        lc_output
                     || (TO_CHAR (i) || ': ' || SUBSTR (lc_msg_data, 1, 250));
               END LOOP;

               fnd_file.put_line (
                  fnd_file.LOG,
                  'Error Occured while Creating Bank: ' || lc_msg_data);
               -- DBMs_OUTPUT.PUT_LINE( v_return_status );
               --DBMs_OUTPUT.PUT_LINE( lc_msg_data);
               DBMS_OUTPUT.put_line (' test s' || v_bank_id);

               UPDATE rgap_supp_emp_stg
                  SET bank_id = v_bank_id,
                      bank_status = 'CREATED',
                      bank_error = v_msg_data
                WHERE ROWID = rec_c_bank.b_row_id;

               --COMMIT;
            ELSE
               DBMS_OUTPUT.put_line (' test f ' || v_bank_id);

               UPDATE rgap_supp_emp_stg
                  SET bank_id = v_bank_id,
                      bank_error = lc_msg_data,
                      bank_status = 'ERROR'
                WHERE ROWID = rec_c_bank.b_row_id;

               --COMMIT;
            END IF;

            --COMMIT;
            fnd_file.put_line (fnd_file.LOG,
                               'BANK API ERROR-' || v_error_reason);
            fnd_file.put_line (fnd_file.LOG, 'End of CRAETE BANK');
            fnd_file.put_line (
               fnd_file.LOG,
               'Bank Loop Ended for ' || rec_c_bank.bank_name);
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('error ' || SQLERRM);
   END;

   BEGIN
      v_bank_id := NULL;
      lc_output := NULL;
      lc_msg_dummy := NULL;
      v_bank_ctry := NULL;
      v_bank_branch := NULL;
      v_branch_id := NULL;
      lc_msg_data := NULL;
      v_error_reason := NULL;
      v_return_status := NULL;
      v_msg_count := NULL;
      v_msg_data := NULL;
      v_branch_type := NULL;
      v_bank_name := NULL;
      vbankname := NULL;
      vvendorname := NULL;
      vsupplierpartyid := NULL;        -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid := NULL;                               -- EXISTING BANK PARTY ID
      vbankbranchid := NULL;                       -- EXISTING BRANCH PARTY ID
      xreturnstatus := NULL;
      xmsgcount := NULL;
      xmsgdata := NULL;
      vinstrumentid := NULL;

      --V_EXTBANK_REC_TYPE := NULL;
      --V_EXTBANK_REC_TYPE := NULL;

      /*Bank Branch Creation code*/
      BEGIN
         FOR rec_c_branch IN c_branch
         LOOP
            v_branch_id := NULL;
            v_bank_id := NULL;
            v_bank_ctry := NULL;
            v_bank_name := NULL;
            v_error_reason := NULL;
            v_return_status := NULL;
            v_msg_count := NULL;
            v_msg_data := NULL;

            BEGIN
               DBMS_OUTPUT.put_line (
                  'Retriving the bank_id:' || rec_c_branch.bank_name);
               DBMS_OUTPUT.put_line (
                  'rec_c_branch.BRANCH_bank_country:' || rec_c_branch.country);

               SELECT b.bank_party_id, b.home_country, b.bank_name
                 INTO v_bank_id, v_bank_ctry, v_bank_name
                 FROM iby_ext_banks_v b
                WHERE     1 = 1
                      AND TRIM (UPPER (b.bank_name)) =
                             TRIM (UPPER (rec_c_branch.bank_name))
                      AND TRIM (NVL (bank_number, 1)) =
                             TRIM (NVL (rec_c_branch.bank_code, 1))
                      AND TRIM ( (b.home_country)) =
                             TRIM ( (rec_c_branch.branch_bank_country));

               DBMS_OUTPUT.put_line ('Retriving the bank_id:' || v_bank_id);
            EXCEPTION
               WHEN NO_DATA_FOUND
               THEN
                  DBMS_OUTPUT.put_line (
                        'Bank Not exists in the system :'
                     || rec_c_branch.bank_name
                     || SQLERRM);
               WHEN OTHERS
               THEN
                  -- retcode := 1;
                  DBMS_OUTPUT.put_line (
                        'Bank Not exists in the system :'
                     || rec_c_branch.bank_name
                     || SQLERRM);
            END;

            DBMS_OUTPUT.put_line (
                  'Start BRANCH API'
               || ' '
               || rec_c_branch.branch_name
               || ' '
               || v_bank_id);
            v_branch_type := 'OTHER';
            DBMS_OUTPUT.put_line ('creating new branch through api');
            v_error_reason := NULL;
            v_return_status := NULL;
            v_msg_count := NULL;
            v_msg_data := NULL;
            v_extbankbranch_rec_type.bank_party_id := v_bank_id;
            v_extbankbranch_rec_type.branch_name := rec_c_branch.bank_branch;
            v_extbankbranch_rec_type.branch_number :=
               rec_c_branch.Branch_Number;
            v_extbankbranch_rec_type.branch_type := v_branch_type;
            v_extbankbranch_rec_type.bic := rec_c_branch.bank_swiftcode;
            DBMS_OUTPUT.put_line (
                  'Bank ID - Branch name'
               || v_bank_id
               || ' - '
               || rec_c_branch.branch_name);
            DBMS_OUTPUT.put_line ('BEFORE CREATE BRANCH ');

            BEGIN
               iby_ext_bankacct_pub.create_ext_bank_branch (
                  p_api_version           => 1.0,
                  p_init_msg_list         => fnd_api.g_true,
                  p_ext_bank_branch_rec   => v_extbankbranch_rec_type,
                  x_branch_id             => v_branch_id,
                  x_return_status         => v_return_status,
                  x_msg_count             => v_msg_count,
                  x_msg_data              => v_msg_data,
                  x_response              => v_result_rec_type);

               UPDATE rgap_supp_emp_stg
                  SET bank_branch_status = 'CREATED',
                      bank_branch_error = lc_msg_data,
                      bank_branch_id = v_branch_id
                WHERE ROWID = rec_c_branch.br_row_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_status = 'ERROR',
                         bank_branch_error = 'ERROR',
                         bank_branch_id = v_branch_id
                   WHERE ROWID = rec_c_branch.br_row_id;
            END;

            --COMMIT;
            DBMS_OUTPUT.put_line ('Branch return status ' || v_return_status);
            DBMS_OUTPUT.put_line ('Bank id ' || v_branch_id);

            IF (v_return_status <> 'S')
            THEN
               FOR i IN 1 .. v_msg_count
               LOOP
                  apps.fnd_msg_pub.get (i,
                                        apps.fnd_api.g_false,
                                        lc_msg_data,
                                        lc_msg_dummy);
                  lc_output :=
                        lc_output
                     || (TO_CHAR (i) || ': ' || SUBSTR (lc_msg_data, 1, 250));
               END LOOP;

               DBMS_OUTPUT.put_line (
                  'Error Occured while Creating Bank Branch: ' || lc_msg_data);
               DBMS_OUTPUT.put_line (
                  'v_branch_id --> ' || v_branch_id || lc_msg_data);

               UPDATE rgap_supp_emp_stg
                  SET bank_branch_status = 'ERROR',
                      bank_branch_error = lc_msg_data,
                      bank_branch_id = v_branch_id
                WHERE     ROWID = rec_c_branch.br_row_id
                      AND bank_branch_status = 'NEW';

               --COMMIT;
            ELSE
               DBMS_OUTPUT.put_line ('v_branch_id F --> ' || v_branch_id);

               UPDATE rgap_supp_emp_stg
                  SET bank_branch_status = 'CREATED',
                      bank_branch_id = v_branch_id
                WHERE     ROWID = rec_c_branch.br_row_id
                      AND bank_branch_status = 'NEW';
            END IF;

            --COMMIT;
            DBMS_OUTPUT.put_line ('End of CRAETE BRANCH');
            DBMS_OUTPUT.put_line (
               'Bank BRANCH Loop Ended for ' || rec_c_branch.branch_name);
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('error ' || SQLERRM);
   END;

   BEGIN
      v_bank_id := NULL;
      lc_output := NULL;
      lc_msg_dummy := NULL;
      v_bank_ctry := NULL;
      v_bank_branch := NULL;
      v_branch_id := NULL;
      lc_msg_data := NULL;
      v_error_reason := NULL;
      v_return_status := NULL;
      v_msg_count := NULL;
      v_msg_data := NULL;
      v_branch_type := NULL;
      v_bank_name := NULL;
      vbankname := NULL;
      vvendorname := NULL;
      vsupplierpartyid := NULL;        -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid := NULL;                               -- EXISTING BANK PARTY ID
      vbankbranchid := NULL;                       -- EXISTING BRANCH PARTY ID
      xreturnstatus := NULL;
      xmsgcount := NULL;
      xmsgdata := NULL;
      vinstrumentid := NULL;
      DBMS_OUTPUT.put_line ('c_bank_bra_ass vvendorname ');

      BEGIN
         FOR i IN c_bank_bra_ass
         LOOP
            vvendorname := TRIM (i.vendor_name);
            vbankname := i.bank_name;
            DBMS_OUTPUT.put_line (
               'c_bank_bra_ass i.vendor_name ' || i.vendor_name);

            BEGIN
               SELECT party_id
                 INTO vsupplierpartyid
                 FROM ap_suppliers
                WHERE TRIM (vendor_name) = TRIM (i.vendor_name);

               DBMS_OUTPUT.put_line (vsupplierpartyid);
            EXCEPTION
               WHEN OTHERS
               THEN
                  vsupplierpartyid := 0;
                  DBMS_OUTPUT.put_line (
                     'Exception while fetching Supplier Details ' || SQLERRM);
            END;

            BEGIN
               SELECT bank_party_id, branch_party_id
                 INTO vbankid, vbankbranchid
                 FROM ce_bank_branches_v
                WHERE     UPPER (bank_name) = UPPER (i.bank_name)
                      AND bank_branch_name = i.bank_branch
                      AND NVL (bank_number, 1) = NVL (i.bank_code, 1)
                      AND NVL (BRANCH_NUMBER, 1) = NVL (i.BRANCH_NUMBER, 1);

               DBMS_OUTPUT.put_line (vbankid || ',' || vbankbranchid);
            EXCEPTION
               WHEN OTHERS
               THEN
                  vbankid := 0;
                  vbankbranchid := 0;
                  DBMS_OUTPUT.put_line (
                        'Exception while fetching Bank Branch Details '
                     || SQLERRM);
            END;

            BEGIN
               SELECT party_site_id
                 INTO l_party_site_id
                 FROM ap_supplier_sites_all
                WHERE     vendor_id = i.ora_vendor_id
                      AND vendor_site_id = i.vendor_site_id;
            EXCEPTION
               WHEN OTHERS
               THEN
                  l_party_site_id := NULL;
            END;

            ---
            pextbankacctrec.object_version_number := 1.0;
            pextbankacctrec.acct_owner_party_id := vsupplierpartyid;
            pextbankacctrec.bank_account_name := TRIM (i.account_name);
            pextbankacctrec.bank_account_num := TRIM (i.account_number);
            pextbankacctrec.bank_id := vbankid;
            pextbankacctrec.branch_id := vbankbranchid;
            pextbankacctrec.start_date := SYSDATE;
            pextbankacctrec.country_code := i.bank_country;
            pextbankacctrec.iban := i.iban;
            pextbankacctrec.currency := i.currency;
            --pExtBankAcctRec.currency := 'USD';
            pextbankacctrec.foreign_payment_use_flag := 'Y';

            BEGIN
               iby_ext_bankacct_pub.create_ext_bank_acct (
                  p_api_version         => papiversion,
                  p_init_msg_list       => pinitmsglist,
                  p_ext_bank_acct_rec   => pextbankacctrec,
                  p_association_level   => 'SS',
                  p_supplier_site_id    => i.vendor_site_id,
                  p_party_site_id       => l_party_site_id,
                  p_org_id              => i.operating_unit_id,
                  p_org_type            => 'OPERATING_UNIT',
                  --Bug7136876: new parameter
                  x_acct_id             => xacctid,
                  x_return_status       => xreturnstatus,
                  x_msg_count           => xmsgcount,
                  x_msg_data            => xmsgdata,
                  x_response            => xresponse);
            /*                  UPDATE rgap_supp_emp_stg
                                 SET bank_branch_ass_status = 'APICREATED',
                                     bank_br_ass_status_error =
                                        xmsgdata || '-' || xreturnstatus
                               WHERE ROWID = i.br_row_id;*/
            EXCEPTION
               WHEN OTHERS
               THEN
                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_ass_status = 'APIERROR',
                         bank_br_ass_status_error = xmsgdata
                   WHERE ROWID = i.br_row_id;
            END;

            --COMMIT;
            DBMS_OUTPUT.put_line ('xReturnStatus :' || xreturnstatus);

            IF xreturnstatus = 'S'
            THEN
               UPDATE rgap_supp_emp_stg
                  SET bank_branch_ass_status = 'CREATED'
                WHERE ROWID = i.br_row_id AND bank_branch_ass_status = 'NEW';

               --COMMIT;
            ELSE
               IF xmsgcount > 1
               THEN
                  FOR i IN 1 .. xmsgcount
                  LOOP
                     DBMS_OUTPUT.put_line (
                           i
                        || '.'
                        || SUBSTR (
                              fnd_msg_pub.get (p_encoded => fnd_api.g_false),
                              1,
                              255));
                  END LOOP;

                  UPDATE rgap_supp_emp_stg
                     SET bank_branch_ass_status = 'ERROR',
                         bank_br_ass_status_error = 'Not Assigned'
                   WHERE     ROWID = i.br_row_id
                         AND bank_branch_ass_status = 'NEW';

                  --COMMIT;
               END IF;
            --ROLLBACK;
            END IF;
         ----
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('error ' || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('error ' || SQLERRM);
   END;

   BEGIN
      v_bank_id := NULL;
      lc_output := NULL;
      lc_msg_dummy := NULL;
      v_bank_ctry := NULL;
      v_bank_branch := NULL;
      v_branch_id := NULL;
      lc_msg_data := NULL;
      v_error_reason := NULL;
      v_return_status := NULL;
      v_msg_count := NULL;
      v_msg_data := NULL;
      v_branch_type := NULL;
      v_bank_name := NULL;
      vbankname := NULL;
      vvendorname := NULL;
      vsupplierpartyid := NULL;        -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID
      vbankid := NULL;                               -- EXISTING BANK PARTY ID
      vbankbranchid := NULL;                       -- EXISTING BRANCH PARTY ID
      xreturnstatus := NULL;
      xmsgcount := NULL;
      xmsgdata := NULL;
      vinstrumentid := NULL;

      --V_EXTBANK_REC_TYPE := NULL;
      --V_EXTBANK_REC_TYPE := NULL;

      /*Vendor Bank and Branch assignment code*/
      BEGIN
         FOR i IN c_bank_ven_bra_ass
         LOOP
            vvendorname := TRIM (i.vendor_name);
            vbankname := i.bank_name;

            BEGIN
               SELECT bank_party_id, branch_party_id
                 INTO vbankid, vbankbranchid
                 FROM ce_bank_branches_v
                WHERE     UPPER (bank_name) = UPPER (vbankname)
                      AND bank_branch_name = i.branch_name
                      AND NVL (bank_number, 1) = TRIM (NVL (i.bank_code, 1))
                      AND NVL (BRANCH_NUMBER, 1) =
                             TRIM (NVL (I.BRANCH_NUMBER, 1));
            -- Replace with your Bank Name
            EXCEPTION
               WHEN OTHERS
               THEN
                  vbankid := 0;
                  vbankbranchid := 0;
                  DBMS_OUTPUT.put_line (
                        'Exception while fetching Bank Branch Details '
                     || SQLERRM);
            END;

            -- SELECT EXISTING Supplier
            BEGIN
               SELECT ext_bank_account_id
                 INTO vinstrumentid
                 FROM iby_ext_bank_accounts
                WHERE     branch_id = vbankbranchid
                      AND bank_id = vbankid
                      AND bank_account_name = TRIM (i.account_name)
                      AND TRIM (bank_account_num) = TRIM (i.account_number);
            EXCEPTION
               WHEN OTHERS
               THEN
                  vinstrumentid := 0;
                  DBMS_OUTPUT.put_line (
                     'Exception while fetching Supplier BANK ' || SQLERRM);
            END;

            -- SELECT EXISTING Supplier
            BEGIN
               SELECT party_id
                 INTO vsupplierpartyid
                 FROM ap_suppliers
                WHERE TRIM (vendor_name) = TRIM (i.vendor_name);
            -- Replace with your Supplier Name
            EXCEPTION
               WHEN OTHERS
               THEN
                  vsupplierpartyid := 0;
                  DBMS_OUTPUT.put_line (
                        'vsupplierpartyid Exception while fetching Supplier Details '
                     || SQLERRM);
            END;

            pinstrument.instrument_type := 'BANKACCOUNT';
            pinstrument.instrument_id := vinstrumentid;
            --<bank account id> IBY_EXT_BANKACCT_PUB.CREATE_EXT_BANK_ACCT API account id
            passignmentattribs.start_date := SYSDATE;
            passignmentattribs.instrument := pinstrument;
            ppayee.party_id := vsupplierpartyid;
            ppayee.party_site_id := i.party_site_id;
            ppayee.supplier_site_id := i.vendor_site_id;
            ppayee.payment_function := 'PAYABLES_DISB';
            ppayee.org_id := i.operating_unit_id;           -- Organization ID
            -- EXISTING SUPPLIERS/CUSTOMER PARTY_ID -- which is party_id of the supplier
            /* iby_disbursement_setup_pub.
            set_payee_instr_assignment (
            p_api_version => 1.0,
            p_init_msg_list => fnd_api.g_false,
            p_commit => fnd_api.g_true,
            x_return_status => xreturnstatus,
            x_msg_count => xmsgcount,
            x_msg_data => xmsgdata,
            p_payee => ppayee,
            p_assignment_attribs => passignmentattribs,
            x_assign_id => xassignid,
            x_response => xresponse);*/

            --COMMIT;

            IF xreturnstatus = 'S'
            THEN
               UPDATE rgap_supp_emp_stg
                  SET bank_vend_ass_status = 'CREATED'
                WHERE ROWID = i.br_row_id;

               --COMMIT;
            ELSE
               IF xmsgcount > 1
               THEN
                  FOR i IN 1 .. xmsgcount
                  LOOP
                     DBMS_OUTPUT.put_line (
                           i
                        || '.'
                        || SUBSTR (
                              fnd_msg_pub.get (p_encoded => fnd_api.g_false),
                              1,
                              255));
                  END LOOP;

                  UPDATE rgap_supp_emp_stg
                     SET bank_vend_ass_status = 'ERROR'
                   WHERE ROWID = i.br_row_id;
               END IF;

               --COMMIT;
            END IF;
         END LOOP;
      EXCEPTION
         WHEN OTHERS
         THEN
            DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         DBMS_OUTPUT.put_line ('Error..' || SQLERRM);
   END;
END;